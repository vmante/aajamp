#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1029(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[2];
    z[5]=k[8];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=z[2] - z[1];
    z[10]=z[3]*z[10];
    z[11]=z[7]*z[3];
    z[10]=z[11] + static_cast<T>(1)+ z[10];
    z[10]=z[2]*z[10];
    z[12]=n<T>(3,8)*z[8];
    z[13]= - static_cast<T>(1)+ z[4];
    z[13]=z[5]*z[13];
    z[14]= - static_cast<T>(1)+ z[8];
    z[14]=n<T>(1,4)*z[14] - z[7];
    z[14]=z[9]*z[14];
    z[14]=z[14] + z[8];
    z[14]= - n<T>(3,2)*z[7] - static_cast<T>(1)+ n<T>(1,2)*z[14];
    z[14]=z[9]*z[14];
    z[10]=z[10] + z[14] + n<T>(15,8)*z[13] - z[11] + z[12] - n<T>(19,4) - z[1];
    z[11]=3*z[4];
    z[13]= - n<T>(9,4)*z[4] + static_cast<T>(3)+ z[7];
    z[13]=z[13]*z[11];
    z[14]=n<T>(5,4)*z[5];
    z[15]= - static_cast<T>(7)+ z[11];
    z[15]=z[4]*z[15];
    z[15]=static_cast<T>(5)+ z[15];
    z[15]=z[4]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[14];
    z[13]=z[15] + z[13] - n<T>(9,4) - z[7];
    z[15]=npow(z[4],2);
    z[15]= - static_cast<T>(5)+ 3*z[15];
    z[15]=z[15]*z[14];
    z[15]=z[15] - n<T>(15,2)*z[4] + static_cast<T>(1)+ 3*z[7];
    z[16]= - static_cast<T>(5)- z[9];
    z[17]= - z[8] + 5*z[5];
    z[16]=z[9]*z[17]*z[16];
    z[17]=z[7]*z[8];
    z[16]=n<T>(1,4)*z[16] + z[8] - n<T>(3,4)*z[17];
    z[18]=n<T>(1,2)*z[2];
    z[16]=z[16]*z[18];
    z[19]= - static_cast<T>(1)- z[14];
    z[19]=z[9]*z[19];
    z[15]=z[16] + n<T>(1,2)*z[15] + z[19];
    z[15]=z[2]*z[15];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[6]*z[13];
    z[15]= - n<T>(5,2) + z[3];
    z[15]=z[7]*z[15];
    z[13]=z[15] + z[13];
    z[15]=z[5]*z[4];
    z[15]= - 15*z[15] + 3*z[17] + static_cast<T>(15)- 7*z[8];
    z[12]= - static_cast<T>(1)- z[12];
    z[16]= - static_cast<T>(1)- n<T>(1,4)*z[8];
    z[16]=z[9]*z[16];
    z[12]=z[16] + 3*z[12] + n<T>(5,8)*z[5];
    z[12]=z[9]*z[12];
    z[12]=n<T>(1,8)*z[15] + z[12];
    z[12]=z[12]*z[18];
    z[11]=static_cast<T>(5)- z[11];
    z[11]=z[4]*z[11];
    z[11]= - static_cast<T>(1)+ n<T>(1,2)*z[11];
    z[11]=z[11]*z[14];
    z[11]=z[12] + z[11] + n<T>(25,8)*z[4] - static_cast<T>(2)+ n<T>(1,2)*z[13];
    z[11]=z[6]*z[11];

    r += n<T>(1,2)*z[10] + z[11];
 
    return r;
}

template double qqb_2lha_r1029(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1029(const std::array<dd_real,30>&);
#endif
