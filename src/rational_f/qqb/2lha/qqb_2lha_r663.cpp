#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r663(const std::array<T,30>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[20];
    z[7]=k[6];
    z[8]=k[5];
    z[9]=k[9];
    z[10]=k[4];
    z[11]=k[15];
    z[12]=k[12];
    z[13]=k[28];
    z[14]=n<T>(1,2)*z[9];
    z[15]=17*z[9];
    z[16]=static_cast<T>(19)- z[15];
    z[16]=z[16]*z[14];
    z[16]= - static_cast<T>(3)+ z[16];
    z[17]=z[9] + 1;
    z[18]=z[17]*z[9];
    z[19]=npow(z[9],2);
    z[20]=z[19]*z[10];
    z[21]=n<T>(3,2)*z[18] - z[20];
    z[22]=3*z[10];
    z[21]=z[21]*z[22];
    z[16]=n<T>(1,2)*z[16] + z[21];
    z[21]=n<T>(1,2)*z[10];
    z[16]=z[16]*z[21];
    z[23]=9*z[9];
    z[24]=n<T>(5,2) - z[23];
    z[24]=z[9]*z[24];
    z[24]=z[24] - z[20];
    z[24]=z[24]*z[21];
    z[25]=z[9] - 1;
    z[26]=z[25]*z[9];
    z[24]=z[26] + z[24];
    z[24]=z[10]*z[24];
    z[27]=z[21]*z[19];
    z[28]=z[27] + z[26];
    z[29]=npow(z[10],2);
    z[28]=z[28]*z[29];
    z[30]=z[3]*z[28];
    z[24]=z[24] + n<T>(3,2)*z[30];
    z[24]=z[3]*z[24];
    z[16]=z[16] + z[24];
    z[16]=z[3]*z[16];
    z[24]=n<T>(1,4)*z[9];
    z[30]=3*z[9];
    z[31]=n<T>(17,2) + z[30];
    z[31]=z[31]*z[24];
    z[32]=n<T>(5,2)*z[9];
    z[33]= - static_cast<T>(9)- 13*z[9];
    z[33]=z[33]*z[32];
    z[33]=z[33] + 31*z[20];
    z[34]=n<T>(1,8)*z[10];
    z[33]=z[33]*z[34];
    z[31]=z[33] + static_cast<T>(1)+ z[31];
    z[31]=z[10]*z[31];
    z[16]=z[16] + n<T>(7,8)*z[25] + z[31];
    z[31]=static_cast<T>(5)- 21*z[9];
    z[31]=z[31]*z[24];
    z[31]=z[31] + 7*z[20];
    z[31]=z[31]*z[21];
    z[31]=z[9] + z[31];
    z[33]=n<T>(1,4)*z[10];
    z[31]=z[31]*z[33];
    z[17]=z[17]*z[24];
    z[35]=z[17] - z[20];
    z[35]=z[35]*z[29];
    z[19]=z[19]*npow(z[10],3);
    z[36]=z[19]*z[3];
    z[35]=z[35] + n<T>(1,4)*z[36];
    z[35]=z[3]*z[35];
    z[31]=z[31] + z[35];
    z[31]=z[3]*z[31];
    z[35]=n<T>(1,8) + z[9];
    z[35]=z[9]*z[35];
    z[35]=z[35] - n<T>(21,16)*z[20];
    z[35]=z[10]*z[35];
    z[37]=z[24] - 1;
    z[35]=z[35] + z[37];
    z[35]=z[35]*z[21];
    z[31]=z[35] + z[31];
    z[31]=z[5]*z[31];
    z[35]=z[30] - n<T>(5,2);
    z[35]=z[35]*z[9];
    z[35]=z[35] + n<T>(5,2)*z[20];
    z[35]=z[35]*z[10];
    z[38]=z[26] + z[20];
    z[39]=z[38]*z[29];
    z[40]=z[39]*z[13];
    z[35]=z[35] - z[40];
    z[39]=z[39]*z[5];
    z[39]=z[39] + z[35];
    z[39]=z[13]*z[39];
    z[16]=n<T>(1,16)*z[39] + n<T>(1,2)*z[16] + z[31];
    z[16]=z[5]*z[16];
    z[31]=z[30]*z[25];
    z[31]=z[31] + z[20];
    z[31]=z[31]*z[21];
    z[39]=z[14] - 1;
    z[40]=z[39]*z[9];
    z[40]=z[40] + n<T>(1,2);
    z[31]=z[31] + 3*z[40];
    z[31]=z[31]*z[10];
    z[41]=z[9] - 3;
    z[42]=z[41]*z[14];
    z[42]=z[42] + 1;
    z[31]=z[31] + z[42];
    z[43]=n<T>(1,2)*z[3];
    z[44]=z[31]*z[43];
    z[45]= - static_cast<T>(13)+ 11*z[9];
    z[45]=z[45]*z[14];
    z[46]=n<T>(1,4) + 2*z[9];
    z[46]=z[9]*z[46];
    z[46]=z[46] - z[27];
    z[46]=z[10]*z[46];
    z[45]=z[46] + static_cast<T>(1)+ z[45];
    z[45]=z[10]*z[45];
    z[46]= - n<T>(9,4) + z[9];
    z[46]=z[46]*z[30];
    z[45]= - z[44] + z[45] + n<T>(7,2) + z[46];
    z[45]=z[3]*z[45];
    z[46]= - z[3] + n<T>(9,4)*z[6];
    z[31]=z[31]*z[46];
    z[47]=static_cast<T>(135)- 53*z[9];
    z[47]=z[47]*z[14];
    z[48]=17*z[20];
    z[49]= - static_cast<T>(5)- 19*z[9];
    z[49]=z[9]*z[49];
    z[49]=z[49] + z[48];
    z[49]=z[49]*z[21];
    z[50]=static_cast<T>(65)- n<T>(89,2)*z[9];
    z[50]=z[9]*z[50];
    z[49]=z[49] - n<T>(41,2) + z[50];
    z[49]=z[10]*z[49];
    z[47]=z[49] - static_cast<T>(45)+ z[47];
    z[31]=n<T>(1,8)*z[47] + z[31];
    z[47]=3*z[6];
    z[31]=z[31]*z[47];
    z[49]= - static_cast<T>(5)+ n<T>(23,2)*z[9];
    z[49]=z[49]*z[14];
    z[15]= - static_cast<T>(7)- z[15];
    z[15]=z[15]*z[14];
    z[15]=z[15] + 5*z[20];
    z[15]=z[10]*z[15];
    z[15]=n<T>(3,4)*z[15] - static_cast<T>(1)+ z[49];
    z[15]=z[10]*z[15];
    z[49]= - static_cast<T>(237)+ 127*z[9];
    z[49]=z[49]*z[14];
    z[49]=static_cast<T>(61)+ z[49];
    z[15]=n<T>(1,4)*z[49] + z[15];
    z[15]=z[31] + n<T>(1,2)*z[15] + z[45];
    z[15]=z[6]*z[15];
    z[31]= - n<T>(3,4) - z[9];
    z[31]=z[31]*z[32];
    z[45]=z[9] + n<T>(3,8);
    z[45]=z[45]*z[9];
    z[45]=z[45] - n<T>(3,8)*z[20];
    z[49]=z[45]*z[22];
    z[31]=z[31] + z[49];
    z[31]=z[10]*z[31];
    z[49]=static_cast<T>(19)- z[23];
    z[49]=z[49]*z[30];
    z[49]= - static_cast<T>(59)+ z[49];
    z[50]=static_cast<T>(9)+ 35*z[9];
    z[50]=z[9]*z[50];
    z[50]=z[50] - 9*z[20];
    z[50]=z[50]*z[21];
    z[51]= - static_cast<T>(7)+ n<T>(17,2)*z[9];
    z[51]=z[9]*z[51];
    z[50]=z[50] - n<T>(3,2) + z[51];
    z[50]=z[10]*z[50];
    z[49]=n<T>(1,2)*z[49] + z[50];
    z[50]=z[38]*z[21];
    z[51]= - z[50] + z[40];
    z[51]=z[10]*z[51];
    z[41]=z[41]*z[9];
    z[52]=static_cast<T>(3)+ z[41];
    z[51]=n<T>(1,2)*z[52] + z[51];
    z[52]=9*z[6];
    z[51]=z[51]*z[52];
    z[49]=n<T>(1,2)*z[49] + z[51];
    z[49]=z[6]*z[49];
    z[51]=n<T>(1,2) + 9*z[26];
    z[31]=n<T>(3,4)*z[49] + n<T>(1,8)*z[51] + z[31];
    z[31]=z[6]*z[31];
    z[49]=z[18] - z[20];
    z[49]=z[49]*z[22];
    z[49]= - static_cast<T>(1)+ z[49];
    z[51]= - z[10]*z[38];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[51]*z[47];
    z[49]=n<T>(1,2)*z[49] + z[51];
    z[49]=z[6]*z[49];
    z[51]=z[14] + 1;
    z[51]=z[51]*z[9];
    z[51]=z[51] - z[20];
    z[53]=z[10]*z[51];
    z[49]=z[53] + z[49];
    z[49]=z[49]*z[52];
    z[52]=9*z[10];
    z[51]=z[51]*z[52];
    z[53]= - z[9] + z[20];
    z[52]=z[53]*z[52];
    z[52]= - static_cast<T>(11)+ z[52];
    z[53]=n<T>(1,2)*z[5];
    z[52]=z[52]*z[53];
    z[51]=z[51] + z[52];
    z[51]=z[5]*z[51];
    z[49]=z[49] + z[51];
    z[51]=n<T>(1,8)*z[4];
    z[49]=z[49]*z[51];
    z[52]= - n<T>(5,2) - z[9];
    z[52]=z[52]*z[24];
    z[45]=z[10]*z[45];
    z[45]=z[52] + z[45];
    z[45]=z[10]*z[45];
    z[45]= - n<T>(1,16) + z[45];
    z[52]= - static_cast<T>(15)- 23*z[9];
    z[52]=z[9]*z[52];
    z[52]=z[52] + z[48];
    z[34]=z[52]*z[34];
    z[34]=3*z[37] + z[34];
    z[34]=z[10]*z[34];
    z[34]=n<T>(1,4) + z[34];
    z[52]=n<T>(1,4)*z[5];
    z[34]=z[34]*z[52];
    z[34]=3*z[45] + z[34];
    z[34]=z[5]*z[34];
    z[31]=z[49] + z[31] + z[34];
    z[31]=z[4]*z[31];
    z[34]=npow(z[5],2);
    z[45]=n<T>(1,2)*z[34];
    z[49]=static_cast<T>(5)- z[22];
    z[49]=z[10]*z[49];
    z[49]= - static_cast<T>(1)+ z[49];
    z[49]=z[49]*z[45];
    z[54]=n<T>(3,2)*z[10];
    z[55]=z[54] - 1;
    z[56]=z[55]*z[5];
    z[57]= - n<T>(1,2) - z[56];
    z[57]=z[5]*z[57];
    z[58]= - z[4]*z[45];
    z[57]=z[57] + z[58];
    z[57]=z[4]*z[57];
    z[49]=z[49] + z[57];
    z[57]=n<T>(1,2)*z[4];
    z[49]=z[49]*z[57];
    z[55]=z[55]*z[10];
    z[55]=z[55] + n<T>(1,2);
    z[33]=static_cast<T>(1)- z[33];
    z[33]=z[10]*z[33];
    z[33]= - n<T>(1,2) + z[33];
    z[33]=z[5]*z[10]*z[33];
    z[33]=n<T>(1,2)*z[55] + z[33];
    z[33]=z[5]*z[33];
    z[58]= - static_cast<T>(1)+ z[22];
    z[33]=z[49] + n<T>(1,4)*z[58] + z[33];
    z[33]=z[4]*z[33];
    z[49]=z[25]*z[24];
    z[58]=n<T>(3,2) - z[9];
    z[58]=z[10]*z[58];
    z[58]=n<T>(1,4)*z[25] + z[58];
    z[58]=z[10]*z[58];
    z[59]=z[29]*z[5];
    z[60]=z[10] - 1;
    z[61]=z[60]*z[59];
    z[60]=z[10]*z[60];
    z[60]=n<T>(1,2) + z[60];
    z[60]=z[10]*z[60];
    z[60]=z[60] + n<T>(1,2)*z[61];
    z[53]=z[60]*z[53];
    z[33]=z[33] + z[53] + z[49] + z[58];
    z[33]=z[8]*z[33];
    z[49]= - static_cast<T>(1)+ z[21];
    z[22]=z[49]*z[22];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[22]*z[45];
    z[45]=n<T>(3,4) + z[56];
    z[45]=z[5]*z[45];
    z[49]=z[4]*z[34];
    z[45]=z[45] + n<T>(3,4)*z[49];
    z[45]=z[4]*z[45];
    z[22]=z[22] + z[45];
    z[22]=z[22]*z[57];
    z[45]=z[10]*z[9];
    z[49]= - n<T>(1,2) - z[9];
    z[49]=z[49]*z[45];
    z[53]=static_cast<T>(1)- n<T>(9,8)*z[9];
    z[53]=z[9]*z[53];
    z[49]=z[49] - n<T>(3,4) + z[53];
    z[49]=z[10]*z[49];
    z[53]= - static_cast<T>(1)- z[43];
    z[53]=z[53]*z[59];
    z[53]=z[53] - z[55];
    z[52]=z[53]*z[52];
    z[53]=static_cast<T>(1)- z[26];
    z[22]=n<T>(1,2)*z[33] + z[22] + z[52] + n<T>(1,8)*z[53] + z[49];
    z[33]=n<T>(1,4)*z[8];
    z[22]=z[22]*z[33];
    z[49]= - static_cast<T>(3)+ z[32];
    z[24]=z[49]*z[24];
    z[24]=static_cast<T>(1)+ z[24];
    z[39]=z[39]*z[14]*z[10];
    z[24]=3*z[24] + z[39];
    z[39]= - z[40]*z[54];
    z[39]=z[39] - z[42];
    z[49]=9*z[3];
    z[39]=z[39]*z[49];
    z[52]= - static_cast<T>(115)+ 37*z[9];
    z[52]=z[52]*z[14];
    z[52]=static_cast<T>(3)+ z[52];
    z[53]=n<T>(9,2) - z[9];
    z[53]=z[9]*z[53];
    z[53]= - n<T>(15,2) + z[53];
    z[53]=z[10]*z[53];
    z[52]=n<T>(1,2)*z[52] + z[53];
    z[39]=n<T>(1,4)*z[52] + z[39];
    z[39]=z[3]*z[39];
    z[24]=n<T>(1,2)*z[24] + z[39];
    z[24]=z[3]*z[24];
    z[37]= - z[9]*z[37];
    z[37]= - n<T>(3,4) + z[37];
    z[37]=z[10]*z[37];
    z[37]= - z[51] + n<T>(1,8)*z[26] + z[37];
    z[37]=z[8]*z[37];
    z[39]=static_cast<T>(1)+ n<T>(3,2)*z[9];
    z[39]=z[39]*z[45];
    z[52]=n<T>(7,4)*z[9];
    z[53]= - static_cast<T>(1)+ z[52];
    z[53]=z[9]*z[53];
    z[37]=z[37] + z[39] + n<T>(3,8) + z[53];
    z[37]=z[37]*z[33];
    z[39]=z[42]*z[49];
    z[42]= - static_cast<T>(5)+ n<T>(13,2)*z[9];
    z[42]=z[9]*z[42];
    z[42]=n<T>(33,2) + z[42];
    z[39]=n<T>(1,4)*z[42] + z[39];
    z[39]=z[3]*z[39];
    z[17]=z[17] + z[39];
    z[17]=z[3]*z[17];
    z[39]=z[8]*z[40];
    z[18]= - z[18] + z[39];
    z[18]=z[18]*z[33];
    z[17]=z[17] + z[18];
    z[18]=n<T>(1,2)*z[2];
    z[17]=z[17]*z[18];
    z[17]=z[17] + z[24] + z[37];
    z[17]=z[2]*z[17];
    z[24]=z[25]*z[14];
    z[25]=n<T>(1,2) - z[9];
    z[25]=z[9]*z[25];
    z[25]=z[25] - z[27];
    z[25]=z[10]*z[25];
    z[25]= - z[24] + z[25];
    z[32]=static_cast<T>(2)- z[32];
    z[32]=z[9]*z[32];
    z[32]=z[32] - n<T>(3,4)*z[20];
    z[32]=z[10]*z[32];
    z[37]=static_cast<T>(9)- n<T>(11,2)*z[9];
    z[37]=z[9]*z[37];
    z[37]= - n<T>(7,2) + z[37];
    z[32]=n<T>(1,2)*z[37] + z[32];
    z[32]=z[10]*z[32];
    z[37]=n<T>(5,2) - z[9];
    z[37]=z[9]*z[37];
    z[32]=z[44] + z[32] - n<T>(5,4) + z[37];
    z[32]=z[3]*z[32];
    z[37]= - static_cast<T>(1)+ n<T>(11,4)*z[9];
    z[37]=z[9]*z[37];
    z[27]=z[37] + z[27];
    z[21]=z[27]*z[21];
    z[21]=z[21] + n<T>(1,4) + 2*z[26];
    z[21]=z[10]*z[21];
    z[27]= - static_cast<T>(3)+ z[52];
    z[27]=z[9]*z[27];
    z[27]=n<T>(1,4) + z[27];
    z[21]=z[32] + n<T>(1,2)*z[27] + z[21];
    z[21]=z[3]*z[21];
    z[21]=n<T>(1,2)*z[25] + z[21];
    z[21]=z[7]*z[21];
    z[25]=n<T>(29,8) - z[46];
    z[25]=z[25]*z[47];
    z[27]= - static_cast<T>(3)+ z[3];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(1,4) + z[27];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[25]=z[6]*z[25];
    z[27]=npow(z[3],2);
    z[32]=static_cast<T>(1)- z[43];
    z[32]=z[7]*z[32]*z[27];
    z[25]=z[25] + z[32];
    z[32]=n<T>(1,2) - z[47];
    z[32]=z[32]*npow(z[6],2);
    z[32]=9*z[32] + 5*z[34];
    z[32]=z[32]*z[51];
    z[34]= - n<T>(25,8) + z[49];
    z[27]=z[34]*z[27];
    z[34]=z[2]*npow(z[3],3);
    z[27]=z[27] - n<T>(9,2)*z[34];
    z[18]=z[27]*z[18];
    z[27]=n<T>(13,16) - 2*z[3];
    z[27]=z[3]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[3]*z[27];
    z[18]=z[18] + z[32] + n<T>(1,8)*z[5] + z[27] + n<T>(1,2)*z[25];
    z[18]=z[1]*z[18];
    z[25]=n<T>(15,4)*z[40] + 2*z[20];
    z[25]=z[10]*z[25];
    z[27]=static_cast<T>(97)- 65*z[9];
    z[14]=z[27]*z[14];
    z[14]=z[14] - z[48];
    z[14]=z[14]*z[29];
    z[14]=z[14] + 17*z[36];
    z[14]=z[3]*z[14];
    z[14]=z[25] + n<T>(1,8)*z[14];
    z[14]=z[3]*z[14];
    z[19]=z[43]*z[19];
    z[19]= - z[28] + z[19];
    z[19]=z[3]*z[19];
    z[19]=z[50] + z[19];
    z[19]=z[11]*z[19];
    z[14]=n<T>(1,8)*z[19] + n<T>(1,16)*z[38] + z[14];
    z[14]=z[11]*z[14];
    z[19]= - z[38]*z[33];
    z[19]=z[19] - n<T>(5,4)*z[26] - z[20];
    z[19]=z[8]*z[19];
    z[25]= - z[13]*z[35];
    z[19]=z[19] + n<T>(1,2)*z[25];
    z[19]=z[12]*z[19];
    z[25]= - static_cast<T>(1)- n<T>(31,8)*z[9];
    z[25]=z[25]*z[45];
    z[27]=static_cast<T>(9)- n<T>(23,4)*z[9];
    z[27]=z[9]*z[27];
    z[27]= - n<T>(13,4) + z[27];
    z[25]=n<T>(5,2)*z[27] + z[25];
    z[25]=z[10]*z[25];
    z[27]=n<T>(73,2) - 15*z[9];
    z[27]=z[9]*z[27];
    z[27]= - n<T>(11,2) + z[27];
    z[25]=n<T>(1,4)*z[27] + z[25];
    z[24]=z[10]*z[24];
    z[24]=z[24] + z[40];
    z[24]=z[10]*z[24];
    z[27]=static_cast<T>(2)+ z[41];
    z[24]=2*z[27] + n<T>(27,2)*z[24];
    z[24]=z[3]*z[24];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[3]*z[24];
    z[25]=static_cast<T>(1)- z[30];
    z[25]=z[25]*z[45];
    z[23]=n<T>(41,2) - z[23];
    z[23]=z[9]*z[23];
    z[23]= - n<T>(23,2) + z[23];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[23]=z[10]*z[23];
    z[25]=static_cast<T>(61)- 47*z[9];
    z[25]=z[9]*z[25];
    z[23]=z[23] - static_cast<T>(5)+ n<T>(1,8)*z[25];
    z[23]=n<T>(1,4)*z[23] + z[24];
    z[23]=z[3]*z[23];
    z[20]=z[26] - z[20];

    r += static_cast<T>(1)+ z[14] + z[15] + z[16] + z[17] + z[18] + n<T>(1,8)*z[19] - n<T>(7,8)
      *z[20] + z[21] + z[22] + z[23] + z[31];
 
    return r;
}

template double qqb_2lha_r663(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r663(const std::array<dd_real,30>&);
#endif
