#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2267(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=z[5] - 2;
    z[7]=2*z[1];
    z[8]=z[7] - z[6];
    z[9]= - z[1] + z[6];
    z[10]= - z[4]*z[6];
    z[9]=3*z[9] + z[10];
    z[9]=z[4]*z[9];
    z[8]=3*z[8] + z[9];
    z[8]=z[4]*z[8];
    z[9]=z[1] + 4;
    z[9]=z[9]*z[1];
    z[9]=z[9] + 3;
    z[10]=z[7] + 3;
    z[11]= - z[4] + z[10];
    z[11]=z[4]*z[11];
    z[11]=z[11] - z[9];
    z[11]=z[4]*z[11];
    z[12]=z[1] + 2;
    z[12]=z[12]*z[1];
    z[12]=z[12] + 1;
    z[11]=z[11] + z[12];
    z[13]=z[1] + 1;
    z[13]=3*z[13];
    z[14]= - z[13] + z[4];
    z[14]=z[4]*z[14];
    z[14]=3*z[12] + z[14];
    z[14]=z[4]*z[14];
    z[15]= - static_cast<T>(3)- z[1];
    z[15]=z[1]*z[15];
    z[15]= - static_cast<T>(3)+ z[15];
    z[15]=z[1]*z[15];
    z[14]=z[14] - static_cast<T>(1)+ z[15];
    z[14]=z[3]*z[14];
    z[11]=3*z[11] + z[14];
    z[11]=z[3]*z[11];
    z[14]=3*z[1];
    z[8]=z[11] + z[8] - z[14] + z[6];
    z[8]=z[2]*z[8];
    z[11]=4*z[1];
    z[6]=3*z[6] - z[11];
    z[15]=static_cast<T>(1)- z[5];
    z[15]=2*z[4] + 3*z[15] - z[7];
    z[15]=z[4]*z[15];
    z[6]=2*z[6] + z[15];
    z[6]=z[4]*z[6];
    z[15]=static_cast<T>(14)+ z[1];
    z[15]=z[1]*z[15];
    z[15]=static_cast<T>(15)+ z[15];
    z[16]= - static_cast<T>(15)- z[7];
    z[16]=z[4]*z[16];
    z[15]=2*z[15] + z[16];
    z[15]=z[4]*z[15];
    z[16]= - static_cast<T>(5)- z[7];
    z[16]=z[1]*z[16];
    z[16]= - static_cast<T>(3)+ z[16];
    z[10]=z[4]*z[10];
    z[10]=2*z[16] + z[10];
    z[10]=z[4]*z[10];
    z[7]=static_cast<T>(7)+ z[7];
    z[7]=z[1]*z[7];
    z[7]=static_cast<T>(8)+ z[7];
    z[7]=z[1]*z[7];
    z[7]=z[10] + static_cast<T>(3)+ z[7];
    z[10]=2*z[3];
    z[7]=z[7]*z[10];
    z[16]= - static_cast<T>(26)- 11*z[1];
    z[16]=z[1]*z[16];
    z[7]=z[7] + z[15] - static_cast<T>(15)+ z[16];
    z[7]=z[3]*z[7];
    z[6]=2*z[8] + z[7] + z[6] + 10*z[1] + static_cast<T>(7)- 3*z[5];
    z[6]=z[2]*z[6];
    z[7]= - static_cast<T>(5)- z[1];
    z[7]=z[1]*z[7];
    z[7]= - static_cast<T>(7)+ z[7];
    z[7]=z[1]*z[7];
    z[8]=z[4]*z[9];
    z[7]=z[8] - static_cast<T>(3)+ z[7];
    z[7]=z[7]*z[10];
    z[8]=5*z[1];
    z[9]=static_cast<T>(17)+ z[8];
    z[9]=z[1]*z[9];
    z[8]= - static_cast<T>(12)- z[8];
    z[8]=z[4]*z[8];
    z[7]=z[7] + z[8] + static_cast<T>(12)+ z[9];
    z[7]=z[3]*z[7];
    z[8]=3*z[4] - z[5] - z[14];
    z[8]=z[4]*z[8];
    z[6]=z[6] + z[7] + z[8] - z[11] - static_cast<T>(3)+ z[5];
    z[6]=z[2]*z[6];
    z[7]=z[12]*z[10];
    z[7]= - z[13] + z[7];
    z[7]=z[3]*z[7];

    r +=  - z[1] + z[4] + z[6] + z[7];
 
    return r;
}

template double qqb_2lha_r2267(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2267(const std::array<dd_real,30>&);
#endif
