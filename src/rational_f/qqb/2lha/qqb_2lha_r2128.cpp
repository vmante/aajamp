#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2128(const std::array<T,30>& k) {
  T z[9];
  T r = 0;

    z[1]=k[3];
    z[2]=k[13];
    z[3]=k[4];
    z[4]=k[11];
    z[5]=2*z[3];
    z[6]=npow(z[4],2);
    z[5]=z[5]*z[6];
    z[7]=2*z[4];
    z[8]= - static_cast<T>(1)+ z[1];
    z[8]=z[8]*z[7];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[4]*z[8];
    z[8]=z[8] + z[5];
    z[8]=z[3]*z[8];
    z[7]= - z[1]*z[7];
    z[6]= - z[3]*z[6];
    z[6]=z[7] + z[6];
    z[6]=z[3]*z[6];
    z[7]=npow(z[1],2);
    z[6]= - z[7] + z[6];
    z[6]=z[2]*z[6];
    z[6]=2*z[6] - 3*z[1] + z[8];
    z[6]=z[2]*z[6];
    z[5]=z[6] - static_cast<T>(1)+ z[5];

    r += 18*z[5]*z[2];
 
    return r;
}

template double qqb_2lha_r2128(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2128(const std::array<dd_real,30>&);
#endif
