#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1827(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=k[15];
    z[9]=n<T>(1,2)*z[6];
    z[10]=npow(z[7],2);
    z[11]=z[9]*z[10];
    z[12]=z[7] + 1;
    z[13]=z[12]*z[7];
    z[14]=z[11] + z[13];
    z[15]=z[6]*z[14];
    z[16]= - static_cast<T>(2)- n<T>(3,2)*z[7];
    z[16]=z[7]*z[16];
    z[15]= - n<T>(3,4)*z[15] - n<T>(1,2) + z[16];
    z[15]=z[6]*z[15];
    z[16]=3*z[7];
    z[17]=z[16] + 1;
    z[17]=z[17]*z[7];
    z[18]=z[10]*z[6];
    z[17]=z[17] + z[18];
    z[17]=z[17]*z[9];
    z[19]= - z[13] - z[17];
    z[19]=z[2]*z[19];
    z[15]=z[15] + n<T>(3,4)*z[19];
    z[15]=z[2]*z[15];
    z[19]=npow(z[6],2);
    z[20]= - z[14]*z[19];
    z[21]=z[13] + z[18];
    z[22]= - z[2]*z[21]*z[9];
    z[20]=z[20] + z[22];
    z[20]=z[2]*z[20];
    z[22]=z[10]*npow(z[6],3);
    z[20]= - n<T>(1,2)*z[22] + z[20];
    z[20]=z[8]*z[20];
    z[23]=static_cast<T>(1)- z[16];
    z[23]=z[7]*z[23];
    z[23]=z[23] + n<T>(3,2)*z[18];
    z[23]=z[23]*z[19];
    z[15]=n<T>(3,2)*z[20] + n<T>(1,4)*z[23] + z[15];
    z[15]=z[8]*z[15];
    z[20]=n<T>(1,4)*z[6];
    z[10]=z[10]*z[20];
    z[23]=n<T>(1,2)*z[7];
    z[24]=z[23] + 1;
    z[25]=z[24]*z[7];
    z[10]=z[25] + z[10];
    z[10]=z[10]*z[19];
    z[19]=n<T>(1,4)*z[3];
    z[22]= - z[19]*z[22];
    z[10]=z[10] + z[22];
    z[10]=z[3]*z[10];
    z[14]=z[14]*z[9];
    z[22]=z[14] + n<T>(1,4);
    z[26]= - z[7] - z[22];
    z[26]=z[6]*z[26];
    z[27]=n<T>(5,2)*z[7];
    z[28]=z[27] + 1;
    z[28]=z[28]*z[7];
    z[28]=z[28] + z[18];
    z[28]=z[28]*z[6];
    z[13]=z[28] + n<T>(3,2)*z[13];
    z[28]=z[2]*z[13];
    z[26]= - n<T>(1,4)*z[28] - n<T>(1,2)*z[12] + z[26];
    z[26]=z[2]*z[26];
    z[21]=z[21]*z[20];
    z[28]=z[21] - static_cast<T>(1)- z[25];
    z[28]=z[28]*z[9];
    z[10]=z[15] + z[26] + z[28] + z[10];
    z[10]=z[8]*z[10];
    z[15]=n<T>(1,2)*z[3];
    z[26]= - static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[26]=z[26]*z[15];
    z[28]= - n<T>(7,2) + z[3];
    z[28]=z[28]*z[19];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=z[3]*z[28];
    z[29]=n<T>(1,4)*z[4];
    z[28]=z[29] - n<T>(1,4) + z[28];
    z[28]=z[4]*z[28];
    z[30]=npow(z[3],2);
    z[31]=n<T>(1,2)*z[2];
    z[32]=z[30]*z[31];
    z[33]= - n<T>(1,2) - z[3];
    z[33]=z[3]*z[33];
    z[33]=n<T>(1,2) + z[33];
    z[19]=z[5]*z[33]*z[19];
    z[19]=z[19] + z[32] + z[26] + z[28];
    z[19]=z[1]*z[19];
    z[26]=z[16]*z[12];
    z[28]=z[26] + z[18];
    z[28]=z[28]*z[9];
    z[32]=z[24]*z[16];
    z[28]=z[28] + z[32] + 1;
    z[28]=z[28]*z[6];
    z[32]=z[7] + 3;
    z[33]=z[32]*z[23];
    z[28]=z[28] + z[33] + 1;
    z[28]=z[28]*z[3];
    z[33]= - static_cast<T>(17)- 5*z[7];
    z[33]=z[33]*z[23];
    z[34]= - static_cast<T>(13)- 11*z[7];
    z[34]=z[7]*z[34];
    z[34]=z[34] - 3*z[18];
    z[34]=z[34]*z[9];
    z[35]= - static_cast<T>(15)- n<T>(13,2)*z[7];
    z[35]=z[7]*z[35];
    z[34]=z[34] - static_cast<T>(5)+ z[35];
    z[34]=z[6]*z[34];
    z[33]=z[34] - static_cast<T>(5)+ z[33];
    z[33]=n<T>(1,2)*z[33] + z[28];
    z[33]=z[33]*z[15];
    z[26]=z[26] + z[11];
    z[26]=z[6]*z[26];
    z[34]=static_cast<T>(11)+ n<T>(9,2)*z[7];
    z[34]=z[7]*z[34];
    z[26]=z[26] + n<T>(5,2) + z[34];
    z[26]=z[26]*z[20];
    z[34]=static_cast<T>(2)+ z[23];
    z[34]=z[7]*z[34];
    z[26]=z[33] + z[26] + n<T>(5,8) + z[34];
    z[26]=z[3]*z[26];
    z[33]= - n<T>(5,2) - z[7];
    z[33]=z[33]*z[23];
    z[34]=z[7] + z[18];
    z[20]=z[34]*z[20];
    z[20]=z[20] + static_cast<T>(1)+ z[33];
    z[20]=z[6]*z[20];
    z[33]= - static_cast<T>(5)- z[7];
    z[33]=z[33]*z[23];
    z[33]=static_cast<T>(1)+ z[33];
    z[20]=n<T>(1,2)*z[33] + z[20];
    z[33]=z[7]*z[9];
    z[24]=z[33] + z[24];
    z[24]=z[6]*z[24];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[29];
    z[20]=z[24] + n<T>(1,2)*z[20] + z[26];
    z[20]=z[4]*z[20];
    z[24]=n<T>(1,4)*z[7];
    z[26]=z[24] + 1;
    z[26]=z[26]*z[7];
    z[22]= - z[26] - z[22];
    z[22]=z[6]*z[22];
    z[14]=z[26] + z[14];
    z[14]=z[6]*z[14];
    z[14]=z[23] + z[14];
    z[26]=n<T>(1,2)*z[4];
    z[14]=z[14]*z[26];
    z[29]=z[7] + n<T>(1,2);
    z[14]=z[14] - n<T>(1,2)*z[29] + z[22];
    z[14]=z[4]*z[14];
    z[22]=z[12]*z[23];
    z[29]=z[7]*z[29];
    z[11]=z[29] + z[11];
    z[11]=z[6]*z[11];
    z[11]=z[22] + z[11];
    z[11]=z[4]*z[11];
    z[11]=z[11] - z[13];
    z[11]=z[11]*z[26];
    z[11]= - z[30] + z[11];
    z[11]=z[11]*z[31];
    z[12]=z[12]*z[24];
    z[13]=z[7]*z[32];
    z[13]= - z[6] + n<T>(3,2) + z[13];
    z[13]=z[3]*z[13];
    z[12]=z[13] + static_cast<T>(1)+ z[12];
    z[12]=z[12]*z[15];
    z[11]=z[11] + z[12] + z[14];
    z[11]=z[2]*z[11];
    z[12]= - static_cast<T>(7)- z[16];
    z[12]=z[12]*z[24];
    z[13]=static_cast<T>(1)- z[7];
    z[13]=z[7]*z[13];
    z[13]=z[13] + z[18];
    z[13]=z[13]*z[9];
    z[14]= - static_cast<T>(3)- z[27];
    z[14]=z[7]*z[14];
    z[13]=z[14] + z[13];
    z[9]=z[13]*z[9];
    z[9]= - z[28] + z[9] - static_cast<T>(1)+ z[12];
    z[9]=z[3]*z[9];
    z[12]=z[25] + z[17];
    z[12]=z[6]*z[12];
    z[12]= - z[22] + z[12];
    z[9]=n<T>(1,2)*z[12] + z[9];
    z[9]=z[3]*z[9];
    z[9]=z[21] + z[9];
    z[9]=z[5]*z[9];
    z[12]=static_cast<T>(1)- z[23];
    z[12]=z[12]*z[24];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[6]*z[12];
    z[13]=z[32]*z[16];
    z[13]=static_cast<T>(5)+ z[13];
    z[14]=n<T>(1,4) + z[25];
    z[14]=z[6]*z[14];
    z[13]=n<T>(1,4)*z[13] + 3*z[14];
    z[13]=z[3]*z[13];
    z[14]=n<T>(1,4) - z[7];
    z[12]=z[13] + n<T>(1,2)*z[14] + z[12];
    z[12]=z[3]*z[12];

    r += n<T>(1,2)*z[9] + z[10] + z[11] + z[12] - n<T>(1,8)*z[18] + z[19] + 
      z[20];
 
    return r;
}

template double qqb_2lha_r1827(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1827(const std::array<dd_real,30>&);
#endif
