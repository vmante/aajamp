#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1661(const std::array<T,30>& k) {
    return k[7];
}

template double qqb_2lha_r1661(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1661(const std::array<dd_real,30>&);
#endif
