#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r613(const std::array<T,30>& k) {
  T z[40];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[6];
    z[6]=k[20];
    z[7]=k[9];
    z[8]=k[4];
    z[9]=k[8];
    z[10]=k[10];
    z[11]=k[5];
    z[12]=k[13];
    z[13]=k[15];
    z[14]=n<T>(27,4)*z[6];
    z[15]=n<T>(1,2)*z[5];
    z[16]=n<T>(23,9) + z[15];
    z[17]=n<T>(1,2)*z[2];
    z[18]=n<T>(533,9) - z[17];
    z[18]=z[2]*z[18];
    z[18]= - n<T>(3787,36) + z[18];
    z[18]=z[2]*z[18];
    z[16]=z[18] + 13*z[16] + z[14];
    z[18]= - n<T>(851,9) + z[2];
    z[18]=z[18]*z[17];
    z[19]=n<T>(1,4)*z[12];
    z[20]=n<T>(1,4)*z[2];
    z[21]= - z[20] + n<T>(27,8)*z[6] + n<T>(11,4)*z[5] + n<T>(53,3) + z[19];
    z[21]=z[8]*z[21];
    z[18]=z[21] + z[18] + z[14] + n<T>(1261,36) + 6*z[5];
    z[18]=z[8]*z[18];
    z[21]=n<T>(1,3)*z[2];
    z[22]=static_cast<T>(215)- n<T>(233,6)*z[2];
    z[22]=z[22]*z[21];
    z[23]=7*z[5];
    z[22]=z[22] - z[14] - n<T>(1637,36) - z[23];
    z[24]=z[6] + z[13];
    z[25]=z[5] + z[12];
    z[24]= - n<T>(215,36) - z[25] - n<T>(9,8)*z[24];
    z[24]=z[8]*z[24];
    z[26]=13*z[5];
    z[27]=n<T>(439,9)*z[2] - n<T>(27,2)*z[6] - z[26] - n<T>(851,9) + z[12];
    z[24]=n<T>(1,4)*z[27] + z[24];
    z[24]=z[8]*z[24];
    z[22]=n<T>(1,2)*z[22] + z[24];
    z[22]=z[8]*z[22];
    z[24]=5*z[5];
    z[27]= - n<T>(439,9) + z[2];
    z[27]=z[2]*z[27];
    z[27]=n<T>(1549,18) + z[27];
    z[27]=z[2]*z[27];
    z[27]=z[27] - n<T>(9,2)*z[6] - n<T>(259,9) - z[24];
    z[22]=n<T>(1,4)*z[27] + z[22];
    z[22]=z[7]*z[22];
    z[16]=z[22] + n<T>(1,2)*z[16] + z[18];
    z[16]=z[7]*z[16];
    z[18]=n<T>(103,3)*z[2];
    z[22]=static_cast<T>(2)- z[2];
    z[22]=z[22]*z[18];
    z[27]= - z[18] + n<T>(103,3) + n<T>(3,2)*z[5];
    z[28]=n<T>(1,2)*z[8];
    z[29]= - z[27]*z[28];
    z[30]=n<T>(197,3) + 3*z[5];
    z[22]=z[29] - n<T>(1,2)*z[30] + z[22];
    z[22]=z[8]*z[22];
    z[29]=z[17] - 1;
    z[18]=z[29]*z[18];
    z[18]=z[18] + n<T>(1,4)*z[30];
    z[29]=n<T>(1,2)*z[12];
    z[30]=z[15] + n<T>(103,9) + z[29];
    z[30]=z[8]*z[30];
    z[27]=z[30] + z[27];
    z[27]=z[27]*z[28];
    z[27]=z[27] + z[18];
    z[27]=z[8]*z[27];
    z[30]=z[21] - 1;
    z[31]=103*z[2];
    z[30]=z[30]*z[31];
    z[30]=z[30] + n<T>(203,2);
    z[31]=z[2]*z[30];
    z[32]=n<T>(1,4)*z[5];
    z[27]=z[27] - n<T>(1,6)*z[31] + n<T>(47,9) + z[32];
    z[27]=z[7]*z[27];
    z[31]=z[30]*z[17];
    z[22]=z[27] + z[22] + z[31] - n<T>(47,3) - n<T>(3,4)*z[5];
    z[22]=z[7]*z[22];
    z[27]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[21]=z[21]*z[27]*z[30];
    z[30]= - z[27]*z[15];
    z[18]=z[8]*z[18];
    z[31]=static_cast<T>(2)- z[1];
    z[18]=z[22] + z[18] + z[21] + n<T>(47,9)*z[31] + z[30];
    z[18]=z[3]*z[18];
    z[21]=3*z[1];
    z[22]= - static_cast<T>(7)+ z[21];
    z[22]=z[22]*z[32];
    z[30]=static_cast<T>(85)- n<T>(671,12)*z[1];
    z[31]= - n<T>(103,9) + z[1];
    z[31]=z[2]*z[31];
    z[31]=z[31] - n<T>(1,2) + n<T>(85,9)*z[1];
    z[31]=z[2]*z[31];
    z[30]=n<T>(1,3)*z[30] + z[31];
    z[30]=z[30]*z[17];
    z[31]= - n<T>(59,12) - z[5];
    z[14]=5*z[31] - z[14];
    z[31]=n<T>(403,4) + 56*z[2];
    z[31]=z[2]*z[31];
    z[14]=n<T>(1,2)*z[14] + n<T>(1,9)*z[31];
    z[14]=z[8]*z[14];
    z[31]= - n<T>(151,4) + 20*z[1];
    z[32]=z[6]*z[27];
    z[14]=z[18] + z[16] + z[14] + z[30] + n<T>(9,4)*z[32] + n<T>(1,9)*z[31] + 
    z[22];
    z[14]=z[3]*z[14];
    z[16]=n<T>(7,2)*z[6];
    z[18]=static_cast<T>(1)- z[6];
    z[18]=z[18]*z[16];
    z[22]=n<T>(1,2)*z[9];
    z[18]=z[18] + 15*z[5] + n<T>(205,9) + z[22];
    z[30]= - n<T>(923,18) - z[9];
    z[31]=n<T>(1,8)*z[9];
    z[32]=z[31] + static_cast<T>(1)- n<T>(1,8)*z[10];
    z[32]=z[2]*z[32];
    z[30]=n<T>(1,4)*z[30] + z[32];
    z[30]=z[2]*z[30];
    z[18]=n<T>(1,4)*z[18] + z[30];
    z[30]=z[16] - 13;
    z[32]=n<T>(1,8)*z[6];
    z[33]= - z[30]*z[32];
    z[34]=static_cast<T>(13)- n<T>(7,2)*z[13];
    z[34]=z[13]*z[34];
    z[25]=z[33] + n<T>(1,8)*z[34] + n<T>(1,4) + z[25];
    z[25]=z[8]*z[25];
    z[33]=static_cast<T>(59)- 21*z[6];
    z[33]=z[33]*z[32];
    z[33]= - z[2] + z[33] + n<T>(31,4)*z[5] + n<T>(19,8)*z[13] + n<T>(121,9) - n<T>(7,4)
   *z[12];
    z[25]=n<T>(1,2)*z[33] + z[25];
    z[25]=z[8]*z[25];
    z[33]=n<T>(1207,36) + 19*z[5];
    z[34]=n<T>(21,8)*z[6];
    z[35]=static_cast<T>(5)- z[34];
    z[35]=z[6]*z[35];
    z[33]=n<T>(1,2)*z[33] + z[35];
    z[20]= - n<T>(65,9) + z[20];
    z[20]=z[2]*z[20];
    z[20]=z[25] + n<T>(1,2)*z[33] + z[20];
    z[20]=z[8]*z[20];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[18]=z[7]*z[18];
    z[20]= - n<T>(797,18) + z[12];
    z[20]=n<T>(1,2)*z[20] - z[26];
    z[25]=n<T>(7,8)*z[6];
    z[26]= - static_cast<T>(5)+ 3*z[6];
    z[26]=z[26]*z[25];
    z[26]=z[26] - z[24] - n<T>(43,8)*z[13] - static_cast<T>(1)- z[12];
    z[26]=z[26]*z[28];
    z[28]= - static_cast<T>(2)+ z[34];
    z[28]=z[6]*z[28];
    z[20]=z[26] + z[2] + n<T>(1,2)*z[20] + z[28];
    z[20]=z[8]*z[20];
    z[26]=3*z[9];
    z[28]=n<T>(1855,18) + z[26];
    z[33]=n<T>(3,8)*z[10];
    z[35]= - n<T>(3,8)*z[9] - static_cast<T>(1)+ z[33];
    z[35]=z[2]*z[35];
    z[28]=n<T>(1,4)*z[28] + z[35];
    z[28]=z[28]*z[17];
    z[26]= - n<T>(919,9) - z[26];
    z[35]=7*z[6];
    z[36]=static_cast<T>(1)+ z[35];
    z[36]=z[6]*z[36];
    z[18]=z[18] + z[20] + z[28] + n<T>(3,16)*z[36] + n<T>(1,16)*z[26] - 4*z[5];
    z[18]=z[7]*z[18];
    z[20]=z[27]*z[16];
    z[20]=z[20] - static_cast<T>(7)+ n<T>(17,4)*z[1];
    z[26]=n<T>(1,2)*z[6];
    z[20]=z[20]*z[26];
    z[27]=z[1] - 1;
    z[28]=z[27]*z[2];
    z[36]=n<T>(1,4)*z[9];
    z[28]=z[28] - z[1] + z[36] - n<T>(85,9) - n<T>(1,4)*z[10];
    z[28]=z[2]*z[28];
    z[37]=n<T>(107,9) - z[9];
    z[28]=z[28] + n<T>(1,2)*z[37] + n<T>(47,9)*z[1];
    z[28]=z[2]*z[28];
    z[37]= - n<T>(179,18)*z[1] + static_cast<T>(5)+ z[22];
    z[38]=n<T>(11,4) - z[1];
    z[38]=z[5]*z[38];
    z[20]=z[28] + z[20] + n<T>(1,2)*z[37] + z[38];
    z[28]= - static_cast<T>(1)- z[34];
    z[28]=z[28]*z[26];
    z[34]=n<T>(47,9) + z[2];
    z[34]=z[2]*z[34];
    z[28]=z[34] + z[28] + n<T>(305,72) + 2*z[5];
    z[28]=z[8]*z[28];
    z[14]=z[14] + z[18] + n<T>(1,2)*z[20] + z[28];
    z[14]=z[3]*z[14];
    z[18]= - static_cast<T>(1)+ n<T>(7,8)*z[13];
    z[18]=z[18]*z[13];
    z[18]=z[18] - z[15];
    z[20]=z[25] - 1;
    z[25]=z[20]*z[6];
    z[28]=z[18] - z[29] + z[25];
    z[28]=z[8]*z[28];
    z[34]= - static_cast<T>(1)+ n<T>(7,2)*z[12];
    z[37]=n<T>(7,4)*z[13];
    z[38]= - static_cast<T>(3)+ z[37];
    z[38]=z[13]*z[38];
    z[39]=npow(z[6],2);
    z[28]=z[28] + n<T>(7,8)*z[39] - n<T>(13,4)*z[5] + n<T>(1,2)*z[34] + z[38];
    z[28]=z[8]*z[28];
    z[34]=n<T>(1,4)*z[6];
    z[30]= - z[30]*z[34];
    z[38]= - n<T>(9,2) + z[13];
    z[24]=z[28] + z[17] + z[30] + n<T>(1,4)*z[38] - z[24];
    z[24]=z[8]*z[24];
    z[28]=static_cast<T>(9)- z[16];
    z[28]=z[6]*z[28];
    z[30]=static_cast<T>(3)+ z[9];
    z[30]=z[2]*z[30];
    z[28]=z[30] + z[28] - 9*z[5] - n<T>(3,2) - z[9];
    z[24]=n<T>(1,4)*z[28] + z[24];
    z[28]=n<T>(1,2)*z[7];
    z[24]=z[24]*z[28];
    z[30]=z[16] - 5;
    z[38]=z[6]*z[30];
    z[39]= - static_cast<T>(5)+ z[10];
    z[39]=n<T>(1,2)*z[39] - z[9];
    z[39]=z[2]*z[39];
    z[23]=z[39] + n<T>(3,2)*z[38] + z[23] + n<T>(7,2) + z[9];
    z[20]= - z[20]*z[26];
    z[19]=z[20] + z[19] - z[18];
    z[19]=z[8]*z[19];
    z[20]=z[30]*z[26];
    z[20]=z[20] + n<T>(9,2)*z[5] + n<T>(3,2)*z[13] + n<T>(7,4) - z[12];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[8]*z[19];
    z[19]=z[24] + n<T>(1,4)*z[23] + z[19];
    z[19]=z[7]*z[19];
    z[20]=z[2] - 1;
    z[20]=z[27]*z[20];
    z[23]=z[2]*z[20];
    z[23]=z[23] + z[31] - n<T>(47,9) - z[33];
    z[23]=z[2]*z[23];
    z[24]= - static_cast<T>(3)+ z[1];
    z[24]=z[24]*z[16];
    z[21]=z[24] - n<T>(5,2) + z[21];
    z[21]=z[6]*z[21];
    z[21]=z[21] - z[5] + n<T>(143,9) - z[22];
    z[21]=n<T>(1,4)*z[21] + z[23];
    z[16]=z[16] + 3;
    z[22]= - z[16]*z[34];
    z[15]=z[22] - z[15] + n<T>(47,9) - z[37];
    z[22]=npow(z[2],2);
    z[15]=n<T>(1,2)*z[15] + z[22];
    z[15]=z[8]*z[15];
    z[14]=z[14] + z[19] + n<T>(1,2)*z[21] + z[15];
    z[14]=z[3]*z[14];
    z[15]= - static_cast<T>(11)+ z[35];
    z[15]=z[15]*z[32];
    z[18]=z[18] - z[25];
    z[19]= - z[29] - z[18];
    z[19]=z[8]*z[19];
    z[21]=static_cast<T>(5)- 7*z[13];
    z[21]=z[13]*z[21];
    z[21]=static_cast<T>(1)+ n<T>(1,4)*z[21];
    z[15]=z[19] + z[15] + n<T>(1,2)*z[21] + z[5];
    z[15]=z[8]*z[15];
    z[19]= - 3*z[13] + static_cast<T>(5)+ z[9];
    z[21]=z[2]*z[11];
    z[19]=z[21] + n<T>(1,4)*z[19] + z[5];
    z[15]=n<T>(1,2)*z[19] + z[15];
    z[15]=z[7]*z[15];
    z[17]=z[17] - n<T>(3,4);
    z[17]=z[11]*z[17];
    z[17]= - z[5] + n<T>(3,4)*z[13] - z[36] - static_cast<T>(1)+ z[17];
    z[19]= - n<T>(1,2)*z[11] + z[12];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[8]*z[18];
    z[15]=z[15] + n<T>(1,2)*z[17] + z[18];
    z[15]=z[15]*z[28];
    z[17]=z[22]*z[4];
    z[18]=z[20]*z[17];
    z[16]= - z[6]*z[16];
    z[16]= - n<T>(1,2)*z[10] + z[16];
    z[16]=n<T>(1,4)*z[16] + z[18];
    z[17]=z[8]*z[17];

    r += z[14] + z[15] + n<T>(1,2)*z[16] + z[17];
 
    return r;
}

template double qqb_2lha_r613(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r613(const std::array<dd_real,30>&);
#endif
