#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2235(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[3];
    z[7]= - static_cast<T>(1)+ z[6];
    z[8]=n<T>(1,2)*z[5];
    z[9]= - z[8] + z[3];
    z[9]=z[1]*z[9];
    z[9]=z[9] - static_cast<T>(3)+ z[3];
    z[9]=z[1]*z[9];
    z[7]=n<T>(3,2)*z[7] + z[9];
    z[9]=n<T>(1,3)*z[3];
    z[8]=z[8] - z[9];
    z[10]= - z[1]*z[8];
    z[11]= - n<T>(5,4) + 2*z[3];
    z[10]=n<T>(1,3)*z[11] + z[10];
    z[10]=z[1]*z[10];
    z[11]=z[3] - 1;
    z[12]=n<T>(1,3)*z[11];
    z[10]=z[12] + z[10];
    z[10]=z[4]*z[10];
    z[7]=n<T>(1,4)*z[7] + z[10];
    z[7]=z[4]*z[7];
    z[10]=n<T>(1,2)*z[3];
    z[13]=z[10]*z[1];
    z[14]=z[13] + z[11];
    z[14]=z[1]*z[14];
    z[10]=z[10] - static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[14]=z[14] + z[10];
    z[15]= - z[5] - z[3];
    z[15]=z[1]*z[15];
    z[15]= - z[3] + n<T>(1,3)*z[15];
    z[15]=z[1]*z[15];
    z[15]=z[15] - z[11];
    z[16]=n<T>(1,2)*z[1];
    z[15]=z[15]*z[16];
    z[15]= - n<T>(1,3)*z[10] + z[15];
    z[15]=z[4]*z[15];
    z[14]=n<T>(1,3)*z[14] + n<T>(1,2)*z[15];
    z[14]=z[4]*z[14];
    z[15]= - z[11]*z[16];
    z[10]=z[15] - z[10];
    z[10]=n<T>(1,6)*z[10] + z[14];
    z[10]=z[2]*z[10];
    z[14]=z[1]*z[3];
    z[7]=5*z[10] + z[7] - z[12] - n<T>(1,4)*z[14];
    z[7]=z[2]*z[7];
    z[10]= - n<T>(5,4)*z[5] + z[3];
    z[9]=z[10]*z[9];
    z[8]= - z[8]*z[13];
    z[8]=z[9] + z[8];
    z[8]=z[1]*z[8];
    z[9]= - z[5] + z[3];
    z[9]=z[3]*z[9];
    z[10]=z[1]*z[5];
    z[12]=z[4]*z[10];
    z[9]= - z[12] + z[5] + z[9];
    z[8]=z[8] + n<T>(1,6)*z[9];
    z[8]=z[4]*z[8];
    z[9]= - z[3]*z[5]*z[11];
    z[10]= - npow(z[3],2)*z[10];
    z[9]=z[9] + z[10];
    z[8]=n<T>(1,6)*z[9] + z[8];

    r += z[7] + n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r2235(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2235(const std::array<dd_real,30>&);
#endif
