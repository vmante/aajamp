#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2012(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[13];
    z[7]=z[2]*z[5];
    z[8]=z[5]*z[6];
    z[9]= - 5*z[8] - z[7];
    z[10]=n<T>(1,2)*z[4];
    z[9]=z[9]*z[10];
    z[9]=z[9] + static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[7]= - z[8] + z[7];
    z[7]=z[7]*z[10];
    z[10]=z[2] - 1;
    z[7]=z[7] - z[10];
    z[7]=z[4]*z[7];
    z[10]=z[1]*z[10];
    z[7]=n<T>(1,2)*z[10] + z[7];
    z[7]=z[3]*z[7];
    z[7]=n<T>(1,2)*z[9] + z[7];
    z[7]=z[3]*z[7];
    z[7]= - n<T>(3,4)*z[8] + z[7];

    r += n<T>(1,2)*z[7];
 
    return r;
}

template double qqb_2lha_r2012(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2012(const std::array<dd_real,30>&);
#endif
