#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2285(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=k[18];
    z[9]=z[5] - z[1];
    z[10]= - z[3]*z[9];
    z[10]=z[10] - n<T>(7,2) - z[1];
    z[10]=z[5]*z[10];
    z[11]=z[1] + 1;
    z[12]=z[1]*z[11];
    z[10]=z[12] + z[10];
    z[10]=z[3]*z[10];
    z[12]=n<T>(1,4)*z[8];
    z[13]= - z[12] + z[7];
    z[13]=z[5]*z[13];
    z[10]=n<T>(1,6)*z[10] - n<T>(5,12)*z[11] + z[13];
    z[10]=z[3]*z[10];
    z[11]=z[6] + 1;
    z[13]=z[7]*z[6];
    z[14]=z[11]*z[13];
    z[14]=n<T>(1,6)*z[14] + static_cast<T>(1)+ n<T>(7,6)*z[6];
    z[14]=z[7]*z[14];
    z[15]=n<T>(1,6)*z[7];
    z[11]=z[11]*z[15];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[7]*z[11];
    z[11]= - z[12] + z[11];
    z[11]=z[5]*z[11];
    z[15]=static_cast<T>(3)- z[8];
    z[10]=z[10] + z[11] + n<T>(1,4)*z[15] + z[14];
    z[10]=z[4]*z[10];
    z[11]=npow(z[1],2);
    z[14]= - 2*z[1] + z[5];
    z[14]=z[5]*z[14];
    z[14]=z[11] + z[14];
    z[14]=z[3]*z[14];
    z[9]=n<T>(5,2)*z[9] + z[14];
    z[9]=z[3]*z[9];
    z[9]=n<T>(5,4) + n<T>(1,3)*z[9];
    z[9]=z[3]*z[9];
    z[13]= - static_cast<T>(2)- n<T>(1,2)*z[13];
    z[13]=z[7]*z[13];
    z[9]=z[10] + z[9] + z[12] + n<T>(1,3)*z[13];
    z[9]=z[4]*z[9];
    z[10]=z[1]*z[2];
    z[12]= - static_cast<T>(1)+ n<T>(1,3)*z[10];
    z[11]=z[12]*z[11];
    z[12]=z[1] - n<T>(1,3)*z[5];
    z[12]=z[5]*z[12];
    z[11]=z[11] + z[12];
    z[11]=z[3]*z[11];
    z[12]=static_cast<T>(1)- n<T>(1,2)*z[10];
    z[12]=z[1]*z[12];
    z[11]=z[11] + z[12] - n<T>(1,2)*z[5];
    z[11]=z[3]*z[11];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=n<T>(1,2)*z[10] + z[11];
    z[10]=z[3]*z[10];

    r += z[9] + n<T>(1,2)*z[10];
 
    return r;
}

template double qqb_2lha_r2285(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2285(const std::array<dd_real,30>&);
#endif
