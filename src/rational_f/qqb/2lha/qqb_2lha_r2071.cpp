#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2071(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[15];
    z[9]=n<T>(1,4)*z[8];
    z[10]=n<T>(3,2) - z[6];
    z[10]=z[10]*z[9];
    z[11]= - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[10]=z[10] + z[11];
    z[10]=z[8]*z[10];
    z[12]=n<T>(1,2)*z[6];
    z[13]=z[12] - 1;
    z[14]=z[13]*z[6];
    z[14]=z[14] + n<T>(1,2);
    z[15]=n<T>(3,4)*z[7];
    z[16]= - z[14]*z[15];
    z[16]=z[16] + z[11];
    z[16]=z[7]*z[16];
    z[17]=n<T>(3,2)*z[8];
    z[18]=n<T>(3,2)*z[7];
    z[19]=z[18] + z[17] - static_cast<T>(3)- z[2];
    z[20]=n<T>(1,2)*z[3];
    z[19]=z[19]*z[20];
    z[10]=z[19] + z[16] + n<T>(5,4) + z[10];
    z[10]=z[5]*z[10];
    z[16]=static_cast<T>(5)- z[6];
    z[16]=z[8]*z[16];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=z[16]*z[9];
    z[19]=z[6] - 1;
    z[21]=z[7]*z[19];
    z[21]= - n<T>(1,4) + z[21];
    z[21]=z[7]*z[21];
    z[16]=z[16] + z[21];
    z[21]=z[7] + z[8];
    z[22]=z[2] + n<T>(3,2);
    z[23]= - z[3]*z[22];
    z[23]=n<T>(5,8)*z[21] + z[23];
    z[23]=z[3]*z[23];
    z[10]=z[10] + n<T>(1,2)*z[16] + z[23];
    z[10]=z[5]*z[10];
    z[16]=2*z[3];
    z[23]=n<T>(1,2)*z[7];
    z[24]=n<T>(1,2)*z[8];
    z[25]= - z[16] + z[23] + static_cast<T>(1)+ z[24];
    z[25]=z[3]*z[25];
    z[26]=z[7] + 1;
    z[27]= - z[26]*z[23];
    z[28]= - n<T>(1,2) + z[8];
    z[28]=z[8]*z[28];
    z[27]=z[28] + z[27];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[25]=z[3]*z[25];
    z[17]= - static_cast<T>(5)+ z[17];
    z[17]=z[17]*z[9];
    z[27]=n<T>(1,4)*z[7];
    z[28]= - static_cast<T>(5)- z[18];
    z[28]=z[28]*z[27];
    z[15]=z[15] - static_cast<T>(1)+ n<T>(3,4)*z[8];
    z[15]=z[3]*z[15];
    z[15]=z[15] + z[28] + static_cast<T>(1)+ z[17];
    z[15]=z[3]*z[15];
    z[17]= - z[13]*z[18];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[7]*z[17];
    z[11]=z[8]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[8]*z[11];
    z[11]=z[11] + z[17];
    z[11]=n<T>(1,2)*z[11] + z[15];
    z[11]=z[5]*z[11];
    z[15]=npow(z[8],2);
    z[17]=npow(z[7],2);
    z[15]=z[15] - z[17];
    z[15]=n<T>(1,4)*z[15];
    z[11]=z[11] - z[15] + z[25];
    z[11]=z[5]*z[11];
    z[18]=z[21]*z[20];
    z[21]= - static_cast<T>(1)+ z[9];
    z[21]=z[8]*z[21];
    z[25]= - static_cast<T>(1)- z[27];
    z[25]=z[7]*z[25];
    z[18]=z[18] + z[21] + z[25];
    z[18]=z[3]*z[18];
    z[21]=z[8] - 1;
    z[21]=z[21]*z[8];
    z[25]=z[7]*z[26];
    z[25]= - z[21] + z[25];
    z[18]=n<T>(1,2)*z[25] + z[18];
    z[18]=z[3]*z[18];
    z[15]=z[15] + z[18];
    z[15]=z[4]*z[15]*npow(z[5],2);
    z[11]=z[11] + n<T>(1,2)*z[15];
    z[11]=z[4]*z[11];
    z[15]= - n<T>(1,2)*z[17] + static_cast<T>(1)+ z[21];
    z[17]=z[2] - 1;
    z[18]=z[3]*z[17];
    z[18]= - 3*z[18] - z[22];
    z[18]=z[3]*z[18];
    z[21]= - z[2] - z[8];
    z[18]=n<T>(1,2)*z[21] + z[18];
    z[18]=z[3]*z[18];
    z[10]=z[11] + z[10] + n<T>(1,4)*z[15] + z[18];
    z[10]=z[4]*z[10];
    z[11]=z[8]*z[6];
    z[11]=z[11] - z[19];
    z[11]=z[11]*z[24];
    z[15]=z[7]*z[6];
    z[18]=z[19]*z[15];
    z[21]=static_cast<T>(1)+ z[6];
    z[18]=n<T>(1,2)*z[21] + z[18];
    z[18]=z[7]*z[18];
    z[11]=z[18] - static_cast<T>(1)+ z[11];
    z[12]=z[8]*z[12];
    z[12]=z[12] - z[19];
    z[12]=z[12]*z[24];
    z[14]= - z[14]*z[15];
    z[14]=z[14] - z[19];
    z[14]=z[14]*z[23];
    z[12]=z[14] + z[12] - z[2] + z[13];
    z[12]=z[5]*z[12];
    z[13]= - z[3]*z[2];
    z[11]=z[12] + n<T>(1,2)*z[11] + z[13];
    z[11]=z[5]*z[11];
    z[12]= - static_cast<T>(1)- z[15];
    z[12]=z[12]*z[27];
    z[9]=z[11] + z[12] - z[2] - z[9];
    z[11]=z[2]*z[1];
    z[12]=2*z[1] - z[11];
    z[12]=z[2]*z[12];
    z[12]= - z[1] + z[12];
    z[12]=z[12]*z[16];
    z[13]=z[1] - 1;
    z[14]= - z[2]*z[13];
    z[14]=z[14] - n<T>(5,2) + z[1];
    z[14]=z[2]*z[14];
    z[12]=z[12] + n<T>(3,2) + z[14];
    z[12]=z[3]*z[12];
    z[14]=n<T>(1,2)*z[1];
    z[15]=z[14] - 1;
    z[16]=z[2] + z[15];
    z[16]=z[2]*z[16];
    z[12]=z[12] + n<T>(1,4)*z[13] + z[16];
    z[12]=z[3]*z[12];
    z[9]=z[10] + z[12] + n<T>(1,2)*z[9];
    z[9]=z[4]*z[9];
    z[10]=z[15]*z[14];
    z[12]= - static_cast<T>(5)+ z[14];
    z[12]=z[12]*z[14];
    z[11]=z[12] + z[11];
    z[11]=z[2]*z[11];
    z[12]=static_cast<T>(2)- z[14];
    z[12]=z[1]*z[12];
    z[11]=z[12] + z[11];
    z[11]=z[2]*z[11];
    z[12]=npow(z[1],2);
    z[13]=3*z[12];
    z[14]= - z[2]*z[12];
    z[14]=z[13] + z[14];
    z[14]=z[2]*z[14];
    z[13]= - z[13] + z[14];
    z[13]=z[2]*z[13];
    z[12]=z[12] + z[13];
    z[12]=z[12]*z[20];
    z[10]=z[12] + z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]= - static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[12]= - n<T>(1,2)*z[2] - z[15];
    z[12]=z[2]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[2]*z[11];
    z[10]=z[10] - n<T>(1,4)*z[1] + z[11];
    z[10]=z[3]*z[10];
    z[11]=z[2]*z[17];

    r += z[9] + z[10] + n<T>(1,4)*z[11];
 
    return r;
}

template double qqb_2lha_r2071(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2071(const std::array<dd_real,30>&);
#endif
