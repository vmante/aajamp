#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r552(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[5];
    z[7]=z[6]*z[4];
    z[8]=z[2]*z[4];
    z[9]=static_cast<T>(1)+ 3*z[1];
    z[10]= - z[4]*z[9];
    z[10]=3*z[8] + z[10] - z[7];
    z[10]=z[2]*z[10];
    z[11]=z[6] + n<T>(9,2);
    z[11]=z[11]*z[6];
    z[9]=n<T>(3,2)*z[2] - n<T>(1,2)*z[9] - z[11];
    z[12]=n<T>(1,2)*z[2];
    z[9]=z[9]*z[12];
    z[13]=static_cast<T>(1)+ n<T>(3,4)*z[1];
    z[9]=z[9] - n<T>(1,2)*z[11] - z[13];
    z[9]=z[5]*z[9];
    z[11]=n<T>(1,4)*z[6];
    z[7]=5*z[4] + z[7];
    z[7]=z[7]*z[11];
    z[14]=z[4]*z[13];
    z[7]=z[14] + z[7];
    z[7]=z[7]*npow(z[2],2);
    z[14]=z[11] + 1;
    z[14]=z[14]*z[6];
    z[15]=z[14] + n<T>(3,4);
    z[7]=z[7] - z[15];
    z[16]=z[6] + 5;
    z[11]=z[16]*z[11];
    z[11]=z[11] + z[13];
    z[12]=z[12] + 1;
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,2) + z[12];
    z[11]=z[5]*z[11]*z[12];
    z[7]=n<T>(1,2)*z[7] + z[11];
    z[7]=z[3]*z[7];
    z[7]=z[7] + n<T>(1,2)*z[9] + n<T>(1,8)*z[10] + z[15];
    z[7]=z[3]*z[7];
    z[9]=z[14] - n<T>(3,4)*z[2];
    z[9]=z[5]*z[9];
    z[8]=z[9] - n<T>(3,4)*z[8] - z[15];

    r += z[7] + n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r552(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r552(const std::array<dd_real,30>&);
#endif
