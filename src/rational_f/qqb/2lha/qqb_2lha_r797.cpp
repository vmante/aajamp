#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r797(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[20];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=k[15];
    z[10]=z[7]*npow(z[8],2);
    z[11]=z[5] + z[9];
    z[12]=z[10]*z[11];
    z[13]= - z[8] - 1;
    z[14]=z[5] + 2*z[9];
    z[13]=z[8]*z[14]*z[13];
    z[13]=z[13] + z[12];
    z[13]=z[7]*z[13];
    z[13]=z[13] - z[5];
    z[14]=z[8]*z[5];
    z[13]=20*z[14] + n<T>(49,4) + 10*z[13];
    z[13]=z[7]*z[13];
    z[15]=z[1] - 1;
    z[16]=n<T>(10,9)*z[5];
    z[17]=z[15]*z[16];
    z[12]=z[14] - n<T>(1,3)*z[12];
    z[12]=z[7]*z[12];
    z[12]=z[12] + static_cast<T>(1)- z[5];
    z[12]=z[7]*z[12];
    z[14]=z[2] - 2;
    z[18]=z[14] + z[5];
    z[18]=z[1]*z[18];
    z[12]=n<T>(1,3)*z[18] + z[12];
    z[12]=z[3]*z[12];
    z[18]=z[2] - 1;
    z[19]=z[1]*z[18];
    z[12]=n<T>(10,3)*z[12] + n<T>(1,9)*z[13] + z[17] - n<T>(10,9)*z[14] + n<T>(1,4)*
    z[19];
    z[12]=z[3]*z[12];
    z[13]=z[5]*z[6];
    z[13]= - z[9] + z[13];
    z[13]=z[8]*z[13];
    z[11]=z[13] - z[11];
    z[11]=z[8]*z[11];
    z[13]=z[9] + 2*z[5];
    z[10]=z[13]*z[10];
    z[10]=z[11] + z[10];
    z[10]=n<T>(1,2)*z[4] + n<T>(10,9)*z[10];
    z[10]=z[7]*z[10];
    z[11]=z[15]*z[4]*z[18];

    r += z[10] + n<T>(1,4)*z[11] + z[12] - z[16];
 
    return r;
}

template double qqb_2lha_r797(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r797(const std::array<dd_real,30>&);
#endif
