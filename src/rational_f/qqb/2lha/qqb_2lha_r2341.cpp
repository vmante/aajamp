#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2341(const std::array<T,30>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[17];
    z[7]=k[4];
    z[8]=k[3];
    z[9]=k[21];
    z[10]=k[10];
    z[11]=n<T>(1,2)*z[5];
    z[12]= - z[11] + n<T>(9,2)*z[6];
    z[13]=n<T>(3,2)*z[6];
    z[14]=z[13]*z[7];
    z[14]=z[14] - 3;
    z[15]=z[14] - z[12];
    z[16]=npow(z[3],2);
    z[16]=n<T>(1,3)*z[16];
    z[13]=z[16] + z[13];
    z[17]=n<T>(1,2)*z[1];
    z[18]= - z[13]*z[17];
    z[19]=n<T>(2,3)*z[3];
    z[20]=n<T>(1,4) - z[19];
    z[20]=z[3]*z[20];
    z[15]=z[18] + n<T>(1,2)*z[15] + z[20];
    z[15]=z[1]*z[15];
    z[18]=3*z[8];
    z[20]=3*z[6];
    z[21]=z[18] - n<T>(73,3) - z[20];
    z[22]=n<T>(1,2) + z[6];
    z[22]=z[7]*z[22];
    z[21]=n<T>(1,2)*z[21] + 3*z[22];
    z[22]=n<T>(1,4)*z[7];
    z[23]=static_cast<T>(2)+ z[22];
    z[23]=n<T>(1,3)*z[23] - z[3];
    z[23]=z[3]*z[23];
    z[15]=z[15] + n<T>(1,2)*z[21] + z[23];
    z[15]=z[1]*z[15];
    z[21]=static_cast<T>(5)+ z[20];
    z[21]=z[21]*z[22];
    z[22]=2*z[3];
    z[23]=n<T>(1,2)*z[7];
    z[24]=static_cast<T>(1)+ z[23];
    z[24]=z[7]*z[24];
    z[24]=n<T>(7,2) + z[24];
    z[24]=n<T>(1,2)*z[24] - z[22];
    z[25]=n<T>(1,3)*z[3];
    z[24]=z[24]*z[25];
    z[26]= - static_cast<T>(23)+ z[20];
    z[15]=z[15] + z[24] + z[21] + n<T>(1,4)*z[26] + z[18];
    z[15]=z[1]*z[15];
    z[21]=n<T>(1,4)*z[9];
    z[24]=n<T>(1,3)*z[10];
    z[26]= - z[24] - z[21];
    z[26]=z[7]*z[26];
    z[27]= - n<T>(5,4) - 2*z[10];
    z[28]=n<T>(1,6)*z[8];
    z[29]= - z[10]*z[28];
    z[21]=z[26] + z[29] + n<T>(1,3)*z[27] + z[21];
    z[21]=z[7]*z[21];
    z[26]=n<T>(1,2)*z[9];
    z[27]=z[24] + z[26];
    z[29]= - z[8]*z[27];
    z[30]=static_cast<T>(2)- z[10];
    z[21]=z[21] + z[29] + n<T>(1,3)*z[30] + z[26];
    z[21]=z[7]*z[21];
    z[29]=4*z[3];
    z[30]= - n<T>(1,4) + z[29];
    z[30]=z[30]*z[25];
    z[31]=z[11] + z[20];
    z[32]=n<T>(1,4) + z[25];
    z[32]=z[3]*z[32];
    z[31]=n<T>(1,2)*z[31] + z[32];
    z[31]=z[1]*z[31];
    z[12]=static_cast<T>(9)+ z[12];
    z[12]=z[31] + n<T>(1,2)*z[12] + z[30];
    z[12]=z[1]*z[12];
    z[30]=9*z[8];
    z[31]=n<T>(137,6) - z[30];
    z[32]= - n<T>(5,3) + z[22];
    z[32]=z[3]*z[32];
    z[12]=z[12] + n<T>(1,2)*z[31] + z[32];
    z[12]=z[1]*z[12];
    z[29]= - n<T>(25,4) + z[29];
    z[29]=z[29]*z[25];
    z[31]= - n<T>(119,6) + z[18];
    z[31]=z[8]*z[31];
    z[31]=n<T>(107,6) + z[31];
    z[12]=z[12] + n<T>(1,2)*z[31] + z[29];
    z[12]=z[1]*z[12];
    z[29]= - n<T>(1,2) - z[22];
    z[29]=z[29]*z[25];
    z[31]= - z[5] - z[20];
    z[32]= - n<T>(1,2) - z[25];
    z[32]=z[3]*z[32];
    z[31]=n<T>(1,2)*z[31] + z[32];
    z[17]=z[31]*z[17];
    z[17]=z[17] - static_cast<T>(3)+ z[29];
    z[17]=z[1]*z[17];
    z[29]=z[8] - 1;
    z[31]=z[3] - 1;
    z[32]= - z[3]*z[31];
    z[17]=z[17] + n<T>(9,2)*z[29] + z[32];
    z[17]=z[1]*z[17];
    z[32]=z[18] - n<T>(37,6);
    z[33]= - z[8]*z[32];
    z[34]=n<T>(3,2) - z[19];
    z[34]=z[3]*z[34];
    z[17]=z[17] + z[34] - static_cast<T>(4)+ z[33];
    z[17]=z[1]*z[17];
    z[33]=n<T>(1,6)*z[3];
    z[34]=n<T>(7,2) - z[3];
    z[34]=z[34]*z[33];
    z[35]= - n<T>(7,3) + n<T>(3,4)*z[8];
    z[35]=z[8]*z[35];
    z[35]=n<T>(8,3) + z[35];
    z[35]=z[8]*z[35];
    z[17]=z[17] + z[34] - n<T>(3,2) + z[35];
    z[17]=z[2]*z[17];
    z[34]=n<T>(1,2)*z[8];
    z[35]=static_cast<T>(9)- z[9];
    z[35]=z[8]*z[35];
    z[35]= - n<T>(49,3) + z[35];
    z[35]=z[35]*z[34];
    z[35]=static_cast<T>(5)+ z[35];
    z[36]= - n<T>(3,4) + z[25];
    z[36]=z[3]*z[36];
    z[12]=z[17] + z[12] + n<T>(1,2)*z[35] + z[36];
    z[12]=z[2]*z[12];
    z[17]=z[24] - z[26];
    z[35]= - z[17]*z[23];
    z[36]=n<T>(1,4) - z[10];
    z[35]=z[35] + n<T>(1,3)*z[36] + z[26];
    z[35]=z[7]*z[35];
    z[36]=static_cast<T>(1)- z[10];
    z[36]=n<T>(1,3)*z[36] + z[26];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[35]=z[7]*z[35];
    z[35]=n<T>(1,12) + z[35];
    z[35]=z[7]*z[35];
    z[33]= - z[33] + n<T>(1,6) + z[35];
    z[33]=z[3]*z[33];
    z[35]=n<T>(7,2) - z[17];
    z[35]=z[8]*z[35];
    z[35]= - n<T>(7,3) + z[35];
    z[12]=z[12] + z[15] + z[33] + n<T>(1,2)*z[35] + z[21];
    z[12]=z[4]*z[12];
    z[15]=n<T>(3,2)*z[8];
    z[21]= - static_cast<T>(1)- z[6];
    z[21]=z[7]*z[21];
    z[21]=n<T>(3,2)*z[21] - z[15] + n<T>(65,6) - z[20];
    z[11]=z[20] - z[11] - z[14];
    z[14]= - n<T>(7,12) + z[3];
    z[14]=z[3]*z[14];
    z[16]=n<T>(3,4)*z[6] + z[16];
    z[16]=z[1]*z[16];
    z[11]=z[16] + n<T>(1,2)*z[11] + z[14];
    z[11]=z[1]*z[11];
    z[14]=n<T>(1,6)*z[7];
    z[16]= - z[14] + z[31];
    z[16]=z[3]*z[16];
    z[11]=z[11] + n<T>(1,2)*z[21] + z[16];
    z[11]=z[1]*z[11];
    z[16]= - n<T>(1,3) - z[3];
    z[16]=z[3]*z[16];
    z[16]=z[16] - n<T>(1,4)*z[5] - z[20];
    z[16]=z[1]*z[16];
    z[21]=3*z[3];
    z[31]=n<T>(7,4) - z[21];
    z[31]=z[3]*z[31];
    z[16]=z[16] - n<T>(39,4) + z[31];
    z[16]=z[1]*z[16];
    z[31]= - static_cast<T>(35)+ n<T>(127,4)*z[8];
    z[21]=n<T>(13,3) - z[21];
    z[21]=z[3]*z[21];
    z[16]=z[16] + n<T>(1,3)*z[31] + z[21];
    z[16]=z[1]*z[16];
    z[21]=n<T>(17,2) - z[30];
    z[31]= - n<T>(7,4) + z[22];
    z[31]=z[3]*z[31];
    z[33]=n<T>(1,4) + z[19];
    z[33]=z[3]*z[33];
    z[33]=n<T>(3,2) + z[33];
    z[33]=z[1]*z[33];
    z[21]=z[33] + n<T>(1,2)*z[21] + z[31];
    z[21]=z[1]*z[21];
    z[30]= - n<T>(37,2) + z[30];
    z[30]=z[30]*z[34];
    z[22]= - n<T>(17,4) + z[22];
    z[22]=z[3]*z[22];
    z[21]=z[21] + z[22] + static_cast<T>(7)+ z[30];
    z[21]=z[1]*z[21];
    z[22]=n<T>(19,2) - z[18];
    z[22]=z[8]*z[22];
    z[22]= - n<T>(71,6) + z[22];
    z[22]=z[8]*z[22];
    z[22]=n<T>(17,2) + z[22];
    z[30]= - n<T>(9,4) + z[19];
    z[30]=z[3]*z[30];
    z[21]=z[21] + n<T>(1,2)*z[22] + z[30];
    z[21]=z[2]*z[21];
    z[22]= - static_cast<T>(5)+ z[9];
    z[22]=z[8]*z[22];
    z[22]=static_cast<T>(9)+ z[22];
    z[22]=z[22]*z[18];
    z[22]= - static_cast<T>(19)+ z[22];
    z[30]=n<T>(9,4) - z[3];
    z[30]=z[3]*z[30];
    z[16]=z[21] + z[16] + n<T>(1,4)*z[22] + z[30];
    z[16]=z[2]*z[16];
    z[21]=z[27]*z[34];
    z[22]=z[10] + z[26];
    z[22]=z[22]*z[23];
    z[21]=z[22] + z[21] - z[26] - n<T>(1,3) + n<T>(1,2)*z[10];
    z[21]=z[7]*z[21];
    z[17]=z[17]*z[7];
    z[22]= - n<T>(1,2) + z[10];
    z[22]=z[17] + n<T>(1,3)*z[22] - z[26];
    z[22]=z[7]*z[22];
    z[22]= - n<T>(1,6) + z[22];
    z[22]=z[7]*z[22];
    z[22]=z[25] - n<T>(5,12) + z[22];
    z[22]=z[3]*z[22];
    z[27]= - z[9] - n<T>(11,2) + z[24];
    z[27]=z[8]*z[27];
    z[27]=n<T>(5,2) + z[27];
    z[11]=z[12] + z[16] + z[11] + z[22] + n<T>(1,2)*z[27] + z[21];
    z[11]=z[4]*z[11];
    z[12]= - z[32]*z[34];
    z[16]=static_cast<T>(1)+ z[28];
    z[16]=z[16]*z[23];
    z[21]= - static_cast<T>(2)+ z[7];
    z[19]=z[21]*z[19];
    z[21]=static_cast<T>(11)- 5*z[7];
    z[19]=n<T>(1,4)*z[21] + z[19];
    z[19]=z[3]*z[19];
    z[15]= - static_cast<T>(1)+ z[15];
    z[21]=n<T>(3,4) - z[3];
    z[21]=z[3]*z[21];
    z[15]=n<T>(1,2)*z[15] + z[21];
    z[15]=z[1]*z[15];
    z[12]=z[15] + z[19] + z[16] - static_cast<T>(3)+ z[12];
    z[12]=z[1]*z[12];
    z[15]= - n<T>(13,6) + z[7];
    z[15]=z[15]*z[23];
    z[16]=n<T>(1,3)*z[7];
    z[19]=static_cast<T>(1)- z[23];
    z[19]=z[19]*z[16];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[3]*z[19];
    z[15]=z[19] + n<T>(5,3) + z[15];
    z[15]=z[3]*z[15];
    z[19]=n<T>(1,3)*z[8];
    z[21]= - static_cast<T>(7)+ z[8];
    z[21]=z[21]*z[19];
    z[21]=static_cast<T>(5)+ z[21];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[7]*z[19];
    z[19]=n<T>(1,2)*z[21] + z[19];
    z[19]=z[7]*z[19];
    z[18]= - n<T>(29,3) + z[18];
    z[18]=z[8]*z[18];
    z[18]=static_cast<T>(13)+ z[18];
    z[18]=z[8]*z[18];
    z[18]= - static_cast<T>(11)+ z[18];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[12]=z[12] + n<T>(1,2)*z[18] + z[15];
    z[12]=z[2]*z[12];
    z[15]=static_cast<T>(1)- z[16];
    z[15]=z[3]*z[15];
    z[15]=z[15] - static_cast<T>(1)+ z[14];
    z[15]=z[3]*z[15];
    z[16]= - n<T>(1,12) + z[3];
    z[16]=z[3]*z[16];
    z[16]=n<T>(19,12) + z[16];
    z[16]=z[1]*z[16];
    z[14]=z[16] + z[15] - 3*z[29] + z[14];
    z[14]=z[1]*z[14];
    z[15]=n<T>(17,6) - z[9];
    z[15]=z[8]*z[15];
    z[15]= - n<T>(25,6) + z[15];
    z[15]=z[15]*z[34];
    z[16]=z[8]*z[9];
    z[18]= - static_cast<T>(1)+ z[16];
    z[18]=z[18]*z[34];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[23];
    z[19]= - n<T>(5,3) - z[7];
    z[19]=n<T>(1,4)*z[19] + z[25];
    z[19]=z[3]*z[19];
    z[12]=z[12] + z[14] + z[19] + z[18] + static_cast<T>(1)+ z[15];
    z[12]=z[2]*z[12];
    z[14]=n<T>(1,6) - z[17];
    z[14]=z[3]*z[14];
    z[15]=z[8]*z[26];
    z[14]=z[14] - z[24] + z[15];
    z[14]=z[7]*z[14];
    z[13]= - z[1]*z[13];
    z[15]= - n<T>(19,3) + z[20];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[25];
    z[13]=z[1]*z[13];
    z[15]= - z[16] + n<T>(13,3) + z[9];
    z[15]=z[8]*z[15];
    z[15]= - n<T>(1,3) + z[15];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[14];

    r += z[11] + z[12] + n<T>(1,2)*z[13];
 
    return r;
}

template double qqb_2lha_r2341(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2341(const std::array<dd_real,30>&);
#endif
