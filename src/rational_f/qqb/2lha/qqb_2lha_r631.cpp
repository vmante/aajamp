#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r631(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[7];
    z[5]=k[9];
    z[6]=k[21];
    z[7]=k[8];
    z[8]=k[28];
    z[9]=k[6];
    z[10]=k[13];
    z[11]=k[18];
    z[12]=k[12];
    z[13]=n<T>(1,2)*z[5];
    z[14]=n<T>(1,4)*z[5];
    z[15]= - static_cast<T>(1)- z[14];
    z[15]=z[15]*z[13];
    z[16]=static_cast<T>(1)- n<T>(9,4)*z[5];
    z[16]=z[4]*z[16]*z[13];
    z[17]= - n<T>(5,8) + z[5];
    z[17]=z[5]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[4]*z[16];
    z[15]=z[15] + z[16];
    z[15]=z[4]*z[15];
    z[16]=npow(z[5],2);
    z[17]=z[16]*z[4];
    z[18]=z[16] + z[17];
    z[18]=z[4]*z[18];
    z[19]=z[5] + 1;
    z[20]=z[19]*z[5];
    z[18]=z[20] + z[18];
    z[21]=n<T>(1,2)*z[4];
    z[22]=z[21]*z[1];
    z[18]=z[18]*z[22];
    z[23]=z[4]*z[5];
    z[19]=z[19]*z[23];
    z[24]= - z[20] - z[19];
    z[24]=z[4]*z[24];
    z[24]= - z[20] + z[24];
    z[25]=z[2]*z[4];
    z[24]=z[24]*z[25];
    z[15]=n<T>(1,4)*z[24] + z[15] + z[18];
    z[15]=z[2]*z[15];
    z[18]=z[13] - 1;
    z[24]=z[18]*z[23];
    z[26]=n<T>(1,2)*z[16];
    z[24]=z[26] + 3*z[24];
    z[24]=z[4]*z[24];
    z[27]=n<T>(9,4)*z[16] - z[17];
    z[28]=npow(z[4],2);
    z[29]=z[28]*z[2];
    z[27]=z[27]*z[29];
    z[24]=z[24] + z[27];
    z[24]=z[2]*z[24];
    z[17]= - z[16] + z[17];
    z[17]=z[17]*z[25];
    z[17]=z[17] - z[16] + z[19];
    z[17]=z[2]*z[17];
    z[17]=z[5] + z[17];
    z[19]=n<T>(1,4)*z[10];
    z[17]=z[17]*z[19];
    z[25]= - static_cast<T>(5)+ 3*z[5];
    z[25]=z[25]*z[23];
    z[17]=z[17] + n<T>(1,4)*z[25] + z[24];
    z[17]=z[19]*z[17];
    z[19]=static_cast<T>(1)- 7*z[5];
    z[19]=z[19]*z[14];
    z[24]= - static_cast<T>(1)+ n<T>(3,4)*z[5];
    z[24]=z[24]*z[5];
    z[25]=n<T>(3,2) + 7*z[24];
    z[27]= - n<T>(1,4) - z[24];
    z[27]=z[4]*z[27];
    z[25]=n<T>(1,2)*z[25] + z[27];
    z[25]=z[4]*z[25];
    z[19]=z[19] + z[25];
    z[19]=z[4]*z[19];
    z[25]=n<T>(3,2) - z[5];
    z[25]=z[25]*z[13];
    z[27]= - n<T>(5,4) + z[5];
    z[27]=z[5]*z[27];
    z[27]=n<T>(1,4) + z[27];
    z[27]=z[4]*z[27];
    z[25]=z[25] + z[27];
    z[25]=z[4]*z[25];
    z[25]=z[14] + z[25];
    z[25]=z[4]*z[25];
    z[27]=z[5] - 1;
    z[23]= - z[27]*z[23];
    z[23]= - z[16] + z[23];
    z[23]=z[4]*z[23];
    z[23]= - z[20] + z[23];
    z[30]=z[1]*z[4];
    z[23]=z[23]*z[30];
    z[23]=z[25] + n<T>(1,4)*z[23];
    z[23]=z[1]*z[23];
    z[15]=z[17] + z[15] + z[19] + z[23];
    z[15]=z[2]*z[15];
    z[17]=z[27]*z[13];
    z[19]= - z[4]*z[17];
    z[19]=z[16] + z[19];
    z[19]=z[4]*z[19];
    z[23]=z[16]*z[2];
    z[25]=z[28]*z[23];
    z[19]=z[19] - n<T>(1,4)*z[25];
    z[19]=z[2]*z[19];
    z[25]=n<T>(3,2)*z[5];
    z[31]=z[25]*z[27];
    z[18]=z[18]*z[5];
    z[32]=z[18] + n<T>(1,2);
    z[33]=z[32]*z[4];
    z[34]=z[31] - z[33];
    z[34]=z[4]*z[34];
    z[34]= - z[26] + z[34];
    z[19]=n<T>(1,2)*z[34] + z[19];
    z[19]=z[2]*z[19];
    z[34]=z[33] + z[17];
    z[35]=z[31] + z[23];
    z[36]= - z[2]*z[35];
    z[36]=z[36] - z[32];
    z[36]=z[6]*z[36];
    z[19]=3*z[36] - n<T>(1,2)*z[34] + z[19];
    z[19]=z[9]*z[19];
    z[29]=z[4] + z[29];
    z[36]=z[4] + 1;
    z[29]=z[11]*npow(z[2],2)*z[36]*z[29];
    z[19]=z[19] + z[29];
    z[29]= - static_cast<T>(23)+ 17*z[5];
    z[29]=z[29]*z[13];
    z[29]=static_cast<T>(3)+ z[29];
    z[29]=z[4]*z[29];
    z[29]= - z[16] + z[29];
    z[36]= - z[32]*z[21];
    z[37]=z[27]*z[5];
    z[36]=z[37] + z[36];
    z[36]=z[36]*z[30];
    z[29]=n<T>(1,2)*z[29] + z[36];
    z[29]=z[1]*z[29];
    z[36]=static_cast<T>(5)- 11*z[5];
    z[36]=z[36]*z[14];
    z[29]=z[36] + z[29];
    z[30]=z[32]*z[30];
    z[30]= - z[17] + z[30];
    z[30]=z[1]*z[30];
    z[16]=z[16]*z[1];
    z[35]=z[16] - z[35];
    z[35]=z[2]*z[35];
    z[30]=z[35] + z[30] - z[32];
    z[30]=z[6]*z[30];
    z[29]=n<T>(3,2)*z[30] + n<T>(1,2)*z[29] + z[23];
    z[29]=z[6]*z[29];
    z[20]=z[20] + z[4];
    z[30]=z[1]*z[20];
    z[30]=z[30] - z[5] - z[4];
    z[30]=z[1]*z[30];
    z[18]=z[18] + z[30];
    z[30]=n<T>(1,2)*z[2];
    z[35]=z[30] - z[1];
    z[20]=z[20]*z[35];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[5]*z[13];
    z[13]=z[13] + z[4];
    z[13]=n<T>(1,2)*z[13] + z[20];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,2)*z[18] + z[13];
    z[13]=z[13]*z[30];
    z[18]=z[32]*z[1];
    z[20]=z[1] - 1;
    z[20]=z[6]*z[20]*z[18];
    z[30]=z[20] - z[32];
    z[14]= - z[1]*z[27]*z[14];
    z[27]= - n<T>(7,4) + z[5];
    z[27]=z[5]*z[27];
    z[14]=z[14] + n<T>(3,4) + z[27];
    z[14]=z[1]*z[14];
    z[14]=z[14] + n<T>(3,4)*z[30];
    z[14]=z[6]*z[14];
    z[16]= - z[24] - n<T>(1,4)*z[16];
    z[24]=n<T>(1,2)*z[1];
    z[16]=z[16]*z[24];
    z[27]= - static_cast<T>(1)+ n<T>(5,8)*z[5];
    z[27]=z[5]*z[27];
    z[13]=z[14] + z[13] + z[16] + n<T>(3,8) + z[27];
    z[13]=z[3]*z[13];
    z[14]=n<T>(5,2) - z[1];
    z[14]=z[1]*z[37]*z[14];
    z[14]=3*z[20] - z[31] + z[14];
    z[14]=z[6]*z[14];
    z[16]= - n<T>(3,4)*z[37] - z[33];
    z[16]=z[4]*z[16];
    z[20]=z[37] + z[33];
    z[20]=z[20]*z[22];
    z[16]=z[16] + z[20];
    z[16]=z[1]*z[16];
    z[20]=z[34]*z[21];
    z[14]=n<T>(1,2)*z[14] + z[20] + z[16];
    z[14]=z[7]*z[14];
    z[16]=2*z[5];
    z[20]=z[16] - 3;
    z[20]=z[20]*z[5];
    z[20]=z[20] + 1;
    z[21]=static_cast<T>(2)- z[5];
    z[21]=z[5]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[20];
    z[21]=z[4]*z[21];
    z[18]=z[28]*z[18];
    z[18]=z[18] - z[26] + z[21];
    z[18]=z[1]*z[18];
    z[20]=z[33] - z[20];
    z[20]=z[4]*z[20];
    z[21]=static_cast<T>(1)+ 5*z[37];
    z[18]=z[23] + z[18] + n<T>(1,2)*z[21] + z[20];
    z[18]=z[8]*z[18];
    z[20]=static_cast<T>(7)- n<T>(9,2)*z[5];
    z[20]=z[5]*z[20];
    z[20]=z[33] - n<T>(5,2) + z[20];
    z[20]=z[4]*z[20];
    z[21]= - static_cast<T>(1)+ n<T>(11,4)*z[5];
    z[21]=z[5]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[4]*z[20];
    z[17]=z[17] - z[33];
    z[17]=z[4]*z[17];
    z[17]=z[26] + z[17];
    z[17]=z[17]*z[22];
    z[17]=z[20] + z[17];
    z[17]=z[17]*z[24];
    z[16]= - n<T>(13,4) + z[16];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(1,4)*z[33] + n<T>(5,4) + z[16];
    z[16]=z[4]*z[16];
    z[20]=static_cast<T>(2)- z[25];
    z[20]=z[5]*z[20];
    z[16]=z[16] - n<T>(1,2) + z[20];
    z[16]=z[4]*z[16];
    z[20]= - z[37] - z[23];
    z[20]=z[12]*z[20];

    r += z[13] + n<T>(1,2)*z[14] + z[15] + z[16] + z[17] + z[18] + n<T>(1,4)*
      z[19] + n<T>(1,16)*z[20] + z[29];
 
    return r;
}

template double qqb_2lha_r631(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r631(const std::array<dd_real,30>&);
#endif
