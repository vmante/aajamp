#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2238(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=z[5] - 1;
    z[7]=z[1]*npow(z[4],2);
    z[8]=5*z[4] + z[7];
    z[8]=z[1]*z[8];
    z[8]=static_cast<T>(7)+ z[8];
    z[8]=z[1]*z[8];
    z[8]=z[8] + static_cast<T>(13)- 3*z[5];
    z[8]=z[1]*z[8];
    z[8]= - 6*z[6] + z[8];
    z[8]=z[3]*z[8];
    z[9]= - 3*z[4] - z[7];
    z[9]=z[1]*z[9];
    z[9]= - static_cast<T>(15)+ z[9];
    z[9]=z[1]*z[9];
    z[8]=z[8] + 11*z[6] + z[9];
    z[8]=z[3]*z[8];
    z[9]=z[1]*z[4];
    z[9]=static_cast<T>(5)+ z[9];
    z[9]=z[1]*z[9];
    z[9]= - 7*z[6] + z[9];
    z[9]=z[1]*z[9];
    z[10]=z[5] - 2;
    z[10]=z[10]*z[5];
    z[10]=z[10] + 1;
    z[10]=3*z[10];
    z[9]=z[10] + z[9];
    z[7]= - 6*z[4] - z[7];
    z[7]=z[1]*z[7];
    z[7]= - static_cast<T>(12)+ z[7];
    z[7]=z[1]*z[7];
    z[7]=10*z[6] + z[7];
    z[7]=z[1]*z[7];
    z[7]= - z[10] + z[7];
    z[7]=z[3]*z[7];
    z[7]=2*z[9] + z[7];
    z[7]=z[3]*z[7];
    z[9]=4*z[6] - z[1];
    z[9]=z[1]*z[9];
    z[7]=z[7] - z[10] + z[9];
    z[7]=z[2]*z[7];
    z[6]=z[7] + z[8] - 5*z[6] + 3*z[1];
    z[6]=z[2]*z[6];
    z[7]= - static_cast<T>(1)- z[1];
    z[7]=z[3]*z[7];
    z[7]=static_cast<T>(5)+ 3*z[7];
    z[7]=z[3]*z[7];
    z[6]=z[6] - static_cast<T>(2)+ z[7];

    r += 2*z[6]*z[2];
 
    return r;
}

template double qqb_2lha_r2238(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2238(const std::array<dd_real,30>&);
#endif
