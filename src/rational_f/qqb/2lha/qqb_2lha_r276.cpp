#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r276(const std::array<T,30>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[6];
    z[6]=k[20];
    z[7]=k[3];
    z[8]=k[4];
    z[9]=k[10];
    z[10]=npow(z[7],2);
    z[11]=n<T>(1,2)*z[4];
    z[12]= - z[10]*z[11];
    z[13]=n<T>(3,2)*z[7];
    z[14]=static_cast<T>(1)- z[13];
    z[14]=z[7]*z[14];
    z[12]=z[14] + z[12];
    z[12]=z[4]*z[12];
    z[14]=z[3]*z[8];
    z[12]=z[14] - z[12];
    z[15]=n<T>(1,3)*z[8];
    z[16]=n<T>(1,3)*z[7];
    z[17]=static_cast<T>(1)- z[16];
    z[17]=z[7]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[7]*z[8]*z[17];
    z[17]=z[15] + z[17];
    z[17]=z[5]*z[17];
    z[18]=4*z[6];
    z[19]= - n<T>(269,48) + z[18];
    z[19]=z[8]*z[19];
    z[20]=static_cast<T>(53)- 23*z[8];
    z[20]=z[7]*z[20];
    z[20]=z[20] - static_cast<T>(235)+ 17*z[8];
    z[20]=z[7]*z[20];
    z[20]=z[20] + static_cast<T>(311)+ 155*z[8];
    z[20]=z[7]*z[20];
    z[12]=z[17] + n<T>(1,48)*z[20] - n<T>(43,16) + z[19] - n<T>(3,2)*z[12];
    z[12]=z[5]*z[12];
    z[17]=static_cast<T>(3)- z[8];
    z[17]=n<T>(1,2)*z[17] + z[14];
    z[19]=n<T>(3,4)*z[3];
    z[17]=z[17]*z[19];
    z[20]=n<T>(1,24)*z[7];
    z[21]=53*z[7] - static_cast<T>(49)- 19*z[8];
    z[21]=z[21]*z[20];
    z[22]=3*z[8];
    z[21]=z[21] - n<T>(247,24) + z[22];
    z[23]=n<T>(1,2)*z[7];
    z[21]=z[21]*z[23];
    z[24]=z[4]*z[7];
    z[25]= - static_cast<T>(5)+ z[23];
    z[25]=z[7]*z[25];
    z[25]=n<T>(29,2) + n<T>(23,3)*z[25];
    z[25]=n<T>(1,8)*z[25] - z[24];
    z[25]=z[4]*z[25];
    z[26]=n<T>(37,16) - z[6];
    z[27]=2*z[6];
    z[28]=static_cast<T>(1)+ z[27];
    z[28]=z[6]*z[28];
    z[28]= - n<T>(23,48) + z[28];
    z[28]=z[8]*z[28];
    z[12]=z[12] + z[25] + z[21] + z[17] + 3*z[26] + z[28];
    z[12]=z[5]*z[12];
    z[17]= - static_cast<T>(3)- 7*z[8];
    z[17]=n<T>(3,8)*z[17] + z[14];
    z[17]=z[3]*z[17];
    z[17]=n<T>(3,2)*z[8] + z[17];
    z[21]=n<T>(1,2)*z[3];
    z[17]=z[17]*z[21];
    z[25]=5*z[8];
    z[26]= - n<T>(133,2) + z[25];
    z[26]=n<T>(1,8)*z[26] + 4*z[7];
    z[26]=z[26]*z[16];
    z[28]=static_cast<T>(3)- z[23];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(9,2) + z[28];
    z[29]= - static_cast<T>(1)+ n<T>(61,12)*z[7];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(3,4) + z[29];
    z[29]=z[4]*z[29];
    z[28]=5*z[28] + z[29];
    z[28]=z[4]*z[28];
    z[29]=npow(z[6],2);
    z[30]=3*z[29];
    z[31]=static_cast<T>(1)- z[30];
    z[32]=2*z[29];
    z[33]= - n<T>(5,16) + z[32];
    z[33]=z[8]*z[33];
    z[12]=z[12] + n<T>(1,8)*z[28] + z[26] + z[17] + n<T>(1,2)*z[31] + z[33];
    z[12]=z[5]*z[12];
    z[17]=n<T>(37,2)*z[7];
    z[26]=static_cast<T>(1)+ z[17];
    z[26]=z[26]*z[16];
    z[26]= - n<T>(29,2) + z[26];
    z[26]=z[4]*z[26];
    z[28]=3*z[6];
    z[31]=n<T>(9,8)*z[3];
    z[26]=n<T>(1,16)*z[26] + n<T>(11,16)*z[7] - z[31] - n<T>(61,16) + z[28];
    z[26]=z[4]*z[26];
    z[33]= - static_cast<T>(11)- n<T>(53,2)*z[7];
    z[33]=z[33]*z[20];
    z[17]= - static_cast<T>(83)+ z[17];
    z[17]=z[17]*z[16];
    z[17]=n<T>(43,2) + z[17];
    z[17]=n<T>(1,2)*z[17] - 3*z[24];
    z[24]=n<T>(1,4)*z[4];
    z[17]=z[17]*z[24];
    z[34]=z[23] - 1;
    z[34]=z[34]*z[7];
    z[35]= - n<T>(1,2) - z[34];
    z[35]=z[5]*z[35];
    z[17]=z[35] + z[17] + z[33] + n<T>(9,4)*z[3] + n<T>(85,16) - 6*z[6];
    z[17]=z[5]*z[17];
    z[33]=n<T>(3,8)*z[3];
    z[35]= - static_cast<T>(1)- z[3];
    z[35]=z[35]*z[33];
    z[36]=z[6] - 1;
    z[37]=z[36]*z[6];
    z[35]=z[35] + n<T>(1,32) - z[37];
    z[38]= - static_cast<T>(31)- n<T>(53,4)*z[7];
    z[20]=z[38]*z[20];
    z[17]=z[17] + z[26] + 3*z[35] + z[20];
    z[17]=z[5]*z[17];
    z[20]= - static_cast<T>(3)+ z[6];
    z[20]=z[20]*z[28];
    z[26]=static_cast<T>(3)+ z[3];
    z[26]=z[26]*z[31];
    z[28]=static_cast<T>(7)- n<T>(55,6)*z[7];
    z[28]=z[28]*z[24];
    z[20]=z[28] + n<T>(5,16)*z[7] + z[26] - n<T>(29,16) + z[20];
    z[20]=z[20]*z[11];
    z[26]=npow(z[3],2);
    z[28]=n<T>(11,4) - z[3];
    z[28]=z[28]*z[26];
    z[31]=n<T>(2,3)*z[7];
    z[17]=z[17] + z[20] - z[31] - z[6] + n<T>(3,4)*z[28];
    z[17]=z[5]*z[17];
    z[20]= - z[36]*z[27];
    z[28]= - n<T>(23,8) + z[3];
    z[28]=z[3]*z[28];
    z[28]= - n<T>(3,2) + z[28];
    z[28]=z[28]*z[21];
    z[35]= - static_cast<T>(3)- z[21];
    z[35]=z[35]*z[33];
    z[36]=n<T>(1,2)*z[6];
    z[38]=static_cast<T>(3)- z[36];
    z[38]=z[6]*z[38];
    z[35]=z[35] + n<T>(109,96) + z[38];
    z[35]=z[4]*z[35];
    z[20]=z[35] + z[20] + z[28];
    z[20]=z[4]*z[20];
    z[28]=n<T>(37,48)*z[7];
    z[35]= - static_cast<T>(43)+ 7*z[7];
    z[35]=z[4]*z[35];
    z[38]=static_cast<T>(1)- z[7];
    z[38]=z[5]*z[38];
    z[18]=n<T>(1,3)*z[38] + n<T>(1,48)*z[35] - z[28] - n<T>(3,2)*z[3] - n<T>(71,48) + 
    z[18];
    z[18]=z[5]*z[18];
    z[35]=n<T>(5,2) + z[3];
    z[35]=z[35]*z[19];
    z[38]=z[33] - z[6];
    z[28]= - z[28] + n<T>(11,48) + z[38];
    z[28]=z[4]*z[28];
    z[39]= - static_cast<T>(5)+ z[27];
    z[39]=z[6]*z[39];
    z[18]=z[18] + z[28] + z[35] - n<T>(5,12) + z[39];
    z[18]=z[5]*z[4]*z[18];
    z[18]=z[20] + z[18];
    z[18]=z[5]*z[18];
    z[20]=n<T>(1,2)*z[2];
    z[28]=static_cast<T>(1)+ z[20];
    z[35]=z[20] - 1;
    z[39]= - z[2]*z[35];
    z[39]= - n<T>(1,2) + z[39];
    z[39]=z[3]*z[39];
    z[28]=n<T>(1,2)*z[28] + z[39];
    z[28]=z[3]*z[28];
    z[28]=static_cast<T>(3)+ z[28];
    z[39]=n<T>(1,4)*z[3];
    z[28]=z[28]*z[39];
    z[40]= - n<T>(5,4) + z[30];
    z[40]=z[40]*z[23];
    z[41]= - n<T>(7,2) + z[27];
    z[41]=z[6]*z[41];
    z[28]=z[40] + z[28] + n<T>(15,32) + z[41];
    z[28]=z[4]*z[28];
    z[40]= - static_cast<T>(3)+ z[2];
    z[40]=z[3]*z[40];
    z[40]=n<T>(7,4) + z[40];
    z[40]=z[40]*z[26];
    z[41]=z[29] - n<T>(5,16);
    z[28]=z[28] + n<T>(1,4)*z[40] + z[41];
    z[28]=z[4]*z[28];
    z[40]= - static_cast<T>(1)- z[39];
    z[19]=z[40]*z[19];
    z[40]=static_cast<T>(2)- z[36];
    z[40]=z[6]*z[40];
    z[38]= - n<T>(1,12)*z[5] - n<T>(7,48) + z[38];
    z[38]=z[5]*z[38];
    z[19]=z[38] + z[19] + n<T>(37,96) + z[40];
    z[38]=npow(z[4],2);
    z[19]=z[5]*z[38]*z[19];
    z[40]=n<T>(1,8)*z[3];
    z[42]=static_cast<T>(3)- z[3];
    z[42]=z[3]*z[42];
    z[42]=static_cast<T>(3)+ z[42];
    z[42]=z[42]*z[40];
    z[37]=z[37] + z[42];
    z[37]=z[37]*z[38];
    z[19]=z[37] + z[19];
    z[19]=z[5]*z[19];
    z[37]= - z[3]*z[35];
    z[37]= - n<T>(3,4) + z[37];
    z[26]=z[37]*z[26];
    z[26]=n<T>(1,2)*z[26] - z[41];
    z[26]=z[26]*z[38];
    z[19]=n<T>(1,2)*z[26] + z[19];
    z[19]=z[1]*z[19];
    z[18]=z[19] + z[28] + z[18];
    z[18]=z[1]*z[18];
    z[19]=z[2] - 1;
    z[26]=z[19]*z[3]*z[2];
    z[28]=z[2] - n<T>(3,2);
    z[26]=z[26] + z[28];
    z[37]=z[26]*z[39];
    z[38]=n<T>(5,8) - z[29];
    z[13]=z[38]*z[13];
    z[38]=static_cast<T>(4)- z[6];
    z[38]=z[6]*z[38];
    z[13]=z[13] - n<T>(45,32) + z[38];
    z[13]=z[7]*z[13];
    z[13]=z[13] + z[37] - n<T>(39,32) + z[27];
    z[13]=z[4]*z[13];
    z[27]= - n<T>(11,2) - z[2];
    z[37]= - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[37]=z[2]*z[37];
    z[37]=n<T>(3,4) + z[37];
    z[37]=z[3]*z[37];
    z[27]=n<T>(1,4)*z[27] + z[37];
    z[27]=z[27]*z[21];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[3]*z[27];
    z[37]=static_cast<T>(9)- 5*z[6];
    z[37]=z[6]*z[37];
    z[37]= - n<T>(5,4) + z[37];
    z[32]=n<T>(15,16) - z[32];
    z[32]=z[7]*z[32];
    z[13]=z[13] + z[32] + n<T>(1,2)*z[37] + z[27];
    z[13]=z[4]*z[13];
    z[27]= - n<T>(1,2) - z[2];
    z[32]=static_cast<T>(3)- z[20];
    z[32]=z[3]*z[32];
    z[32]= - n<T>(5,4) + z[32];
    z[32]=z[3]*z[32];
    z[27]=n<T>(1,2)*z[27] + z[32];
    z[27]=z[27]*z[39];
    z[13]=z[18] + z[17] + z[13] + z[27] + n<T>(5,48) - z[29];
    z[13]=z[1]*z[13];
    z[17]= - z[3]*npow(z[2],2);
    z[17]= - n<T>(7,2)*z[28] + z[17];
    z[18]=n<T>(15,16) - z[6];
    z[27]= - n<T>(5,4) + z[29];
    z[27]=z[7]*z[27];
    z[18]=3*z[18] + z[27];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(1,8) + z[18];
    z[18]=z[7]*z[18];
    z[17]=n<T>(1,4)*z[17] + z[18];
    z[17]=z[17]*z[11];
    z[18]= - z[26]*z[40];
    z[26]= - static_cast<T>(5)+ z[6];
    z[26]=z[6]*z[26];
    z[26]=n<T>(5,2) + z[26];
    z[27]= - n<T>(15,16) + z[29];
    z[27]=z[7]*z[27];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[26]=z[7]*z[26];
    z[17]=z[17] + z[26] + z[18] + n<T>(5,4) - z[6];
    z[17]=z[4]*z[17];
    z[18]= - z[8] + z[28];
    z[18]=z[18]*z[39];
    z[26]=z[8] + z[2];
    z[18]=z[18] + static_cast<T>(1)+ n<T>(1,16)*z[26];
    z[18]=z[3]*z[18];
    z[26]=z[8] - static_cast<T>(5)- 3*z[2];
    z[18]=n<T>(1,16)*z[26] + z[18];
    z[18]=z[3]*z[18];
    z[26]= - n<T>(5,12) + z[30];
    z[26]=z[26]*z[23];
    z[27]=n<T>(1,3) + z[20];
    z[28]= - static_cast<T>(2)- z[6];
    z[28]=z[6]*z[28];
    z[30]= - n<T>(1,4) + z[29];
    z[30]=z[8]*z[30];
    z[12]=z[13] + z[12] + z[17] + z[26] + z[18] + z[30] + n<T>(1,2)*z[27] + 
    z[28];
    z[12]=z[1]*z[12];
    z[13]= - static_cast<T>(7)- n<T>(1,8)*z[8];
    z[13]=z[13]*z[15];
    z[15]=n<T>(1,4)*z[7];
    z[17]=3*z[9];
    z[18]=n<T>(23,12) - z[17];
    z[18]=z[8]*z[18];
    z[18]= - 9*z[9] + z[18];
    z[18]=z[18]*z[15];
    z[13]=z[18] - n<T>(9,4) + z[13];
    z[13]=z[7]*z[13];
    z[18]=n<T>(11,2) + z[8];
    z[18]=z[8]*z[18];
    z[18]=static_cast<T>(3)+ z[18];
    z[13]=n<T>(3,4)*z[18] + z[13];
    z[13]=z[7]*z[13];
    z[18]= - n<T>(19,6) - 2*z[8];
    z[18]=z[8]*z[18];
    z[13]=z[13] - n<T>(3,4) + z[18];
    z[13]=z[7]*z[13];
    z[18]=npow(z[8],2);
    z[26]=z[18]*z[33];
    z[27]=z[7]*z[9];
    z[27]=z[27] + 1;
    z[28]=z[27]*z[7];
    z[30]=static_cast<T>(1)- 3*z[28];
    z[30]=z[30]*z[10];
    z[27]=z[27]*z[4];
    z[32]= - npow(z[7],3)*z[27];
    z[30]=z[30] + z[32];
    z[30]=z[4]*z[30];
    z[15]=static_cast<T>(1)- z[15];
    z[15]=z[16]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[7]*z[18]*z[15];
    z[15]=n<T>(1,3)*z[18] + z[15];
    z[15]=z[7]*z[15];
    z[15]= - n<T>(1,12)*z[18] + z[15];
    z[15]=z[5]*z[15];
    z[16]=n<T>(23,12) - z[6];
    z[16]=z[8]*z[16];
    z[16]=n<T>(43,48) + z[16];
    z[16]=z[8]*z[16];
    z[13]=z[15] + n<T>(3,4)*z[30] + z[13] + z[16] + z[26];
    z[13]=z[5]*z[13];
    z[15]= - z[18]*z[21];
    z[16]= - static_cast<T>(1)+ z[8];
    z[16]=z[8]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[15]*z[33];
    z[16]=n<T>(1,2)*z[8];
    z[21]=n<T>(19,24) - 5*z[9];
    z[21]=z[21]*z[16];
    z[21]= - n<T>(53,96)*z[7] + z[21] + n<T>(25,16) - 6*z[9];
    z[21]=z[7]*z[21];
    z[26]= - static_cast<T>(139)+ z[25];
    z[26]=z[8]*z[26];
    z[26]= - static_cast<T>(59)+ n<T>(1,6)*z[26];
    z[21]=n<T>(1,8)*z[26] + z[21];
    z[21]=z[7]*z[21];
    z[26]=n<T>(251,2) - z[25];
    z[26]=z[8]*z[26];
    z[26]=n<T>(229,2) + z[26];
    z[21]=n<T>(1,24)*z[26] + z[21];
    z[21]=z[7]*z[21];
    z[10]= - z[10]*z[27];
    z[26]=static_cast<T>(1)- n<T>(9,2)*z[28];
    z[26]=z[7]*z[26];
    z[10]=z[26] + z[10];
    z[10]=z[4]*z[10];
    z[26]=z[36] + 1;
    z[26]=z[26]*z[6];
    z[28]=n<T>(5,12) - z[26];
    z[28]=z[8]*z[28];
    z[28]=z[28] - n<T>(161,48) + z[6];
    z[28]=z[8]*z[28];
    z[10]=z[13] + z[10] + z[21] + z[15] - n<T>(29,32) + z[28];
    z[10]=z[5]*z[10];
    z[13]= - n<T>(3,2) - z[8];
    z[13]=z[13]*z[22];
    z[15]=static_cast<T>(3)+ z[25];
    z[15]=z[15]*z[16];
    z[18]= - z[3]*z[18];
    z[15]=z[15] + z[18];
    z[15]=z[3]*z[15];
    z[13]=z[13] + z[15];
    z[13]=z[13]*z[40];
    z[15]=11*z[9];
    z[18]=n<T>(49,24) - z[15];
    z[21]= - n<T>(5,24) - z[17];
    z[21]=z[8]*z[21];
    z[18]= - z[31] + n<T>(1,2)*z[18] + z[21];
    z[18]=z[7]*z[18];
    z[21]=n<T>(5,2) - z[8];
    z[21]=z[8]*z[21];
    z[21]= - n<T>(127,3) + z[21];
    z[18]=n<T>(1,8)*z[21] + z[18];
    z[18]=z[7]*z[18];
    z[21]=static_cast<T>(3)+ z[6];
    z[21]=z[6]*z[21];
    z[21]= - n<T>(35,24) + z[21];
    z[25]= - z[8]*z[41];
    z[21]=n<T>(1,2)*z[21] + z[25];
    z[21]=z[8]*z[21];
    z[15]=n<T>(5,8)*z[7] - n<T>(15,8) - z[15];
    z[15]=z[7]*z[15];
    z[15]= - z[27] - n<T>(73,8) + z[15];
    z[15]=z[7]*z[15];
    z[15]=n<T>(3,8) + z[15];
    z[15]=z[15]*z[24];
    z[10]=z[10] + z[15] + z[18] + z[13] + n<T>(31,16) + z[21];
    z[10]=z[5]*z[10];
    z[13]= - z[29]*z[16];
    z[15]= - z[9]*z[20];
    z[15]= - static_cast<T>(1)+ z[15];
    z[16]=static_cast<T>(1)+ n<T>(3,2)*z[6];
    z[16]=z[6]*z[16];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[16];
    z[13]=z[8]*z[13];
    z[15]=n<T>(1,2) - z[17];
    z[15]=n<T>(1,2)*z[15] - z[29];
    z[15]=z[8]*z[15];
    z[16]=n<T>(5,24) - z[29];
    z[16]=z[16]*z[23];
    z[15]=z[16] + z[15] + z[26] - n<T>(1,8)*z[2] - n<T>(65,48) - 2*z[9];
    z[15]=z[7]*z[15];
    z[16]= - static_cast<T>(3)+ z[7];
    z[16]=z[7]*z[16];
    z[16]=static_cast<T>(23)+ 5*z[16];
    z[16]=z[7]*z[16];
    z[16]=n<T>(1,8)*z[16] - n<T>(5,8) + z[2];
    z[16]=z[4]*z[16]*z[23];
    z[17]=n<T>(5,4)*z[34] + n<T>(9,8) - z[9];
    z[17]=z[7]*z[17];
    z[16]=z[16] + z[17] + z[35];
    z[11]=z[16]*z[11];
    z[16]=z[22] - z[19];
    z[14]=z[14] + z[2] - n<T>(7,2)*z[8];
    z[14]=z[3]*z[14];
    z[14]=n<T>(3,2)*z[16] + z[14];
    z[14]=z[14]*z[40];
    z[16]= - n<T>(1,4) - z[9];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(7,4) + z[16];

    r +=  - z[6] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + n<T>(1,4)
      *z[16];
 
    return r;
}

template double qqb_2lha_r276(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r276(const std::array<dd_real,30>&);
#endif
