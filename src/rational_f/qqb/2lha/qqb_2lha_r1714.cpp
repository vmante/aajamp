#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1714(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=2*z[4];
    z[8]= - static_cast<T>(1)- z[7];
    z[8]=z[8]*z[7];
    z[9]=z[3]*z[4];
    z[10]=7*z[4];
    z[11]=static_cast<T>(8)- z[10];
    z[11]=z[11]*z[9];
    z[12]=3*z[4];
    z[13]=z[12] - z[9];
    z[14]=2*z[2];
    z[13]=z[13]*z[14];
    z[8]=z[13] + z[8] + z[11];
    z[8]=z[2]*z[3]*z[8];
    z[10]= - static_cast<T>(3)+ z[10];
    z[10]=z[10]*z[9];
    z[11]=npow(z[4],2);
    z[13]=2*z[11];
    z[10]=z[13] + z[10];
    z[15]=2*z[3];
    z[10]=z[10]*z[15];
    z[8]=z[10] + z[8];
    z[8]=z[2]*z[8];
    z[10]=static_cast<T>(2)- z[2];
    z[10]=z[2]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[16]=npow(z[9],2);
    z[10]=z[1]*z[14]*z[16]*z[10];
    z[8]=z[10] - 7*z[16] + z[8];
    z[8]=z[1]*z[8];
    z[10]=static_cast<T>(2)- 5*z[4];
    z[10]=z[10]*z[7];
    z[16]=9*z[4];
    z[17]=static_cast<T>(22)- z[16];
    z[17]=z[4]*z[17];
    z[17]= - static_cast<T>(6)+ z[17];
    z[17]=z[3]*z[17];
    z[10]=z[10] + z[17];
    z[10]=z[3]*z[10];
    z[17]=z[12] - 2;
    z[17]= - z[3]*z[17];
    z[17]=3*z[2] + z[17];
    z[18]=z[3] - 1;
    z[17]=z[18]*z[17];
    z[17]= - static_cast<T>(1)+ z[4] + z[17];
    z[17]=z[17]*z[14];
    z[10]=z[17] - z[13] + z[10];
    z[10]=z[2]*z[10];
    z[13]=z[5] + 1;
    z[16]=z[13]*z[16];
    z[16]= - static_cast<T>(7)+ z[16];
    z[16]=z[16]*z[9];
    z[16]=10*z[11] + z[16];
    z[16]=z[3]*z[16];
    z[8]=z[8] + z[16] + z[10];
    z[8]=z[1]*z[8];
    z[10]=z[5] - 1;
    z[16]=z[6] - 3;
    z[16]= - z[14]*z[16]*z[10];
    z[17]=2*z[5];
    z[19]=static_cast<T>(5)+ z[17];
    z[19]=z[3]*z[19];
    z[16]=z[16] + z[19] - static_cast<T>(1)+ 4*z[5];
    z[16]=z[2]*z[16];
    z[19]= - z[12] + static_cast<T>(3)+ z[5];
    z[15]=z[19]*z[15];
    z[12]=z[15] + static_cast<T>(1)- z[12];
    z[12]=z[3]*z[12];
    z[12]=z[12] + z[16];
    z[12]=z[2]*z[12];
    z[15]=npow(z[5],2);
    z[16]=z[15]*z[4];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=2*z[17] - 5*z[16];
    z[17]=z[17]*z[9];
    z[19]=z[4]*z[5];
    z[20]=static_cast<T>(3)- 8*z[19];
    z[20]=z[4]*z[20];
    z[17]=z[20] + z[17];
    z[17]=z[3]*z[17];
    z[8]=z[8] + z[12] - 3*z[11] + z[17];
    z[8]=z[1]*z[8];
    z[7]=z[10]*z[7];
    z[7]= - static_cast<T>(1)+ z[7];
    z[7]=z[7]*z[19];
    z[12]=z[10]*z[16];
    z[12]= - z[15] + z[12];
    z[9]=z[12]*z[9];
    z[7]=z[9] - static_cast<T>(1)+ z[7];
    z[7]=z[3]*z[7];
    z[9]=z[10]*z[11];
    z[10]=z[5] - 2;
    z[11]= - z[5]*z[6]*z[10];
    z[11]=z[11] - z[6];
    z[11]=z[11]*z[2];
    z[12]=static_cast<T>(3)- z[5];
    z[12]=z[5]*z[12];
    z[12]= - z[11] - static_cast<T>(2)+ z[12];
    z[12]=z[12]*z[14];
    z[13]=z[3]*z[13];
    z[12]=z[12] + z[13] + static_cast<T>(2)+ z[5];
    z[12]=z[2]*z[12];
    z[7]=z[8] + z[12] + z[7] + static_cast<T>(1)+ z[9];
    z[7]=z[1]*z[7];
    z[8]=z[6] - z[10];
    z[8]=z[5]*z[8];
    z[8]= - z[11] + z[8] - static_cast<T>(1)- z[6];
    z[8]=z[2]*z[8];
    z[9]=z[5]*z[18];

    r += static_cast<T>(2)+ z[7] + z[8] + z[9];
 
    return r;
}

template double qqb_2lha_r1714(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1714(const std::array<dd_real,30>&);
#endif
