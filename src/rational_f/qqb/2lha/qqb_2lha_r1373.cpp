#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1373(const std::array<T,30>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[12];
    z[9]=k[4];
    z[10]=n<T>(1,2)*z[3];
    z[11]=n<T>(1,2)*z[5];
    z[12]=z[11] + z[10] - 1;
    z[13]=z[9] - 1;
    z[13]=z[13]*z[9];
    z[14]=z[8] - 1;
    z[13]=z[13] - z[14];
    z[12]=z[7]*z[13]*z[12];
    z[13]=n<T>(1,4) - z[8];
    z[15]=static_cast<T>(19)- n<T>(11,2)*z[9];
    z[15]=z[9]*z[15];
    z[13]=5*z[13] + n<T>(1,4)*z[15];
    z[15]= - static_cast<T>(1)+ n<T>(7,4)*z[8];
    z[16]=n<T>(5,12) + z[9];
    z[16]=z[9]*z[16];
    z[15]=n<T>(5,3)*z[15] + z[16];
    z[10]=z[15]*z[10];
    z[15]=n<T>(1,2)*z[9];
    z[16]= - static_cast<T>(43)- z[9];
    z[16]=z[16]*z[15];
    z[17]=static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[16]=5*z[17] + z[16];
    z[16]=z[5]*z[16];
    z[10]=n<T>(5,4)*z[12] + n<T>(1,12)*z[16] + n<T>(1,3)*z[13] + z[10];
    z[12]=n<T>(1,2)*z[7];
    z[10]=z[10]*z[12];
    z[13]=n<T>(5,3)*z[8];
    z[16]= - n<T>(11,12) + z[2];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(1,12) + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[16] - z[13] + n<T>(19,12) - z[2];
    z[17]=z[2] - 1;
    z[18]=n<T>(1,3)*z[3];
    z[19]= - z[17]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[19];
    z[16]=z[3]*z[16];
    z[19]=static_cast<T>(1)- n<T>(1,2)*z[2];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[18]=z[19]*z[18];
    z[17]=n<T>(1,8)*z[17] + z[18];
    z[17]=z[3]*z[4]*z[17];
    z[18]=z[6] + z[3];
    z[19]= - z[7] + 1;
    z[18]=z[7]*z[19]*z[8]*z[18];
    z[19]=z[5]*z[4];
    z[20]=z[19]*z[6];
    z[21]=z[20] + z[4];
    z[17]=n<T>(5,8)*z[18] + z[17] - n<T>(1,8)*z[21];
    z[17]=z[1]*z[17];
    z[18]=z[19] + 1;
    z[21]=n<T>(3,2)*z[6];
    z[18]=z[21]*z[18];
    z[22]=n<T>(3,2) - z[6];
    z[22]=z[4]*z[22];
    z[18]=z[22] + z[18];
    z[22]=n<T>(1,4)*z[5];
    z[18]=z[18]*z[22];
    z[23]=3*z[2];
    z[24]=static_cast<T>(1)- z[23];
    z[24]=z[4]*z[24];
    z[24]=z[24] + static_cast<T>(3)- z[6];
    z[25]=z[7]*z[14];
    z[25]= - n<T>(15,2)*z[25] - static_cast<T>(3)+ n<T>(65,6)*z[8];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(5,2)*z[6] + z[25];
    z[25]=z[7]*z[25];
    z[16]=z[17] + n<T>(1,4)*z[25] + z[18] + n<T>(1,8)*z[24] + z[16];
    z[16]=z[1]*z[16];
    z[17]=n<T>(1,2)*z[4];
    z[18]= - n<T>(1,6) - z[2];
    z[18]=z[18]*z[17];
    z[18]=z[18] + 1;
    z[18]=z[2]*z[18];
    z[24]=n<T>(1,2)*z[6];
    z[25]= - static_cast<T>(1)+ z[24];
    z[15]=z[25]*z[15];
    z[15]=z[16] + z[15] - n<T>(1,8)*z[6] - n<T>(3,2) + z[18];
    z[16]=z[6] - 1;
    z[18]=z[9]*z[6];
    z[25]= - n<T>(1,2)*z[18] + z[16];
    z[23]=z[23] - z[16];
    z[17]=z[23]*z[17];
    z[17]=3*z[25] + z[17];
    z[23]= - n<T>(3,4) + z[6];
    z[23]=z[4]*z[23];
    z[20]= - n<T>(3,4)*z[20] - z[21] + z[23];
    z[20]=z[5]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[17]=z[17]*z[22];
    z[20]= - z[3] + 1;
    z[14]=z[14] + z[9];
    z[14]=z[7]*z[14]*z[20];
    z[20]=n<T>(13,4)*z[9] - n<T>(23,8) - 5*z[8];
    z[23]= - static_cast<T>(1)+ n<T>(17,8)*z[8];
    z[23]=n<T>(5,3)*z[23] - n<T>(3,8)*z[9];
    z[23]=z[3]*z[23];
    z[14]=n<T>(15,8)*z[14] + n<T>(1,3)*z[20] + z[23];
    z[12]=z[14]*z[12];
    z[14]= - n<T>(1,12)*z[3] - n<T>(1,3);
    z[14]=z[9]*z[14];
    z[14]= - n<T>(5,6)*z[8] + n<T>(3,8) + n<T>(1,3)*z[2] + z[14];
    z[14]=z[3]*z[14];
    z[12]=z[12] + z[17] + z[14] + n<T>(1,2)*z[15];
    z[12]=z[1]*z[12];
    z[14]=z[19]*z[24];
    z[15]=n<T>(1,2) - z[6];
    z[15]=z[4]*z[15];
    z[14]=z[14] + z[21] + z[15];
    z[14]=z[5]*z[14];
    z[15]= - z[2] + z[16];
    z[15]=z[4]*z[15];
    z[15]=z[15] + 3*z[18] + static_cast<T>(3)- 5*z[6];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[11]=z[14]*z[11];
    z[14]=n<T>(1,4)*z[18] - z[16];
    z[14]=z[9]*z[14];
    z[15]=n<T>(1,3)*z[4];
    z[16]=n<T>(5,4) + z[2];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(5,4)*z[8] + n<T>(5,4) + z[16];
    z[15]=z[16]*z[15];
    z[11]=z[11] + z[15] + z[14] + z[24] + n<T>(17,12) - z[2];
    z[11]=z[11]*z[22];
    z[14]=n<T>(5,4) - z[2];
    z[15]=z[18] - z[6];
    z[15]=n<T>(1,3) - n<T>(1,16)*z[15];
    z[15]=z[9]*z[15];
    z[13]= - z[13] - z[9];
    z[13]=z[3]*z[13];

    r += z[10] + z[11] + z[12] + n<T>(1,4)*z[13] + n<T>(1,6)*z[14] + z[15];
 
    return r;
}

template double qqb_2lha_r1373(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1373(const std::array<dd_real,30>&);
#endif
