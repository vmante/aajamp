#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1705(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[3];
    z[5]=k[4];
    z[6]= - n<T>(75305,2) - n<T>(74729,5)*z[3];
    z[6]=z[3]*z[6];
    z[7]=npow(z[3],2);
    z[7]=n<T>(74729,720)*z[7];
    z[8]= - static_cast<T>(2)- z[7];
    z[8]=z[4]*z[8];
    z[9]=z[1]*z[7];
    z[6]=z[9] + z[8] - static_cast<T>(1)+ n<T>(1,144)*z[6];
    z[6]=z[1]*z[6];
    z[8]= - z[4] + 1;
    z[9]=static_cast<T>(4)+ n<T>(74729,360)*z[3];
    z[8]=z[9]*z[8];
    z[9]=n<T>(74729,720)*z[3];
    z[10]= - static_cast<T>(2)- z[9];
    z[10]=z[1]*z[3]*z[10];
    z[8]=z[10] + z[8];
    z[8]=z[1]*z[8];
    z[10]=n<T>(74729,720) + 2*z[5];
    z[11]= - z[4]*z[10];
    z[11]=z[11] + n<T>(74729,360) + 4*z[5];
    z[11]=z[4]*z[11];
    z[8]=z[8] + z[11] - z[10];
    z[8]=z[2]*z[8];
    z[9]=z[9] + z[5];
    z[10]=2*z[4] - n<T>(76649,480) - z[9];
    z[10]=z[4]*z[10];
    z[6]=z[8] + z[6] + z[10] + n<T>(75689,480) + z[9];
    z[6]=z[2]*z[6];
    z[8]=z[1] - z[4];
    z[7]=z[7] - 1;
    z[7]=z[7]*z[8];
    z[8]= - n<T>(76169,3) - 74729*z[3];
    z[6]=z[6] + n<T>(1,480)*z[8] + z[7];

    r += z[6]*z[2];
 
    return r;
}

template double qqb_2lha_r1705(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1705(const std::array<dd_real,30>&);
#endif
