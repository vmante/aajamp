#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r93(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[2];
    z[4]=k[3];
    z[5]=k[5];
    z[6]= - z[4] + n<T>(1,4)*z[1];
    z[7]=npow(z[2],2);
    z[6]=z[6]*z[7];
    z[8]=n<T>(1,2)*z[2];
    z[9]=n<T>(1,4)*z[2];
    z[10]=static_cast<T>(1)- z[9];
    z[10]=z[10]*z[8];
    z[10]=z[10] - z[6];
    z[10]=z[1]*z[10];
    z[11]= - static_cast<T>(13)+ 3*z[2];
    z[11]=z[11]*z[9];
    z[7]=z[7]*z[4];
    z[11]=z[11] - 3*z[7];
    z[11]=z[4]*z[11];
    z[12]=z[2] - 1;
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[10]=z[1]*z[10];
    z[11]=static_cast<T>(5)- z[2];
    z[11]=z[2]*z[11];
    z[11]=n<T>(3,8)*z[11] + z[7];
    z[11]=z[4]*z[11];
    z[11]= - n<T>(5,8)*z[12] + z[11];
    z[11]=z[4]*z[11];
    z[10]=z[10] - n<T>(1,8) + z[11];
    z[10]=z[1]*z[10];
    z[11]= - z[8] - z[7];
    z[11]=z[4]*z[11];
    z[6]=z[9] - z[6];
    z[6]=z[1]*z[6];
    z[6]=n<T>(3,2)*z[11] + z[6];
    z[6]=z[1]*z[6];
    z[9]=npow(z[4],2);
    z[11]=n<T>(3,4)*z[2] + z[7];
    z[11]=z[11]*z[9];
    z[6]=z[11] + z[6];
    z[6]=z[3]*z[6];
    z[11]= - static_cast<T>(7)+ z[2];
    z[8]=z[11]*z[8];
    z[8]=z[8] - z[7];
    z[11]=n<T>(1,2)*z[4];
    z[8]=z[8]*z[11];
    z[8]=z[8] + z[12];
    z[8]=z[4]*z[8];
    z[8]=n<T>(1,2) + z[8];
    z[8]=z[8]*z[11];
    z[11]=z[4]*z[2];
    z[11]=z[11] - z[12];
    z[11]=z[4]*z[11];
    z[11]= - static_cast<T>(1)+ z[11];
    z[9]=z[11]*z[9];
    z[7]= - z[2] - z[7];
    z[7]=z[3]*z[7]*npow(z[4],3);
    z[7]=z[9] + z[7];
    z[7]=z[5]*z[7];

    r += n<T>(1,2)*z[6] + n<T>(1,8)*z[7] + z[8] + z[10];
 
    return r;
}

template double qqb_2lha_r93(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r93(const std::array<dd_real,30>&);
#endif
