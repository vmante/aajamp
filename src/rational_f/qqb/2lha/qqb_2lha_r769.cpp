#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r769(const std::array<T,30>& k) {
  T z[45];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=k[5];
    z[9]=k[3];
    z[10]=k[12];
    z[11]=k[13];
    z[12]=k[15];
    z[13]=k[28];
    z[14]=z[13] - n<T>(5,2);
    z[14]=z[14]*z[13];
    z[15]= - n<T>(1,2) + z[13];
    z[15]=z[11]*z[15];
    z[15]= - z[14] + z[15];
    z[16]=n<T>(1,4)*z[11];
    z[15]=z[15]*z[16];
    z[17]=z[11]*z[13];
    z[18]=npow(z[13],2);
    z[17]=z[17] - z[18];
    z[17]=z[17]*z[11];
    z[19]=z[10]*z[18];
    z[19]=z[19] + z[17];
    z[20]=n<T>(1,8)*z[5];
    z[19]=z[19]*z[20];
    z[21]=npow(z[12],2);
    z[14]=z[10]*z[14];
    z[14]= - n<T>(1,2) + z[14];
    z[22]=n<T>(1,4)*z[9];
    z[23]=z[10]*z[22];
    z[14]=z[19] + z[15] + z[23] + n<T>(1,4)*z[14] + z[21];
    z[15]=n<T>(1,2)*z[5];
    z[14]=z[14]*z[15];
    z[19]=n<T>(1,2)*z[13];
    z[23]=z[19] - 5;
    z[24]=z[23]*z[19];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[10]*z[24];
    z[24]= - n<T>(3,4) + z[24];
    z[25]=n<T>(1,2)*z[9];
    z[26]= - z[21] - n<T>(1,4) + z[10];
    z[26]=z[26]*z[25];
    z[19]= - z[25] + static_cast<T>(1)+ z[19];
    z[19]=z[11]*z[19];
    z[23]= - z[13]*z[23];
    z[19]=z[23] + z[19];
    z[23]=n<T>(1,8)*z[11];
    z[19]=z[19]*z[23];
    z[27]=n<T>(1,4)*z[2];
    z[28]= - n<T>(1,4) + 2*z[12];
    z[28]=z[12]*z[28];
    z[14]=z[14] - z[27] + z[19] + z[26] + n<T>(1,4)*z[24] + z[28];
    z[14]=z[5]*z[14];
    z[19]= - n<T>(1,2) + z[10];
    z[24]= - n<T>(1,4) - z[12];
    z[24]=z[12]*z[24];
    z[19]=n<T>(3,4)*z[19] + z[24];
    z[19]=z[9]*z[19];
    z[24]= - z[22] + static_cast<T>(1)+ n<T>(5,4)*z[13];
    z[24]=z[24]*z[16];
    z[26]=n<T>(1,8)*z[9];
    z[28]=n<T>(1,8)*z[2];
    z[29]=z[28] - static_cast<T>(1)+ z[26];
    z[29]=z[2]*z[29];
    z[30]=static_cast<T>(1)- n<T>(5,8)*z[13];
    z[30]=z[10]*z[30];
    z[30]= - n<T>(15,4) + z[30];
    z[31]=static_cast<T>(1)+ n<T>(3,2)*z[12];
    z[31]=z[12]*z[31];
    z[14]=z[14] + z[29] + z[24] + z[19] + n<T>(1,2)*z[30] + z[31];
    z[14]=z[5]*z[14];
    z[19]=5*z[12];
    z[24]=z[19] - n<T>(61,4) + z[10];
    z[29]= - static_cast<T>(1)- z[12];
    z[29]=z[12]*z[29];
    z[29]=z[29] - n<T>(3,4) + z[10];
    z[29]=z[9]*z[29];
    z[30]=z[9] - 1;
    z[31]=n<T>(1,2)*z[30] + z[2];
    z[31]=z[2]*z[31];
    z[24]=z[31] + n<T>(1,2)*z[24] + z[29];
    z[14]=n<T>(1,2)*z[24] + z[14];
    z[14]=z[5]*z[14];
    z[24]=z[10] - 1;
    z[24]= - z[12] + n<T>(1,2)*z[24];
    z[24]=z[24]*z[9];
    z[29]= - n<T>(25,2) + z[24];
    z[22]=static_cast<T>(3)+ z[22];
    z[22]=n<T>(3,8)*z[2] + n<T>(1,2)*z[22] + z[4];
    z[22]=z[2]*z[22];
    z[14]=z[14] + z[22] + n<T>(1,4)*z[29] - z[4];
    z[14]=z[7]*z[14];
    z[22]=static_cast<T>(1)- 3*z[10];
    z[22]=n<T>(1,4)*z[22] + z[21];
    z[22]=z[9]*z[22];
    z[29]= - static_cast<T>(1)- 3*z[13];
    z[31]=z[11]*z[30];
    z[29]=n<T>(3,2)*z[29] + z[31];
    z[29]=z[29]*z[16];
    z[31]=n<T>(3,4)*z[2];
    z[32]=static_cast<T>(1)+ n<T>(9,2)*z[13];
    z[32]=z[10]*z[32];
    z[22]=z[31] + z[29] + z[22] + n<T>(1,4)*z[32] - 5*z[21];
    z[29]=z[13] - n<T>(9,2);
    z[29]=z[29]*z[13];
    z[32]=static_cast<T>(3)- z[29];
    z[26]= - z[26] + n<T>(1,8)*z[32];
    z[26]=z[10]*z[26];
    z[32]=static_cast<T>(1)- z[13];
    z[32]=z[11]*z[32];
    z[29]=z[29] + z[32];
    z[29]=z[29]*z[23];
    z[32]=static_cast<T>(1)- z[18];
    z[32]=z[10]*z[32];
    z[32]=z[32] - z[17];
    z[32]=z[32]*z[20];
    z[26]=z[32] + z[29] - z[21] + z[26];
    z[26]=z[5]*z[26];
    z[22]=n<T>(1,2)*z[22] + z[26];
    z[22]=z[5]*z[22];
    z[26]=static_cast<T>(1)- n<T>(3,2)*z[10];
    z[29]=n<T>(1,2) + z[12];
    z[29]=z[12]*z[29];
    z[26]=n<T>(1,2)*z[26] + z[29];
    z[26]=z[9]*z[26];
    z[23]=z[30]*z[23];
    z[29]= - z[2] + static_cast<T>(9)- z[9];
    z[29]=z[29]*z[27];
    z[30]=n<T>(25,2) - z[10];
    z[30]=n<T>(1,2)*z[30] - 9*z[12];
    z[23]=z[29] + z[23] + n<T>(1,2)*z[30] + z[26];
    z[22]=n<T>(1,2)*z[23] + z[22];
    z[22]=z[5]*z[22];
    z[23]=n<T>(1,2)*z[2];
    z[25]= - static_cast<T>(1)- z[25];
    z[25]= - z[23] + n<T>(1,4)*z[25] - z[4];
    z[25]=z[2]*z[25];
    z[24]=static_cast<T>(7)- z[24];
    z[14]=z[14] + z[22] + z[25] + n<T>(1,4)*z[24] + z[4];
    z[14]=z[7]*z[14];
    z[22]=z[2] - n<T>(3,2);
    z[24]=5*z[2];
    z[25]=z[22]*z[24];
    z[25]=z[25] + 3;
    z[26]=n<T>(3,2)*z[5] + n<T>(9,2) - z[24];
    z[26]=z[5]*z[26];
    z[26]=z[26] + z[25];
    z[26]=z[5]*z[26];
    z[25]=z[26] - z[25];
    z[25]=z[5]*z[25];
    z[26]=3*z[4];
    z[29]= - static_cast<T>(1)- z[26];
    z[30]=z[26] + z[6];
    z[32]=z[2]*z[30];
    z[29]=5*z[29] + z[32];
    z[29]=z[29]*z[23];
    z[32]=static_cast<T>(1)+ z[4];
    z[29]=15*z[32] + z[29];
    z[29]=z[2]*z[29];
    z[32]= - static_cast<T>(5)- z[26];
    z[29]=5*z[32] + z[29];
    z[29]=z[2]*z[29];
    z[32]=static_cast<T>(7)+ z[26];
    z[29]=n<T>(5,2)*z[32] + z[29];
    z[29]=z[2]*z[29];
    z[32]= - static_cast<T>(3)- z[4];
    z[25]=z[25] + n<T>(3,2)*z[32] + z[29];
    z[25]=z[7]*z[25];
    z[29]= - z[30]*z[31];
    z[30]=n<T>(45,4)*z[4];
    z[29]=z[29] + static_cast<T>(1)+ z[30];
    z[29]=z[2]*z[29];
    z[32]= - static_cast<T>(1)- n<T>(5,2)*z[4];
    z[29]=9*z[32] + z[29];
    z[29]=z[2]*z[29];
    z[32]=n<T>(45,2)*z[4];
    z[29]=z[29] + static_cast<T>(16)+ z[32];
    z[29]=z[2]*z[29];
    z[33]= - static_cast<T>(23)- z[32];
    z[29]=n<T>(1,2)*z[33] + z[29];
    z[29]=z[2]*z[29];
    z[33]=7*z[2];
    z[34]= - n<T>(15,2) + z[33];
    z[34]=z[34]*z[15];
    z[35]=static_cast<T>(2)- z[2];
    z[35]=z[2]*z[35];
    z[35]= - static_cast<T>(1)+ z[35];
    z[34]=9*z[35] + z[34];
    z[34]=z[5]*z[34];
    z[35]= - n<T>(31,2) + 6*z[2];
    z[35]=z[2]*z[35];
    z[35]=n<T>(57,4) + z[35];
    z[35]=z[2]*z[35];
    z[34]=z[34] - n<T>(9,2) + z[35];
    z[34]=z[5]*z[34];
    z[35]=static_cast<T>(1)+ n<T>(3,4)*z[4];
    z[25]=n<T>(1,2)*z[25] + z[34] + 3*z[35] + z[29];
    z[25]=z[7]*z[25];
    z[29]=3*z[1];
    z[34]=n<T>(1,2)*z[1];
    z[35]=z[34] - 1;
    z[36]=z[35]*z[29];
    z[37]= - z[35]*z[26];
    z[37]=z[37] - n<T>(1,2) + z[6];
    z[37]=z[2]*z[37];
    z[38]=z[35]*z[4];
    z[39]=15*z[38];
    z[36]=z[37] + z[39] + n<T>(1,2) + z[36];
    z[36]=z[36]*z[23];
    z[37]=5*z[4];
    z[40]= - z[35]*z[37];
    z[41]=z[1] - 2;
    z[42]= - z[1]*z[41];
    z[40]=z[40] + n<T>(1,2) + z[42];
    z[36]=3*z[40] + z[36];
    z[36]=z[2]*z[36];
    z[35]=z[35]*z[1];
    z[36]=z[36] + z[39] - n<T>(7,2) + 9*z[35];
    z[36]=z[2]*z[36];
    z[39]= - z[41]*z[29];
    z[36]=z[36] - n<T>(15,2)*z[38] + n<T>(11,4) + z[39];
    z[36]=z[2]*z[36];
    z[39]=3*z[41];
    z[40]=z[6]*npow(z[2],2);
    z[40]=z[39] - n<T>(1,4)*z[40];
    z[40]=z[2]*z[40];
    z[42]=9*z[41];
    z[40]= - z[42] + z[40];
    z[40]=z[2]*z[40];
    z[40]=z[42] + z[40];
    z[40]=z[2]*z[40];
    z[22]= - z[15]*z[8]*z[22];
    z[42]=z[23] - 1;
    z[43]=z[2]*z[42];
    z[43]=n<T>(1,2) + z[43];
    z[22]=9*z[43] + z[22];
    z[22]=z[5]*z[22];
    z[22]=z[22] - z[39] + z[40];
    z[22]=z[5]*z[22];
    z[35]=z[38] - n<T>(1,2) + z[35];
    z[22]=z[25] + z[22] + n<T>(3,2)*z[35] + z[36];
    z[22]=z[3]*z[22];
    z[25]=n<T>(1,8)*z[1];
    z[35]=static_cast<T>(2)+ z[25];
    z[35]=z[35]*z[29];
    z[36]=n<T>(1,2)*z[4];
    z[38]=13*z[1];
    z[39]= - static_cast<T>(23)+ z[38];
    z[39]=z[39]*z[36];
    z[40]=n<T>(13,8)*z[1];
    z[43]=z[31] + static_cast<T>(1)- z[40];
    z[43]=z[4]*z[43];
    z[44]=n<T>(7,4)*z[1];
    z[43]= - z[44] + n<T>(3,4) + z[6] + z[43];
    z[43]=z[2]*z[43];
    z[35]=z[43] + z[39] - static_cast<T>(10)+ z[35];
    z[35]=z[2]*z[35];
    z[39]= - static_cast<T>(5)- n<T>(3,4)*z[1];
    z[39]=z[1]*z[39];
    z[39]=static_cast<T>(17)+ z[39];
    z[43]=static_cast<T>(7)- n<T>(13,4)*z[1];
    z[43]=z[4]*z[43];
    z[39]=n<T>(1,2)*z[39] + z[43];
    z[35]=3*z[39] + z[35];
    z[35]=z[2]*z[35];
    z[38]= - n<T>(61,2) + z[38];
    z[36]=z[38]*z[36];
    z[38]=static_cast<T>(4)+ n<T>(9,8)*z[1];
    z[38]=z[1]*z[38];
    z[35]=z[35] + z[36] - static_cast<T>(24)+ z[38];
    z[35]=z[2]*z[35];
    z[36]=n<T>(25,2)*z[4];
    z[38]=z[36] + static_cast<T>(5)+ z[6];
    z[38]=z[38]*z[23];
    z[39]= - n<T>(11,2) - z[37];
    z[38]=5*z[39] + z[38];
    z[38]=z[2]*z[38];
    z[39]=25*z[4];
    z[43]=static_cast<T>(47)+ z[39];
    z[38]=n<T>(3,2)*z[43] + z[38];
    z[38]=z[2]*z[38];
    z[38]=z[38] - n<T>(133,2) - z[39];
    z[38]=z[2]*z[38];
    z[36]=static_cast<T>(43)+ z[36];
    z[36]=n<T>(1,2)*z[36] + z[38];
    z[38]=static_cast<T>(1)+ n<T>(23,2)*z[2];
    z[39]= - n<T>(11,8) + z[12];
    z[39]=z[5]*z[39];
    z[38]=n<T>(1,4)*z[38] + z[39];
    z[38]=z[5]*z[38];
    z[39]= - static_cast<T>(11)- z[31];
    z[39]=z[2]*z[39];
    z[39]=n<T>(49,4) + z[39];
    z[38]=n<T>(1,2)*z[39] + z[38];
    z[38]=z[5]*z[38];
    z[39]=static_cast<T>(137)- 19*z[2];
    z[39]=z[2]*z[39];
    z[39]= - static_cast<T>(225)+ z[39];
    z[39]=z[2]*z[39];
    z[39]=static_cast<T>(105)+ z[39];
    z[38]=n<T>(1,8)*z[39] + z[38];
    z[38]=z[5]*z[38];
    z[36]=n<T>(1,2)*z[36] + z[38];
    z[36]=z[7]*z[36];
    z[38]= - static_cast<T>(1)- z[6];
    z[38]=5*z[38] - n<T>(63,2)*z[4];
    z[38]=z[38]*z[23];
    z[39]=63*z[4];
    z[38]=z[38] + n<T>(71,2) + z[39];
    z[38]=z[2]*z[38];
    z[43]= - static_cast<T>(23)- 21*z[4];
    z[38]=n<T>(9,2)*z[43] + z[38];
    z[38]=z[2]*z[38];
    z[38]=z[38] + n<T>(205,2) + z[39];
    z[38]=z[38]*z[23];
    z[39]=static_cast<T>(5)- n<T>(13,4)*z[2];
    z[15]=z[39]*z[15];
    z[39]= - n<T>(25,4) + 2*z[2];
    z[39]=z[2]*z[39];
    z[15]=z[15] + n<T>(15,4) + z[39];
    z[15]=z[5]*z[15];
    z[33]= - static_cast<T>(59)+ z[33];
    z[33]=z[2]*z[33];
    z[33]=static_cast<T>(121)+ z[33];
    z[33]=z[2]*z[33];
    z[33]= - static_cast<T>(63)+ z[33];
    z[15]=n<T>(1,8)*z[33] + z[15];
    z[15]=z[5]*z[15];
    z[15]=z[36] + z[15] + z[38] - static_cast<T>(17)- n<T>(63,8)*z[4];
    z[15]=z[7]*z[15];
    z[33]=z[40] - 4;
    z[31]= - z[6]*z[31];
    z[31]=z[31] + z[33];
    z[31]=z[2]*z[31];
    z[31]=z[31] + static_cast<T>(8)- z[44];
    z[31]=z[2]*z[31];
    z[31]=z[31] - static_cast<T>(4)- n<T>(11,8)*z[1];
    z[31]=z[2]*z[31];
    z[36]=n<T>(3,2) + z[8];
    z[38]=n<T>(15,4) - z[8];
    z[38]=z[2]*z[38];
    z[36]=n<T>(1,2)*z[36] + z[38];
    z[36]=z[2]*z[36];
    z[38]= - static_cast<T>(3)+ z[8];
    z[36]=n<T>(3,2)*z[38] + z[36];
    z[38]=z[5]*z[8]*z[42];
    z[36]=n<T>(1,2)*z[36] + z[38];
    z[36]=z[5]*z[36];
    z[31]=z[36] + n<T>(3,2)*z[1] + z[31];
    z[31]=z[5]*z[31];
    z[36]= - static_cast<T>(1)- z[34];
    z[36]=z[36]*z[29];
    z[36]=static_cast<T>(31)+ z[36];
    z[33]= - z[4]*z[33];
    z[15]=z[22] + z[15] + z[31] + z[35] + n<T>(1,4)*z[36] + z[33];
    z[15]=z[3]*z[15];
    z[22]=static_cast<T>(9)+ 13*z[4];
    z[22]=z[22]*z[28];
    z[31]= - n<T>(35,4) + z[6];
    z[33]= - n<T>(5,4) - z[1];
    z[33]=z[4]*z[33];
    z[22]=z[22] + z[33] + n<T>(1,2)*z[31] - z[1];
    z[22]=z[2]*z[22];
    z[31]=n<T>(1,4)*z[1];
    z[33]=static_cast<T>(31)- z[34];
    z[33]=z[33]*z[31];
    z[33]= - static_cast<T>(9)+ z[33];
    z[26]=z[41]*z[26];
    z[22]=z[22] + n<T>(1,2)*z[33] + z[26];
    z[22]=z[2]*z[22];
    z[26]= - static_cast<T>(19)+ z[34];
    z[26]=z[1]*z[26];
    z[26]=n<T>(141,2) + z[26];
    z[29]=n<T>(37,4) - z[29];
    z[29]=z[4]*z[29];
    z[22]=z[22] + n<T>(1,4)*z[26] + z[29];
    z[22]=z[2]*z[22];
    z[26]=z[28] + n<T>(17,4) + z[37];
    z[26]=z[2]*z[26];
    z[28]= - n<T>(73,8) - z[37];
    z[26]=3*z[28] + z[26];
    z[26]=z[2]*z[26];
    z[26]=z[26] + n<T>(83,2) + 15*z[4];
    z[26]=z[2]*z[26];
    z[19]= - n<T>(3,2)*z[2] + z[11] - n<T>(41,2) - z[19];
    z[28]= - static_cast<T>(2)+ n<T>(1,2)*z[12];
    z[28]=z[12]*z[28];
    z[28]=n<T>(9,16) + z[28];
    z[28]=z[5]*z[28];
    z[19]=n<T>(1,4)*z[19] + z[28];
    z[19]=z[5]*z[19];
    z[28]=static_cast<T>(7)- z[23];
    z[28]=z[2]*z[28];
    z[28]= - n<T>(287,2) + 13*z[28];
    z[19]=n<T>(1,8)*z[28] + z[19];
    z[19]=z[5]*z[19];
    z[28]= - static_cast<T>(25)+ z[2];
    z[28]=z[2]*z[28];
    z[28]=n<T>(153,2) + z[28];
    z[28]=z[2]*z[28];
    z[28]= - n<T>(105,2) + z[28];
    z[19]=n<T>(1,2)*z[28] + z[19];
    z[19]=z[5]*z[19];
    z[19]=z[19] + z[26] - n<T>(37,2) - z[37];
    z[19]=z[7]*z[19];
    z[26]=41*z[4];
    z[28]=n<T>(87,2) + z[26];
    z[29]= - z[23] - n<T>(41,2)*z[4] - n<T>(21,4) - z[6];
    z[29]=z[2]*z[29];
    z[28]=n<T>(3,2)*z[28] + z[29];
    z[28]=z[2]*z[28];
    z[29]= - n<T>(463,2) - 123*z[4];
    z[28]=n<T>(1,2)*z[29] + z[28];
    z[28]=z[2]*z[28];
    z[26]=n<T>(221,2) + z[26];
    z[26]=n<T>(1,2)*z[26] + z[28];
    z[28]=z[23] + 1;
    z[29]= - z[2]*z[28];
    z[29]=static_cast<T>(23)+ z[29];
    z[33]=3*z[12];
    z[35]= - n<T>(5,8) + z[33];
    z[35]=z[5]*z[35];
    z[29]=n<T>(1,2)*z[29] + z[35];
    z[29]=z[5]*z[29];
    z[35]=n<T>(15,2) + z[2];
    z[35]=z[2]*z[35];
    z[35]= - n<T>(121,2) + z[35];
    z[35]=z[2]*z[35];
    z[35]=static_cast<T>(53)+ z[35];
    z[29]=n<T>(1,2)*z[35] + z[29];
    z[29]=z[5]*z[29];
    z[19]=z[19] + n<T>(1,2)*z[26] + z[29];
    z[19]=z[7]*z[19];
    z[26]= - n<T>(17,2) - 3*z[6];
    z[26]=z[26]*z[23];
    z[26]=z[26] + z[44] + n<T>(21,4) - z[8];
    z[26]=z[26]*z[23];
    z[26]=z[26] - z[44] + static_cast<T>(7)+ n<T>(3,4)*z[8];
    z[26]=z[2]*z[26];
    z[29]=n<T>(15,4)*z[2] - n<T>(3,2) + z[8];
    z[29]=z[29]*z[27];
    z[35]=n<T>(1,4)*z[5] - 1;
    z[35]=z[8]*z[35];
    z[29]=z[29] - n<T>(5,2) + z[35];
    z[29]=z[5]*z[29];
    z[35]= - static_cast<T>(15)+ z[44];
    z[26]=z[29] + n<T>(1,2)*z[35] + z[26];
    z[26]=z[5]*z[26];
    z[29]=z[34] - 15;
    z[34]= - z[1]*z[29];
    z[34]= - static_cast<T>(79)+ z[34];
    z[35]= - n<T>(29,8) + z[1];
    z[35]=z[4]*z[35];
    z[15]=z[15] + z[19] + z[26] + z[22] + n<T>(1,8)*z[34] + z[35];
    z[15]=z[3]*z[15];
    z[19]=z[12] - 1;
    z[22]=z[19]*z[12];
    z[26]=n<T>(13,8) - z[22];
    z[34]= - static_cast<T>(1)- z[16];
    z[34]=z[11]*z[34];
    z[26]=z[23] + 3*z[26] + z[34];
    z[22]=z[22]*z[5];
    z[26]=n<T>(1,2)*z[26] - z[22];
    z[26]=z[5]*z[26];
    z[33]=n<T>(181,4) - z[33];
    z[34]= - n<T>(37,8) - z[2];
    z[34]=z[2]*z[34];
    z[33]=z[34] + n<T>(1,2)*z[33] - z[11];
    z[26]=n<T>(1,2)*z[33] + z[26];
    z[26]=z[5]*z[26];
    z[28]=z[28]*z[23];
    z[28]= - static_cast<T>(14)+ z[28];
    z[28]=z[2]*z[28];
    z[26]=z[26] + n<T>(71,4) + z[28];
    z[26]=z[5]*z[26];
    z[28]=29*z[4];
    z[33]=3*z[2] + static_cast<T>(37)+ z[28];
    z[33]=z[33]*z[23];
    z[33]=z[33] - n<T>(147,2) - z[28];
    z[33]=z[2]*z[33];
    z[28]=static_cast<T>(107)+ z[28];
    z[28]=n<T>(1,2)*z[28] + z[33];
    z[26]=n<T>(1,4)*z[28] + z[26];
    z[26]=z[7]*z[26];
    z[28]= - n<T>(5,2)*z[2] - static_cast<T>(7)- z[32];
    z[28]=z[2]*z[28];
    z[28]=z[28] + n<T>(143,2) + 45*z[4];
    z[23]=z[28]*z[23];
    z[23]=z[23] - static_cast<T>(31)- z[30];
    z[28]= - n<T>(9,8)*z[2] + n<T>(7,4)*z[11] - n<T>(79,8) + z[12];
    z[22]=n<T>(1,2)*z[28] + 2*z[22];
    z[22]=z[5]*z[22];
    z[24]=static_cast<T>(19)+ z[24];
    z[24]=z[2]*z[24];
    z[24]= - static_cast<T>(53)+ z[24];
    z[22]=n<T>(1,4)*z[24] + z[22];
    z[22]=z[5]*z[22];
    z[22]=z[26] + n<T>(1,2)*z[23] + z[22];
    z[22]=z[7]*z[22];
    z[23]=n<T>(7,8) + z[4];
    z[23]=z[2]*z[23];
    z[23]=z[23] - z[4] - static_cast<T>(4)+ z[25];
    z[23]=z[2]*z[23];
    z[24]=static_cast<T>(7)- n<T>(5,2)*z[1];
    z[23]=z[23] + n<T>(1,8)*z[24] - z[4];
    z[23]=z[2]*z[23];
    z[24]= - n<T>(13,2) - z[6];
    z[24]=z[2]*z[24];
    z[24]=z[24] - z[8] - z[29];
    z[24]=z[2]*z[24];
    z[24]=n<T>(9,4) + z[24];
    z[19]=n<T>(5,8)*z[2] + n<T>(1,4)*z[8] + z[19];
    z[19]=z[5]*z[19];
    z[19]=n<T>(1,4)*z[24] + z[19];
    z[19]=z[5]*z[19];
    z[24]=static_cast<T>(3)+ z[31];
    z[15]=z[15] + z[22] + z[19] + z[23] + n<T>(3,4)*z[24] + z[4];
    z[15]=z[3]*z[15];
    z[19]=z[13] - z[16];
    z[16]=z[19]*z[16];
    z[18]= - static_cast<T>(1)+ n<T>(1,2)*z[18];
    z[18]=z[10]*z[18];
    z[17]=z[18] + n<T>(1,2)*z[17];
    z[17]=z[17]*z[20];
    z[18]= - static_cast<T>(1)- z[13];
    z[18]=z[10]*z[18];
    z[18]=n<T>(1,4) + z[18];
    z[16]=z[17] + z[16] + n<T>(1,4)*z[18] + z[21];
    z[16]=z[5]*z[16];
    z[17]= - n<T>(1,2) - z[10];
    z[18]= - z[11]*z[9];
    z[18]=static_cast<T>(3)+ z[18];
    z[18]=z[11]*z[18];
    z[16]=z[16] - z[27] + n<T>(1,16)*z[18] + n<T>(1,8)*z[17] + z[12];
    z[16]=z[5]*z[16];
    z[17]= - static_cast<T>(1)+ z[2];
    z[17]=z[17]*z[27];
    z[17]=static_cast<T>(1)+ z[17];

    r += z[14] + z[15] + z[16] + n<T>(1,4)*z[17];
 
    return r;
}

template double qqb_2lha_r769(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r769(const std::array<dd_real,30>&);
#endif
