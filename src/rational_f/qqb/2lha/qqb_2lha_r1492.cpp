#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1492(const std::array<T,30>& k) {
  T z[57];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[17];
    z[6]=k[8];
    z[7]=k[3];
    z[8]=k[4];
    z[9]=k[2];
    z[10]=k[11];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=3*z[3];
    z[14]=z[13] - 1;
    z[15]=n<T>(1,4)*z[3];
    z[16]= - z[14]*z[15];
    z[17]=n<T>(3,4)*z[3];
    z[18]=z[17] - 1;
    z[19]=z[2]*z[3];
    z[20]=z[18]*z[19];
    z[21]=npow(z[3],2);
    z[22]=n<T>(1,2)*z[21];
    z[23]=z[22]*z[2];
    z[23]=z[23] - z[21];
    z[23]=z[23]*z[2];
    z[23]=z[23] + z[22];
    z[24]=n<T>(1,2)*z[1];
    z[25]= - z[23]*z[24];
    z[16]=z[25] + z[16] + z[20];
    z[16]=z[1]*z[16];
    z[20]=n<T>(1,2)*z[3];
    z[25]=z[20] - 1;
    z[26]=z[25]*z[3];
    z[27]=z[26] + n<T>(1,2);
    z[28]=n<T>(1,2)*z[27];
    z[16]= - z[28] + z[16];
    z[16]=z[1]*z[16];
    z[16]=z[28] + z[16];
    z[16]=z[1]*z[16];
    z[29]=z[24]*z[21];
    z[30]=z[3] - n<T>(1,2);
    z[31]=z[30]*z[3];
    z[32]=z[29] + z[31];
    z[32]=z[32]*z[1];
    z[33]=z[3] - 1;
    z[34]=z[33]*z[20];
    z[32]=z[32] + z[34];
    z[34]=z[8]*z[32]*z[24];
    z[16]=z[16] + z[34];
    z[16]=z[5]*z[16];
    z[23]=z[23]*z[1];
    z[34]=z[3] - n<T>(3,2);
    z[35]=z[34]*z[19];
    z[36]= - static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[36]=z[36]*z[3];
    z[35]= - z[23] - z[36] + z[35];
    z[35]=z[2]*z[35];
    z[22]=z[22] + z[35];
    z[22]=z[1]*z[22];
    z[35]=static_cast<T>(5)- z[13];
    z[35]=z[35]*z[20];
    z[35]= - static_cast<T>(1)+ z[35];
    z[35]=z[2]*z[35];
    z[35]=z[36] + z[35];
    z[22]=n<T>(1,2)*z[35] + z[22];
    z[35]=npow(z[1],2);
    z[22]=z[22]*z[35];
    z[36]=n<T>(1,2)*z[8];
    z[32]= - z[32]*z[36];
    z[16]=z[16] + z[32] - z[28] + z[22];
    z[16]=z[5]*z[16];
    z[22]=npow(z[7],3);
    z[32]=n<T>(9,2) - z[3];
    z[32]=z[3]*z[32];
    z[32]= - n<T>(3,4)*z[7] - n<T>(7,2) + z[32];
    z[32]=z[32]*z[22];
    z[37]=z[3] - 3;
    z[38]=z[37]*z[20];
    z[39]=z[38] + 1;
    z[40]=z[2]*npow(z[7],4);
    z[40]=n<T>(3,2)*z[40];
    z[41]= - z[39]*z[40];
    z[32]=z[32] + z[41];
    z[32]=z[2]*z[32];
    z[41]=static_cast<T>(11)- z[3];
    z[41]=z[41]*z[20];
    z[41]= - 5*z[7] - static_cast<T>(5)+ z[41];
    z[42]=npow(z[7],2);
    z[41]=z[41]*z[42];
    z[32]=n<T>(1,2)*z[41] + z[32];
    z[32]=z[2]*z[32];
    z[41]=3*z[7];
    z[43]=n<T>(1,2)*z[33] - z[41];
    z[43]=z[7]*z[43];
    z[22]=5*z[22] + z[40];
    z[22]=z[2]*z[22];
    z[22]=3*z[42] + n<T>(1,2)*z[22];
    z[22]=z[2]*z[22];
    z[22]=n<T>(3,2)*z[7] + z[22];
    z[22]=z[8]*z[22];
    z[22]=z[22] + z[43] + z[32];
    z[22]=z[10]*z[22];
    z[32]=static_cast<T>(9)- n<T>(577,48)*z[3];
    z[32]=z[3]*z[32];
    z[32]=n<T>(19,4) + z[32];
    z[18]=z[3]*z[18];
    z[18]=n<T>(27,32) + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(1,4)*z[32] + z[18];
    z[18]=z[7]*z[18];
    z[32]= - static_cast<T>(125)+ n<T>(263,3)*z[3];
    z[32]=z[3]*z[32];
    z[32]=n<T>(131,6) + z[32];
    z[16]=z[22] + z[16] + n<T>(1,32)*z[32] + z[18];
    z[18]=n<T>(1,3)*z[3];
    z[22]=n<T>(1117,2) - 739*z[3];
    z[22]=z[22]*z[18];
    z[32]=n<T>(1,2)*z[7];
    z[40]=static_cast<T>(13)+ n<T>(179,3)*z[3];
    z[40]=z[3]*z[40];
    z[40]= - n<T>(65,6) + z[40];
    z[40]=z[40]*z[32];
    z[22]=z[40] - n<T>(7,2) + z[22];
    z[40]=n<T>(1,16)*z[7];
    z[22]=z[22]*z[40];
    z[43]= - n<T>(2315,6) + 271*z[3];
    z[43]=z[3]*z[43];
    z[44]=n<T>(599,6) - 89*z[3];
    z[44]=z[3]*z[44];
    z[44]= - n<T>(65,6) + z[44];
    z[44]=z[7]*z[44];
    z[43]=z[44] + n<T>(725,6) + z[43];
    z[40]=z[43]*z[40];
    z[43]=z[32] - 1;
    z[43]=z[43]*z[7];
    z[44]=z[27]*z[43];
    z[44]=z[28] + z[44];
    z[45]=11*z[2];
    z[44]=z[44]*z[45];
    z[46]=n<T>(13,8) - z[3];
    z[46]=z[3]*z[46];
    z[46]= - n<T>(5,8) + z[46];
    z[40]=z[44] + 11*z[46] + z[40];
    z[40]=z[2]*z[40];
    z[44]= - n<T>(1811,96) + 16*z[3];
    z[44]=z[3]*z[44];
    z[22]=z[40] + z[22] + n<T>(431,96) + z[44];
    z[22]=z[2]*z[22];
    z[40]=z[21]*z[7];
    z[44]=n<T>(43,32) + n<T>(7,3)*z[3];
    z[44]=z[3]*z[44];
    z[44]=n<T>(13,16)*z[40] - n<T>(11,32) + z[44];
    z[44]=z[7]*z[44];
    z[46]=n<T>(16,3) - n<T>(557,64)*z[3];
    z[46]=z[3]*z[46];
    z[22]=z[22] + z[44] + n<T>(1,8) + z[46];
    z[22]=z[2]*z[22];
    z[44]=static_cast<T>(87)- 109*z[3];
    z[44]=z[44]*z[15];
    z[46]=z[7]*z[3];
    z[47]= - n<T>(241,24) + 19*z[3];
    z[47]=z[47]*z[46];
    z[44]=z[44] + z[47];
    z[47]= - z[33]*z[46];
    z[48]=z[33]*z[3];
    z[47]=z[48] + z[47];
    z[47]=z[2]*z[47];
    z[44]=n<T>(1,2)*z[44] + n<T>(11,3)*z[47];
    z[44]=z[2]*z[44];
    z[47]= - n<T>(373,12) + 63*z[3];
    z[47]=z[47]*z[20];
    z[47]=z[47] - n<T>(35,3)*z[40];
    z[44]=n<T>(1,2)*z[47] + z[44];
    z[44]=z[2]*z[44];
    z[47]= - n<T>(227,3)*z[21] - 13*z[40];
    z[44]=n<T>(1,16)*z[47] + z[44];
    z[44]=z[2]*z[44];
    z[47]= - n<T>(139,4) + z[45];
    z[47]=z[2]*z[47];
    z[47]=n<T>(241,8) + z[47];
    z[49]=npow(z[2],2);
    z[47]=z[49]*z[21]*z[47];
    z[47]=n<T>(3,2)*z[21] + n<T>(1,3)*z[47];
    z[50]=n<T>(1,4)*z[1];
    z[47]=z[47]*z[50];
    z[51]= - static_cast<T>(1)+ n<T>(9,4)*z[3];
    z[51]=z[3]*z[51];
    z[51]=z[51] - z[40];
    z[44]=z[47] + n<T>(1,2)*z[51] + z[44];
    z[44]=z[44]*z[24];
    z[47]=z[32]*z[21];
    z[51]=static_cast<T>(1)- n<T>(7,2)*z[3];
    z[51]=z[3]*z[51];
    z[51]=z[51] + z[47];
    z[51]=z[51]*z[32];
    z[52]= - static_cast<T>(1)+ n<T>(245,64)*z[3];
    z[52]=z[3]*z[52];
    z[51]=z[51] + n<T>(27,32) + z[52];
    z[22]=z[44] + n<T>(1,2)*z[51] + z[22];
    z[22]=z[1]*z[22];
    z[44]=z[18] - 1;
    z[44]=z[44]*z[20];
    z[44]=z[44] + n<T>(1,3);
    z[51]= - z[7]*z[44];
    z[51]=z[51] + z[39];
    z[51]=z[7]*z[51];
    z[51]=z[51] - z[39];
    z[51]=z[7]*z[51];
    z[44]=z[51] + z[44];
    z[44]=z[44]*z[45];
    z[51]=static_cast<T>(311)- n<T>(377,3)*z[3];
    z[51]=z[51]*z[15];
    z[51]= - static_cast<T>(39)+ z[51];
    z[52]=n<T>(1,3)*z[7];
    z[53]= - n<T>(475,32) + 8*z[3];
    z[53]=z[3]*z[53];
    z[53]=n<T>(43,32) + z[53];
    z[53]=z[53]*z[52];
    z[54]=n<T>(731,3) - n<T>(229,2)*z[3];
    z[54]=z[3]*z[54];
    z[54]= - n<T>(511,6) + z[54];
    z[53]=n<T>(1,8)*z[54] + z[53];
    z[53]=z[7]*z[53];
    z[54]= - n<T>(4279,12) + 153*z[3];
    z[54]=z[3]*z[54];
    z[54]=n<T>(1915,12) + z[54];
    z[53]=n<T>(1,8)*z[54] + z[53];
    z[53]=z[7]*z[53];
    z[44]=z[44] + n<T>(1,4)*z[51] + z[53];
    z[44]=z[2]*z[44];
    z[51]=n<T>(41,6) - 13*z[3];
    z[51]=z[51]*z[32];
    z[53]= - n<T>(333,4) + n<T>(227,3)*z[3];
    z[53]=z[3]*z[53];
    z[51]=z[51] - n<T>(85,12) + z[53];
    z[51]=z[7]*z[51];
    z[53]=static_cast<T>(1787)- 1103*z[3];
    z[53]=z[3]*z[53];
    z[53]= - n<T>(725,2) + z[53];
    z[51]=n<T>(1,6)*z[53] + z[51];
    z[51]=z[7]*z[51];
    z[53]= - n<T>(2377,2) + 625*z[3];
    z[53]=z[3]*z[53];
    z[53]=n<T>(769,2) + z[53];
    z[51]=n<T>(1,6)*z[53] + z[51];
    z[44]=n<T>(1,8)*z[51] + z[44];
    z[44]=z[2]*z[44];
    z[51]= - n<T>(1345,16) + 107*z[3];
    z[51]=z[51]*z[18];
    z[53]= - static_cast<T>(41)+ n<T>(25,2)*z[3];
    z[53]=z[3]*z[53];
    z[53]= - n<T>(13,2)*z[40] + static_cast<T>(5)+ z[53];
    z[54]=n<T>(1,4)*z[7];
    z[53]=z[53]*z[54];
    z[51]=z[53] - n<T>(7,2) + z[51];
    z[51]=z[7]*z[51];
    z[53]=n<T>(1279,8) - 119*z[3];
    z[53]=z[53]*z[18];
    z[51]=z[51] - n<T>(33,4) + z[53];
    z[44]=n<T>(1,4)*z[51] + z[44];
    z[44]=z[2]*z[44];
    z[51]= - n<T>(1,4) + z[3];
    z[51]=z[51]*z[46];
    z[53]= - n<T>(9,16) - z[31];
    z[51]=3*z[53] + z[51];
    z[51]=z[7]*z[51];
    z[53]= - static_cast<T>(797)+ 1135*z[3];
    z[53]=z[3]*z[53];
    z[53]=static_cast<T>(1)+ n<T>(1,12)*z[53];
    z[51]=n<T>(1,16)*z[53] + z[51];
    z[22]=z[22] + n<T>(1,2)*z[51] + z[44];
    z[22]=z[1]*z[22];
    z[44]=z[21]*z[1];
    z[14]= - z[3]*z[14];
    z[14]= - z[44] + z[14] + z[40];
    z[14]=z[14]*z[50];
    z[51]=z[30]*z[46];
    z[53]=static_cast<T>(1)- n<T>(181,96)*z[3];
    z[53]=z[3]*z[53];
    z[51]=z[53] + z[51];
    z[53]= - static_cast<T>(1)+ z[52];
    z[53]=z[7]*z[53];
    z[53]=static_cast<T>(1)+ z[53];
    z[53]=z[7]*z[53];
    z[53]= - n<T>(1,3) + z[53];
    z[53]=z[53]*z[45];
    z[55]=static_cast<T>(71)+ n<T>(37,4)*z[7];
    z[55]=z[7]*z[55];
    z[55]= - n<T>(679,4) + z[55];
    z[55]=z[7]*z[55];
    z[55]=n<T>(179,2) + z[55];
    z[53]=n<T>(1,12)*z[55] + z[53];
    z[53]=z[2]*z[53];
    z[55]=static_cast<T>(17)+ n<T>(29,2)*z[7];
    z[55]=z[7]*z[55];
    z[55]= - n<T>(63,2) + z[55];
    z[53]=n<T>(1,8)*z[55] + z[53];
    z[53]=z[2]*z[53];
    z[55]=n<T>(19,16)*z[7];
    z[53]=z[55] + z[53];
    z[53]=z[2]*z[53];
    z[14]=z[14] + n<T>(1,2)*z[51] + z[53];
    z[14]=z[14]*z[24];
    z[51]=z[54] - 1;
    z[53]=z[51]*z[52];
    z[53]=z[53] + n<T>(1,2);
    z[53]=z[53]*z[7];
    z[53]=z[53] - n<T>(1,3);
    z[53]=z[53]*z[7];
    z[53]=z[53] + n<T>(1,12);
    z[53]=z[53]*z[45];
    z[56]= - static_cast<T>(5)- 7*z[7];
    z[52]=z[56]*z[52];
    z[52]=static_cast<T>(19)+ z[52];
    z[52]=z[7]*z[52];
    z[52]= - n<T>(71,3) + z[52];
    z[52]=z[52]*z[32];
    z[52]=n<T>(13,3) + z[52];
    z[52]=n<T>(7,16)*z[52] - z[53];
    z[52]=z[2]*z[52];
    z[56]=static_cast<T>(1)- n<T>(33,32)*z[7];
    z[56]=z[7]*z[56];
    z[56]=n<T>(35,32) + z[56];
    z[56]=z[7]*z[56];
    z[52]=z[52] - n<T>(17,16) + z[56];
    z[52]=z[2]*z[52];
    z[55]=static_cast<T>(1)- z[55];
    z[55]=z[7]*z[55];
    z[55]=n<T>(3,16) + z[55];
    z[52]=n<T>(1,2)*z[55] + z[52];
    z[52]=z[2]*z[52];
    z[55]= - static_cast<T>(1)+ n<T>(59,48)*z[3];
    z[46]=z[55]*z[46];
    z[55]=static_cast<T>(19)- n<T>(61,3)*z[3];
    z[55]=z[3]*z[55];
    z[46]=n<T>(1,16)*z[55] + z[46];
    z[55]=z[7] - 3;
    z[56]=z[7]*z[55];
    z[56]=static_cast<T>(3)+ z[56];
    z[56]=z[7]*z[56];
    z[56]= - static_cast<T>(1)+ z[56];
    z[53]=n<T>(23,16)*z[56] + z[53];
    z[53]=z[2]*z[53];
    z[43]=n<T>(1,2) + z[43];
    z[43]=n<T>(19,16)*z[43] + z[53];
    z[43]=z[43]*z[49];
    z[43]=n<T>(1,3)*z[21] + z[43];
    z[43]=z[43]*z[36];
    z[14]=z[43] + z[14] + n<T>(1,8)*z[46] + z[52];
    z[14]=z[8]*z[14];
    z[43]=static_cast<T>(7)+ z[13];
    z[43]=z[43]*z[15];
    z[17]= - static_cast<T>(1)- z[17];
    z[17]=z[17]*z[19];
    z[17]= - z[23] + z[43] + z[17];
    z[17]=z[1]*z[17];
    z[17]= - n<T>(15,8) + z[17];
    z[17]=z[1]*z[17];
    z[19]= - static_cast<T>(1)+ z[7];
    z[17]=n<T>(5,8)*z[19] + z[17];
    z[17]=z[1]*z[17];
    z[19]=z[36]*z[21];
    z[19]=z[19] + z[48];
    z[43]= - z[29] + z[19];
    z[43]=z[8]*z[43];
    z[46]= - z[48] + z[44];
    z[46]=z[46]*z[24];
    z[43]=z[43] + z[46] + z[27];
    z[43]=z[8]*z[43];
    z[46]=5*z[3];
    z[49]=z[46] - 3*z[44];
    z[35]=z[49]*z[35];
    z[35]=z[35] + z[43];
    z[43]=n<T>(1,4)*z[8];
    z[35]=z[35]*z[43];
    z[17]=z[17] + z[35];
    z[17]=z[4]*z[17];
    z[35]= - z[2]*z[38];
    z[23]=z[23] + z[26] + z[35];
    z[23]=z[1]*z[23];
    z[35]=n<T>(7,8) - z[3];
    z[35]=z[3]*z[35];
    z[23]=z[23] + n<T>(1,2) + z[35];
    z[23]=z[2]*z[23];
    z[35]=n<T>(1,8) + z[3];
    z[35]=z[3]*z[35];
    z[23]=z[35] + z[23];
    z[23]=z[1]*z[23];
    z[23]=n<T>(3,16) + z[23];
    z[23]=z[1]*z[23];
    z[35]= - static_cast<T>(1)+ n<T>(5,4)*z[3];
    z[35]=z[35]*z[3];
    z[49]=z[21]*z[8];
    z[52]=n<T>(3,4)*z[49];
    z[29]=z[52] + z[35] - z[29];
    z[29]=z[8]*z[29];
    z[38]= - z[38] - z[44];
    z[38]=z[1]*z[38];
    z[44]=z[3]*z[34];
    z[44]=n<T>(1,2) + z[44];
    z[29]=z[29] + n<T>(1,2)*z[44] + z[38];
    z[29]=z[29]*z[43];
    z[17]=n<T>(1,2)*z[17] + z[23] + z[29];
    z[23]=n<T>(1,4)*z[4];
    z[17]=z[17]*z[23];
    z[29]=n<T>(5,2) - z[3];
    z[29]=z[29]*z[20];
    z[29]=z[29] + z[51];
    z[38]=n<T>(9,2) - z[46];
    z[38]=z[38]*z[20];
    z[38]=z[38] - z[49];
    z[38]=z[38]*z[36];
    z[43]=n<T>(7,4) - z[3];
    z[43]=z[3]*z[43];
    z[38]=z[38] - n<T>(1,2) + z[43];
    z[38]=z[8]*z[38];
    z[29]=n<T>(1,2)*z[29] + z[38];
    z[29]=z[8]*z[29];
    z[38]= - z[25]*z[13];
    z[43]= - z[33]*z[13];
    z[43]=z[43] - z[49];
    z[43]=z[43]*z[36];
    z[38]=z[43] - static_cast<T>(1)+ z[38];
    z[38]=z[8]*z[38];
    z[37]=z[37]*z[3];
    z[43]= - z[37] + z[55];
    z[38]=n<T>(1,2)*z[43] + z[38];
    z[23]=z[38]*npow(z[8],2)*z[23];
    z[23]=z[29] + z[23];
    z[23]=z[4]*z[23];
    z[29]=static_cast<T>(1)- n<T>(7,4)*z[3];
    z[29]=z[3]*z[29];
    z[29]= - z[52] + z[29] + z[47];
    z[29]=z[8]*z[29];
    z[38]= - n<T>(1,8) + z[3];
    z[38]=z[3]*z[38];
    z[38]=z[38] - n<T>(3,8)*z[40];
    z[38]=z[7]*z[38];
    z[29]=z[29] - z[35] + z[38];
    z[29]=z[8]*z[29];
    z[35]=n<T>(27,2) + z[3];
    z[35]=z[3]*z[35];
    z[35]=z[35] - n<T>(55,4)*z[40];
    z[35]=z[7]*z[35];
    z[38]= - n<T>(1,2) - z[21];
    z[35]=n<T>(1,2)*z[38] + z[35];
    z[29]=n<T>(1,2)*z[35] + z[29];
    z[35]=113*z[3];
    z[38]=z[35] - 61*z[40];
    z[38]=z[38]*z[54];
    z[43]=z[3] - z[47];
    z[43]=z[7]*z[43];
    z[43]= - n<T>(1,2) + z[43];
    z[43]=z[6]*z[43];
    z[38]=n<T>(9,2)*z[43] - static_cast<T>(13)+ z[38];
    z[43]=n<T>(1,4)*z[6];
    z[38]=z[38]*z[43];
    z[23]=z[38] + n<T>(1,2)*z[29] + z[23];
    z[23]=z[12]*z[23];
    z[29]=n<T>(287,4) - 23*z[3];
    z[13]=z[29]*z[13];
    z[29]= - n<T>(413,2) + 71*z[3];
    z[29]=z[3]*z[29];
    z[37]=n<T>(5,3) - n<T>(13,2)*z[37];
    z[37]=z[7]*z[37];
    z[29]=z[37] + n<T>(461,6) + z[29];
    z[29]=z[29]*z[32];
    z[13]=z[29] - n<T>(409,4) + z[13];
    z[13]=z[7]*z[13];
    z[29]= - n<T>(735,2) + z[35];
    z[29]=z[3]*z[29];
    z[29]=n<T>(1175,6) + z[29];
    z[13]=n<T>(1,2)*z[29] + z[13];
    z[13]=z[7]*z[13];
    z[29]=z[20] - 2;
    z[29]=z[29]*z[18];
    z[29]=z[29] + n<T>(1,2);
    z[35]=z[15] - 1;
    z[18]=z[35]*z[18];
    z[18]=z[18] + n<T>(1,4);
    z[37]=z[18]*z[32];
    z[37]=z[37] - z[29];
    z[37]=z[7]*z[37];
    z[35]=z[35]*z[3];
    z[35]=z[35] + n<T>(3,4);
    z[37]=z[37] + z[35];
    z[37]=z[7]*z[37];
    z[29]=z[37] - z[29];
    z[29]=z[7]*z[29];
    z[18]=n<T>(1,2)*z[18] + z[29];
    z[18]=z[18]*z[45];
    z[29]=static_cast<T>(7)- n<T>(67,32)*z[3];
    z[29]=z[3]*z[29];
    z[13]=z[18] + n<T>(1,8)*z[13] - n<T>(383,96) + z[29];
    z[13]=z[2]*z[13];
    z[18]= - static_cast<T>(65)+ n<T>(49,2)*z[3];
    z[18]=z[18]*z[46];
    z[18]=n<T>(763,6) + z[18];
    z[26]= - n<T>(29,6) - 13*z[26];
    z[26]=z[26]*z[32];
    z[29]=n<T>(85,2) - 27*z[3];
    z[29]=z[3]*z[29];
    z[26]=z[26] + n<T>(59,3) + z[29];
    z[26]=z[7]*z[26];
    z[29]= - static_cast<T>(323)+ 141*z[3];
    z[29]=z[3]*z[29];
    z[26]=z[26] + n<T>(143,2) + z[29];
    z[26]=z[26]*z[54];
    z[29]=n<T>(817,8) - 41*z[3];
    z[29]=z[3]*z[29];
    z[26]=z[26] - n<T>(397,12) + z[29];
    z[26]=z[7]*z[26];
    z[18]=n<T>(1,8)*z[18] + z[26];
    z[13]=n<T>(1,4)*z[18] + z[13];
    z[13]=z[2]*z[13];
    z[18]=static_cast<T>(127)- 117*z[3];
    z[18]=z[3]*z[18];
    z[26]=7*z[3];
    z[29]=n<T>(41,4) - z[26];
    z[29]=z[3]*z[29];
    z[29]= - n<T>(3,4) + z[29];
    z[29]=z[7]*z[29];
    z[18]=z[29] + static_cast<T>(11)+ n<T>(1,8)*z[18];
    z[18]=z[18]*z[32];
    z[29]=25*z[3];
    z[37]= - n<T>(731,16) + z[29];
    z[37]=z[3]*z[37];
    z[18]=z[18] + n<T>(123,16) + z[37];
    z[18]=z[7]*z[18];
    z[37]=static_cast<T>(221)- n<T>(219,2)*z[3];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(109,2) + z[37];
    z[18]=n<T>(1,8)*z[37] + z[18];
    z[13]=n<T>(1,4)*z[18] + z[13];
    z[13]=z[2]*z[13];
    z[18]=static_cast<T>(31)+ 17*z[3];
    z[15]=z[18]*z[15];
    z[18]=static_cast<T>(61)- n<T>(65,2)*z[3];
    z[18]=z[3]*z[18];
    z[18]=n<T>(21,2)*z[40] - n<T>(63,2) + z[18];
    z[18]=z[7]*z[18];
    z[15]=z[18] - static_cast<T>(13)+ z[15];
    z[15]=z[7]*z[15];
    z[18]= - static_cast<T>(61)+ z[29];
    z[18]=z[3]*z[18];
    z[18]=static_cast<T>(63)+ z[18];
    z[18]=n<T>(1,2)*z[18] - 21*z[40];
    z[18]=z[7]*z[18];
    z[29]= - static_cast<T>(1)+ z[40];
    z[29]=z[1]*z[29];
    z[18]=n<T>(21,2)*z[29] + n<T>(13,2) + z[18];
    z[18]=z[1]*z[18];
    z[15]=z[18] - n<T>(13,2) + z[15];
    z[15]=z[15]*z[50];
    z[18]=n<T>(21,4)*z[7];
    z[29]= - static_cast<T>(3)+ z[20];
    z[29]=z[3]*z[29];
    z[29]=static_cast<T>(3)+ z[29];
    z[29]=z[29]*z[18];
    z[29]=z[29] + z[34];
    z[29]=z[7]*z[29];
    z[25]=z[7]*z[25];
    z[25]=n<T>(21,4)*z[1] + static_cast<T>(1)+ 21*z[25];
    z[25]=z[25]*z[24];
    z[25]=z[25] + n<T>(9,16) + z[29];
    z[24]=z[25]*z[24];
    z[25]= - z[39]*z[18];
    z[25]=z[25] + z[35];
    z[25]=z[7]*z[25];
    z[25]=n<T>(9,16)*z[33] + z[25];
    z[25]=z[7]*z[25];
    z[24]=z[24] - n<T>(9,32) + z[25];
    z[24]=z[1]*z[24];
    z[18]=z[18] - 1;
    z[18]=z[18]*z[27]*z[7];
    z[18]=n<T>(9,8)*z[27] + z[18];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(9,8)*z[30] + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(9,16) + z[18];
    z[18]=n<T>(1,2)*z[18] + z[24];
    z[18]=z[6]*z[18];
    z[24]= - static_cast<T>(31)+ z[46];
    z[24]=z[24]*z[20];
    z[24]=static_cast<T>(13)+ z[24];
    z[25]= - n<T>(61,8) + z[46];
    z[25]=z[3]*z[25];
    z[25]=n<T>(21,8) + z[25];
    z[25]=z[7]*z[25];
    z[24]=n<T>(1,8)*z[24] + z[25];
    z[24]=z[7]*z[24];
    z[25]=static_cast<T>(13)- n<T>(113,4)*z[3];
    z[24]=n<T>(1,8)*z[25] + z[24];
    z[24]=z[7]*z[24];
    z[15]=z[18] + z[15] + n<T>(13,8) + z[24];
    z[15]=z[15]*z[43];
    z[18]= - n<T>(25,3) + z[26];
    z[18]=z[18]*z[20];
    z[18]=n<T>(7,2)*z[49] + z[18] + n<T>(13,3)*z[40];
    z[18]=z[18]*z[36];
    z[20]= - n<T>(3,2) + n<T>(13,3)*z[3];
    z[20]=z[3]*z[20];
    z[20]=z[20] + n<T>(5,6)*z[40];
    z[20]=z[20]*z[32];
    z[18]=z[18] - n<T>(1,3)*z[33] + z[20];
    z[18]=z[8]*z[18];
    z[20]=z[3] + n<T>(5,12)*z[40];
    z[20]=z[7]*z[20];
    z[18]=z[20] + z[18];
    z[20]=z[31] + n<T>(1,4)*z[40];
    z[20]=z[7]*z[20];
    z[19]=z[40] + z[19];
    z[19]=z[19]*z[36];
    z[19]=z[19] + z[28] + z[20];
    z[19]=z[8]*z[19];
    z[20]=z[48] + z[40];
    z[20]=z[20]*z[32];
    z[19]=z[20] + z[19];
    z[19]=z[8]*z[19];
    z[20]=z[21]*z[42];
    z[19]=n<T>(1,4)*z[20] + z[19];
    z[19]=z[11]*z[19];
    z[18]=n<T>(1,8)*z[18] + n<T>(1,3)*z[19];
    z[18]=z[11]*z[18];
    z[19]= - n<T>(1,2)*z[9] - z[41] + z[36];
    z[19]=z[10]*z[19];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[9]*z[19];

    r += z[13] + z[14] + z[15] + n<T>(1,2)*z[16] + z[17] + z[18] + n<T>(1,4)*
      z[19] + z[22] + n<T>(1,8)*z[23];
 
    return r;
}

template double qqb_2lha_r1492(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1492(const std::array<dd_real,30>&);
#endif
