#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r468(const std::array<T,30>& k) {
  T z[49];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[2];
    z[7]=k[8];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[10];
    z[11]=k[9];
    z[12]=k[15];
    z[13]=k[7];
    z[14]=z[11] - n<T>(3,2);
    z[15]=n<T>(1,2)*z[1];
    z[16]= - z[15] + z[14];
    z[17]=n<T>(2935,36) + z[11];
    z[18]=n<T>(1,8)*z[11];
    z[19]= - n<T>(107,9) - z[18];
    z[19]=z[2]*z[19];
    z[17]=z[19] + n<T>(1,2)*z[17] + n<T>(107,9)*z[1];
    z[17]=z[2]*z[17];
    z[19]=3*z[11];
    z[20]= - n<T>(1043,18)*z[1] - n<T>(2881,18) - z[19];
    z[17]=n<T>(1,4)*z[20] + z[17];
    z[17]=z[2]*z[17];
    z[16]=n<T>(1,2)*z[16] + z[17];
    z[16]=z[2]*z[16];
    z[17]=z[4] + n<T>(1,4);
    z[20]=n<T>(5,4)*z[5];
    z[21]=z[20] - z[17];
    z[22]=n<T>(1,3)*z[2];
    z[23]=n<T>(223,4) - n<T>(49,3)*z[2];
    z[23]=z[2]*z[23];
    z[23]= - n<T>(205,12) + z[23];
    z[23]=z[23]*z[22];
    z[21]=3*z[21] + z[23];
    z[23]=n<T>(1,2)*z[8];
    z[21]=z[21]*z[23];
    z[24]=z[1] + 3;
    z[25]=z[24]*z[20];
    z[26]=z[17]*z[1];
    z[27]=n<T>(1,2)*z[11];
    z[28]=z[27] + 3;
    z[29]=3*z[4];
    z[16]=z[21] + z[16] + z[25] - z[26] - n<T>(1,4)*z[28] - z[29];
    z[16]=z[8]*z[16];
    z[21]=n<T>(1,4)*z[2];
    z[25]=z[11] + static_cast<T>(9)+ z[7];
    z[25]=z[25]*z[21];
    z[25]=z[25] - n<T>(241,36) - z[11];
    z[25]=z[2]*z[25];
    z[26]= - n<T>(7,2) + z[19];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[25]=z[2]*z[25];
    z[25]=z[25] + n<T>(3,4) - z[11];
    z[25]=z[2]*z[25];
    z[26]=z[20] - z[4];
    z[30]=z[27] - 1;
    z[25]=z[25] + n<T>(1,2)*z[30] + z[26];
    z[31]= - n<T>(11,2)*z[12] + n<T>(545,18) + z[13];
    z[26]=n<T>(1,2)*z[31] + z[26];
    z[26]=z[26]*z[23];
    z[31]=2*z[4];
    z[32]= - z[31] + n<T>(5,2)*z[5];
    z[26]=z[26] + n<T>(143,6)*z[2] - n<T>(565,72) + z[32];
    z[26]=z[8]*z[26];
    z[33]= - n<T>(379,2) + 313*z[2];
    z[33]=z[2]*z[33];
    z[26]=z[26] + n<T>(1,12)*z[33] + n<T>(15,4)*z[5] - n<T>(7,8) - z[29];
    z[26]=z[8]*z[26];
    z[33]= - n<T>(307,4) + n<T>(197,3)*z[2];
    z[33]=z[2]*z[33];
    z[33]= - static_cast<T>(1)+ n<T>(1,6)*z[33];
    z[33]=z[2]*z[33];
    z[26]=z[26] + z[33] - n<T>(3,8) + z[32];
    z[26]=z[8]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[25]=z[10]*z[25];
    z[26]=n<T>(1,36)*z[2];
    z[33]= - static_cast<T>(653)+ 707*z[2];
    z[33]=z[33]*z[26];
    z[34]= - n<T>(1,2) + z[32];
    z[35]=n<T>(313,36)*z[2] + n<T>(79,72) + z[32];
    z[35]=z[8]*z[35];
    z[33]=z[35] + 3*z[34] + z[33];
    z[33]=z[8]*z[33];
    z[34]= - n<T>(559,2) + 95*z[2];
    z[34]=z[2]*z[34];
    z[34]= - static_cast<T>(2)+ n<T>(5,36)*z[34];
    z[34]=z[2]*z[34];
    z[33]=z[33] + z[34] + n<T>(15,2)*z[5] - n<T>(11,8) - 6*z[4];
    z[33]=z[8]*z[33];
    z[34]=n<T>(1,2)*z[7];
    z[28]=z[34] + z[28];
    z[35]=n<T>(3,2)*z[2];
    z[28]=z[28]*z[35];
    z[28]=z[28] - n<T>(113,3) - z[19];
    z[35]=n<T>(1,2)*z[2];
    z[28]=z[28]*z[35];
    z[28]=z[28] - static_cast<T>(2)+ n<T>(9,4)*z[11];
    z[28]=z[2]*z[28];
    z[36]=static_cast<T>(1)- z[19];
    z[28]=n<T>(1,2)*z[36] + z[28];
    z[28]=z[2]*z[28];
    z[25]=z[25] + z[33] + z[28] + n<T>(3,4)*z[30] + z[32];
    z[25]=z[10]*z[25];
    z[28]=z[1] + 4;
    z[30]=n<T>(1,3)*z[1];
    z[28]= - z[28]*z[30];
    z[32]=static_cast<T>(2)+ z[1];
    z[32]=2*z[32] - z[2];
    z[32]=z[32]*z[22];
    z[28]=z[32] - static_cast<T>(2)+ z[28];
    z[32]=npow(z[2],4);
    z[28]=z[28]*z[32];
    z[33]=z[2] - 4;
    z[36]=z[1] - z[33];
    z[36]=z[36]*z[22];
    z[37]= - static_cast<T>(1)- z[30];
    z[36]=2*z[37] + z[36];
    z[37]=npow(z[2],3);
    z[36]=z[36]*z[37];
    z[33]= - z[33]*z[22];
    z[33]= - static_cast<T>(2)+ z[33];
    z[38]=npow(z[2],2);
    z[33]=z[8]*z[33]*z[38];
    z[33]=2*z[36] + z[33];
    z[33]=z[8]*z[33];
    z[22]= - z[8]*z[22];
    z[22]= - z[38] + z[22];
    z[22]=z[8]*z[22];
    z[22]= - z[37] + z[22];
    z[22]=z[8]*z[22];
    z[32]=n<T>(1,3)*z[32];
    z[22]= - z[32] + z[22];
    z[36]= - 4*z[2] - z[8];
    z[36]=z[8]*z[36];
    z[36]= - 2*z[38] + n<T>(1,3)*z[36];
    z[36]=z[8]*z[36];
    z[36]= - n<T>(4,3)*z[37] + z[36];
    z[36]=z[8]*z[36];
    z[32]= - z[32] + z[36];
    z[32]=z[10]*z[32];
    z[22]=4*z[22] + z[32];
    z[22]=z[10]*z[22];
    z[22]=z[22] + z[28] + z[33];
    z[22]=z[3]*z[22];
    z[28]=n<T>(1,4)*z[7];
    z[32]= - n<T>(55,3) - z[28];
    z[32]=z[32]*z[35];
    z[33]=n<T>(3,2)*z[7];
    z[36]=z[11] + n<T>(1133,9) + z[33];
    z[37]=n<T>(1,8)*z[7];
    z[38]=n<T>(55,3) + z[37];
    z[38]=z[1]*z[38];
    z[32]=z[32] + n<T>(1,4)*z[36] + z[38];
    z[32]=z[2]*z[32];
    z[36]= - n<T>(272,3) - n<T>(55,2)*z[1];
    z[30]=z[36]*z[30];
    z[30]=z[32] + z[30] - n<T>(443,12) - z[11];
    z[30]=z[2]*z[30];
    z[32]=z[15] + 1;
    z[36]= - z[32]*z[15];
    z[38]= - n<T>(1,4) + z[11];
    z[36]=3*z[38] + z[36];
    z[30]=n<T>(1,2)*z[36] + z[30];
    z[30]=z[2]*z[30];
    z[32]=z[32]*z[1];
    z[30]=z[30] - n<T>(1,4)*z[32] - n<T>(3,8) - z[11];
    z[30]=z[2]*z[30];
    z[14]=n<T>(1,2)*z[14] - z[29];
    z[17]= - z[17]*z[32];
    z[32]=z[32] + n<T>(3,2);
    z[20]=z[32]*z[20];
    z[14]=n<T>(29,3)*z[22] + z[25] + z[16] + z[30] + z[20] + n<T>(1,2)*z[14] + 
    z[17];
    z[14]=z[3]*z[14];
    z[16]=3*z[5];
    z[17]=n<T>(17,4) - z[16];
    z[17]=z[17]*z[16];
    z[17]=z[17] + n<T>(31,9) + n<T>(111,4)*z[4];
    z[20]=n<T>(3,2)*z[5];
    z[22]= - static_cast<T>(1)- z[5];
    z[22]=z[22]*z[20];
    z[25]= - n<T>(439,18) - z[13];
    z[22]=z[22] + n<T>(17,4)*z[4] + n<T>(1,4)*z[25] + 5*z[12];
    z[22]=z[8]*z[22];
    z[17]=z[22] + n<T>(1,2)*z[17] + n<T>(2,9)*z[2];
    z[17]=z[8]*z[17];
    z[22]=n<T>(23,2) - z[16];
    z[22]=z[22]*z[20];
    z[25]=n<T>(1,9)*z[2];
    z[30]= - static_cast<T>(92)+ n<T>(331,4)*z[2];
    z[30]=z[30]*z[25];
    z[36]= - n<T>(35,8) + z[29];
    z[17]=z[17] + z[30] + 5*z[36] + z[22];
    z[17]=z[8]*z[17];
    z[22]=23*z[4];
    z[30]= - n<T>(11,2) + z[22];
    z[36]=n<T>(7,2) - z[16];
    z[36]=z[5]*z[36];
    z[30]=n<T>(1,2)*z[30] + z[36];
    z[36]=n<T>(1513,2) - 359*z[2];
    z[25]=z[36]*z[25];
    z[25]=3*z[30] + z[25];
    z[30]=n<T>(1,2)*z[12];
    z[36]=n<T>(1,4)*z[12];
    z[38]=static_cast<T>(11)- z[36];
    z[38]=z[38]*z[30];
    z[39]=z[16] + 11;
    z[40]=n<T>(1,4)*z[5];
    z[41]= - z[39]*z[40];
    z[38]=z[41] + n<T>(5,2)*z[4] + z[38] - n<T>(197,18) - z[13];
    z[38]=z[8]*z[38];
    z[41]= - n<T>(5,4) - z[5];
    z[41]=z[41]*z[16];
    z[42]=3*z[13];
    z[43]=43*z[4] + 27*z[12] + n<T>(995,9) + z[42];
    z[38]=z[38] - n<T>(475,18)*z[2] + n<T>(1,4)*z[43] + z[41];
    z[38]=z[8]*z[38];
    z[25]=n<T>(1,2)*z[25] + z[38];
    z[25]=z[8]*z[25];
    z[38]=9*z[2];
    z[41]=n<T>(725,18) - z[38];
    z[41]=z[2]*z[41];
    z[41]= - n<T>(17,2) + z[41];
    z[41]=z[41]*z[35];
    z[43]= - n<T>(135,2) + 49*z[4];
    z[44]=n<T>(43,4) - z[16];
    z[44]=z[5]*z[44];
    z[25]=z[25] + z[41] + n<T>(1,4)*z[43] + z[44];
    z[25]=z[8]*z[25];
    z[41]=13*z[4];
    z[43]= - static_cast<T>(41)- 7*z[11];
    z[43]=n<T>(1,2)*z[43] + z[41];
    z[44]=n<T>(1,2)*z[5];
    z[45]=static_cast<T>(3)- z[44];
    z[45]=z[45]*z[16];
    z[43]=n<T>(1,2)*z[43] + z[45];
    z[45]=static_cast<T>(1)- n<T>(21,2)*z[11];
    z[37]=n<T>(7,8)*z[11] + static_cast<T>(1)+ z[37];
    z[37]=z[2]*z[37];
    z[37]=n<T>(1,4)*z[45] + z[37];
    z[37]=z[2]*z[37];
    z[45]= - static_cast<T>(55)+ 21*z[11];
    z[37]=n<T>(1,8)*z[45] + z[37];
    z[37]=z[2]*z[37];
    z[25]=z[25] + n<T>(1,2)*z[43] + z[37];
    z[25]=z[10]*z[25];
    z[37]=n<T>(25,4) - z[5];
    z[37]=z[37]*z[20];
    z[43]=n<T>(23,4) + z[7];
    z[43]=n<T>(1,2)*z[43] + z[11];
    z[43]=z[2]*z[43];
    z[43]=z[43] - n<T>(76,9) - z[19];
    z[43]=z[2]*z[43];
    z[43]=z[43] - n<T>(151,16) + z[19];
    z[43]=z[2]*z[43];
    z[17]=n<T>(1,2)*z[25] + z[17] + z[43] + z[37] + n<T>(43,8)*z[4] - n<T>(171,16)
    - z[11];
    z[17]=z[10]*z[17];
    z[24]= - z[24]*z[16];
    z[25]=static_cast<T>(5)+ z[1];
    z[24]=5*z[25] + z[24];
    z[24]=z[5]*z[24];
    z[25]=7*z[4];
    z[37]= - static_cast<T>(5)+ z[25];
    z[37]=z[1]*z[37];
    z[22]=z[24] + z[37] + z[22] - static_cast<T>(27)+ z[27];
    z[24]=n<T>(1,8)*z[5];
    z[37]= - static_cast<T>(1)- 9*z[5];
    z[37]=z[37]*z[24];
    z[43]=n<T>(205,2) - 49*z[2];
    z[26]=z[43]*z[26];
    z[26]=z[26] + z[37] - n<T>(133,144) + z[29];
    z[26]=z[8]*z[26];
    z[37]=n<T>(1525,9) + z[19];
    z[43]= - n<T>(49,3) - n<T>(1,4)*z[11];
    z[43]=z[2]*z[43];
    z[37]=z[43] + n<T>(1,4)*z[37] + n<T>(49,3)*z[1];
    z[37]=z[37]*z[35];
    z[43]=n<T>(3,4)*z[11];
    z[45]= - n<T>(49,3) - z[43];
    z[37]=z[37] + n<T>(1,2)*z[45] - n<T>(40,9)*z[1];
    z[37]=z[2]*z[37];
    z[22]=z[26] + n<T>(1,4)*z[22] + z[37];
    z[22]=z[8]*z[22];
    z[26]=n<T>(9,2)*z[11];
    z[37]=static_cast<T>(17)+ z[34];
    z[37]=13*z[37] + z[26];
    z[33]= - n<T>(361,9) - z[33];
    z[33]=z[33]*z[35];
    z[45]=n<T>(3,4)*z[7];
    z[46]=n<T>(361,9) + z[45];
    z[46]=z[1]*z[46];
    z[33]=z[33] + n<T>(1,4)*z[37] + z[46];
    z[33]=z[2]*z[33];
    z[37]=n<T>(27,2)*z[11];
    z[46]= - n<T>(523,3) - z[37];
    z[47]= - n<T>(193,2) - n<T>(361,9)*z[1];
    z[47]=z[1]*z[47];
    z[46]=n<T>(1,2)*z[46] + z[47];
    z[33]=n<T>(1,2)*z[46] + z[33];
    z[33]=z[2]*z[33];
    z[37]= - static_cast<T>(31)+ z[37];
    z[46]= - static_cast<T>(7)- n<T>(11,2)*z[1];
    z[46]=z[1]*z[46];
    z[37]=n<T>(1,2)*z[37] + z[46];
    z[33]=n<T>(1,2)*z[37] + z[33];
    z[33]=z[2]*z[33];
    z[37]=7*z[1];
    z[46]=static_cast<T>(11)+ z[37];
    z[46]=z[1]*z[46];
    z[46]=static_cast<T>(17)+ z[46];
    z[32]= - z[5]*z[32];
    z[32]=n<T>(1,2)*z[46] + z[32];
    z[32]=z[32]*z[20];
    z[46]= - n<T>(9,2) + z[4];
    z[46]=z[1]*z[46];
    z[46]=z[46] - static_cast<T>(7)+ n<T>(9,4)*z[4];
    z[46]=z[1]*z[46];
    z[32]=z[33] + z[32] + z[46] + n<T>(11,2)*z[4] - static_cast<T>(13)- n<T>(9,8)*z[11];
    z[14]=z[14] + z[17] + n<T>(1,2)*z[32] + z[22];
    z[14]=z[3]*z[14];
    z[17]=n<T>(49,9)*z[1];
    z[22]=n<T>(107,3) + z[11];
    z[18]= - n<T>(49,9) - z[18];
    z[18]=z[2]*z[18];
    z[18]=z[18] + n<T>(1,4)*z[22] + z[17];
    z[18]=z[18]*z[35];
    z[22]=n<T>(1,2) + z[16];
    z[22]=z[22]*z[44];
    z[32]=n<T>(7,2)*z[4];
    z[33]=n<T>(1,2) - z[12];
    z[22]=z[22] + 3*z[33] - z[32];
    z[22]=z[22]*z[23];
    z[33]=z[20] + 1;
    z[46]= - n<T>(3,2)*z[1] - z[33];
    z[46]=z[46]*z[44];
    z[47]= - 25*z[4] + n<T>(91,9) - z[27];
    z[48]=static_cast<T>(1)- n<T>(3,4)*z[4];
    z[48]=z[1]*z[48];
    z[18]=z[22] + z[18] + z[46] + n<T>(1,8)*z[47] + z[48];
    z[18]=z[8]*z[18];
    z[22]= - static_cast<T>(11)+ z[12];
    z[22]=z[22]*z[30];
    z[22]=z[22] + n<T>(9,2) + z[13];
    z[46]=n<T>(7,2) + z[16];
    z[46]=z[46]*z[40];
    z[22]=z[46] + n<T>(1,4)*z[22] - z[4];
    z[22]=z[8]*z[22];
    z[46]= - static_cast<T>(11)+ z[30];
    z[46]=z[12]*z[46];
    z[42]=n<T>(3,2)*z[46] - n<T>(493,18) - z[42];
    z[46]=15*z[5];
    z[47]= - static_cast<T>(17)+ z[46];
    z[47]=z[47]*z[24];
    z[48]=n<T>(9,4)*z[2];
    z[22]=z[22] + z[48] + z[47] + n<T>(1,4)*z[42] - 5*z[4];
    z[22]=z[8]*z[22];
    z[42]=n<T>(1,8)*z[2];
    z[38]= - n<T>(547,9) + z[38];
    z[38]=z[38]*z[42];
    z[47]= - static_cast<T>(5)+ z[5];
    z[47]=z[5]*z[47];
    z[22]=z[22] + z[38] + n<T>(9,8)*z[47] - 9*z[4] + static_cast<T>(5)- n<T>(33,16)*z[12];
    z[22]=z[8]*z[22];
    z[38]= - z[39]*z[24];
    z[39]=7*z[2];
    z[47]=n<T>(11,2) - z[39];
    z[47]=z[47]*z[21];
    z[22]=z[22] + z[47] + z[38] + n<T>(45,8) - z[25];
    z[22]=z[8]*z[22];
    z[38]=static_cast<T>(5)- z[20];
    z[38]=z[38]*z[40];
    z[26]= - static_cast<T>(7)+ z[26];
    z[26]=z[2]*z[26];
    z[26]=z[26] + n<T>(23,2) - 9*z[11];
    z[26]=z[26]*z[42];
    z[42]=z[11] - 1;
    z[22]=z[22] + z[26] + z[38] + n<T>(9,16)*z[42] - z[31];
    z[22]=z[10]*z[22];
    z[26]=n<T>(9,4)*z[5];
    z[31]= - static_cast<T>(3)+ z[5];
    z[31]=z[31]*z[26];
    z[31]=n<T>(27,4)*z[2] + z[31] - n<T>(85,4)*z[4] - n<T>(53,8)*z[12] - n<T>(85,9) - n<T>(3,4)*z[13];
    z[38]=11*z[4];
    z[47]= - n<T>(19,2) + z[12];
    z[47]=z[12]*z[47];
    z[47]= - z[38] + z[47] + static_cast<T>(9)+ z[13];
    z[33]=z[5]*z[33];
    z[33]=n<T>(1,4)*z[47] + z[33];
    z[33]=z[8]*z[33];
    z[31]=n<T>(1,2)*z[31] + z[33];
    z[31]=z[8]*z[31];
    z[33]=static_cast<T>(1)- z[5];
    z[26]=z[33]*z[26];
    z[33]= - static_cast<T>(1)+ z[48];
    z[33]=z[33]*z[35];
    z[26]=z[31] + z[33] + z[26] + n<T>(25,4) - z[41];
    z[26]=z[8]*z[26];
    z[31]= - static_cast<T>(9)+ n<T>(5,2)*z[11];
    z[33]=static_cast<T>(53)- z[46];
    z[33]=z[5]*z[33];
    z[31]=z[33] + 3*z[31] - 41*z[4];
    z[33]= - n<T>(1,2) - 15*z[11];
    z[28]=n<T>(5,4)*z[11] + static_cast<T>(1)+ z[28];
    z[28]=z[2]*z[28];
    z[28]=n<T>(1,2)*z[33] + 3*z[28];
    z[28]=z[2]*z[28];
    z[28]=n<T>(1,2)*z[31] + z[28];
    z[22]=z[22] + n<T>(1,4)*z[28] + z[26];
    z[22]=z[10]*z[22];
    z[26]= - n<T>(3,2) - z[1];
    z[15]=z[26]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[16];
    z[26]=z[1] + 1;
    z[26]=z[26]*z[1];
    z[28]=static_cast<T>(17)+ n<T>(21,2)*z[26];
    z[15]=n<T>(1,2)*z[28] + z[15];
    z[15]=z[5]*z[15];
    z[28]= - n<T>(15,4)*z[1] - static_cast<T>(3)- n<T>(1,4)*z[4];
    z[28]=z[1]*z[28];
    z[15]=z[15] + z[28] - z[29] - static_cast<T>(5)+ z[43];
    z[28]=n<T>(1,16)*z[7];
    z[31]=n<T>(14,9) + z[28];
    z[31]=z[31]*z[37];
    z[28]= - n<T>(7,9) - z[28];
    z[28]=z[28]*z[39];
    z[33]=z[19] + n<T>(713,9) + 5*z[7];
    z[28]=z[28] + n<T>(1,8)*z[33] + z[31];
    z[28]=z[2]*z[28];
    z[19]= - n<T>(25,3) - z[19];
    z[31]= - static_cast<T>(61)- 49*z[1];
    z[31]=z[1]*z[31];
    z[19]=z[28] + n<T>(1,4)*z[19] + n<T>(1,9)*z[31];
    z[19]=z[2]*z[19];
    z[14]=z[14] + z[22] + z[18] + n<T>(1,2)*z[15] + z[19];
    z[14]=z[3]*z[14];
    z[15]=n<T>(13,4) - z[12];
    z[15]=z[15]*z[36];
    z[18]=z[20] + n<T>(1,2);
    z[18]=z[6]*z[18];
    z[18]=static_cast<T>(5)+ z[18];
    z[18]=z[18]*z[40];
    z[19]=npow(z[12],2);
    z[19]= - z[4] + n<T>(1,4)*z[19];
    z[21]= - z[19]*z[21];
    z[22]=z[16] + 1;
    z[22]=z[22]*z[44];
    z[28]=z[22] + z[19];
    z[31]=n<T>(1,4)*z[8];
    z[33]= - z[28]*z[31];
    z[15]=z[33] + z[21] + z[18] + n<T>(13,8)*z[4] + z[15] + static_cast<T>(1)+ n<T>(3,8)*z[13];
    z[15]=z[8]*z[15];
    z[18]=n<T>(3,4)*z[5];
    z[21]=static_cast<T>(3)+ z[6];
    z[21]=z[21]*z[18];
    z[21]=z[21] - n<T>(3,4) - z[6];
    z[21]=z[5]*z[21];
    z[33]= - n<T>(9,2) - z[12];
    z[33]=z[12]*z[33];
    z[33]=static_cast<T>(7)+ z[33];
    z[33]=n<T>(1,2)*z[33] + z[29];
    z[33]=z[33]*z[35];
    z[37]=static_cast<T>(1)- z[30];
    z[37]=z[12]*z[37];
    z[37]=27*z[4] + 3*z[37] + n<T>(3,2) - z[6];
    z[21]=z[33] + n<T>(1,4)*z[37] + z[21];
    z[15]=n<T>(1,2)*z[21] + z[15];
    z[15]=z[8]*z[15];
    z[21]= - static_cast<T>(9)- z[30];
    z[21]=z[12]*z[21];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=n<T>(1,2)*z[21] + z[29];
    z[21]=z[2]*z[21];
    z[29]=n<T>(1,4)*z[6];
    z[30]=z[16] - 7;
    z[33]=z[5]*z[30];
    z[21]=z[21] + z[33] + n<T>(23,2)*z[4] - n<T>(7,4)*z[12] + static_cast<T>(3)- z[29];
    z[15]=n<T>(1,4)*z[21] + z[15];
    z[15]=z[8]*z[15];
    z[21]=z[29] + 1;
    z[21]= - z[21]*z[42];
    z[33]=z[2]*z[9];
    z[37]= - n<T>(3,2)*z[33] + z[4] - n<T>(9,4)*z[12] + z[42];
    z[37]=z[2]*z[37];
    z[21]=z[37] + z[32] + z[21];
    z[15]=n<T>(1,4)*z[21] + z[15];
    z[15]=z[10]*z[15];
    z[21]= - z[30]*z[44];
    z[30]=z[12] + z[9];
    z[30]=5*z[11] + static_cast<T>(1)- 9*z[30];
    z[30]= - z[33] + n<T>(1,4)*z[30] + z[4];
    z[30]=z[2]*z[30];
    z[32]=z[6] + 5;
    z[33]= - z[11]*z[32];
    z[33]= - static_cast<T>(7)+ z[33];
    z[21]=z[30] + z[21] + n<T>(1,4)*z[33] + z[25];
    z[25]=static_cast<T>(5)+ z[13];
    z[30]= - static_cast<T>(1)- n<T>(5,2)*z[12];
    z[30]=z[12]*z[30];
    z[25]=3*z[25] + z[30];
    z[30]=z[6] + n<T>(1,2);
    z[33]=z[30]*z[16];
    z[32]=z[33] + z[32];
    z[32]=z[5]*z[32];
    z[19]= - z[2]*z[19];
    z[19]=z[19] + z[32] + n<T>(1,2)*z[25] + z[38];
    z[25]= - z[8]*z[28];
    z[19]=n<T>(1,2)*z[19] + z[25];
    z[19]=z[19]*z[23];
    z[23]=static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[20]=z[23]*z[20];
    z[20]=z[20] - n<T>(7,4) - z[6];
    z[20]=z[20]*z[44];
    z[23]= - static_cast<T>(9)- z[12];
    z[23]=z[23]*z[36];
    z[23]=z[23] + n<T>(7,2) - z[9];
    z[23]=n<T>(1,2)*z[23] + z[4];
    z[23]=z[23]*z[35];
    z[25]=n<T>(9,2) - z[6];
    z[19]=z[19] + z[23] + z[20] + 4*z[4] + n<T>(1,8)*z[25] - 2*z[12];
    z[19]=z[8]*z[19];
    z[15]=z[15] + n<T>(1,4)*z[21] + z[19];
    z[15]=z[10]*z[15];
    z[19]=static_cast<T>(3)- z[9];
    z[20]=z[9]*z[30];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[6]*z[20];
    z[19]= - z[11] + n<T>(3,2)*z[19] + z[20];
    z[20]= - z[6] + 1;
    z[20]=z[34]*z[20];
    z[20]=n<T>(1,9) + z[20];
    z[17]=n<T>(1,2)*z[20] - z[17];
    z[17]=z[1]*z[17];
    z[17]=z[17] + n<T>(1,2)*z[19] + z[4];
    z[19]=z[7]*z[29];
    z[19]=z[19] + n<T>(31,9) - z[9] + z[34] + z[27];
    z[20]= - n<T>(49,9) - z[45];
    z[20]=z[20]*z[35];
    z[21]=n<T>(49,9) + n<T>(3,8)*z[7];
    z[21]=z[1]*z[21];
    z[19]=z[20] + n<T>(1,2)*z[19] + z[21];
    z[19]=z[2]*z[19];
    z[20]= - static_cast<T>(1)- z[26];
    z[18]=z[20]*z[18];
    z[20]=static_cast<T>(1)+ 5*z[1];
    z[20]=z[1]*z[20];
    z[18]=z[18] + static_cast<T>(1)+ n<T>(1,4)*z[20];
    z[18]=z[5]*z[18];
    z[17]=z[19] + n<T>(1,2)*z[17] + z[18];
    z[18]=static_cast<T>(1)+ z[6];
    z[16]=z[18]*z[16];
    z[16]=z[6] + z[16];
    z[16]=z[16]*z[24];
    z[18]=static_cast<T>(3)- z[12];
    z[18]=z[12]*z[18];
    z[18]=z[9] + z[18];
    z[18]= - z[22] + n<T>(1,2)*z[18] + z[4];
    z[18]=z[18]*z[31];
    z[19]= - z[29] - n<T>(1,16);
    z[19]=z[9]*z[19];
    z[16]=z[18] + z[16] + n<T>(3,8)*z[4] - n<T>(25,16)*z[12] + static_cast<T>(1)+ z[19];
    z[16]=z[8]*z[16];

    r += z[14] + z[15] + z[16] + n<T>(1,2)*z[17];
 
    return r;
}

template double qqb_2lha_r468(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r468(const std::array<dd_real,30>&);
#endif
