#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r407(const std::array<T,30>& k) {
  T z[44];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[4];
    z[10]=k[10];
    z[11]=k[12];
    z[12]=k[15];
    z[13]= - static_cast<T>(1)+ n<T>(7,8)*z[12];
    z[13]=z[13]*z[12];
    z[14]=n<T>(1,2)*z[7];
    z[13]=z[13] - z[14];
    z[15]=3*z[7];
    z[16]=3*z[4];
    z[17]=z[15] + z[16];
    z[18]=n<T>(1,2)*z[6];
    z[19]=z[17] - z[18];
    z[20]=static_cast<T>(1)+ n<T>(33,8)*z[12] - z[19];
    z[20]=z[20]*z[18];
    z[21]=n<T>(1,2)*z[4];
    z[20]=z[20] + z[21] + n<T>(1,4) - z[13];
    z[20]=z[6]*z[20];
    z[22]=n<T>(1,4)*z[6];
    z[23]=static_cast<T>(1)+ n<T>(13,6)*z[6];
    z[23]=z[23]*z[22];
    z[24]=z[3]*z[2];
    z[25]=n<T>(1,2)*z[2];
    z[26]=n<T>(1,3) - z[25];
    z[26]=z[26]*z[24];
    z[27]=n<T>(5,2)*z[6];
    z[28]=z[27] + n<T>(23,2)*z[2] - n<T>(41,2) + z[11];
    z[26]=n<T>(1,12)*z[28] + z[26];
    z[26]=z[3]*z[26];
    z[28]=z[2] - n<T>(37,48);
    z[23]=n<T>(1,4)*z[26] + z[23] - z[28];
    z[23]=z[3]*z[23];
    z[26]=z[4] + n<T>(1,2);
    z[26]=z[26]*z[16];
    z[26]=z[26] + 1;
    z[29]=z[2]*z[4];
    z[30]=z[26]*z[29];
    z[31]=z[8] - 1;
    z[32]=n<T>(1,8)*z[8];
    z[33]=z[31]*z[32];
    z[34]=9*z[4];
    z[35]=n<T>(7,2) - z[34];
    z[35]=z[4]*z[35];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[4]*z[35];
    z[20]=z[23] + z[20] - z[30] + z[35] - z[33];
    z[20]=z[9]*z[20];
    z[23]=z[18]*z[5];
    z[35]=n<T>(33,8) - z[7];
    z[35]=z[23] - z[16] + 3*z[35] + n<T>(7,8)*z[5];
    z[35]=z[6]*z[35];
    z[36]=n<T>(1,4) - z[12];
    z[36]=z[5] + 15*z[36] + 7*z[7];
    z[36]=n<T>(1,2)*z[36] - z[4];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[35]=z[6]*z[35];
    z[36]=n<T>(1,2)*z[3];
    z[37]=n<T>(41,24) - z[2];
    z[37]=z[2]*z[37];
    z[37]= - n<T>(23,24) + z[37];
    z[37]=z[37]*z[36];
    z[38]=static_cast<T>(23)- n<T>(19,4)*z[11];
    z[39]= - static_cast<T>(2)+ z[2];
    z[39]=z[2]*z[39];
    z[37]=z[37] + n<T>(55,48)*z[6] + n<T>(1,24)*z[38] + z[39];
    z[37]=z[3]*z[37];
    z[38]=n<T>(1,2)*z[13];
    z[39]=n<T>(1,4)*z[8];
    z[40]= - n<T>(1,8) - z[5];
    z[40]=z[8]*z[40];
    z[40]=z[40] + n<T>(1,2) + z[5];
    z[40]=z[40]*z[39];
    z[41]= - static_cast<T>(5)+ 6*z[4];
    z[41]=z[4]*z[41];
    z[41]= - static_cast<T>(2)+ z[41];
    z[41]=z[4]*z[41];
    z[41]=z[41] + z[30];
    z[41]=z[2]*z[41];
    z[42]=static_cast<T>(13)- 18*z[4];
    z[42]=z[4]*z[42];
    z[42]=n<T>(1,2) + z[42];
    z[42]=z[4]*z[42];
    z[20]=z[20] + z[37] + z[35] + z[41] + z[40] + z[38] + z[42];
    z[20]=z[9]*z[20];
    z[35]=n<T>(7,2)*z[12];
    z[37]= - static_cast<T>(3)+ z[35];
    z[37]= - z[22] + n<T>(1,4)*z[37];
    z[37]=z[12]*z[37];
    z[40]=z[4] + z[7];
    z[37]=z[37] - z[40];
    z[37]=z[6]*z[37];
    z[13]=z[37] + z[4] - z[13];
    z[13]=z[13]*z[18];
    z[37]=npow(z[2],2);
    z[41]= - z[37]*z[36];
    z[42]=z[11] + n<T>(37,2)*z[2];
    z[43]= - static_cast<T>(1)+ z[6];
    z[43]=z[6]*z[43];
    z[41]=z[41] + n<T>(1,4)*z[42] + z[43];
    z[41]=z[3]*z[41];
    z[42]=n<T>(1,3)*z[6];
    z[27]= - static_cast<T>(7)+ z[27];
    z[27]=z[27]*z[42];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[22];
    z[27]=n<T>(1,12)*z[41] + z[27] + n<T>(35,96) - z[2];
    z[27]=z[3]*z[27];
    z[41]=z[4] - n<T>(1,2);
    z[41]=z[41]*z[16];
    z[41]=z[41] - n<T>(1,2);
    z[43]= - z[4]*z[41];
    z[13]=z[27] + z[13] + z[43] - z[30];
    z[13]=z[9]*z[13];
    z[27]= - n<T>(13,2) + z[11];
    z[28]= - z[2]*z[28];
    z[27]=n<T>(11,48)*z[6] + n<T>(1,24)*z[27] + z[28];
    z[27]=z[27]*z[36];
    z[28]=static_cast<T>(35)- n<T>(19,2)*z[11];
    z[30]= - static_cast<T>(1)+ z[2];
    z[30]=z[2]*z[30];
    z[43]= - n<T>(2,3) + z[18];
    z[43]=z[6]*z[43];
    z[27]=z[27] + z[43] + n<T>(1,48)*z[28] + z[30];
    z[27]=z[3]*z[27];
    z[28]= - static_cast<T>(1)- z[35];
    z[28]=z[12]*z[28];
    z[28]=z[16] + z[28] + n<T>(9,2)*z[7];
    z[30]=n<T>(13,3) + 29*z[12];
    z[19]=n<T>(1,8)*z[30] - z[19];
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,2)*z[28] + z[19];
    z[19]=z[19]*z[18];
    z[28]=n<T>(5,2) - z[16];
    z[28]=z[28]*z[16];
    z[28]=n<T>(1,4) + z[28];
    z[28]=z[4]*z[28];
    z[30]= - z[4] + z[29];
    z[26]=z[2]*z[26]*z[30];
    z[13]=z[13] + z[27] + z[19] + z[26] + z[38] + z[28];
    z[13]=z[9]*z[13];
    z[17]=z[23] + n<T>(5,6)*z[5] + n<T>(91,8) - z[17];
    z[17]=z[6]*z[17];
    z[19]=n<T>(1,4)*z[5];
    z[23]= - n<T>(29,3) - 19*z[12];
    z[15]=z[17] + z[19] + n<T>(1,4)*z[23] + z[15];
    z[15]=z[15]*z[18];
    z[17]=z[4] - n<T>(3,2);
    z[18]=z[17]*z[16];
    z[23]=n<T>(5,4) + z[18];
    z[23]=z[4]*z[23];
    z[26]=z[41]*z[29];
    z[23]=z[23] + z[26];
    z[23]=z[2]*z[23];
    z[26]=n<T>(7,2) - z[16];
    z[26]=z[26]*z[16];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[4]*z[26];
    z[27]=n<T>(1,2)*z[8];
    z[28]=n<T>(7,6)*z[11] - z[5];
    z[28]=z[28]*z[27];
    z[30]=z[5] + n<T>(1,4);
    z[28]=z[28] - n<T>(7,3)*z[11] + z[30];
    z[28]=z[28]*z[32];
    z[35]= - static_cast<T>(1)- n<T>(5,4)*z[11];
    z[38]=n<T>(1,2) - z[2];
    z[38]=z[2]*z[38];
    z[35]=n<T>(7,8)*z[6] + n<T>(1,6)*z[35] + z[38];
    z[35]=z[35]*z[36];
    z[38]=static_cast<T>(3)+ 7*z[12];
    z[38]=z[12]*z[38];
    z[38]= - static_cast<T>(1)+ n<T>(1,4)*z[38];
    z[38]=n<T>(1,2)*z[38] - z[7];
    z[13]=z[13] + z[35] + z[15] + z[23] + z[28] + n<T>(1,2)*z[38] + z[26];
    z[13]=z[9]*z[13];
    z[14]=z[14] - 1;
    z[15]=3*z[5];
    z[23]=n<T>(97,3) + z[15];
    z[23]=z[5]*z[23];
    z[26]=npow(z[5],2);
    z[28]=z[6]*z[26];
    z[23]=n<T>(1,12)*z[28] - z[21] + n<T>(1,16)*z[23] - z[14];
    z[23]=z[6]*z[23];
    z[28]=5*z[7];
    z[35]=n<T>(3,2) + z[5];
    z[35]=z[5]*z[35];
    z[35]=z[35] - n<T>(23,12) + z[28];
    z[35]=n<T>(1,2)*z[35] - z[4];
    z[23]=n<T>(1,4)*z[35] + z[23];
    z[23]=z[6]*z[23];
    z[35]=z[5] - n<T>(1,2);
    z[35]=z[35]*z[5];
    z[38]= - static_cast<T>(1)+ z[11];
    z[38]=n<T>(7,6)*z[38] - z[35];
    z[38]=z[38]*z[27];
    z[41]=n<T>(19,6)*z[11];
    z[43]= - static_cast<T>(1)- z[41];
    z[35]=z[38] + n<T>(1,2)*z[43] + z[35];
    z[35]=z[35]*z[32];
    z[17]= - z[4]*z[17];
    z[17]= - n<T>(1,4) + z[17];
    z[17]=z[17]*z[16];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[29];
    z[38]= - z[7] + n<T>(11,4)*z[12];
    z[38]=n<T>(1,4)*z[38];
    z[13]=z[13] + z[23] + z[18] + z[35] + z[38] + z[17];
    z[13]=z[10]*z[13];
    z[17]= - static_cast<T>(5)+ n<T>(7,2)*z[11];
    z[18]= - n<T>(3,4) - z[5];
    z[18]=z[5]*z[18];
    z[17]=n<T>(1,6)*z[17] + z[18];
    z[17]=z[17]*z[27];
    z[18]=static_cast<T>(1)- z[41];
    z[23]=static_cast<T>(1)+ n<T>(1,2)*z[5];
    z[23]=z[5]*z[23];
    z[17]=z[17] + n<T>(1,4)*z[18] + z[23];
    z[17]=z[17]*z[39];
    z[18]=z[26]*z[22];
    z[15]=n<T>(325,6) + z[15];
    z[15]=z[5]*z[15];
    z[14]=z[18] - n<T>(3,2)*z[4] - 3*z[14] + n<T>(1,8)*z[15];
    z[14]=z[6]*z[14];
    z[15]=n<T>(203,24) + z[28];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[5]*z[19];
    z[15]=n<T>(1,2)*z[15] + z[19];
    z[14]=z[14] + n<T>(1,2)*z[15] - z[4];
    z[14]=z[6]*z[14];
    z[15]=z[34] - n<T>(19,2);
    z[15]=z[15]*z[4];
    z[19]=static_cast<T>(1)+ z[15];
    z[19]=z[19]*z[29];
    z[15]= - n<T>(1,2) - z[15];
    z[15]=z[4]*z[15];
    z[13]=z[13] + z[20] + n<T>(1,48)*z[3] + z[14] + z[19] + z[17] + z[38] + 
    z[15];
    z[13]=z[10]*z[13];
    z[14]=z[8] + n<T>(5,4);
    z[14]=z[14]*z[8];
    z[15]=n<T>(5,3)*z[14];
    z[17]=npow(z[8],2);
    z[19]=z[17]*z[2];
    z[20]=n<T>(11,4)*z[19];
    z[23]= - z[15] + z[20];
    z[23]=z[2]*z[23];
    z[26]=z[22] + n<T>(7,12);
    z[26]=z[8]*z[26];
    z[23]=z[23] + z[26];
    z[26]=z[8] + 1;
    z[28]=z[26]*z[8];
    z[29]= - z[28] + z[19];
    z[29]=z[29]*z[25];
    z[34]=n<T>(1,3)*z[8];
    z[29]=z[34] + z[29];
    z[29]=z[29]*z[24];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[23]=z[3]*z[23];
    z[29]=n<T>(13,2)*z[19];
    z[35]=z[27] + 1;
    z[38]=z[35]*z[8];
    z[39]=n<T>(11,3)*z[38];
    z[41]= - z[39] + z[29];
    z[37]= - z[3]*z[17]*z[37];
    z[37]= - z[20] + z[37];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(13,4)*z[17] + z[37];
    z[37]=z[9]*z[37];
    z[23]=n<T>(1,6)*z[37] + n<T>(1,4)*z[41] + z[23];
    z[23]=z[9]*z[3]*z[23];
    z[31]=z[31]*z[8];
    z[23]=z[31] - z[23];
    z[37]= - static_cast<T>(23)- n<T>(7,2)*z[8];
    z[37]=z[37]*z[27];
    z[37]= - static_cast<T>(7)+ z[37];
    z[15]=z[15] - n<T>(11,8)*z[19];
    z[15]=z[2]*z[15];
    z[15]=n<T>(1,6)*z[37] + z[15];
    z[15]=z[2]*z[15];
    z[37]=n<T>(1,4) + z[8];
    z[15]=n<T>(1,3)*z[37] + z[15];
    z[37]= - n<T>(5,3) - z[27];
    z[37]=z[37]*z[27];
    z[17]= - z[17]*z[25];
    z[17]=z[28] + z[17];
    z[17]=z[17]*z[25];
    z[17]=z[17] - n<T>(1,3) + z[37];
    z[17]=z[2]*z[17];
    z[17]=n<T>(1,3)*z[26] + z[17];
    z[17]=z[17]*z[24];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[3]*z[15];
    z[17]=z[39] - n<T>(13,4)*z[19];
    z[17]=z[2]*z[17];
    z[17]=z[17] - n<T>(1,2) - n<T>(1,3)*z[31];
    z[15]=z[15] + n<T>(1,4)*z[17] + z[42];
    z[15]=z[15]*z[36];
    z[17]= - static_cast<T>(1)+ n<T>(1,3)*z[5];
    z[22]= - z[17]*z[22];
    z[22]=z[22] - n<T>(109,48);
    z[22]=z[5]*z[22];
    z[22]=n<T>(91,16) + z[22] - z[40];
    z[22]=z[6]*z[22];
    z[24]=z[7] + static_cast<T>(1)- n<T>(11,2)*z[12];
    z[22]=n<T>(1,4)*z[24] + z[22];
    z[22]=z[6]*z[22];
    z[24]=static_cast<T>(1)- z[16];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1,2) + 2*z[24];
    z[24]=z[4]*z[24];
    z[15]=z[15] + z[22] + z[24] - n<T>(1,4)*z[23];
    z[15]=z[9]*z[15];
    z[22]=n<T>(1,2)*z[1];
    z[23]=z[22] + 1;
    z[23]= - z[23]*z[40];
    z[17]=n<T>(1,3)*z[1] - z[17];
    z[17]=z[17]*z[18];
    z[18]=z[1] + 2;
    z[24]=static_cast<T>(311)+ 131*z[1];
    z[24]=n<T>(1,2)*z[24] - 61*z[5];
    z[24]=z[5]*z[24];
    z[17]=z[17] + n<T>(1,24)*z[24] + z[18] + z[23];
    z[17]=z[6]*z[17];
    z[23]= - n<T>(3,2) - z[1];
    z[21]=z[23]*z[21];
    z[23]=n<T>(1,4)*z[7] + static_cast<T>(9)+ n<T>(127,24)*z[1];
    z[17]=z[17] + z[21] + n<T>(1,2)*z[23] - n<T>(7,3)*z[5];
    z[17]=z[6]*z[17];
    z[21]=static_cast<T>(13)+ n<T>(7,3)*z[8];
    z[21]=z[21]*z[27];
    z[21]=static_cast<T>(3)+ z[21];
    z[14]= - 5*z[14] + z[20];
    z[20]=n<T>(1,3)*z[2];
    z[14]=z[14]*z[20];
    z[14]=n<T>(1,2)*z[21] + z[14];
    z[14]=z[2]*z[14];
    z[21]= - static_cast<T>(3)+ z[1];
    z[23]= - static_cast<T>(1)+ n<T>(1,6)*z[8];
    z[23]=z[8]*z[23];
    z[14]=z[14] + n<T>(1,2)*z[21] + z[23];
    z[14]=z[2]*z[14];
    z[21]= - z[1] - z[34];
    z[14]=n<T>(1,2)*z[21] + z[14];
    z[21]= - static_cast<T>(7)- z[8];
    z[21]=z[21]*z[27];
    z[21]=z[21] - n<T>(5,2) - z[1];
    z[19]= - z[28] + n<T>(1,3)*z[19];
    z[19]=z[2]*z[19];
    z[23]=n<T>(1,3) + z[32];
    z[23]=z[8]*z[23];
    z[19]=n<T>(1,8)*z[19] + n<T>(1,12) + z[23];
    z[19]=z[2]*z[19];
    z[19]=n<T>(1,12)*z[21] + z[19];
    z[19]=z[2]*z[19];
    z[21]=z[1] + z[35];
    z[19]=n<T>(1,6)*z[21] + z[19];
    z[19]=z[2]*z[19];
    z[21]= - n<T>(1,2) - z[1];
    z[19]=n<T>(1,12)*z[21] + z[19];
    z[19]=z[3]*z[19];
    z[14]=n<T>(1,8)*z[14] + z[19];
    z[14]=z[3]*z[14];
    z[19]= - 11*z[38] + z[29];
    z[19]=z[19]*z[25];
    z[19]=z[31] + z[19];
    z[19]=z[19]*z[20];
    z[20]=n<T>(7,3) - z[8];
    z[20]=z[8]*z[20];
    z[20]=n<T>(13,3) + z[20];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[14]=n<T>(1,8)*z[19] + z[14];
    z[14]=z[3]*z[14];
    z[16]=z[18]*z[16];
    z[16]= - z[22] + z[16] - 2;
    z[16]=z[16]*z[4];
    z[18]= - n<T>(1,2) + z[16];
    z[18]=z[4]*z[18];
    z[18]=z[18] + z[33];
    z[18]=z[2]*z[18];
    z[16]=n<T>(3,4) - z[16];
    z[16]=z[4]*z[16];
    z[19]= - z[8]*z[30];
    z[19]=z[5] + z[19];
    z[19]=z[19]*z[32];

    r += n<T>(1,8) + z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + z[19];
 
    return r;
}

template double qqb_2lha_r407(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r407(const std::array<dd_real,30>&);
#endif
