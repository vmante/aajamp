#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r318(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[13];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[3];
    z[9]=k[10];
    z[10]=z[7]*z[6];
    z[11]=z[10] + 1;
    z[12]=n<T>(1,2)*z[2];
    z[11]=z[12]*z[11];
    z[13]=z[2] - 1;
    z[14]=z[6]*z[2];
    z[15]= - z[13]*z[14];
    z[11]=z[15] + z[11];
    z[11]=z[7]*z[11];
    z[15]=z[12] - 1;
    z[16]=z[2]*z[15];
    z[16]=n<T>(1,2) + z[16];
    z[14]=z[16]*z[14];
    z[16]=z[1] + 1;
    z[15]=n<T>(1,2)*z[1] - z[15];
    z[15]=z[2]*z[15];
    z[11]=z[11] + z[14] - n<T>(1,2)*z[16] + z[15];
    z[11]=z[3]*z[11];
    z[14]=7*z[2];
    z[15]= - static_cast<T>(5)+ z[14];
    z[12]=z[15]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[6]*z[12];
    z[15]=n<T>(5,2) - z[14];
    z[15]=z[6]*z[15];
    z[15]=n<T>(7,2)*z[10] + n<T>(7,2) + z[15];
    z[15]=z[7]*z[15];
    z[14]= - z[14] + static_cast<T>(1)- z[1];
    z[12]=z[15] + n<T>(1,2)*z[14] + z[12];
    z[14]=n<T>(5,3)*z[9];
    z[15]=static_cast<T>(1)- z[14];
    z[15]=z[7]*z[15];
    z[17]=z[5]*z[9]*npow(z[7],2);
    z[15]=z[15] - n<T>(5,4)*z[17];
    z[15]=z[5]*z[15];
    z[12]=n<T>(1,6)*z[12] + z[15];
    z[11]=n<T>(1,2)*z[12] + n<T>(1,3)*z[11];
    z[11]=z[3]*z[11];
    z[12]= - z[14] + z[13];
    z[12]=z[6]*z[12];
    z[12]=n<T>(1,3) + z[12];
    z[13]=n<T>(1,2)*z[4];
    z[10]=z[10]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[6]*z[13];
    z[10]=z[13] + z[10];
    z[10]=z[7]*z[10];
    z[10]=n<T>(1,2)*z[12] + z[10];
    z[12]= - z[4] + 1;
    z[13]=z[16] + z[9];
    z[12]=z[13]*z[12];
    z[12]= - z[8] + z[12];
    z[13]= - static_cast<T>(1)- n<T>(1,2)*z[7];
    z[13]=z[9]*z[13];
    z[13]= - n<T>(1,2) + z[13];
    z[13]=z[4]*z[13];
    z[13]= - n<T>(1,2) + z[13];
    z[13]=z[7]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[5]*z[12];
    z[13]=z[8] + z[7] - z[1];
    z[12]=n<T>(5,2)*z[12] - n<T>(5,4)*z[4] - n<T>(5,12)*z[9] + static_cast<T>(1)+ n<T>(1,12)*z[13];
    z[12]=z[5]*z[12];
    z[10]=n<T>(1,2)*z[10] + z[12];
    z[10]=n<T>(1,2)*z[10] + z[11];

    r += n<T>(1,2)*z[10];
 
    return r;
}

template double qqb_2lha_r318(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r318(const std::array<dd_real,30>&);
#endif
