#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r63(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[3];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=z[4]*z[1];
    z[8]= - n<T>(1,2) + z[1];
    z[8]=z[1]*z[8];
    z[9]= - z[2]*npow(z[1],2);
    z[8]=z[9] + z[8] - z[7];
    z[8]=z[2]*z[8];
    z[9]= - z[4] - n<T>(1,3) + z[1];
    z[8]=n<T>(1,2)*z[9] + z[8];
    z[8]=z[2]*z[8];
    z[9]=z[6]*z[5];
    z[10]= - n<T>(5,4) + z[9];
    z[11]=z[5] - 1;
    z[10]=z[11]*z[10];
    z[11]= - n<T>(1,4) + z[9];
    z[11]=z[4]*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[6]*z[10];
    z[10]=n<T>(1,4) + z[10];
    z[8]=n<T>(1,3)*z[10] + n<T>(1,2)*z[8];
    z[8]=z[3]*z[8];
    z[9]=n<T>(7,4) - z[9];
    z[9]=z[6]*z[9];
    z[10]= - z[4]*npow(z[6],2);
    z[9]=z[9] + z[10];
    z[7]=z[1] - z[7];
    z[7]=z[2]*z[7];
    z[7]=z[7] - n<T>(1,4)*z[4] + n<T>(1,4) - z[1];
    z[7]=z[2]*z[7];
    z[7]= - n<T>(1,2) + z[7];
    z[7]=z[2]*z[7];
    z[7]=z[8] + n<T>(1,3)*z[9] + z[7];
    z[7]=z[3]*z[7];
    z[8]=static_cast<T>(1)- z[4];
    z[9]=static_cast<T>(1)- n<T>(1,2)*z[4];
    z[9]=z[4]*z[9];
    z[9]= - n<T>(1,2) + z[9];
    z[9]=z[2]*z[9];
    z[8]=n<T>(1,2)*z[8] + z[9];
    z[8]=z[2]*z[8];
    z[8]= - n<T>(1,2) + z[8];
    z[8]=z[2]*z[8];
    z[7]=z[8] + z[7];

    r += n<T>(1,2)*z[7];
 
    return r;
}

template double qqb_2lha_r63(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r63(const std::array<dd_real,30>&);
#endif
