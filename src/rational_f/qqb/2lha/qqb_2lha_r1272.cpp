#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1272(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[17];
    z[6]=k[8];
    z[7]=k[3];
    z[8]=n<T>(1,2)*z[5];
    z[9]=z[8] - 1;
    z[10]= - n<T>(1,2)*z[4] - z[9];
    z[10]=z[2]*z[10];
    z[8]=z[8] + z[10];
    z[8]=z[2]*z[8];
    z[8]= - n<T>(1,2)*z[6] + z[8];
    z[8]=z[1]*z[8];
    z[10]=z[7]*z[6];
    z[11]=z[5] - 1;
    z[12]= - z[6] - z[11];
    z[8]=z[8] + n<T>(1,2)*z[12] + z[10];
    z[12]=n<T>(1,2)*z[7];
    z[13]= - z[12] - n<T>(1,4)*z[4] + z[11];
    z[14]=n<T>(1,4) - z[7];
    z[14]=z[2]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[13]=z[2]*z[13];
    z[8]=z[13] + n<T>(1,2)*z[8];
    z[8]=z[1]*z[8];
    z[10]=z[6] - z[10];
    z[10]=z[10]*z[12];
    z[9]=z[10] - n<T>(1,4)*z[6] - z[9];
    z[10]=z[12] - 1;
    z[10]=z[10]*z[7];
    z[10]=z[10] + n<T>(1,2);
    z[10]=z[10]*z[2];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[7]*z[12];
    z[12]= - n<T>(5,4) + z[12];
    z[12]=n<T>(1,2)*z[12] + z[10];
    z[12]=z[2]*z[12];
    z[8]=z[8] + n<T>(1,2)*z[9] + z[12];
    z[8]=z[1]*z[8];
    z[9]=z[7] - 1;
    z[10]=z[10] + z[9];
    z[10]=z[10]*z[2];
    z[10]=z[10] + n<T>(1,2);
    z[8]=n<T>(1,2)*z[10] + z[8];
    z[8]=z[3]*z[8];
    z[12]=z[2]*z[7];
    z[12]=z[12] - z[11];
    z[12]=z[1]*z[12];
    z[13]=n<T>(3,2) - z[7];
    z[13]=z[7]*z[13];
    z[13]= - n<T>(1,2) + z[13];
    z[13]=z[2]*z[13];
    z[9]=z[12] + z[13] - z[9];
    z[9]=z[2]*z[9];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[1]*z[9];
    z[9]=z[9] - z[10];
    z[8]=n<T>(1,2)*z[9] + z[8];

    r += n<T>(1,2)*z[8]*z[3];
 
    return r;
}

template double qqb_2lha_r1272(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1272(const std::array<dd_real,30>&);
#endif
