#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r687(const std::array<T,30>& k) {
  T z[39];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[20];
    z[6]=k[8];
    z[7]=k[4];
    z[8]=k[3];
    z[9]=k[10];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=k[13];
    z[13]= - static_cast<T>(47)+ 19*z[1];
    z[14]=n<T>(1,6)*z[2];
    z[15]=z[13]*z[14];
    z[16]= - static_cast<T>(25)+ 11*z[1];
    z[15]=z[15] - z[16];
    z[17]=n<T>(1,2)*z[2];
    z[15]=z[15]*z[17];
    z[18]=n<T>(1,2)*z[7];
    z[19]=7*z[5];
    z[20]=n<T>(11,2)*z[2] - n<T>(25,2) + z[19];
    z[20]=z[20]*z[18];
    z[21]= - static_cast<T>(53)+ 25*z[1];
    z[22]=z[1] - 2;
    z[23]=z[5]*z[22];
    z[15]=z[20] + z[15] + n<T>(1,4)*z[21] - n<T>(7,3)*z[23];
    z[15]=z[7]*z[15];
    z[20]= - static_cast<T>(39)+ n<T>(25,2)*z[2];
    z[20]=z[20]*z[17];
    z[23]=n<T>(1,3)*z[7];
    z[24]=n<T>(25,4) - z[19];
    z[24]=z[24]*z[23];
    z[24]=z[24] - n<T>(25,4)*z[2] + n<T>(53,4) - z[19];
    z[24]=z[7]*z[24];
    z[20]=z[24] + z[20] + n<T>(81,4) - z[19];
    z[20]=z[7]*z[20];
    z[24]=z[7]*z[5];
    z[25]=n<T>(1,4)*z[24] - static_cast<T>(1)+ z[5];
    z[23]=z[25]*z[23];
    z[25]=n<T>(1,2)*z[5];
    z[23]=z[23] + z[17] - static_cast<T>(1)+ z[25];
    z[23]=z[7]*z[23];
    z[26]=n<T>(1,3)*z[5];
    z[27]=z[26] - 1;
    z[28]=n<T>(1,3)*z[2];
    z[29]=static_cast<T>(1)- z[28];
    z[29]=z[2]*z[29];
    z[23]=z[23] + z[29] + z[27];
    z[23]=z[7]*z[23];
    z[29]=n<T>(1,4)*z[2];
    z[30]= - static_cast<T>(1)+ z[29];
    z[30]=z[30]*z[28];
    z[30]=n<T>(1,2) + z[30];
    z[30]=z[2]*z[30];
    z[31]=n<T>(1,4)*z[5];
    z[32]= - static_cast<T>(1)+ z[31];
    z[23]=z[23] + n<T>(1,3)*z[32] + z[30];
    z[23]=z[11]*z[23];
    z[30]=static_cast<T>(103)- 25*z[2];
    z[28]=z[30]*z[28];
    z[28]= - static_cast<T>(53)+ z[28];
    z[28]=z[28]*z[29];
    z[30]=n<T>(109,4) - z[19];
    z[20]=7*z[23] + z[20] + n<T>(1,3)*z[30] + z[28];
    z[20]=z[11]*z[20];
    z[23]=n<T>(1,3)*z[1];
    z[21]= - z[21]*z[23];
    z[21]= - static_cast<T>(27)+ z[21];
    z[28]=n<T>(1,2)*z[1];
    z[30]=z[28] - 1;
    z[30]=z[30]*z[23];
    z[30]=z[30] + n<T>(1,2);
    z[19]=z[30]*z[19];
    z[19]=n<T>(1,2)*z[21] + z[19];
    z[13]= - z[13]*z[23];
    z[13]= - static_cast<T>(25)+ z[13];
    z[21]= - n<T>(11,4) + z[1];
    z[21]=z[21]*z[23];
    z[21]=n<T>(3,2) + z[21];
    z[21]=z[2]*z[21];
    z[13]=n<T>(1,4)*z[13] + z[21];
    z[13]=z[2]*z[13];
    z[21]=z[1]*z[16];
    z[21]=static_cast<T>(39)+ z[21];
    z[13]=n<T>(1,4)*z[21] + z[13];
    z[13]=z[2]*z[13];
    z[13]=z[20] + z[15] + n<T>(1,2)*z[19] + z[13];
    z[13]=z[3]*z[13];
    z[15]=3*z[4];
    z[19]=n<T>(71,3) - z[15];
    z[19]=z[19]*z[28];
    z[19]=z[19] - n<T>(85,3) + n<T>(3,2)*z[4];
    z[20]=z[22]*z[26];
    z[21]=static_cast<T>(19)- n<T>(55,6)*z[1];
    z[20]=n<T>(1,2)*z[21] + z[20];
    z[20]=z[5]*z[20];
    z[21]=13*z[1];
    z[22]= - static_cast<T>(47)+ z[21];
    z[32]= - z[2]*z[9];
    z[22]=n<T>(1,3)*z[22] + z[32];
    z[22]=z[22]*z[29];
    z[22]=z[22] + static_cast<T>(9)- n<T>(29,12)*z[1];
    z[22]=z[22]*z[17];
    z[32]= - n<T>(23,6) + z[15];
    z[33]=n<T>(11,2) - z[5];
    z[33]=z[5]*z[33];
    z[32]=n<T>(11,6)*z[2] + n<T>(1,4)*z[32] + z[33];
    z[32]=z[32]*z[18];
    z[19]=z[32] + z[22] + n<T>(1,4)*z[19] + z[20];
    z[19]=z[7]*z[19];
    z[20]=n<T>(11,4) + z[5];
    z[20]=z[20]*z[26];
    z[22]=z[4] + z[12];
    z[32]= - n<T>(13,3) - z[22];
    z[20]=n<T>(1,8)*z[32] + z[20];
    z[20]=z[7]*z[20];
    z[32]=n<T>(3,8)*z[4];
    z[33]= - n<T>(7,2) + z[5];
    z[33]=z[5]*z[33];
    z[20]=z[20] + n<T>(17,24)*z[2] + z[33] - n<T>(13,3) - z[32];
    z[20]=z[7]*z[20];
    z[33]= - n<T>(39,4) + z[5];
    z[33]=z[5]*z[33];
    z[34]=static_cast<T>(89)+ 5*z[2];
    z[34]=z[2]*z[34];
    z[20]=z[20] + n<T>(1,24)*z[34] + z[33] - n<T>(5,3) - z[32];
    z[20]=z[7]*z[20];
    z[32]= - n<T>(19,4) - z[5];
    z[32]=z[32]*z[26];
    z[33]=z[5] + 11;
    z[34]=z[33]*z[24];
    z[32]= - n<T>(1,12)*z[34] + n<T>(5,2) + z[32];
    z[32]=z[7]*z[32];
    z[34]=n<T>(3,2) - z[5];
    z[34]=z[5]*z[34];
    z[34]=n<T>(21,2) + z[34];
    z[32]=z[32] + n<T>(1,2)*z[34] - 2*z[2];
    z[32]=z[7]*z[32];
    z[34]=n<T>(31,4) - z[5];
    z[34]=z[34]*z[26];
    z[35]= - n<T>(37,2) + z[2];
    z[14]=z[35]*z[14];
    z[14]=z[32] + z[14] + n<T>(47,16) + z[34];
    z[14]=z[7]*z[14];
    z[32]=static_cast<T>(7)- z[25];
    z[32]=z[32]*z[26];
    z[34]=3*z[6];
    z[35]= - n<T>(43,3) + z[34];
    z[36]=n<T>(1,4)*z[6];
    z[37]=z[36] + static_cast<T>(1)+ n<T>(1,4)*z[9];
    z[37]=z[2]*z[37];
    z[37]=z[37] + n<T>(1,3) - n<T>(3,4)*z[6];
    z[37]=z[2]*z[37];
    z[35]=n<T>(1,4)*z[35] + z[37];
    z[35]=z[35]*z[17];
    z[37]=n<T>(1,2)*z[6];
    z[38]=static_cast<T>(1)- z[37];
    z[32]=z[35] + n<T>(1,4)*z[38] + z[32];
    z[14]=n<T>(1,2)*z[32] + z[14];
    z[14]=z[11]*z[14];
    z[32]= - static_cast<T>(16)+ z[5];
    z[32]=z[32]*z[26];
    z[35]=n<T>(13,6) - 15*z[6];
    z[38]= - n<T>(5,2)*z[6] - n<T>(3,2) - z[9];
    z[38]=z[2]*z[38];
    z[38]=z[38] + n<T>(1,3) + n<T>(15,2)*z[6];
    z[38]=z[2]*z[38];
    z[35]=n<T>(1,2)*z[35] + z[38];
    z[35]=z[35]*z[29];
    z[38]=5*z[6] + static_cast<T>(21)- z[4];
    z[14]=z[14] + z[20] + z[35] + n<T>(1,8)*z[38] + z[32];
    z[14]=z[11]*z[14];
    z[16]=z[16]*z[28];
    z[16]=static_cast<T>(16)+ z[16];
    z[20]= - z[30]*z[25];
    z[16]=n<T>(1,3)*z[16] + z[20];
    z[16]=z[5]*z[16];
    z[20]=n<T>(9,4)*z[6] + n<T>(11,3) + n<T>(3,4)*z[9];
    z[30]= - n<T>(13,12) - z[6];
    z[30]=z[1]*z[30];
    z[20]=n<T>(1,2)*z[20] + z[30];
    z[20]=z[20]*z[17];
    z[30]= - n<T>(49,3) - n<T>(27,4)*z[6];
    z[32]=n<T>(3,2)*z[6];
    z[35]=n<T>(5,48)*z[1] + n<T>(8,3) + z[32];
    z[35]=z[1]*z[35];
    z[20]=z[20] + n<T>(1,4)*z[30] + z[35];
    z[20]=z[2]*z[20];
    z[30]=static_cast<T>(55)+ n<T>(27,2)*z[6];
    z[35]=n<T>(5,12)*z[1] - n<T>(16,3) - z[32];
    z[35]=z[1]*z[35];
    z[20]=z[20] + n<T>(1,8)*z[30] + z[35];
    z[20]=z[2]*z[20];
    z[30]=n<T>(1,4)*z[1];
    z[35]= - n<T>(29,2) + z[4];
    z[35]=z[35]*z[30];
    z[38]=n<T>(1,4)*z[4];
    z[35]=z[35] + z[6] + n<T>(37,3) - z[38];
    z[35]=z[1]*z[35];
    z[35]=z[35] - n<T>(9,8)*z[6] - n<T>(43,3) + z[38];
    z[13]=z[13] + z[14] + z[19] + z[20] + n<T>(1,2)*z[35] + z[16];
    z[13]=z[3]*z[13];
    z[14]=29*z[6];
    z[15]= - z[14] - n<T>(173,3) + z[15];
    z[16]= - static_cast<T>(19)+ n<T>(5,2)*z[5];
    z[16]=z[16]*z[26];
    z[19]=z[2] - n<T>(29,2)*z[6] - static_cast<T>(11)- n<T>(3,2)*z[9];
    z[19]=z[2]*z[19];
    z[14]=z[19] + n<T>(235,6) + z[14];
    z[14]=z[14]*z[29];
    z[14]=z[14] + n<T>(1,8)*z[15] + z[16];
    z[15]= - static_cast<T>(3)- z[6];
    z[16]=static_cast<T>(1)+ n<T>(3,8)*z[6];
    z[16]=z[2]*z[16];
    z[15]=n<T>(3,4)*z[15] + z[16];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(5)+ z[32];
    z[19]=n<T>(1,6)*z[5];
    z[20]=static_cast<T>(1)- z[19];
    z[20]=z[5]*z[20];
    z[15]=z[15] + n<T>(1,4)*z[16] + z[20];
    z[16]= - static_cast<T>(7)+ n<T>(5,3)*z[5];
    z[16]=z[16]*z[31];
    z[20]=n<T>(1,2) + z[5];
    z[20]=z[20]*z[24];
    z[16]=n<T>(1,6)*z[20] + n<T>(1,3) + z[16];
    z[16]=z[7]*z[16];
    z[20]= - static_cast<T>(13)+ z[5];
    z[20]=z[5]*z[20];
    z[20]= - n<T>(11,3)*z[2] + n<T>(23,3) + z[20];
    z[16]=n<T>(1,4)*z[20] + z[16];
    z[16]=z[7]*z[16];
    z[20]= - z[5]*z[33];
    z[20]=n<T>(97,4) + z[20];
    z[32]= - n<T>(7,2) + z[2];
    z[32]=z[2]*z[32];
    z[20]=n<T>(1,6)*z[20] + z[32];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[16]=z[7]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[11]*z[15];
    z[16]=n<T>(1,4) - z[5];
    z[16]=z[16]*z[26];
    z[16]=n<T>(1,8)*z[22] + z[16];
    z[16]=z[7]*z[16];
    z[20]=z[2] - n<T>(23,3) + n<T>(7,2)*z[4];
    z[22]=static_cast<T>(2)- z[31];
    z[22]=z[5]*z[22];
    z[16]=z[16] + z[22] + n<T>(1,8)*z[20];
    z[16]=z[7]*z[16];
    z[20]=static_cast<T>(9)- z[2];
    z[20]=z[20]*z[17];
    z[22]= - n<T>(5,2) + z[5];
    z[22]=z[5]*z[22];
    z[20]=z[20] + z[22] - n<T>(15,2) + z[4];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[16]=z[7]*z[16];
    z[14]=z[15] + n<T>(1,2)*z[14] + z[16];
    z[14]=z[11]*z[14];
    z[15]= - n<T>(7,2) + 17*z[6];
    z[15]=z[15]*z[28];
    z[16]=7*z[6];
    z[15]=z[15] + n<T>(1,4) - z[16];
    z[20]= - n<T>(61,3) + 3*z[9];
    z[22]=n<T>(17,2)*z[6];
    z[32]= - n<T>(5,3) - z[22];
    z[32]=z[1]*z[32];
    z[20]=z[32] + n<T>(1,2)*z[20] + z[6];
    z[32]=n<T>(1,6) + z[6];
    z[32]=z[2]*z[32];
    z[20]=n<T>(1,4)*z[20] + z[32];
    z[20]=z[2]*z[20];
    z[15]=n<T>(1,2)*z[15] + z[20];
    z[15]=z[2]*z[15];
    z[20]=n<T>(1,2)*z[4];
    z[22]= - z[22] + n<T>(83,3) - z[20];
    z[22]=n<T>(1,4)*z[22] - z[1];
    z[22]=z[1]*z[22];
    z[32]=n<T>(5,2) - z[1];
    z[32]=z[32]*z[23];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[5]*z[32];
    z[33]= - n<T>(5,3) + z[28];
    z[33]=z[1]*z[33];
    z[32]=z[32] + n<T>(25,2) + 7*z[33];
    z[32]=z[5]*z[32];
    z[33]= - n<T>(71,3) - z[4];
    z[33]=n<T>(1,2)*z[33] + 9*z[6];
    z[15]=z[15] + z[32] + n<T>(1,4)*z[33] + z[22];
    z[22]=static_cast<T>(9)+ z[4];
    z[22]=z[22]*z[30];
    z[22]=z[22] - n<T>(13,3) - z[38];
    z[21]=static_cast<T>(27)- z[21];
    z[30]= - n<T>(3,4) + z[23];
    z[30]=z[5]*z[30];
    z[21]=n<T>(1,4)*z[21] + z[30];
    z[21]=z[5]*z[21];
    z[30]=static_cast<T>(13)+ n<T>(5,2)*z[1];
    z[32]= - n<T>(1,3) - n<T>(5,8)*z[9];
    z[32]=z[2]*z[32];
    z[30]=n<T>(1,6)*z[30] + z[32];
    z[30]=z[30]*z[17];
    z[20]= - z[20] - static_cast<T>(1)- n<T>(1,4)*z[12];
    z[20]=n<T>(1,12)*z[2] + n<T>(1,2)*z[20] + n<T>(4,3)*z[5];
    z[20]=z[7]*z[20];
    z[20]=z[20] + z[30] + n<T>(1,2)*z[22] + z[21];
    z[20]=z[7]*z[20];
    z[13]=z[13] + z[14] + n<T>(1,2)*z[15] + z[20];
    z[13]=z[3]*z[13];
    z[14]= - n<T>(1,2) + z[8];
    z[14]=z[14]*z[26];
    z[14]=z[14] - n<T>(11,6) - z[8];
    z[14]=z[5]*z[14];
    z[15]=z[27]*z[24];
    z[20]=static_cast<T>(3)- z[38];
    z[21]= - z[10]*z[29];
    z[14]=z[15] + z[21] + n<T>(1,2)*z[20] + z[14];
    z[14]=z[14]*z[18];
    z[15]= - static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[15]=z[15]*z[19];
    z[19]=n<T>(1,3)*z[8];
    z[15]=z[15] + n<T>(1,4) - z[19];
    z[15]=z[5]*z[15];
    z[20]=z[2]*z[10];
    z[21]=z[20] - static_cast<T>(1)- z[10];
    z[21]=z[21]*z[29];
    z[22]=n<T>(11,3) - z[38];
    z[14]=z[14] + z[21] + n<T>(1,2)*z[22] + z[15];
    z[14]=z[7]*z[14];
    z[15]= - static_cast<T>(3)+ z[8];
    z[15]=z[15]*z[37];
    z[21]=static_cast<T>(7)+ z[34];
    z[21]=z[21]*z[17];
    z[15]=z[21] + z[15] - static_cast<T>(5)+ z[8];
    z[21]= - z[8]*z[27];
    z[21]=static_cast<T>(3)+ z[21];
    z[21]=z[21]*z[25];
    z[22]= - z[5]*z[27]*z[18];
    z[21]=z[22] - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[18];
    z[22]=static_cast<T>(1)- z[19];
    z[22]=z[22]*z[31];
    z[24]=z[8] - 1;
    z[22]=n<T>(1,3)*z[24] + z[22];
    z[22]=z[5]*z[22];
    z[21]=z[21] + z[29] - n<T>(13,12) + z[22];
    z[21]=z[7]*z[21];
    z[22]=z[5] - 5;
    z[27]=z[22]*z[26];
    z[30]= - static_cast<T>(15)+ z[8];
    z[27]=z[2] + n<T>(1,8)*z[30] + z[27];
    z[21]=n<T>(1,2)*z[27] + z[21];
    z[21]=z[7]*z[21];
    z[15]=n<T>(1,8)*z[15] + z[21];
    z[15]=z[11]*z[15];
    z[21]= - static_cast<T>(19)+ z[10];
    z[27]=z[17]*z[10];
    z[30]=z[27] - z[10];
    z[30]= - z[2]*z[30];
    z[16]=z[30] + n<T>(1,4)*z[21] - z[16];
    z[16]=z[16]*z[29];
    z[21]=static_cast<T>(7)- n<T>(9,4)*z[8];
    z[21]=z[21]*z[36];
    z[22]=z[5]*z[22];
    z[14]=z[15] + z[14] + z[16] + n<T>(1,12)*z[22] + z[21] - n<T>(9,16)*z[8] + 3
    - n<T>(1,16)*z[4];
    z[14]=z[11]*z[14];
    z[15]=z[10] - n<T>(5,12);
    z[16]=n<T>(3,4)*z[20];
    z[20]=n<T>(3,2) - z[26];
    z[20]=z[5]*z[20];
    z[21]=z[7]*z[10];
    z[20]= - n<T>(1,4)*z[21] + z[16] - n<T>(1,2)*z[15] + z[20];
    z[18]=z[20]*z[18];
    z[20]=n<T>(23,3) + n<T>(5,2)*z[8];
    z[21]=n<T>(1,6)*z[1] - n<T>(1,3) - n<T>(1,4)*z[8];
    z[21]=z[5]*z[21];
    z[20]=z[21] + n<T>(1,2)*z[20] - z[1];
    z[20]=z[5]*z[20];
    z[21]= - n<T>(5,6) - z[9];
    z[16]= - z[16] + n<T>(1,2)*z[21] + z[10];
    z[16]=z[16]*z[17];
    z[21]= - z[8]*z[9];
    z[21]=z[21] - z[4] - n<T>(37,3) + z[10];
    z[16]=z[18] + z[16] + n<T>(1,16)*z[21] + z[20];
    z[16]=z[7]*z[16];
    z[18]=z[28] - z[8];
    z[20]=n<T>(5,2) - z[18];
    z[20]=z[20]*z[23];
    z[19]= - static_cast<T>(1)- z[19];
    z[19]=z[8]*z[19];
    z[19]= - n<T>(5,3) + z[19];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[19]*z[25];
    z[20]=n<T>(13,3) + z[8];
    z[20]=z[8]*z[20];
    z[20]=n<T>(53,6) + z[20];
    z[18]= - n<T>(47,12) + z[18];
    z[18]=z[1]*z[18];
    z[18]=z[19] + n<T>(1,2)*z[20] + z[18];
    z[18]=z[5]*z[18];
    z[15]=z[27] + n<T>(17,4)*z[6] - z[15];
    z[15]=z[15]*z[17];
    z[19]= - z[1] + z[24];
    z[19]=z[6]*z[19];
    z[20]= - z[10] - n<T>(53,3) - z[9];
    z[15]=z[15] + n<T>(1,8)*z[20] + z[19];
    z[15]=z[15]*z[17];
    z[17]= - static_cast<T>(1)- n<T>(1,16)*z[9];
    z[17]=z[8]*z[17];
    z[19]=static_cast<T>(1)+ z[37];
    z[19]=z[1]*z[19];

    r +=  - n<T>(133,48) - n<T>(9,16)*z[6] + z[13] + z[14] + z[15] + z[16] + 
      z[17] + z[18] + z[19];
 
    return r;
}

template double qqb_2lha_r687(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r687(const std::array<dd_real,30>&);
#endif
