#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1779(const std::array<T,30>& k) {
  T z[9];
  T r = 0;

    z[1]=k[4];
    z[2]=k[10];
    z[3]=k[11];
    z[4]=k[12];
    z[5]=k[14];
    z[6]=z[3] - 1;
    z[7]=npow(z[2],2);
    z[6]=z[1]*z[7]*z[6];
    z[8]=z[2] + 1;
    z[8]=z[8]*z[3]*z[2];
    z[6]=z[6] + z[8];
    z[7]=z[7] - z[6];
    z[7]=z[4]*z[7];
    z[6]=z[5]*z[6];

    r += n<T>(3,4)*z[6] + z[7] + n<T>(1,4)*z[8];
 
    return r;
}

template double qqb_2lha_r1779(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1779(const std::array<dd_real,30>&);
#endif
