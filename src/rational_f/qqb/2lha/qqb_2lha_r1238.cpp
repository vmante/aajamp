#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1238(const std::array<T,30>& k) {
  T z[56];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[8];
    z[11]=k[10];
    z[12]=k[15];
    z[13]=n<T>(1,4)*z[12];
    z[14]=7*z[12];
    z[15]= - static_cast<T>(13)+ z[14];
    z[15]=z[15]*z[13];
    z[15]=n<T>(1,8)*z[11] + static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];
    z[16]=n<T>(1,4)*z[6];
    z[17]=n<T>(13,6)*z[5] - static_cast<T>(1)- 9*z[12];
    z[17]=z[17]*z[16];
    z[18]=z[6] - 1;
    z[19]=z[18]*z[3];
    z[20]=n<T>(21,2)*z[12];
    z[21]= - static_cast<T>(23)+ z[20];
    z[21]=z[12]*z[21];
    z[21]=n<T>(57,4) + z[21];
    z[15]=n<T>(17,12)*z[19] + z[17] + n<T>(1,4)*z[21] + z[15];
    z[17]=z[4] + n<T>(1,2);
    z[21]=3*z[4];
    z[22]=z[17]*z[21];
    z[22]=z[22] + 1;
    z[23]=z[22]*z[21];
    z[24]=n<T>(1,2)*z[3];
    z[25]=n<T>(39,8) - z[3];
    z[25]=z[25]*z[24];
    z[25]= - static_cast<T>(3)+ z[25];
    z[25]=z[3]*z[25];
    z[26]=z[2]*npow(z[3],3);
    z[27]=n<T>(1,4)*z[26];
    z[25]=z[27] - z[23] + z[25];
    z[25]=z[2]*z[25];
    z[28]=2*z[4];
    z[29]=static_cast<T>(7)- z[28];
    z[29]=z[29]*z[21];
    z[29]=static_cast<T>(10)+ z[29];
    z[29]=z[4]*z[29];
    z[30]= - n<T>(141,8) + z[3];
    z[30]=z[30]*z[24];
    z[30]=static_cast<T>(23)+ z[30];
    z[30]=z[30]*z[24];
    z[25]=z[25] + z[29] + z[30];
    z[25]=z[2]*z[25];
    z[29]=n<T>(1,4)*z[3];
    z[30]= - static_cast<T>(43)+ n<T>(103,12)*z[3];
    z[30]=z[30]*z[29];
    z[31]= - static_cast<T>(17)+ 12*z[4];
    z[31]=z[4]*z[31];
    z[25]=z[25] + z[30] + n<T>(13,16) + z[31];
    z[25]=z[2]*z[25];
    z[30]=15*z[4];
    z[31]=n<T>(9,16)*z[3] + n<T>(91,16) - z[30];
    z[25]=n<T>(1,2)*z[31] + z[25];
    z[25]=z[2]*z[25];
    z[15]=n<T>(1,2)*z[15] + z[25];
    z[15]=z[9]*z[15];
    z[25]=n<T>(1,2)*z[7];
    z[31]= - static_cast<T>(1)+ n<T>(7,8)*z[12];
    z[31]=z[31]*z[12];
    z[32]=z[25] - z[31];
    z[33]=n<T>(1,2)*z[11];
    z[34]=n<T>(3,4)*z[11];
    z[35]=static_cast<T>(1)+ z[34];
    z[35]=z[35]*z[33];
    z[35]=z[35] + z[32];
    z[36]=n<T>(1,2)*z[5];
    z[35]=z[35]*z[36];
    z[37]=z[28] + 1;
    z[37]=z[37]*z[21];
    z[37]=z[37] + 2;
    z[38]=z[37]*z[28];
    z[39]=z[29] - 1;
    z[40]=n<T>(7,2)*z[3];
    z[41]=z[39]*z[40];
    z[41]=static_cast<T>(4)+ z[41];
    z[41]=z[3]*z[41];
    z[42]=n<T>(1,2)*z[26];
    z[38]= - z[42] + z[38] + z[41];
    z[38]=z[2]*z[38];
    z[41]=n<T>(1,8)*z[3];
    z[43]=3*z[3];
    z[44]=n<T>(553,12) - z[43];
    z[44]=z[3]*z[44];
    z[44]= - static_cast<T>(99)+ z[44];
    z[44]=z[44]*z[41];
    z[30]= - n<T>(47,2) + z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(21,2) + z[30];
    z[30]=z[4]*z[30];
    z[30]=z[38] + z[30] + z[44];
    z[30]=z[2]*z[30];
    z[38]=n<T>(1,96)*z[3];
    z[44]=static_cast<T>(863)- 223*z[3];
    z[44]=z[44]*z[38];
    z[45]=9*z[4];
    z[46]= - n<T>(49,2) + z[45];
    z[46]=z[4]*z[46];
    z[46]=n<T>(59,4) + z[46];
    z[46]=z[4]*z[46];
    z[30]=z[30] + z[44] - n<T>(17,16) + z[46];
    z[30]=z[2]*z[30];
    z[44]=static_cast<T>(9)- n<T>(35,4)*z[12];
    z[44]=z[44]*z[13];
    z[46]=n<T>(1,6)*z[6];
    z[47]= - static_cast<T>(17)- n<T>(5,2)*z[5];
    z[47]=z[47]*z[46];
    z[48]=9*z[7];
    z[49]=n<T>(77,2)*z[12] - n<T>(59,3) - z[48];
    z[47]=z[47] + n<T>(1,2)*z[49] - z[5];
    z[47]=z[47]*z[16];
    z[49]=static_cast<T>(25)- n<T>(9,2)*z[6];
    z[49]=n<T>(1,2)*z[49] - z[45];
    z[49]=z[4]*z[49];
    z[50]=static_cast<T>(7)+ 17*z[6];
    z[38]=z[50]*z[38];
    z[15]=z[15] + z[30] + z[38] + z[49] + z[47] + z[35] + n<T>(1,16)*z[11]
    + z[44] - n<T>(91,48) + z[7];
    z[15]=z[9]*z[15];
    z[30]=3*z[7];
    z[35]= - static_cast<T>(11)+ z[20];
    z[35]=z[12]*z[35];
    z[38]= - n<T>(11,6) - 5*z[12];
    z[38]=z[6]*z[38];
    z[35]=z[38] + z[35] + n<T>(5,6) - z[30];
    z[35]=z[35]*z[16];
    z[38]= - n<T>(179,12) + z[30];
    z[44]=n<T>(47,4) - z[14];
    z[44]=z[12]*z[44];
    z[38]=n<T>(1,2)*z[38] + z[44];
    z[32]=z[5]*z[32];
    z[32]=z[35] + n<T>(1,2)*z[38] + z[32];
    z[35]=n<T>(1,3)*z[3];
    z[38]= - n<T>(17,4) + z[35];
    z[38]=z[38]*z[24];
    z[38]=static_cast<T>(3)+ z[38];
    z[38]=z[3]*z[38];
    z[44]=n<T>(1,6)*z[26];
    z[23]= - z[44] + z[23] + z[38];
    z[23]=z[2]*z[23];
    z[38]=4*z[4];
    z[47]= - static_cast<T>(5)+ z[38];
    z[47]=z[47]*z[21];
    z[47]= - n<T>(13,2) + z[47];
    z[47]=z[4]*z[47];
    z[49]=n<T>(3,2)*z[3];
    z[50]= - static_cast<T>(5)+ n<T>(21,16)*z[3];
    z[50]=z[50]*z[49];
    z[23]=z[23] + z[47] + z[50];
    z[23]=z[2]*z[23];
    z[47]= - n<T>(11,2) + z[4];
    z[47]=z[47]*z[21];
    z[47]=n<T>(35,4) + z[47];
    z[47]=z[4]*z[47];
    z[50]=static_cast<T>(1)- n<T>(1,16)*z[3];
    z[40]=z[50]*z[40];
    z[23]=z[23] + z[40] - n<T>(13,24) + z[47];
    z[23]=z[2]*z[23];
    z[40]=n<T>(1,3)*z[6];
    z[47]= - static_cast<T>(31)+ 7*z[6];
    z[47]=z[47]*z[40];
    z[19]=n<T>(7,8)*z[19] + n<T>(57,8) + z[47];
    z[19]=z[19]*z[29];
    z[47]= - z[21] + static_cast<T>(7)- n<T>(3,4)*z[6];
    z[47]=z[4]*z[47];
    z[19]=z[23] + z[19] + n<T>(1,2)*z[32] + z[47];
    z[19]=z[9]*z[19];
    z[23]=z[37]*z[4];
    z[32]=static_cast<T>(3)- z[29];
    z[32]=z[32]*z[24];
    z[32]= - static_cast<T>(2)+ z[32];
    z[32]=z[3]*z[32];
    z[32]=z[44] - z[23] + z[32];
    z[32]=z[2]*z[32];
    z[37]=static_cast<T>(2)- z[21];
    z[37]=z[37]*z[38];
    z[37]=static_cast<T>(3)+ z[37];
    z[37]=z[4]*z[37];
    z[38]=static_cast<T>(29)- n<T>(19,2)*z[3];
    z[38]=z[38]*z[41];
    z[32]=z[32] + z[37] + z[38];
    z[32]=z[2]*z[32];
    z[37]=n<T>(3,2) - z[14];
    z[37]=z[12]*z[37];
    z[37]=z[48] + z[37];
    z[38]=z[40] + n<T>(11,4)*z[12] + n<T>(11,24) - 2*z[7];
    z[38]=z[6]*z[38];
    z[37]=n<T>(1,4)*z[37] + z[38];
    z[37]=z[6]*z[37];
    z[38]=6*z[4];
    z[47]=static_cast<T>(7)- z[38];
    z[47]=z[47]*z[28];
    z[48]=z[6]*z[18];
    z[47]=z[47] - static_cast<T>(3)- 2*z[48];
    z[47]=z[4]*z[47];
    z[48]= - z[18]*z[41];
    z[50]=n<T>(3,2) + n<T>(5,3)*z[6];
    z[50]=z[6]*z[50];
    z[48]=z[48] - n<T>(9,4) + z[50];
    z[48]=z[48]*z[29];
    z[50]=n<T>(17,24) - z[7];
    z[19]=z[19] + z[32] + z[48] + z[47] + z[37] + n<T>(1,2)*z[50] + z[31];
    z[19]=z[9]*z[19];
    z[32]=z[12] - 1;
    z[37]=n<T>(7,2)*z[12];
    z[47]= - z[32]*z[37];
    z[47]=z[30] + z[47];
    z[48]=n<T>(1,2)*z[12];
    z[50]= - static_cast<T>(1)+ n<T>(7,4)*z[12];
    z[50]=z[50]*z[48];
    z[51]= - z[12]*z[16];
    z[50]=z[51] - z[7] + z[50];
    z[50]=z[6]*z[50];
    z[47]=n<T>(1,2)*z[47] + z[50];
    z[47]=z[6]*z[47];
    z[50]=n<T>(13,24) - z[7];
    z[31]=z[47] + n<T>(1,2)*z[50] + z[31];
    z[47]= - static_cast<T>(19)+ 5*z[6];
    z[16]=z[47]*z[16];
    z[16]=static_cast<T>(5)+ z[16];
    z[16]=z[16]*z[40];
    z[40]=n<T>(1,2)*z[6];
    z[47]=z[40] - 1;
    z[47]=z[47]*z[6];
    z[50]=n<T>(1,2) + z[47];
    z[50]=z[50]*z[35];
    z[16]=z[50] - n<T>(1,2) + z[16];
    z[16]=z[16]*z[24];
    z[22]=z[22]*z[4];
    z[50]= - static_cast<T>(1)+ n<T>(21,32)*z[3];
    z[50]=z[3]*z[50];
    z[50]=n<T>(1,24)*z[26] - z[22] + z[50];
    z[50]=z[2]*z[50];
    z[51]=static_cast<T>(1)- z[28];
    z[51]=z[51]*z[21];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[4]*z[51];
    z[52]=n<T>(5,4)*z[3];
    z[50]=z[50] + z[51] + z[52];
    z[50]=z[2]*z[50];
    z[51]=n<T>(3,2) - z[4];
    z[51]=z[51]*z[21];
    z[47]=z[51] - n<T>(3,2) - z[47];
    z[47]=z[4]*z[47];
    z[16]=z[50] + z[16] + n<T>(1,2)*z[31] + z[47];
    z[16]=z[16]*npow(z[9],2);
    z[31]=n<T>(1,3)*z[26];
    z[47]=npow(z[3],2);
    z[50]=n<T>(5,4)*z[47] + z[31];
    z[51]=n<T>(1,2)*z[2];
    z[50]=z[51]*z[50];
    z[50]=z[3] + z[50];
    z[51]=npow(z[11],2);
    z[50]=z[2]*z[51]*z[50];
    z[50]=n<T>(13,24)*z[51] + z[50];
    z[16]=n<T>(1,4)*z[50] + z[16];
    z[16]=z[8]*z[16];
    z[49]= - z[49] + static_cast<T>(5)+ n<T>(13,3)*z[11];
    z[49]=z[29]*z[11]*z[49];
    z[39]=n<T>(17,48)*z[11] - z[39];
    z[39]=z[47]*z[39];
    z[39]=z[39] + z[31];
    z[39]=z[2]*z[11]*z[39];
    z[39]=z[49] + z[39];
    z[39]=z[2]*z[39];
    z[18]=z[24]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[3]*z[11]*z[18];
    z[49]=static_cast<T>(17)+ n<T>(53,2)*z[11];
    z[49]=z[11]*z[49];
    z[18]=n<T>(1,3)*z[49] + z[18];
    z[18]=n<T>(1,8)*z[18] + z[39];
    z[16]=z[16] + n<T>(1,2)*z[18] + z[19];
    z[16]=z[8]*z[16];
    z[18]=n<T>(1,4)*z[11];
    z[19]=n<T>(35,2) + n<T>(17,3)*z[11];
    z[19]=z[19]*z[18];
    z[39]= - static_cast<T>(11)- n<T>(19,12)*z[11];
    z[39]=z[39]*z[24];
    z[19]=z[39] + static_cast<T>(11)+ z[19];
    z[19]=z[19]*z[29];
    z[27]=z[22] - z[27];
    z[39]=static_cast<T>(19)+ n<T>(35,6)*z[11];
    z[39]=n<T>(1,4)*z[39] - z[3];
    z[39]=z[39]*z[29];
    z[39]= - static_cast<T>(1)+ z[39];
    z[39]=z[3]*z[39];
    z[39]=z[39] - z[27];
    z[39]=z[2]*z[39];
    z[49]=static_cast<T>(5)- z[38];
    z[49]=z[4]*z[49];
    z[49]=static_cast<T>(2)+ z[49];
    z[49]=z[4]*z[49];
    z[19]=z[39] + z[49] + z[19];
    z[19]=z[2]*z[19];
    z[39]= - static_cast<T>(179)+ n<T>(109,2)*z[5];
    z[39]=z[5]*z[39];
    z[39]=n<T>(1,12)*z[39] + n<T>(221,12) - z[30];
    z[49]=n<T>(1,4)*z[5];
    z[50]=z[49] - 1;
    z[51]=n<T>(1,3)*z[5];
    z[53]=z[50]*z[51];
    z[53]=n<T>(1,2) + z[53];
    z[53]=z[6]*z[5]*z[53];
    z[39]=n<T>(1,2)*z[39] + z[53];
    z[39]=z[6]*z[39];
    z[20]= - z[20] + n<T>(41,12) + z[30];
    z[20]=n<T>(1,2)*z[20] + z[39];
    z[20]=z[6]*z[20];
    z[39]= - static_cast<T>(19)+ z[14];
    z[39]=z[39]*z[48];
    z[53]=static_cast<T>(53)+ 35*z[11];
    z[53]=z[11]*z[53];
    z[39]=n<T>(1,12)*z[53] + z[39] + n<T>(13,4) - z[7];
    z[53]=static_cast<T>(1)- n<T>(3,2)*z[6];
    z[53]=z[6]*z[53];
    z[54]=n<T>(19,2) - z[45];
    z[54]=z[4]*z[54];
    z[53]=z[54] - n<T>(3,2) + z[53];
    z[53]=z[4]*z[53];
    z[54]=z[34] - 1;
    z[55]=z[6]*z[54];
    z[55]=n<T>(7,3)*z[3] + z[55] - static_cast<T>(9)- n<T>(5,4)*z[11];
    z[55]=z[55]*z[41];
    z[15]=z[16] + z[15] + z[19] + z[55] + z[53] + n<T>(1,4)*z[39] + z[20];
    z[15]=z[8]*z[15];
    z[16]=static_cast<T>(5)- n<T>(11,4)*z[3];
    z[16]=z[16]*z[24];
    z[16]= - static_cast<T>(2)+ z[16];
    z[16]=z[3]*z[16];
    z[16]=z[42] - z[23] + z[16];
    z[16]=z[2]*z[16];
    z[19]= - n<T>(143,6) + 5*z[3];
    z[19]=z[3]*z[19];
    z[19]=n<T>(75,2) + z[19];
    z[19]=z[19]*z[29];
    z[20]=n<T>(31,2) - z[21];
    z[20]=z[4]*z[20];
    z[20]=n<T>(15,2) + z[20];
    z[20]=z[4]*z[20];
    z[16]=z[16] + z[20] + z[19];
    z[16]=z[2]*z[16];
    z[19]= - n<T>(19,4) + z[28];
    z[19]=z[19]*z[21];
    z[20]=n<T>(431,12) - z[43];
    z[20]=z[3]*z[20];
    z[20]= - n<T>(289,3) + z[20];
    z[20]=z[20]*z[41];
    z[16]=z[16] + z[20] + n<T>(17,16) + z[19];
    z[16]=z[2]*z[16];
    z[19]=n<T>(5,6) - z[21];
    z[20]=n<T>(41,3) - n<T>(33,8)*z[3];
    z[20]=z[3]*z[20];
    z[19]=5*z[19] + z[20];
    z[16]=n<T>(1,4)*z[19] + z[16];
    z[16]=z[2]*z[16];
    z[19]= - n<T>(9,4) + z[3];
    z[19]=z[19]*z[24];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[3]*z[19];
    z[19]= - z[44] + z[22] + z[19];
    z[19]=z[2]*z[19];
    z[17]= - z[17]*z[45];
    z[20]=n<T>(93,16) - z[3];
    z[20]=z[3]*z[20];
    z[20]= - static_cast<T>(11)+ z[20];
    z[20]=z[20]*z[24];
    z[17]=z[19] + z[17] + z[20];
    z[17]=z[2]*z[17];
    z[19]=n<T>(1,6)*z[3];
    z[20]= - n<T>(239,16) + z[3];
    z[20]=z[20]*z[19];
    z[20]=static_cast<T>(8)+ z[20];
    z[20]=z[3]*z[20];
    z[22]= - n<T>(1,6) + z[21];
    z[17]=z[17] + n<T>(13,4)*z[22] + z[20];
    z[17]=z[2]*z[17];
    z[20]= - n<T>(115,2) + 17*z[3];
    z[20]=z[20]*z[35];
    z[20]= - n<T>(35,4) + z[20];
    z[17]=n<T>(1,8)*z[20] + z[17];
    z[17]=z[2]*z[17];
    z[20]= - n<T>(49,24)*z[3] + n<T>(53,24) - z[5];
    z[17]=n<T>(1,4)*z[20] + z[17];
    z[17]=z[2]*z[17];
    z[20]=static_cast<T>(1)- z[48];
    z[20]=z[20]*z[37];
    z[22]=static_cast<T>(3)+ z[11];
    z[22]=z[11]*z[22]*z[49];
    z[20]=z[22] + z[20] + z[54];
    z[20]=z[5]*z[20];
    z[22]=static_cast<T>(9)- z[14];
    z[20]=n<T>(1,4)*z[22] + z[20];
    z[17]=n<T>(1,4)*z[20] + z[17];
    z[17]=z[9]*z[17];
    z[14]= - static_cast<T>(11)+ z[14];
    z[13]=z[14]*z[13];
    z[14]=static_cast<T>(1)+ z[33];
    z[14]=z[11]*z[14];
    z[13]=n<T>(5,2)*z[14] + static_cast<T>(1)+ z[13];
    z[13]=z[5]*z[13];
    z[14]=n<T>(15,4)*z[11] - n<T>(19,4) + 15*z[12];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[14]= - n<T>(7,8) + z[51];
    z[14]=z[6]*z[14];
    z[13]=z[17] + z[16] + n<T>(13,48)*z[3] + n<T>(1,4)*z[13] + z[14];
    z[13]=z[9]*z[13];
    z[14]=n<T>(27,8) + n<T>(5,3)*z[11];
    z[14]=z[14]*z[33];
    z[16]=n<T>(1,2)*z[1];
    z[14]=z[16] + z[14] - n<T>(23,8)*z[12] + n<T>(5,4)*z[7] - z[50];
    z[17]=n<T>(235,12)*z[5] - n<T>(189,4) - n<T>(61,3)*z[1];
    z[17]=z[17]*z[36];
    z[17]=z[17] + n<T>(101,12) + 15*z[1];
    z[17]=z[17]*z[36];
    z[20]=n<T>(1,3)*z[1];
    z[22]=z[51] - static_cast<T>(1)- z[20];
    z[22]=z[22]*z[36];
    z[23]=z[1] + 1;
    z[22]=n<T>(1,3)*z[23] + z[22];
    z[22]=z[6]*z[22]*npow(z[5],2);
    z[28]=static_cast<T>(2)- z[7];
    z[28]=z[1]*z[28];
    z[17]=z[22] + z[28] + z[17];
    z[17]=z[6]*z[17];
    z[22]=n<T>(13,3) + z[30];
    z[22]=z[1]*z[22];
    z[22]=z[22] + n<T>(3,8)*z[11] + n<T>(175,12) - z[30];
    z[28]= - n<T>(9,8) - n<T>(2,3)*z[1];
    z[28]=7*z[28] + n<T>(221,48)*z[5];
    z[28]=z[5]*z[28];
    z[17]=z[17] + n<T>(1,4)*z[22] + z[28];
    z[17]=z[6]*z[17];
    z[22]=z[20] - n<T>(17,3) - z[34];
    z[28]=z[1] + n<T>(1,2);
    z[33]=z[28]*z[24];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[33]*z[35];
    z[22]=n<T>(1,4)*z[22] + z[33];
    z[22]=z[22]*z[24];
    z[33]=z[1] + n<T>(7,2);
    z[34]=z[3]*z[33];
    z[34]= - n<T>(61,8) + z[34];
    z[19]=z[34]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[3]*z[19];
    z[19]=z[19] + z[27];
    z[19]=z[2]*z[19];
    z[27]=z[1] + n<T>(3,2);
    z[34]= - z[3]*z[27];
    z[34]=z[34] + n<T>(35,3) + n<T>(3,2)*z[1];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(63,4) + z[34];
    z[34]=z[34]*z[29];
    z[37]= - n<T>(13,2) + z[21];
    z[37]=z[4]*z[37];
    z[37]= - static_cast<T>(3)+ z[37];
    z[37]=z[4]*z[37];
    z[19]=z[19] + z[37] + z[34];
    z[19]=z[2]*z[19];
    z[34]=3*z[1];
    z[37]= - n<T>(29,2) - z[34];
    z[37]=z[3]*z[37];
    z[37]=z[37] + n<T>(103,3) + 3*z[11];
    z[37]=z[37]*z[41];
    z[39]=2*z[1];
    z[43]= - static_cast<T>(3)+ z[39];
    z[43]=z[4]*z[43];
    z[43]=z[43] + static_cast<T>(6)- z[1];
    z[43]=z[4]*z[43];
    z[19]=z[19] + z[37] - n<T>(21,16) + z[43];
    z[19]=z[2]*z[19];
    z[37]=5*z[1];
    z[38]= - z[1]*z[38];
    z[38]=z[38] - static_cast<T>(6)+ z[37];
    z[38]=z[4]*z[38];
    z[43]= - z[6]*z[1];
    z[43]= - n<T>(1,2) + z[43];
    z[43]=z[6]*z[43];
    z[38]=z[38] + n<T>(11,2) + z[43];
    z[38]=z[4]*z[38];
    z[13]=z[15] + z[13] + z[19] + z[22] + z[38] + n<T>(1,2)*z[14] + z[17];
    z[13]=z[8]*z[13];
    z[14]=n<T>(133,4) + z[10];
    z[14]=n<T>(1,3)*z[14] - 7*z[3];
    z[14]=z[3]*z[14];
    z[14]= - static_cast<T>(5)+ z[14];
    z[14]=z[14]*z[29];
    z[15]= - static_cast<T>(1)+ z[52];
    z[15]=z[15]*z[47];
    z[15]=z[15] - z[31];
    z[15]=z[2]*z[15];
    z[14]=z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]=3*z[10];
    z[17]=n<T>(13,3)*z[3] - n<T>(119,12) - z[10];
    z[17]=z[3]*z[17];
    z[17]=z[17] + n<T>(107,12) - z[15];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(17,6) + z[17];
    z[14]=n<T>(1,4)*z[17] + z[14];
    z[14]=z[2]*z[14];
    z[17]=n<T>(97,3) + 27*z[10];
    z[19]= - z[3] + n<T>(31,12) + z[10];
    z[19]=z[3]*z[19];
    z[17]=n<T>(1,4)*z[17] + z[19];
    z[17]=z[3]*z[17];
    z[19]=static_cast<T>(17)- n<T>(19,2)*z[10];
    z[17]=n<T>(1,6)*z[19] + z[17];
    z[14]=n<T>(1,4)*z[17] + z[14];
    z[14]=z[2]*z[14];
    z[17]= - static_cast<T>(29)- 9*z[10];
    z[19]=n<T>(1,3)*z[10];
    z[22]=n<T>(1,4) - z[19];
    z[22]=z[3]*z[22];
    z[17]=n<T>(1,2)*z[17] + z[22];
    z[17]=z[3]*z[17];
    z[17]=n<T>(9,2) + z[17];
    z[14]=n<T>(1,4)*z[17] + z[14];
    z[14]=z[2]*z[14];
    z[17]= - n<T>(15,2) - z[19];
    z[17]=n<T>(1,4)*z[17] + z[3];
    z[17]=z[3]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[24];
    z[22]=n<T>(5,16) - z[35];
    z[22]=z[22]*z[47];
    z[22]=z[22] + n<T>(1,12)*z[26];
    z[22]=z[2]*z[22];
    z[17]=z[17] + z[22];
    z[17]=z[2]*z[17];
    z[22]= - z[35] + static_cast<T>(1)+ n<T>(1,8)*z[10];
    z[22]=z[3]*z[22];
    z[22]=z[22] - static_cast<T>(1)+ n<T>(17,48)*z[10];
    z[22]=z[3]*z[22];
    z[17]=z[17] + n<T>(13,48) + z[22];
    z[17]=z[2]*z[17];
    z[22]= - n<T>(7,2) - z[10];
    z[22]=n<T>(1,2)*z[22] + z[35];
    z[22]=z[3]*z[22];
    z[26]= - static_cast<T>(49)- 43*z[10];
    z[22]=n<T>(1,12)*z[26] + z[22];
    z[22]=z[3]*z[22];
    z[26]=19*z[10];
    z[31]= - static_cast<T>(13)+ z[26];
    z[22]=n<T>(1,12)*z[31] + z[22];
    z[17]=n<T>(1,4)*z[22] + z[17];
    z[17]=z[2]*z[17];
    z[22]=n<T>(1,2) + z[19];
    z[22]=z[22]*z[24];
    z[24]=n<T>(35,12)*z[10];
    z[22]=z[22] + static_cast<T>(9)+ z[24];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(73,12) + z[22];
    z[17]=n<T>(1,4)*z[22] + z[17];
    z[17]=z[2]*z[17];
    z[15]= - n<T>(35,3) - z[15];
    z[15]=z[15]*z[41];
    z[22]=z[24] + 3;
    z[15]=z[15] - n<T>(1,2)*z[22] - z[5];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[2]*z[15];
    z[17]=static_cast<T>(3)+ n<T>(7,3)*z[10];
    z[17]=z[17]*z[18];
    z[17]=z[17] + z[22];
    z[17]=z[17]*z[36];
    z[18]=static_cast<T>(53)+ 23*z[10];
    z[17]=z[17] + n<T>(1,24)*z[18];
    z[17]=z[11]*z[17];
    z[18]=static_cast<T>(1)+ z[24];
    z[17]=n<T>(1,2)*z[18] + z[12] + z[17];
    z[17]=z[17]*z[36];
    z[18]=n<T>(11,4) + z[10];
    z[15]=z[15] + n<T>(1,3)*z[18] + z[17];
    z[15]=z[9]*z[15];
    z[17]=z[19] - 1;
    z[18]=n<T>(13,24)*z[11] - z[17];
    z[18]=z[11]*z[18];
    z[18]= - n<T>(1,2)*z[32] + z[18];
    z[18]=z[5]*z[18];
    z[19]=static_cast<T>(5)+ n<T>(3,2)*z[10];
    z[19]=z[19]*z[41];
    z[22]=n<T>(19,12)*z[10];
    z[24]=n<T>(7,4)*z[11] + static_cast<T>(7)+ z[22];
    z[14]=z[15] + z[14] + z[19] + n<T>(1,4)*z[24] + z[18];
    z[14]=z[9]*z[14];
    z[15]= - static_cast<T>(29)- n<T>(239,3)*z[1];
    z[15]=n<T>(1,2)*z[15] + n<T>(59,3)*z[5];
    z[15]=z[15]*z[49];
    z[18]= - static_cast<T>(23)+ z[11];
    z[19]=static_cast<T>(3)+ n<T>(127,24)*z[1];
    z[19]=z[1]*z[19];
    z[14]=z[14] + z[15] + n<T>(1,48)*z[18] + z[19];
    z[15]= - static_cast<T>(13)+ z[1];
    z[15]=z[15]*z[16];
    z[18]=z[1] - 1;
    z[18]=z[18]*z[1];
    z[19]= - z[21]*z[18];
    z[15]=z[19] + static_cast<T>(3)+ z[15];
    z[15]=z[4]*z[15];
    z[15]=z[15] - n<T>(7,2) + z[1];
    z[15]=z[4]*z[15];
    z[19]=n<T>(71,6) + z[10];
    z[24]=n<T>(7,3) - n<T>(1,4)*z[1];
    z[24]=z[1]*z[24];
    z[27]= - z[1]*z[27];
    z[27]= - n<T>(3,2) + z[27];
    z[27]=z[3]*z[27];
    z[19]=z[27] + n<T>(1,4)*z[19] + z[24];
    z[19]=z[3]*z[19];
    z[22]=z[16] - static_cast<T>(5)+ z[22];
    z[19]=n<T>(1,2)*z[22] + z[19];
    z[19]=z[19]*z[29];
    z[22]=z[33]*z[20];
    z[22]=n<T>(5,2) + z[22];
    z[22]=z[3]*z[22];
    z[24]= - n<T>(13,4)*z[1] - static_cast<T>(11)- n<T>(1,4)*z[10];
    z[22]=n<T>(1,3)*z[24] + z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(5,3) + z[22];
    z[22]=z[3]*z[22];
    z[24]= - n<T>(11,2) - z[1];
    z[24]=z[24]*z[35];
    z[24]=n<T>(11,8) + z[24];
    z[24]=z[24]*z[47];
    z[24]=z[24] + z[42];
    z[24]=z[2]*z[24];
    z[22]=z[22] + z[24];
    z[24]=n<T>(1,4)*z[2];
    z[22]=z[22]*z[24];
    z[15]=z[22] + z[19] + n<T>(5,8) + z[15];
    z[15]=z[2]*z[15];
    z[19]= - n<T>(15,2) - n<T>(19,3)*z[10];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[20]= - n<T>(5,3) - z[10];
    z[22]= - n<T>(17,6) + z[1];
    z[22]=z[1]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[22]=n<T>(5,6) + z[1];
    z[22]=z[1]*z[22];
    z[22]=n<T>(1,3) + z[22];
    z[22]=z[3]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[3]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[19]*z[29];
    z[20]= - static_cast<T>(1)+ z[39];
    z[20]=z[20]*z[21];
    z[20]=z[20] + n<T>(19,2) - z[1];
    z[20]=z[1]*z[20];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[4]*z[20];
    z[22]=static_cast<T>(13)- z[37];
    z[20]=n<T>(1,4)*z[22] + z[20];
    z[20]=z[4]*z[20];
    z[22]= - n<T>(13,3) - z[1];
    z[15]=z[15] + z[19] + n<T>(1,8)*z[22] + z[20];
    z[15]=z[2]*z[15];
    z[19]=npow(z[1],2);
    z[20]=static_cast<T>(1)- z[25];
    z[20]=z[20]*z[19];
    z[22]=static_cast<T>(1)+ n<T>(15,16)*z[1];
    z[22]=z[22]*z[34];
    z[24]= - static_cast<T>(61)- 87*z[1];
    z[24]=n<T>(1,2)*z[24] + 21*z[5];
    z[24]=z[5]*z[24];
    z[22]=n<T>(1,8)*z[24] + n<T>(19,16) + z[22];
    z[22]=z[5]*z[22];
    z[22]=z[19] + z[22];
    z[22]=z[5]*z[22];
    z[23]=z[36] - z[23];
    z[23]=z[5]*z[23];
    z[24]=z[16] + 1;
    z[24]=z[1]*z[24];
    z[23]=z[23] + n<T>(1,2) + z[24];
    z[23]=z[23]*npow(z[5],3)*z[46];
    z[20]=z[23] + z[20] + z[22];
    z[20]=z[6]*z[20];
    z[22]=static_cast<T>(1)- z[30];
    z[22]=n<T>(1,4)*z[22] + z[34];
    z[22]=z[22]*z[16];
    z[23]=n<T>(91,3) + z[11];
    z[24]=n<T>(101,3) + 43*z[1];
    z[24]=z[1]*z[24];
    z[23]=n<T>(1,4)*z[23] + z[24];
    z[24]= - n<T>(131,6) - 41*z[1];
    z[24]=n<T>(1,4)*z[24] + 5*z[5];
    z[24]=z[5]*z[24];
    z[23]=n<T>(1,8)*z[23] + z[24];
    z[23]=z[5]*z[23];
    z[20]=z[20] + z[22] + z[23];
    z[20]=z[6]*z[20];
    z[22]= - z[19]*z[40];
    z[23]=n<T>(1,4) - z[1];
    z[23]=z[1]*z[23];
    z[22]=z[23] + z[22];
    z[22]=z[6]*z[22];
    z[19]= - z[19]*z[21];
    z[21]= - static_cast<T>(3)+ z[16];
    z[21]=z[1]*z[21];
    z[19]=z[21] + z[19];
    z[19]=z[4]*z[19];
    z[18]=n<T>(1,2) - z[18];
    z[18]=z[19] + n<T>(1,2)*z[18] + z[22];
    z[18]=z[4]*z[18];
    z[16]=n<T>(1,3) - z[16];
    z[16]=z[1]*z[16];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[17]= - z[1]*z[28]*z[35];
    z[16]=n<T>(1,2)*z[16] + z[17];
    z[16]=z[3]*z[16];
    z[17]=static_cast<T>(65)+ z[26];
    z[17]=n<T>(1,2)*z[17] - z[37];
    z[16]=n<T>(1,12)*z[17] + z[16];
    z[16]=z[16]*z[29];

    r += z[13] + n<T>(1,2)*z[14] + z[15] + z[16] + z[18] + z[20];
 
    return r;
}

template double qqb_2lha_r1238(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1238(const std::array<dd_real,30>&);
#endif
