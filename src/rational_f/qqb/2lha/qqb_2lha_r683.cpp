#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r683(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[9];
    z[6]=k[3];
    z[7]=k[15];
    z[8]=n<T>(1,3)*z[2];
    z[9]=n<T>(23,2) - z[2];
    z[9]=z[9]*z[8];
    z[10]= - n<T>(11,2) + z[2];
    z[10]=z[10]*z[8];
    z[10]=n<T>(3,2) + z[10];
    z[10]=z[5]*z[10];
    z[9]=z[10] - n<T>(7,2) + z[9];
    z[10]=n<T>(1,2)*z[5];
    z[9]=z[9]*z[10];
    z[11]=n<T>(1,2)*z[7];
    z[12]=static_cast<T>(1)+ n<T>(1,6)*z[7];
    z[12]=z[5]*z[12];
    z[12]=z[12] - n<T>(1,3) - z[11];
    z[12]=z[5]*z[12];
    z[13]=n<T>(1,4)*z[7];
    z[14]=z[13] - 1;
    z[15]=z[4]*npow(z[5],2);
    z[16]=z[7]*z[15];
    z[17]=z[14]*z[16];
    z[12]=z[12] - n<T>(1,3)*z[17];
    z[12]=z[4]*z[12];
    z[12]=z[12] + static_cast<T>(1)+ z[8];
    z[17]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[18]= - z[5]*z[17];
    z[18]=z[18] - n<T>(3,2) + z[8];
    z[18]=z[5]*z[18];
    z[12]=z[18] + n<T>(1,2)*z[12];
    z[12]=z[4]*z[12];
    z[18]=n<T>(1,4)*z[1];
    z[19]=n<T>(1,2)*z[1];
    z[20]=z[19] - 1;
    z[21]=z[2]*z[20];
    z[21]=z[21] - static_cast<T>(1)+ z[18];
    z[21]=z[21]*z[8];
    z[9]=z[12] + z[9] + z[21] + n<T>(2,3) - z[18];
    z[9]=z[4]*z[9];
    z[12]=n<T>(1,3)*z[1];
    z[12]=z[12]*z[20];
    z[12]=z[12] + n<T>(1,2);
    z[18]=z[2]*z[12];
    z[20]=z[20]*z[1];
    z[21]=z[20] + n<T>(3,2);
    z[18]=z[18] - z[21];
    z[18]=z[2]*z[18];
    z[18]=z[18] + z[21];
    z[18]=z[2]*z[18];
    z[12]=z[18] - z[12];
    z[18]=z[10] - 1;
    z[18]=z[5]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[21]=z[2] - 1;
    z[18]=z[21]*z[18];
    z[13]= - z[13]*z[15];
    z[22]=static_cast<T>(1)- z[5];
    z[22]=z[5]*z[22];
    z[13]=z[22] + z[13];
    z[22]=n<T>(1,3)*z[4];
    z[13]=z[13]*z[22];
    z[13]=z[13] + z[18];
    z[13]=z[4]*z[13];
    z[18]=z[1] - 2;
    z[23]=n<T>(1,3)*z[5];
    z[24]= - z[23] + 1;
    z[24]=z[5]*z[24];
    z[18]=n<T>(1,3)*z[18] + z[24];
    z[24]=z[2] - 2;
    z[24]=z[24]*z[2];
    z[24]=z[24] + 1;
    z[18]=z[24]*z[18];
    z[13]=z[13] + z[18];
    z[13]=z[4]*z[13];
    z[18]=n<T>(1,4)*z[5] - 1;
    z[25]=z[8] - 1;
    z[25]=z[25]*z[2];
    z[25]=z[25] + 1;
    z[25]=z[25]*z[2];
    z[25]=z[25] - n<T>(1,3);
    z[18]=z[5]*z[25]*z[18];
    z[12]=z[13] + n<T>(1,2)*z[12] + z[18];
    z[12]=z[3]*z[12];
    z[13]=z[1] - 1;
    z[18]= - z[13]*z[8];
    z[25]=n<T>(1,6)*z[1];
    z[26]=static_cast<T>(1)+ z[25];
    z[26]=z[1]*z[26];
    z[26]= - n<T>(1,6) + z[26];
    z[18]=n<T>(1,2)*z[26] + z[18];
    z[18]=z[2]*z[18];
    z[26]=npow(z[1],2);
    z[26]= - static_cast<T>(5)- z[26];
    z[18]=n<T>(1,6)*z[26] + z[18];
    z[18]=z[2]*z[18];
    z[20]=n<T>(7,2) + z[20];
    z[18]=n<T>(1,6)*z[20] + z[18];
    z[17]=z[2]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[17]=z[5]*z[17];
    z[17]= - 2*z[24] + n<T>(5,4)*z[17];
    z[17]=z[17]*z[23];
    z[9]=z[12] + z[9] + n<T>(1,2)*z[18] + z[17];
    z[9]=z[3]*z[9];
    z[12]= - n<T>(7,3) + z[5];
    z[12]=z[10]*z[21]*z[12];
    z[17]= - z[1] + z[2];
    z[17]=z[17]*z[8];
    z[17]=z[17] - n<T>(1,3) + z[19];
    z[17]=z[2]*z[17];
    z[12]=z[12] - z[25] + z[17];
    z[17]= - n<T>(1,3) + z[11];
    z[17]=z[17]*z[11];
    z[17]= - n<T>(1,3) + z[17];
    z[17]=z[5]*z[17];
    z[18]=z[7] - 1;
    z[19]=z[7]*z[18];
    z[17]= - n<T>(1,3)*z[19] + z[17];
    z[17]=z[5]*z[17];
    z[16]=z[18]*z[16];
    z[16]=z[17] + n<T>(1,6)*z[16];
    z[16]=z[4]*z[16];
    z[17]=n<T>(5,3) - z[11];
    z[14]=n<T>(1,6)*z[2] + z[14];
    z[14]=z[5]*z[14];
    z[14]=n<T>(1,2)*z[17] + z[14];
    z[14]=z[5]*z[14];
    z[17]=z[2] - z[18];
    z[14]=z[16] + n<T>(1,6)*z[17] + z[14];
    z[14]=z[4]*z[14];
    z[16]= - n<T>(3,2) + z[2];
    z[16]=z[5]*z[16];
    z[16]=z[16] + n<T>(7,3) - z[2];
    z[16]=z[16]*z[10];
    z[13]=n<T>(1,2)*z[13] - z[2];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,4) + z[13];
    z[13]=z[14] + n<T>(1,3)*z[13] + z[16];
    z[13]=z[4]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[9]=n<T>(1,2)*z[12] + z[9];
    z[9]=z[3]*z[9];
    z[12]= - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[13]=z[5]*z[12];
    z[13]=n<T>(1,2) + z[13];
    z[13]=z[5]*z[13];
    z[13]=z[13] - n<T>(1,4)*z[15];
    z[14]=npow(z[7],2);
    z[13]=z[22]*z[14]*z[13];
    z[15]= - n<T>(1,2) + n<T>(1,3)*z[6];
    z[15]=z[15]*z[11];
    z[12]=n<T>(1,3)*z[12] + z[15];
    z[12]=z[7]*z[12];
    z[12]=n<T>(1,12) + z[12];
    z[12]=z[5]*z[12];
    z[15]=static_cast<T>(5)- z[6];
    z[15]=z[7]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[7]*z[15];
    z[12]=n<T>(1,12)*z[15] + z[12];
    z[12]=z[5]*z[12];
    z[14]=n<T>(1,2) - z[14];
    z[12]=z[13] + n<T>(1,6)*z[14] + z[12];
    z[12]=z[4]*z[12];
    z[13]=n<T>(1,3) - z[7];
    z[8]=n<T>(1,2)*z[13] - z[8];
    z[13]=z[7]*z[6];
    z[14]= - z[13] + static_cast<T>(7)- z[6];
    z[14]=z[7]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[15]=n<T>(1,4)*z[13] - static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[15]=z[5]*z[7]*z[15];
    z[14]=n<T>(1,4)*z[14] + z[15];
    z[14]=z[14]*z[23];
    z[8]=z[12] + n<T>(1,2)*z[8] + z[14];
    z[8]=z[4]*z[8];
    z[12]= - static_cast<T>(1)+ z[13];
    z[10]=z[12]*z[10];
    z[11]= - z[6]*z[11];
    z[10]=z[10] + static_cast<T>(1)+ z[11];
    z[10]=z[5]*z[10];
    z[11]=z[2]*z[21];
    z[11]= - static_cast<T>(1)+ z[11];
    z[10]=n<T>(1,2)*z[11] + z[10];
    z[8]=n<T>(1,6)*z[10] + z[8];
    z[8]=n<T>(1,2)*z[8] + z[9];

    r += 13*z[8];
 
    return r;
}

template double qqb_2lha_r683(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r683(const std::array<dd_real,30>&);
#endif
