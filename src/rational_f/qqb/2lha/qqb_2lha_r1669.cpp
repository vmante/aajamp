#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1669(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[3];
    z[5]=2*z[3];
    z[6]=z[1] - z[4];
    z[7]=static_cast<T>(1)- z[6];
    z[7]=z[7]*z[5];
    z[7]=z[7] + 5;
    z[7]=z[1]*z[7];
    z[8]=z[4] - 1;
    z[7]=2*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[9]=2*z[1];
    z[9]=z[8]*z[9];
    z[10]=z[3]*npow(z[1],2);
    z[9]=z[9] + z[10];
    z[9]=z[3]*z[9];
    z[10]= - static_cast<T>(2)+ z[4];
    z[10]=z[4]*z[10];
    z[9]=z[9] + static_cast<T>(1)+ z[10];
    z[9]=z[2]*z[9];
    z[7]=2*z[9] + 3*z[8] + z[7];
    z[7]=z[2]*z[7];
    z[5]= - z[6]*z[5];
    z[5]=static_cast<T>(3)+ z[5];
    z[5]=z[3]*z[5];
    z[5]=z[7] + static_cast<T>(1)+ z[5];

    r += z[5]*z[2];
 
    return r;
}

template double qqb_2lha_r1669(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1669(const std::array<dd_real,30>&);
#endif
