#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1239(const std::array<T,30>& k) {
  T z[49];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[11];
    z[9]=k[8];
    z[10]=k[28];
    z[11]=k[15];
    z[12]=k[10];
    z[13]= - static_cast<T>(1)+ n<T>(3,2)*z[11];
    z[14]=9*z[11];
    z[15]=z[13]*z[14];
    z[16]= - z[2]*z[15];
    z[17]=n<T>(1,3)*z[4];
    z[18]=3*z[11];
    z[19]=static_cast<T>(7)- z[18];
    z[19]=z[11]*z[19];
    z[19]= - static_cast<T>(43)+ n<T>(27,2)*z[19];
    z[16]=z[16] + n<T>(1,2)*z[19] + z[17];
    z[19]=n<T>(1,4)*z[2];
    z[16]=z[16]*z[19];
    z[20]=z[12] - 1;
    z[21]=n<T>(3,8)*z[10];
    z[22]=z[20]*z[21];
    z[23]=n<T>(47,3)*z[5];
    z[24]=n<T>(1,2)*z[6];
    z[25]= - n<T>(1,3) - z[24];
    z[25]=z[25]*z[23];
    z[26]=3*z[6];
    z[25]=z[25] + n<T>(331,18) + z[26];
    z[25]=z[6]*z[25];
    z[25]=n<T>(67,9) + z[25];
    z[25]=z[5]*z[25];
    z[27]=n<T>(1,3)*z[3];
    z[28]= - n<T>(625,4) - z[4];
    z[28]=n<T>(1,4)*z[28] + n<T>(38,3)*z[2];
    z[28]=z[28]*z[27];
    z[29]= - static_cast<T>(1)- z[21];
    z[29]=z[6]*z[29];
    z[16]=z[28] + z[16] + z[25] + 5*z[29] + n<T>(1,12)*z[4] + n<T>(81,16)*z[11]
    + z[22] + n<T>(2,9) - n<T>(3,8)*z[12];
    z[16]=z[8]*z[16];
    z[22]=3*z[10];
    z[25]=n<T>(3,2) - z[12];
    z[25]=z[25]*z[22];
    z[28]=3*z[12];
    z[25]=z[25] + n<T>(53,3) + z[28];
    z[29]=n<T>(7,8)*z[6];
    z[25]=z[29] - n<T>(31,12)*z[4] + n<T>(1,2)*z[25] - z[14];
    z[30]=n<T>(9,8)*z[2];
    z[31]=z[18] + 1;
    z[32]=z[31]*z[11];
    z[32]=z[32] + z[4];
    z[30]=z[30]*z[32];
    z[33]= - static_cast<T>(1)+ n<T>(15,8)*z[11];
    z[33]=z[33]*z[14];
    z[34]=n<T>(1,3)*z[12];
    z[33]=z[30] + n<T>(15,8)*z[4] + z[33] + n<T>(3,8) - z[34];
    z[35]=n<T>(1,2)*z[2];
    z[33]=z[33]*z[35];
    z[36]=n<T>(67,3)*z[5];
    z[37]=35*z[4];
    z[38]=n<T>(641,3) - z[37];
    z[38]= - n<T>(313,18)*z[2] + n<T>(1,8)*z[38] - z[36];
    z[38]=z[3]*z[38];
    z[39]= - n<T>(449,3) + 167*z[4];
    z[40]=n<T>(47,6)*z[5];
    z[41]=static_cast<T>(20)- z[40];
    z[41]=z[5]*z[41];
    z[38]=n<T>(1,2)*z[38] + n<T>(1,6)*z[2] + n<T>(1,48)*z[39] + z[41];
    z[38]=z[3]*z[38];
    z[39]=n<T>(47,9)*z[5];
    z[41]=2*z[6];
    z[42]=n<T>(5,2) + z[41];
    z[42]=z[42]*z[39];
    z[42]=z[42] - n<T>(49,2) - 4*z[6];
    z[42]=z[5]*z[42];
    z[16]=z[16] + z[38] + z[33] + n<T>(1,2)*z[25] + z[42];
    z[16]=z[8]*z[16];
    z[15]=z[30] + z[15] - n<T>(1,4)*z[4];
    z[15]=z[15]*z[35];
    z[25]=n<T>(1,3)*z[5];
    z[30]=static_cast<T>(32)- z[40];
    z[30]=z[30]*z[25];
    z[33]= - n<T>(313,3) - n<T>(35,4)*z[4];
    z[33]=n<T>(1,2)*z[33] - z[36];
    z[38]=n<T>(1,6)*z[3];
    z[33]=z[33]*z[38];
    z[40]=n<T>(3,4)*z[10];
    z[42]=n<T>(19,6)*z[4] + n<T>(197,9) - z[40];
    z[30]=z[33] + n<T>(1,2)*z[42] + z[30];
    z[30]=z[3]*z[30];
    z[33]=static_cast<T>(1)+ n<T>(1,2)*z[12];
    z[33]=z[33]*z[40];
    z[42]=n<T>(2,3) + z[24];
    z[23]=z[42]*z[23];
    z[23]=z[23] - n<T>(160,9) - z[26];
    z[23]=z[5]*z[23];
    z[26]=z[18] - 5;
    z[26]=z[26]*z[11];
    z[15]=z[30] + z[15] + z[23] - n<T>(41,48)*z[4] + n<T>(27,16)*z[26] + static_cast<T>(4)+ 
    z[33];
    z[15]=z[8]*z[15];
    z[23]=n<T>(2,9)*z[5];
    z[30]=47*z[5];
    z[33]=static_cast<T>(113)- z[30];
    z[33]=z[33]*z[23];
    z[42]= - n<T>(79,3) + 4*z[4];
    z[36]= - z[36] - z[42];
    z[36]=z[3]*z[36];
    z[43]= - n<T>(403,9) + 19*z[4];
    z[33]=n<T>(2,3)*z[36] + n<T>(1,4)*z[43] + z[33];
    z[33]=z[3]*z[33];
    z[36]= - static_cast<T>(56)+ z[30];
    z[23]=z[36]*z[23];
    z[36]=n<T>(1,2)*z[10] + z[18];
    z[36]=3*z[36] - n<T>(1,6)*z[4];
    z[23]=z[33] + n<T>(1,2)*z[36] + z[23];
    z[23]=z[3]*z[23];
    z[33]=npow(z[11],2);
    z[33]=27*z[33];
    z[36]= - z[12]*z[22];
    z[36]= - z[33] - n<T>(7,12) + z[36];
    z[31]=z[31]*z[14];
    z[43]= - z[31] - n<T>(19,2)*z[4];
    z[44]=n<T>(1,8)*z[2];
    z[43]=z[43]*z[44];
    z[45]=2*z[4];
    z[46]=static_cast<T>(2)- z[39];
    z[46]=z[5]*z[46];
    z[15]=z[15] + z[23] + z[43] + z[46] + n<T>(1,4)*z[36] - z[45];
    z[15]=z[8]*z[15];
    z[23]=z[31] - n<T>(17,3)*z[4];
    z[31]=n<T>(1,9)*z[5];
    z[36]= - n<T>(103,2) + z[30];
    z[36]=z[36]*z[31];
    z[43]=n<T>(47,2)*z[5];
    z[46]=static_cast<T>(76)- z[43];
    z[46]=z[46]*z[25];
    z[45]=z[45] + n<T>(67,6)*z[5];
    z[47]= - z[3]*z[45];
    z[46]=z[47] + n<T>(35,8)*z[4] + z[46];
    z[46]=z[46]*z[27];
    z[23]=z[46] + n<T>(1,16)*z[23] + z[36];
    z[23]=z[3]*z[23];
    z[18]=n<T>(1,2) - z[18];
    z[18]=z[18]*z[14];
    z[18]=z[18] - n<T>(7,2)*z[4];
    z[36]=n<T>(47,18)*z[5];
    z[46]=z[36] - 1;
    z[46]=z[46]*z[5];
    z[32]=z[2]*z[32];
    z[18]=z[23] - n<T>(9,16)*z[32] + n<T>(1,8)*z[18] - z[46];
    z[18]=z[7]*z[18]*npow(z[8],2);
    z[23]= - z[4]*z[35];
    z[23]=z[23] - n<T>(7,6) + z[22];
    z[23]=z[12]*z[23];
    z[32]= - z[3]*z[22];
    z[23]=z[32] + z[23];
    z[15]=z[18] + n<T>(1,8)*z[23] + z[15];
    z[15]=z[7]*z[15];
    z[18]=static_cast<T>(1)+ z[12];
    z[18]=z[18]*z[22];
    z[22]=27*z[11];
    z[23]=9*z[4];
    z[32]= - static_cast<T>(7)- n<T>(5,2)*z[12];
    z[18]= - z[23] - z[22] + n<T>(1,3)*z[32] + z[18];
    z[32]=static_cast<T>(50)- z[43];
    z[25]=z[32]*z[25];
    z[32]= - n<T>(79,6) + z[45];
    z[43]=n<T>(1,3)*z[2];
    z[45]= - static_cast<T>(1)+ z[43];
    z[45]=z[2]*z[45];
    z[45]=static_cast<T>(1)+ z[45];
    z[45]=z[2]*z[45];
    z[45]=n<T>(79,6)*z[45] - z[32];
    z[45]=z[3]*z[45];
    z[47]= - static_cast<T>(81)+ n<T>(79,3)*z[4];
    z[48]= - static_cast<T>(1)+ z[35];
    z[48]=z[2]*z[48];
    z[25]=z[45] + n<T>(77,36)*z[48] + n<T>(1,8)*z[47] + z[25];
    z[25]=z[3]*z[25];
    z[45]= - n<T>(121,2) + z[30];
    z[45]=z[45]*z[31];
    z[47]=static_cast<T>(29)+ n<T>(13,6)*z[4];
    z[25]=z[25] - z[2] + n<T>(1,8)*z[47] + z[45];
    z[25]=z[3]*z[25];
    z[45]= - static_cast<T>(9)- z[12];
    z[45]=z[4]*z[45];
    z[33]= - z[33] + z[45];
    z[33]=z[33]*z[44];
    z[15]=z[15] + z[16] + z[25] + z[33] + n<T>(1,8)*z[18] - z[46];
    z[15]=z[7]*z[15];
    z[16]=17*z[6] - z[22] + n<T>(83,3) + z[28];
    z[18]=n<T>(9,4)*z[26] + n<T>(9,2) - z[34];
    z[18]=z[2]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[16]=z[16]*z[19];
    z[18]=n<T>(9,4)*z[10];
    z[22]=n<T>(7,3) - z[18];
    z[25]=static_cast<T>(2)+ n<T>(3,2)*z[10];
    z[25]=z[6]*z[25];
    z[22]=n<T>(1,2)*z[22] + z[25];
    z[22]=z[6]*z[22];
    z[25]= - n<T>(19,3) - z[6];
    z[25]=z[25]*npow(z[6],2);
    z[26]=npow(z[6],3)*z[36];
    z[25]=z[25] + z[26];
    z[25]=z[5]*z[25];
    z[20]= - z[10] - z[20];
    z[16]=z[16] + z[25] + n<T>(3,8)*z[20] + z[22];
    z[16]=z[8]*z[16];
    z[18]= - z[29] - n<T>(11,3) - z[18];
    z[18]=z[6]*z[18];
    z[20]=n<T>(9,2)*z[10] - n<T>(653,36) + z[28];
    z[17]=z[18] + n<T>(1,2)*z[20] + z[17];
    z[18]= - n<T>(1,2) - z[6];
    z[18]=z[18]*z[39];
    z[18]=z[18] + n<T>(217,18) + z[41];
    z[18]=z[6]*z[18];
    z[18]=n<T>(67,18) + z[18];
    z[18]=z[5]*z[18];
    z[13]=z[13]*z[2];
    z[13]= - n<T>(9,2)*z[13] + n<T>(63,4);
    z[13]=z[11]*z[13];
    z[13]= - n<T>(3,4)*z[6] - static_cast<T>(11)- n<T>(7,12)*z[12] + z[13];
    z[13]=z[13]*z[19];
    z[19]= - n<T>(313,12)*z[2] + static_cast<T>(1)- z[4];
    z[19]=z[19]*z[38];
    z[13]=z[16] + z[19] + z[13] + n<T>(1,2)*z[17] + z[18];
    z[13]=z[8]*z[13];
    z[16]=n<T>(79,3)*z[2];
    z[17]=z[2] - static_cast<T>(2)- z[1];
    z[17]=z[2]*z[17];
    z[17]=z[1] + z[17];
    z[17]=z[2]*z[17];
    z[17]=z[1] + z[17];
    z[17]=z[17]*z[16];
    z[18]=z[5]*z[1];
    z[19]= - z[1]*z[42];
    z[17]=z[17] + z[19] - n<T>(67,3)*z[18];
    z[17]=z[17]*z[27];
    z[19]=n<T>(481,3) - z[37];
    z[20]= - static_cast<T>(7)+ n<T>(11,3)*z[4];
    z[20]=z[1]*z[20];
    z[19]=n<T>(1,12)*z[19] + z[20];
    z[20]= - z[30] + 74;
    z[20]=z[1]*z[20];
    z[20]= - static_cast<T>(47)+ z[20];
    z[20]=z[20]*z[31];
    z[22]=n<T>(481,4) + 55*z[1];
    z[25]= - n<T>(2641,36) - 43*z[1];
    z[25]=n<T>(1,2)*z[25] + n<T>(65,3)*z[2];
    z[25]=z[2]*z[25];
    z[22]=n<T>(1,9)*z[22] + z[25];
    z[22]=z[22]*z[35];
    z[17]=z[17] + z[22] + n<T>(1,2)*z[19] + z[20];
    z[17]=z[3]*z[17];
    z[19]= - n<T>(223,3) + 91*z[4];
    z[20]= - static_cast<T>(1)+ 5*z[4];
    z[20]=z[1]*z[20];
    z[19]=n<T>(1,6)*z[19] + z[20];
    z[20]=n<T>(10,9) - z[1];
    z[20]=z[5]*z[20];
    z[22]=n<T>(11,6)*z[2] - n<T>(59,2) - n<T>(1,3)*z[1];
    z[22]=z[2]*z[22];
    z[17]=z[17] + n<T>(1,24)*z[22] + n<T>(1,8)*z[19] + z[20];
    z[17]=z[3]*z[17];
    z[19]= - n<T>(7,3) - z[23];
    z[19]=z[1]*z[19];
    z[19]=z[19] + n<T>(7,3)*z[6] + n<T>(5,2) + z[34];
    z[20]=static_cast<T>(1)+ z[6];
    z[20]=z[20]*z[36];
    z[20]=z[20] - n<T>(103,18) - z[6];
    z[20]=z[5]*z[20];
    z[22]=static_cast<T>(9)- z[12];
    z[22]=z[4]*z[22];
    z[14]=z[22] + n<T>(13,3) - z[14];
    z[14]=z[2]*z[14];
    z[13]=z[15] + z[13] + z[17] + n<T>(1,16)*z[14] + n<T>(1,8)*z[19] + z[20];
    z[13]=z[7]*z[13];
    z[14]=n<T>(1,8)*z[1];
    z[15]=n<T>(107,2) + n<T>(697,3)*z[1];
    z[15]=z[15]*z[14];
    z[17]= - n<T>(251,16) - 35*z[1];
    z[17]=5*z[17] + n<T>(703,8)*z[2];
    z[17]=z[17]*z[43];
    z[15]=z[15] + z[17];
    z[15]=z[2]*z[15];
    z[17]=n<T>(107,2) + n<T>(461,3)*z[1];
    z[17]=z[17]*z[14];
    z[15]=z[17] + z[15];
    z[15]=z[15]*z[43];
    z[17]=npow(z[1],2);
    z[19]= - z[17]*z[32];
    z[20]=n<T>(1,2)*z[17];
    z[22]=z[35] - n<T>(1,2) - z[1];
    z[22]=z[2]*z[22];
    z[22]=z[20] + z[22];
    z[22]=z[2]*z[22];
    z[22]=z[20] + z[22];
    z[22]=z[2]*z[22];
    z[20]=z[20] + z[22];
    z[16]=z[20]*z[16];
    z[16]=z[16] + z[19];
    z[16]=z[16]*z[27];
    z[19]=static_cast<T>(107)- z[37];
    z[20]=static_cast<T>(25)+ 3*z[4];
    z[20]=z[1]*z[20];
    z[19]=n<T>(1,6)*z[19] + z[20];
    z[14]=z[19]*z[14];
    z[17]= - z[17]*z[36];
    z[19]=n<T>(2,9)*z[1];
    z[20]= - n<T>(3,2) - z[19];
    z[20]=z[1]*z[20];
    z[17]=z[20] + z[17];
    z[17]=z[5]*z[17];
    z[14]=z[16] + z[15] + z[14] + z[17];
    z[14]=z[3]*z[14];
    z[15]=n<T>(763,6)*z[2] - static_cast<T>(151)- n<T>(1505,6)*z[1];
    z[15]=z[15]*z[35];
    z[16]=n<T>(209,2) + 73*z[1];
    z[16]=z[1]*z[16];
    z[16]= - static_cast<T>(91)+ n<T>(5,2)*z[16];
    z[15]=n<T>(1,3)*z[16] + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] - n<T>(91,3) - z[4];
    z[16]=n<T>(329,9) + n<T>(5,4)*z[4];
    z[17]= - n<T>(10,9) + n<T>(9,8)*z[4];
    z[17]=z[1]*z[17];
    z[16]=n<T>(1,4)*z[16] + z[17];
    z[16]=z[1]*z[16];
    z[17]=n<T>(1,2) - z[1];
    z[17]=z[17]*z[18];
    z[18]= - n<T>(34,3) + n<T>(11,2)*z[1];
    z[18]=z[1]*z[18];
    z[17]=n<T>(47,9)*z[17] + n<T>(47,18) + z[18];
    z[17]=z[5]*z[17];
    z[14]=z[14] + z[17] + z[16] + n<T>(1,12)*z[15];
    z[14]=z[3]*z[14];
    z[15]= - n<T>(7,4) - z[9];
    z[15]=n<T>(7,48)*z[6] + n<T>(1,3)*z[15] + z[21];
    z[15]=z[6]*z[15];
    z[16]=n<T>(5,2) + 2*z[9];
    z[15]=z[15] + n<T>(1,3)*z[16] - z[40];
    z[15]=z[6]*z[15];
    z[16]=z[9] + n<T>(5,16);
    z[17]=z[6]*z[9];
    z[17]= - n<T>(7,4) + z[17];
    z[17]=z[6]*z[17];
    z[17]=z[16] - z[17];
    z[16]= - z[16]*z[43];
    z[16]=z[16] + n<T>(3,8);
    z[16]=z[12]*z[16];
    z[16]=z[16] - n<T>(1,3)*z[17];
    z[16]=z[2]*z[16];
    z[17]= - z[21] + n<T>(1,3)*z[9];
    z[18]=z[6]*z[17];
    z[18]=z[18] + z[40] - n<T>(17,16) - n<T>(2,3)*z[9];
    z[18]=z[6]*z[18];
    z[17]=z[18] + n<T>(11,16) + z[17];
    z[17]=z[6]*z[17];
    z[16]=z[16] + n<T>(3,8) + z[17];
    z[16]=z[8]*z[16];
    z[17]= - static_cast<T>(7)+ z[6];
    z[17]=z[17]*z[24];
    z[17]= - n<T>(7,3)*z[12] + z[17];
    z[17]=z[17]*z[44];
    z[18]= - n<T>(19,16) - z[9];
    z[15]=z[16] + z[17] + z[15] + n<T>(1,3)*z[18] + z[21];
    z[15]=z[8]*z[15];
    z[16]=n<T>(7,6)*z[6] + n<T>(541,18) + z[23];
    z[16]=n<T>(1,8)*z[16] - z[19];
    z[16]=z[1]*z[16];
    z[17]=static_cast<T>(1)- z[1];
    z[17]=z[17]*z[36];
    z[17]=z[17] - n<T>(65,9) + 2*z[1];
    z[17]=z[1]*z[17];
    z[17]=n<T>(47,18) + z[17];
    z[17]=z[5]*z[17];
    z[18]= - n<T>(11,18)*z[2] + n<T>(31,18)*z[1] - n<T>(137,9) - z[24];
    z[18]=z[18]*z[44];
    z[19]=static_cast<T>(1)- n<T>(1,4)*z[6];
    z[19]=z[6]*z[19];
    z[19]= - n<T>(61,12) + z[19];

    r += z[13] + z[14] + z[15] + z[16] + z[17] + z[18] + n<T>(7,12)*z[19];
 
    return r;
}

template double qqb_2lha_r1239(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1239(const std::array<dd_real,30>&);
#endif
