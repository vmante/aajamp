#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r479(const std::array<T,30>& k) {
  T z[52];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[7];
    z[9]=k[8];
    z[10]=k[11];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=z[6] + 1;
    z[14]=n<T>(1,4)*z[5];
    z[15]=z[13]*z[14];
    z[16]=n<T>(1,3)*z[8];
    z[17]=z[16] + n<T>(1,4);
    z[17]=z[17]*z[6];
    z[18]=n<T>(1,4)*z[4];
    z[15]=z[17] + z[15] - z[18];
    z[17]=z[2]*z[10];
    z[19]= - n<T>(1,4)*z[17] + n<T>(25,12) + z[15];
    z[19]=z[11]*z[19];
    z[20]=z[6]*z[8];
    z[21]= - n<T>(1,4) + z[20];
    z[21]=n<T>(1,3)*z[21] - z[14];
    z[21]=z[11]*z[21];
    z[21]= - n<T>(1,4)*z[10] + z[21];
    z[21]=z[7]*z[21];
    z[22]=n<T>(1,2)*z[10];
    z[23]=n<T>(1,2)*z[5];
    z[24]=n<T>(1,2) - z[8];
    z[19]=z[21] + z[19] - n<T>(3,4)*z[17] - z[23] - z[22] + n<T>(1,3)*z[24] + 
    z[20];
    z[19]=z[11]*z[19];
    z[19]= - z[10] + z[19];
    z[21]=n<T>(1,2)*z[7];
    z[19]=z[19]*z[21];
    z[24]=n<T>(1,8)*z[2];
    z[25]= - 7*z[10] - 3*z[17];
    z[25]=z[25]*z[24];
    z[26]=static_cast<T>(5)- z[10];
    z[26]=n<T>(1,2)*z[26] - z[17];
    z[26]=z[2]*z[26];
    z[26]=z[26] + z[5] - z[4] - n<T>(5,3) - n<T>(3,2)*z[6];
    z[27]=n<T>(1,4)*z[11];
    z[26]=z[26]*z[27];
    z[28]=n<T>(23,2) - z[8];
    z[15]=z[26] + z[25] - n<T>(5,8)*z[10] + n<T>(1,6)*z[28] + z[15];
    z[15]=z[11]*z[15];
    z[25]=n<T>(1,8)*z[5];
    z[16]= - n<T>(1,2) - z[16];
    z[26]=z[6] - 1;
    z[28]=z[10]*z[26];
    z[15]=z[19] + z[15] - n<T>(3,8)*z[17] - z[25] + n<T>(3,4)*z[28] + n<T>(1,2)*
    z[16] + n<T>(1,3)*z[20];
    z[15]=z[7]*z[15];
    z[16]=z[6] + n<T>(3,2);
    z[19]=n<T>(1,2)*z[4];
    z[28]= - z[19] - z[16];
    z[29]=z[17] + z[10];
    z[30]=n<T>(1,4)*z[2];
    z[29]=z[29]*z[30];
    z[29]=z[29] + z[10];
    z[31]= - n<T>(1,4) - z[29];
    z[31]=z[2]*z[31];
    z[28]=n<T>(1,2)*z[28] + z[31];
    z[28]=z[11]*z[28];
    z[31]=z[19] + 1;
    z[32]=n<T>(1,2)*z[6];
    z[33]= - z[32] - z[31];
    z[34]=n<T>(1,2)*z[2];
    z[35]= - static_cast<T>(3)- z[34];
    z[35]=z[34]*z[35];
    z[35]=z[35] - n<T>(7,4);
    z[35]=z[10]*z[35];
    z[35]=static_cast<T>(1)+ z[35];
    z[35]=z[2]*z[35];
    z[28]=z[28] + z[35] + z[14] + n<T>(3,2)*z[33] - z[10];
    z[33]=n<T>(1,2)*z[11];
    z[28]=z[28]*z[33];
    z[35]=n<T>(13,4) - z[8];
    z[36]=static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[36]=z[6]*z[36];
    z[35]=n<T>(1,2)*z[35] + z[36];
    z[36]=n<T>(3,2) - z[6];
    z[36]=z[6]*z[36];
    z[36]= - n<T>(5,2) + z[36];
    z[36]=z[36]*z[22];
    z[13]=z[13]*z[25];
    z[37]= - static_cast<T>(5)+ z[6];
    z[37]=z[10]*z[37];
    z[17]= - z[17] - n<T>(7,3) + z[37];
    z[17]=z[17]*z[24];
    z[20]=z[20] - n<T>(1,2) - z[8];
    z[20]=z[1]*z[20];
    z[13]=z[15] + z[28] + z[17] + n<T>(1,6)*z[20] + z[13] + n<T>(1,3)*z[35] + 
    z[36];
    z[13]=z[7]*z[13];
    z[15]=3*z[4];
    z[17]= - 3*z[5] + static_cast<T>(7)- z[15];
    z[20]=n<T>(23,3) - 5*z[2];
    z[20]=z[2]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[17]=z[17]*z[33];
    z[20]=z[5] + z[4];
    z[28]=n<T>(3,2)*z[2];
    z[35]=n<T>(7,3) - z[28];
    z[35]=z[2]*z[35];
    z[17]=z[17] + z[35] + n<T>(17,3) - n<T>(9,4)*z[20];
    z[17]=z[11]*z[17];
    z[35]=z[2] - 1;
    z[36]= - z[35] - n<T>(3,2)*z[20];
    z[17]=n<T>(1,2)*z[36] + z[17];
    z[36]=z[4] + 1;
    z[37]= - z[34] - z[5] - z[36];
    z[38]= - n<T>(7,4)*z[2] + n<T>(4,3) - n<T>(3,8)*z[20];
    z[38]=z[11]*z[38];
    z[37]=n<T>(1,2)*z[37] + z[38];
    z[37]=z[11]*z[37];
    z[38]=z[23] + z[19];
    z[39]= - static_cast<T>(3)- z[38];
    z[40]=npow(z[11],2);
    z[41]=z[40]*z[7];
    z[39]=z[39]*z[41];
    z[37]=z[37] + n<T>(1,4)*z[39];
    z[37]=z[7]*z[37];
    z[17]=n<T>(1,2)*z[17] + z[37];
    z[17]=z[7]*z[17];
    z[37]=z[38] - 1;
    z[39]=static_cast<T>(1)- z[20];
    z[42]=n<T>(5,3) - z[34];
    z[42]=z[2]*z[42];
    z[42]=n<T>(5,4) + z[42];
    z[42]=z[2]*z[42];
    z[39]=n<T>(1,4)*z[39] + z[42];
    z[39]=z[11]*z[39];
    z[42]=n<T>(17,6) - z[2];
    z[42]=z[2]*z[42];
    z[42]=n<T>(73,12) + z[42];
    z[42]=z[2]*z[42];
    z[39]=z[39] - 3*z[37] + z[42];
    z[39]=z[39]*z[33];
    z[42]=n<T>(1,2)*z[1];
    z[43]=z[42] + 1;
    z[44]= - z[37]*z[43];
    z[45]=3*z[1];
    z[46]=n<T>(29,3) + z[45];
    z[47]=z[2] - static_cast<T>(3)- z[1];
    z[47]=z[2]*z[47];
    z[46]=n<T>(1,2)*z[46] + z[47];
    z[46]=z[46]*z[34];
    z[17]=z[17] + z[39] + z[46] + z[44];
    z[17]=z[7]*z[17];
    z[39]=n<T>(1,2)*z[9];
    z[44]=z[12] - 1;
    z[46]=z[39] + z[44];
    z[46]=z[2]*z[46];
    z[47]=3*z[12];
    z[46]=z[46] + n<T>(19,3) - z[47];
    z[46]=z[2]*z[46];
    z[46]=z[46] - n<T>(1,2) + z[47];
    z[46]=z[2]*z[46];
    z[48]=3*z[44];
    z[49]=z[2]*z[12];
    z[49]= - z[48] + z[49];
    z[49]=z[2]*z[49];
    z[48]=z[48] + z[49];
    z[48]=z[2]*z[48];
    z[48]=z[48] - z[44];
    z[48]=z[48]*z[33];
    z[38]=z[48] + z[46] + n<T>(3,2) - z[12] - z[38];
    z[38]=z[38]*z[27];
    z[46]=npow(z[2],2);
    z[48]=z[46]*z[33];
    z[49]=n<T>(1,2) + z[2];
    z[49]=z[2]*z[49];
    z[48]=z[49] + z[48];
    z[48]=z[11]*z[48];
    z[49]=z[11] + 1;
    z[49]=z[2]*z[49];
    z[49]=n<T>(1,3) + z[49];
    z[49]=z[11]*z[49];
    z[41]=z[49] + n<T>(1,3)*z[41];
    z[41]=z[41]*z[21];
    z[41]=z[41] + z[34] + z[48];
    z[41]=z[7]*z[41];
    z[48]=n<T>(1,3)*z[1];
    z[49]=z[48] + 1;
    z[50]=n<T>(1,3)*z[2] - z[49];
    z[50]=z[50]*z[34];
    z[43]=z[50] + z[43];
    z[43]=z[43]*z[46];
    z[50]=static_cast<T>(1)+ z[2];
    z[46]=z[50]*z[46];
    z[50]=npow(z[2],3);
    z[51]=z[50]*z[11];
    z[46]=z[46] + n<T>(1,3)*z[51];
    z[46]=z[46]*z[33];
    z[41]=z[41] + z[43] + z[46];
    z[41]=z[7]*z[41];
    z[43]=z[1]*z[49];
    z[43]=static_cast<T>(1)+ z[43];
    z[46]=n<T>(1,6)*z[2] - n<T>(1,2) - z[48];
    z[46]=z[2]*z[46];
    z[43]=n<T>(1,2)*z[43] + z[46];
    z[43]=z[43]*z[50];
    z[41]=z[41] + z[43] + n<T>(1,6)*z[51];
    z[41]=z[3]*z[41];
    z[43]=n<T>(1,2)*z[12];
    z[46]= - static_cast<T>(9)+ z[39];
    z[46]=z[1]*z[46];
    z[49]=static_cast<T>(5)- z[39];
    z[49]=z[2]*z[49];
    z[46]=z[49] + z[46] + z[43] - n<T>(77,6) + z[9];
    z[30]=z[46]*z[30];
    z[46]=n<T>(61,3) - z[47];
    z[47]=n<T>(7,3) + z[1];
    z[47]=z[1]*z[47];
    z[30]=z[30] + n<T>(1,8)*z[46] + z[47];
    z[30]=z[2]*z[30];
    z[46]=z[1] + 1;
    z[46]=z[46]*z[1];
    z[46]=z[46] + 1;
    z[47]=n<T>(3,2)*z[12] + z[46];
    z[30]=n<T>(1,4)*z[47] + z[30];
    z[30]=z[2]*z[30];
    z[37]= - z[37]*z[46];
    z[37]= - z[43] + z[37];
    z[17]=n<T>(5,2)*z[41] + z[17] + z[38] + n<T>(1,4)*z[37] + z[30];
    z[17]=z[3]*z[17];
    z[30]=static_cast<T>(3)+ 5*z[9];
    z[24]= - z[24] + n<T>(1,8)*z[30] + z[12];
    z[24]=z[2]*z[24];
    z[24]=z[24] + n<T>(61,24) - 2*z[12];
    z[24]=z[2]*z[24];
    z[30]=z[12] + n<T>(1,4);
    z[37]=z[30]*z[34];
    z[37]=z[37] - z[44];
    z[37]=z[2]*z[37];
    z[37]=n<T>(1,2)*z[44] + z[37];
    z[37]=z[11]*z[37];
    z[38]=z[12] - n<T>(1,2);
    z[24]=z[37] + z[24] - z[25] + z[18] + z[38];
    z[24]=z[11]*z[24];
    z[25]=n<T>(3,2) - z[5];
    z[25]=z[1]*z[25];
    z[23]=z[25] - z[23] + 1;
    z[23]=z[23]*z[42];
    z[18]=z[23] - z[14] + z[18] + z[30];
    z[23]=n<T>(3,4)*z[2];
    z[25]= - z[35]*z[23];
    z[30]= - 13*z[2] + n<T>(1,3) + 5*z[4];
    z[27]=z[30]*z[27];
    z[25]=z[27] + z[25] + z[14] - static_cast<T>(3)+ n<T>(5,2)*z[4];
    z[25]=z[11]*z[25];
    z[27]=z[5] + z[31];
    z[27]=z[27]*z[40]*z[21];
    z[15]=n<T>(11,3) + z[15];
    z[23]=z[23] + n<T>(3,4)*z[5] - n<T>(7,2) + z[4];
    z[23]=z[11]*z[23];
    z[15]=z[23] + n<T>(1,4)*z[15] + z[5];
    z[15]=z[11]*z[15];
    z[15]=z[15] + z[27];
    z[15]=z[7]*z[15];
    z[20]=n<T>(5,6) + z[20];
    z[15]=z[15] + n<T>(1,2)*z[20] + z[25];
    z[15]=z[7]*z[15];
    z[20]= - static_cast<T>(1)- z[34];
    z[20]=z[2]*z[20];
    z[20]=n<T>(13,6) + z[20];
    z[20]=z[20]*z[34];
    z[20]=z[20] - z[14] + z[31];
    z[20]=z[11]*z[20];
    z[23]= - static_cast<T>(7)- z[2];
    z[23]=z[23]*z[34];
    z[23]=n<T>(13,3) + z[23];
    z[23]=z[23]*z[34];
    z[25]=n<T>(11,3) + n<T>(9,2)*z[4];
    z[20]=z[20] + z[23] + n<T>(1,2)*z[25] - z[5];
    z[20]=z[11]*z[20];
    z[23]=z[42] + n<T>(3,2);
    z[23]=z[36]*z[23];
    z[23]= - z[5] + z[23];
    z[25]= - n<T>(5,12)*z[2] - n<T>(1,4) - z[48];
    z[25]=z[2]*z[25];
    z[15]=z[15] + z[20] + n<T>(1,2)*z[23] + z[25];
    z[15]=z[15]*z[21];
    z[20]= - n<T>(85,3) + 13*z[9];
    z[20]=n<T>(1,4)*z[20] + z[12];
    z[21]= - n<T>(17,8) + z[9];
    z[21]=z[1]*z[21];
    z[23]=n<T>(5,4) - z[9];
    z[23]=z[2]*z[23];
    z[20]=z[23] + n<T>(1,2)*z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]=n<T>(43,6) + z[45];
    z[21]=z[1]*z[21];
    z[20]=z[20] + n<T>(1,4)*z[21] + n<T>(77,24) - z[12];
    z[20]=z[2]*z[20];
    z[15]=z[17] + z[15] + z[24] + n<T>(1,2)*z[18] + z[20];
    z[15]=z[3]*z[15];
    z[17]=z[26]*z[22];
    z[18]= - n<T>(17,3) + 7*z[9];
    z[18]=z[18]*z[42];
    z[20]= - n<T>(17,3) + 11*z[9];
    z[21]= - z[9]*z[32];
    z[22]= - z[22] + n<T>(5,3) - n<T>(7,2)*z[9];
    z[22]=z[2]*z[22];
    z[17]=z[22] + z[18] + z[17] + z[21] + n<T>(1,2)*z[20] + z[12];
    z[17]=z[17]*z[34];
    z[18]= - z[6]*z[12];
    z[18]= - z[19] - 3*z[38] + z[18];
    z[16]=z[28] - z[16];
    z[16]=z[33]*z[44]*z[16];
    z[19]=n<T>(3,2)*z[38] - z[29];
    z[19]=z[2]*z[19];
    z[16]=z[16] + n<T>(1,2)*z[18] + z[19];
    z[16]=z[11]*z[16];
    z[18]=z[32] - 1;
    z[18]=z[18]*z[6];
    z[19]=n<T>(5,2) + z[18];
    z[19]=z[19]*z[32];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[10]*z[19];
    z[20]=static_cast<T>(1)+ z[9];
    z[20]=z[20]*z[32];
    z[20]=z[20] + n<T>(7,3) - z[39];
    z[14]=n<T>(1,3) - z[14];
    z[14]=z[1]*z[14];
    z[14]=n<T>(1,2)*z[20] + z[14];
    z[14]=z[1]*z[14];
    z[18]= - z[18] - n<T>(1,3) - z[12];
    z[14]=z[16] + z[17] + z[14] + n<T>(1,2)*z[18] + z[19];

    r += z[13] + n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qqb_2lha_r479(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r479(const std::array<dd_real,30>&);
#endif
