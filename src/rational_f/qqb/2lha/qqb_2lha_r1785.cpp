#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1785(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[10];
    z[7]=k[27];
    z[8]=k[4];
    z[9]=k[12];
    z[10]=n<T>(1,2)*z[8];
    z[11]=z[2] + z[3];
    z[12]=z[11]*z[10];
    z[12]=z[2] + z[12];
    z[12]=z[8]*z[12];
    z[13]=z[2] - 1;
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[4]*z[12];
    z[14]=n<T>(3,8)*z[7];
    z[15]= - z[5]*z[14];
    z[11]=z[11]*z[8];
    z[16]= - 5*z[11] - n<T>(1,2)*z[3] - 11*z[2];
    z[16]=z[8]*z[16];
    z[12]=n<T>(5,6)*z[12] + n<T>(1,12)*z[16] - n<T>(1,2)*z[2] - n<T>(1,3) + z[15];
    z[12]=z[4]*z[12];
    z[15]=n<T>(1,3)*z[9];
    z[14]=z[15] - z[14];
    z[16]=z[8]*z[3];
    z[17]= - z[16] - z[3] - 1;
    z[17]=z[14]*z[17];
    z[18]=n<T>(1,12)*z[2];
    z[17]=z[18] + z[17];
    z[17]=z[8]*z[17];
    z[19]=n<T>(3,4)*z[7];
    z[12]=z[12] + z[17] + z[19] + z[18] - z[15];
    z[12]=z[6]*z[12];
    z[15]=n<T>(1,3)*z[8];
    z[11]=z[2] + n<T>(1,4)*z[11];
    z[11]=z[11]*z[15];
    z[11]=n<T>(1,4)*z[13] + z[11];
    z[11]=z[4]*z[11];
    z[17]=z[7]*z[5];
    z[18]= - 7*z[3] - 19*z[2];
    z[18]=z[8]*z[18];
    z[11]=5*z[11] + n<T>(1,24)*z[18] - n<T>(7,8)*z[2] - n<T>(2,3) - n<T>(3,4)*z[17];
    z[11]=z[4]*z[11];
    z[14]= - z[14]*z[16];
    z[11]=z[12] + z[11] + z[19] + z[14];
    z[11]=z[6]*z[11];
    z[12]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[12]=z[12]*z[13];
    z[13]=static_cast<T>(1)+ z[2];
    z[10]=z[13]*z[10];
    z[10]=z[10] + n<T>(1,2)*z[5] + z[12];
    z[10]=z[4]*z[10];
    z[12]=z[1] + n<T>(1,3);
    z[13]=z[2]*z[12];
    z[13]=z[13] - n<T>(5,3) - 3*z[17];
    z[13]=n<T>(1,2)*z[13] - z[16];
    z[10]=n<T>(1,2)*z[13] + n<T>(5,3)*z[10];
    z[10]=z[4]*z[10];
    z[13]=z[3]*z[1];
    z[13]=n<T>(1,3)*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[3]*z[12];
    z[14]= - npow(z[3],2)*z[15];
    z[12]=z[14] + z[12];
    z[12]=z[2]*z[12];
    z[13]= - static_cast<T>(1)- z[13];
    z[13]=z[3]*z[13];
    z[12]=z[13] + z[12];
    z[10]=n<T>(1,2)*z[12] + z[10];

    r += n<T>(1,2)*z[10] + z[11];
 
    return r;
}

template double qqb_2lha_r1785(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1785(const std::array<dd_real,30>&);
#endif
