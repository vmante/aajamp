#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r753(const std::array<T,30>& k) {
  T z[44];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=k[3];
    z[9]=k[21];
    z[10]=k[11];
    z[11]=z[3] + n<T>(1,2);
    z[12]=n<T>(1,2)*z[3];
    z[13]=z[11]*z[12];
    z[14]=z[3] - n<T>(1,2);
    z[15]=z[5]*z[3];
    z[16]=z[14]*z[15];
    z[13]=z[13] + z[16];
    z[16]=n<T>(1,2)*z[4];
    z[13]=z[13]*z[16];
    z[17]=z[3] + n<T>(5,2);
    z[17]=z[17]*z[3];
    z[18]= - n<T>(5,4) - z[17];
    z[18]=z[18]*z[12];
    z[19]=z[3] + 1;
    z[19]=z[19]*z[3];
    z[20]=n<T>(1,4) - z[19];
    z[20]=z[20]*z[15];
    z[13]=z[13] + z[18] + z[20];
    z[13]=z[4]*z[13];
    z[18]=3*z[3];
    z[20]=z[18] + n<T>(5,2);
    z[21]=npow(z[3],2);
    z[22]=n<T>(1,2)*z[21];
    z[23]= - z[20]*z[22];
    z[24]=z[12] + 1;
    z[25]=z[24]*z[21];
    z[26]=z[4]*z[21];
    z[26]=z[25] - n<T>(1,4)*z[26];
    z[26]=z[4]*z[26];
    z[27]=z[18] + 1;
    z[27]=z[27]*z[21];
    z[28]=npow(z[3],3);
    z[29]= - z[2]*z[28];
    z[29]=z[27] + z[29];
    z[30]=n<T>(1,2)*z[2];
    z[29]=z[29]*z[30];
    z[23]=z[29] + z[23] + z[26];
    z[26]=n<T>(1,2)*z[1];
    z[23]=z[23]*z[26];
    z[29]=n<T>(1,4)*z[5];
    z[31]=z[28]*z[29];
    z[25]=z[25] + z[31];
    z[25]=z[2]*z[25];
    z[31]= - z[5]*z[27];
    z[32]=z[18] + n<T>(11,2);
    z[33]= - z[3]*z[32];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[3]*z[33];
    z[31]=z[33] + z[31];
    z[25]=n<T>(1,2)*z[31] + z[25];
    z[25]=z[2]*z[25];
    z[31]=z[32]*z[12];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[3]*z[31];
    z[32]=static_cast<T>(1)+ n<T>(9,4)*z[3];
    z[32]=z[3]*z[32];
    z[32]=n<T>(1,4) + z[32];
    z[32]=z[32]*z[15];
    z[13]=z[23] + z[25] + z[13] + z[31] + z[32];
    z[13]=z[13]*z[26];
    z[23]=n<T>(1,2)*z[5];
    z[25]=z[23] + 1;
    z[25]=z[25]*z[5];
    z[25]=z[25] + n<T>(1,2);
    z[26]=npow(z[5],2);
    z[31]=z[26]*z[7];
    z[32]=z[25]*z[31];
    z[33]=static_cast<T>(3)+ n<T>(11,4)*z[5];
    z[33]=z[33]*z[5];
    z[33]=z[33] + n<T>(1,4);
    z[33]=z[33]*z[5];
    z[32]=z[33] - n<T>(5,2)*z[32];
    z[32]=z[32]*z[7];
    z[33]=static_cast<T>(1)+ 5*z[5];
    z[33]=z[33]*z[29];
    z[32]=z[32] - z[33];
    z[34]=z[5] + 3;
    z[34]=z[34]*z[5];
    z[34]=z[34] + 3;
    z[34]=z[34]*z[5];
    z[34]=z[34] + 1;
    z[35]=n<T>(1,2)*z[7];
    z[34]=z[34]*z[35]*z[26];
    z[36]=3*z[26];
    z[25]=z[25]*z[36];
    z[25]=z[34] - z[25];
    z[25]=z[25]*z[7];
    z[34]=z[5] + 1;
    z[36]=z[34]*z[26];
    z[25]=z[25] + z[36];
    z[36]=z[25]*z[16];
    z[37]=z[36] + z[32];
    z[37]=z[4]*z[37];
    z[36]= - z[9]*z[36];
    z[36]=z[37] + z[36];
    z[36]=z[9]*z[36];
    z[25]=z[25]*z[9];
    z[25]=n<T>(1,2)*z[25] - z[32];
    z[25]=z[9]*z[25];
    z[32]=z[34]*z[31];
    z[32]= - z[33] + z[32];
    z[32]=z[7]*z[32];
    z[25]=z[32] + z[25];
    z[25]=z[10]*z[25];
    z[32]=z[7] - static_cast<T>(1)+ z[5];
    z[32]=z[21]*z[32];
    z[33]=static_cast<T>(3)- z[7];
    z[33]=z[35]*z[33];
    z[33]=z[33] - static_cast<T>(1)+ z[23];
    z[33]=z[2]*z[28]*z[33];
    z[32]=z[33] + z[32];
    z[32]=z[2]*z[32];
    z[33]=z[12]*z[5];
    z[32]=z[33] + z[32];
    z[32]=z[6]*z[32]*npow(z[2],2);
    z[25]=z[32] + z[36] + z[25];
    z[32]=n<T>(1,4) - z[3];
    z[32]=z[3]*z[32];
    z[32]=n<T>(5,8) + z[32];
    z[32]=z[32]*z[18];
    z[34]=z[3] - 1;
    z[36]= - z[3]*z[34];
    z[36]= - n<T>(1,4) + z[36];
    z[36]=z[36]*z[15];
    z[32]=z[32] + z[36];
    z[32]=z[5]*z[32];
    z[36]= - z[11]*z[18];
    z[36]=n<T>(13,4) + z[36];
    z[36]=z[3]*z[36];
    z[32]=z[36] + z[32];
    z[32]=z[5]*z[32];
    z[14]=z[14]*z[18];
    z[14]= - n<T>(13,4) + z[14];
    z[14]=z[14]*z[12];
    z[36]=z[12] - 1;
    z[37]=z[36]*z[3];
    z[38]=n<T>(1,2) + z[37];
    z[38]=z[38]*z[33];
    z[39]=z[3] - n<T>(5,4);
    z[40]=z[39]*z[3];
    z[41]= - n<T>(1,4) + z[40];
    z[41]=z[3]*z[41];
    z[38]=z[41] + z[38];
    z[38]=z[5]*z[38];
    z[14]=z[14] + z[38];
    z[14]=z[5]*z[14];
    z[38]=n<T>(1,4) + z[3];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(3,2) + z[38];
    z[38]=z[3]*z[38];
    z[14]=z[38] + z[14];
    z[14]=z[5]*z[14];
    z[38]=n<T>(1,4)*z[3];
    z[19]= - n<T>(3,2) + z[19];
    z[19]=z[19]*z[38];
    z[14]=z[19] + z[14];
    z[14]=z[7]*z[14];
    z[19]=z[3] + n<T>(5,4);
    z[19]=z[19]*z[3];
    z[41]=n<T>(9,8) - z[19];
    z[41]=z[3]*z[41];
    z[14]=z[14] + z[41] + z[32];
    z[14]=z[14]*z[35];
    z[32]=z[39]*z[18];
    z[39]=z[3] - n<T>(3,2);
    z[41]=z[39]*z[15];
    z[32]=z[32] + z[41];
    z[32]=z[5]*z[32];
    z[41]=z[34]*z[18];
    z[32]=z[41] + z[32];
    z[32]=z[5]*z[32];
    z[42]=n<T>(3,2)*z[3];
    z[39]= - z[39]*z[42];
    z[36]= - z[36]*z[33];
    z[43]=n<T>(7,4) - z[3];
    z[43]=z[3]*z[43];
    z[36]=z[43] + z[36];
    z[36]=z[5]*z[36];
    z[36]=z[39] + z[36];
    z[36]=z[5]*z[36];
    z[36]= - z[40] + z[36];
    z[36]=z[5]*z[36];
    z[34]= - z[34]*z[38];
    z[34]=z[34] + z[36];
    z[34]=z[7]*z[34];
    z[36]= - n<T>(3,4) + z[3];
    z[36]=z[3]*z[36];
    z[32]=z[34] + z[36] + z[32];
    z[32]=z[32]*z[35];
    z[34]= - n<T>(1,2) - z[41];
    z[34]=z[34]*z[29];
    z[36]=n<T>(3,8) - z[3];
    z[36]=z[3]*z[36];
    z[34]=z[34] + n<T>(1,4) + z[36];
    z[34]=z[5]*z[34];
    z[32]=z[32] - n<T>(3,8)*z[21] + z[34];
    z[16]=z[32]*z[16];
    z[24]=z[24]*z[18];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[3]*z[24];
    z[24]=n<T>(1,4) + z[24];
    z[32]=3*z[21];
    z[34]= - n<T>(1,2) + z[32];
    z[34]=z[34]*z[15];
    z[36]=n<T>(9,8) + z[3];
    z[36]=z[3]*z[36];
    z[36]= - n<T>(3,8) + z[36];
    z[36]=z[3]*z[36];
    z[34]=n<T>(1,4)*z[34] - n<T>(1,16) + z[36];
    z[34]=z[5]*z[34];
    z[14]=z[16] + z[14] + n<T>(1,4)*z[24] + z[34];
    z[14]=z[4]*z[14];
    z[16]=z[18] - 1;
    z[22]= - z[16]*z[22];
    z[24]=z[18] - n<T>(1,2);
    z[34]=z[42] - 1;
    z[36]= - z[7]*z[34];
    z[36]=z[24] + z[36];
    z[36]=z[7]*z[21]*z[36];
    z[28]=z[28]*z[30];
    z[22]=z[28] + z[22] + z[36];
    z[22]=z[22]*z[30];
    z[28]= - static_cast<T>(1)- n<T>(5,2)*z[3];
    z[28]=z[28]*z[42];
    z[11]= - z[5]*z[11]*z[21];
    z[11]=z[28] + z[11];
    z[28]= - n<T>(11,4) + z[3];
    z[28]=z[3]*z[28];
    z[28]=n<T>(1,2) + z[28];
    z[28]=z[28]*z[12];
    z[36]=n<T>(1,4) + z[37];
    z[36]=z[36]*z[15];
    z[28]=z[28] + z[36];
    z[28]=z[7]*z[28];
    z[36]=z[23]*z[21];
    z[37]=n<T>(11,2) - z[3];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(1,2) + z[37];
    z[37]=z[3]*z[37];
    z[37]=z[37] + z[36];
    z[28]=n<T>(1,2)*z[37] + z[28];
    z[28]=z[7]*z[28];
    z[11]=z[22] + n<T>(1,2)*z[11] + z[28];
    z[11]=z[11]*z[30];
    z[22]= - z[24]*z[29]*z[21];
    z[24]= - static_cast<T>(1)- 2*z[3];
    z[24]=z[3]*z[24];
    z[24]=n<T>(3,8) + z[24];
    z[24]=z[3]*z[24];
    z[22]=z[24] + z[22];
    z[22]=z[5]*z[22];
    z[17]= - n<T>(5,4) + z[17];
    z[17]=z[17]*z[12];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[21]*z[33];
    z[19]= - n<T>(5,4) + z[19];
    z[19]=z[3]*z[19];
    z[19]=z[19] + z[21];
    z[19]=z[5]*z[19];
    z[17]=z[17] + z[19];
    z[17]=z[17]*z[35];
    z[19]= - static_cast<T>(7)- 5*z[3];
    z[19]=z[3]*z[19];
    z[19]=n<T>(9,4) + z[19];
    z[19]=z[19]*z[38];
    z[17]=z[17] + z[19] + z[22];
    z[17]=z[7]*z[17];
    z[19]=z[29]*z[27];
    z[20]=z[3]*z[20];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[3]*z[20];
    z[19]=z[20] + z[19];
    z[19]=z[19]*z[23];
    z[20]=n<T>(15,8) + z[3];
    z[20]=z[3]*z[20];
    z[20]=n<T>(3,4) + z[20];
    z[20]=z[3]*z[20];
    z[11]=z[11] + z[17] + z[19] + n<T>(1,8) + z[20];
    z[11]=z[2]*z[11];
    z[17]= - z[34]*z[42];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17];
    z[19]=n<T>(3,4)*z[3];
    z[20]=static_cast<T>(1)- z[19];
    z[20]=z[3]*z[20];
    z[20]= - n<T>(1,4) + z[20];
    z[15]=z[20]*z[15];
    z[15]=z[15] - n<T>(3,8) + z[17];
    z[15]=z[5]*z[15];
    z[17]=n<T>(5,2) - z[32];
    z[17]=z[17]*z[19];
    z[15]=z[17] + z[15];
    z[15]=z[5]*z[15];
    z[17]= - static_cast<T>(1)- z[42];
    z[17]=z[3]*z[17];
    z[17]=n<T>(5,4) + z[17];
    z[12]=z[17]*z[12];
    z[12]=z[12] + z[15];
    z[12]=z[7]*z[12];
    z[15]=n<T>(3,4) + z[3];
    z[15]=z[15]*z[18];
    z[15]= - n<T>(13,8) + z[15];
    z[15]=z[3]*z[15];
    z[16]=z[16]*z[36];
    z[17]=9*z[3];
    z[18]= - n<T>(5,2) + z[17];
    z[18]=z[3]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[3]*z[18];
    z[16]=z[18] + z[16];
    z[16]=z[5]*z[16];
    z[18]=static_cast<T>(5)+ 27*z[3];
    z[18]=z[3]*z[18];
    z[18]= - static_cast<T>(11)+ z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(3,2) + z[18];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[16]*z[23];
    z[12]=z[12] + z[15] + z[16];
    z[12]=z[7]*z[12];
    z[15]= - static_cast<T>(13)- z[17];
    z[15]=z[15]*z[38];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[3]*z[15];
    z[16]= - n<T>(1,2) - z[17];
    z[16]=z[3]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[16]*z[33];
    z[17]= - static_cast<T>(7)- z[17];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[3]*z[17];
    z[16]=z[16] - n<T>(1,2) + z[17];
    z[16]=z[16]*z[23];
    z[12]=z[12] + z[16] - n<T>(3,8) + z[15];
    z[15]=z[26]*z[9];
    z[15]=z[15] + z[5];
    z[16]=z[4]*z[26];
    z[16]=z[16] - z[15];
    z[16]=z[9]*z[4]*z[16];
    z[17]= - z[5] + z[31];
    z[17]=z[7]*z[17];
    z[15]=z[9]*z[15];
    z[15]=3*z[17] + z[15];
    z[15]=z[10]*z[15];
    z[15]=z[16] + z[15];
    z[15]=z[8]*z[15];

    r += z[11] + n<T>(1,2)*z[12] + z[13] + z[14] + n<T>(1,16)*z[15] + n<T>(1,4)*
      z[25];
 
    return r;
}

template double qqb_2lha_r753(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r753(const std::array<dd_real,30>&);
#endif
