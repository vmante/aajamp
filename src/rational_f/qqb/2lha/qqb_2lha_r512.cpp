#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r512(const std::array<T,30>& k) {
  T z[76];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[7];
    z[6]=k[17];
    z[7]=k[4];
    z[8]=k[15];
    z[9]=k[2];
    z[10]=k[5];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=k[12];
    z[14]=k[11];
    z[15]=n<T>(1,2)*z[7];
    z[16]= - static_cast<T>(129)- n<T>(85,2)*z[7];
    z[16]=z[7]*z[16];
    z[16]= - n<T>(261,2) + z[16];
    z[16]=z[16]*z[15];
    z[17]=n<T>(1,12)*z[7];
    z[18]= - static_cast<T>(301)- n<T>(149,2)*z[7];
    z[18]=z[18]*z[17];
    z[18]= - static_cast<T>(38)+ z[18];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(307,12) + z[18];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(155,24) + z[18];
    z[18]=z[11]*z[18];
    z[16]=z[18] - static_cast<T>(22)+ z[16];
    z[16]=z[11]*z[16];
    z[18]=n<T>(1,3)*z[7];
    z[19]= - static_cast<T>(163)- n<T>(361,4)*z[7];
    z[19]=z[19]*z[18];
    z[19]= - n<T>(97,4) + z[19];
    z[20]=static_cast<T>(2)+ z[15];
    z[20]=z[20]*z[18];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[7]*z[20];
    z[20]=n<T>(2,3) + z[20];
    z[20]=z[7]*z[20];
    z[20]=n<T>(1,6) + z[20];
    z[20]=z[11]*z[20];
    z[21]=z[18] + 1;
    z[21]=z[21]*z[7];
    z[21]=z[21] + 1;
    z[21]=z[21]*z[7];
    z[21]=z[21] + n<T>(1,3);
    z[20]=2*z[21] + z[20];
    z[20]=z[11]*z[20];
    z[22]=z[15] + 1;
    z[22]=z[22]*z[7];
    z[23]=z[22] + n<T>(1,2);
    z[20]=z[20] + z[23];
    z[20]=z[3]*z[20];
    z[16]=13*z[20] + n<T>(1,2)*z[19] + z[16];
    z[16]=z[3]*z[16];
    z[19]=n<T>(1,4)*z[7];
    z[20]=n<T>(923,6) + 55*z[7];
    z[20]=z[20]*z[19];
    z[20]=n<T>(91,3) + z[20];
    z[20]=z[7]*z[20];
    z[24]=31*z[7];
    z[25]=n<T>(17,4) + z[7];
    z[25]=z[25]*z[24];
    z[25]=n<T>(773,4) + z[25];
    z[25]=z[7]*z[25];
    z[25]=n<T>(461,4) + z[25];
    z[25]=z[7]*z[25];
    z[25]=n<T>(91,4) + z[25];
    z[26]=n<T>(1,6)*z[11];
    z[25]=z[25]*z[26];
    z[20]=z[25] + n<T>(45,8) + z[20];
    z[20]=z[11]*z[20];
    z[25]=n<T>(57,2) + n<T>(103,3)*z[7];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(19,2) + z[25];
    z[16]=z[16] + n<T>(1,4)*z[25] + z[20];
    z[16]=z[3]*z[16];
    z[20]=n<T>(3,4)*z[7];
    z[25]=z[20] + 2;
    z[27]=3*z[7];
    z[25]=z[25]*z[27];
    z[28]= - n<T>(53,8) - z[25];
    z[28]=z[7]*z[28];
    z[29]=z[27] + n<T>(11,2);
    z[30]= - z[29]*z[27];
    z[30]=static_cast<T>(7)+ z[30];
    z[30]=z[7]*z[30];
    z[30]=n<T>(173,6) + z[30];
    z[30]=z[7]*z[30];
    z[30]=n<T>(43,3) + z[30];
    z[30]=z[11]*z[30];
    z[28]=n<T>(1,8)*z[30] - n<T>(23,8) + z[28];
    z[28]=z[11]*z[28];
    z[30]=z[7] + n<T>(7,2);
    z[31]=z[30]*z[19];
    z[31]=z[31] + 1;
    z[31]=z[31]*z[7];
    z[31]=z[31] + n<T>(3,8);
    z[31]=z[31]*z[11];
    z[32]=z[7] + n<T>(5,2);
    z[32]=z[32]*z[7];
    z[33]=z[32] + n<T>(3,2);
    z[31]=z[31] + n<T>(1,4)*z[33];
    z[34]=z[2]*z[11];
    z[35]=n<T>(9,2)*z[34];
    z[31]=z[31]*z[35];
    z[30]= - z[30]*z[15];
    z[30]= - static_cast<T>(1)+ z[30];
    z[28]= - z[31] + n<T>(9,4)*z[30] + z[28];
    z[28]=z[2]*z[28];
    z[30]=7*z[7];
    z[36]=n<T>(25,2) + z[30];
    z[36]=z[36]*z[27];
    z[36]=n<T>(187,3) + z[36];
    z[36]=z[7]*z[36];
    z[36]=n<T>(97,2) + z[36];
    z[37]=static_cast<T>(31)+ 15*z[7];
    z[37]=z[7]*z[37];
    z[37]=n<T>(39,2) + z[37];
    z[37]=z[37]*z[15];
    z[37]=n<T>(11,3) + z[37];
    z[37]=z[7]*z[37];
    z[37]=n<T>(23,12) + z[37];
    z[37]=z[11]*z[37];
    z[36]=n<T>(1,2)*z[36] + z[37];
    z[36]=z[11]*z[36];
    z[37]=5*z[7];
    z[38]=n<T>(17,2) + z[37];
    z[38]=z[7]*z[38];
    z[36]=z[36] - static_cast<T>(1)+ z[38];
    z[28]=n<T>(1,4)*z[36] + z[28];
    z[28]=z[2]*z[28];
    z[36]=25*z[7];
    z[38]=n<T>(107,2) + z[36];
    z[38]=z[38]*z[18];
    z[38]=n<T>(19,2) + z[38];
    z[39]=n<T>(31,2)*z[7];
    z[40]=static_cast<T>(71)+ z[39];
    z[17]=z[40]*z[17];
    z[17]=static_cast<T>(10)+ z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(89,12) + z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(49,24) + z[17];
    z[17]=z[11]*z[17];
    z[40]=static_cast<T>(149)+ n<T>(131,3)*z[7];
    z[40]=z[7]*z[40];
    z[40]=static_cast<T>(167)+ z[40];
    z[40]=z[7]*z[40];
    z[40]=n<T>(185,3) + z[40];
    z[17]=n<T>(1,8)*z[40] + z[17];
    z[17]=z[11]*z[17];
    z[40]=z[19] + 1;
    z[41]= - z[40]*z[18];
    z[41]= - n<T>(1,2) + z[41];
    z[41]=z[7]*z[41];
    z[41]= - n<T>(1,3) + z[41];
    z[41]=z[7]*z[41];
    z[41]= - n<T>(1,12) + z[41];
    z[41]=z[11]*z[41];
    z[21]=z[41] - z[21];
    z[21]=z[11]*z[21];
    z[21]= - n<T>(1,2)*z[23] + z[21];
    z[41]=n<T>(11,2)*z[3];
    z[21]=z[21]*z[41];
    z[17]=z[21] + n<T>(1,2)*z[38] + z[17];
    z[17]=z[3]*z[17];
    z[21]=n<T>(19,2) + z[27];
    z[21]=z[21]*z[15];
    z[21]=static_cast<T>(5)+ z[21];
    z[21]=z[7]*z[21];
    z[21]=n<T>(7,4) + z[21];
    z[38]=static_cast<T>(7)+ z[27];
    z[38]=z[38]*z[27];
    z[38]=n<T>(23,3) + z[38];
    z[38]=z[7]*z[38];
    z[38]= - n<T>(35,3) + z[38];
    z[38]=z[38]*z[15];
    z[38]= - n<T>(11,3) + z[38];
    z[42]=n<T>(1,2)*z[11];
    z[38]=z[38]*z[42];
    z[21]=3*z[21] + z[38];
    z[21]=z[11]*z[21];
    z[38]=z[7] + 3;
    z[43]=z[38]*z[7];
    z[44]=z[43] + 3;
    z[44]=z[44]*z[7];
    z[44]=z[44] + 1;
    z[45]=z[44]*z[42];
    z[45]=z[45] + z[23];
    z[35]=z[45]*z[35];
    z[45]=z[7] + n<T>(3,2);
    z[46]=z[45]*z[7];
    z[47]=n<T>(1,2) + z[46];
    z[21]=z[35] + n<T>(9,2)*z[47] + z[21];
    z[21]=z[2]*z[21];
    z[35]=n<T>(1,8)*z[7];
    z[47]= - static_cast<T>(17)- z[27];
    z[47]=z[7]*z[47];
    z[47]= - static_cast<T>(33)+ z[47];
    z[47]=z[7]*z[47];
    z[47]= - static_cast<T>(27)+ z[47];
    z[47]=z[47]*z[35];
    z[47]= - static_cast<T>(1)+ z[47];
    z[47]=z[11]*z[47];
    z[48]= - n<T>(49,3) + z[27];
    z[48]=z[7]*z[48];
    z[48]= - static_cast<T>(43)+ z[48];
    z[48]=z[7]*z[48];
    z[48]= - n<T>(71,3) + z[48];
    z[47]=n<T>(1,8)*z[48] + z[47];
    z[47]=z[11]*z[47];
    z[48]=z[15] - 1;
    z[49]= - z[7]*z[48];
    z[49]=static_cast<T>(1)+ z[49];
    z[21]=n<T>(1,4)*z[21] + n<T>(3,4)*z[49] + z[47];
    z[21]=z[2]*z[21];
    z[47]=23*z[7];
    z[49]= - static_cast<T>(107)- z[47];
    z[49]=z[49]*z[18];
    z[49]= - static_cast<T>(61)+ z[49];
    z[49]=z[7]*z[49];
    z[49]= - n<T>(137,3) + z[49];
    z[49]=z[49]*z[15];
    z[49]= - n<T>(19,3) + z[49];
    z[49]=z[49]*z[42];
    z[50]= - static_cast<T>(27)- n<T>(31,4)*z[7];
    z[50]=z[7]*z[50];
    z[50]= - n<T>(123,4) + z[50];
    z[50]=z[7]*z[50];
    z[49]=z[49] - n<T>(23,2) + z[50];
    z[49]=z[11]*z[49];
    z[50]= - static_cast<T>(253)- 107*z[7];
    z[50]=z[7]*z[50];
    z[49]=z[49] - static_cast<T>(5)+ n<T>(1,24)*z[50];
    z[17]=z[21] + n<T>(1,2)*z[49] + z[17];
    z[17]=z[4]*z[17];
    z[21]= - n<T>(253,12) - z[30];
    z[21]=z[7]*z[21];
    z[21]= - n<T>(11,3) + z[21];
    z[21]=z[7]*z[21];
    z[21]=n<T>(125,12) + z[21];
    z[49]= - n<T>(353,24) - z[27];
    z[49]=z[7]*z[49];
    z[49]= - n<T>(263,12) + z[49];
    z[49]=z[7]*z[49];
    z[49]= - n<T>(281,24) + z[49];
    z[49]=z[7]*z[49];
    z[49]= - n<T>(3,2) + z[49];
    z[49]=z[11]*z[49];
    z[21]=n<T>(1,2)*z[21] + z[49];
    z[21]=z[11]*z[21];
    z[49]=n<T>(181,12) - z[27];
    z[49]=z[7]*z[49];
    z[49]=n<T>(181,12) + z[49];
    z[21]=n<T>(1,2)*z[49] + z[21];
    z[16]=z[17] + z[28] + n<T>(1,2)*z[21] + z[16];
    z[16]=z[4]*z[16];
    z[17]=npow(z[3],2);
    z[21]= - n<T>(649,3) - n<T>(31,2)*z[3];
    z[21]=z[21]*z[17];
    z[28]=z[2]*npow(z[3],4);
    z[49]=n<T>(29,9)*z[28];
    z[50]=npow(z[3],3);
    z[51]= - n<T>(27,2)*z[50] - z[49];
    z[51]=z[2]*z[51];
    z[21]=n<T>(1,12)*z[21] + z[51];
    z[21]=z[2]*z[21];
    z[51]=n<T>(1,3)*z[3];
    z[52]=static_cast<T>(53)- 31*z[3];
    z[52]=z[3]*z[52];
    z[52]= - n<T>(67,3) + n<T>(1,8)*z[52];
    z[52]=z[52]*z[51];
    z[21]=z[52] + z[21];
    z[21]=z[2]*z[21];
    z[52]=n<T>(1,2)*z[3];
    z[53]=static_cast<T>(25)- n<T>(31,4)*z[3];
    z[53]=z[53]*z[52];
    z[53]=static_cast<T>(22)+ z[53];
    z[53]=z[3]*z[53];
    z[53]=n<T>(103,24) + z[53];
    z[41]=static_cast<T>(19)- z[41];
    z[41]=z[41]*z[51];
    z[41]=n<T>(9,2) + z[41];
    z[54]=n<T>(1,2)*z[4];
    z[41]=z[41]*z[54];
    z[55]= - n<T>(21,4) + n<T>(13,3)*z[3];
    z[55]=z[3]*z[55];
    z[55]= - n<T>(25,3) + z[55];
    z[55]=z[3]*z[55];
    z[41]=z[41] - n<T>(9,2) + z[55];
    z[41]=z[41]*z[54];
    z[55]= - static_cast<T>(1)- n<T>(1,4)*z[3];
    z[55]=z[3]*z[55];
    z[55]= - n<T>(31,4) + 7*z[55];
    z[55]=z[3]*z[55];
    z[56]=static_cast<T>(1)+ z[52];
    z[56]=z[3]*z[56];
    z[56]=n<T>(1,2) + z[56];
    z[56]=z[6]*z[56];
    z[55]=z[56] - n<T>(5,2) + z[55];
    z[56]=n<T>(1,2)*z[6];
    z[55]=z[55]*z[56];
    z[21]=z[55] + z[41] + n<T>(1,3)*z[53] + z[21];
    z[21]=z[1]*z[21];
    z[41]=116*z[3];
    z[53]= - z[41] + 185;
    z[53]=z[7]*z[53];
    z[53]= - n<T>(817,2) + z[53];
    z[53]=z[3]*z[53];
    z[53]=n<T>(649,2) + z[53];
    z[53]=z[53]*z[17];
    z[55]=z[7] - 2;
    z[57]=z[55]*z[3];
    z[57]=n<T>(58,9)*z[57];
    z[58]=static_cast<T>(27)+ z[57];
    z[58]=z[58]*z[50];
    z[28]=z[58] + n<T>(58,9)*z[28];
    z[28]=z[2]*z[28];
    z[28]=n<T>(1,9)*z[53] + z[28];
    z[28]=z[2]*z[28];
    z[53]= - static_cast<T>(31)- n<T>(2147,6)*z[7];
    z[53]=z[3]*z[53];
    z[39]=n<T>(1,12)*z[53] - n<T>(164,3) + z[39];
    z[39]=z[3]*z[39];
    z[39]=n<T>(134,9) + z[39];
    z[39]=z[3]*z[39];
    z[28]=z[39] + z[28];
    z[28]=z[2]*z[28];
    z[39]=11*z[7];
    z[53]= - static_cast<T>(443)- z[39];
    z[58]=z[7] + 1;
    z[59]=z[58]*z[3];
    z[60]= - n<T>(31,2)*z[59] + n<T>(77,4) - n<T>(269,3)*z[7];
    z[60]=z[3]*z[60];
    z[53]=n<T>(1,3)*z[53] + z[60];
    z[53]=z[3]*z[53];
    z[53]= - n<T>(103,6) + z[53];
    z[28]=n<T>(1,6)*z[53] + z[28];
    z[28]=z[2]*z[28];
    z[47]=n<T>(65,3) + z[47];
    z[47]=n<T>(1,2)*z[47] - n<T>(11,3)*z[59];
    z[47]=z[3]*z[47];
    z[53]=13*z[7];
    z[60]=static_cast<T>(9)- z[53];
    z[61]=n<T>(9,2)*z[7];
    z[62]=z[2]*z[61];
    z[47]=z[62] + n<T>(1,2)*z[60] + z[47];
    z[60]=n<T>(1,4)*z[4];
    z[47]=z[47]*z[60];
    z[62]= - n<T>(97,2) + 41*z[7];
    z[63]=13*z[59] - static_cast<T>(17)- n<T>(53,2)*z[7];
    z[63]=z[3]*z[63];
    z[62]=n<T>(1,4)*z[62] + z[63];
    z[62]=z[62]*z[51];
    z[63]= - static_cast<T>(11)+ n<T>(3,2)*z[7];
    z[47]=z[47] + n<T>(1,4)*z[63] + z[62];
    z[47]=z[4]*z[47];
    z[62]=static_cast<T>(61)- n<T>(59,3)*z[7];
    z[63]= - n<T>(31,12)*z[59] + n<T>(57,8) + n<T>(23,3)*z[7];
    z[63]=z[3]*z[63];
    z[62]=n<T>(1,8)*z[62] + z[63];
    z[62]=z[3]*z[62];
    z[63]= - 7*z[59] - n<T>(43,2) - z[39];
    z[63]=z[3]*z[63];
    z[63]=z[63] - n<T>(31,2) + z[7];
    z[63]=z[63]*z[52];
    z[59]=n<T>(3,2) + z[59];
    z[59]=z[3]*z[59];
    z[59]=n<T>(1,2) + z[59];
    z[59]=z[6]*z[59];
    z[59]=z[59] - static_cast<T>(1)+ z[63];
    z[59]=z[59]*z[56];
    z[63]=z[2] - 1;
    z[64]= - z[8]*z[7]*z[63];
    z[21]=z[21] + z[59] + n<T>(9,4)*z[64] + z[47] + z[28] + n<T>(103,72) + z[62]
   ;
    z[21]=z[1]*z[21];
    z[28]=n<T>(605,4) + 343*z[7];
    z[28]=z[28]*z[18];
    z[28]=n<T>(317,4) + z[28];
    z[47]=static_cast<T>(443)- n<T>(475,4)*z[7];
    z[47]=z[47]*z[18];
    z[47]=n<T>(59,2) + z[47];
    z[47]=z[7]*z[47];
    z[47]=n<T>(23,4) + z[47];
    z[47]=z[11]*z[47];
    z[28]=n<T>(1,2)*z[28] + z[47];
    z[47]=n<T>(1,3)*z[11];
    z[28]=z[28]*z[47];
    z[59]= - n<T>(1039,4) + 143*z[7];
    z[59]=z[7]*z[59];
    z[59]= - static_cast<T>(37)+ z[59];
    z[59]=z[7]*z[59];
    z[59]= - n<T>(7,4) + z[59];
    z[59]=z[59]*z[47];
    z[62]=313*z[7];
    z[64]= - static_cast<T>(2615)+ z[62];
    z[64]=z[7]*z[64];
    z[64]= - static_cast<T>(33)+ n<T>(1,18)*z[64];
    z[64]=z[7]*z[64];
    z[59]=z[59] - n<T>(19,3) + z[64];
    z[59]=z[11]*z[59];
    z[64]= - static_cast<T>(31)- n<T>(1897,18)*z[7];
    z[64]=z[7]*z[64];
    z[64]= - n<T>(31,2) + z[64];
    z[59]=n<T>(1,2)*z[64] + z[59];
    z[64]=z[11]*npow(z[7],3);
    z[65]=z[11] + 1;
    z[66]= - z[3]*z[65]*z[64];
    z[59]=n<T>(1,2)*z[59] + n<T>(116,9)*z[66];
    z[59]=z[3]*z[59];
    z[66]= - static_cast<T>(739)+ n<T>(73,3)*z[7];
    z[66]=z[66]*z[18];
    z[66]=static_cast<T>(65)+ z[66];
    z[28]=z[59] + n<T>(1,8)*z[66] + z[28];
    z[28]=z[3]*z[28];
    z[59]= - static_cast<T>(5)+ 27*z[7];
    z[59]=z[7]*z[59];
    z[59]=n<T>(163,2) + z[59];
    z[66]=9*z[7];
    z[67]= - n<T>(925,18) + z[66];
    z[67]=z[7]*z[67];
    z[67]= - n<T>(11,6) + z[67];
    z[67]=z[7]*z[67];
    z[67]=n<T>(67,3) + z[67];
    z[67]=z[11]*z[67];
    z[59]=n<T>(1,2)*z[59] + z[67];
    z[67]=n<T>(1,4)*z[11];
    z[59]=z[59]*z[67];
    z[68]=z[7] + n<T>(3,4);
    z[69]=z[68]*z[7];
    z[28]=z[28] + z[59] - n<T>(201,16) + z[69];
    z[28]=z[3]*z[28];
    z[59]=n<T>(5299,4) - 127*z[7];
    z[59]=z[59]*z[18];
    z[59]= - n<T>(725,2) + z[59];
    z[70]=197*z[7];
    z[71]= - n<T>(565,4) + z[70];
    z[26]=z[71]*z[26];
    z[71]= - static_cast<T>(22)+ n<T>(95,12)*z[7];
    z[26]=5*z[71] + z[26];
    z[26]=z[11]*z[26];
    z[71]= - static_cast<T>(1)+ z[18];
    z[71]=z[7]*z[71];
    z[72]= - z[11]*z[18];
    z[72]= - z[7] + z[72];
    z[72]=z[11]*z[72];
    z[71]=z[71] + z[72];
    z[41]=z[71]*z[41];
    z[26]=z[41] + n<T>(1,2)*z[59] + z[26];
    z[26]=z[26]*z[51];
    z[41]=n<T>(5,2) - z[66];
    z[41]=z[11]*z[41];
    z[41]=n<T>(25,2) + z[41];
    z[41]=z[41]*z[42];
    z[41]=z[41] + n<T>(1325,12) - z[24];
    z[26]=n<T>(1,2)*z[41] + z[26];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(67,9) + z[26];
    z[26]=z[3]*z[26];
    z[41]=static_cast<T>(8)- z[7];
    z[41]=z[41]*z[18];
    z[51]= - static_cast<T>(4)- z[11];
    z[51]=z[51]*z[47];
    z[41]=z[51] - static_cast<T>(2)+ z[41];
    z[41]=z[3]*z[41];
    z[51]=n<T>(173,2) - 37*z[7];
    z[59]=static_cast<T>(1)+ z[42];
    z[59]=z[11]*z[59];
    z[41]=n<T>(29,3)*z[41] + n<T>(5,9)*z[51] + n<T>(9,4)*z[59];
    z[41]=z[3]*z[41];
    z[41]= - n<T>(649,36) + z[41];
    z[41]=z[41]*z[17];
    z[51]= - n<T>(27,2) - z[57];
    z[51]=z[51]*z[50];
    z[49]=z[51] - z[49];
    z[49]=z[2]*z[49];
    z[41]=z[41] + z[49];
    z[41]=z[2]*z[41];
    z[26]=z[26] + z[41];
    z[26]=z[2]*z[26];
    z[41]=n<T>(1,6)*z[7];
    z[49]= - n<T>(6683,2) + 707*z[7];
    z[49]=z[49]*z[41];
    z[51]= - n<T>(743,2) + z[62];
    z[51]=z[7]*z[51];
    z[51]= - n<T>(67,2) + z[51];
    z[51]=z[51]*z[42];
    z[49]=z[51] - static_cast<T>(49)+ z[49];
    z[49]=z[49]*z[47];
    z[51]= - n<T>(6229,3) + 619*z[7];
    z[51]=z[51]*z[18];
    z[51]= - static_cast<T>(31)+ z[51];
    z[49]=n<T>(1,4)*z[51] + z[49];
    z[51]=npow(z[7],2);
    z[57]=z[51]*z[11];
    z[59]= - 2*z[51] - z[57];
    z[59]=z[11]*z[59];
    z[59]= - z[51] + z[59];
    z[59]=z[3]*z[59];
    z[49]=n<T>(1,2)*z[49] + n<T>(58,3)*z[59];
    z[49]=z[3]*z[49];
    z[59]=n<T>(1409,2) - 359*z[7];
    z[59]=z[59]*z[18];
    z[59]=n<T>(113,4) + z[59];
    z[59]=z[59]*z[67];
    z[62]= - n<T>(463,4) + 169*z[7];
    z[59]=n<T>(1,3)*z[62] + z[59];
    z[47]=z[59]*z[47];
    z[36]=static_cast<T>(1673)- z[36];
    z[36]=z[7]*z[36];
    z[36]= - n<T>(889,4) + n<T>(1,9)*z[36];
    z[36]=z[49] + n<T>(1,4)*z[36] + z[47];
    z[36]=z[3]*z[36];
    z[47]=z[61] + 5;
    z[49]= - static_cast<T>(2)+ n<T>(9,8)*z[7];
    z[49]=z[7]*z[49];
    z[49]= - n<T>(11,8) + z[49];
    z[49]=z[11]*z[49];
    z[49]=n<T>(1,4)*z[47] + z[49];
    z[49]=z[11]*z[49];
    z[59]=static_cast<T>(347)+ z[39];
    z[36]=z[36] + n<T>(1,18)*z[59] + z[49];
    z[36]=z[3]*z[36];
    z[26]=z[26] + n<T>(103,72) + z[36];
    z[26]=z[2]*z[26];
    z[36]= - n<T>(13,2) - z[37];
    z[36]=z[7]*z[36];
    z[36]= - n<T>(7,2) + z[36];
    z[36]=z[11]*z[36];
    z[36]=z[36] + static_cast<T>(3)- z[37];
    z[36]=z[11]*z[36];
    z[36]= - n<T>(145,18) + z[36];
    z[26]=z[26] + n<T>(1,4)*z[36] + z[28];
    z[26]=z[2]*z[26];
    z[28]= - n<T>(5,8) + z[7];
    z[28]=z[28]*z[37];
    z[28]=n<T>(121,8) + z[28];
    z[28]=z[7]*z[28];
    z[36]=z[11]*z[7];
    z[49]=static_cast<T>(3)+ n<T>(13,4)*z[7];
    z[49]=z[7]*z[49];
    z[49]=static_cast<T>(1)+ z[49];
    z[49]=z[7]*z[49];
    z[49]=n<T>(55,8) + z[49];
    z[49]=z[49]*z[36];
    z[28]=z[28] + z[49];
    z[28]=z[11]*z[28];
    z[49]= - static_cast<T>(11)- z[7];
    z[49]=z[49]*z[27];
    z[59]= - n<T>(49,2) - z[66];
    z[59]=z[7]*z[59];
    z[59]= - static_cast<T>(47)+ z[59];
    z[59]=z[7]*z[59];
    z[47]= - z[7]*z[47];
    z[47]= - n<T>(29,2) + z[47];
    z[47]=z[7]*z[47];
    z[47]= - static_cast<T>(5)+ z[47];
    z[47]=z[7]*z[47];
    z[47]=static_cast<T>(9)+ z[47];
    z[47]=z[11]*z[47];
    z[47]=z[47] + static_cast<T>(9)+ z[59];
    z[47]=z[11]*z[47];
    z[47]=z[49] + z[47];
    z[49]= - z[38]*z[15];
    z[49]= - static_cast<T>(1)+ z[49];
    z[40]=z[40]*z[7];
    z[59]= - n<T>(5,4) - z[40];
    z[59]=z[7]*z[59];
    z[59]= - n<T>(1,2) + z[59];
    z[59]=z[11]*z[59];
    z[49]=n<T>(1,2)*z[49] + z[59];
    z[49]=z[49]*z[34];
    z[47]=n<T>(1,2)*z[47] + 9*z[49];
    z[49]=n<T>(1,2)*z[2];
    z[47]=z[47]*z[49];
    z[59]=n<T>(33,2) + z[7];
    z[59]=z[59]*z[15];
    z[28]=z[47] + z[59] + z[28];
    z[28]=z[2]*z[28];
    z[47]=z[7] - n<T>(5,2);
    z[59]= - z[47]*z[51];
    z[62]=z[51]*z[42];
    z[71]=z[7] - 1;
    z[72]=z[71]*z[7];
    z[73]=static_cast<T>(3)- z[72];
    z[73]=z[73]*z[62];
    z[59]=z[59] + z[73];
    z[59]=z[11]*z[59];
    z[59]=z[51] + z[59];
    z[73]= - z[58]*z[15];
    z[74]= - z[23]*z[36];
    z[73]=z[73] + z[74];
    z[73]=z[2]*z[73]*z[42];
    z[74]=z[19]*z[11];
    z[43]= - static_cast<T>(1)- z[43];
    z[43]=z[7]*z[43];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[43]*z[74];
    z[22]=n<T>(1,4) - z[22];
    z[22]=z[7]*z[22];
    z[22]=z[22] + z[43];
    z[22]=z[11]*z[22];
    z[22]=z[73] - n<T>(1,2)*z[51] + z[22];
    z[22]=z[2]*z[22];
    z[22]=n<T>(1,2)*z[59] + z[22];
    z[22]=z[2]*z[22];
    z[38]=z[38]*z[67];
    z[38]=static_cast<T>(1)+ z[38];
    z[38]=z[38]*z[64];
    z[43]=npow(z[11],2);
    z[59]=npow(z[7],4)*z[43]*z[3];
    z[22]=z[22] + z[38] - n<T>(1,4)*z[59];
    z[22]=z[8]*z[22];
    z[38]=n<T>(1,4)*z[51];
    z[73]=n<T>(83,2) - z[7];
    z[73]=z[73]*z[38];
    z[75]= - n<T>(13,4) + z[7];
    z[75]=z[7]*z[75];
    z[75]=n<T>(57,8) + z[75];
    z[75]=z[75]*z[57];
    z[73]=z[73] + z[75];
    z[73]=z[11]*z[73];
    z[75]= - n<T>(21,2) + z[53];
    z[75]=z[75]*z[42];
    z[75]=static_cast<T>(5)+ z[75];
    z[75]=z[75]*z[64];
    z[75]=z[75] - n<T>(17,4)*z[59];
    z[75]=z[75]*z[52];
    z[22]=9*z[22] + z[28] + z[75] + z[38] + z[73];
    z[22]=z[8]*z[22];
    z[28]= - n<T>(31,2) - z[7];
    z[28]=z[28]*z[51];
    z[38]=static_cast<T>(27)+ z[7];
    z[38]=z[7]*z[38];
    z[38]= - n<T>(63,2) + z[38];
    z[38]=z[38]*z[57];
    z[28]=z[28] + z[38];
    z[28]=z[11]*z[28];
    z[28]= - 29*z[51] + z[28];
    z[38]=n<T>(5,4) + z[53];
    z[38]=z[11]*z[38];
    z[38]=n<T>(39,2) + z[38];
    z[38]=z[38]*z[64];
    z[38]=z[38] - n<T>(37,4)*z[59];
    z[38]=z[3]*z[38];
    z[28]=n<T>(1,4)*z[28] + z[38];
    z[28]=z[3]*z[28];
    z[38]=n<T>(27,4) - z[39];
    z[38]=z[38]*z[15];
    z[24]= - static_cast<T>(17)- z[24];
    z[24]=z[7]*z[24];
    z[24]= - static_cast<T>(77)+ z[24];
    z[24]=z[7]*z[24];
    z[24]=n<T>(11,2) + z[24];
    z[24]=z[24]*z[74];
    z[64]= - n<T>(19,4) - z[53];
    z[64]=z[7]*z[64];
    z[64]=n<T>(5,2) + z[64];
    z[64]=z[7]*z[64];
    z[24]=z[64] + z[24];
    z[24]=z[11]*z[24];
    z[24]=z[28] + z[38] + z[24];
    z[25]= - static_cast<T>(4)- z[25];
    z[25]=z[7]*z[25];
    z[20]= - z[29]*z[20];
    z[20]=static_cast<T>(7)+ z[20];
    z[20]=z[7]*z[20];
    z[20]=n<T>(103,8) + z[20];
    z[20]=z[20]*z[15];
    z[20]=static_cast<T>(2)+ z[20];
    z[20]=z[11]*z[20];
    z[20]=z[20] - n<T>(29,8) + z[25];
    z[20]=z[11]*z[20];
    z[25]= - static_cast<T>(5)- z[46];
    z[20]= - z[31] + n<T>(9,8)*z[25] + z[20];
    z[20]=z[2]*z[20];
    z[25]=static_cast<T>(11)+ z[37];
    z[25]=z[25]*z[27];
    z[25]=static_cast<T>(49)+ z[25];
    z[25]=z[7]*z[25];
    z[25]= - static_cast<T>(21)+ z[25];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(5,2) + z[25];
    z[25]=z[11]*z[25];
    z[28]=n<T>(121,2) + 21*z[7];
    z[28]=z[7]*z[28];
    z[28]=n<T>(89,2) + z[28];
    z[28]=z[7]*z[28];
    z[25]=z[25] + n<T>(85,2) + z[28];
    z[25]=z[25]*z[42];
    z[25]=z[25] + n<T>(45,2) + z[51];
    z[20]=n<T>(1,4)*z[25] + z[20];
    z[20]=z[2]*z[20];
    z[20]=n<T>(3,2)*z[22] + n<T>(1,2)*z[24] + z[20];
    z[20]=z[8]*z[20];
    z[22]=z[71]*z[39];
    z[22]= - static_cast<T>(43)+ z[22];
    z[24]=n<T>(7,4) + z[7];
    z[24]=z[24]*z[37];
    z[24]= - static_cast<T>(11)+ z[24];
    z[24]=z[7]*z[24];
    z[25]=static_cast<T>(11)+ n<T>(9,4)*z[7];
    z[25]=z[7]*z[25];
    z[25]=n<T>(51,4) + z[25];
    z[25]=z[7]*z[25];
    z[25]=n<T>(3,2) + z[25];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(5,2) + z[25];
    z[25]=z[11]*z[25];
    z[24]=z[25] - n<T>(59,4) + z[24];
    z[24]=z[11]*z[24];
    z[22]=n<T>(1,4)*z[22] + z[24];
    z[24]= - n<T>(21,2) - z[7];
    z[24]=z[24]*z[15];
    z[24]= - static_cast<T>(9)+ z[24];
    z[24]=z[7]*z[24];
    z[24]= - n<T>(17,4) + z[24];
    z[25]= - n<T>(1,2) + z[27];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(39,2) + z[25];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(51,2) + z[25];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(19,2) + z[25];
    z[25]=z[25]*z[42];
    z[24]=5*z[24] + z[25];
    z[24]=z[24]*z[42];
    z[25]=z[40] + n<T>(3,2);
    z[25]=z[25]*z[7];
    z[25]=z[25] + 1;
    z[25]=z[25]*z[7];
    z[25]=z[25] + n<T>(1,4);
    z[25]=z[25]*z[11];
    z[25]=z[25] + z[44];
    z[25]=z[25]*z[11];
    z[25]=z[25] + n<T>(3,2)*z[23];
    z[25]=z[25]*z[3];
    z[28]=2*z[7];
    z[29]= - n<T>(39,4) - z[28];
    z[29]=z[7]*z[29];
    z[24]= - n<T>(7,2)*z[25] + z[24] - n<T>(31,4) + z[29];
    z[24]=z[3]*z[24];
    z[22]=n<T>(1,2)*z[22] + z[24];
    z[22]=z[3]*z[22];
    z[24]= - z[71]*z[15];
    z[29]= - z[7]*z[33];
    z[29]=n<T>(1,2) + z[29];
    z[29]=z[7]*z[29];
    z[29]=n<T>(1,2) + z[29];
    z[29]=z[29]*z[42];
    z[31]=n<T>(3,2) - z[69];
    z[31]=z[7]*z[31];
    z[29]=z[29] + n<T>(5,4) + z[31];
    z[29]=z[11]*z[29];
    z[24]=z[25] + z[29] + static_cast<T>(1)+ z[24];
    z[24]=z[3]*z[24];
    z[25]= - static_cast<T>(3)+ z[51];
    z[25]=z[25]*z[15];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[36];
    z[29]=z[7] - n<T>(1,2);
    z[31]=z[29]*z[7];
    z[33]= - static_cast<T>(1)+ z[31];
    z[33]=z[7]*z[33];
    z[25]=z[25] + n<T>(1,2) + z[33];
    z[25]=z[11]*z[25];
    z[33]=static_cast<T>(1)+ z[72];
    z[25]=n<T>(1,2)*z[33] + z[25];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[6]*z[24];
    z[25]= - n<T>(5,8) - z[7];
    z[25]=z[7]*z[25];
    z[25]=n<T>(19,8) + z[25];
    z[25]=z[7]*z[25];
    z[25]=static_cast<T>(2)+ z[25];
    z[25]=z[25]*z[36];
    z[33]= - n<T>(1,4) - z[28];
    z[33]=z[7]*z[33];
    z[33]=n<T>(11,8) + z[33];
    z[33]=z[7]*z[33];
    z[25]=z[25] - static_cast<T>(1)+ z[33];
    z[25]=z[11]*z[25];
    z[33]=n<T>(3,8) - z[7];
    z[33]=z[7]*z[33];
    z[22]=z[24] + z[22] + z[25] - n<T>(3,4) + z[33];
    z[22]=z[6]*z[22];
    z[24]= - static_cast<T>(31)- n<T>(1241,36)*z[7];
    z[24]=z[7]*z[24];
    z[24]= - n<T>(61,2) + z[24];
    z[24]=z[24]*z[15];
    z[25]= - static_cast<T>(1897)+ 545*z[7];
    z[25]=z[7]*z[25];
    z[25]= - static_cast<T>(8)+ n<T>(1,72)*z[25];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(59,12) + z[25];
    z[25]=z[7]*z[25];
    z[25]= - n<T>(37,24) + z[25];
    z[25]=z[11]*z[25];
    z[24]=z[25] - n<T>(17,3) + z[24];
    z[24]=z[11]*z[24];
    z[24]= - n<T>(29,9)*z[59] - n<T>(31,4)*z[23] + z[24];
    z[24]=z[3]*z[24];
    z[25]=static_cast<T>(151)+ n<T>(1493,24)*z[7];
    z[25]=z[25]*z[41];
    z[33]=n<T>(3041,2) - z[70];
    z[33]=z[33]*z[18];
    z[33]=n<T>(611,2) + z[33];
    z[18]=z[33]*z[18];
    z[18]=n<T>(185,2) + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(63,2) + z[18];
    z[18]=z[18]*z[67];
    z[33]=static_cast<T>(803)+ 49*z[7];
    z[33]=z[7]*z[33];
    z[33]=static_cast<T>(64)+ n<T>(5,72)*z[33];
    z[33]=z[7]*z[33];
    z[18]=z[18] + n<T>(625,24) + z[33];
    z[18]=z[11]*z[18];
    z[18]=z[24] + z[18] + static_cast<T>(14)+ z[25];
    z[18]=z[3]*z[18];
    z[24]= - n<T>(1225,9) + z[66];
    z[19]=z[24]*z[19];
    z[19]= - n<T>(59,3) + z[19];
    z[19]=z[7]*z[19];
    z[19]= - n<T>(49,4) + z[19];
    z[19]=z[7]*z[19];
    z[19]= - n<T>(5,2) + z[19];
    z[19]=z[11]*z[19];
    z[24]= - n<T>(749,9) + z[66];
    z[24]=z[7]*z[24];
    z[24]=n<T>(23,4) + z[24];
    z[24]=z[7]*z[24];
    z[24]=n<T>(205,6) + z[24];
    z[19]=n<T>(1,2)*z[24] + z[19];
    z[19]=z[11]*z[19];
    z[24]= - n<T>(89,18) + z[27];
    z[24]=z[7]*z[24];
    z[24]=n<T>(83,2) + z[24];
    z[19]=n<T>(1,2)*z[24] + z[19];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[3]*z[18];
    z[19]=z[11] + 3;
    z[24]=z[19]*z[11];
    z[24]=z[24] - z[55];
    z[24]=z[24]*z[3];
    z[25]=z[7] - 5;
    z[33]=z[11] + n<T>(9,4);
    z[33]=z[33]*z[11];
    z[25]= - z[33] + n<T>(1,4)*z[25];
    z[33]= - z[24] - z[25];
    z[17]=z[33]*z[17];
    z[19]=z[19]*z[42];
    z[19]=z[19] - z[48];
    z[33]=z[19]*z[50]*z[49];
    z[17]=z[17] + z[33];
    z[17]=z[2]*z[17];
    z[33]=z[3]*z[19];
    z[33]=z[33] + z[25];
    z[33]=z[3]*z[33];
    z[38]=static_cast<T>(23)+ 13*z[11];
    z[38]=z[38]*z[42];
    z[38]=z[38] - z[15] + 5;
    z[39]=n<T>(1,4)*z[38];
    z[33]=z[39] + 3*z[33];
    z[33]=z[3]*z[33];
    z[17]=z[33] + z[17];
    z[17]=z[2]*z[17];
    z[24]= - 3*z[25] - z[24];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(1,2)*z[38] + z[24];
    z[24]=z[3]*z[24];
    z[33]=static_cast<T>(11)+ 7*z[11];
    z[33]=z[33]*z[67];
    z[33]=z[33] + 1;
    z[33]=n<T>(1,2)*z[33];
    z[17]=z[17] + z[33] + z[24];
    z[17]=z[2]*z[17];
    z[19]=z[19]*z[52];
    z[19]=z[19] + z[25];
    z[19]=z[3]*z[19];
    z[19]=z[39] + z[19];
    z[19]=z[3]*z[19];
    z[17]=z[17] - z[33] + z[19];
    z[17]=z[12]*z[17];
    z[19]=z[23]*z[51];
    z[23]=z[44]*z[62];
    z[19]=3*z[19] + z[23];
    z[19]=z[11]*z[19];
    z[23]= - z[49] + z[45];
    z[23]=z[51]*z[23];
    z[19]=z[19] + z[23];
    z[19]=z[19]*z[60];
    z[23]= - z[61] + z[63];
    z[23]=z[35]*z[23];
    z[24]= - static_cast<T>(3)- n<T>(7,4)*z[7];
    z[24]=z[7]*z[24];
    z[24]= - n<T>(3,4) + z[24];
    z[24]=z[7]*z[24];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[24]*z[74];
    z[25]= - n<T>(7,8) - z[7];
    z[25]=z[7]*z[25];
    z[25]=n<T>(1,8) + z[25];
    z[25]=z[7]*z[25];
    z[24]=z[25] + z[24];
    z[24]=z[11]*z[24];
    z[19]=z[19] + z[24] + z[23];
    z[19]=z[4]*z[19];
    z[23]= - z[47]*z[15];
    z[24]=z[47]*z[7];
    z[25]=n<T>(7,2) - z[24];
    z[25]=z[25]*z[62];
    z[24]=n<T>(5,4) - z[24];
    z[24]=z[7]*z[24];
    z[24]=z[24] + z[25];
    z[24]=z[11]*z[24];
    z[23]=z[23] + z[24];
    z[19]=n<T>(1,4)*z[23] + z[19];
    z[19]=z[10]*z[19];
    z[23]=z[68]*z[57];
    z[24]=n<T>(3,4) + z[28];
    z[24]=z[7]*z[24];
    z[23]=z[24] + z[23];
    z[23]=z[11]*z[23];
    z[24]= - z[58]*z[62];
    z[25]= - n<T>(1,2) - z[7];
    z[25]=z[7]*z[25];
    z[24]=z[25] + z[24];
    z[24]=z[11]*z[24];
    z[24]= - z[15] + z[24];
    z[24]=z[24]*z[56];
    z[23]=z[24] + z[7] + z[23];
    z[23]=z[6]*z[23];
    z[24]= - static_cast<T>(3)- z[30];
    z[24]=z[7]*z[24];
    z[24]=static_cast<T>(3)+ z[24];
    z[24]=z[11]*z[24];
    z[24]= - z[30] + z[24];
    z[25]=z[12]*z[65];
    z[24]= - n<T>(3,8)*z[25] + n<T>(1,8)*z[24];
    z[24]=z[11]*z[24];
    z[25]=z[71]*z[62];
    z[25]=z[31] + z[25];
    z[25]=z[11]*z[25];
    z[15]=z[15] + z[25];
    z[15]=z[10]*z[15];
    z[15]=n<T>(1,4)*z[15] + z[23] + z[24];
    z[15]=z[9]*z[15];
    z[23]=z[58]*z[42];
    z[23]=z[23] + static_cast<T>(3)+ n<T>(5,2)*z[7];
    z[23]=z[11]*z[23];
    z[23]=n<T>(5,2) + z[23];
    z[24]=z[65]*z[34];
    z[25]= - static_cast<T>(1)- z[37];
    z[25]=n<T>(1,4)*z[25] + z[11];
    z[25]=z[11]*z[25];
    z[24]= - n<T>(5,4)*z[24] - n<T>(5,4) + z[25];
    z[24]=z[2]*z[24];
    z[25]=z[36]*z[58];
    z[25]=z[25] + z[7];
    z[28]= - z[10]*z[25]*z[67];
    z[23]=z[28] + n<T>(1,2)*z[23] + z[24];
    z[23]=z[14]*z[23];
    z[24]=static_cast<T>(1)- z[4];
    z[24]=z[54]*z[7]*z[24];
    z[28]= - z[4]*z[29];
    z[28]=z[28] + z[71];
    z[28]=z[4]*z[28];
    z[29]= - static_cast<T>(2)+ z[4];
    z[29]=z[4]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[1]*z[29];
    z[28]=z[29] + n<T>(1,2) + z[28];
    z[28]=z[1]*z[28];
    z[24]=z[24] + z[28];
    z[24]=z[5]*z[24];
    z[25]= - z[11]*z[25];
    z[28]=z[2]*z[58]*z[43];
    z[29]= - static_cast<T>(1)+ z[51];
    z[29]=z[11]*z[29];
    z[29]=z[7] + z[29];
    z[29]=z[11]*z[29];
    z[28]=z[29] + z[28];
    z[28]=z[2]*z[28];
    z[25]=z[25] + z[28];
    z[25]=z[13]*z[25];
    z[28]= - static_cast<T>(7)- z[32];
    z[27]=z[28]*z[27];
    z[27]=static_cast<T>(7)+ z[27];
    z[27]=z[11]*z[27];
    z[28]= - n<T>(27,2) - z[53];
    z[28]=z[7]*z[28];
    z[27]=z[27] + static_cast<T>(11)+ z[28];
    z[27]=z[27]*z[42];
    z[27]=z[27] + n<T>(47,6) - z[37];

    r += z[15] + z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22]
       + n<T>(1,2)*z[23] + n<T>(1,3)*z[24] + n<T>(1,6)*z[25] + z[26] + n<T>(1,4)*z[27];
 
    return r;
}

template double qqb_2lha_r512(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r512(const std::array<dd_real,30>&);
#endif
