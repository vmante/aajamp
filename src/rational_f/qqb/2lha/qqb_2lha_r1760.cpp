#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1760(const std::array<T,30>& k) {
  T z[67];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[20];
    z[6]=k[6];
    z[7]=k[17];
    z[8]=k[2];
    z[9]=k[4];
    z[10]=npow(z[1],2);
    z[11]=n<T>(1,2)*z[10];
    z[12]=11*z[1];
    z[13]= - static_cast<T>(5)+ z[12];
    z[13]=z[13]*z[11];
    z[14]=5*z[2];
    z[15]= - z[10]*z[14];
    z[13]=z[13] + z[15];
    z[13]=z[2]*z[13];
    z[15]=npow(z[1],3);
    z[16]=n<T>(1,2)*z[15];
    z[17]=n<T>(21,2)*z[1];
    z[18]= - static_cast<T>(5)+ z[17];
    z[18]=z[18]*z[16];
    z[19]= - z[15]*z[14];
    z[18]=z[18] + z[19];
    z[18]=z[3]*z[18];
    z[19]=n<T>(3,2)*z[10];
    z[20]=static_cast<T>(5)- n<T>(27,2)*z[1];
    z[20]=z[20]*z[19];
    z[13]=z[18] + z[20] + z[13];
    z[13]=z[3]*z[13];
    z[18]=z[2] - 1;
    z[20]= - z[15]*z[18];
    z[21]=npow(z[1],4);
    z[22]=n<T>(1,2)*z[3];
    z[23]= - z[21]*z[22];
    z[20]=2*z[20] + z[23];
    z[20]=z[3]*z[20];
    z[23]=z[10]*z[2];
    z[24]=2*z[10] - z[23];
    z[24]=z[2]*z[24];
    z[24]= - z[10] + z[24];
    z[20]=3*z[24] + z[20];
    z[24]=5*z[6];
    z[20]=z[20]*z[24];
    z[25]=15*z[1];
    z[26]=2*z[1];
    z[27]= - n<T>(1,2) + z[26];
    z[27]=z[27]*z[25];
    z[28]=n<T>(1,2)*z[1];
    z[29]=n<T>(19,2)*z[1];
    z[30]= - static_cast<T>(35)- z[29];
    z[30]=z[30]*z[28];
    z[31]=z[2]*z[1];
    z[30]=z[30] + 5*z[31];
    z[30]=z[2]*z[30];
    z[32]=static_cast<T>(20)- n<T>(43,2)*z[1];
    z[32]=z[1]*z[32];
    z[30]=z[32] + z[30];
    z[30]=z[2]*z[30];
    z[32]=n<T>(1,4)*z[3];
    z[33]=z[32]*z[21];
    z[33]=z[33] - z[15];
    z[33]=z[33]*z[3];
    z[19]=z[33] + z[19];
    z[33]=z[5]*z[19];
    z[13]=z[20] - n<T>(5,2)*z[33] + z[13] + z[27] + z[30];
    z[13]=z[6]*z[13];
    z[20]=3*z[1];
    z[27]=n<T>(17,4) - z[20];
    z[27]=z[27]*z[15];
    z[30]= - static_cast<T>(5)+ n<T>(11,2)*z[1];
    z[30]=z[30]*z[10];
    z[23]=z[30] - n<T>(5,2)*z[23];
    z[23]=z[2]*z[23];
    z[23]=z[27] + z[23];
    z[23]=z[3]*z[23];
    z[27]=n<T>(1,4)*z[10];
    z[30]= - static_cast<T>(53)+ 39*z[1];
    z[30]=z[30]*z[27];
    z[33]=6*z[1];
    z[34]=n<T>(5,2) - z[33];
    z[34]=z[1]*z[34];
    z[34]=z[34] - n<T>(5,2)*z[31];
    z[34]=z[2]*z[34];
    z[23]=z[23] + z[30] + z[34];
    z[23]=z[3]*z[23];
    z[30]=7*z[1];
    z[34]= - static_cast<T>(5)+ z[30];
    z[34]=z[34]*z[15]*z[32];
    z[35]=z[1] - n<T>(5,4);
    z[36]=3*z[10];
    z[37]= - z[35]*z[36];
    z[34]=z[37] + z[34];
    z[34]=z[3]*z[34];
    z[37]=3*z[5];
    z[19]=z[19]*z[37];
    z[38]=z[1] + n<T>(5,2);
    z[39]=n<T>(3,2)*z[1];
    z[40]= - z[38]*z[39];
    z[34]= - z[19] + z[40] + z[34];
    z[40]=n<T>(1,2)*z[5];
    z[34]=z[34]*z[40];
    z[41]=13*z[1];
    z[42]= - static_cast<T>(57)- z[41];
    z[42]=z[42]*z[28];
    z[42]=static_cast<T>(5)+ z[42];
    z[43]=z[1] + n<T>(3,2);
    z[44]=n<T>(1,2)*z[2];
    z[45]= - z[44] + z[43];
    z[45]=z[2]*z[45];
    z[46]=z[43]*z[1];
    z[47]= - static_cast<T>(3)- z[46];
    z[45]=n<T>(1,2)*z[47] + z[45];
    z[45]=z[45]*z[14];
    z[42]=n<T>(1,2)*z[42] + z[45];
    z[42]=z[2]*z[42];
    z[12]=n<T>(119,4) - z[12];
    z[12]=z[12]*z[28];
    z[12]=z[13] + z[34] + z[23] + z[12] + z[42];
    z[12]=z[6]*z[12];
    z[13]=n<T>(9,4) - z[30];
    z[13]=z[13]*z[10];
    z[23]=z[1] - n<T>(1,2);
    z[34]=z[15]*z[3];
    z[42]=z[23]*z[34];
    z[13]=z[13] + n<T>(3,2)*z[42];
    z[13]=z[13]*z[22];
    z[42]= - n<T>(3,8) + z[26];
    z[42]=z[42]*z[20];
    z[13]= - z[19] + z[42] + z[13];
    z[13]=z[5]*z[13];
    z[19]=static_cast<T>(11)+ z[1];
    z[19]=z[19]*z[34];
    z[19]= - 11*z[10] + z[19];
    z[19]=z[19]*z[22];
    z[42]= - n<T>(11,2) - z[25];
    z[42]=z[1]*z[42];
    z[19]=z[42] + z[19];
    z[13]=n<T>(1,4)*z[19] + z[13];
    z[13]=z[5]*z[13];
    z[19]=n<T>(1,4)*z[1];
    z[42]= - static_cast<T>(2)+ z[19];
    z[42]=z[1]*z[42];
    z[42]= - n<T>(7,8) + z[42];
    z[42]=z[42]*z[10];
    z[45]=z[28]*z[2];
    z[47]= - z[10] + z[45];
    z[48]=z[28] - 5;
    z[47]=z[2]*z[48]*z[47];
    z[42]=z[42] + z[47];
    z[42]=z[3]*z[42];
    z[47]= - static_cast<T>(11)+ z[39];
    z[47]=z[1]*z[47];
    z[47]=z[47] - z[45];
    z[47]=z[2]*z[47];
    z[48]=n<T>(29,2) - z[1];
    z[48]=z[1]*z[48];
    z[48]=n<T>(5,4) + z[48];
    z[48]=z[1]*z[48];
    z[47]=z[48] + z[47];
    z[42]=n<T>(1,2)*z[47] + z[42];
    z[42]=z[3]*z[42];
    z[47]=3*z[2];
    z[48]= - z[47] + n<T>(53,8) + z[33];
    z[48]=z[2]*z[48];
    z[49]= - n<T>(35,4) - z[20];
    z[49]=z[1]*z[49];
    z[48]=z[48] - n<T>(17,4) + z[49];
    z[48]=z[2]*z[48];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[1]*z[17];
    z[17]=n<T>(5,2) + z[17];
    z[12]=z[12] + z[13] + z[42] + n<T>(1,4)*z[17] + z[48];
    z[12]=z[6]*z[12];
    z[13]=z[1] + n<T>(15,8);
    z[13]=z[13]*z[1];
    z[13]=z[13] - z[45];
    z[13]=z[13]*z[2];
    z[17]=z[1] + n<T>(21,4);
    z[42]=z[17]*z[28];
    z[42]=static_cast<T>(2)+ z[42];
    z[42]=z[1]*z[42];
    z[42]=z[42] - z[13];
    z[42]=z[2]*z[42];
    z[48]=z[43]*z[20];
    z[49]= - n<T>(7,2) - z[48];
    z[49]=z[49]*z[19];
    z[42]=z[49] + z[42];
    z[42]=z[2]*z[42];
    z[49]=z[1] + n<T>(13,2);
    z[49]=z[49]*z[1];
    z[50]= - n<T>(19,2) - z[49];
    z[50]=z[50]*z[28];
    z[50]= - static_cast<T>(1)+ z[50];
    z[50]=z[1]*z[50];
    z[48]=z[48] - z[31];
    z[48]=z[48]*z[44];
    z[51]=z[39] + 5;
    z[51]=z[51]*z[1];
    z[52]=n<T>(13,4) + z[51];
    z[52]=z[1]*z[52];
    z[52]=z[52] - z[48];
    z[52]=z[2]*z[52];
    z[50]=z[50] + z[52];
    z[50]=z[2]*z[50];
    z[52]=z[1] + n<T>(7,2);
    z[52]=z[52]*z[28];
    z[53]= - static_cast<T>(1)+ z[52];
    z[53]=z[53]*z[10];
    z[50]=z[53] + z[50];
    z[50]=z[2]*z[50];
    z[53]=static_cast<T>(9)- z[1];
    z[53]=z[53]*z[28];
    z[53]= - static_cast<T>(3)+ z[53];
    z[53]=z[53]*z[11];
    z[50]=z[53] + z[50];
    z[50]=z[50]*z[22];
    z[53]=z[28] - 1;
    z[54]= - z[53]*z[11];
    z[42]=z[50] + z[54] + z[42];
    z[42]=z[3]*z[42];
    z[50]=z[28] + 1;
    z[54]=z[50]*z[1];
    z[55]=z[54] + n<T>(1,2);
    z[56]=z[55]*z[28];
    z[57]=z[46] - z[45];
    z[57]=z[57]*z[44];
    z[58]=z[19] + 1;
    z[58]=z[58]*z[1];
    z[59]= - n<T>(3,4) - z[58];
    z[59]=z[1]*z[59];
    z[57]=z[59] + z[57];
    z[57]=z[2]*z[57];
    z[56]=z[56] + z[57];
    z[56]=z[2]*z[56];
    z[55]= - z[55]*z[20];
    z[57]=z[1] + 1;
    z[59]=z[57]*z[20];
    z[59]=z[59] - z[31];
    z[59]=z[59]*z[44];
    z[55]=z[55] + z[59];
    z[55]=z[2]*z[55];
    z[59]=z[1] + 3;
    z[60]=z[1]*z[59];
    z[60]=static_cast<T>(3)+ z[60];
    z[60]=z[1]*z[60];
    z[60]=static_cast<T>(1)+ z[60];
    z[60]=z[60]*z[28];
    z[55]=z[60] + z[55];
    z[55]=z[55]*npow(z[2],2)*z[32];
    z[55]=z[56] + z[55];
    z[55]=z[3]*z[55];
    z[56]=z[38] - z[2];
    z[56]=z[56]*z[44];
    z[60]= - z[56] + z[57];
    z[60]=z[2]*z[60];
    z[61]=n<T>(1,2)*z[9];
    z[62]= - static_cast<T>(1)+ z[44];
    z[62]=z[2]*z[62];
    z[62]=n<T>(1,2) + z[62];
    z[62]=z[62]*z[61];
    z[63]=z[1] + n<T>(1,2);
    z[60]=z[62] - n<T>(1,2)*z[63] + z[60];
    z[60]=z[60]*z[61];
    z[62]= - 3*z[57] + z[2];
    z[62]=z[2]*z[62];
    z[58]=n<T>(1,8)*z[62] + n<T>(3,8) + z[58];
    z[58]=z[2]*z[58];
    z[54]=z[58] - n<T>(1,8) - z[54];
    z[54]=z[2]*z[54];
    z[58]=z[1]*z[57];
    z[54]=z[60] + z[55] + n<T>(3,8)*z[58] + z[54];
    z[54]=z[4]*z[54];
    z[55]= - z[57]*z[28];
    z[58]=z[1] - 1;
    z[60]=z[58]*z[44];
    z[55]=z[60] + static_cast<T>(1)+ z[55];
    z[55]=z[2]*z[55];
    z[60]= - n<T>(3,2) + z[1];
    z[60]=z[1]*z[60];
    z[60]= - static_cast<T>(1)+ z[60];
    z[55]=n<T>(1,2)*z[60] + z[55];
    z[55]=z[2]*z[55];
    z[60]=z[39] + 1;
    z[62]=z[60]*z[28];
    z[55]=z[62] + z[55];
    z[62]=z[58]*z[10];
    z[64]=z[10]*z[3];
    z[65]= - n<T>(15,4) + z[1];
    z[65]=z[1]*z[65];
    z[65]=n<T>(3,2) + z[65];
    z[65]=z[65]*z[64];
    z[62]=z[62] + z[65];
    z[62]=z[3]*z[62];
    z[65]=static_cast<T>(3)- n<T>(5,2)*z[1];
    z[65]=z[65]*z[34];
    z[65]= - z[15] + z[65];
    z[65]=z[3]*z[65];
    z[21]=npow(z[3],2)*z[21]*z[6];
    z[65]=z[65] + z[21];
    z[66]=n<T>(1,2)*z[6];
    z[65]=z[65]*z[66];
    z[62]=z[62] + z[65];
    z[62]=z[62]*z[66];
    z[18]= - z[18]*z[61];
    z[43]= - z[2] + z[43];
    z[43]=z[2]*z[43];
    z[18]=z[18] + n<T>(3,4)*z[43] - n<T>(3,8) - z[1];
    z[18]=z[9]*z[18];
    z[18]=z[54] + z[18] + z[62] + n<T>(1,2)*z[55] + z[42];
    z[18]=z[4]*z[18];
    z[42]=n<T>(47,2) + z[20];
    z[42]=z[42]*z[19];
    z[42]=static_cast<T>(2)+ z[42];
    z[42]=z[1]*z[42];
    z[43]= - n<T>(31,4) - z[20];
    z[43]=z[1]*z[43];
    z[43]=z[43] + n<T>(3,2)*z[31];
    z[43]=z[43]*z[44];
    z[42]=z[42] + z[43];
    z[42]=z[2]*z[42];
    z[43]= - n<T>(1,8) - z[26];
    z[43]=z[1]*z[43];
    z[43]=n<T>(9,8) + z[43];
    z[43]=z[1]*z[43];
    z[42]=z[43] + z[42];
    z[42]=z[2]*z[42];
    z[43]= - static_cast<T>(5)- z[28];
    z[43]=z[1]*z[43];
    z[43]= - n<T>(5,2) + z[43];
    z[43]=z[43]*z[10];
    z[50]= - z[50]*z[20];
    z[50]=z[50] + z[45];
    z[50]=z[2]*z[50];
    z[54]=static_cast<T>(7)+ z[39];
    z[54]=z[1]*z[54];
    z[54]=n<T>(5,2) + z[54];
    z[54]=z[1]*z[54];
    z[50]=z[54] + z[50];
    z[50]=z[2]*z[50];
    z[43]=z[43] + z[50];
    z[43]=z[2]*z[43];
    z[50]= - static_cast<T>(3)+ z[10];
    z[50]=z[50]*z[10];
    z[43]=z[50] + z[43];
    z[43]=z[2]*z[43];
    z[43]=3*z[15] + z[43];
    z[43]=z[3]*z[43];
    z[50]= - n<T>(25,2) - z[20];
    z[27]=z[50]*z[27];
    z[27]=n<T>(3,4)*z[43] + z[27] + z[42];
    z[27]=z[3]*z[27];
    z[23]=z[23]*z[1];
    z[42]= - n<T>(1,2) + z[23];
    z[42]=z[42]*z[28];
    z[43]=z[1] + n<T>(1,4);
    z[50]= - z[1]*z[43];
    z[50]=z[50] + z[45];
    z[50]=z[2]*z[50];
    z[42]=z[42] + z[50];
    z[42]=z[2]*z[42];
    z[50]=z[57]*z[11];
    z[42]=z[50] + z[42];
    z[42]=z[2]*z[42];
    z[50]=z[20] + 1;
    z[54]=z[50]*z[10];
    z[55]= - z[1]*z[50];
    z[55]=z[55] + z[31];
    z[55]=z[2]*z[55];
    z[54]=z[54] + z[55];
    z[54]=z[54]*z[44];
    z[55]=z[58]*z[28];
    z[61]=static_cast<T>(1)- z[55];
    z[61]=z[61]*z[10];
    z[54]=z[61] + z[54];
    z[54]=z[2]*z[54];
    z[59]= - z[59]*z[16];
    z[54]=z[59] + z[54];
    z[54]=z[2]*z[54];
    z[16]=z[58]*z[16];
    z[16]=z[16] + z[54];
    z[16]=z[16]*z[22];
    z[16]=z[16] + n<T>(3,4)*z[10] + z[42];
    z[16]=z[3]*z[16];
    z[42]=z[63]*z[20];
    z[54]= - z[44] + z[60];
    z[54]=z[2]*z[54];
    z[38]=z[38]*z[1];
    z[54]=z[54] - n<T>(1,2) - z[38];
    z[54]=z[2]*z[54];
    z[38]=z[38] + z[54];
    z[38]=z[2]*z[38];
    z[38]= - z[42] + z[38];
    z[16]=n<T>(1,2)*z[38] + z[16];
    z[16]=z[16]*z[37];
    z[38]=z[2] + static_cast<T>(7)+ z[1];
    z[38]=z[38]*z[44];
    z[54]= - static_cast<T>(9)- z[1];
    z[54]=z[1]*z[54];
    z[38]=z[38] - n<T>(17,2) + z[54];
    z[54]=n<T>(1,4)*z[2];
    z[38]=z[38]*z[54];
    z[38]=z[38] + n<T>(9,8) + z[46];
    z[38]=z[2]*z[38];
    z[46]=z[20] - n<T>(1,2);
    z[46]=z[46]*z[1];
    z[16]=z[16] + z[27] + z[46] + z[38];
    z[16]=z[5]*z[16];
    z[17]=z[1]*z[17];
    z[17]=static_cast<T>(7)+ z[17];
    z[17]=z[17]*z[28];
    z[13]=z[17] - z[13];
    z[13]=z[2]*z[13];
    z[17]= - n<T>(29,2) - z[20];
    z[17]=z[17]*z[28];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[28];
    z[13]=z[17] + z[13];
    z[13]=z[2]*z[13];
    z[17]= - n<T>(39,2) - z[49];
    z[17]=z[1]*z[17];
    z[17]= - static_cast<T>(7)+ z[17];
    z[17]=z[17]*z[28];
    z[27]=n<T>(21,4) + z[51];
    z[27]=z[1]*z[27];
    z[27]=z[27] - z[48];
    z[27]=z[2]*z[27];
    z[17]=z[17] + z[27];
    z[17]=z[2]*z[17];
    z[27]=n<T>(23,2) + z[1];
    z[27]=z[1]*z[27];
    z[27]=n<T>(15,2) + z[27];
    z[27]=z[27]*z[11];
    z[17]=z[27] + z[17];
    z[17]=z[2]*z[17];
    z[27]=5*z[1];
    z[38]= - static_cast<T>(3)- z[27];
    z[38]=z[38]*z[28];
    z[38]=static_cast<T>(5)+ z[38];
    z[11]=z[38]*z[11];
    z[11]=z[11] + z[17];
    z[11]=z[11]*z[22];
    z[17]= - n<T>(5,2) + z[46];
    z[17]=z[17]*z[19];
    z[11]=z[11] + z[17] + z[13];
    z[11]=z[3]*z[11];
    z[13]=static_cast<T>(1)+ z[52];
    z[17]= - static_cast<T>(5)- z[1];
    z[17]=z[1]*z[17];
    z[19]=z[2]*z[57];
    z[17]=z[19] - n<T>(9,2) + z[17];
    z[17]=z[17]*z[44];
    z[13]=3*z[13] + z[17];
    z[13]=z[2]*z[13];
    z[17]= - static_cast<T>(9)- z[27];
    z[17]=z[1]*z[17];
    z[17]= - n<T>(5,2) + z[17];
    z[13]=n<T>(1,2)*z[17] + z[13];
    z[11]=z[16] + n<T>(1,2)*z[13] + z[11];
    z[11]=z[5]*z[11];
    z[13]=z[2] - z[1];
    z[16]= - n<T>(7,2) + z[13];
    z[14]=z[16]*z[14];
    z[16]=static_cast<T>(45)- z[29];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[2]*z[14];
    z[14]=z[14] - n<T>(25,2) + 27*z[1];
    z[14]=z[2]*z[14];
    z[16]=z[20] - z[31];
    z[16]=z[2]*z[16];
    z[16]= - z[20] + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[1] + z[16];
    z[16]=z[6]*z[16];
    z[17]=z[5]*z[1];
    z[19]=static_cast<T>(5)- n<T>(79,2)*z[1];
    z[14]=10*z[16] + n<T>(5,2)*z[17] + n<T>(1,2)*z[19] + z[14];
    z[14]=z[6]*z[14];
    z[16]=n<T>(121,2) + 21*z[1];
    z[19]=6*z[2] - n<T>(65,4) - z[33];
    z[19]=z[2]*z[19];
    z[16]=n<T>(1,4)*z[16] + z[19];
    z[16]=z[2]*z[16];
    z[19]=z[5]*z[20];
    z[19]=5*z[43] + z[19];
    z[19]=z[19]*z[40];
    z[27]= - static_cast<T>(11)- z[39];
    z[14]=z[14] + z[19] + n<T>(1,2)*z[27] + z[16];
    z[14]=z[6]*z[14];
    z[16]=n<T>(7,2) + z[13];
    z[16]=z[16]*z[54];
    z[19]=n<T>(1,4) - z[20];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[17]*z[37];
    z[17]=z[17] + n<T>(11,8) + z[30];
    z[17]=z[5]*z[17];
    z[14]=z[14] + z[17] + z[16] - n<T>(9,4) - 4*z[1];
    z[14]=z[6]*z[14];
    z[13]=n<T>(1,2) + z[13];
    z[13]=z[13]*z[54];
    z[13]=z[13] - n<T>(11,8) + z[1];
    z[13]=z[2]*z[13];
    z[16]=z[56] - z[60];
    z[16]=z[2]*z[16];
    z[16]=z[16] + n<T>(1,4) + z[26];
    z[16]=z[16]*z[37];
    z[17]=n<T>(11,2) - z[25];
    z[13]=z[16] + n<T>(1,4)*z[17] + z[13];
    z[13]=z[5]*z[13];
    z[16]=z[47] - n<T>(19,2) - z[20];
    z[16]=z[2]*z[16];
    z[16]=z[16] + static_cast<T>(11)+ z[41];
    z[13]=n<T>(1,4)*z[16] + z[13];
    z[13]=z[5]*z[13];
    z[16]= - static_cast<T>(3)- z[5];
    z[16]=z[16]*z[37];
    z[17]= - static_cast<T>(3)- z[2];
    z[17]=z[2]*z[17];
    z[16]=z[16] + static_cast<T>(11)+ z[17];
    z[17]=static_cast<T>(2)- z[44];
    z[17]=z[2]*z[17];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[2]*z[17];
    z[17]=static_cast<T>(2)+ z[17];
    z[17]=z[2]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[17]*z[24];
    z[19]=n<T>(77,8) - z[47];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(87,8) + z[19];
    z[19]=z[2]*z[19];
    z[17]=z[17] - n<T>(5,8)*z[5] + n<T>(39,8) + z[19];
    z[17]=z[6]*z[17];
    z[16]=n<T>(1,8)*z[16] + z[17];
    z[16]=z[6]*z[16];
    z[17]=static_cast<T>(5)- z[37];
    z[17]=z[5]*z[17];
    z[17]= - n<T>(27,2) + z[17];
    z[17]=z[5]*z[17];
    z[16]=z[16] + n<T>(1,4)*z[17] + n<T>(21,8) - z[2];
    z[16]=z[6]*z[16];
    z[17]= - static_cast<T>(3)+ z[44];
    z[17]=z[17]*z[44];
    z[19]=static_cast<T>(3)- z[2];
    z[19]=z[2]*z[19];
    z[19]= - static_cast<T>(3)+ z[19];
    z[19]=z[5]*z[19];
    z[17]=n<T>(3,2)*z[19] + static_cast<T>(3)+ z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] - n<T>(17,4) + z[2];
    z[17]=z[17]*z[40];
    z[16]=z[17] + z[16];
    z[16]=z[9]*z[16];
    z[13]=z[16] + z[13] + z[14];
    z[13]=z[9]*z[13];
    z[14]=static_cast<T>(3)+ z[28];
    z[14]=z[1]*z[14];
    z[14]= - n<T>(9,2) + z[14];
    z[14]=z[14]*z[64];
    z[14]= - n<T>(19,2)*z[10] + z[14];
    z[14]=z[14]*z[22];
    z[16]= - n<T>(9,4) + z[1];
    z[16]=z[16]*z[34];
    z[15]= - n<T>(7,2)*z[15] + z[16];
    z[15]=z[3]*z[15];
    z[15]=z[15] - n<T>(3,4)*z[21];
    z[15]=z[6]*z[15];
    z[14]=z[15] - z[36] + z[14];
    z[14]=z[6]*z[14];
    z[15]= - z[50]*z[28];
    z[16]=n<T>(5,2) - z[1];
    z[16]=z[16]*z[10];
    z[16]= - n<T>(3,2) + z[16];
    z[16]=z[3]*z[16]*z[28];
    z[17]=z[1]*z[35];
    z[17]= - n<T>(5,4) + z[17];
    z[17]=z[1]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[3]*z[16];
    z[14]=z[14] + z[15] + z[16];
    z[14]=z[7]*z[14];
    z[15]= - z[23] + z[45];
    z[15]=z[3]*z[15];
    z[16]= - z[1]*z[53];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[3]*z[1]*z[16];
    z[16]=z[55] + z[16];
    z[16]=z[7]*z[16];
    z[15]=z[16] + z[28] + z[15];
    z[15]=z[8]*z[3]*z[15];
    z[14]=z[14] + z[15];
    z[15]= - static_cast<T>(11)- z[30];
    z[15]=z[15]*z[28];
    z[16]= - z[44] + z[63];
    z[16]=z[2]*z[16];
    z[15]=z[15] + 7*z[16];
    z[10]=n<T>(1,2) - z[10];
    z[10]=z[10]*z[20];
    z[16]=z[42] - z[31];
    z[16]=z[2]*z[16];
    z[10]=z[10] + z[16];
    z[10]=z[10]*z[32];
    z[16]=n<T>(5,8) + z[1];
    z[16]=z[1]*z[16];
    z[10]=z[10] + z[16] - z[45];
    z[10]=z[3]*z[10];

    r += z[10] + z[11] + z[12] + z[13] + n<T>(1,2)*z[14] + n<T>(1,4)*z[15] + 
      z[18];
 
    return r;
}

template double qqb_2lha_r1760(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1760(const std::array<dd_real,30>&);
#endif
