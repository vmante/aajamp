#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r291(const std::array<T,30>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[29];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[2];
    z[9]=k[6];
    z[10]=k[9];
    z[11]=k[5];
    z[12]= - static_cast<T>(31)+ 9*z[3];
    z[13]= - n<T>(13,16)*z[3] + static_cast<T>(7)- n<T>(25,4)*z[6];
    z[13]=z[4]*z[13];
    z[14]=n<T>(5,8)*z[3];
    z[15]=static_cast<T>(3)- z[14];
    z[15]=z[4]*z[15];
    z[15]=n<T>(21,8)*z[3] + z[15];
    z[15]=z[1]*z[15];
    z[12]=z[15] + n<T>(1,4)*z[12] + z[13];
    z[12]=z[4]*z[12];
    z[13]=npow(z[4],2);
    z[15]=z[13]*z[1];
    z[16]=z[3]*z[15];
    z[17]= - n<T>(5,2) + z[3];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(17,2)*z[3] + 5*z[17];
    z[17]=z[4]*z[17];
    z[13]=z[13]*z[2];
    z[18]=z[3]*z[13];
    z[17]= - n<T>(5,2)*z[18] + z[17] + 3*z[16];
    z[17]=z[2]*z[17];
    z[18]=n<T>(3,2)*z[3];
    z[12]=n<T>(1,4)*z[17] - z[18] + z[12];
    z[12]=z[2]*z[12];
    z[17]=n<T>(1,8)*z[9];
    z[19]=static_cast<T>(5)+ z[9];
    z[19]=z[19]*z[17];
    z[20]=n<T>(1,4)*z[9];
    z[21]=z[20] + 3;
    z[22]=n<T>(1,2)*z[9];
    z[23]=z[6]*z[21]*z[22];
    z[19]=z[23] - static_cast<T>(3)+ z[19];
    z[19]=z[6]*z[19];
    z[23]=3*z[8];
    z[24]= - z[22] - n<T>(25,2) + z[23];
    z[25]=n<T>(1,4)*z[3];
    z[26]=static_cast<T>(1)+ 7*z[8];
    z[26]=z[26]*z[25];
    z[27]=5*z[6];
    z[28]=static_cast<T>(3)- n<T>(5,4)*z[6];
    z[28]=z[28]*z[27];
    z[28]=z[25] - n<T>(83,16) + z[28];
    z[28]=z[4]*z[28];
    z[29]= - z[25] + static_cast<T>(31)- 41*z[6];
    z[28]=n<T>(1,4)*z[29] + z[28];
    z[28]=z[4]*z[28];
    z[29]=n<T>(1,2)*z[3];
    z[30]= - n<T>(3,4) + z[6];
    z[30]=9*z[30] + z[29];
    z[30]=z[4]*z[30];
    z[31]=5*z[3];
    z[32]=static_cast<T>(63)- z[31];
    z[30]=n<T>(1,4)*z[32] + z[30];
    z[30]=z[4]*z[30];
    z[30]=n<T>(15,4)*z[3] + z[30];
    z[32]=n<T>(1,2)*z[1];
    z[30]=z[30]*z[32];
    z[12]=z[12] + z[30] + z[28] + z[26] + n<T>(1,2)*z[24] + z[19];
    z[12]=z[2]*z[12];
    z[19]=static_cast<T>(5)+ z[20];
    z[19]=z[19]*z[22];
    z[24]=static_cast<T>(11)+ z[22];
    z[24]=z[24]*z[20];
    z[24]=static_cast<T>(3)+ z[24];
    z[24]=z[6]*z[24];
    z[26]=n<T>(3,4)*z[9];
    z[28]=z[26] + static_cast<T>(5)- n<T>(27,4)*z[8];
    z[28]=z[28]*z[29];
    z[30]=3*z[6];
    z[33]= - n<T>(15,8) + z[6];
    z[33]=z[33]*z[30];
    z[33]=n<T>(5,4) + z[33];
    z[33]=z[4]*z[33];
    z[34]=z[3] + static_cast<T>(7)+ n<T>(69,2)*z[6];
    z[33]=n<T>(1,4)*z[34] + z[33];
    z[33]=z[4]*z[33];
    z[19]=z[33] + z[28] + z[24] + z[19] + n<T>(83,8) - z[23];
    z[19]=z[1]*z[19];
    z[24]= - n<T>(33,4) - z[9];
    z[24]=z[24]*z[20];
    z[28]=n<T>(5,2)*z[9];
    z[33]= - static_cast<T>(31)- z[28];
    z[17]=z[6]*z[33]*z[17];
    z[17]=z[17] + static_cast<T>(1)+ z[24];
    z[17]=z[6]*z[17];
    z[24]=static_cast<T>(41)- 23*z[6];
    z[24]=z[6]*z[24];
    z[33]=static_cast<T>(61)- n<T>(25,2)*z[6];
    z[33]=z[6]*z[33];
    z[33]= - n<T>(189,4) + z[33];
    z[33]=z[6]*z[33];
    z[33]=n<T>(13,2) + z[33];
    z[33]=z[4]*z[33];
    z[24]=z[33] - z[25] + n<T>(23,2) + z[24];
    z[33]=n<T>(1,4)*z[4];
    z[24]=z[24]*z[33];
    z[34]=static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[34]=z[34]*z[25];
    z[35]=n<T>(37,2) + z[8];
    z[35]=n<T>(1,2)*z[35] + z[9];
    z[12]=z[12] + z[19] + z[24] + z[34] + n<T>(1,4)*z[35] + z[17];
    z[12]=z[2]*z[12];
    z[17]=static_cast<T>(25)+ 37*z[6];
    z[17]=z[6]*z[17];
    z[17]= - z[29] - static_cast<T>(23)+ z[17];
    z[19]= - n<T>(25,2) + z[30];
    z[19]=z[6]*z[19];
    z[19]=static_cast<T>(7)+ z[19];
    z[19]=z[6]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[4]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[19]=n<T>(1,2)*z[4];
    z[17]=z[17]*z[19];
    z[24]= - static_cast<T>(19)+ 15*z[8];
    z[24]=z[24]*z[25];
    z[23]=z[24] - n<T>(31,4) + z[23];
    z[23]=z[1]*z[23];
    z[24]= - static_cast<T>(25)- z[9];
    z[24]=z[9]*z[24];
    z[24]=z[24] + static_cast<T>(41)+ z[8];
    z[30]= - static_cast<T>(19)- z[9];
    z[30]=z[9]*z[30];
    z[30]=static_cast<T>(11)+ n<T>(3,8)*z[30];
    z[30]=z[6]*z[30];
    z[34]= - static_cast<T>(7)+ 9*z[8];
    z[34]=n<T>(1,8)*z[34] - z[9];
    z[34]=z[3]*z[34];
    z[17]=z[23] + z[17] + z[34] + n<T>(1,4)*z[24] + z[30];
    z[17]=z[1]*z[17];
    z[23]=static_cast<T>(17)+ z[9];
    z[23]=z[9]*z[23];
    z[23]=z[23] - static_cast<T>(35)- z[8];
    z[24]=static_cast<T>(13)+ z[9];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(3,2) + z[24];
    z[24]=z[6]*z[24];
    z[23]=n<T>(1,2)*z[23] + z[24];
    z[23]=z[6]*z[23];
    z[24]=5*z[8];
    z[30]= - static_cast<T>(1)+ z[24];
    z[30]=z[30]*z[25];
    z[34]=n<T>(3,4) + z[8];
    z[23]=z[30] + z[23] + 11*z[34] + z[26];
    z[26]=static_cast<T>(23)- n<T>(9,2)*z[6];
    z[26]=z[6]*z[26];
    z[26]=n<T>(89,4) + z[26];
    z[26]=z[6]*z[26];
    z[26]= - z[25] - n<T>(79,8) + z[26];
    z[30]=n<T>(1,2)*z[6];
    z[34]=n<T>(5,2)*z[6];
    z[35]=static_cast<T>(29)- z[34];
    z[35]=z[6]*z[35];
    z[35]= - n<T>(191,4) + z[35];
    z[35]=z[35]*z[30];
    z[35]=static_cast<T>(7)+ z[35];
    z[35]=z[6]*z[35];
    z[35]= - n<T>(1,4) + z[35];
    z[35]=z[4]*z[35];
    z[26]=n<T>(1,2)*z[26] + z[35];
    z[26]=z[4]*z[26];
    z[17]=z[17] + n<T>(1,2)*z[23] + z[26];
    z[12]=n<T>(1,2)*z[17] + z[12];
    z[12]=z[2]*z[12];
    z[17]= - static_cast<T>(1)- n<T>(21,4)*z[3];
    z[17]=z[4]*z[17];
    z[17]=z[17] - static_cast<T>(1)- z[31];
    z[17]=z[4]*z[17];
    z[23]=3*z[3];
    z[26]= - z[4]*z[31];
    z[26]= - z[23] + z[26];
    z[26]=z[4]*z[26];
    z[26]=z[26] - z[16];
    z[26]=z[26]*z[32];
    z[17]=z[17] + z[26];
    z[17]=z[17]*z[32];
    z[26]= - static_cast<T>(3)- z[6];
    z[26]=n<T>(1,2)*z[26] - z[23];
    z[26]=z[4]*z[26];
    z[31]=7*z[3];
    z[35]= - z[31] + static_cast<T>(3)- z[6];
    z[26]=n<T>(1,2)*z[35] + z[26];
    z[26]=z[4]*z[26];
    z[35]=static_cast<T>(3)- n<T>(5,2)*z[8];
    z[35]=z[3]*z[35];
    z[35]=n<T>(1,2) + z[35];
    z[17]=z[17] + n<T>(1,2)*z[35] + z[26];
    z[17]=z[17]*z[32];
    z[26]=static_cast<T>(13)- n<T>(21,2)*z[8];
    z[26]=z[3]*z[26];
    z[26]=z[26] + n<T>(21,2) - z[24];
    z[35]=static_cast<T>(4)- n<T>(1,4)*z[6];
    z[35]=z[6]*z[35];
    z[36]= - n<T>(3,2) - z[6];
    z[36]=z[6]*z[36];
    z[36]= - n<T>(7,2) + z[36];
    z[36]=n<T>(1,4)*z[36] - z[3];
    z[36]=z[4]*z[36];
    z[35]=z[36] - n<T>(19,16)*z[3] + n<T>(3,8) + z[35];
    z[35]=z[4]*z[35];
    z[17]=z[17] + n<T>(1,8)*z[26] + z[35];
    z[17]=z[1]*z[17];
    z[26]=static_cast<T>(1)+ z[30];
    z[26]=z[26]*z[27];
    z[26]= - static_cast<T>(9)+ z[26];
    z[26]=z[6]*z[26];
    z[35]=static_cast<T>(1)- z[6];
    z[35]=z[6]*z[35];
    z[35]= - static_cast<T>(1)+ z[35];
    z[35]=z[35]*z[30];
    z[36]=static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[35]=z[35] - z[36];
    z[35]=z[4]*z[35];
    z[26]=z[35] - z[29] - n<T>(11,8) + z[26];
    z[26]=z[26]*z[19];
    z[35]=n<T>(1,2)*z[8];
    z[37]=static_cast<T>(9)+ z[35];
    z[38]=static_cast<T>(1)+ n<T>(1,16)*z[9];
    z[38]=z[9]*z[38];
    z[37]=z[6] + n<T>(1,2)*z[37] + z[38];
    z[37]=z[6]*z[37];
    z[38]=z[22] + 7;
    z[39]= - z[24] + z[38];
    z[40]=n<T>(1,8)*z[3];
    z[39]=z[39]*z[40];
    z[41]=11*z[8];
    z[42]=n<T>(5,4)*z[9] + static_cast<T>(5)- z[41];
    z[17]=z[17] + z[26] + z[39] + n<T>(1,2)*z[42] + z[37];
    z[17]=z[1]*z[17];
    z[26]= - z[38]*z[22];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[26]*z[30];
    z[26]=z[26] + static_cast<T>(11)- n<T>(17,16)*z[9];
    z[26]=z[6]*z[26];
    z[37]=static_cast<T>(1)+ z[8];
    z[37]=z[37]*z[40];
    z[39]=z[20] - 1;
    z[26]=z[37] + z[26] - z[39];
    z[37]= - n<T>(41,2) + z[27];
    z[37]=z[6]*z[37];
    z[37]=n<T>(25,2) + z[37];
    z[37]=z[6]*z[37];
    z[37]= - n<T>(5,4) + z[37];
    z[37]=z[6]*z[37];
    z[37]= - z[25] - n<T>(1,2) + z[37];
    z[37]=z[37]*z[33];
    z[42]=n<T>(11,4) + z[6];
    z[42]=z[6]*z[42];
    z[42]= - n<T>(165,32) + z[42];
    z[42]=z[6]*z[42];
    z[37]=z[37] + n<T>(1,16)*z[3] - n<T>(7,16) + z[42];
    z[37]=z[4]*z[37];
    z[12]=z[12] + z[17] + n<T>(1,2)*z[26] + z[37];
    z[12]=z[2]*z[12];
    z[17]=z[6]*z[7];
    z[26]=static_cast<T>(5)- 7*z[17];
    z[26]=z[26]*z[33];
    z[37]=z[7] + 1;
    z[42]=n<T>(5,4)*z[3];
    z[26]=z[26] - z[42] + 3*z[37] - z[17];
    z[26]=z[4]*z[26];
    z[17]=z[17] - 1;
    z[17]=z[17]*z[4];
    z[43]= - z[3] - z[17];
    z[43]=z[43]*z[32];
    z[44]= - z[30] + 1;
    z[44]=z[7]*z[44];
    z[17]=z[43] - n<T>(3,2)*z[17] - z[18] + n<T>(1,2) + z[44];
    z[17]=z[4]*z[17];
    z[17]= - z[18] + z[17];
    z[17]=z[1]*z[17];
    z[43]= - static_cast<T>(3)+ z[8];
    z[18]=z[43]*z[18];
    z[17]=z[17] + z[26] + z[18] - n<T>(3,2) + z[7];
    z[17]=z[17]*z[32];
    z[18]=static_cast<T>(9)+ 7*z[7];
    z[26]=static_cast<T>(3)- z[7];
    z[26]=z[6]*z[26];
    z[37]= - z[6]*z[37];
    z[37]= - n<T>(1,2) + z[37];
    z[37]=z[4]*z[37];
    z[18]=z[37] + z[25] + n<T>(1,2)*z[18] + z[26];
    z[18]=z[18]*z[19];
    z[26]= - static_cast<T>(23)+ z[41];
    z[26]=z[26]*z[40];
    z[37]=n<T>(5,2)*z[7] + static_cast<T>(1)+ n<T>(3,2)*z[8];
    z[17]=z[17] + z[18] + z[26] + n<T>(1,2)*z[37] - z[6];
    z[17]=z[17]*z[32];
    z[18]= - z[27] + static_cast<T>(15)- z[8];
    z[18]=z[18]*z[30];
    z[24]= - static_cast<T>(13)+ z[24];
    z[24]=z[24]*z[25];
    z[18]=z[24] + z[18] + n<T>(11,4)*z[7] - n<T>(25,4) + z[41];
    z[24]= - n<T>(3,4)*z[6] - static_cast<T>(1)- n<T>(1,8)*z[7];
    z[24]=z[6]*z[24];
    z[24]= - n<T>(3,4) + z[24];
    z[19]=z[24]*z[19];
    z[24]=n<T>(25,16) + z[7];
    z[26]= - static_cast<T>(1)- n<T>(5,4)*z[7];
    z[26]=n<T>(1,8)*z[26] + z[6];
    z[26]=z[6]*z[26];
    z[19]=z[19] + n<T>(3,16)*z[3] + n<T>(1,2)*z[24] + z[26];
    z[19]=z[4]*z[19];
    z[17]=z[17] + n<T>(1,4)*z[18] + z[19];
    z[17]=z[1]*z[17];
    z[18]=n<T>(1,2)*z[7];
    z[19]= - static_cast<T>(5)- z[18];
    z[24]= - n<T>(19,2) + z[27];
    z[24]=z[6]*z[24];
    z[19]=n<T>(1,4)*z[19] + z[24];
    z[19]=z[6]*z[19];
    z[24]=z[7] + n<T>(5,2);
    z[19]=z[25] + n<T>(1,2)*z[24] + z[19];
    z[26]=z[6] + n<T>(1,4);
    z[27]= - z[6]*z[26];
    z[27]= - n<T>(3,4) + z[27];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[4]*z[27];
    z[19]=n<T>(1,2)*z[19] + z[27];
    z[19]=z[4]*z[19];
    z[27]=z[22] - 1;
    z[35]= - z[35] + z[27];
    z[35]=z[35]*z[25];
    z[30]=z[30] - 11;
    z[37]=z[30]*z[6];
    z[28]=z[28] + static_cast<T>(11)+ n<T>(13,2)*z[7];
    z[19]=z[19] + z[35] + n<T>(1,8)*z[28] - z[37];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[1]*z[17];
    z[19]=n<T>(3,2)*z[7];
    z[28]=z[9] - 1;
    z[35]= - z[9]*z[34];
    z[35]=z[35] + z[19] - z[28];
    z[37]= - n<T>(13,8) + z[37];
    z[37]=z[6]*z[37];
    z[37]=n<T>(1,8) + z[37];
    z[41]= - static_cast<T>(1)+ n<T>(5,8)*z[6];
    z[41]=z[41]*z[6];
    z[41]=z[41] + n<T>(3,16);
    z[43]= - z[6]*z[41];
    z[43]= - n<T>(1,8) + z[43];
    z[43]=z[6]*z[43];
    z[43]= - n<T>(1,16) + z[43];
    z[43]=z[4]*z[43];
    z[37]=n<T>(1,4)*z[37] + z[43];
    z[37]=z[4]*z[37];
    z[12]=z[12] + z[17] + n<T>(1,16)*z[35] + z[37];
    z[12]=z[5]*z[12];
    z[17]=z[29]*z[7];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[4]*z[3]*z[19];
    z[19]=z[17] + z[19];
    z[19]=z[4]*z[19];
    z[15]=z[17]*z[15];
    z[15]=z[19] + z[15];
    z[15]=z[15]*z[32];
    z[17]=npow(z[9],2);
    z[19]= - n<T>(1,2) - z[9];
    z[19]=z[19]*z[17];
    z[35]=npow(z[7],2);
    z[19]=n<T>(1,2)*z[35] + z[19];
    z[19]=z[19]*npow(z[3],2);
    z[35]=npow(z[9],3);
    z[37]=z[35]*z[25];
    z[43]=z[7] + n<T>(3,2);
    z[37]=z[37] + z[43];
    z[37]=z[37]*z[29];
    z[44]=z[17]*z[3];
    z[45]= - n<T>(1,16)*z[44] + static_cast<T>(1)+ n<T>(7,8)*z[7];
    z[45]=z[3]*z[45];
    z[45]=n<T>(3,4) + z[45];
    z[45]=z[4]*z[45];
    z[37]=z[37] + z[45];
    z[37]=z[4]*z[37];
    z[15]=z[15] + n<T>(1,4)*z[19] + z[37];
    z[15]=z[1]*z[15];
    z[19]=z[9]*z[28];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[20];
    z[37]= - static_cast<T>(1)+ z[7];
    z[37]=z[37]*z[18];
    z[39]=z[9]*z[39];
    z[39]= - n<T>(1,2) + z[39];
    z[39]=z[9]*z[39];
    z[37]=z[37] + z[39];
    z[37]=z[3]*z[37];
    z[19]=z[19] + z[37];
    z[19]=z[19]*z[29];
    z[17]=n<T>(3,4)*z[44] + n<T>(1,4)*z[17] + z[24];
    z[17]=z[3]*z[17];
    z[17]=n<T>(3,2) + z[17];
    z[24]=n<T>(3,8)*z[3];
    z[37]= - z[9]*z[24];
    z[37]=z[37] + z[43];
    z[37]=z[37]*z[29];
    z[26]=z[37] + z[26];
    z[26]=z[4]*z[26];
    z[17]=n<T>(1,2)*z[17] + z[26];
    z[17]=z[4]*z[17];
    z[15]=z[15] + z[19] + z[17];
    z[15]=z[15]*z[32];
    z[17]=z[10]*z[35]*z[29];
    z[19]=z[10]*z[22];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[9]*z[19];
    z[19]= - static_cast<T>(7)+ z[19];
    z[19]=z[9]*z[19];
    z[17]=z[19] + z[17];
    z[17]=z[17]*z[25];
    z[19]=z[31] + 15*z[4];
    z[19]=z[19]*z[33];
    z[13]=z[13]*z[42];
    z[21]= - z[9]*z[21];
    z[13]=z[13] + z[16] + z[19] + z[21] + z[17];
    z[13]=z[2]*z[13];
    z[17]= - z[28]*z[20];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17]*z[22];
    z[19]=static_cast<T>(1)+ z[24];
    z[19]=z[4]*z[19];
    z[14]=z[14] + z[19];
    z[14]=z[4]*z[14];
    z[14]=n<T>(3,8)*z[16] + z[17] + z[14];
    z[14]=z[1]*z[14];
    z[16]=z[10] - n<T>(1,2);
    z[16]=z[16]*z[9];
    z[16]=z[16] - z[10];
    z[17]= - z[16]*z[44];
    z[16]= - z[9]*z[16];
    z[16]=n<T>(11,2) + z[16];
    z[16]=z[9]*z[16];
    z[16]=z[16] + z[17];
    z[16]=z[3]*z[16];
    z[17]=z[9]*z[38];
    z[16]=z[17] + z[16];
    z[17]=z[11]*z[29];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[25];
    z[19]= - static_cast<T>(1)+ z[34];
    z[17]=3*z[19] + z[17];
    z[17]=z[17]*z[33];
    z[17]=z[17] + z[36];
    z[17]=z[4]*z[17];
    z[13]=n<T>(1,2)*z[13] + z[14] + n<T>(1,8)*z[16] + z[17];
    z[13]=z[2]*z[13];
    z[14]=z[11] + 3;
    z[16]=z[14]*z[18];
    z[17]=n<T>(1,2)*z[11];
    z[19]= - static_cast<T>(1)- z[17];
    z[16]=3*z[19] + z[16];
    z[16]=z[16]*z[18];
    z[19]=z[10] - 1;
    z[19]=z[9]*z[27]*z[19];
    z[20]= - static_cast<T>(3)+ z[10];
    z[20]=n<T>(1,2)*z[20] + z[19];
    z[20]=z[9]*z[20];
    z[16]=z[16] + z[20];
    z[16]=z[3]*z[16];
    z[19]=n<T>(1,2)*z[10] + z[19];
    z[19]=z[9]*z[19];
    z[16]=z[19] + z[16];
    z[16]=z[16]*z[40];
    z[19]=static_cast<T>(5)+ z[11];
    z[19]=z[19]*z[18];
    z[17]=z[17] + z[9];
    z[17]=z[17]*z[23];
    z[17]=z[17] + n<T>(3,2)*z[9] + static_cast<T>(5)+ z[19];
    z[17]=z[17]*z[25];
    z[17]=z[17] - z[30];
    z[14]= - z[14]*z[25];
    z[14]=z[14] + static_cast<T>(1)+ z[18];
    z[14]=z[14]*z[40];
    z[14]=z[14] + z[41];
    z[14]=z[4]*z[14];
    z[14]=n<T>(1,4)*z[17] + z[14];
    z[14]=z[4]*z[14];

    r += z[12] + z[13] + z[14] + z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r291(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r291(const std::array<dd_real,30>&);
#endif
