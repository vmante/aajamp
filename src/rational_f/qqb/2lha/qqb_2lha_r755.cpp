#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r755(const std::array<T,30>& k) {
  T z[81];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[6];
    z[6]=k[20];
    z[7]=k[4];
    z[8]=k[3];
    z[9]=k[15];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=k[5];
    z[13]=k[11];
    z[14]=k[13];
    z[15]=k[21];
    z[16]=n<T>(1,4)*z[3];
    z[17]=n<T>(11,2) - z[3];
    z[17]=z[17]*z[16];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17];
    z[18]=n<T>(11,4) - z[3];
    z[18]=z[18]*z[16];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(9,8) + z[18];
    z[18]=z[5]*z[18];
    z[17]=z[18] - n<T>(9,4) + z[17];
    z[17]=z[5]*z[17];
    z[18]=n<T>(1,9)*z[3];
    z[19]=206*z[3];
    z[20]= - n<T>(197,4) + z[19];
    z[20]=z[20]*z[18];
    z[20]=n<T>(1,2) + z[20];
    z[20]=z[3]*z[20];
    z[20]=n<T>(1,2) + z[20];
    z[20]=z[3]*z[20];
    z[21]=n<T>(1,2)*z[3];
    z[22]=n<T>(103,9)*z[3];
    z[23]= - static_cast<T>(1)- z[22];
    z[23]=z[3]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[3]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[2]*z[23]*z[21];
    z[20]=z[20] + z[23];
    z[20]=z[2]*z[20];
    z[23]=n<T>(1,6)*z[3];
    z[24]=n<T>(469,2) - 409*z[3];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(85,3) + z[24];
    z[24]=z[24]*z[23];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[21];
    z[20]=z[24] + z[20];
    z[20]=z[2]*z[20];
    z[24]= - n<T>(487,2) + n<T>(797,3)*z[3];
    z[24]=z[3]*z[24];
    z[24]=n<T>(259,6) + z[24];
    z[23]=z[24]*z[23];
    z[23]= - static_cast<T>(3)+ z[23];
    z[23]=z[23]*z[21];
    z[20]=z[23] + z[20];
    z[20]=z[2]*z[20];
    z[23]=n<T>(439,2) - 179*z[3];
    z[23]=z[3]*z[23];
    z[23]=n<T>(425,4) + z[23];
    z[23]=z[3]*z[23];
    z[23]=static_cast<T>(3)+ n<T>(1,36)*z[23];
    z[23]=z[3]*z[23];
    z[17]=z[20] + z[17] + n<T>(9,8) + z[23];
    z[17]=z[1]*z[17];
    z[20]=z[7] + n<T>(1,2);
    z[23]=z[20]*z[3];
    z[24]=5*z[7];
    z[25]=z[23] - static_cast<T>(3)- z[24];
    z[25]=z[3]*z[25];
    z[26]=17*z[7];
    z[27]=n<T>(33,2) + z[26];
    z[25]=n<T>(1,4)*z[27] + z[25];
    z[25]=z[3]*z[25];
    z[27]=z[7] + n<T>(3,2);
    z[25]= - n<T>(17,8)*z[27] + z[25];
    z[25]=z[3]*z[25];
    z[28]= - n<T>(13,4)*z[20] + z[23];
    z[28]=z[3]*z[28];
    z[29]=9*z[7];
    z[30]=n<T>(25,8) + z[29];
    z[28]=n<T>(1,2)*z[30] + z[28];
    z[28]=z[3]*z[28];
    z[30]= - n<T>(7,4) - z[29];
    z[28]=n<T>(1,2)*z[30] + z[28];
    z[28]=z[5]*z[28];
    z[30]=z[7] + n<T>(3,4);
    z[25]=z[28] + 5*z[30] + z[25];
    z[25]=z[5]*z[25];
    z[28]=z[7] + 2;
    z[31]= - z[28]*z[19];
    z[31]=z[31] - n<T>(743,4) - 112*z[7];
    z[31]=z[31]*z[18];
    z[32]=2*z[7];
    z[31]=z[31] + n<T>(103,9) - z[32];
    z[31]=z[3]*z[31];
    z[33]=static_cast<T>(1)+ z[3];
    z[22]=z[33]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[3]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[2]*z[22];
    z[33]=z[32] + n<T>(1,2);
    z[22]=z[22] + z[31] - z[33];
    z[31]=z[2]*z[3];
    z[22]=z[22]*z[31];
    z[34]=n<T>(1,3)*z[3];
    z[35]=206*z[7];
    z[36]=n<T>(409,2) + z[35];
    z[36]=z[36]*z[34];
    z[37]= - static_cast<T>(1)- n<T>(41,36)*z[7];
    z[36]=7*z[37] + z[36];
    z[36]=z[3]*z[36];
    z[37]=n<T>(1,2)*z[7];
    z[38]=z[37] + 1;
    z[36]= - n<T>(233,9)*z[38] + z[36];
    z[36]=z[3]*z[36];
    z[36]=z[36] + n<T>(85,18) - z[7];
    z[36]=z[3]*z[36];
    z[22]=z[36] + z[22];
    z[22]=z[2]*z[22];
    z[36]=n<T>(1049,4) + 484*z[7];
    z[39]= - n<T>(797,6) - 203*z[7];
    z[39]=z[3]*z[39];
    z[36]=n<T>(1,3)*z[36] + z[39];
    z[36]=z[36]*z[34];
    z[39]=n<T>(107,18) + z[7];
    z[36]=n<T>(13,4)*z[39] + z[36];
    z[36]=z[3]*z[36];
    z[39]= - n<T>(73,2) - n<T>(85,3)*z[7];
    z[36]=n<T>(1,6)*z[39] + z[36];
    z[36]=z[3]*z[36];
    z[22]=z[22] + static_cast<T>(2)+ z[36];
    z[22]=z[2]*z[22];
    z[36]=n<T>(179,2) + 188*z[7];
    z[36]=z[3]*z[36];
    z[36]=z[36] - static_cast<T>(103)- n<T>(1129,4)*z[7];
    z[18]=z[36]*z[18];
    z[36]= - n<T>(139,4) + n<T>(35,9)*z[7];
    z[18]=n<T>(1,2)*z[36] + z[18];
    z[18]=z[3]*z[18];
    z[36]= - n<T>(577,9) - 23*z[7];
    z[18]=n<T>(1,8)*z[36] + z[18];
    z[18]=z[3]*z[18];
    z[36]=npow(z[5],2)*z[37];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[8]*z[36];
    z[17]=z[17] + n<T>(9,4)*z[36] + z[22] + z[25] + z[18] - n<T>(89,8) - z[32];
    z[17]=z[1]*z[17];
    z[18]=z[7] + n<T>(5,4);
    z[22]= - z[23] - z[18];
    z[22]=z[3]*z[22];
    z[23]=n<T>(1,2)*z[1];
    z[25]=static_cast<T>(1)+ z[21];
    z[25]=z[3]*z[25];
    z[25]=n<T>(1,2) + z[25];
    z[25]=z[25]*z[23];
    z[36]=z[7] + n<T>(5,2);
    z[39]=n<T>(1,2)*z[8];
    z[22]=z[25] - z[39] - n<T>(1,2)*z[36] + z[22];
    z[22]=z[22]*z[23];
    z[25]=n<T>(1,2)*z[10];
    z[36]=z[36]*z[7];
    z[36]=z[36] + n<T>(3,2);
    z[40]= - z[7]*z[36];
    z[40]=n<T>(1,2) + z[40];
    z[40]=z[7]*z[40];
    z[40]=n<T>(1,2) + z[40];
    z[40]=z[40]*z[25];
    z[41]=z[7]*z[30];
    z[41]= - n<T>(3,2) + z[41];
    z[41]=z[7]*z[41];
    z[40]=z[40] - n<T>(5,4) + z[41];
    z[40]=z[10]*z[40];
    z[41]=n<T>(3,2)*z[7];
    z[42]=static_cast<T>(1)+ z[41];
    z[40]=n<T>(3,2)*z[42] + z[40];
    z[42]=n<T>(1,4)*z[7];
    z[43]=z[42] + 1;
    z[43]=z[43]*z[7];
    z[44]=z[43] + n<T>(3,2);
    z[44]=z[44]*z[7];
    z[44]=z[44] + 1;
    z[44]=z[44]*z[7];
    z[44]=z[44] + n<T>(1,4);
    z[44]=z[44]*z[10];
    z[45]=z[7] + 3;
    z[46]=z[45]*z[7];
    z[47]=z[46] + 3;
    z[47]=z[47]*z[7];
    z[47]=z[47] + 1;
    z[44]=z[44] - z[47];
    z[48]=z[44]*z[25];
    z[49]=n<T>(3,4)*z[7];
    z[50]=static_cast<T>(1)+ z[49];
    z[50]=z[7]*z[50];
    z[48]=z[48] + n<T>(3,8) + z[50];
    z[48]=z[3]*z[48];
    z[40]=n<T>(1,2)*z[40] + z[48];
    z[40]=z[3]*z[40];
    z[48]=npow(z[7],2);
    z[50]=z[25]*z[48];
    z[51]=z[7] + 1;
    z[52]=z[51]*z[50];
    z[53]= - z[7]*z[20];
    z[52]=z[53] + z[52];
    z[52]=z[10]*z[52];
    z[52]=z[39] + n<T>(3,2)*z[51] + z[52];
    z[52]=z[8]*z[52];
    z[53]= - static_cast<T>(3)+ z[48];
    z[53]=z[53]*z[37];
    z[53]= - static_cast<T>(1)+ z[53];
    z[54]=z[10]*z[7];
    z[53]=z[53]*z[54];
    z[55]=z[7] - n<T>(1,2);
    z[56]=z[55]*z[7];
    z[57]=static_cast<T>(1)- z[56];
    z[57]=z[7]*z[57];
    z[53]=z[53] - n<T>(1,2) + z[57];
    z[53]=z[53]*z[25];
    z[38]=z[38]*z[7];
    z[53]=z[53] + n<T>(5,4) + z[38];
    z[22]=z[22] + n<T>(1,4)*z[52] + n<T>(1,2)*z[53] + z[40];
    z[22]=z[6]*z[22];
    z[40]= - static_cast<T>(41)- z[24];
    z[40]=z[40]*z[37];
    z[40]= - static_cast<T>(19)+ z[40];
    z[52]= - n<T>(15,4) - z[7];
    z[52]=z[52]*z[24];
    z[52]=static_cast<T>(27)+ z[52];
    z[52]=z[7]*z[52];
    z[53]=n<T>(39,2) + z[7];
    z[53]=z[7]*z[53];
    z[53]=n<T>(111,4) + z[53];
    z[53]=z[7]*z[53];
    z[53]=static_cast<T>(1)+ z[53];
    z[53]=z[7]*z[53];
    z[53]= - n<T>(33,4) + z[53];
    z[53]=z[10]*z[53];
    z[52]=z[53] + n<T>(163,4) + z[52];
    z[52]=z[10]*z[52];
    z[40]=3*z[40] + z[52];
    z[52]= - static_cast<T>(9)+ z[7];
    z[52]=z[7]*z[52];
    z[52]= - static_cast<T>(33)+ z[52];
    z[52]=z[7]*z[52];
    z[52]= - static_cast<T>(35)+ z[52];
    z[52]=z[52]*z[37];
    z[52]= - static_cast<T>(6)+ z[52];
    z[52]=z[10]*z[52];
    z[53]=static_cast<T>(129)+ z[26];
    z[53]=z[7]*z[53];
    z[53]=static_cast<T>(207)+ z[53];
    z[53]=z[7]*z[53];
    z[53]=static_cast<T>(95)+ z[53];
    z[52]=n<T>(1,4)*z[53] + z[52];
    z[52]=z[10]*z[52];
    z[53]=z[37] + 2;
    z[57]= - z[7]*z[53];
    z[57]= - static_cast<T>(3)+ z[57];
    z[57]=z[7]*z[57];
    z[57]= - static_cast<T>(2)+ z[57];
    z[57]=z[7]*z[57];
    z[57]= - n<T>(1,2) + z[57];
    z[57]=z[10]*z[57];
    z[57]=2*z[47] + z[57];
    z[57]=z[10]*z[57];
    z[58]=3*z[7];
    z[59]= - static_cast<T>(4)- z[58];
    z[59]=z[7]*z[59];
    z[57]=z[57] - n<T>(3,2) + z[59];
    z[57]=z[3]*z[57];
    z[59]= - static_cast<T>(145)- 63*z[7];
    z[59]=z[7]*z[59];
    z[59]= - static_cast<T>(75)+ z[59];
    z[52]=z[57] + n<T>(1,4)*z[59] + z[52];
    z[52]=z[3]*z[52];
    z[40]=n<T>(1,2)*z[40] + z[52];
    z[40]=z[3]*z[40];
    z[52]=static_cast<T>(51)+ 67*z[7];
    z[57]=static_cast<T>(1)+ z[32];
    z[57]=z[3]*z[57];
    z[52]=n<T>(1,4)*z[52] + z[57];
    z[52]=z[3]*z[52];
    z[57]=13*z[7];
    z[52]=z[52] + n<T>(189,8) + z[57];
    z[52]=z[3]*z[52];
    z[59]= - n<T>(23,2) - z[3];
    z[59]=z[3]*z[59];
    z[59]= - n<T>(29,2) + z[59];
    z[59]=z[59]*z[21];
    z[59]= - static_cast<T>(2)+ z[59];
    z[59]=z[1]*z[59];
    z[60]=4*z[7];
    z[52]=z[59] + 4*z[8] + z[52] + n<T>(69,4) + z[60];
    z[52]=z[1]*z[52];
    z[59]= - n<T>(11,2) - z[7];
    z[59]=z[7]*z[59];
    z[59]=n<T>(13,2) + z[59];
    z[59]=z[7]*z[59];
    z[59]=static_cast<T>(11)+ z[59];
    z[59]=z[10]*z[59]*z[37];
    z[61]=n<T>(21,8) + z[7];
    z[61]=z[7]*z[61];
    z[61]= - n<T>(23,8) + z[61];
    z[61]=z[7]*z[61];
    z[59]=z[59] + n<T>(11,4) + z[61];
    z[59]=z[10]*z[59];
    z[61]=z[7] + n<T>(11,4);
    z[62]= - z[61]*z[50];
    z[63]=n<T>(11,8) + z[7];
    z[63]=z[7]*z[63];
    z[62]=z[63] + z[62];
    z[62]=z[10]*z[62];
    z[63]=n<T>(9,2)*z[7];
    z[62]= - 2*z[8] + z[62] - static_cast<T>(10)- z[63];
    z[62]=z[8]*z[62];
    z[64]= - n<T>(25,4) - z[7];
    z[64]=z[7]*z[64];
    z[64]= - n<T>(33,4) + z[64];
    z[22]=11*z[22] + z[52] + z[62] + z[40] + n<T>(5,2)*z[64] + z[59];
    z[22]=z[6]*z[22];
    z[40]=n<T>(119,2) + z[29];
    z[40]=z[40]*z[37];
    z[40]=static_cast<T>(41)+ z[40];
    z[40]=z[7]*z[40];
    z[40]= - n<T>(9,2) + z[40];
    z[52]= - static_cast<T>(5)- n<T>(9,16)*z[7];
    z[52]=z[7]*z[52];
    z[52]= - n<T>(169,16) + z[52];
    z[52]=z[7]*z[52];
    z[52]= - static_cast<T>(5)+ z[52];
    z[52]=z[7]*z[52];
    z[52]=n<T>(9,8) + z[52];
    z[52]=z[10]*z[52];
    z[40]=n<T>(1,4)*z[40] + z[52];
    z[40]=z[10]*z[40];
    z[52]= - z[45]*z[37];
    z[52]= - static_cast<T>(1)+ z[52];
    z[43]=n<T>(5,4) + z[43];
    z[43]=z[7]*z[43];
    z[43]=n<T>(1,2) + z[43];
    z[43]=z[10]*z[43];
    z[43]=n<T>(1,2)*z[52] + z[43];
    z[52]=z[8]*z[10];
    z[43]=z[43]*z[52];
    z[59]= - static_cast<T>(11)- z[7];
    z[59]=z[7]*z[59];
    z[40]=n<T>(9,4)*z[43] + n<T>(3,8)*z[59] + z[40];
    z[40]=z[8]*z[40];
    z[43]= - n<T>(33,2) + 25*z[7];
    z[43]=z[43]*z[42];
    z[59]=z[42]*z[10];
    z[62]=static_cast<T>(33)+ n<T>(13,2)*z[7];
    z[62]=z[7]*z[62];
    z[62]=static_cast<T>(11)+ z[62];
    z[62]=z[7]*z[62];
    z[62]= - n<T>(107,4) + z[62];
    z[62]=z[62]*z[59];
    z[64]= - n<T>(233,16) - z[60];
    z[64]=z[7]*z[64];
    z[64]=n<T>(173,16) + z[64];
    z[64]=z[7]*z[64];
    z[62]=z[64] + z[62];
    z[62]=z[10]*z[62];
    z[40]=z[40] + z[43] + z[62];
    z[40]=z[8]*z[40];
    z[43]=n<T>(5,2) - z[7];
    z[43]=z[43]*z[48];
    z[62]=z[7] - 1;
    z[64]=z[7]*z[62];
    z[64]= - static_cast<T>(3)+ z[64];
    z[64]=z[64]*z[50];
    z[43]=z[43] + z[64];
    z[43]=z[10]*z[43];
    z[43]= - z[48] + z[43];
    z[64]= - z[51]*z[37];
    z[65]=z[38] + n<T>(1,2);
    z[66]=z[65]*z[54];
    z[64]=z[64] + z[66];
    z[64]=z[8]*z[64]*z[25];
    z[46]= - static_cast<T>(1)- z[46];
    z[46]=z[7]*z[46];
    z[46]=static_cast<T>(1)+ z[46];
    z[46]=z[46]*z[59];
    z[38]= - n<T>(1,4) + z[38];
    z[38]=z[7]*z[38];
    z[38]=z[38] + z[46];
    z[38]=z[10]*z[38];
    z[46]=n<T>(1,2)*z[48];
    z[38]=z[64] - z[46] + z[38];
    z[38]=z[8]*z[38];
    z[38]=n<T>(1,2)*z[43] + z[38];
    z[38]=z[8]*z[38];
    z[43]=npow(z[10],2)*npow(z[7],4);
    z[64]=z[43]*z[16];
    z[66]=n<T>(1,4)*z[10];
    z[45]=z[45]*z[66];
    z[45]= - static_cast<T>(1)+ z[45];
    z[67]=npow(z[7],3);
    z[68]=z[67]*z[10];
    z[45]=z[45]*z[68];
    z[38]=z[38] + z[45] - z[64];
    z[38]=z[9]*z[38];
    z[45]= - n<T>(69,2) + z[24];
    z[45]=z[45]*z[25];
    z[45]=static_cast<T>(11)+ z[45];
    z[45]=z[45]*z[68];
    z[45]=z[45] - z[64];
    z[21]=z[45]*z[21];
    z[45]= - n<T>(253,2) + 35*z[7];
    z[69]=n<T>(13,4)*z[7];
    z[70]=static_cast<T>(1)- z[69];
    z[70]=z[7]*z[70];
    z[70]=n<T>(159,8) + z[70];
    z[70]=z[10]*z[70];
    z[45]=n<T>(1,4)*z[45] + z[70];
    z[70]=z[48]*z[10];
    z[45]=z[45]*z[70];
    z[21]=z[21] + n<T>(35,4)*z[48] + z[45];
    z[21]=n<T>(9,2)*z[38] + n<T>(1,2)*z[21] + z[40];
    z[21]=z[9]*z[21];
    z[38]=31*z[7];
    z[40]=n<T>(681,4) + z[38];
    z[40]=z[7]*z[40];
    z[40]= - n<T>(825,4) + z[40];
    z[40]=z[40]*z[37];
    z[45]=7*z[7];
    z[49]= - static_cast<T>(5)- z[49];
    z[49]=z[49]*z[45];
    z[49]= - n<T>(59,2) + z[49];
    z[49]=z[7]*z[49];
    z[49]=n<T>(225,4) + z[49];
    z[49]=z[49]*z[54];
    z[40]=z[40] + z[49];
    z[40]=z[40]*z[25];
    z[49]=3*z[48];
    z[71]=n<T>(121,2) - z[29];
    z[71]=z[71]*z[49];
    z[72]=static_cast<T>(49)+ z[58];
    z[72]=z[7]*z[72];
    z[72]= - n<T>(291,2) + z[72];
    z[70]=z[72]*z[70];
    z[70]=z[71] + z[70];
    z[70]=z[10]*z[70];
    z[70]= - 45*z[48] + z[70];
    z[71]= - n<T>(1,2) + z[58];
    z[71]=z[71]*z[68];
    z[71]= - z[67] + z[71];
    z[71]=z[10]*z[71];
    z[43]=z[3]*z[43];
    z[43]=z[71] - n<T>(3,2)*z[43];
    z[43]=z[3]*z[43];
    z[43]=n<T>(1,2)*z[70] + z[43];
    z[16]=z[43]*z[16];
    z[43]=z[58] + n<T>(67,4);
    z[70]=z[43]*z[58];
    z[70]=n<T>(379,4) + z[70];
    z[70]=z[7]*z[70];
    z[71]=z[58] + 23;
    z[72]= - z[71]*z[41];
    z[72]= - static_cast<T>(91)+ z[72];
    z[72]=z[7]*z[72];
    z[72]= - n<T>(211,2) + z[72];
    z[72]=z[7]*z[72];
    z[72]= - n<T>(89,2) + z[72];
    z[72]=z[10]*z[72];
    z[70]=z[72] + static_cast<T>(67)+ z[70];
    z[70]=z[10]*z[70];
    z[27]=z[27]*z[7];
    z[72]= - static_cast<T>(5)- z[27];
    z[70]=n<T>(9,2)*z[72] + z[70];
    z[72]=z[7] + n<T>(7,2);
    z[73]=z[72]*z[42];
    z[73]=z[73] + 1;
    z[73]=z[73]*z[7];
    z[73]=z[73] + n<T>(3,8);
    z[73]=z[73]*z[10];
    z[36]=z[73] - n<T>(1,4)*z[36];
    z[73]=z[36]*z[52];
    z[70]=n<T>(1,2)*z[70] + 9*z[73];
    z[70]=z[70]*z[39];
    z[73]=19*z[7];
    z[74]= - n<T>(193,2) - z[73];
    z[74]=z[74]*z[58];
    z[74]= - n<T>(1001,2) + z[74];
    z[74]=z[7]*z[74];
    z[74]=n<T>(241,2) + z[74];
    z[75]=static_cast<T>(19)+ z[58];
    z[75]=z[7]*z[75];
    z[75]=static_cast<T>(45)+ z[75];
    z[75]=z[7]*z[75];
    z[75]=n<T>(103,4) + z[75];
    z[75]=z[7]*z[75];
    z[75]= - n<T>(151,16) + z[75];
    z[75]=z[10]*z[75];
    z[74]=n<T>(1,8)*z[74] + z[75];
    z[74]=z[10]*z[74];
    z[75]=static_cast<T>(33)+ n<T>(23,4)*z[7];
    z[75]=z[7]*z[75];
    z[70]=z[70] + z[74] - n<T>(45,8) + z[75];
    z[70]=z[8]*z[70];
    z[74]= - static_cast<T>(1)- z[8];
    z[74]=z[1]*z[74];
    z[74]=n<T>(9,4)*z[74] + n<T>(357,16) - z[57];
    z[74]=z[7]*z[74];
    z[16]=3*z[21] + z[70] + z[16] + z[40] + z[74];
    z[16]=z[9]*z[16];
    z[21]=z[7] - n<T>(3,2);
    z[40]=z[10] - 1;
    z[70]=z[2]*z[40];
    z[70]=z[70] + z[25] + z[21];
    z[70]=z[10]*z[70];
    z[70]=static_cast<T>(1)+ z[70];
    z[70]=z[2]*z[70];
    z[74]=z[25] - 1;
    z[74]=z[74]*z[10];
    z[75]= - z[51]*z[74];
    z[54]= - z[51]*z[54];
    z[54]=z[7] + z[54];
    z[54]=z[13]*z[54]*z[25];
    z[54]=z[54] + z[70] + z[75] - z[20];
    z[54]=z[12]*z[54];
    z[70]=static_cast<T>(3)- z[10];
    z[70]=z[70]*z[25];
    z[75]=n<T>(5,2)*z[7];
    z[70]=z[70] - static_cast<T>(1)+ z[75];
    z[70]=z[3]*z[70];
    z[40]=z[10]*z[40];
    z[40]=z[7] + z[40];
    z[40]=z[2]*z[40]*npow(z[3],2);
    z[40]=z[70] + z[40];
    z[70]=n<T>(1,2)*z[2];
    z[40]=z[40]*z[70];
    z[40]=z[40] + z[25] + z[55];
    z[40]=z[2]*z[40];
    z[76]=z[8]*z[42];
    z[40]=z[40] + z[76];
    z[40]=z[11]*z[40];
    z[76]=n<T>(1,9)*z[7];
    z[77]=103*z[7];
    z[78]= - n<T>(557,2) - z[77];
    z[78]=z[78]*z[76];
    z[78]=n<T>(325,4) + z[78];
    z[79]=static_cast<T>(97)- z[45];
    z[79]=z[7]*z[79];
    z[79]= - n<T>(431,2) + z[79];
    z[80]= - n<T>(7,4) + z[7];
    z[80]=z[7]*z[80];
    z[80]= - n<T>(11,4) + z[80];
    z[80]=z[7]*z[80];
    z[80]=n<T>(195,8) + z[80];
    z[80]=z[10]*z[80];
    z[79]=n<T>(1,4)*z[79] + z[80];
    z[79]=z[10]*z[79];
    z[40]=z[40] + z[54] + n<T>(1,2)*z[78] + z[79];
    z[44]=z[44]*z[10];
    z[54]=z[41] + 2;
    z[54]=z[54]*z[7];
    z[44]=z[44] + z[54] + n<T>(3,4);
    z[44]=z[44]*z[3];
    z[54]=static_cast<T>(5)+ z[58];
    z[54]=z[54]*z[29];
    z[54]=n<T>(41,2) + z[54];
    z[78]=n<T>(7,2)*z[7];
    z[79]=static_cast<T>(19)+ z[78];
    z[79]=z[79]*z[42];
    z[79]=static_cast<T>(9)+ z[79];
    z[79]=z[7]*z[79];
    z[79]=n<T>(29,4) + z[79];
    z[79]=z[7]*z[79];
    z[79]=n<T>(17,8) + z[79];
    z[79]=z[10]*z[79];
    z[60]= - n<T>(63,4) - z[60];
    z[60]=z[7]*z[60];
    z[60]= - n<T>(39,2) + z[60];
    z[60]=z[7]*z[60];
    z[60]=z[79] - n<T>(31,4) + z[60];
    z[60]=z[10]*z[60];
    z[54]= - z[44] + n<T>(1,4)*z[54] + z[60];
    z[54]=z[3]*z[54];
    z[60]= - z[61]*z[57];
    z[60]= - n<T>(41,2) + z[60];
    z[26]=n<T>(217,2) + z[26];
    z[26]=z[26]*z[42];
    z[26]=static_cast<T>(43)+ z[26];
    z[26]=z[7]*z[26];
    z[61]= - n<T>(71,8) - z[7];
    z[61]=z[7]*z[61];
    z[61]= - n<T>(171,8) + z[61];
    z[61]=z[7]*z[61];
    z[61]= - n<T>(161,8) + z[61];
    z[61]=z[7]*z[61];
    z[61]= - n<T>(53,8) + z[61];
    z[61]=z[10]*z[61];
    z[26]=z[61] + n<T>(161,8) + z[26];
    z[26]=z[10]*z[26];
    z[26]=z[54] + n<T>(1,2)*z[60] + z[26];
    z[26]=z[3]*z[26];
    z[54]=static_cast<T>(109)+ z[73];
    z[54]=z[54]*z[37];
    z[54]=static_cast<T>(31)+ z[54];
    z[60]=n<T>(1,8)*z[7];
    z[29]=static_cast<T>(77)+ z[29];
    z[29]=z[7]*z[29];
    z[29]=n<T>(387,2) + z[29];
    z[29]=z[29]*z[60];
    z[29]=static_cast<T>(24)+ z[29];
    z[29]=z[7]*z[29];
    z[29]=n<T>(133,16) + z[29];
    z[29]=z[10]*z[29];
    z[61]= - n<T>(359,2) - z[73];
    z[61]=z[7]*z[61];
    z[61]= - static_cast<T>(333)+ z[61];
    z[61]=z[7]*z[61];
    z[61]= - n<T>(345,2) + z[61];
    z[29]=n<T>(1,8)*z[61] + z[29];
    z[29]=z[10]*z[29];
    z[26]=z[26] + n<T>(1,4)*z[54] + z[29];
    z[26]=z[3]*z[26];
    z[29]= - static_cast<T>(27)- n<T>(17,2)*z[7];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(57,2) + z[29];
    z[29]=z[29]*z[37];
    z[54]=static_cast<T>(5)+ n<T>(19,16)*z[7];
    z[54]=z[7]*z[54];
    z[54]=n<T>(63,8) + z[54];
    z[54]=z[7]*z[54];
    z[54]=n<T>(11,2) + z[54];
    z[54]=z[7]*z[54];
    z[54]=n<T>(23,16) + z[54];
    z[54]=z[10]*z[54];
    z[29]=z[54] - static_cast<T>(5)+ z[29];
    z[29]=z[10]*z[29];
    z[54]=static_cast<T>(31)+ n<T>(45,2)*z[7];
    z[54]=z[7]*z[54];
    z[54]=n<T>(49,4) + z[54];
    z[29]= - z[44] + n<T>(1,4)*z[54] + z[29];
    z[29]=z[3]*z[29];
    z[44]= - n<T>(127,4) - z[45];
    z[44]=z[7]*z[44];
    z[44]= - n<T>(213,4) + z[44];
    z[44]=z[7]*z[44];
    z[44]= - n<T>(157,4) + z[44];
    z[44]=z[7]*z[44];
    z[44]= - n<T>(43,4) + z[44];
    z[44]=z[44]*z[66];
    z[54]=n<T>(325,16) + 6*z[7];
    z[54]=z[7]*z[54];
    z[54]=n<T>(181,8) + z[54];
    z[54]=z[7]*z[54];
    z[44]=z[44] + n<T>(133,16) + z[54];
    z[44]=z[10]*z[44];
    z[38]= - n<T>(153,4) - z[38];
    z[38]=z[7]*z[38];
    z[38]= - static_cast<T>(15)+ z[38];
    z[29]=z[29] + n<T>(1,4)*z[38] + z[44];
    z[29]=z[3]*z[29];
    z[38]=static_cast<T>(5)+ n<T>(19,4)*z[7];
    z[38]=z[38]*z[37];
    z[38]=static_cast<T>(1)+ z[38];
    z[44]= - n<T>(151,4) - 11*z[7];
    z[44]=z[7]*z[44];
    z[44]= - n<T>(85,2) + z[44];
    z[44]=z[7]*z[44];
    z[54]=static_cast<T>(15)+ z[69];
    z[54]=z[7]*z[54];
    z[54]=n<T>(51,2) + z[54];
    z[54]=z[7]*z[54];
    z[54]=static_cast<T>(19)+ z[54];
    z[54]=z[7]*z[54];
    z[54]=n<T>(21,4) + z[54];
    z[54]=z[10]*z[54];
    z[44]=z[54] - n<T>(63,4) + z[44];
    z[44]=z[44]*z[25];
    z[29]=z[29] + 3*z[38] + z[44];
    z[29]=z[5]*z[29];
    z[38]=21*z[7];
    z[44]= - n<T>(109,2) - z[38];
    z[44]=z[44]*z[58];
    z[44]= - static_cast<T>(77)+ z[44];
    z[38]= - n<T>(241,2) - z[38];
    z[38]=z[7]*z[38];
    z[38]= - n<T>(495,2) + z[38];
    z[38]=z[7]*z[38];
    z[38]= - n<T>(435,2) + z[38];
    z[38]=z[7]*z[38];
    z[38]= - n<T>(139,2) + z[38];
    z[38]=z[38]*z[66];
    z[54]=static_cast<T>(69)+ n<T>(31,2)*z[7];
    z[54]=z[7]*z[54];
    z[54]=n<T>(197,2) + z[54];
    z[54]=z[7]*z[54];
    z[38]=z[38] + static_cast<T>(45)+ z[54];
    z[38]=z[10]*z[38];
    z[38]=n<T>(1,4)*z[44] + z[38];
    z[26]=z[29] + n<T>(1,2)*z[38] + z[26];
    z[26]=z[5]*z[26];
    z[29]=n<T>(1,3)*z[7];
    z[38]=n<T>(6173,2) + 215*z[7];
    z[38]=z[7]*z[38];
    z[38]=static_cast<T>(6137)+ z[38];
    z[38]=z[38]*z[29];
    z[38]=static_cast<T>(1075)+ z[38];
    z[44]= - n<T>(757,2) - 106*z[7];
    z[44]=z[7]*z[44];
    z[44]= - static_cast<T>(430)+ z[44];
    z[44]=z[44]*z[29];
    z[44]= - n<T>(423,8) + z[44];
    z[44]=z[10]*z[44];
    z[38]=n<T>(1,6)*z[38] + z[44];
    z[38]=z[10]*z[38];
    z[44]=n<T>(206,3)*z[7];
    z[54]= - static_cast<T>(2)- z[29];
    z[54]=z[54]*z[44];
    z[54]= - static_cast<T>(203)+ z[54];
    z[54]=z[7]*z[54];
    z[61]=z[29] + 1;
    z[35]=z[61]*z[35];
    z[61]=static_cast<T>(203)+ z[35];
    z[61]=z[7]*z[61];
    z[61]=n<T>(797,12) + z[61];
    z[69]=n<T>(1,3)*z[10];
    z[61]=z[61]*z[69];
    z[54]=z[61] - n<T>(797,9) + z[54];
    z[54]=z[10]*z[54];
    z[61]=static_cast<T>(203)+ z[77];
    z[61]=z[61]*z[32];
    z[61]=n<T>(797,4) + z[61];
    z[54]=n<T>(1,3)*z[61] + z[54];
    z[54]=z[3]*z[54];
    z[61]= - static_cast<T>(2339)- 811*z[7];
    z[61]=z[7]*z[61];
    z[61]= - n<T>(5905,4) + z[61];
    z[38]=z[54] + n<T>(1,18)*z[61] + z[38];
    z[38]=z[3]*z[38];
    z[54]=233*z[7];
    z[61]=n<T>(4597,4) + z[54];
    z[61]=z[7]*z[61];
    z[61]=n<T>(2857,2) + z[61];
    z[61]=z[61]*z[29];
    z[61]=n<T>(683,4) + z[61];
    z[61]=z[10]*z[61];
    z[79]= - static_cast<T>(4873)- 1631*z[7];
    z[79]=z[7]*z[79];
    z[79]= - n<T>(5683,2) + z[79];
    z[61]=n<T>(1,6)*z[79] + z[61];
    z[61]=z[10]*z[61];
    z[79]=n<T>(53,2) - 139*z[7];
    z[79]=z[7]*z[79];
    z[79]= - n<T>(29,2) + z[79];
    z[61]=n<T>(1,3)*z[79] + z[61];
    z[38]=n<T>(1,6)*z[61] + z[38];
    z[38]=z[3]*z[38];
    z[61]= - static_cast<T>(323)- 251*z[7];
    z[61]=z[61]*z[76];
    z[61]= - n<T>(41,4) + z[61];
    z[79]=n<T>(305,9) + z[41];
    z[79]=z[7]*z[79];
    z[80]= - n<T>(121,9) - z[7];
    z[80]=z[7]*z[80];
    z[80]= - n<T>(725,36) + z[80];
    z[80]=z[7]*z[80];
    z[80]= - n<T>(41,72) + z[80];
    z[80]=z[10]*z[80];
    z[79]=z[80] + n<T>(311,36) + z[79];
    z[79]=z[10]*z[79];
    z[61]=n<T>(1,2)*z[61] + z[79];
    z[38]=n<T>(1,2)*z[61] + z[38];
    z[38]=z[3]*z[38];
    z[61]= - n<T>(6899,2) - 439*z[7];
    z[61]=z[61]*z[29];
    z[61]= - n<T>(2219,2) + z[61];
    z[79]=static_cast<T>(274)+ n<T>(215,2)*z[7];
    z[79]=z[79]*z[29];
    z[79]=n<T>(439,8) + z[79];
    z[79]=z[10]*z[79];
    z[61]=n<T>(1,6)*z[61] + z[79];
    z[61]=z[10]*z[61];
    z[28]= - z[28]*z[77];
    z[28]= - n<T>(409,4) + z[28];
    z[28]=z[28]*z[69];
    z[28]=z[28] + n<T>(409,3) + z[35];
    z[28]=z[10]*z[28];
    z[35]=n<T>(103,3)*z[7];
    z[79]= - static_cast<T>(4)- z[7];
    z[79]=z[79]*z[35];
    z[28]=z[28] - n<T>(409,4) + z[79];
    z[28]=z[3]*z[28];
    z[79]=static_cast<T>(1139)- 421*z[7];
    z[76]=z[79]*z[76];
    z[76]=n<T>(495,2) + z[76];
    z[28]=z[28] + n<T>(1,4)*z[76] + z[61];
    z[28]=z[3]*z[28];
    z[61]= - n<T>(959,4) - 65*z[7];
    z[61]=z[61]*z[29];
    z[61]= - n<T>(415,8) + z[61];
    z[61]=z[10]*z[61];
    z[76]=static_cast<T>(887)+ 475*z[7];
    z[61]=n<T>(1,6)*z[76] + z[61];
    z[61]=z[10]*z[61];
    z[76]=n<T>(323,4) - 56*z[7];
    z[76]=z[7]*z[76];
    z[76]=n<T>(655,8) + z[76];
    z[61]=n<T>(1,3)*z[76] + z[61];
    z[28]=n<T>(1,3)*z[61] + z[28];
    z[28]=z[3]*z[28];
    z[61]=z[51]*z[7];
    z[76]= - n<T>(3,2) + z[61];
    z[76]=z[10]*z[76];
    z[76]=z[76] - z[72];
    z[76]=z[76]*z[66];
    z[79]=n<T>(215,18) - z[7];
    z[79]=z[7]*z[79];
    z[28]=z[28] + z[76] + n<T>(65,9) + z[79];
    z[28]=z[3]*z[28];
    z[54]=n<T>(2267,2) + z[54];
    z[76]= - n<T>(227,4) - n<T>(112,3)*z[7];
    z[76]=z[10]*z[76];
    z[54]=n<T>(1,6)*z[54] + z[76];
    z[54]=z[10]*z[54];
    z[76]=z[51]*z[69];
    z[76]=z[76] - n<T>(4,3) - z[7];
    z[76]=z[10]*z[76];
    z[76]=z[76] + static_cast<T>(1)+ n<T>(2,3)*z[7];
    z[19]=z[76]*z[19];
    z[57]=static_cast<T>(1)+ z[57];
    z[19]=z[19] + n<T>(31,6)*z[57] + z[54];
    z[19]=z[19]*z[34];
    z[30]=z[10]*z[30];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[25];
    z[19]=z[19] + z[30] - n<T>(15,8) + n<T>(103,9)*z[7];
    z[19]=z[3]*z[19];
    z[30]=static_cast<T>(2)- z[25];
    z[30]=z[30]*z[69];
    z[30]= - n<T>(1,2) + z[30];
    z[30]=z[3]*z[30];
    z[54]= - n<T>(403,18) + z[74];
    z[30]=n<T>(1,2)*z[54] + n<T>(103,3)*z[30];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(103,18) + z[30];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(1,2) + z[30];
    z[30]=z[2]*z[30];
    z[19]=z[30] + z[19] - n<T>(215,36) + z[7];
    z[19]=z[19]*z[31];
    z[19]=z[19] - n<T>(103,36) + z[28];
    z[19]=z[2]*z[19];
    z[28]= - static_cast<T>(45)+ z[7];
    z[30]=static_cast<T>(3)- z[37];
    z[30]=z[7]*z[30];
    z[30]=n<T>(23,2) + z[30];
    z[30]=z[10]*z[30];
    z[28]=n<T>(1,2)*z[28] + z[30];
    z[28]=z[28]*z[25];
    z[30]=n<T>(359,4) + z[77];
    z[28]=n<T>(1,9)*z[30] + z[28];
    z[19]=z[19] + n<T>(1,2)*z[28] + z[38];
    z[19]=z[2]*z[19];
    z[28]= - z[72]*z[37];
    z[28]= - static_cast<T>(1)+ z[28];
    z[30]= - z[71]*z[60];
    z[30]= - static_cast<T>(7)+ z[30];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(55,8) + z[30];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(19,8) + z[30];
    z[30]=z[10]*z[30];
    z[31]=z[7]*z[43];
    z[31]=n<T>(117,4) + z[31];
    z[31]=z[7]*z[31];
    z[31]=n<T>(31,2) + z[31];
    z[30]=n<T>(1,4)*z[31] + z[30];
    z[30]=z[10]*z[30];
    z[28]=n<T>(3,4)*z[28] + z[30];
    z[30]= - static_cast<T>(23)- z[63];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(65,2) + z[30];
    z[30]=z[30]*z[37];
    z[30]= - static_cast<T>(7)+ z[30];
    z[31]=static_cast<T>(7)+ n<T>(9,8)*z[7];
    z[31]=z[7]*z[31];
    z[31]=n<T>(57,4) + z[31];
    z[31]=z[31]*z[37];
    z[31]=static_cast<T>(6)+ z[31];
    z[31]=z[7]*z[31];
    z[31]=n<T>(29,16) + z[31];
    z[31]=z[10]*z[31];
    z[30]=n<T>(1,2)*z[30] + z[31];
    z[30]=z[10]*z[30];
    z[27]=n<T>(1,2) + z[27];
    z[27]=n<T>(9,8)*z[27] + z[30];
    z[27]=z[5]*z[27];
    z[30]=z[10]*z[36];
    z[31]= - z[47]*z[25];
    z[31]=z[31] + z[65];
    z[31]=z[5]*z[31]*z[66];
    z[30]=z[30] + z[31];
    z[30]=z[8]*z[30];
    z[27]=n<T>(9,2)*z[30] + 3*z[28] + z[27];
    z[27]=z[5]*z[27];
    z[27]=n<T>(9,8) + z[27];
    z[27]=z[8]*z[27];
    z[18]= - z[18]*z[45];
    z[28]=static_cast<T>(111)+ 29*z[7];
    z[28]=z[7]*z[28];
    z[28]=static_cast<T>(135)+ z[28];
    z[28]=z[7]*z[28];
    z[28]=static_cast<T>(53)+ z[28];
    z[30]= - n<T>(49,2) - z[24];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(87,2) + z[30];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(67,2) + z[30];
    z[30]=z[7]*z[30];
    z[30]= - n<T>(19,2) + z[30];
    z[30]=z[10]*z[30];
    z[28]=n<T>(1,2)*z[28] + z[30];
    z[28]=z[28]*z[25];
    z[18]=z[28] - n<T>(19,4) + z[18];
    z[18]=z[5]*z[18];
    z[28]=static_cast<T>(29)+ n<T>(19,2)*z[7];
    z[28]=z[7]*z[28];
    z[18]=z[18] + n<T>(69,4) + z[28];
    z[28]= - n<T>(183,2) - z[73];
    z[28]=z[28]*z[60];
    z[28]= - static_cast<T>(17)+ z[28];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(127,16) + z[28];
    z[30]=n<T>(71,4) + z[58];
    z[30]=z[7]*z[30];
    z[30]=n<T>(591,16) + z[30];
    z[30]=z[7]*z[30];
    z[30]=n<T>(261,8) + z[30];
    z[30]=z[7]*z[30];
    z[30]=n<T>(167,16) + z[30];
    z[30]=z[10]*z[30];
    z[28]=3*z[28] + z[30];
    z[28]=z[10]*z[28];
    z[18]=z[28] + n<T>(1,2)*z[18];
    z[18]=z[5]*z[18];
    z[24]=static_cast<T>(39)+ z[24];
    z[24]=z[7]*z[24];
    z[24]=static_cast<T>(43)+ z[24];
    z[24]=z[24]*z[25];
    z[24]=z[24] - static_cast<T>(33)- z[75];
    z[24]=z[24]*z[66];
    z[18]=z[27] - z[2] + z[18] + z[24] + static_cast<T>(11)+ z[32];
    z[18]=z[8]*z[18];
    z[24]=z[47]*z[50];
    z[27]=z[49]*z[65];
    z[24]=z[24] - z[27];
    z[24]=z[24]*z[10];
    z[27]=z[51]*z[48];
    z[24]=z[24] + z[27];
    z[28]=z[39]*z[48];
    z[28]=z[24] + z[28];
    z[30]=n<T>(1,4)*z[15];
    z[28]=z[28]*z[30];
    z[24]=z[5]*z[24];
    z[30]=z[78] + 9;
    z[30]=z[30]*z[7];
    z[30]=z[30] + n<T>(15,2);
    z[30]=z[30]*z[37];
    z[30]=z[30] + 1;
    z[30]=z[30]*z[59];
    z[31]=z[7] + n<T>(13,8);
    z[31]=z[31]*z[7];
    z[31]=z[31] + n<T>(5,8);
    z[31]=z[31]*z[7];
    z[30]=z[30] - z[31];
    z[30]=z[30]*z[10];
    z[31]=z[78] + 3;
    z[32]= - z[31]*z[60];
    z[36]=z[5]*z[48];
    z[36]= - z[7] + z[36];
    z[36]=z[8]*z[36];
    z[24]= - z[28] + n<T>(1,8)*z[36] + n<T>(1,4)*z[24] + z[32] - z[30];
    z[24]=z[15]*z[5]*z[24];
    z[32]= - static_cast<T>(1)+ z[58];
    z[32]=z[32]*z[46];
    z[36]=static_cast<T>(1)- z[41];
    z[36]=z[36]*z[68];
    z[32]=z[32] + z[36];
    z[32]=z[10]*z[32];
    z[36]= - n<T>(1,4) + z[7];
    z[36]=z[36]*z[68];
    z[36]= - n<T>(1,2)*z[67] + z[36];
    z[36]=z[10]*z[36];
    z[36]=z[36] - z[64];
    z[36]=z[3]*z[36];
    z[32]=z[36] - n<T>(1,4)*z[48] + z[32];
    z[32]=z[3]*z[32];
    z[36]=z[62]*z[48];
    z[21]=z[21]*z[68];
    z[21]= - n<T>(3,2)*z[36] + z[21];
    z[21]=z[10]*z[21];
    z[36]=z[55]*z[37];
    z[21]=z[32] + z[36] + z[21];
    z[21]=z[3]*z[21];
    z[32]= - static_cast<T>(3)+ z[7];
    z[32]=z[32]*z[46];
    z[36]=static_cast<T>(1)- z[42];
    z[36]=z[36]*z[68];
    z[32]=z[32] + z[36];
    z[32]=z[10]*z[32];
    z[36]=static_cast<T>(1)- z[37];
    z[36]=z[36]*z[37];
    z[25]= - z[67]*z[25];
    z[25]=z[48] + z[25];
    z[25]=z[10]*z[25];
    z[25]= - z[37] + z[25];
    z[25]=z[25]*z[39];
    z[21]=z[25] + z[21] + z[36] + z[32];
    z[21]=z[14]*z[21];
    z[25]=z[8] + z[31];
    z[25]=z[60]*z[25];
    z[25]=z[28] + z[30] + z[25];
    z[25]=z[15]*z[25];
    z[27]=z[10]*z[27];
    z[28]= - static_cast<T>(5)- z[7];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(3,2) + z[28];
    z[28]=z[7]*z[28];
    z[27]=z[28] + n<T>(5,2)*z[27];
    z[27]=z[10]*z[27];
    z[28]=z[2] + z[20];
    z[28]=z[7]*z[28];
    z[27]=z[27] + z[28];
    z[28]= - z[62]*z[50];
    z[28]=z[56] + z[28];
    z[28]=z[28]*z[52];
    z[27]=n<T>(1,2)*z[27] + z[28];
    z[25]=n<T>(1,4)*z[27] + z[25];
    z[25]=z[13]*z[25];
    z[27]= - static_cast<T>(1717)- n<T>(1541,3)*z[7];
    z[27]=z[27]*z[29];
    z[27]= - static_cast<T>(607)+ z[27];
    z[27]=z[7]*z[27];
    z[27]= - n<T>(1871,9) + z[27];
    z[28]=static_cast<T>(54)+ n<T>(421,36)*z[7];
    z[28]=z[7]*z[28];
    z[28]=n<T>(1039,12) + z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(2167,36) + z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(367,24) + z[28];
    z[28]=z[10]*z[28];
    z[27]=n<T>(1,4)*z[27] + z[28];
    z[27]=z[10]*z[27];
    z[28]= - z[53]*z[35];
    z[28]= - n<T>(197,2) + z[28];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(188,3) + z[28];
    z[28]=z[7]*z[28];
    z[28]= - n<T>(179,12) + z[28];
    z[28]=z[10]*z[28];
    z[29]=static_cast<T>(197)+ z[44];
    z[29]=z[7]*z[29];
    z[29]=static_cast<T>(188)+ z[29];
    z[29]=z[7]*z[29];
    z[28]=z[28] + n<T>(179,3) + z[29];
    z[28]=z[10]*z[28];
    z[29]= - n<T>(376,3) - n<T>(197,2)*z[7];
    z[29]=z[7]*z[29];
    z[28]=z[28] - n<T>(179,4) + z[29];
    z[28]=z[28]*z[34];
    z[29]=n<T>(212,3) + n<T>(113,2)*z[7];
    z[29]=z[7]*z[29];
    z[27]=z[28] + z[27] + n<T>(1925,72) + z[29];
    z[27]=z[3]*z[27];
    z[28]=n<T>(1307,3) + n<T>(233,2)*z[7];
    z[28]=z[7]*z[28];
    z[28]=n<T>(1853,6) + z[28];
    z[28]=z[28]*z[37];
    z[29]= - n<T>(227,2) - n<T>(56,3)*z[7];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(695,4) + z[29];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(2095,24) + z[29];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(95,16) + z[29];
    z[29]=z[10]*z[29];
    z[28]=z[29] - n<T>(11,3) + z[28];
    z[28]=z[10]*z[28];
    z[29]=static_cast<T>(647)- 1325*z[7];
    z[29]=z[7]*z[29];
    z[29]=n<T>(2825,2) + z[29];
    z[28]=n<T>(1,24)*z[29] + z[28];
    z[27]=n<T>(1,3)*z[28] + z[27];
    z[27]=z[3]*z[27];
    z[28]= - n<T>(583,18) - z[7];
    z[28]=z[7]*z[28];
    z[28]=n<T>(2167,72) + z[28];
    z[28]=z[7]*z[28];
    z[28]=n<T>(263,18) + z[28];
    z[29]=n<T>(56,9) + z[42];
    z[29]=z[7]*z[29];
    z[29]=n<T>(95,12) + z[29];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(703,48) + z[29];
    z[29]=z[7]*z[29];
    z[29]= - n<T>(733,144) + z[29];
    z[29]=z[10]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[28]=z[10]*z[28];
    z[29]= - n<T>(103,9) + z[45];
    z[29]=z[7]*z[29];
    z[29]=n<T>(145,4) + z[29];
    z[27]=z[27] + n<T>(1,4)*z[29] + z[28];
    z[27]=z[3]*z[27];
    z[28]=z[2] - z[33];
    z[28]=z[2]*z[28];
    z[28]=z[28] - z[51];
    z[28]=z[2]*z[28];
    z[29]=static_cast<T>(1)- z[2];
    z[29]=z[2]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[2]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[23]=z[29]*z[23];
    z[20]=z[23] + z[28] + z[20];
    z[20]=z[1]*z[20];
    z[23]=z[7] - z[70];
    z[23]=z[2]*z[23];
    z[23]=z[23] + n<T>(1,2) - z[48];
    z[23]=z[2]*z[23];
    z[20]=z[20] - z[61] + z[23];
    z[20]=z[4]*z[2]*z[20];

    r += z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + z[24]
       + z[25] + z[26] + z[27] + n<T>(1,2)*z[40];
 
    return r;
}

template double qqb_2lha_r755(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r755(const std::array<dd_real,30>&);
#endif
