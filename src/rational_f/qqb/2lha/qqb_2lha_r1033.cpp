#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1033(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[13];
    z[8]=k[9];
    z[9]=n<T>(1,2)*z[4];
    z[10]= - static_cast<T>(7)- 5*z[2];
    z[10]=z[10]*z[9];
    z[11]= - z[5]*z[4];
    z[10]=z[10] + z[11];
    z[11]=n<T>(1,2)*z[6];
    z[10]=z[10]*z[11];
    z[12]= - n<T>(13,4) - 3*z[2];
    z[12]=z[4]*z[12];
    z[13]=n<T>(1,2)*z[5];
    z[14]= - z[13] - n<T>(9,4) - z[4];
    z[14]=z[5]*z[14];
    z[10]=z[10] + z[14] + z[12] - static_cast<T>(3)- n<T>(5,4)*z[2];
    z[10]=z[10]*z[11];
    z[12]=n<T>(1,2) - z[7];
    z[12]=z[5]*z[12];
    z[12]=z[12] + z[4] - n<T>(3,2) - z[7];
    z[12]=z[13]*z[12];
    z[12]=z[12] - 1;
    z[12]=z[7]*z[12];
    z[13]= - n<T>(1,4)*z[8] + z[7];
    z[9]=z[13]*z[9];
    z[13]= - static_cast<T>(3)+ n<T>(1,2)*z[1];
    z[13]=z[2]*z[13];
    z[13]= - static_cast<T>(3)+ z[13];
    z[9]=z[10] + z[9] + n<T>(1,2)*z[13] + z[12];
    z[9]=z[3]*z[9];
    z[10]= - z[5] - 1;
    z[12]=z[4]*npow(z[7],2);
    z[12]=z[12] - z[7];
    z[10]=z[12]*z[10];
    z[12]= - n<T>(7,2) - z[5];
    z[12]=z[5]*z[12];
    z[12]= - n<T>(5,2) + z[12];
    z[11]=z[12]*z[11];
    z[11]=z[11] - n<T>(7,4) - z[5];
    z[11]=z[6]*z[11];
    z[12]=n<T>(1,2)*z[8] - z[2];
    z[10]=z[11] + n<T>(1,2)*z[12] + z[10];
    z[9]=n<T>(1,2)*z[10] + z[9];

    r += n<T>(1,2)*z[9]*z[3];
 
    return r;
}

template double qqb_2lha_r1033(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1033(const std::array<dd_real,30>&);
#endif
