#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1777(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=n<T>(3,4)*z[5];
    z[10]= - static_cast<T>(5)+ z[5];
    z[10]=z[10]*z[9];
    z[11]=z[5] + 1;
    z[12]=z[3]*z[5];
    z[13]=z[11]*z[12];
    z[14]=n<T>(19,4) + z[5];
    z[14]=z[5]*z[14];
    z[13]=z[14] - n<T>(5,2)*z[13];
    z[13]=z[3]*z[13];
    z[10]=z[10] + z[13];
    z[13]=n<T>(1,2)*z[3];
    z[10]=z[10]*z[13];
    z[14]=5*z[3];
    z[15]=n<T>(1,4)*z[4];
    z[16]= - static_cast<T>(1)- z[15];
    z[16]=z[16]*z[14];
    z[16]=z[16] + static_cast<T>(1)- n<T>(7,8)*z[4];
    z[16]=z[3]*z[16];
    z[17]=z[4] + 1;
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[3]*z[16];
    z[18]=z[3]*z[4];
    z[19]=n<T>(1,2)*z[4];
    z[20]=static_cast<T>(5)+ z[19];
    z[20]=z[20]*z[18];
    z[21]= - n<T>(1,2) + z[4];
    z[21]=z[4]*z[21];
    z[20]=z[21] + z[20];
    z[21]=npow(z[3],2);
    z[20]=z[20]*z[21];
    z[22]=npow(z[4],2);
    z[23]=z[22]*npow(z[3],3);
    z[24]=z[23]*z[1];
    z[20]=z[20] - z[24];
    z[25]=n<T>(1,2)*z[1];
    z[20]=z[20]*z[25];
    z[16]=z[16] + z[20];
    z[16]=z[16]*z[25];
    z[20]=n<T>(1,2) + z[5];
    z[20]=z[20]*z[14];
    z[11]= - n<T>(3,2)*z[11] + z[20];
    z[11]=z[3]*z[11];
    z[20]=static_cast<T>(1)- z[5];
    z[11]=n<T>(3,4)*z[20] + z[11];
    z[11]=z[11]*z[13];
    z[20]=n<T>(1,3)*z[22];
    z[11]=z[16] + z[11] - n<T>(1,8) + z[20];
    z[11]=z[1]*z[11];
    z[16]= - n<T>(11,2) + z[6];
    z[26]= - n<T>(1,2) - z[6];
    z[26]=z[4]*z[26];
    z[16]=n<T>(1,2)*z[16] + z[26];
    z[26]=n<T>(1,3)*z[4];
    z[16]=z[16]*z[26];
    z[10]=z[11] + z[10] + n<T>(9,16)*z[5] + z[16];
    z[10]=z[1]*z[10];
    z[11]=npow(z[5],2);
    z[16]=static_cast<T>(17)- z[5];
    z[16]=z[16]*z[11];
    z[27]=n<T>(1,4)*z[3];
    z[28]=n<T>(5,2) + z[5];
    z[28]=z[28]*z[27];
    z[29]=n<T>(1,8)*z[5];
    z[28]=z[28] - static_cast<T>(2)- z[29];
    z[30]=z[11]*z[3];
    z[28]=z[28]*z[30];
    z[16]=n<T>(1,8)*z[16] + z[28];
    z[16]=z[3]*z[16];
    z[28]=z[6] + 1;
    z[31]=n<T>(1,2)*z[6];
    z[32]= - z[28]*z[31];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[32]*z[26];
    z[33]=n<T>(3,4)*z[11];
    z[34]= - static_cast<T>(2)- z[31];
    z[34]=z[6]*z[34];
    z[34]=n<T>(5,2) + z[34];
    z[10]=z[10] + z[16] + z[32] + n<T>(1,3)*z[34] - z[33];
    z[10]=z[1]*z[10];
    z[16]=z[23]*z[25];
    z[23]=z[19] - z[18];
    z[23]=z[23]*z[21];
    z[23]=3*z[23] + z[16];
    z[23]=z[1]*z[23];
    z[32]= - static_cast<T>(1)+ z[3];
    z[32]=z[32]*z[14];
    z[32]=static_cast<T>(1)+ z[32];
    z[34]=n<T>(3,2)*z[3];
    z[32]=z[32]*z[34];
    z[23]=z[32] + z[23];
    z[23]=z[23]*z[25];
    z[32]=n<T>(3,2)*z[5];
    z[35]=z[32] - z[12];
    z[35]=z[35]*z[14];
    z[36]=3*z[5];
    z[35]= - z[36] + z[35];
    z[35]=z[3]*z[35];
    z[37]=n<T>(1,4)*z[5];
    z[23]=z[23] + z[35] + z[37] - z[20];
    z[23]=z[1]*z[23];
    z[35]= - static_cast<T>(1)+ z[13];
    z[14]=z[14]*z[35];
    z[14]=static_cast<T>(3)+ z[14];
    z[14]=z[34]*z[11]*z[14];
    z[35]=z[6] - 1;
    z[38]= - z[4]*z[35];
    z[14]=z[23] + z[14] - z[33] + z[38];
    z[14]=z[14]*z[25];
    z[23]=npow(z[5],3);
    z[33]=z[23]*z[3];
    z[38]=n<T>(5,2)*z[23] - z[33];
    z[38]=z[38]*z[13];
    z[38]= - z[23] + z[38];
    z[34]=z[38]*z[34];
    z[38]=z[31] - 1;
    z[38]=z[38]*z[6];
    z[38]=z[38] + n<T>(1,2);
    z[14]=z[14] + z[34] + n<T>(3,8)*z[23] - z[38];
    z[14]=z[1]*z[14];
    z[34]= - static_cast<T>(1)+ n<T>(1,3)*z[6];
    z[34]=z[34]*z[6];
    z[34]=z[34] + 1;
    z[34]=z[34]*z[6];
    z[34]=z[34] - n<T>(1,3);
    z[39]= - n<T>(1,4)*z[23] - z[34];
    z[39]=z[5]*z[39];
    z[40]=z[3] - 3;
    z[41]=z[40]*z[3];
    z[41]=static_cast<T>(3)+ z[41];
    z[41]=z[27]*z[41]*npow(z[5],4);
    z[39]=z[39] + z[41];
    z[14]=n<T>(1,2)*z[39] + z[14];
    z[14]=z[2]*z[14];
    z[11]=n<T>(5,8)*z[11] - z[38];
    z[11]=z[5]*z[11];
    z[33]=n<T>(9,2)*z[23] - z[33];
    z[33]=z[33]*z[13];
    z[23]= - 3*z[23] + z[33];
    z[23]=z[23]*z[13];
    z[11]=z[23] + z[11] + z[34];
    z[10]=z[14] + n<T>(1,2)*z[11] + z[10];
    z[10]=z[2]*z[10];
    z[11]=n<T>(11,4) + z[4];
    z[11]=z[11]*z[15];
    z[14]=n<T>(3,4) + z[4];
    z[14]=z[3]*z[14];
    z[11]=z[11] + z[14];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,8)*z[22] + z[11];
    z[11]=z[3]*z[11];
    z[14]= - static_cast<T>(1)- z[19];
    z[14]=z[14]*z[18];
    z[14]= - n<T>(1,2)*z[22] + z[14];
    z[14]=z[14]*z[21];
    z[14]=z[14] + n<T>(1,4)*z[24];
    z[14]=z[14]*z[25];
    z[11]=z[11] + z[14];
    z[11]=z[1]*z[11];
    z[14]=z[5] + 3;
    z[15]= - z[3]*z[14];
    z[15]= - n<T>(9,8)*z[17] + z[15];
    z[15]=z[3]*z[15];
    z[17]=z[4] - 1;
    z[15]= - n<T>(3,8)*z[17] + z[15];
    z[15]=z[3]*z[15];
    z[15]= - z[20] + z[15];
    z[11]=n<T>(1,2)*z[15] + z[11];
    z[11]=z[1]*z[11];
    z[15]=z[4]*z[28];
    z[15]=n<T>(5,4) + z[15];
    z[15]=z[15]*z[26];
    z[19]=static_cast<T>(1)+ z[29];
    z[19]=z[19]*z[12];
    z[14]=n<T>(1,16)*z[14] + z[19];
    z[14]=z[3]*z[14];
    z[19]=n<T>(3,2) - z[5];
    z[14]=n<T>(3,8)*z[19] + z[14];
    z[14]=z[3]*z[14];
    z[11]=z[11] + z[14] - n<T>(3,16) + z[15];
    z[11]=z[1]*z[11];
    z[14]= - static_cast<T>(17)+ z[36];
    z[14]=z[14]*z[37];
    z[15]=static_cast<T>(5)+ z[5];
    z[15]=z[15]*z[37];
    z[15]=z[15] - z[30];
    z[15]=z[3]*z[15];
    z[14]=z[14] + z[15];
    z[14]=z[14]*z[27];
    z[15]= - static_cast<T>(7)- 5*z[6];
    z[19]=z[31] + 1;
    z[23]= - z[4]*z[6]*z[19];
    z[15]=n<T>(1,4)*z[15] + z[23];
    z[15]=z[15]*z[26];
    z[19]=n<T>(9,8)*z[5] - z[19];
    z[11]=z[11] + z[14] + n<T>(1,2)*z[19] + z[15];
    z[11]=z[1]*z[11];
    z[14]= - z[32] - z[35];
    z[14]=z[5]*z[14];
    z[15]= - z[40]*z[30];
    z[14]=n<T>(3,8)*z[15] + n<T>(1,2)*z[14] + z[38];
    z[10]=z[10] + n<T>(1,2)*z[14] + z[11];
    z[10]=z[2]*z[10];
    z[11]=z[8] + 1;
    z[14]=z[11]*z[8];
    z[15]=npow(z[8],2);
    z[19]=z[15]*z[7];
    z[23]= - z[14] + z[19];
    z[23]=z[7]*z[23];
    z[23]=z[8] + z[23];
    z[23]=z[7]*z[23];
    z[24]=z[15]*npow(z[7],2);
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[31];
    z[23]=z[23] + z[24];
    z[23]=z[6]*z[23];
    z[14]= - z[14] + n<T>(1,2)*z[19];
    z[14]=z[7]*z[14];
    z[24]=static_cast<T>(2)+ n<T>(1,2)*z[8];
    z[24]=z[8]*z[24];
    z[14]=z[14] + n<T>(1,2) + z[24];
    z[14]=z[7]*z[14];
    z[11]=z[14] - z[11];
    z[11]=z[7]*z[11];
    z[11]=z[23] + n<T>(1,2) + z[11];
    z[11]=z[11]*z[26];
    z[14]=z[8] + n<T>(7,4);
    z[14]=z[14]*z[8];
    z[23]=z[14] - z[19];
    z[24]=n<T>(1,3)*z[7];
    z[23]=z[23]*z[24];
    z[28]= - static_cast<T>(1)- n<T>(7,3)*z[8];
    z[23]=n<T>(1,4)*z[28] + z[23];
    z[23]=z[7]*z[23];
    z[14]=z[14] - 2*z[19];
    z[14]=z[14]*z[24];
    z[19]=n<T>(1,3)*z[19];
    z[24]= - z[6]*z[19];
    z[14]=z[24] - n<T>(1,4) + z[14];
    z[14]=z[6]*z[14];
    z[11]=z[11] + z[14] + n<T>(1,4) + z[23];
    z[11]=z[4]*z[11];
    z[14]=n<T>(1,2)*z[7];
    z[9]=n<T>(1,4)*z[12] - z[9] + static_cast<T>(1)+ z[14];
    z[9]=z[9]*z[27];
    z[18]= - z[22] - n<T>(3,2)*z[18];
    z[18]=z[18]*z[21];
    z[16]=z[18] + z[16];
    z[16]=z[1]*z[16];
    z[18]=n<T>(3,2)*z[4] + z[3];
    z[18]=z[3]*z[18];
    z[18]=z[22] + 3*z[18];
    z[13]=z[18]*z[13];
    z[13]=z[13] + z[16];
    z[13]=z[13]*z[25];
    z[16]= - static_cast<T>(3)- z[12];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(3,2)*z[17] + z[16];
    z[16]=z[16]*z[27];
    z[13]=z[13] - z[20] + z[16];
    z[13]=z[13]*z[25];
    z[16]=z[6]*z[26];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[4]*z[16];
    z[9]=z[13] + z[9] - n<T>(7,16) + z[16];
    z[9]=z[1]*z[9];
    z[13]=static_cast<T>(1)- z[8];
    z[15]=z[6]*z[15];
    z[13]=n<T>(1,6)*z[15] + n<T>(1,4)*z[13] + z[19];
    z[13]=z[6]*z[13];
    z[15]= - n<T>(1,2) - z[8];
    z[15]=n<T>(1,2)*z[15] + z[19];
    z[14]=z[15]*z[14];

    r += n<T>(5,16)*z[5] + z[9] + z[10] + z[11] - n<T>(3,16)*z[12] + z[13] + 
      z[14];
 
    return r;
}

template double qqb_2lha_r1777(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1777(const std::array<dd_real,30>&);
#endif
