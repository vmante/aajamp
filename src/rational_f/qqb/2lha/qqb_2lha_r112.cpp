#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r112(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[13];
    z[6]=k[3];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[24];
    z[10]=k[4];
    z[11]=z[3]*z[10];
    z[12]= - static_cast<T>(1)- z[10];
    z[11]=3*z[12] + z[11];
    z[12]=n<T>(1,2)*z[3];
    z[11]=z[11]*z[12];
    z[13]=n<T>(3,2)*z[3];
    z[14]= - static_cast<T>(1)- z[3];
    z[14]=z[14]*z[13];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[1]*z[14];
    z[11]=z[14] + z[11] - 3*z[6] + n<T>(9,2) + z[10];
    z[14]=z[6] - 1;
    z[15]=n<T>(1,2)*z[6];
    z[16]= - z[14]*z[15];
    z[17]=n<T>(3,4)*z[3];
    z[18]=static_cast<T>(5)+ z[3];
    z[18]=z[18]*z[17];
    z[19]=n<T>(1,4)*z[5];
    z[20]=z[19] - 1;
    z[18]=z[18] + z[20];
    z[21]=n<T>(1,2)*z[1];
    z[18]=z[18]*z[21];
    z[18]=z[18] + z[17] - n<T>(13,4) + z[6];
    z[18]=z[1]*z[18];
    z[16]=z[16] + z[18];
    z[18]= - static_cast<T>(9)- z[3];
    z[18]=z[3]*z[18];
    z[18]= - z[5] + z[18];
    z[18]=z[1]*z[18];
    z[18]=z[18] + z[5];
    z[18]= - n<T>(3,16)*z[3] + static_cast<T>(1)+ n<T>(1,16)*z[18];
    z[18]=z[1]*z[18];
    z[18]= - z[15] + z[18];
    z[18]=z[4]*z[1]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[16]=z[4]*z[16];
    z[13]=static_cast<T>(1)- z[13];
    z[13]=z[1]*z[13];
    z[13]=z[13] - z[14];
    z[13]=z[1]*z[13];
    z[14]=n<T>(1,2)*z[4];
    z[18]=z[1]*z[3];
    z[22]= - static_cast<T>(1)+ z[18];
    z[22]=z[22]*npow(z[1],2)*z[14];
    z[13]=z[13] + z[22];
    z[13]=z[13]*z[14];
    z[22]= - z[12] + 1;
    z[22]=z[10]*z[22];
    z[15]= - z[15] + static_cast<T>(1)- n<T>(1,2)*z[10];
    z[15]=z[6]*z[15];
    z[15]=z[15] - n<T>(1,2) + z[22];
    z[17]=z[17] - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[17]=z[1]*z[17];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[17];
    z[13]=z[2]*z[13];
    z[11]=z[13] + n<T>(1,8)*z[11] + z[16];
    z[11]=z[2]*z[11];
    z[13]=n<T>(7,3)*z[3];
    z[15]=n<T>(1,2) + z[10];
    z[15]=z[15]*z[13];
    z[15]=z[15] + static_cast<T>(1)- n<T>(1,3)*z[10];
    z[15]=z[3]*z[15];
    z[16]=static_cast<T>(1)- n<T>(25,2)*z[3];
    z[16]=z[16]*z[18];
    z[17]=z[6]*z[9];
    z[15]=n<T>(1,12)*z[16] + n<T>(1,4)*z[15] + n<T>(7,6) + z[17];
    z[16]=n<T>(3,2) - z[13];
    z[16]=z[16]*z[12];
    z[16]=z[16] + z[20];
    z[17]=z[1]*npow(z[3],2);
    z[16]=n<T>(1,2)*z[16] + n<T>(1,3)*z[17];
    z[16]=z[16]*z[21];
    z[13]= - static_cast<T>(5)+ z[13];
    z[13]=z[3]*z[13];
    z[13]=z[17] - 3*z[5] + z[13];
    z[13]=z[1]*z[13];
    z[13]=n<T>(1,4)*z[13] - n<T>(1,12)*z[3] + static_cast<T>(1)+ z[19];
    z[13]=z[13]*z[21];
    z[18]= - n<T>(1,4) + z[9];
    z[18]=z[6]*z[18];
    z[13]=z[13] + n<T>(9,8) + z[18];
    z[13]=z[13]*z[14];
    z[14]=n<T>(1,4) + z[9];
    z[14]=z[6]*z[14];
    z[13]=z[13] + z[16] + n<T>(1,48)*z[3] + n<T>(37,48) + z[14];
    z[13]=z[4]*z[13];
    z[11]=z[11] + n<T>(1,2)*z[15] + z[13];
    z[11]=z[2]*z[11];
    z[13]= - z[9] + n<T>(11,12)*z[8];
    z[13]=z[13]*z[7];
    z[14]=static_cast<T>(1)- 11*z[8];
    z[14]=z[13] + n<T>(1,12)*z[14] + z[9];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(1,12) + z[14];
    z[14]=z[14]*z[12];
    z[15]= - n<T>(7,6)*z[17] - z[5] + n<T>(1,3)*z[3];
    z[15]=z[1]*z[15];
    z[15]=z[15] - n<T>(11,3) - n<T>(1,2)*z[8];
    z[16]=n<T>(25,24)*z[8] + z[9];
    z[16]=z[7]*z[16];
    z[18]=z[6]*z[8];
    z[14]=z[14] + n<T>(1,16)*z[18] + n<T>(1,2)*z[16] - z[9] + n<T>(1,8)*z[15];
    z[14]=z[4]*z[14];
    z[13]=n<T>(1,8) - z[13];
    z[12]=z[13]*z[12];
    z[12]=z[14] + n<T>(7,16)*z[17] + z[12] - n<T>(35,48)*z[8] - z[9];
    z[12]=z[4]*z[12];
    z[13]=static_cast<T>(1)- 7*z[3];
    z[13]=z[3]*z[13];

    r += z[11] + z[12] + n<T>(1,24)*z[13];
 
    return r;
}

template double qqb_2lha_r112(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r112(const std::array<dd_real,30>&);
#endif
