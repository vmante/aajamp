#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1875(const std::array<T,30>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[4];
    z[7]=k[15];
    z[8]=k[2];
    z[9]=k[10];
    z[10]=z[6] + 3;
    z[11]=n<T>(1,2)*z[6];
    z[12]=z[10]*z[11];
    z[13]= - static_cast<T>(2)- z[12];
    z[13]=z[6]*z[13];
    z[14]=z[6] + n<T>(5,2);
    z[15]= - z[14]*z[11];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[6]*z[15];
    z[15]= - n<T>(1,4) + z[15];
    z[16]=z[11]*z[9];
    z[15]=z[15]*z[16];
    z[13]=z[15] - static_cast<T>(1)+ z[13];
    z[13]=z[9]*z[13];
    z[15]=z[6] + n<T>(7,2);
    z[17]=n<T>(1,4)*z[6];
    z[18]=z[15]*z[17];
    z[18]=z[18] + 1;
    z[18]=z[18]*z[6];
    z[18]=z[18] + n<T>(3,8);
    z[19]=z[18]*z[9];
    z[20]=z[14]*z[6];
    z[21]=z[20] + n<T>(3,2);
    z[19]=z[19] + n<T>(1,4)*z[21];
    z[21]=z[2]*z[9];
    z[19]=z[19]*z[21];
    z[22]= - z[15]*z[11];
    z[22]= - static_cast<T>(1)+ z[22];
    z[13]= - z[19] + n<T>(1,2)*z[22] + z[13];
    z[13]=z[2]*z[13];
    z[22]= - static_cast<T>(3)+ z[6];
    z[22]=z[6]*z[22];
    z[22]= - n<T>(13,2) + z[22];
    z[22]=z[6]*z[22];
    z[23]=z[6] + n<T>(3,2);
    z[24]=z[23]*z[6];
    z[25]= - n<T>(1,2) + z[24];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(3,2) + z[25];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(1,2) + z[25];
    z[25]=z[9]*z[25];
    z[22]=z[25] - n<T>(5,2) + z[22];
    z[25]=n<T>(1,2)*z[9];
    z[22]=z[22]*z[25];
    z[22]=z[22] - static_cast<T>(1)- z[24];
    z[13]=n<T>(1,2)*z[22] + z[13];
    z[13]=z[2]*z[13];
    z[22]=3*z[6];
    z[26]= - n<T>(29,2) - z[22];
    z[26]=z[26]*z[17];
    z[26]= - static_cast<T>(5)+ z[26];
    z[26]=z[6]*z[26];
    z[27]=z[6] + 7;
    z[27]=z[27]*z[6];
    z[28]= - static_cast<T>(15)- z[27];
    z[28]=z[6]*z[28];
    z[28]= - static_cast<T>(13)+ z[28];
    z[28]=z[28]*z[17];
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[28]*z[25];
    z[26]=z[28] - n<T>(17,8) + z[26];
    z[26]=z[9]*z[26];
    z[28]=z[17] + 1;
    z[28]=z[28]*z[6];
    z[29]=z[28] + n<T>(3,2);
    z[29]=z[29]*z[6];
    z[29]=z[29] + 1;
    z[29]=z[29]*z[6];
    z[29]=z[29] + n<T>(1,4);
    z[29]=z[29]*z[9];
    z[10]=z[10]*z[6];
    z[30]=z[10] + 3;
    z[30]=z[30]*z[6];
    z[31]=z[30] + 1;
    z[29]=z[29] + z[31];
    z[29]=z[29]*z[9];
    z[32]=z[11] + 1;
    z[33]=z[32]*z[6];
    z[34]=z[33] + n<T>(1,2);
    z[29]=z[29] + n<T>(3,2)*z[34];
    z[29]=z[29]*z[3];
    z[35]=n<T>(5,2)*z[6];
    z[36]= - static_cast<T>(11)- z[35];
    z[36]=z[36]*z[11];
    z[36]= - static_cast<T>(9)+ z[36];
    z[36]=z[6]*z[36];
    z[36]= - n<T>(13,2) + z[36];
    z[36]=z[6]*z[36];
    z[36]= - n<T>(7,4) + z[36];
    z[36]=z[36]*z[25];
    z[37]= - static_cast<T>(9)- n<T>(11,4)*z[6];
    z[37]=z[6]*z[37];
    z[37]= - n<T>(39,4) + z[37];
    z[37]=z[6]*z[37];
    z[36]=z[36] - n<T>(7,2) + z[37];
    z[36]=z[9]*z[36];
    z[36]=z[29] - n<T>(17,4)*z[34] + z[36];
    z[36]=z[3]*z[36];
    z[37]=5*z[6];
    z[38]=n<T>(77,4) + z[37];
    z[38]=z[6]*z[38];
    z[38]=n<T>(47,2) + z[38];
    z[38]=z[6]*z[38];
    z[39]=n<T>(21,4) + z[6];
    z[39]=z[6]*z[39];
    z[39]=n<T>(39,4) + z[39];
    z[39]=z[6]*z[39];
    z[39]=n<T>(31,4) + z[39];
    z[39]=z[6]*z[39];
    z[39]=n<T>(9,4) + z[39];
    z[39]=z[9]*z[39];
    z[38]=z[39] + n<T>(37,4) + z[38];
    z[38]=z[38]*z[25];
    z[39]=static_cast<T>(2)+ z[6];
    z[39]=z[6]*z[39];
    z[39]=static_cast<T>(1)+ z[39];
    z[36]=z[36] + 2*z[39] + z[38];
    z[36]=z[3]*z[36];
    z[38]=z[9]*z[6];
    z[39]=z[31]*z[38];
    z[18]=n<T>(1,8)*z[39] + z[18];
    z[18]=z[9]*z[18];
    z[31]=z[31]*z[25];
    z[31]=z[31] + z[34];
    z[31]=z[31]*z[21];
    z[31]=z[31] + n<T>(1,2) + z[24];
    z[18]=z[18] + n<T>(1,4)*z[31];
    z[18]=z[2]*z[18];
    z[31]=z[34]*z[38];
    z[34]=z[6] + 1;
    z[39]=n<T>(1,2)*z[34] + z[31];
    z[18]=n<T>(1,2)*z[39] + z[18];
    z[18]=z[2]*z[18];
    z[39]=z[6]*z[34];
    z[18]=n<T>(1,8)*z[39] + z[18];
    z[18]=z[4]*z[18];
    z[27]= - static_cast<T>(5)- z[27];
    z[13]=z[18] + z[36] + z[13] + n<T>(1,8)*z[27] + z[26];
    z[13]=z[4]*z[13];
    z[18]= - n<T>(19,2) - z[22];
    z[18]=z[18]*z[17];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[6]*z[18];
    z[26]= - n<T>(7,2) - z[33];
    z[26]=z[6]*z[26];
    z[26]= - static_cast<T>(3)+ z[26];
    z[26]=z[26]*z[38];
    z[18]=z[18] + n<T>(3,4)*z[26];
    z[18]=z[9]*z[18];
    z[26]= - static_cast<T>(11)- z[6];
    z[26]=z[26]*z[17];
    z[12]= - static_cast<T>(1)- z[12];
    z[27]= - n<T>(5,4) - z[28];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[9]*z[27];
    z[12]=n<T>(1,2)*z[12] + z[27];
    z[12]=z[12]*z[21];
    z[12]=n<T>(3,2)*z[12] + z[26] + z[18];
    z[12]=z[2]*z[12];
    z[18]=npow(z[6],2);
    z[21]= - n<T>(13,4) + z[6];
    z[21]=z[21]*z[18];
    z[26]=z[18]*z[9];
    z[27]=z[6] + n<T>(1,2);
    z[27]=z[27]*z[6];
    z[36]= - static_cast<T>(3)+ z[27];
    z[36]=z[36]*z[26];
    z[21]=z[21] + n<T>(3,4)*z[36];
    z[21]=z[9]*z[21];
    z[12]=z[21] + z[12];
    z[12]=z[2]*z[12];
    z[21]=n<T>(1,2)*z[18];
    z[36]= - z[14]*z[21];
    z[39]= - n<T>(3,4) - z[28];
    z[39]=z[39]*z[26];
    z[36]=z[36] + z[39];
    z[36]=z[9]*z[36];
    z[39]= - z[34]*z[11];
    z[31]=z[39] - z[31];
    z[31]=z[2]*z[31]*z[25];
    z[31]=z[31] - z[21] + z[36];
    z[31]=z[2]*z[31];
    z[36]=npow(z[6],3);
    z[39]=z[36]*z[23];
    z[40]= - z[25]*z[39];
    z[40]= - z[36] + z[40];
    z[40]=z[9]*z[40];
    z[31]=z[40] + z[31];
    z[31]=z[2]*z[31];
    z[40]=npow(z[9],2)*npow(z[6],4);
    z[31]= - n<T>(1,4)*z[40] + z[31];
    z[31]=z[7]*z[31];
    z[41]=n<T>(3,2)*z[6];
    z[42]= - static_cast<T>(1)+ z[41];
    z[42]=z[42]*z[9];
    z[42]=static_cast<T>(1)+ n<T>(3,4)*z[42];
    z[42]=z[9]*z[36]*z[42];
    z[12]=3*z[31] + z[42] + z[12];
    z[12]=z[7]*z[12];
    z[31]=z[6] - n<T>(1,2);
    z[17]=z[31]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[6]*z[17];
    z[42]=n<T>(11,4) + z[33];
    z[42]=z[6]*z[42];
    z[42]= - n<T>(1,2) + z[42];
    z[42]=z[42]*z[16];
    z[17]=z[17] + z[42];
    z[17]=z[9]*z[17];
    z[14]= - z[14]*z[18];
    z[14]=n<T>(3,2) + z[14];
    z[14]=z[14]*z[16];
    z[14]=z[14] - n<T>(5,2) - z[30];
    z[14]=z[9]*z[14];
    z[16]= - static_cast<T>(5)- z[24];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=n<T>(1,2)*z[14] - z[19];
    z[14]=z[2]*z[14];
    z[16]=z[11] - 1;
    z[19]=z[16]*z[6];
    z[14]=z[14] + z[19] + z[17];
    z[14]=z[2]*z[14];
    z[17]=z[22] + n<T>(5,2);
    z[24]= - z[17]*z[18];
    z[30]=n<T>(1,4) - z[6];
    z[30]=z[6]*z[30];
    z[30]= - n<T>(3,2) + z[30];
    z[30]=z[30]*z[26];
    z[24]=z[24] + z[30];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(9,2)*z[18] + z[24];
    z[30]=z[9]*z[39];
    z[30]=3*z[36] + z[30];
    z[30]=z[9]*z[30];
    z[36]=n<T>(1,2)*z[3];
    z[39]= - z[40]*z[36];
    z[30]=z[30] + z[39];
    z[30]=z[30]*z[36];
    z[12]=z[12] + z[30] + n<T>(1,2)*z[24] + z[14];
    z[12]=z[7]*z[12];
    z[14]= - z[2] - z[31];
    z[14]=z[2]*z[14];
    z[14]=z[14] + z[32];
    z[14]=z[2]*z[14];
    z[14]=z[14] + z[23];
    z[14]=z[3]*z[14];
    z[24]=n<T>(3,4) + z[6];
    z[24]=z[2]*z[24];
    z[14]=z[14] - n<T>(1,2)*z[16] + z[24];
    z[14]=z[3]*z[14];
    z[16]=z[34]*z[3];
    z[17]=z[16] - z[17];
    z[17]=z[3]*z[17];
    z[17]=z[17] + n<T>(5,4) + z[22];
    z[17]=z[3]*z[17];
    z[24]=n<T>(1,2)*z[4];
    z[30]=z[2]*z[6];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[24];
    z[17]=z[30] - z[6] + z[17];
    z[17]=z[17]*z[24];
    z[24]=n<T>(1,2)*z[2];
    z[30]=n<T>(3,2) + z[2];
    z[30]=z[30]*z[24];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(3,8) + z[30];
    z[30]=z[3]*z[30];
    z[31]= - n<T>(7,2) + z[3];
    z[31]=z[3]*z[31];
    z[31]=static_cast<T>(1)+ n<T>(1,4)*z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,4)*z[4] - n<T>(1,2) + z[31];
    z[31]=z[4]*z[31];
    z[36]= - n<T>(3,2) - z[3];
    z[36]=z[36]*npow(z[3],2);
    z[36]=n<T>(1,2) + z[36];
    z[36]=z[5]*z[36];
    z[30]=n<T>(1,4)*z[36] + z[31] - n<T>(1,4) + z[30];
    z[30]=z[1]*z[30];
    z[16]= - n<T>(3,2) - z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(1,2)*z[23] + z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(1,4) + z[16];
    z[16]=z[5]*z[16];
    z[23]= - z[7]*z[2]*z[11];
    z[14]=z[30] + n<T>(1,2)*z[16] + z[23] + z[17] + z[24] + z[14];
    z[14]=z[1]*z[14];
    z[16]=z[34]*z[18]*z[25];
    z[16]=z[27] + z[16];
    z[16]=z[9]*z[16];
    z[16]=z[11] + z[16];
    z[16]=z[5]*z[16];
    z[17]= - z[6] - z[26];
    z[17]=z[17]*z[25];
    z[16]=z[17] + z[16];
    z[16]=z[8]*z[16];
    z[17]= - static_cast<T>(1)- z[27];
    z[17]=z[17]*z[38];
    z[23]=static_cast<T>(1)- z[35];
    z[23]=z[6]*z[23];
    z[17]=z[17] + n<T>(1,2) + z[23];
    z[17]=z[9]*z[17];
    z[23]= - static_cast<T>(1)- z[41];
    z[23]=z[23]*z[38];
    z[26]=z[22] - 1;
    z[26]=n<T>(1,2)*z[26];
    z[23]= - z[26] + z[23];
    z[23]=z[9]*z[23];
    z[23]= - z[2] + static_cast<T>(1)+ z[23];
    z[23]=z[2]*z[23];
    z[16]=z[16] + z[23] - z[26] + z[17];
    z[17]=2*z[6];
    z[23]=static_cast<T>(3)+ z[17];
    z[23]=z[23]*z[22];
    z[22]=z[34]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[9]*z[22];
    z[22]=z[22] + static_cast<T>(4)+ z[23];
    z[22]=z[9]*z[22];
    z[23]= - static_cast<T>(5)+ z[6];
    z[23]=z[23]*z[11];
    z[26]=static_cast<T>(2)+ z[25];
    z[26]=z[9]*z[26];
    z[24]=z[24] - n<T>(5,4) + z[6];
    z[24]=z[2]*z[24];
    z[23]=z[24] + z[26] + static_cast<T>(1)+ z[23];
    z[23]=z[2]*z[23];
    z[24]=static_cast<T>(1)+ z[17];
    z[17]=n<T>(3,4) + z[17];
    z[17]=z[9]*z[17];
    z[17]=3*z[24] + z[17];
    z[17]=z[9]*z[17];
    z[24]=static_cast<T>(2)- n<T>(5,4)*z[6];
    z[24]=z[6]*z[24];
    z[17]=z[23] + z[17] + n<T>(7,4) + z[24];
    z[17]=z[2]*z[17];
    z[15]=z[6]*z[15];
    z[15]=z[17] + z[22] + n<T>(5,2) + z[15];
    z[15]=z[3]*z[15];
    z[17]= - z[11] - z[38];
    z[17]=z[17]*z[25];
    z[22]=z[6] - n<T>(3,2);
    z[23]= - z[6]*z[22];
    z[24]=static_cast<T>(1)+ z[25];
    z[24]=z[9]*z[24];
    z[24]=n<T>(1,4)*z[24] - n<T>(1,4) - z[6];
    z[24]=z[2]*z[24];
    z[17]=z[24] + z[17] + n<T>(1,2) + z[23];
    z[17]=z[2]*z[17];
    z[23]= - static_cast<T>(2)- n<T>(9,4)*z[6];
    z[23]=z[6]*z[23];
    z[23]= - n<T>(3,8) + z[23];
    z[23]=z[9]*z[23];
    z[23]=z[23] - n<T>(3,2) - 5*z[28];
    z[23]=z[9]*z[23];
    z[24]= - static_cast<T>(5)+ 7*z[6];
    z[24]=z[6]*z[24];
    z[24]= - n<T>(1,2) + z[24];
    z[15]=z[15] + z[17] + n<T>(1,4)*z[24] + z[23];
    z[15]=z[3]*z[15];
    z[17]= - z[22]*z[18];
    z[22]=static_cast<T>(9)+ z[37];
    z[22]=z[6]*z[22];
    z[22]=static_cast<T>(3)+ z[22];
    z[22]=z[6]*z[22];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[25];
    z[17]=z[22] - n<T>(5,2) + z[17];
    z[17]=z[17]*z[25];
    z[17]=z[17] - static_cast<T>(1)- z[21];
    z[21]= - static_cast<T>(3)+ z[18];
    z[11]=z[21]*z[11];
    z[18]=z[32]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[6]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[18]*z[25];
    z[11]=z[18] - static_cast<T>(1)+ z[11];
    z[11]=z[9]*z[11];
    z[18]= - n<T>(3,2) + z[19];
    z[11]=n<T>(1,2)*z[18] + z[11];
    z[11]=n<T>(3,2)*z[11] - z[29];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,2)*z[17] + z[11];
    z[11]=z[3]*z[11];
    z[17]=n<T>(1,2) - z[33];
    z[17]=z[6]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[38];
    z[18]= - static_cast<T>(1)- z[20];
    z[18]=z[6]*z[18];
    z[17]=z[17] - n<T>(1,2) + z[18];
    z[17]=z[9]*z[17];
    z[10]= - static_cast<T>(1)- z[10];
    z[10]=n<T>(1,2)*z[10] + z[17];
    z[10]=n<T>(1,4)*z[10] + z[11];
    z[10]=z[5]*z[10];

    r += z[10] + z[12] + z[13] + z[14] + z[15] + n<T>(1,4)*z[16];
 
    return r;
}

template double qqb_2lha_r1875(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1875(const std::array<dd_real,30>&);
#endif
