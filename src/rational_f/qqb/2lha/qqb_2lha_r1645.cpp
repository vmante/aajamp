#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1645(const std::array<T,30>& k) {
  T z[52];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[6];
    z[9]=k[5];
    z[10]=n<T>(1,4)*z[9];
    z[11]=static_cast<T>(83)+ z[9];
    z[11]=z[9]*z[11];
    z[11]=static_cast<T>(339)+ z[11];
    z[11]=z[11]*z[10];
    z[12]=n<T>(1,2)*z[6];
    z[13]= - static_cast<T>(67)- n<T>(113,4)*z[9];
    z[13]=z[9]*z[13];
    z[13]= - n<T>(75,2) + z[13];
    z[13]=z[13]*z[12];
    z[14]=45*z[1];
    z[11]=z[13] + z[11] + static_cast<T>(109)+ z[14];
    z[11]=z[6]*z[11];
    z[13]=z[1] + 3;
    z[15]=15*z[1];
    z[16]= - z[13]*z[15];
    z[16]= - n<T>(97,2) + z[16];
    z[17]=n<T>(3,2)*z[9];
    z[18]= - static_cast<T>(23)- z[17];
    z[18]=z[18]*z[10];
    z[18]= - static_cast<T>(33)+ z[18];
    z[18]=z[9]*z[18];
    z[11]=z[11] + n<T>(3,2)*z[16] + z[18];
    z[16]=n<T>(101,2) + 23*z[9];
    z[16]=z[16]*z[10];
    z[18]=z[10]*z[6];
    z[19]=n<T>(1,2) + z[9];
    z[19]=z[19]*z[18];
    z[20]= - static_cast<T>(3)- n<T>(19,8)*z[9];
    z[20]=z[9]*z[20];
    z[19]=z[19] - n<T>(5,8) + z[20];
    z[19]=z[6]*z[19];
    z[16]=z[19] + z[16] + static_cast<T>(8)+ n<T>(5,4)*z[1];
    z[16]=z[6]*z[16];
    z[19]=n<T>(1,2)*z[1];
    z[20]=n<T>(5,2)*z[1];
    z[21]= - static_cast<T>(17)- z[20];
    z[21]=z[21]*z[19];
    z[22]=n<T>(1,2)*z[9];
    z[23]=n<T>(19,2)*z[9];
    z[24]= - static_cast<T>(31)- z[23];
    z[24]=z[24]*z[22];
    z[16]=z[16] + z[24] - static_cast<T>(18)+ z[21];
    z[16]=z[6]*z[16];
    z[21]=z[9] + n<T>(9,2);
    z[24]=z[21]*z[10];
    z[25]=n<T>(1,4)*z[1];
    z[26]=z[19] + 3;
    z[26]=z[26]*z[1];
    z[27]=static_cast<T>(7)+ z[26];
    z[27]=z[27]*z[25];
    z[27]=z[24] + static_cast<T>(2)+ z[27];
    z[16]=5*z[27] + z[16];
    z[16]=z[6]*z[16];
    z[27]= - n<T>(15,2) - z[26];
    z[27]=z[27]*z[19];
    z[27]= - static_cast<T>(5)+ z[27];
    z[27]=z[1]*z[27];
    z[28]=z[22] + 3;
    z[29]= - z[28]*z[22];
    z[27]=z[29] - n<T>(15,4) + z[27];
    z[16]=n<T>(1,4)*z[27] + z[16];
    z[16]=z[5]*z[16];
    z[27]=static_cast<T>(221)+ n<T>(147,2)*z[9];
    z[27]=z[27]*z[10];
    z[29]=npow(z[9],2);
    z[30]=z[29]*z[6];
    z[31]=z[30] + z[29];
    z[32]=n<T>(1,8)*z[6];
    z[33]=z[31]*z[32];
    z[34]= - static_cast<T>(5)- n<T>(23,8)*z[9];
    z[34]=z[9]*z[34];
    z[34]= - n<T>(15,8) + z[34];
    z[33]=3*z[34] + z[33];
    z[33]=z[6]*z[33];
    z[27]=z[33] + z[27] + n<T>(103,2) + z[15];
    z[27]=z[6]*z[27];
    z[33]= - static_cast<T>(103)- n<T>(105,4)*z[1];
    z[33]=z[1]*z[33];
    z[33]= - static_cast<T>(147)+ z[33];
    z[34]= - static_cast<T>(45)- n<T>(79,8)*z[9];
    z[34]=z[9]*z[34];
    z[27]=z[27] + n<T>(1,2)*z[33] + z[34];
    z[27]=z[6]*z[27];
    z[33]=n<T>(3,4)*z[9];
    z[34]=static_cast<T>(5)+ z[33];
    z[34]=z[34]*z[22];
    z[35]=n<T>(35,8) + z[1];
    z[35]=z[1]*z[35];
    z[35]=n<T>(15,2) + z[35];
    z[35]=z[1]*z[35];
    z[34]=z[34] + n<T>(25,4) + z[35];
    z[27]=3*z[34] + z[27];
    z[16]=n<T>(1,2)*z[27] + z[16];
    z[16]=z[5]*z[16];
    z[11]=n<T>(1,4)*z[11] + z[16];
    z[11]=z[5]*z[11];
    z[16]=n<T>(3,2)*z[1];
    z[27]=static_cast<T>(5)+ z[16];
    z[34]=n<T>(11,4) + z[9];
    z[34]=z[9]*z[34];
    z[27]=n<T>(1,2)*z[27] + z[34];
    z[34]=3*z[9];
    z[35]=n<T>(3,4) + z[9];
    z[35]=z[35]*z[34];
    z[35]=z[35] - n<T>(5,16)*z[30];
    z[35]=z[6]*z[35];
    z[36]= - static_cast<T>(31)- n<T>(37,2)*z[9];
    z[36]=z[9]*z[36];
    z[36]= - n<T>(25,2) + z[36];
    z[35]=n<T>(3,8)*z[36] + z[35];
    z[35]=z[6]*z[35];
    z[27]=5*z[27] + z[35];
    z[27]=z[6]*z[27];
    z[35]=z[10] + 1;
    z[35]=z[35]*z[9];
    z[25]=z[25] + 1;
    z[36]=z[25]*z[1];
    z[37]= - z[35] - n<T>(3,2) - z[36];
    z[27]=n<T>(15,4)*z[37] + z[27];
    z[27]=z[5]*z[27];
    z[37]=static_cast<T>(9)+ n<T>(5,2)*z[9];
    z[37]=z[37]*z[10];
    z[37]=z[37] + n<T>(21,8) + z[1];
    z[38]=n<T>(5,4)*z[30];
    z[39]=static_cast<T>(11)+ n<T>(45,4)*z[9];
    z[39]=z[9]*z[39];
    z[39]=z[39] - z[38];
    z[39]=z[39]*z[12];
    z[40]= - n<T>(13,2) - z[34];
    z[40]=z[9]*z[40];
    z[40]= - n<T>(55,16) + z[40];
    z[39]=3*z[40] + z[39];
    z[39]=z[6]*z[39];
    z[27]=z[27] + 5*z[37] + z[39];
    z[27]=z[5]*z[27];
    z[37]= - static_cast<T>(135)- n<T>(97,2)*z[9];
    z[37]=z[37]*z[22];
    z[39]=5*z[30];
    z[40]=31*z[9];
    z[41]=static_cast<T>(39)+ z[40];
    z[41]=z[9]*z[41];
    z[41]=z[41] - z[39];
    z[41]=z[6]*z[41];
    z[37]=z[41] - static_cast<T>(45)+ z[37];
    z[27]=n<T>(1,8)*z[37] + z[27];
    z[27]=z[5]*z[27];
    z[37]= - static_cast<T>(7)- z[34];
    z[37]=z[37]*z[10];
    z[37]= - static_cast<T>(1)+ z[37];
    z[41]=n<T>(73,2)*z[9];
    z[42]=static_cast<T>(39)+ z[41];
    z[42]=z[42]*z[22];
    z[39]=z[42] - z[39];
    z[39]=z[6]*z[39];
    z[37]=15*z[37] + z[39];
    z[39]=z[9] + 3;
    z[42]=z[9]*z[39];
    z[13]=z[42] + z[13];
    z[42]=n<T>(5,2)*z[30];
    z[43]=static_cast<T>(11)+ 13*z[9];
    z[43]=z[9]*z[43];
    z[43]=z[43] - z[42];
    z[43]=z[43]*z[12];
    z[44]= - static_cast<T>(3)- n<T>(13,8)*z[9];
    z[44]=z[9]*z[44];
    z[44]= - n<T>(11,8) + z[44];
    z[43]=5*z[44] + z[43];
    z[43]=z[6]*z[43];
    z[13]=n<T>(5,2)*z[13] + z[43];
    z[13]=z[5]*z[13];
    z[13]=n<T>(1,2)*z[37] + z[13];
    z[13]=z[5]*z[13];
    z[37]=z[33] + 1;
    z[43]=z[37]*z[34];
    z[38]=z[43] - z[38];
    z[13]=n<T>(3,2)*z[38] + z[13];
    z[13]=z[5]*z[13];
    z[38]=static_cast<T>(1)+ n<T>(7,8)*z[9];
    z[38]=z[9]*z[38];
    z[30]=z[38] - n<T>(5,8)*z[30];
    z[38]=n<T>(13,2) + 7*z[9];
    z[38]=z[9]*z[38];
    z[38]=z[38] - z[42];
    z[38]=z[6]*z[38];
    z[43]=z[22] + 1;
    z[44]= - z[9]*z[43];
    z[44]= - n<T>(1,2) + z[44];
    z[38]=n<T>(15,2)*z[44] + z[38];
    z[44]=n<T>(1,2)*z[5];
    z[38]=z[38]*z[44];
    z[30]=3*z[30] + z[38];
    z[30]=z[5]*z[30];
    z[30]= - n<T>(3,4)*z[29] + z[30];
    z[30]=z[5]*z[30];
    z[38]=z[9] + 1;
    z[45]=z[38]*z[34];
    z[45]=z[45] - z[42];
    z[45]=z[45]*z[44];
    z[45]= - z[29] + z[45];
    z[45]=z[45]*npow(z[5],2);
    z[46]=z[4]*z[29]*npow(z[5],3);
    z[45]=z[45] - n<T>(1,4)*z[46];
    z[46]=n<T>(1,2)*z[4];
    z[45]=z[45]*z[46];
    z[30]=z[30] + z[45];
    z[30]=z[4]*z[30];
    z[13]=z[30] - n<T>(3,8)*z[29] + z[13];
    z[13]=z[13]*z[46];
    z[30]=static_cast<T>(11)+ z[2];
    z[30]=z[30]*z[22];
    z[30]=static_cast<T>(9)+ z[30];
    z[30]=z[9]*z[30];
    z[30]=z[30] - z[42];
    z[13]=z[13] + n<T>(1,8)*z[30] + z[27];
    z[13]=z[4]*z[13];
    z[27]=z[9] + 5;
    z[30]=z[27]*z[22];
    z[42]=static_cast<T>(5)+ z[1];
    z[42]=z[42]*z[19];
    z[42]=static_cast<T>(5)+ z[42];
    z[42]=z[1]*z[42];
    z[42]=z[30] + static_cast<T>(5)+ z[42];
    z[45]= - z[29]*z[12];
    z[46]=static_cast<T>(7)+ 11*z[9];
    z[46]=z[9]*z[46];
    z[45]=z[46] + z[45];
    z[45]=z[6]*z[45];
    z[46]= - static_cast<T>(23)- n<T>(31,2)*z[9];
    z[46]=z[9]*z[46];
    z[46]= - n<T>(15,2) + z[46];
    z[45]=3*z[46] + z[45];
    z[45]=z[45]*z[12];
    z[40]=static_cast<T>(77)+ z[40];
    z[40]=z[9]*z[40];
    z[40]=z[45] + z[40] + static_cast<T>(61)+ z[15];
    z[45]=n<T>(1,4)*z[6];
    z[40]=z[40]*z[45];
    z[46]= - static_cast<T>(5)- n<T>(11,8)*z[9];
    z[46]=z[46]*z[22];
    z[47]= - static_cast<T>(2)- n<T>(7,16)*z[1];
    z[47]=z[1]*z[47];
    z[46]=z[46] - n<T>(27,8) + z[47];
    z[40]=5*z[46] + z[40];
    z[40]=z[6]*z[40];
    z[40]=n<T>(3,4)*z[42] + z[40];
    z[40]=z[5]*z[40];
    z[41]=static_cast<T>(121)+ z[41];
    z[41]=z[9]*z[41];
    z[14]=z[41] + n<T>(259,2) + z[14];
    z[41]=z[6]*z[9];
    z[42]=static_cast<T>(3)+ n<T>(7,2)*z[9];
    z[42]=z[42]*z[41];
    z[46]= - static_cast<T>(20)- n<T>(81,8)*z[9];
    z[46]=z[9]*z[46];
    z[42]=n<T>(3,4)*z[42] - n<T>(75,8) + z[46];
    z[42]=z[6]*z[42];
    z[14]=n<T>(1,4)*z[14] + z[42];
    z[14]=z[6]*z[14];
    z[42]=z[1] + n<T>(7,2);
    z[46]= - z[1]*z[42];
    z[30]= - z[30] - n<T>(9,2) + z[46];
    z[14]=z[40] + n<T>(15,4)*z[30] + z[14];
    z[14]=z[5]*z[14];
    z[30]=n<T>(1,8)*z[9];
    z[40]=static_cast<T>(11)+ z[30];
    z[40]=z[9]*z[40];
    z[40]=n<T>(97,2) + z[40];
    z[40]=z[40]*z[22];
    z[46]=z[22]*z[6];
    z[23]=static_cast<T>(11)+ z[23];
    z[23]=z[23]*z[46];
    z[47]= - static_cast<T>(33)- n<T>(203,16)*z[9];
    z[47]=z[9]*z[47];
    z[23]=z[23] - n<T>(165,8) + z[47];
    z[23]=z[6]*z[23];
    z[47]=n<T>(9,4) + z[1];
    z[23]=z[23] + 15*z[47] + z[40];
    z[14]=n<T>(1,2)*z[23] + z[14];
    z[14]=z[5]*z[14];
    z[23]= - n<T>(31,2) - 3*z[2];
    z[23]=z[23]*z[22];
    z[40]= - static_cast<T>(9)- z[2];
    z[23]=3*z[40] + z[23];
    z[23]=z[9]*z[23];
    z[40]=static_cast<T>(13)+ n<T>(17,2)*z[9];
    z[40]=z[40]*z[41];
    z[23]=z[40] - n<T>(45,2) + z[23];
    z[13]=z[13] + n<T>(1,8)*z[23] + z[14];
    z[13]=z[4]*z[13];
    z[14]=z[2] + 3;
    z[23]=7*z[14] + z[9];
    z[23]=z[23]*z[22];
    z[40]=15*z[2];
    z[23]=z[23] + static_cast<T>(59)+ z[40];
    z[23]=z[23]*z[10];
    z[47]=static_cast<T>(7)+ z[2];
    z[47]=n<T>(1,4)*z[47] + z[1];
    z[23]=15*z[47] + z[23];
    z[47]= - static_cast<T>(1)- n<T>(5,16)*z[9];
    z[47]=z[9]*z[47];
    z[47]= - n<T>(55,8) + 9*z[47];
    z[47]=z[6]*z[47];
    z[23]=n<T>(1,2)*z[23] + z[47];
    z[11]=z[13] + n<T>(1,2)*z[23] + z[11];
    z[11]=z[4]*z[11];
    z[13]=9*z[1];
    z[23]=n<T>(15,4) + z[1];
    z[23]=z[23]*z[13];
    z[23]=n<T>(97,2) + z[23];
    z[23]=z[1]*z[23];
    z[47]=z[28]*z[9];
    z[48]=static_cast<T>(15)+ z[47];
    z[48]=z[48]*z[33];
    z[23]=z[48] + static_cast<T>(33)+ z[23];
    z[48]=static_cast<T>(41)+ z[22];
    z[48]=z[9]*z[48];
    z[48]=static_cast<T>(153)+ z[48];
    z[48]=z[48]*z[30];
    z[31]=z[31]*z[6];
    z[49]= - n<T>(1,2)*z[29] - z[31];
    z[49]=z[49]*z[32];
    z[20]=static_cast<T>(7)+ z[20];
    z[20]=z[49] + 3*z[20] + z[48];
    z[20]=z[6]*z[20];
    z[48]= - static_cast<T>(13)- z[9];
    z[48]=z[48]*z[10];
    z[48]= - static_cast<T>(17)+ z[48];
    z[48]=z[48]*z[17];
    z[49]= - static_cast<T>(43)- n<T>(105,8)*z[1];
    z[49]=z[1]*z[49];
    z[20]=z[20] + z[48] - static_cast<T>(51)+ z[49];
    z[20]=z[6]*z[20];
    z[20]=n<T>(1,2)*z[23] + z[20];
    z[23]= - z[38]*z[17];
    z[48]= - static_cast<T>(1)- z[17];
    z[48]=z[48]*z[41];
    z[23]=z[23] + z[48];
    z[23]=z[23]*z[12];
    z[48]=static_cast<T>(73)+ n<T>(55,2)*z[9];
    z[48]=z[48]*z[22];
    z[49]=5*z[1];
    z[50]=z[49] + n<T>(51,2);
    z[23]=z[23] + z[48] + z[50];
    z[23]=z[23]*z[12];
    z[48]= - static_cast<T>(283)- 69*z[9];
    z[48]=z[48]*z[30];
    z[51]= - static_cast<T>(27)- z[49];
    z[51]=z[1]*z[51];
    z[23]=z[23] + z[48] - n<T>(195,4) + z[51];
    z[23]=z[6]*z[23];
    z[48]=z[50]*z[19];
    z[48]=static_cast<T>(25)+ z[48];
    z[48]=z[1]*z[48];
    z[47]=n<T>(7,2)*z[47] + n<T>(47,2) + z[48];
    z[23]=n<T>(3,2)*z[47] + z[23];
    z[23]=z[6]*z[23];
    z[47]= - static_cast<T>(9)- z[9];
    z[47]=z[47]*z[10];
    z[48]= - n<T>(21,4) - z[1];
    z[48]=z[1]*z[48];
    z[48]= - n<T>(45,4) + z[48];
    z[48]=z[1]*z[48];
    z[48]= - n<T>(25,2) + z[48];
    z[48]=z[1]*z[48];
    z[47]=z[47] - n<T>(15,2) + z[48];
    z[23]=n<T>(1,2)*z[47] + z[23];
    z[47]= - n<T>(23,8) - z[9];
    z[47]=z[9]*z[47];
    z[42]= - n<T>(3,4)*z[42] + z[47];
    z[38]=z[38]*z[41];
    z[47]=n<T>(15,4) + 2*z[9];
    z[47]=z[9]*z[47];
    z[47]= - n<T>(3,8)*z[38] + n<T>(3,2) + z[47];
    z[47]=z[6]*z[47];
    z[42]=3*z[42] + z[47];
    z[42]=z[6]*z[42];
    z[47]=static_cast<T>(2)+ z[19];
    z[47]=z[1]*z[47];
    z[48]=static_cast<T>(2)+ z[22];
    z[48]=z[9]*z[48];
    z[47]=z[48] + static_cast<T>(3)+ z[47];
    z[42]=3*z[47] + z[42];
    z[42]=z[6]*z[42];
    z[47]=static_cast<T>(1)+ n<T>(3,16)*z[9];
    z[47]=z[47]*z[9];
    z[48]= - static_cast<T>(1)- n<T>(1,8)*z[1];
    z[48]=z[1]*z[48];
    z[48]= - n<T>(25,8) + z[48];
    z[48]=z[1]*z[48];
    z[48]= - static_cast<T>(5)+ z[48];
    z[48]=z[1]*z[48];
    z[48]= - n<T>(35,8) + z[48];
    z[42]=z[42] + n<T>(1,2)*z[48] - z[47];
    z[42]=z[5]*z[6]*z[42];
    z[23]=n<T>(1,2)*z[23] + z[42];
    z[23]=z[5]*z[23];
    z[20]=n<T>(1,2)*z[20] + z[23];
    z[20]=z[5]*z[20];
    z[23]=n<T>(1,2)*z[8];
    z[29]= - z[29]*z[23];
    z[31]= - z[8]*z[31];
    z[29]=z[29] + z[31];
    z[31]=npow(z[6],2);
    z[29]=z[29]*z[31];
    z[42]=n<T>(29,2) + z[9];
    z[42]=z[9]*z[42];
    z[42]=static_cast<T>(75)+ z[42];
    z[42]=z[42]*z[10];
    z[15]=n<T>(1,4)*z[29] + z[42] + n<T>(59,2) + z[15];
    z[15]=z[6]*z[15];
    z[29]= - n<T>(9,4)*z[1] - static_cast<T>(5)- z[2];
    z[29]=z[29]*z[49];
    z[42]=static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[48]= - z[42]*z[22];
    z[48]=z[48] - n<T>(11,4) - z[2];
    z[48]=z[48]*z[22];
    z[42]= - 7*z[42] + z[48];
    z[42]=z[9]*z[42];
    z[48]= - n<T>(27,2) - 5*z[2];
    z[15]=z[15] + z[42] + n<T>(3,2)*z[48] + z[29];
    z[11]=z[11] + n<T>(1,4)*z[15] + z[20];
    z[11]=z[4]*z[11];
    z[15]=z[39]*z[22];
    z[20]=z[9] + n<T>(3,2);
    z[29]=z[9]*z[20];
    z[29]=z[29] + z[38];
    z[29]=z[6]*z[29];
    z[29]=z[15] + z[29];
    z[29]=z[29]*z[32];
    z[32]= - z[25]*z[49];
    z[38]= - static_cast<T>(11)- z[9];
    z[38]=z[38]*z[17];
    z[38]= - static_cast<T>(79)+ z[38];
    z[38]=z[9]*z[38];
    z[29]=z[29] + n<T>(1,16)*z[38] - n<T>(121,16) + z[32];
    z[29]=z[6]*z[29];
    z[32]=3*z[1];
    z[38]=static_cast<T>(21)+ z[49];
    z[38]=z[38]*z[32];
    z[38]=n<T>(205,2) + z[38];
    z[38]=z[1]*z[38];
    z[39]=z[27]*z[9];
    z[42]=static_cast<T>(21)+ z[39];
    z[42]=z[42]*z[17];
    z[38]=z[42] + n<T>(159,2) + z[38];
    z[29]=n<T>(1,8)*z[38] + z[29];
    z[29]=z[6]*z[29];
    z[17]=z[17] + 7;
    z[17]=z[17]*z[22];
    z[38]=z[19] + 1;
    z[38]=z[38]*z[1];
    z[42]= - n<T>(5,2) + z[38];
    z[42]=z[42]*z[19];
    z[42]= - static_cast<T>(5)+ z[42];
    z[42]=z[1]*z[42];
    z[42]= - z[17] - n<T>(25,4) + z[42];
    z[48]=n<T>(7,2) + z[9];
    z[48]=z[48]*z[10];
    z[48]=z[48] + static_cast<T>(1)+ n<T>(3,8)*z[1];
    z[18]=z[20]*z[18];
    z[20]= - static_cast<T>(2)- n<T>(13,16)*z[9];
    z[20]=z[9]*z[20];
    z[18]=z[18] - n<T>(9,8) + z[20];
    z[18]=z[6]*z[18];
    z[18]=3*z[48] + z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(1,4)*z[42] + z[18];
    z[18]=z[5]*z[18]*z[31];
    z[20]=z[43]*z[34];
    z[20]= - static_cast<T>(1)+ z[20];
    z[37]=z[37]*z[41];
    z[20]=n<T>(1,2)*z[20] + z[37];
    z[20]=z[6]*z[20];
    z[13]= - static_cast<T>(35)- z[13];
    z[37]= - static_cast<T>(19)- n<T>(21,4)*z[9];
    z[37]=z[9]*z[37];
    z[13]=z[20] + n<T>(1,2)*z[13] + z[37];
    z[13]=z[13]*z[45];
    z[20]=static_cast<T>(51)+ 13*z[1];
    z[20]=z[1]*z[20];
    z[20]=n<T>(141,2) + z[20];
    z[13]=z[13] + n<T>(1,8)*z[20] + 5*z[47];
    z[13]=z[6]*z[13];
    z[20]= - static_cast<T>(5)- n<T>(3,4)*z[1];
    z[20]=z[1]*z[20];
    z[20]= - static_cast<T>(13)+ z[20];
    z[20]=z[1]*z[20];
    z[20]= - static_cast<T>(17)+ z[20];
    z[20]=z[1]*z[20];
    z[20]= - n<T>(47,4) + z[20];
    z[37]= - static_cast<T>(1)- z[30];
    z[37]=z[9]*z[37];
    z[13]=z[13] + n<T>(1,4)*z[20] + z[37];
    z[13]=z[6]*z[13];
    z[13]=z[13] + z[18];
    z[13]=z[5]*z[13];
    z[18]= - n<T>(9,2) - z[1];
    z[18]=z[18]*z[32];
    z[18]= - n<T>(97,4) + z[18];
    z[18]=z[18]*z[19];
    z[18]= - static_cast<T>(11)+ z[18];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(23,4) + z[18];
    z[20]= - static_cast<T>(1)- n<T>(1,16)*z[39];
    z[20]=z[9]*z[20];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[13]=z[13] + n<T>(1,2)*z[18] + z[29];
    z[13]=z[5]*z[13];
    z[18]=3*z[8];
    z[20]=z[9]*z[8];
    z[29]=z[18] + z[20];
    z[29]=z[29]*z[22];
    z[37]=z[8] + z[20];
    z[37]=z[37]*z[41];
    z[20]=n<T>(3,2)*z[8] + z[20];
    z[20]=z[9]*z[20];
    z[20]=z[20] + z[37];
    z[20]=z[6]*z[20];
    z[20]=z[29] + z[20];
    z[20]=z[6]*z[20];
    z[37]=z[8]*z[10];
    z[20]=z[37] + z[20];
    z[20]=z[20]*z[12];
    z[27]= - z[27]*z[34];
    z[27]= - static_cast<T>(73)+ z[27];
    z[27]=z[27]*z[30];
    z[34]= - static_cast<T>(23)- n<T>(35,4)*z[1];
    z[34]=z[1]*z[34];
    z[20]=z[20] + z[27] - n<T>(45,2) + z[34];
    z[20]=z[6]*z[20];
    z[14]=n<T>(5,2)*z[14] + z[32];
    z[14]=z[14]*z[16];
    z[14]=z[14] + static_cast<T>(11)+ n<T>(15,2)*z[2];
    z[14]=z[1]*z[14];
    z[16]=z[7]*z[43];
    z[27]=z[2] + 1;
    z[16]=n<T>(3,2)*z[27] + z[16];
    z[16]=z[16]*z[22];
    z[32]=n<T>(5,4) + z[2];
    z[16]=z[16] + 3*z[32] + n<T>(1,4)*z[7];
    z[16]=z[16]*z[22];
    z[14]=z[20] + z[16] + n<T>(21,4)*z[27] + z[14];
    z[11]=z[11] + n<T>(1,4)*z[14] + z[13];
    z[11]=z[4]*z[11];
    z[13]=3*z[7];
    z[14]=z[19]*z[7];
    z[16]=z[13] + z[14];
    z[16]=z[16]*z[1];
    z[20]=z[16] + static_cast<T>(1)+ n<T>(31,4)*z[7];
    z[20]=z[1]*z[20];
    z[32]=static_cast<T>(9)+ 17*z[7];
    z[20]=n<T>(1,2)*z[32] + z[20];
    z[20]=z[1]*z[20];
    z[32]=z[3] - 3;
    z[32]= - z[32]*z[35];
    z[34]=static_cast<T>(37)- 5*z[3];
    z[32]=n<T>(1,4)*z[34] + z[32];
    z[32]=z[9]*z[32];
    z[34]= - z[3] + static_cast<T>(21)+ n<T>(13,2)*z[7];
    z[20]=z[32] + n<T>(1,2)*z[34] + z[20];
    z[32]=z[43]*z[41];
    z[32]=z[32] - 1;
    z[34]=z[15] + z[32];
    z[34]=z[34]*z[12];
    z[28]= - z[28]*z[10];
    z[28]= - z[34] + static_cast<T>(1)+ z[28];
    z[28]=z[6]*z[28];
    z[20]=n<T>(1,2)*z[20] + z[28];
    z[20]=z[20]*z[45];
    z[28]= - static_cast<T>(19)- z[23];
    z[37]= - static_cast<T>(1)- n<T>(3,16)*z[1];
    z[37]=z[1]*z[37];
    z[37]= - n<T>(69,32) + z[37];
    z[37]=z[1]*z[37];
    z[28]=n<T>(1,8)*z[28] + z[37];
    z[28]=z[1]*z[28];
    z[24]= - static_cast<T>(3)- z[24];
    z[24]=z[24]*z[10];
    z[20]=z[20] + z[24] - n<T>(13,8) + z[28];
    z[20]=z[6]*z[20];
    z[16]= - n<T>(9,2)*z[7] - z[16];
    z[16]=z[16]*z[19];
    z[16]=z[16] + n<T>(1,2) - z[7];
    z[16]=z[1]*z[16];
    z[16]= - z[34] + z[17] + n<T>(13,4) + z[16];
    z[16]=z[16]*z[12];
    z[17]=z[9] + 7;
    z[24]= - z[17]*z[30];
    z[28]=static_cast<T>(1)+ z[36];
    z[28]=z[1]*z[28];
    z[28]= - n<T>(5,8) + z[28];
    z[28]=z[1]*z[28];
    z[16]=z[16] + z[24] - n<T>(13,8) + z[28];
    z[16]=z[16]*z[31];
    z[24]= - z[43]*z[46];
    z[14]=z[7] + z[14];
    z[14]=z[1]*z[14];
    z[14]=n<T>(1,2)*z[7] + z[14];
    z[14]=z[14]*npow(z[1],2);
    z[14]=z[24] + z[15] + static_cast<T>(1)+ n<T>(1,2)*z[14];
    z[14]=z[6]*z[14];
    z[15]= - n<T>(1,2) - z[38];
    z[15]=z[1]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(5,2) + z[15];
    z[14]=z[14] + n<T>(1,2)*z[15] - z[35];
    z[14]=z[14]*npow(z[6],3)*z[44];
    z[14]=z[16] + z[14];
    z[14]=z[14]*z[44];
    z[15]=static_cast<T>(1)- z[8];
    z[15]=z[1]*z[15];
    z[14]=z[14] + n<T>(1,32)*z[15] + z[20];
    z[14]=z[5]*z[14];
    z[15]= - z[8]*z[32];
    z[15]= - z[29] + z[15];
    z[12]=z[15]*z[12];
    z[15]= - z[8]*z[22];
    z[15]= - z[18] + z[15];
    z[15]=z[15]*z[10];
    z[12]=z[12] + z[8] + z[15];
    z[12]=z[6]*z[12];
    z[15]=n<T>(31,4) + z[26];
    z[16]=z[3]*z[7];
    z[15]=z[1]*z[16]*z[15];
    z[15]=n<T>(45,4)*z[16] + z[15];
    z[15]=z[15]*z[19];
    z[17]=z[22]*z[17];
    z[17]=z[17] + 11;
    z[17]=z[16]*z[17];
    z[17]= - z[23] + z[17];
    z[10]=z[17]*z[10];
    z[16]=n<T>(1,4)*z[8] + z[16];
    z[10]=z[12] + z[10] + 5*z[16] + z[15];
    z[10]=z[6]*z[10];
    z[12]=static_cast<T>(11)- n<T>(5,2)*z[3];
    z[15]= - z[3]*z[25];
    z[15]=n<T>(5,2) + z[15];
    z[15]=z[1]*z[15];
    z[12]=n<T>(3,4)*z[12] + z[15];
    z[12]=z[1]*z[12];
    z[15]=static_cast<T>(3)- n<T>(1,2)*z[3];
    z[12]=n<T>(15,4)*z[15] + z[12];
    z[12]=z[1]*z[12];
    z[15]=z[22]*z[7]*z[21];
    z[13]=z[15] - n<T>(1,4)*z[3] + static_cast<T>(5)+ z[13];
    z[13]=z[13]*z[22];
    z[15]= - 7*z[3] + 5*z[7] + static_cast<T>(59)+ z[18];
    z[10]=z[10] + z[13] + n<T>(1,8)*z[15] + z[12];
    z[10]=z[6]*z[10];
    z[12]=z[2] - 1;
    z[12]=z[12]*z[3];
    z[13]=n<T>(1,2)*z[12] - z[27];
    z[15]= - static_cast<T>(3)+ z[12];
    z[15]=z[15]*z[19];
    z[13]=3*z[13] + z[15];
    z[13]=z[1]*z[13];
    z[15]=n<T>(7,2)*z[12] - n<T>(7,2) - z[40];
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[1]*z[13];
    z[13]=z[13] - 7*z[2] + z[12];
    z[13]=z[1]*z[13];
    z[15]= - z[33] - 3;
    z[15]=z[2]*z[15];
    z[12]=z[13] + n<T>(1,4)*z[12] - n<T>(1,4) + z[15];
    z[10]=n<T>(1,2)*z[12] + z[10];

    r += n<T>(1,4)*z[10] + z[11] + z[14];
 
    return r;
}

template double qqb_2lha_r1645(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1645(const std::array<dd_real,30>&);
#endif
