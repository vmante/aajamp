#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r894(const std::array<T,30>& k) {
  T z[47];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[29];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[6];
    z[9]=k[5];
    z[10]=k[10];
    z[11]=k[24];
    z[12]=n<T>(3,2)*z[4];
    z[13]=z[7]*z[4];
    z[14]=z[12] + z[13];
    z[15]=n<T>(1,4)*z[13];
    z[16]=z[15] + z[4];
    z[17]=z[16]*z[6];
    z[18]=n<T>(1,4)*z[14] + z[17];
    z[18]=z[8]*z[18];
    z[19]=z[13] + n<T>(11,8)*z[4];
    z[18]=z[18] - z[17] - z[19];
    z[18]=z[6]*z[18];
    z[20]=n<T>(1,2)*z[3];
    z[21]=z[20]*z[13];
    z[22]=5*z[13];
    z[23]=z[21] + n<T>(33,4)*z[4] + z[22];
    z[23]=z[3]*z[23];
    z[24]=n<T>(5,2)*z[13];
    z[21]= - z[21] - 9*z[4] - z[24];
    z[21]=z[3]*z[21];
    z[25]=z[3]*z[4];
    z[26]=z[25]*z[2];
    z[21]=z[21] + n<T>(7,2)*z[26];
    z[21]=z[2]*z[21];
    z[21]=z[21] - z[4] + z[23];
    z[23]=n<T>(1,2)*z[2];
    z[21]=z[21]*z[23];
    z[27]=z[5]*z[6];
    z[28]=z[27] + 1;
    z[29]=n<T>(1,2)*z[4];
    z[30]=z[29]*z[28];
    z[31]=n<T>(1,2)*z[13];
    z[32]=z[31] - z[4];
    z[33]= - z[17] + z[32];
    z[33]=z[6]*z[33];
    z[30]=z[33] + z[30];
    z[30]=z[5]*z[30];
    z[28]= - z[4]*z[28];
    z[26]=z[26] - z[25] + z[28];
    z[26]=z[1]*z[26];
    z[28]=3*z[13];
    z[33]= - n<T>(11,2)*z[4] - z[28];
    z[33]=z[33]*z[20];
    z[33]=z[33] - z[4] + z[13];
    z[18]=n<T>(1,4)*z[26] + z[30] + z[21] + n<T>(1,2)*z[33] + z[18];
    z[18]=z[1]*z[18];
    z[21]=static_cast<T>(1)+ z[4];
    z[26]= - static_cast<T>(1)- z[29];
    z[26]=z[7]*z[26];
    z[21]=n<T>(1,2)*z[21] + z[26];
    z[21]=z[9]*z[21];
    z[26]=z[9]*z[7];
    z[30]=z[31] + z[26];
    z[30]=z[9]*z[30];
    z[33]=z[3]*npow(z[26],2);
    z[30]=z[30] + z[33];
    z[30]=z[3]*z[30];
    z[21]=z[30] + static_cast<T>(1)+ z[21];
    z[30]=n<T>(1,4)*z[3];
    z[21]=z[21]*z[30];
    z[34]=npow(z[7],2);
    z[35]=z[34]*z[16]*z[9];
    z[36]=npow(z[9],2);
    z[37]= - z[7]*z[36];
    z[33]=z[37] - z[33];
    z[37]=n<T>(1,8)*z[3];
    z[33]=z[33]*z[37];
    z[33]=z[35] + z[33];
    z[33]=z[6]*z[33];
    z[35]=z[29] + 7*z[13];
    z[35]=z[7]*z[35];
    z[35]= - z[29] + z[35];
    z[38]=n<T>(1,4)*z[9];
    z[35]=z[35]*z[38];
    z[39]=n<T>(1,4)*z[4];
    z[40]= - n<T>(5,8)*z[4] + z[13];
    z[40]=z[7]*z[40];
    z[21]=z[33] + z[21] + z[35] + z[39] + z[40];
    z[21]=z[6]*z[21];
    z[33]= - z[39] - z[22];
    z[33]=z[7]*z[33];
    z[35]=5*z[4] + n<T>(9,2)*z[13];
    z[35]=z[7]*z[35];
    z[35]=n<T>(15,4)*z[4] + z[35];
    z[35]=z[35]*z[26];
    z[33]=z[35] + z[12] + z[33];
    z[35]=n<T>(1,2)*z[7];
    z[40]=static_cast<T>(1)- z[29];
    z[40]=z[40]*z[35];
    z[40]= - z[38] - static_cast<T>(1)+ z[40];
    z[40]=z[9]*z[40];
    z[41]=n<T>(1,2)*z[9];
    z[42]=z[26] - 1;
    z[43]= - 3*z[7] - z[42];
    z[43]=z[43]*z[41];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[3]*z[43];
    z[40]=z[43] - n<T>(1,4) + z[40];
    z[40]=z[40]*z[20];
    z[43]=n<T>(1,4)*z[7];
    z[44]=3*z[4];
    z[45]=static_cast<T>(5)+ z[44];
    z[45]=z[45]*z[43];
    z[46]=n<T>(1,8)*z[4];
    z[45]=z[45] + static_cast<T>(1)- z[46];
    z[45]=z[9]*z[45];
    z[19]=z[7]*z[19];
    z[19]=z[40] + z[45] - n<T>(3,4)*z[4] + z[19];
    z[19]=z[3]*z[19];
    z[18]=z[18] + z[21] + n<T>(1,2)*z[33] + z[19];
    z[19]=n<T>(5,4)*z[4];
    z[21]= - z[19] - z[28];
    z[21]=z[9]*z[21];
    z[21]=z[21] + z[44] - z[13];
    z[33]= - n<T>(7,2)*z[4] - z[13];
    z[33]=z[33]*z[35];
    z[33]=z[4] + z[33];
    z[33]=z[33]*z[41];
    z[33]=z[33] + z[16];
    z[33]=z[6]*z[33];
    z[21]=n<T>(1,4)*z[21] + z[33];
    z[21]=z[6]*z[21];
    z[33]=z[9]*z[4];
    z[35]=z[33] + z[29];
    z[17]= - z[9]*z[17];
    z[17]= - n<T>(1,2)*z[35] + z[17];
    z[17]=z[17]*z[27];
    z[17]=n<T>(1,2)*z[17] - n<T>(1,4)*z[35] + z[21];
    z[17]=z[5]*z[17];
    z[21]=n<T>(5,2)*z[4];
    z[27]=z[21] - z[13];
    z[35]= - z[4] - n<T>(5,4)*z[13];
    z[35]=z[7]*z[35];
    z[19]= - z[19] + z[35];
    z[19]=z[7]*z[19];
    z[19]= - z[46] + z[19];
    z[19]=z[9]*z[19];
    z[19]=n<T>(1,2)*z[27] + z[19];
    z[15]=z[44] - z[15];
    z[15]=z[7]*z[15];
    z[27]=n<T>(9,8)*z[4] - z[13];
    z[27]=z[7]*z[27];
    z[27]=z[46] + z[27];
    z[27]=z[9]*z[27];
    z[15]=z[27] + n<T>(7,4)*z[4] + z[15];
    z[27]= - z[4] - z[31];
    z[27]=z[27]*z[43];
    z[27]=z[4] + z[27];
    z[26]=z[27]*z[26];
    z[16]=z[16]*z[7];
    z[26]=z[16] + z[26];
    z[26]=z[6]*z[26];
    z[15]=n<T>(1,2)*z[15] + z[26];
    z[15]=z[6]*z[15];
    z[26]=z[4] + n<T>(17,8)*z[13];
    z[26]=z[7]*z[26];
    z[26]=z[39] + z[26];
    z[26]=z[9]*z[26];
    z[27]= - z[4] - n<T>(19,4)*z[13];
    z[27]=z[9]*z[27];
    z[31]=z[2]*z[33];
    z[27]=z[27] + n<T>(7,4)*z[31];
    z[27]=z[27]*z[23];
    z[26]=z[27] + z[46] + z[26];
    z[26]=z[2]*z[26];
    z[15]=z[17] + z[26] + n<T>(1,2)*z[19] + z[15];
    z[15]=z[5]*z[15];
    z[17]=z[13] - z[9];
    z[19]=z[17]*z[20];
    z[19]=z[19] - n<T>(1,2)*z[36] + z[29] + z[13];
    z[19]=z[3]*z[19];
    z[26]=z[13] + z[4];
    z[27]=z[26] - z[9];
    z[17]= - z[3]*z[17];
    z[17]=z[17] - z[27];
    z[17]=z[17]*z[20];
    z[17]=z[4] + z[17];
    z[17]=z[6]*z[17];
    z[31]=z[3]*z[13];
    z[31]=z[31] + z[4] + z[28];
    z[31]=z[3]*z[31];
    z[35]=z[2]*npow(z[3],2)*z[13];
    z[31]=z[31] + z[35];
    z[23]=z[31]*z[23];
    z[24]=z[24] + z[4];
    z[17]=z[23] + z[17] + z[19] + z[24];
    z[17]=z[10]*z[17];
    z[19]=z[5] + 1;
    z[23]=n<T>(3,2)*z[26];
    z[19]=z[23]*z[19];
    z[23]=z[27]*z[6];
    z[19]= - n<T>(3,2)*z[23] + z[19] - z[36];
    z[23]=z[42]*z[9];
    z[26]=z[23]*z[8];
    z[27]=n<T>(3,2)*z[26] + z[19];
    z[27]=z[8]*z[27];
    z[19]=z[10]*z[19];
    z[31]=z[10]*z[23];
    z[26]=z[26] + z[31];
    z[26]=z[11]*z[26];
    z[19]=n<T>(3,2)*z[26] + z[27] + z[19];
    z[19]=z[11]*z[19];
    z[17]=z[17] + z[19];
    z[19]=z[13]*z[9];
    z[26]= - z[4]*z[34];
    z[27]= - static_cast<T>(1)- z[9];
    z[27]=z[3]*z[27];
    z[12]=z[27] + z[19] - z[12] + z[26];
    z[12]=z[12]*z[30];
    z[14]= - z[7]*z[14];
    z[26]=n<T>(3,8)*z[4];
    z[14]=z[26] + z[14];
    z[27]= - static_cast<T>(7)- z[29];
    z[27]=n<T>(1,2)*z[27] - z[13];
    z[27]=z[27]*z[41];
    z[12]=z[12] + 3*z[14] + z[27];
    z[12]=z[3]*z[12];
    z[14]= - z[44] - n<T>(11,2)*z[13];
    z[14]=z[7]*z[14];
    z[14]= - z[26] + z[14];
    z[14]=z[9]*z[14];
    z[24]= - z[7]*z[24];
    z[12]=z[12] + z[14] - n<T>(23,8)*z[4] + z[24];
    z[14]=z[32]*z[38];
    z[19]=z[21] - z[19];
    z[19]=z[19]*z[37];
    z[14]=z[19] + z[14] - z[39] + 2*z[13];
    z[14]=z[3]*z[14];
    z[13]=z[4] + n<T>(13,2)*z[13];
    z[13]=z[13]*z[38];
    z[19]=z[33] - z[25];
    z[19]=z[19]*z[20];
    z[19]= - 7*z[4] + z[19];
    z[19]=z[2]*z[19];
    z[13]=n<T>(1,4)*z[19] + z[14] + z[13] + n<T>(11,4)*z[4] + z[28];
    z[13]=z[2]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[2]*z[12];
    z[13]=z[34]*z[41];
    z[13]=z[13] - z[7];
    z[13]=z[13]*z[9];
    z[13]=z[13] + n<T>(1,2);
    z[13]=z[13]*z[3];
    z[14]= - z[42]*z[41];
    z[14]=z[14] - z[13];
    z[14]=z[3]*z[14];
    z[19]=z[7] + n<T>(3,2);
    z[20]= - z[9] - z[19];
    z[20]=z[20]*z[41];
    z[14]=z[20] + z[14];
    z[14]=z[3]*z[14];
    z[20]=z[44] + z[22];
    z[14]=n<T>(1,4)*z[20] + z[14];
    z[20]= - static_cast<T>(1)- z[43];
    z[20]=z[6]*z[20]*z[41];
    z[19]=z[9]*z[19];
    z[16]=z[20] - n<T>(1,8)*z[19] - n<T>(3,16)*z[4] - z[16];
    z[16]=z[6]*z[16];
    z[13]=n<T>(3,2)*z[23] + z[13];
    z[13]=z[8]*z[13]*z[37];
    z[13]=z[13] + n<T>(1,4)*z[14] + z[16];
    z[13]=z[8]*z[13];

    r += z[12] + z[13] + z[15] + n<T>(1,8)*z[17] + n<T>(1,2)*z[18];
 
    return r;
}

template double qqb_2lha_r894(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r894(const std::array<dd_real,30>&);
#endif
