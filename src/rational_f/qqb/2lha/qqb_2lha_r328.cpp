#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r328(const std::array<T,30>& k) {
  T z[45];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[10];
    z[4]=k[17];
    z[5]=k[9];
    z[6]=k[11];
    z[7]=k[3];
    z[8]=k[8];
    z[9]=k[13];
    z[10]=k[4];
    z[11]=k[12];
    z[12]=k[15];
    z[13]=k[6];
    z[14]=k[7];
    z[15]=z[9]*z[3];
    z[16]=z[3] + 1;
    z[17]=z[15]*z[16];
    z[18]=n<T>(1,2)*z[3];
    z[19]= - n<T>(1,2) - z[3];
    z[19]=z[19]*z[18];
    z[20]=npow(z[3],2);
    z[21]=n<T>(1,2)*z[20];
    z[22]=z[20]*z[9];
    z[23]= - z[21] + z[22];
    z[24]=n<T>(1,2)*z[10];
    z[23]=z[23]*z[24];
    z[19]=z[23] + z[19] + z[17];
    z[19]=z[10]*z[19];
    z[23]=z[18] + 1;
    z[23]=z[23]*z[3];
    z[23]=z[23] + n<T>(1,2);
    z[25]=z[23]*z[9];
    z[26]=z[16]*z[3];
    z[27]=n<T>(1,4)*z[26];
    z[19]=z[19] - z[27] + z[25];
    z[19]=z[13]*z[19];
    z[28]=z[22] - z[20];
    z[29]=n<T>(3,5)*z[10];
    z[28]=z[28]*z[29]*z[9];
    z[30]=z[21] - 6*z[17];
    z[31]=n<T>(1,5)*z[9];
    z[30]=z[30]*z[31];
    z[32]=n<T>(1,4)*z[20];
    z[30]= - z[28] + z[32] + z[30];
    z[30]=z[10]*z[30];
    z[33]= - n<T>(11,5) - z[3];
    z[33]=z[33]*z[18];
    z[34]=z[3] + 2;
    z[35]=z[34]*z[3];
    z[36]= - static_cast<T>(1)- z[35];
    z[36]=z[9]*z[36];
    z[33]=n<T>(3,5)*z[36] - n<T>(3,5) + z[33];
    z[33]=z[9]*z[33];
    z[19]=z[19] + z[30] + z[27] + z[33];
    z[19]=z[13]*z[19];
    z[27]=z[10]*z[3];
    z[30]=z[34]*z[27];
    z[33]=2*z[3];
    z[36]=z[33] + 1;
    z[37]=z[7]*z[26];
    z[30]=z[37] + z[30] + z[36];
    z[30]=z[7]*z[30];
    z[27]=z[27] + z[34];
    z[27]=z[10]*z[27];
    z[27]=z[30] + z[27] + static_cast<T>(1)+ 4*z[26];
    z[27]=z[6]*z[27];
    z[30]=npow(z[10],2);
    z[34]=z[20]*z[30];
    z[38]=z[20]*z[7];
    z[35]= - z[35] - z[38];
    z[35]=z[7]*z[35];
    z[27]=z[27] + z[34] + z[35];
    z[34]= - static_cast<T>(2)- n<T>(9,5)*z[3];
    z[34]=z[3]*z[34];
    z[27]=z[34] + n<T>(1,5)*z[27];
    z[27]=z[6]*z[27];
    z[34]=static_cast<T>(17)+ 11*z[3];
    z[35]=z[34]*z[18];
    z[35]=z[35] + 3;
    z[35]=n<T>(1,5)*z[35];
    z[39]=3*z[9];
    z[40]=z[23]*z[39];
    z[41]= - z[35] + z[40];
    z[42]=npow(z[9],2);
    z[41]=z[41]*z[42];
    z[43]=z[23]*z[7]*npow(z[9],3);
    z[41]=z[41] - z[43];
    z[41]=z[7]*z[41];
    z[34]=z[3]*z[34];
    z[34]=static_cast<T>(6)+ z[34];
    z[34]=n<T>(1,5)*z[34] - z[40];
    z[34]=z[9]*z[34];
    z[40]=n<T>(3,5)*z[26];
    z[34]= - z[40] + z[34];
    z[34]=z[9]*z[34];
    z[34]=z[34] + z[41];
    z[34]=z[7]*z[34];
    z[25]= - z[35] + z[25];
    z[25]=z[9]*z[25];
    z[25]=z[40] + z[25];
    z[25]=z[9]*z[25];
    z[25]=z[34] + n<T>(1,5)*z[16] + z[25];
    z[25]=z[5]*z[25];
    z[34]=z[20]*z[10];
    z[35]=z[34] + z[26];
    z[29]=z[35]*z[29];
    z[40]= - z[26] - n<T>(1,2)*z[34];
    z[40]=z[10]*z[40];
    z[23]=z[40] - z[23];
    z[23]=z[13]*z[23]*z[24];
    z[23]=z[29] + z[23];
    z[23]=z[13]*z[23];
    z[24]=n<T>(4,5) - n<T>(1,4)*z[6];
    z[24]=z[6]*z[24];
    z[24]=z[24] - n<T>(23,20);
    z[24]=z[34]*z[24];
    z[23]=z[23] + z[24];
    z[23]=z[2]*z[23];
    z[24]=n<T>(7,2)*z[3];
    z[40]=static_cast<T>(13)+ z[24];
    z[40]=z[3]*z[40];
    z[40]=n<T>(19,2) + z[40];
    z[40]=z[40]*z[31];
    z[41]=n<T>(6,5)*z[3];
    z[44]=n<T>(7,2) + z[41];
    z[44]=z[3]*z[44];
    z[44]=n<T>(23,10) + z[44];
    z[44]=z[44]*z[42];
    z[43]=z[44] + n<T>(11,5)*z[43];
    z[43]=z[7]*z[43];
    z[40]=z[40] + z[43];
    z[40]=z[7]*z[40];
    z[40]=n<T>(3,10)*z[16] + z[40];
    z[40]=z[8]*z[40];
    z[19]=z[40] + z[19] + z[25] + z[27] + z[23];
    z[23]= - z[36]*z[41];
    z[25]=static_cast<T>(46)+ 19*z[3];
    z[25]=z[3]*z[25];
    z[25]=static_cast<T>(12)+ z[25];
    z[17]=n<T>(1,5)*z[25] + n<T>(9,2)*z[17];
    z[17]=z[9]*z[17];
    z[25]= - n<T>(2,5) - z[3];
    z[25]=z[3]*z[25];
    z[27]=n<T>(32,5) + 5*z[3];
    z[27]=z[3]*z[27];
    z[27]=n<T>(7,5) + z[27];
    z[27]=z[9]*z[27];
    z[25]=3*z[25] + z[27];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(3,5)*z[20] + z[25];
    z[25]=z[7]*z[25];
    z[17]=z[25] + z[23] + z[17];
    z[17]=z[9]*z[17];
    z[23]= - static_cast<T>(3)- 14*z[3];
    z[23]=z[23]*z[33];
    z[25]=static_cast<T>(14)+ 23*z[3];
    z[25]=z[25]*z[15];
    z[23]=z[23] + z[25];
    z[23]=z[10]*z[23]*z[42];
    z[17]=n<T>(1,5)*z[23] + z[17];
    z[17]=z[7]*z[17];
    z[23]= - z[10]*z[35];
    z[25]= - z[20] - z[34];
    z[25]=z[7]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[11]*z[23];
    z[25]=n<T>(1,5)*z[3];
    z[27]= - static_cast<T>(87)- 83*z[3];
    z[27]=z[27]*z[25];
    z[27]=z[27] - 15*z[34];
    z[33]=static_cast<T>(9)- n<T>(11,4)*z[6];
    z[33]=z[6]*z[35]*z[33];
    z[23]=n<T>(2,5)*z[23] + n<T>(3,5)*z[33] + n<T>(1,4)*z[27] - n<T>(6,5)*z[38];
    z[23]=z[11]*z[23];
    z[24]= - static_cast<T>(6)- z[24];
    z[15]=z[24]*z[15];
    z[15]=z[28] + z[32] + n<T>(1,5)*z[15];
    z[15]=z[10]*z[15];
    z[24]= - z[20]*z[31];
    z[21]=z[21] + z[24];
    z[21]=z[10]*z[21];
    z[21]=z[18] + z[21];
    z[21]=z[10]*z[21];
    z[24]=z[30]*z[35]*z[11];
    z[21]=z[21] - n<T>(3,10)*z[24];
    z[21]=z[14]*z[21];
    z[24]=z[11]*z[29];
    z[15]=n<T>(3,2)*z[21] + z[24] - n<T>(4,5) + 3*z[15];
    z[15]=z[14]*z[15];
    z[21]=z[5] - z[2];
    z[21]=z[4]*z[26]*z[21];
    z[24]=npow(z[6],2);
    z[26]=z[2]*z[20];
    z[21]=z[21] - z[24] + z[26];
    z[21]=n<T>(3,5)*z[21];
    z[21]=z[1]*z[21];
    z[24]=static_cast<T>(23)+ n<T>(71,2)*z[3];
    z[24]=z[24]*z[25];
    z[24]=z[24] + n<T>(3,2)*z[22];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(28,5)*z[20] + z[24];
    z[24]=z[9]*z[24];
    z[22]= - 13*z[20] + 7*z[22];
    z[22]=z[9]*z[22];
    z[22]=3*z[20] + z[22];
    z[22]=z[10]*z[22]*z[31];
    z[22]=z[24] + z[22];
    z[22]=z[10]*z[22];
    z[16]= - z[9]*z[16]*z[18];
    z[24]=static_cast<T>(9)+ n<T>(17,2)*z[3];
    z[24]=z[3]*z[24];
    z[24]=static_cast<T>(3)+ z[24];
    z[16]=n<T>(1,5)*z[24] + z[16];
    z[16]=z[16]*z[39];
    z[18]=static_cast<T>(2)+ z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(3,2) + 7*z[18];
    z[16]=n<T>(1,5)*z[18] + z[16];
    z[16]=z[9]*z[16];
    z[18]=z[11] - z[8];
    z[18]=z[12]*z[37]*z[18];

    r += z[15] + z[16] + z[17] + n<T>(6,5)*z[18] + 3*z[19] - n<T>(3,10)*z[20]
       + z[21] + z[22] + z[23];
 
    return r;
}

template double qqb_2lha_r328(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r328(const std::array<dd_real,30>&);
#endif
