#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1780(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[3];
    z[2]=k[10];
    z[3]=k[11];
    z[4]=k[27];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=z[4] - z[5];
    z[9]=n<T>(3,8)*z[6];
    z[8]=z[9]*z[8];
    z[10]=z[1]*z[4];
    z[11]=static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[12]= - z[5]*z[11];
    z[13]=n<T>(1,2)*z[4];
    z[12]=z[13] + z[12];
    z[14]=n<T>(3,4)*z[1];
    z[12]=z[12]*z[14];
    z[15]=n<T>(3,8)*z[1];
    z[10]=z[10] + 1;
    z[16]=z[4] - z[10];
    z[16]=z[16]*z[15];
    z[17]=z[6] + 1;
    z[17]=z[7]*z[17];
    z[18]= - static_cast<T>(1)+ z[17];
    z[16]=n<T>(1,3)*z[18] + z[16];
    z[16]=z[3]*z[16];
    z[8]=z[16] + z[12] - n<T>(1,3)*z[17] + z[8];
    z[8]=z[2]*z[8];
    z[11]=z[13] - z[11];
    z[11]=z[11]*z[14];
    z[12]=n<T>(1,8) + z[7];
    z[9]=z[11] + n<T>(1,3)*z[12] - z[9];
    z[9]=z[3]*z[9];
    z[10]= - z[15]*z[5]*z[10];
    z[8]=z[8] + z[10] + z[9];
    z[8]=z[2]*z[8];

    r +=  - n<T>(3,8)*z[3] + z[8];
 
    return r;
}

template double qqb_2lha_r1780(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1780(const std::array<dd_real,30>&);
#endif
