#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r388(const std::array<T,30>& k) {
  T z[54];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[7];
    z[6]=k[17];
    z[7]=k[2];
    z[8]=k[11];
    z[9]=k[4];
    z[10]=k[8];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=k[5];
    z[14]=n<T>(1,2)*z[11];
    z[15]= - static_cast<T>(47)- 65*z[11];
    z[15]=z[15]*z[14];
    z[16]=z[14] + 1;
    z[17]=3*z[11];
    z[18]=z[16]*z[17];
    z[19]=z[18] + 1;
    z[20]=z[19]*z[3];
    z[21]=static_cast<T>(353)+ 289*z[11];
    z[21]=z[11]*z[21];
    z[21]=n<T>(169,2) + z[21];
    z[21]=n<T>(1,2)*z[21] - 105*z[20];
    z[21]=z[3]*z[21];
    z[15]=z[15] + z[21];
    z[15]=z[3]*z[15];
    z[21]=npow(z[11],2);
    z[22]=z[21]*z[3];
    z[23]=z[22] - z[21];
    z[24]=z[23]*z[9];
    z[25]=npow(z[3],2);
    z[26]=z[24]*z[25];
    z[27]=z[3]*z[11];
    z[28]=z[11] + 1;
    z[29]=z[27]*z[28];
    z[30]=n<T>(379,4) + 151*z[11];
    z[30]=z[11]*z[30];
    z[30]=z[30] - n<T>(315,2)*z[29];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(31,4)*z[21] + z[30];
    z[30]=z[3]*z[30];
    z[30]=z[30] - n<T>(105,2)*z[26];
    z[30]=z[9]*z[30];
    z[15]=z[30] - n<T>(103,24)*z[21] + z[15];
    z[30]=n<T>(1,2)*z[9];
    z[15]=z[15]*z[30];
    z[31]=z[11] + 3;
    z[32]=z[31]*z[14];
    z[32]=z[32] + 1;
    z[33]=z[32]*z[3];
    z[34]=n<T>(327,8) + 23*z[11];
    z[34]=z[11]*z[34];
    z[34]= - n<T>(105,2)*z[33] - n<T>(5,4) + z[34];
    z[34]=z[3]*z[34];
    z[35]= - static_cast<T>(9)- n<T>(11,2)*z[11];
    z[35]=z[11]*z[35];
    z[34]=z[34] - static_cast<T>(5)+ n<T>(9,4)*z[35];
    z[34]=z[3]*z[34];
    z[35]=static_cast<T>(89)+ 25*z[11];
    z[35]=z[11]*z[35];
    z[15]=z[15] + n<T>(1,48)*z[35] + z[34];
    z[34]=z[9]*z[3];
    z[23]=z[23]*z[34];
    z[35]= - static_cast<T>(25)- n<T>(79,2)*z[11];
    z[35]=z[11]*z[35];
    z[35]=z[35] + n<T>(171,2)*z[29];
    z[35]=z[3]*z[35];
    z[35]=n<T>(57,2)*z[23] - n<T>(51,4)*z[21] + z[35];
    z[35]=z[35]*z[30];
    z[36]=n<T>(1,4)*z[11];
    z[37]= - n<T>(115,2) - n<T>(217,3)*z[11];
    z[37]=z[37]*z[36];
    z[38]=static_cast<T>(21)+ n<T>(13,4)*z[11];
    z[38]=z[11]*z[38];
    z[38]=n<T>(57,2)*z[20] + n<T>(7,4) + z[38];
    z[38]=z[3]*z[38];
    z[35]=z[35] + z[37] + z[38];
    z[35]=z[9]*z[35];
    z[37]=static_cast<T>(67)+ n<T>(35,2)*z[11];
    z[37]=z[37]*z[14];
    z[37]=n<T>(57,2)*z[33] + n<T>(85,3) + z[37];
    z[37]=z[3]*z[37];
    z[38]= - static_cast<T>(601)- 281*z[11];
    z[38]=z[11]*z[38];
    z[35]=z[35] + n<T>(1,24)*z[38] + z[37];
    z[37]=n<T>(1,3) + z[11];
    z[37]=z[11]*z[37];
    z[24]= - n<T>(1,3)*z[24] + z[37] - z[29];
    z[24]=z[9]*z[24];
    z[37]=z[28]*z[11];
    z[38]=z[11] + 2;
    z[39]= - z[11]*z[38];
    z[39]= - n<T>(2,3) + z[39];
    z[39]=z[3]*z[39];
    z[24]=z[24] + z[37] + z[39];
    z[24]=z[9]*z[24];
    z[39]=n<T>(1,3)*z[11];
    z[38]=z[38]*z[39];
    z[39]=z[39] + 1;
    z[39]=z[39]*z[11];
    z[40]= - n<T>(2,3) - z[39];
    z[40]=z[3]*z[40];
    z[24]=z[24] + z[38] + z[40];
    z[38]=z[21]*z[9];
    z[40]=2*z[21] + z[38];
    z[40]=z[9]*z[40];
    z[40]=z[21] + z[40];
    z[41]=n<T>(1,3)*z[2];
    z[40]=z[40]*z[41];
    z[24]=2*z[24] + z[40];
    z[24]=z[4]*z[24];
    z[40]= - z[21] - z[38];
    z[40]=z[40]*z[41];
    z[24]=z[24] + n<T>(1,16)*z[35] + z[40];
    z[24]=z[4]*z[24];
    z[35]=n<T>(1,2) - n<T>(1,3)*z[4];
    z[35]=z[4]*z[35];
    z[40]= - static_cast<T>(1)+ n<T>(2,3)*z[4];
    z[42]= - z[4]*z[40];
    z[42]= - static_cast<T>(1)+ z[42];
    z[42]=z[1]*z[42];
    z[35]=z[42] - n<T>(1,2) + z[35];
    z[35]=z[5]*z[35];
    z[15]=z[35] + n<T>(1,8)*z[15] + z[24];
    z[15]=z[4]*z[15];
    z[24]=static_cast<T>(25)- n<T>(209,8)*z[11];
    z[24]=z[24]*z[14];
    z[24]= - static_cast<T>(3)+ z[24];
    z[35]=n<T>(1,16)*z[11];
    z[42]=static_cast<T>(881)+ 547*z[11];
    z[42]=z[42]*z[35];
    z[42]=static_cast<T>(9)+ z[42];
    z[42]=z[3]*z[42];
    z[24]=n<T>(1,2)*z[24] + z[42];
    z[24]=z[24]*z[25];
    z[42]=static_cast<T>(9)+ n<T>(343,12)*z[11];
    z[42]=z[42]*z[14];
    z[42]=z[42] - 13*z[29];
    z[42]=z[3]*z[42];
    z[43]=static_cast<T>(1)- z[11];
    z[43]=z[11]*z[43];
    z[42]=n<T>(9,8)*z[43] + z[42];
    z[42]=z[3]*z[42];
    z[43]=n<T>(9,8)*z[21];
    z[42]= - z[43] + z[42];
    z[42]=z[42]*z[34];
    z[24]=z[42] - n<T>(1,4)*z[21] + z[24];
    z[24]=z[9]*z[24];
    z[42]=n<T>(1,6)*z[11];
    z[44]=static_cast<T>(6493)+ 3007*z[11];
    z[44]=z[44]*z[42];
    z[44]=static_cast<T>(429)+ z[44];
    z[45]=static_cast<T>(401)+ 101*z[11];
    z[45]=z[45]*z[36];
    z[45]=static_cast<T>(75)+ z[45];
    z[45]=z[3]*z[45];
    z[44]=n<T>(1,4)*z[44] + 3*z[45];
    z[45]=n<T>(1,2)*z[3];
    z[44]=z[44]*z[45];
    z[46]=5*z[11];
    z[47]=n<T>(19,2) - z[46];
    z[47]=z[11]*z[47];
    z[44]=z[44] - n<T>(21,2) + z[47];
    z[47]=n<T>(1,4)*z[3];
    z[44]=z[44]*z[47];
    z[24]=z[44] + z[24];
    z[44]=n<T>(9,2) + 7*z[11];
    z[35]=z[44]*z[35];
    z[44]=n<T>(1,32)*z[3];
    z[48]=n<T>(1079,3) + n<T>(427,2)*z[11];
    z[48]=z[11]*z[48];
    z[48]= - n<T>(457,6) + z[48];
    z[48]=z[48]*z[44];
    z[49]=static_cast<T>(2)- n<T>(7,32)*z[11];
    z[49]=z[11]*z[49];
    z[48]=z[48] - static_cast<T>(3)+ z[49];
    z[48]=z[3]*z[48];
    z[35]=z[35] + z[48];
    z[35]=z[3]*z[35];
    z[48]=npow(z[3],4);
    z[49]=z[48]*z[2];
    z[48]=z[9]*z[48];
    z[48]=z[49] + z[48];
    z[16]=z[16]*z[11];
    z[50]=n<T>(23,6) + 9*z[16];
    z[39]= - static_cast<T>(1)- z[39];
    z[39]=z[3]*z[39];
    z[39]=n<T>(1,4)*z[50] + 13*z[39];
    z[39]=z[3]*z[39];
    z[39]=n<T>(9,16)*z[37] + z[39];
    z[39]=z[39]*z[25];
    z[39]=z[39] + n<T>(13,3)*z[48];
    z[48]=n<T>(1,2)*z[2];
    z[39]=z[39]*z[48];
    z[50]=z[16] + n<T>(1,2);
    z[51]=z[50]*z[3];
    z[52]=n<T>(27,2) + n<T>(53,3)*z[11];
    z[52]=z[11]*z[52];
    z[52]= - n<T>(9,2) + z[52];
    z[52]=n<T>(1,4)*z[52] - 13*z[51];
    z[52]=z[3]*z[52];
    z[52]=n<T>(9,16)*z[16] + z[52];
    z[52]=z[3]*z[52];
    z[52]= - n<T>(9,32)*z[21] + z[52];
    z[52]=z[52]*z[34];
    z[35]=z[39] + z[35] + z[52];
    z[35]=z[2]*z[35];
    z[24]=n<T>(1,2)*z[24] + z[35];
    z[24]=z[2]*z[24];
    z[35]=n<T>(3,2)*z[11];
    z[39]=static_cast<T>(1)+ z[35];
    z[39]=z[39]*z[46];
    z[52]=15*z[3];
    z[19]= - z[19]*z[52];
    z[53]= - static_cast<T>(3)+ z[46];
    z[53]=z[11]*z[53];
    z[19]=z[19] - n<T>(1,4) + z[53];
    z[19]=z[3]*z[19];
    z[19]=z[39] + z[19];
    z[19]=z[3]*z[19];
    z[39]=static_cast<T>(29)+ 55*z[11];
    z[39]=z[39]*z[14];
    z[39]=z[39] - 45*z[29];
    z[39]=z[3]*z[39];
    z[39]=7*z[21] + z[39];
    z[39]=z[3]*z[39];
    z[26]=z[39] - 15*z[26];
    z[26]=z[26]*z[30];
    z[19]=z[26] - n<T>(7,4)*z[21] + z[19];
    z[19]=z[19]*z[30];
    z[26]=z[37] - 3*z[29];
    z[26]=z[3]*z[26];
    z[23]= - z[23] + z[21] + z[26];
    z[23]=z[23]*z[30];
    z[26]=z[28]*z[14];
    z[16]= - z[16] - z[20];
    z[16]=z[3]*z[16];
    z[16]=z[23] + z[26] + z[16];
    z[16]=z[9]*z[16];
    z[20]= - z[33] - z[32];
    z[20]=z[3]*z[20];
    z[16]=z[20] + z[16];
    z[16]=z[6]*z[16];
    z[20]= - z[32]*z[52];
    z[23]= - static_cast<T>(41)- z[46];
    z[23]=z[11]*z[23];
    z[23]= - static_cast<T>(45)+ z[23];
    z[20]=n<T>(1,4)*z[23] + z[20];
    z[20]=z[20]*z[45];
    z[23]=n<T>(11,4) + 2*z[11];
    z[23]=z[11]*z[23];
    z[20]=z[20] - n<T>(3,8) + z[23];
    z[20]=z[3]*z[20];
    z[16]=n<T>(9,4)*z[16] + z[20] + z[19];
    z[16]=z[6]*z[16];
    z[19]=z[30]*z[21];
    z[20]=z[11] + n<T>(1,2);
    z[20]=z[20]*z[11];
    z[23]=z[20] - z[19];
    z[23]=z[2]*z[23];
    z[29]= - z[21]*npow(z[9],2);
    z[18]=z[23] + z[18] + z[29];
    z[18]=z[2]*z[18];
    z[23]=z[31]*z[35];
    z[23]=static_cast<T>(1)+ z[23];
    z[20]= - z[20] - z[19];
    z[20]=z[9]*z[20];
    z[29]=static_cast<T>(1)- n<T>(3,4)*z[11];
    z[29]=z[11]*z[29];
    z[20]=z[20] - static_cast<T>(1)+ z[29];
    z[20]=z[9]*z[20];
    z[18]=z[18] + n<T>(1,2)*z[23] + z[20];
    z[20]=z[9]*z[11];
    z[23]= - n<T>(3,2) - z[11];
    z[23]=z[23]*z[20];
    z[26]= - z[2]*z[26];
    z[29]= - n<T>(5,2) - z[11];
    z[29]=z[11]*z[29];
    z[23]=z[26] + z[23] - n<T>(1,2) + z[29];
    z[23]=z[2]*z[23];
    z[26]=n<T>(1,8)*z[11];
    z[17]= - static_cast<T>(11)- z[17];
    z[17]=z[17]*z[26];
    z[17]= - static_cast<T>(1)+ z[17];
    z[29]=z[31]*z[20];
    z[31]=z[36] + 1;
    z[31]=z[31]*z[11];
    z[29]= - n<T>(1,8)*z[29] - n<T>(3,8) - z[31];
    z[29]=z[9]*z[29];
    z[17]=n<T>(1,4)*z[23] + n<T>(1,2)*z[17] + z[29];
    z[17]=z[2]*z[17];
    z[20]= - 3*z[28] - z[20];
    z[20]=z[20]*z[30];
    z[20]=z[20] - static_cast<T>(3)- n<T>(7,4)*z[11];
    z[20]=z[9]*z[20];
    z[20]=z[20] - n<T>(7,4) - z[31];
    z[17]=n<T>(1,4)*z[20] + z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,4)*z[18] + z[17];
    z[18]=n<T>(5,4)*z[8];
    z[17]=z[17]*z[18];
    z[20]=static_cast<T>(537)+ n<T>(79,6)*z[11];
    z[20]=z[20]*z[26];
    z[23]=static_cast<T>(675)+ n<T>(1301,4)*z[11];
    z[23]=z[11]*z[23];
    z[23]=static_cast<T>(225)+ z[23];
    z[23]=z[23]*z[45];
    z[20]=z[23] + static_cast<T>(15)+ z[20];
    z[20]=z[3]*z[20];
    z[23]=static_cast<T>(5)- n<T>(1819,24)*z[11];
    z[23]=z[23]*z[14];
    z[20]=z[23] + z[20];
    z[20]=z[3]*z[20];
    z[20]= - z[11] + z[20];
    z[23]=n<T>(887,3) + n<T>(1187,4)*z[11];
    z[23]=z[23]*z[27];
    z[23]= - n<T>(645,4)*z[21] + z[23];
    z[23]=z[3]*z[23];
    z[26]= - static_cast<T>(9)- n<T>(23,2)*z[11];
    z[26]=z[11]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[23]*z[45];
    z[23]= - z[21] + z[23];
    z[22]=n<T>(79,4)*z[21] - 13*z[22];
    z[22]=z[3]*z[22];
    z[22]= - z[43] + n<T>(1,3)*z[22];
    z[22]=z[3]*z[22];
    z[22]= - z[43] + z[22];
    z[22]=z[22]*z[34];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[22]=z[9]*z[22];
    z[20]=n<T>(1,4)*z[20] + z[22];
    z[20]=z[9]*z[20];
    z[22]=static_cast<T>(603)- 41*z[11];
    z[22]=z[22]*z[36];
    z[23]=static_cast<T>(1399)+ 499*z[11];
    z[23]=z[23]*z[36];
    z[23]=static_cast<T>(225)+ z[23];
    z[23]=z[3]*z[23];
    z[22]=z[23] + static_cast<T>(205)+ z[22];
    z[22]=z[3]*z[22];
    z[23]=static_cast<T>(1073)+ 541*z[11];
    z[23]=z[23]*z[42];
    z[23]=static_cast<T>(159)+ z[23];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(27,4)*z[21] + z[22];
    z[20]=n<T>(1,8)*z[22] + z[20];
    z[22]=static_cast<T>(11)+ n<T>(75,4)*z[3];
    z[22]=z[22]*z[25];
    z[23]=npow(z[3],3);
    z[26]=n<T>(1,2)*z[23] - 13*z[49];
    z[26]=z[26]*z[41];
    z[22]=n<T>(3,4)*z[22] + z[26];
    z[22]=z[2]*z[22];
    z[26]=static_cast<T>(257)+ 225*z[3];
    z[26]=z[26]*z[47];
    z[26]=static_cast<T>(15)+ z[26];
    z[26]=z[26]*z[47];
    z[22]=z[26] + z[22];
    z[26]= - n<T>(41,2) - 105*z[3];
    z[26]=z[26]*z[44];
    z[27]=n<T>(57,64)*z[3] - z[40];
    z[27]=z[4]*z[27];
    z[26]=z[27] - static_cast<T>(1)+ z[26];
    z[26]=z[4]*z[3]*z[26];
    z[27]= - n<T>(31,2) - z[52];
    z[27]=z[3]*z[27];
    z[29]= - static_cast<T>(1)- z[3];
    z[29]=z[6]*z[29];
    z[27]=n<T>(9,2)*z[29] - static_cast<T>(5)+ z[27];
    z[27]=z[6]*z[3]*z[27];
    z[22]=n<T>(1,4)*z[27] + n<T>(1,2)*z[22] + z[26];
    z[22]=z[1]*z[22];
    z[26]=z[37] + z[38];
    z[27]= - z[37] - z[19];
    z[27]=z[9]*z[27];
    z[27]=z[27] - z[50];
    z[27]=z[4]*z[27];
    z[26]=17*z[26] + 5*z[27];
    z[26]=z[4]*z[26];
    z[19]= - z[11] - z[19];
    z[19]=z[9]*z[19];
    z[21]= - static_cast<T>(1)- n<T>(27,8)*z[21];
    z[19]=n<T>(1,16)*z[26] + n<T>(1,2)*z[21] + z[19];
    z[19]=z[9]*z[19];
    z[21]= - static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[18]=z[18]*z[38]*z[21];
    z[18]=z[18] + z[19];
    z[18]=z[13]*z[18];
    z[19]=z[2]*z[50]*z[23];
    z[21]=n<T>(195,2) + 61*z[11];
    z[21]=z[21]*z[11];
    z[21]=z[21] + n<T>(73,2);
    z[23]= - 147*z[51] + z[21];
    z[23]=z[23]*z[25];
    z[23]=z[23] + 49*z[19];
    z[23]=z[23]*z[48];
    z[26]=n<T>(147,2)*z[51] - z[21];
    z[26]=z[3]*z[26];
    z[27]=static_cast<T>(65)+ n<T>(203,4)*z[11];
    z[27]=z[27]*z[11];
    z[27]=z[27] + n<T>(57,4);
    z[26]=n<T>(1,2)*z[27] + z[26];
    z[26]=z[3]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[2]*z[23];
    z[21]= - 49*z[51] + z[21];
    z[21]=z[3]*z[21];
    z[21]=z[21] - z[27];
    z[21]=z[3]*z[21];
    z[21]=n<T>(57,4)*z[37] + z[21];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[12]*z[21];
    z[23]=static_cast<T>(5)- z[11];
    z[14]=z[23]*z[14];
    z[14]=static_cast<T>(3)+ z[14];
    z[14]=z[14]*z[25];
    z[14]=z[14] + 7*z[19];
    z[14]=z[2]*z[14];
    z[19]= - n<T>(3,4) - z[31];
    z[19]=z[3]*z[19];
    z[14]=z[19] + z[14];
    z[14]=z[2]*z[14];
    z[14]= - n<T>(1,4)*z[28] + z[14];
    z[14]=z[10]*z[14];
    z[19]=static_cast<T>(1)+ z[9];
    z[19]=3*z[19] + z[2];
    z[19]=z[8]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[8]*z[19];
    z[23]= - z[7]*npow(z[8],2);
    z[19]=z[19] + z[23];
    z[19]=z[7]*z[19];

    r += n<T>(3,32)*z[14] + z[15] + z[16] + z[17] + n<T>(1,4)*z[18] + n<T>(5,32)*
      z[19] + n<T>(1,2)*z[20] + n<T>(1,16)*z[21] + z[22] + z[24];
 
    return r;
}

template double qqb_2lha_r388(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r388(const std::array<dd_real,30>&);
#endif
