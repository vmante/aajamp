#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2134(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[20];
    z[5]=k[17];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[11];
    z[9]=k[15];
    z[10]=z[7]*npow(z[8],2);
    z[11]=z[5] + z[9];
    z[12]=z[10]*z[11];
    z[13]=z[8] - 1;
    z[14]=z[5] + 2*z[9];
    z[13]=z[8]*z[14]*z[13];
    z[13]=z[13] - z[12];
    z[13]=z[7]*z[13];
    z[14]= - static_cast<T>(1)+ z[2];
    z[15]=2*z[5];
    z[16]=z[8]*z[15];
    z[13]=z[13] + n<T>(1,4)*z[14] + z[16];
    z[13]=z[7]*z[13];
    z[14]=z[8]*z[5];
    z[12]=3*z[14] + z[12];
    z[12]=z[7]*z[12];
    z[12]=z[12] + z[15] - static_cast<T>(2)+ z[2];
    z[12]=z[7]*z[12];
    z[14]=z[5] - 1;
    z[14]=z[1]*z[14];
    z[16]=z[2] - z[1];
    z[17]=z[2]*z[16];
    z[12]=z[12] + z[17] + z[14];
    z[12]=z[3]*z[12];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=z[2]*z[16];
    z[12]=z[12] + z[13] + n<T>(1,4)*z[16] + z[14];
    z[12]=z[3]*z[12];
    z[13]=z[5]*z[6];
    z[13]=z[13] - static_cast<T>(1)+ z[9];
    z[13]=z[8]*z[13];
    z[11]=z[13] - z[11];
    z[11]=z[8]*z[11];
    z[13]= - z[9] - z[15];
    z[10]=z[13]*z[10];
    z[13]=z[2]*z[4];
    z[14]= - z[4] + z[13];
    z[10]=z[10] + n<T>(1,4)*z[14] + z[11];
    z[10]=z[7]*z[10];
    z[11]=z[1]*z[4];
    z[13]= - z[11] + z[13];
    z[13]=z[2]*z[13];
    z[11]=z[13] - z[4] - z[11];

    r += z[10] + n<T>(1,4)*z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r2134(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2134(const std::array<dd_real,30>&);
#endif
