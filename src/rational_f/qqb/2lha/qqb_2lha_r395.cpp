#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r395(const std::array<T,30>& k) {
  T z[32];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[7];
    z[6]=k[17];
    z[7]=k[4];
    z[8]=k[8];
    z[9]=k[10];
    z[10]=k[9];
    z[11]=k[5];
    z[12]=n<T>(27,4)*z[4];
    z[13]=21*z[6];
    z[14]= - static_cast<T>(47)+ z[13];
    z[15]=n<T>(9,4)*z[4];
    z[16]=z[15] - n<T>(1,3) + n<T>(7,4)*z[6];
    z[16]=z[7]*z[16];
    z[14]=z[16] + z[12] + n<T>(1,4)*z[14] - n<T>(2,3)*z[2];
    z[14]=z[7]*z[14];
    z[16]= - n<T>(185,4) + z[13];
    z[17]=n<T>(1,3)*z[2];
    z[18]= - n<T>(97,8) - z[17];
    z[18]=z[2]*z[18];
    z[12]=z[14] + z[12] + n<T>(1,4)*z[16] + z[18];
    z[12]=z[7]*z[12];
    z[14]=n<T>(21,16)*z[10];
    z[16]=7*z[10];
    z[18]=z[16] + 3*z[8];
    z[19]=z[2]*z[18];
    z[19]= - n<T>(1,16)*z[19] - static_cast<T>(5)+ z[14];
    z[19]=z[2]*z[19];
    z[20]= - static_cast<T>(43)- 21*z[10];
    z[19]=n<T>(1,16)*z[20] + z[19];
    z[19]=z[2]*z[19];
    z[20]= - static_cast<T>(71)+ z[16];
    z[20]=n<T>(1,4)*z[20] + 7*z[6];
    z[12]=z[12] + z[15] + n<T>(1,4)*z[20] + z[19];
    z[12]=z[9]*z[12];
    z[19]=n<T>(1,2)*z[2];
    z[18]= - z[18]*z[19];
    z[18]=z[18] - n<T>(131,3) + n<T>(21,2)*z[10];
    z[18]=z[2]*z[18];
    z[20]= - n<T>(57,2) - z[16];
    z[18]=n<T>(3,2)*z[20] + z[18];
    z[18]=z[2]*z[18];
    z[16]= - n<T>(199,2) + z[16];
    z[13]=27*z[4] + z[18] + n<T>(1,2)*z[16] + z[13];
    z[16]= - static_cast<T>(8)+ n<T>(7,2)*z[6];
    z[18]=n<T>(27,2)*z[4];
    z[20]=z[18] - n<T>(65,3) + n<T>(21,2)*z[6];
    z[20]=z[7]*z[20];
    z[18]=n<T>(1,2)*z[20] + z[18] + 3*z[16] - n<T>(41,2)*z[2];
    z[18]=z[7]*z[18];
    z[12]=z[12] + n<T>(1,4)*z[13] + z[18];
    z[12]=z[9]*z[12];
    z[13]=npow(z[2],2);
    z[18]=z[7]*z[2];
    z[18]=2*z[13] + z[18];
    z[18]=z[7]*z[18];
    z[20]=z[2] + n<T>(1,3)*z[7];
    z[20]=z[7]*z[20];
    z[20]=z[13] + z[20];
    z[20]=z[7]*z[20];
    z[21]=npow(z[2],3);
    z[20]=n<T>(1,3)*z[21] + z[20];
    z[20]=z[9]*z[20];
    z[18]=z[20] + z[21] + z[18];
    z[18]=z[9]*z[18];
    z[20]=z[17] - 1;
    z[22]=n<T>(1,3)*z[1] - z[20];
    z[21]=z[22]*z[21];
    z[13]= - z[7]*z[20]*z[13];
    z[13]=z[18] + z[21] + z[13];
    z[13]=z[3]*z[13];
    z[18]= - n<T>(49,2) - 13*z[1];
    z[20]= - n<T>(7,8)*z[10] + n<T>(13,3) - n<T>(3,8)*z[8];
    z[20]=z[2]*z[20];
    z[18]=z[20] + n<T>(1,3)*z[18] + n<T>(21,8)*z[10];
    z[18]=z[18]*z[19];
    z[20]=z[1] + 2;
    z[21]=4*z[20];
    z[14]=z[18] - z[21] - z[14];
    z[14]=z[2]*z[14];
    z[18]=5*z[2];
    z[22]= - static_cast<T>(1)+ z[19];
    z[22]=z[22]*z[18];
    z[23]=n<T>(9,2)*z[4];
    z[16]=z[23] + z[22] + z[16];
    z[16]=z[7]*z[16];
    z[22]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[23]=z[22]*z[23];
    z[24]=z[22]*z[6];
    z[12]=z[13] + z[12] + z[16] + z[23] + z[14] + n<T>(7,2)*z[24] - z[21] + 
   n<T>(7,16)*z[10];
    z[12]=z[3]*z[12];
    z[13]=17*z[10];
    z[14]=static_cast<T>(5)- n<T>(17,2)*z[10];
    z[14]=z[2]*z[14];
    z[14]=z[14] - n<T>(229,6) + z[13];
    z[14]=z[2]*z[14];
    z[13]=static_cast<T>(5)- z[13];
    z[16]=static_cast<T>(13)+ z[6];
    z[16]=z[6]*z[16];
    z[21]=3*z[4];
    z[23]= - static_cast<T>(23)- z[21];
    z[23]=z[4]*z[23];
    z[13]=z[23] + z[14] + n<T>(1,2)*z[13] + z[16];
    z[14]=n<T>(3,2)*z[6];
    z[16]= - static_cast<T>(5)+ z[6];
    z[16]=z[16]*z[14];
    z[23]=n<T>(1,2)*z[4];
    z[25]=9*z[4];
    z[26]= - static_cast<T>(59)- z[25];
    z[26]=z[26]*z[23];
    z[23]= - static_cast<T>(3)- z[23];
    z[23]=z[23]*z[21];
    z[27]=n<T>(1,2)*z[6];
    z[28]=z[27] - 7;
    z[28]=z[28]*z[6];
    z[23]=z[28] + z[23];
    z[23]=z[7]*z[23];
    z[16]=z[23] + z[26] + static_cast<T>(31)+ z[16];
    z[23]=n<T>(1,4)*z[7];
    z[16]=z[16]*z[23];
    z[26]=3*z[6];
    z[29]=n<T>(1,4)*z[6];
    z[30]=static_cast<T>(1)+ z[29];
    z[30]=z[30]*z[26];
    z[30]=n<T>(35,4)*z[2] - n<T>(11,12) + z[30];
    z[31]= - static_cast<T>(8)- n<T>(9,8)*z[4];
    z[31]=z[4]*z[31];
    z[16]=z[16] + n<T>(1,2)*z[30] + z[31];
    z[16]=z[7]*z[16];
    z[13]=n<T>(1,8)*z[13] + z[16];
    z[13]=z[9]*z[13];
    z[16]= - static_cast<T>(5)- z[4];
    z[16]=z[16]*z[25];
    z[30]=static_cast<T>(49)+ z[26];
    z[30]=z[6]*z[30];
    z[31]= - z[8] - 9*z[10];
    z[31]=z[2]*z[31];
    z[31]=n<T>(3,2)*z[31] - n<T>(583,6) + 27*z[10];
    z[31]=z[2]*z[31];
    z[16]=z[16] + z[31] + z[30] - static_cast<T>(47)- n<T>(27,2)*z[10];
    z[26]= - static_cast<T>(5)+ z[26];
    z[26]=z[6]*z[26];
    z[25]= - static_cast<T>(35)- z[25];
    z[25]=z[4]*z[25];
    z[25]=z[26] + z[25];
    z[25]=z[7]*z[25];
    z[26]=static_cast<T>(11)+ z[14];
    z[26]=z[6]*z[26];
    z[26]= - n<T>(25,2) + z[26];
    z[15]= - static_cast<T>(10)- z[15];
    z[15]=z[4]*z[15];
    z[15]=n<T>(1,8)*z[25] + n<T>(1,2)*z[26] + z[15];
    z[15]=z[7]*z[15];
    z[13]=z[13] + n<T>(1,8)*z[16] + z[15];
    z[13]=z[9]*z[13];
    z[15]=static_cast<T>(37)+ 23*z[1];
    z[15]=n<T>(1,2)*z[15] + z[24];
    z[15]=z[15]*z[29];
    z[16]=n<T>(5,2)*z[10];
    z[25]= - z[16] + static_cast<T>(17)- n<T>(3,4)*z[8];
    z[25]=z[25]*z[19];
    z[16]=z[25] + z[16] - static_cast<T>(15)- n<T>(17,2)*z[1];
    z[16]=z[16]*z[19];
    z[22]= - z[22]*z[21];
    z[25]= - static_cast<T>(7)+ z[1];
    z[22]=n<T>(1,2)*z[25] + z[22];
    z[22]=z[4]*z[22];
    z[25]=n<T>(9,2) + z[6];
    z[25]=z[6]*z[25];
    z[21]= - n<T>(17,2) - z[21];
    z[21]=z[4]*z[21];
    z[21]=z[21] + 7*z[2] - static_cast<T>(7)+ z[25];
    z[21]=z[21]*z[23];
    z[23]=5*z[1];
    z[12]=z[12] + z[13] + z[21] + n<T>(1,4)*z[22] + z[16] + z[15] - n<T>(5,8)*
    z[10] - n<T>(31,4) - z[23];
    z[12]=z[3]*z[12];
    z[13]=static_cast<T>(1)- n<T>(3,2)*z[10];
    z[13]=z[13]*z[18];
    z[15]=npow(z[6],2);
    z[16]= - n<T>(11,3) + 3*z[10];
    z[13]=z[13] + n<T>(5,2)*z[16] + z[15];
    z[16]= - static_cast<T>(7)- z[27];
    z[16]=z[16]*z[29];
    z[21]=npow(z[4],2);
    z[22]=n<T>(1,3)*z[4];
    z[25]=n<T>(3,8) + z[22];
    z[25]=z[25]*z[21];
    z[15]=n<T>(1,8)*z[15];
    z[25]= - z[15] + z[25];
    z[25]=z[7]*z[25];
    z[26]=n<T>(1,2) + z[4];
    z[26]=z[4]*z[26];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[4]*z[26];
    z[16]=z[25] + z[16] + z[26];
    z[16]=z[7]*z[16];
    z[25]=n<T>(157,12) + z[28];
    z[26]= - n<T>(1,8) + z[4];
    z[26]=z[4]*z[26];
    z[26]=n<T>(9,4) + z[26];
    z[26]=z[4]*z[26];
    z[16]=z[16] + n<T>(1,4)*z[25] + z[26];
    z[16]=z[7]*z[16];
    z[25]= - n<T>(1,4) + z[22];
    z[25]=z[4]*z[25];
    z[25]=n<T>(5,4) + z[25];
    z[25]=z[4]*z[25];
    z[13]=z[16] + n<T>(1,8)*z[13] + z[25];
    z[13]=z[9]*z[13];
    z[16]= - n<T>(25,12) + z[10];
    z[14]=static_cast<T>(5)+ z[14];
    z[14]=z[6]*z[14];
    z[18]= - z[10]*z[18];
    z[14]=z[18] + 5*z[16] + z[14];
    z[16]= - static_cast<T>(1)+ z[27];
    z[16]=z[16]*z[27];
    z[18]=n<T>(1,4) + z[4];
    z[18]=z[18]*z[21];
    z[18]= - z[15] + z[18];
    z[18]=z[7]*z[18];
    z[25]=2*z[4];
    z[26]= - n<T>(3,4) + z[25];
    z[26]=z[4]*z[26];
    z[26]=n<T>(3,2) + z[26];
    z[26]=z[4]*z[26];
    z[16]=z[18] + z[16] + z[26];
    z[16]=z[7]*z[16];
    z[18]= - static_cast<T>(1)+ z[4];
    z[18]=z[4]*z[18];
    z[18]=n<T>(7,4) + z[18];
    z[18]=z[4]*z[18];
    z[13]=z[13] + z[16] + n<T>(1,4)*z[14] + z[18];
    z[13]=z[9]*z[13];
    z[14]=z[24] + n<T>(11,2) + z[23];
    z[14]=z[6]*z[14];
    z[16]=n<T>(5,4)*z[10];
    z[18]=static_cast<T>(7)- z[16];
    z[18]=z[2]*z[18];
    z[14]=z[18] + z[14] + z[16] - static_cast<T>(9)- 7*z[1];
    z[16]=z[20]*z[22];
    z[18]= - n<T>(19,12) - z[1];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[4]*z[16];
    z[18]=n<T>(1,4) + z[1];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[4]*z[16];
    z[18]=z[21]*z[7];
    z[20]= - n<T>(1,8) + n<T>(2,3)*z[4];
    z[20]=z[20]*z[18];
    z[12]=z[12] + z[13] + z[20] + n<T>(1,4)*z[14] + z[16];
    z[12]=z[3]*z[12];
    z[13]=n<T>(1,6)*z[2];
    z[14]=n<T>(1,8)*z[11];
    z[16]= - static_cast<T>(1)- z[17];
    z[16]=z[4]*z[16];
    z[16]=z[16] + z[13] + n<T>(2,3) - z[14];
    z[16]=z[4]*z[16];
    z[17]=n<T>(1,3) + z[11];
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[4]*z[16];
    z[17]=static_cast<T>(1)- z[11];
    z[13]= - static_cast<T>(1)- z[13];
    z[13]=z[4]*z[13];
    z[13]=n<T>(1,4)*z[17] + z[13];
    z[13]=z[4]*z[13];
    z[17]=n<T>(1,4)*z[11];
    z[13]=z[17] + z[13];
    z[13]=z[4]*z[13];
    z[20]= - z[14] - z[22];
    z[20]=z[20]*z[18];
    z[13]=z[20] - z[15] + z[13];
    z[13]=z[7]*z[13];
    z[20]= - static_cast<T>(1)- z[6];
    z[20]=z[6]*z[20];
    z[13]=z[13] + n<T>(1,8)*z[20] + z[16];
    z[13]=z[7]*z[13];
    z[16]=n<T>(5,2) + z[2];
    z[19]= - static_cast<T>(1)- z[19];
    z[19]=z[4]*z[19];
    z[16]=n<T>(1,2)*z[16] + z[19];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(1,4) + z[16];
    z[16]=z[16]*z[22];
    z[19]=static_cast<T>(1)- z[10];
    z[13]=z[13] + n<T>(5,16)*z[19] + z[16];
    z[13]=z[9]*z[13];
    z[16]=static_cast<T>(1)- n<T>(1,2)*z[11];
    z[16]=n<T>(1,2)*z[16] - z[4];
    z[16]=z[4]*z[16];
    z[16]=z[17] + z[16];
    z[16]=z[4]*z[16];
    z[17]= - z[17] - z[22];
    z[17]=z[17]*z[18];
    z[15]=z[17] - z[15] + z[16];
    z[15]=z[7]*z[15];
    z[16]=n<T>(5,2) - z[25];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[16]*z[22];
    z[13]=z[13] + z[15] - n<T>(5,16)*z[10] + z[16];
    z[13]=z[9]*z[13];
    z[15]=z[1] + n<T>(1,2);
    z[16]= - n<T>(1,2) + z[22];
    z[16]=z[4]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[15]=z[4]*z[16]*z[5]*z[15];
    z[14]= - z[18]*z[14];

    r += z[12] + z[13] + z[14] + z[15];
 
    return r;
}

template double qqb_2lha_r395(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r395(const std::array<dd_real,30>&);
#endif
