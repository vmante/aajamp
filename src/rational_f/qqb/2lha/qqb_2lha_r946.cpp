#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r946(const std::array<T,30>& k) {
  T z[64];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[20];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[5];
    z[10]=k[9];
    z[11]=k[4];
    z[12]=k[8];
    z[13]=k[10];
    z[14]=k[23];
    z[15]=k[27];
    z[16]=k[13];
    z[17]=k[15];
    z[18]=k[12];
    z[19]=k[24];
    z[20]=z[9] + n<T>(49,2);
    z[20]=z[20]*z[9];
    z[21]=npow(z[9],2);
    z[22]=z[21]*z[11];
    z[20]=z[20] - n<T>(47,4)*z[22];
    z[20]=z[20]*z[11];
    z[23]=n<T>(1,2)*z[11];
    z[24]=z[23]*z[21];
    z[25]=n<T>(1,2)*z[9];
    z[26]=z[25] - 1;
    z[26]=z[26]*z[9];
    z[26]=z[26] + z[24];
    z[26]=z[26]*z[11];
    z[27]=z[9] - n<T>(1,2);
    z[26]=z[26] - z[27];
    z[26]=z[26]*z[11];
    z[26]=z[26] + n<T>(1,2);
    z[28]=n<T>(51,2)*z[19];
    z[28]=z[26]*z[28];
    z[20]=z[28] + z[20] - z[9] - n<T>(51,4);
    z[28]=z[19]*z[20];
    z[29]=z[11]*z[9];
    z[30]=z[29] - 1;
    z[31]=z[9] + 1;
    z[32]=z[6]*z[30]*z[31];
    z[33]=19*z[9];
    z[34]= - static_cast<T>(5)+ z[33];
    z[34]=z[9]*z[34];
    z[34]=z[34] + 41*z[22];
    z[35]=z[21]*z[2];
    z[34]=n<T>(1,12)*z[34] - 3*z[35];
    z[36]= - static_cast<T>(17)+ n<T>(13,4)*z[10];
    z[37]= - z[35]*z[36];
    z[37]=n<T>(13,4)*z[21] + z[37];
    z[37]=z[10]*z[37];
    z[28]=n<T>(1,8)*z[28] + n<T>(1,24)*z[37] + n<T>(1,2)*z[34] - n<T>(1,3)*z[32];
    z[28]=z[13]*z[28];
    z[34]=n<T>(1,3)*z[11];
    z[37]=z[34]*z[21];
    z[38]=z[37] - z[9];
    z[38]=z[38]*z[11];
    z[38]=z[38] + 1;
    z[39]=z[3]*z[11];
    z[40]=z[38]*z[39];
    z[41]=n<T>(1,24)*z[9];
    z[42]=static_cast<T>(5)+ z[41];
    z[42]=z[42]*z[25];
    z[42]=z[42] - n<T>(5,3)*z[22];
    z[42]=z[11]*z[42];
    z[42]= - z[41] + z[42];
    z[42]=z[11]*z[42];
    z[42]=5*z[40] + n<T>(1,48) + z[42];
    z[42]=z[3]*z[42];
    z[43]=n<T>(1,8)*z[9];
    z[44]=n<T>(5,3) + z[43];
    z[44]=z[9]*z[44];
    z[44]=z[44] - n<T>(41,48)*z[22];
    z[44]=z[11]*z[44];
    z[45]= - n<T>(13,2) - z[9];
    z[42]=z[42] + n<T>(1,8)*z[45] + z[44];
    z[42]=z[3]*z[42];
    z[44]=n<T>(11,16)*z[9];
    z[45]= - n<T>(5,3) + z[44];
    z[45]=z[9]*z[45];
    z[45]=z[45] + n<T>(5,6)*z[22];
    z[45]=z[11]*z[45];
    z[46]=n<T>(5,3) - n<T>(11,4)*z[9];
    z[45]=n<T>(1,2)*z[46] + z[45];
    z[45]=z[11]*z[45];
    z[40]= - n<T>(5,2)*z[40] + n<T>(11,16) + z[45];
    z[40]=z[3]*z[40];
    z[45]=z[22] - z[9];
    z[45]=z[45]*z[11];
    z[40]=n<T>(29,32)*z[45] + z[40];
    z[40]=z[7]*z[40];
    z[40]=z[40] - n<T>(29,32)*z[22] + z[42];
    z[40]=z[7]*z[40];
    z[42]=z[35] + z[9];
    z[46]=z[42]*z[10];
    z[36]=z[36]*z[46];
    z[36]=n<T>(95,8)*z[42] + z[36];
    z[42]=z[21]*z[5];
    z[36]=n<T>(1,3)*z[36] - 9*z[42];
    z[47]=z[31]*z[9];
    z[48]=z[17]*z[5];
    z[49]=z[47]*z[48];
    z[36]=n<T>(1,4)*z[36] + z[49];
    z[36]=z[12]*z[36];
    z[35]= - z[46] - z[22] + z[35];
    z[35]=z[14]*z[35];
    z[28]=z[40] + z[28] + z[36] + z[35];
    z[35]=npow(z[3],2);
    z[36]=n<T>(1,8)*z[35];
    z[40]= - n<T>(117,2) + n<T>(37,3)*z[3];
    z[40]=z[3]*z[40];
    z[40]=n<T>(79,24) + z[40];
    z[40]=z[40]*z[36];
    z[46]=n<T>(1,12)*z[3];
    z[49]=31*z[3];
    z[50]=n<T>(343,4) - z[49];
    z[50]=z[50]*z[46];
    z[50]=static_cast<T>(1)+ z[50];
    z[51]= - static_cast<T>(1)+ n<T>(3,8)*z[3];
    z[51]=z[3]*z[51];
    z[51]= - n<T>(1,2) + z[51];
    z[51]=z[2]*z[51];
    z[50]=n<T>(1,2)*z[50] + z[51];
    z[50]=z[2]*z[35]*z[50];
    z[40]=z[40] + z[50];
    z[40]=z[2]*z[40];
    z[49]=static_cast<T>(11)- z[49];
    z[49]=z[49]*z[46];
    z[50]=3*z[6];
    z[51]=n<T>(9,4)*z[6] - n<T>(19,4) - z[3];
    z[51]=z[51]*z[50];
    z[49]=z[51] + static_cast<T>(5)+ z[49];
    z[49]=z[6]*z[49];
    z[51]=n<T>(1,2)*z[3];
    z[52]=z[3] + n<T>(1,2);
    z[53]=z[3]*z[52];
    z[53]= - static_cast<T>(1)- n<T>(31,3)*z[53];
    z[53]=z[53]*z[51];
    z[49]=z[53] + z[49];
    z[49]=z[6]*z[49];
    z[53]=5*z[3];
    z[54]=n<T>(743,12) - z[53];
    z[54]=z[3]*z[54];
    z[54]= - n<T>(79,24) + z[54];
    z[36]=z[54]*z[36];
    z[54]=npow(z[6],3);
    z[55]=n<T>(3,2)*z[6];
    z[56]= - static_cast<T>(1)+ z[55];
    z[56]=z[5]*z[56]*z[54];
    z[52]= - z[52] + n<T>(1,2)*z[7];
    z[52]=z[7]*z[35]*z[52];
    z[36]=n<T>(5,12)*z[52] + n<T>(9,4)*z[56] + n<T>(1,2)*z[49] + z[36] + z[40];
    z[36]=z[1]*z[36];
    z[40]=n<T>(1,3)*z[9];
    z[49]=static_cast<T>(5)- n<T>(151,2)*z[9];
    z[49]=z[49]*z[40];
    z[49]= - n<T>(61,2) + z[49];
    z[52]=static_cast<T>(1)+ n<T>(47,4)*z[9];
    z[52]=z[9]*z[52];
    z[52]=z[52] - z[37];
    z[52]=z[11]*z[52];
    z[49]=n<T>(1,4)*z[49] + z[52];
    z[49]=z[11]*z[49];
    z[52]= - static_cast<T>(1081)+ 119*z[9];
    z[49]=n<T>(1,24)*z[52] + z[49];
    z[52]=static_cast<T>(141)- n<T>(53,3)*z[9];
    z[52]=z[52]*z[25];
    z[52]= - n<T>(61,3) + z[52];
    z[52]=n<T>(1,8)*z[52] - z[22];
    z[52]=z[11]*z[52];
    z[56]=7*z[9];
    z[57]= - n<T>(1391,6) + z[56];
    z[52]=n<T>(1,8)*z[57] + z[52];
    z[52]=z[11]*z[52];
    z[57]=n<T>(11,2)*z[9];
    z[58]= - n<T>(5,3) - z[57];
    z[58]=z[58]*z[23];
    z[58]=z[58] + n<T>(5,6) - z[9];
    z[58]=z[11]*z[58];
    z[58]=n<T>(35,12) + z[58];
    z[58]=z[11]*z[58];
    z[58]=n<T>(5,12) + z[58];
    z[58]=z[3]*z[58];
    z[52]=z[58] - n<T>(11,16) + z[52];
    z[52]=z[3]*z[52];
    z[49]=n<T>(1,4)*z[49] + z[52];
    z[49]=z[3]*z[49];
    z[44]=static_cast<T>(1)+ z[44];
    z[44]=z[9]*z[44];
    z[44]=z[44] - z[24];
    z[44]=z[44]*z[34];
    z[52]= - static_cast<T>(5)+ n<T>(31,3)*z[9];
    z[52]=z[9]*z[52];
    z[52]=n<T>(79,12) + z[52];
    z[44]=n<T>(1,4)*z[52] + z[44];
    z[44]=z[11]*z[44];
    z[52]= - static_cast<T>(133)+ n<T>(23,2)*z[9];
    z[52]=z[9]*z[52];
    z[52]= - static_cast<T>(17)+ n<T>(1,6)*z[52];
    z[44]=n<T>(1,4)*z[52] + z[44];
    z[44]=n<T>(1,2)*z[44] + z[49];
    z[44]=z[3]*z[44];
    z[49]=npow(z[11],3);
    z[52]=n<T>(71,3) + n<T>(27,2)*z[9];
    z[52]=z[9]*z[52];
    z[52]=n<T>(61,6) + z[52];
    z[52]=z[52]*z[49];
    z[58]=n<T>(5,3)*z[3];
    z[59]=z[25] + 1;
    z[60]=z[59]*z[9];
    z[61]=n<T>(1,2) + z[60];
    z[61]=z[61]*npow(z[11],4)*z[58];
    z[52]=n<T>(1,4)*z[52] + z[61];
    z[52]=z[3]*z[52];
    z[61]=npow(z[11],2);
    z[62]=n<T>(53,2) + n<T>(55,3)*z[9];
    z[62]=z[9]*z[62];
    z[62]=n<T>(49,6) + z[62];
    z[62]=z[62]*z[61];
    z[52]=n<T>(1,4)*z[62] + z[52];
    z[52]=z[3]*z[52];
    z[62]=static_cast<T>(29)+ n<T>(43,2)*z[9];
    z[62]=z[62]*z[40];
    z[62]=n<T>(5,2) + z[62];
    z[62]=z[11]*z[62];
    z[52]=n<T>(7,16)*z[62] + z[52];
    z[52]=z[3]*z[52];
    z[48]= - z[31]*z[25]*z[48];
    z[62]=n<T>(1,16)*z[9];
    z[63]=static_cast<T>(67)+ 51*z[9];
    z[63]=z[63]*z[62];
    z[63]=static_cast<T>(1)+ z[63];
    z[48]=z[48] + n<T>(1,2)*z[63] + z[52];
    z[48]=z[18]*z[48];
    z[45]= - n<T>(553,16)*z[21] + z[45];
    z[44]=z[48] + n<T>(1,12)*z[45] + z[44];
    z[45]= - static_cast<T>(37)- z[25];
    z[45]=z[45]*z[34];
    z[48]=static_cast<T>(5)+ n<T>(17,3)*z[11];
    z[48]=z[3]*z[48];
    z[45]=z[48] - n<T>(21,4) + z[45];
    z[48]=n<T>(1,4)*z[3];
    z[45]=z[45]*z[48];
    z[52]= - n<T>(49,3) - z[62];
    z[62]= - static_cast<T>(5)- n<T>(1,48)*z[60];
    z[62]=z[11]*z[62];
    z[45]=z[45] + n<T>(1,2)*z[52] + z[62];
    z[45]=z[45]*z[51];
    z[52]=n<T>(1,96)*z[9];
    z[62]=static_cast<T>(1)- n<T>(29,2)*z[9];
    z[62]=z[62]*z[52];
    z[63]= - static_cast<T>(3)- n<T>(1,32)*z[21];
    z[63]=z[63]*z[23];
    z[53]=n<T>(11,4) - z[53];
    z[46]=z[53]*z[46];
    z[46]=z[46] + static_cast<T>(3)+ z[52];
    z[46]=z[46]*z[51];
    z[46]=z[46] + static_cast<T>(1)+ n<T>(1,192)*z[21];
    z[46]=z[2]*z[46];
    z[45]=z[46] + z[45] + z[63] - static_cast<T>(1)+ z[62];
    z[45]=z[2]*z[45];
    z[46]= - static_cast<T>(11)- n<T>(19,4)*z[9];
    z[46]=z[46]*z[40];
    z[52]=z[34]*z[9];
    z[53]= - n<T>(11,2) + z[9];
    z[53]=z[53]*z[52];
    z[46]=z[53] + n<T>(85,8) + z[46];
    z[46]=z[11]*z[46];
    z[53]=n<T>(139,3) - n<T>(39,8)*z[9];
    z[46]=n<T>(1,2)*z[53] + z[46];
    z[53]= - static_cast<T>(1)- n<T>(1,32)*z[9];
    z[53]=z[53]*z[52];
    z[62]=3*z[9];
    z[63]=n<T>(619,24) - z[62];
    z[53]=n<T>(1,4)*z[63] + z[53];
    z[53]=z[11]*z[53];
    z[63]= - static_cast<T>(23)- 5*z[11];
    z[63]=z[63]*z[34];
    z[63]= - n<T>(5,2) + z[63];
    z[48]=z[63]*z[48];
    z[48]=z[48] + n<T>(85,96) + z[53];
    z[48]=z[3]*z[48];
    z[46]=n<T>(1,4)*z[46] + z[48];
    z[46]=z[3]*z[46];
    z[48]= - static_cast<T>(25)- z[57];
    z[41]=z[48]*z[41];
    z[48]= - static_cast<T>(1)+ n<T>(5,48)*z[9];
    z[48]=z[48]*z[29];
    z[41]=z[41] + z[48];
    z[41]=z[11]*z[41];
    z[48]= - static_cast<T>(5)- n<T>(91,6)*z[9];
    z[48]=z[9]*z[48];
    z[48]= - n<T>(79,6) + z[48];
    z[41]=n<T>(1,16)*z[48] + z[41];
    z[41]=z[45] + n<T>(1,2)*z[41] + z[46];
    z[41]=z[3]*z[41];
    z[41]= - n<T>(1,24)*z[22] + z[41];
    z[41]=z[2]*z[41];
    z[45]=static_cast<T>(17)+ n<T>(7,2)*z[11];
    z[45]=z[45]*z[34];
    z[46]=z[11] + 1;
    z[48]=z[46]*z[11];
    z[53]= - static_cast<T>(1)+ z[48];
    z[53]=z[11]*z[53];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[53]*z[58];
    z[45]=z[53] + n<T>(9,2) + z[45];
    z[45]=z[3]*z[45];
    z[45]= - n<T>(17,6)*z[46] + z[45];
    z[45]=z[45]*z[35];
    z[46]=n<T>(17,3)*z[9];
    z[53]=z[46] - 5*z[29];
    z[53]=z[11]*z[53];
    z[53]=n<T>(161,6)*z[9] + z[53];
    z[53]=z[11]*z[53];
    z[46]=z[46] + n<T>(1,4)*z[53];
    z[53]=n<T>(5,32)*z[22];
    z[57]=n<T>(1,3)*z[21] - z[53];
    z[57]=z[11]*z[57];
    z[57]=n<T>(97,192)*z[21] + z[57];
    z[57]=z[2]*z[57];
    z[46]=n<T>(1,8)*z[46] + z[57];
    z[46]=z[10]*z[46];
    z[57]= - n<T>(35,3) + n<T>(13,8)*z[9];
    z[43]=z[57]*z[43];
    z[57]=n<T>(5,16) + z[40];
    z[57]=z[9]*z[57];
    z[53]=z[57] - z[53];
    z[53]=z[11]*z[53];
    z[57]= - static_cast<T>(37)+ n<T>(97,4)*z[9];
    z[57]=z[9]*z[57];
    z[53]=n<T>(1,48)*z[57] + z[53];
    z[53]=z[11]*z[53];
    z[43]=z[46] + n<T>(1,4)*z[45] + z[43] + z[53];
    z[45]=z[11] + n<T>(1,2);
    z[46]= - static_cast<T>(23)- 13*z[11];
    z[46]=z[3]*z[46];
    z[46]=n<T>(1,24)*z[46] + n<T>(83,48) + z[11];
    z[46]=z[3]*z[46];
    z[46]=z[46] + z[45];
    z[46]=z[3]*z[46];
    z[46]=z[46] + z[45];
    z[46]=z[3]*z[46];
    z[53]= - static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[53]=z[3]*z[53];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[3]*z[53];
    z[53]= - static_cast<T>(1)+ z[53];
    z[53]=z[2]*z[53]*z[51];
    z[46]=z[46] + z[53];
    z[46]=z[2]*z[46];
    z[53]=z[25]*z[11];
    z[57]= - static_cast<T>(1)+ n<T>(5,32)*z[9];
    z[57]=z[57]*z[53];
    z[58]= - n<T>(1,2) - z[40];
    z[58]=z[9]*z[58];
    z[57]=z[58] + z[57];
    z[57]=z[11]*z[57];
    z[58]=static_cast<T>(3)- n<T>(1,6)*z[11];
    z[58]=z[11]*z[58];
    z[58]=n<T>(19,6) + z[58];
    z[58]=z[58]*z[51];
    z[63]= - n<T>(15,4) - z[11];
    z[63]=z[11]*z[63];
    z[58]=z[58] - n<T>(43,12) + z[63];
    z[58]=z[3]*z[58];
    z[58]=z[58] + n<T>(17,24) - z[48];
    z[58]=z[3]*z[58];
    z[58]= - z[48] + z[58];
    z[58]=z[58]*z[51];
    z[46]=z[46] + z[58] - n<T>(211,384)*z[21] + z[57];
    z[46]=z[2]*z[46];
    z[43]=z[46] + n<T>(1,2)*z[43];
    z[43]=z[10]*z[43];
    z[46]=n<T>(31,2)*z[39];
    z[38]=z[46]*z[38];
    z[46]=static_cast<T>(73)+ z[33];
    z[46]=z[9]*z[46];
    z[46]=z[46] - 31*z[22];
    z[46]=z[46]*z[23];
    z[33]=z[46] - n<T>(53,2) - z[33];
    z[33]=z[11]*z[33];
    z[33]=n<T>(19,2) + z[33];
    z[33]=n<T>(1,3)*z[33] + z[38];
    z[33]=z[33]*z[51];
    z[46]= - n<T>(7,6)*z[22] - static_cast<T>(5)+ n<T>(37,12)*z[9];
    z[46]=z[11]*z[46];
    z[33]=z[33] - n<T>(23,12) + z[46];
    z[46]= - n<T>(11,4) + z[9];
    z[46]=z[9]*z[46];
    z[46]=z[46] - z[22];
    z[46]=z[46]*z[23];
    z[57]=z[62] - z[22];
    z[57]=z[57]*z[11];
    z[57]=z[57] - 3;
    z[58]=z[6]*z[57];
    z[46]=n<T>(9,8)*z[58] + z[46] + n<T>(17,4) - z[9];
    z[46]=z[11]*z[46];
    z[57]= - z[3]*z[57]*z[23];
    z[46]=z[57] + n<T>(1,2) + z[46];
    z[46]=z[46]*z[50];
    z[33]=n<T>(1,2)*z[33] + z[46];
    z[33]=z[6]*z[33];
    z[46]=n<T>(29,3)*z[9];
    z[50]=n<T>(31,2) + z[46];
    z[50]=z[9]*z[50];
    z[50]=z[50] - n<T>(31,3)*z[22];
    z[50]=z[50]*z[23];
    z[46]= - z[46] + z[50];
    z[46]=z[11]*z[46];
    z[38]=z[38] + n<T>(29,6) + z[46];
    z[38]=z[3]*z[38];
    z[46]=n<T>(19,6) - z[9];
    z[46]=z[9]*z[46];
    z[46]= - n<T>(89,12)*z[22] + n<T>(1,2) + z[46];
    z[46]=z[11]*z[46];
    z[38]=z[38] + z[46] + n<T>(17,4) + z[9];
    z[38]=z[3]*z[38];
    z[46]=static_cast<T>(1)- n<T>(3,2)*z[22];
    z[38]=n<T>(3,2)*z[46] + z[38];
    z[33]=n<T>(1,2)*z[38] + z[33];
    z[33]=z[6]*z[33];
    z[38]=static_cast<T>(1)+ n<T>(1,4)*z[9];
    z[38]=z[38]*z[40];
    z[46]=static_cast<T>(41)+ 67*z[9];
    z[46]=z[46]*z[25];
    z[50]=n<T>(41,3)*z[22];
    z[46]=z[46] - z[50];
    z[57]=n<T>(1,32)*z[11];
    z[46]=z[46]*z[57];
    z[38]=z[38] + z[46];
    z[38]=z[11]*z[38];
    z[46]=z[60] + n<T>(25,48)*z[22];
    z[46]=z[11]*z[46];
    z[46]= - z[40] + z[46];
    z[46]=z[11]*z[46];
    z[58]= - z[25] + z[37];
    z[60]=z[61]*z[3];
    z[58]=z[58]*z[60];
    z[46]=z[46] + z[58];
    z[46]=z[46]*z[51];
    z[38]=z[38] + z[46];
    z[38]=z[3]*z[38];
    z[46]=n<T>(11,2) + z[56];
    z[46]=z[9]*z[46];
    z[46]=n<T>(5,24)*z[46] - z[22];
    z[46]=z[11]*z[46];
    z[38]=z[46] + z[38];
    z[38]=z[3]*z[38];
    z[46]=n<T>(1,2) + z[9];
    z[46]=z[46]*z[40];
    z[46]=z[46] - z[24];
    z[46]=z[46]*z[61];
    z[49]=z[49]*z[21];
    z[58]=z[3]*z[49];
    z[46]=z[46] + n<T>(1,6)*z[58];
    z[46]=z[3]*z[46];
    z[58]= - n<T>(1,3) - z[9];
    z[58]=z[9]*z[58];
    z[58]=z[58] - n<T>(3,32)*z[22];
    z[58]=z[11]*z[58];
    z[58]=n<T>(1,6)*z[47] + z[58];
    z[58]=z[11]*z[58];
    z[46]=z[58] + z[46];
    z[46]=z[3]*z[46];
    z[58]= - n<T>(41,3) - 9*z[9];
    z[58]=z[9]*z[58];
    z[50]=z[58] + z[50];
    z[50]=z[50]*z[57];
    z[57]= - n<T>(1,3) - z[25];
    z[57]=z[9]*z[57];
    z[50]=z[57] + z[50];
    z[50]=z[11]*z[50];
    z[46]=z[50] + z[46];
    z[46]=z[3]*z[46];
    z[50]= - static_cast<T>(25)- n<T>(31,4)*z[9];
    z[50]=z[9]*z[50];
    z[50]=z[50] + n<T>(91,2)*z[22];
    z[50]=z[11]*z[50];
    z[46]=n<T>(1,48)*z[50] + z[46];
    z[46]=z[16]*z[46];
    z[50]=n<T>(355,6) + 63*z[9];
    z[50]=z[50]*z[25];
    z[50]=z[50] - n<T>(115,3)*z[22];
    z[38]=n<T>(1,2)*z[46] + n<T>(1,32)*z[50] + z[38];
    z[38]=z[16]*z[38];
    z[25]= - z[25] + z[22];
    z[25]=z[11]*z[25];
    z[25]= - n<T>(1,2) + z[25];
    z[46]=9*z[6];
    z[25]=z[25]*z[46];
    z[25]=z[25] + static_cast<T>(3)- n<T>(13,2)*z[22];
    z[25]=z[25]*z[54];
    z[50]=npow(z[16],2);
    z[54]=z[21]*z[50];
    z[57]=z[5]*npow(z[6],4)*z[22];
    z[25]=n<T>(27,2)*z[57] + 3*z[25] - n<T>(7,24)*z[54];
    z[54]=n<T>(1,2)*z[5];
    z[25]=z[25]*z[54];
    z[57]=z[47] - z[22];
    z[23]=z[57]*z[23];
    z[27]= - z[23] + z[27];
    z[27]=z[11]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[27]*z[46];
    z[46]= - n<T>(17,2)*z[22] + static_cast<T>(3)- z[9];
    z[46]=z[11]*z[46];
    z[27]=z[27] + n<T>(19,2) + z[46];
    z[27]=z[27]*z[55];
    z[27]=z[27] - static_cast<T>(5)+ n<T>(17,12)*z[22];
    z[27]=z[27]*npow(z[6],2);
    z[46]=n<T>(5,4) + z[56];
    z[46]=z[9]*z[46];
    z[46]=z[46] + n<T>(95,4)*z[22];
    z[46]=z[16]*z[46];
    z[46]= - n<T>(383,4)*z[21] + z[46];
    z[46]=z[16]*z[46];
    z[25]=z[25] + z[27] + n<T>(1,48)*z[46];
    z[25]=z[25]*z[54];
    z[27]=n<T>(7,2)*z[9];
    z[46]= - static_cast<T>(11)- z[27];
    z[46]=z[46]*z[52];
    z[27]=z[46] + n<T>(11,3) + z[27];
    z[27]=z[11]*z[27];
    z[27]= - n<T>(7,3) + z[27];
    z[27]=z[11]*z[27];
    z[29]= - z[59]*z[29];
    z[29]=z[29] + static_cast<T>(1)+ 2*z[9];
    z[29]=z[29]*z[34];
    z[29]= - n<T>(1,2) + z[29];
    z[29]=z[29]*z[60];
    z[27]=n<T>(1,8)*z[27] + z[29];
    z[27]=z[3]*z[27];
    z[29]=23*z[9];
    z[34]= - static_cast<T>(53)- z[29];
    z[34]=z[34]*z[53];
    z[29]=z[34] + n<T>(53,2) + z[29];
    z[29]=z[11]*z[29];
    z[29]= - n<T>(23,2) + z[29];
    z[27]=n<T>(1,24)*z[29] + z[27];
    z[27]=z[3]*z[27];
    z[29]=static_cast<T>(13)+ n<T>(85,6)*z[9];
    z[29]=z[29]*z[30];
    z[27]=n<T>(1,32)*z[29] + z[27];
    z[27]=z[3]*z[27];
    z[29]=n<T>(1,3)*z[3];
    z[30]= - n<T>(1,2) + z[11];
    z[30]=z[3]*z[30];
    z[30]=n<T>(11,8) + z[30];
    z[30]=z[30]*z[29];
    z[30]= - n<T>(5,8) + z[30];
    z[30]=z[30]*z[35];
    z[34]= - n<T>(5,16) + z[29];
    z[34]=z[34]*npow(z[3],3);
    z[35]=z[2]*npow(z[3],4);
    z[34]=z[34] - n<T>(1,6)*z[35];
    z[34]=z[2]*z[34];
    z[30]=z[30] + z[34];
    z[30]=z[2]*z[30];
    z[34]= - static_cast<T>(1)+ z[11];
    z[34]=z[34]*z[39];
    z[35]= - n<T>(7,2) + 11*z[11];
    z[34]=n<T>(1,8)*z[35] + z[34];
    z[34]=z[3]*z[34];
    z[34]=n<T>(53,16) + z[34];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(7,64) + z[34];
    z[34]=z[34]*z[29];
    z[30]=z[34] + z[30];
    z[30]=z[2]*z[30];
    z[27]= - n<T>(5,12)*z[32] + z[30] + n<T>(5,192)*z[31] + z[27];
    z[27]=z[8]*z[27];
    z[30]= - z[31]*z[40];
    z[30]=z[30] + z[24];
    z[30]=z[11]*z[30];
    z[31]= - n<T>(1,2) - n<T>(2,3)*z[9];
    z[31]=z[9]*z[31];
    z[31]=z[31] + z[37];
    z[31]=z[31]*z[61];
    z[29]= - z[29]*z[49];
    z[29]=z[31] + z[29];
    z[29]=z[3]*z[29];
    z[29]=z[30] + z[29];
    z[29]=z[3]*z[29];
    z[24]= - z[47] + z[24];
    z[24]=z[24]*z[61];
    z[30]= - z[51]*z[49];
    z[24]=z[24] + z[30];
    z[24]=z[3]*z[24];
    z[23]= - z[23] + z[24];
    z[23]=z[17]*z[23];
    z[22]=n<T>(1,3)*z[23] + n<T>(1,6)*z[22] + z[29];
    z[22]=z[17]*z[22];
    z[23]=z[16]*z[47];
    z[21]= - 143*z[21] + 23*z[23];
    z[21]=z[16]*z[21];
    z[24]=z[50]*z[42];
    z[21]=z[21] - 23*z[24];
    z[21]=z[5]*z[21];
    z[24]=5*z[47] - n<T>(143,24)*z[42];
    z[24]=z[12]*z[24];
    z[21]=z[24] + 5*z[23] + n<T>(1,24)*z[21];
    z[21]=z[5]*z[21];
    z[24]=z[47] - z[42];
    z[24]=z[12]*z[24];
    z[29]= - z[16]*z[42];
    z[23]=z[24] + z[23] + z[29];
    z[23]=z[15]*z[23]*npow(z[5],2);
    z[21]=n<T>(23,24)*z[23] + z[21];
    z[21]=z[15]*z[21];
    z[23]=z[7]*z[26];
    z[20]=n<T>(51,2)*z[23] + z[20];
    z[20]=z[19]*z[7]*z[20];
    z[23]= - z[45] + n<T>(1,2)*z[2];
    z[24]=z[2] - 1;
    z[26]=z[1]*z[24];
    z[26]= - z[23] + n<T>(1,2)*z[26];
    z[26]=z[4]*z[26];
    z[24]=z[26] - n<T>(3,2)*z[11] + z[24];
    z[24]=z[24]*npow(z[2],2);
    z[23]= - z[2]*z[23];
    z[23]= - z[48] + z[23];
    z[23]=z[10]*z[2]*z[23];
    z[23]=z[23] + z[24];
    z[23]=z[4]*z[23];

    r += n<T>(1,32)*z[20] + n<T>(1,16)*z[21] + z[22] + z[23] + z[25] + z[27] + 
      n<T>(1,4)*z[28] + z[33] + z[36] + z[38] + z[41] + z[43] + n<T>(1,2)*z[44]
      ;
 
    return r;
}

template double qqb_2lha_r946(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r946(const std::array<dd_real,30>&);
#endif
