#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r277(const std::array<T,30>& k) {
  T z[48];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[6];
    z[6]=k[13];
    z[7]=k[20];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[11];
    z[11]=k[9];
    z[12]=n<T>(1,3)*z[8];
    z[13]=2*z[8];
    z[14]=n<T>(1,4) + z[13];
    z[14]=z[14]*z[12];
    z[15]=2*z[3];
    z[16]=z[15] + 2*z[7];
    z[17]=n<T>(2,3)*z[8];
    z[18]= - z[17] + n<T>(19,2) - z[16];
    z[18]=z[1]*z[18];
    z[14]=z[18] - n<T>(3,4) + z[14];
    z[14]=z[1]*z[14];
    z[18]=n<T>(1,2)*z[8];
    z[19]=z[18] - 1;
    z[19]=z[19]*z[8];
    z[19]=z[19] + n<T>(1,2);
    z[14]= - n<T>(3,2)*z[19] + z[14];
    z[14]=z[1]*z[14];
    z[20]=n<T>(1,2)*z[6];
    z[21]=n<T>(1,2)*z[3];
    z[22]=n<T>(1,2)*z[7];
    z[23]=z[21] + z[22] - n<T>(2,3) - z[20];
    z[23]=z[1]*z[23];
    z[23]=z[23] + n<T>(1,4) + z[17];
    z[23]=z[1]*z[23];
    z[24]=z[8] - 1;
    z[23]= - n<T>(1,4)*z[24] + z[23];
    z[23]=z[1]*z[23];
    z[19]= - n<T>(1,2)*z[19] + z[23];
    z[19]=z[1]*z[19];
    z[23]=n<T>(1,4)*z[11];
    z[25]=z[23]*z[8];
    z[26]=z[11] - n<T>(1,4);
    z[25]=z[25] - z[26];
    z[25]=z[25]*z[8];
    z[27]=z[11] - n<T>(1,2);
    z[25]=z[25] + n<T>(3,2)*z[27];
    z[25]=z[25]*z[8];
    z[28]=z[11] - n<T>(3,4);
    z[25]=z[25] - z[28];
    z[25]=z[25]*z[8];
    z[29]=z[11] - 1;
    z[25]=z[25] + n<T>(1,4)*z[29];
    z[19]=z[19] - z[25];
    z[19]=z[4]*z[19];
    z[14]=z[19] - 3*z[25] + z[14];
    z[14]=z[4]*z[14];
    z[19]=z[24]*npow(z[1],3);
    z[25]=z[4]*npow(z[1],4);
    z[19]=4*z[19] + z[25];
    z[19]=z[4]*z[19];
    z[25]=npow(z[9],2);
    z[19]=z[25] + z[19];
    z[30]= - static_cast<T>(4)+ z[8];
    z[30]=z[12]*z[30];
    z[30]=static_cast<T>(2)+ z[30];
    z[30]=z[8]*z[25]*z[30];
    z[30]= - n<T>(4,3)*z[25] + z[30];
    z[30]=z[8]*z[30];
    z[31]=n<T>(1,3)*z[9];
    z[32]= - static_cast<T>(1)+ z[12];
    z[32]=z[8]*z[32];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[8]*z[9]*z[32];
    z[32]= - z[31] + z[32];
    z[33]= - static_cast<T>(2)+ z[8];
    z[33]=z[8]*z[33];
    z[33]=static_cast<T>(1)+ z[33];
    z[33]=z[1]*z[33];
    z[32]=2*z[32] + z[33];
    z[32]=z[1]*z[32];
    z[19]=2*z[32] + z[30] + n<T>(1,3)*z[19];
    z[19]=z[5]*z[19];
    z[30]=n<T>(3,4)*z[11];
    z[32]= - n<T>(2,3) - z[23];
    z[32]=z[9]*z[32];
    z[32]= - z[30] + z[32];
    z[32]=z[8]*z[32];
    z[33]=3*z[9] + n<T>(9,4) + z[11];
    z[33]=z[9]*z[33];
    z[26]=z[32] + 3*z[26] + z[33];
    z[26]=z[8]*z[26];
    z[32]=3*z[11];
    z[33]= - n<T>(11,2) - z[32];
    z[33]=n<T>(1,2)*z[33] - 10*z[9];
    z[33]=z[9]*z[33];
    z[26]=z[26] - n<T>(9,2)*z[27] + z[33];
    z[26]=z[8]*z[26];
    z[33]=12*z[9] + n<T>(17,12) + z[11];
    z[33]=z[9]*z[33];
    z[26]=z[26] + 3*z[28] + z[33];
    z[26]=z[8]*z[26];
    z[28]=n<T>(89,2) + z[13];
    z[28]=z[28]*z[12];
    z[33]=3*z[3];
    z[34]=3*z[7];
    z[28]=z[28] + z[33] - n<T>(43,2) + z[34];
    z[28]=z[1]*z[28];
    z[35]= - static_cast<T>(1)+ z[9];
    z[13]=z[35]*z[13];
    z[35]=n<T>(25,2) + 59*z[9];
    z[13]=n<T>(1,2)*z[35] + z[13];
    z[13]=z[8]*z[13];
    z[13]=z[13] - n<T>(13,2) - 77*z[9];
    z[13]=z[13]*z[12];
    z[16]=n<T>(115,6) - z[16];
    z[16]=z[9]*z[16];
    z[13]=z[28] + z[13] + n<T>(3,4) + z[16];
    z[13]=z[1]*z[13];
    z[16]=z[21]*z[25];
    z[28]= - static_cast<T>(1)- z[11];
    z[35]= - static_cast<T>(6)+ z[22];
    z[35]=z[9]*z[35];
    z[28]=n<T>(1,4)*z[28] + z[35];
    z[28]=z[9]*z[28];
    z[13]=z[19] + z[14] + z[13] + z[26] + z[16] - n<T>(3,4)*z[29] + z[28];
    z[13]=z[5]*z[13];
    z[14]=z[7] - 1;
    z[19]=z[14]*z[34];
    z[26]= - static_cast<T>(1)- z[3];
    z[26]=z[26]*z[33];
    z[19]=z[26] + n<T>(139,6) + z[19];
    z[26]=n<T>(9,2) + z[12];
    z[26]=z[8]*z[26];
    z[19]=n<T>(1,2)*z[19] + z[26];
    z[19]=z[1]*z[19];
    z[26]=z[3]*z[9];
    z[28]=n<T>(3,2) - z[9];
    z[28]=n<T>(1,2)*z[28] + z[26];
    z[28]=z[3]*z[28];
    z[35]=static_cast<T>(29)+ z[9];
    z[36]= - z[17] - n<T>(43,12) + 6*z[9];
    z[36]=z[8]*z[36];
    z[35]=n<T>(1,2)*z[35] + z[36];
    z[35]=z[8]*z[35];
    z[36]= - static_cast<T>(25)+ z[34];
    z[37]= - n<T>(1,2) - z[7];
    z[37]=z[7]*z[37];
    z[37]= - n<T>(15,2) + z[37];
    z[37]=z[9]*z[37];
    z[19]=z[19] + z[35] + z[28] + n<T>(1,2)*z[36] + z[37];
    z[19]=z[1]*z[19];
    z[28]=3*z[6];
    z[35]= - n<T>(31,3) - z[28];
    z[36]=n<T>(5,2) - z[7];
    z[36]=z[7]*z[36];
    z[37]=n<T>(5,2) + z[3];
    z[37]=z[3]*z[37];
    z[35]=z[37] + n<T>(1,2)*z[35] + z[36];
    z[35]=z[1]*z[35];
    z[36]=n<T>(59,6)*z[8] - n<T>(3,2)*z[3] + n<T>(19,2) - z[34];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[35]=z[1]*z[35];
    z[36]= - n<T>(17,6) + z[8];
    z[36]=z[8]*z[36];
    z[36]=n<T>(11,6) + z[36];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[35]=z[1]*z[35];
    z[36]=static_cast<T>(1)+ n<T>(1,4)*z[3];
    z[36]=z[36]*z[3];
    z[37]=n<T>(1,4)*z[6];
    z[38]=static_cast<T>(1)+ z[37];
    z[38]=z[6]*z[38];
    z[39]= - static_cast<T>(1)+ n<T>(1,4)*z[7];
    z[39]=z[7]*z[39];
    z[38]= - z[36] + z[39] + n<T>(1,3) + z[38];
    z[38]=z[1]*z[38];
    z[39]=z[28] - z[21];
    z[40]=z[7] + static_cast<T>(5)- z[39];
    z[17]=z[38] + n<T>(1,2)*z[40] - z[17];
    z[17]=z[1]*z[17];
    z[38]=static_cast<T>(1)+ z[12];
    z[38]=z[8]*z[38];
    z[17]=z[17] - n<T>(1,3) + z[38];
    z[17]=z[1]*z[17];
    z[17]= - n<T>(3,4)*z[24] + z[17];
    z[17]=z[1]*z[17];
    z[24]=z[8]*z[11];
    z[32]= - z[24] + z[32] - 1;
    z[32]=z[32]*z[18];
    z[38]=n<T>(3,2)*z[11];
    z[32]=z[32] - z[38] + 1;
    z[32]=z[32]*z[8];
    z[29]=n<T>(1,2)*z[29];
    z[32]=z[32] + z[29];
    z[17]=n<T>(3,2)*z[32] + z[17];
    z[17]=z[4]*z[17];
    z[17]=z[17] + n<T>(7,2)*z[32] + z[35];
    z[17]=z[4]*z[17];
    z[32]=static_cast<T>(27)+ z[11];
    z[35]=z[22] + 1;
    z[40]=z[35]*z[7];
    z[41]=static_cast<T>(3)+ z[40];
    z[41]=z[9]*z[41];
    z[32]=z[41] + n<T>(1,2)*z[32] - z[7];
    z[32]=z[9]*z[32];
    z[41]=z[9] - n<T>(1,2);
    z[41]=z[41]*z[9];
    z[16]=z[41] - z[16];
    z[16]=z[3]*z[16];
    z[42]=n<T>(1,2)*z[11];
    z[43]= - n<T>(1,3) + z[42];
    z[16]=z[16] + 5*z[43] + z[32];
    z[32]= - n<T>(7,3) - n<T>(5,2)*z[11];
    z[43]= - static_cast<T>(6)- z[23];
    z[43]=z[9]*z[43];
    z[12]=z[12] + n<T>(1,2)*z[32] + z[43];
    z[12]=z[8]*z[12];
    z[32]=z[11] + 23;
    z[32]=n<T>(3,4)*z[32];
    z[43]=2*z[9];
    z[44]=z[32] + z[43];
    z[44]=z[9]*z[44];
    z[45]=n<T>(15,2)*z[11];
    z[46]=static_cast<T>(1)+ z[45];
    z[12]=z[12] + n<T>(1,2)*z[46] + z[44];
    z[12]=z[8]*z[12];
    z[44]=4*z[9];
    z[32]= - z[32] - z[44];
    z[32]=z[9]*z[32];
    z[45]=n<T>(7,3) - z[45];
    z[12]=z[12] + n<T>(1,2)*z[45] + z[32];
    z[12]=z[8]*z[12];
    z[12]=z[13] + z[17] + z[19] + n<T>(1,2)*z[16] + z[12];
    z[12]=z[5]*z[12];
    z[13]=3*z[8];
    z[16]=z[7] + static_cast<T>(1)- z[6];
    z[17]= - static_cast<T>(1)+ z[15];
    z[17]=z[3]*z[17];
    z[17]=n<T>(3,2) + z[17];
    z[17]=z[3]*z[17];
    z[16]=z[13] + n<T>(1,2)*z[16] + z[17];
    z[16]=z[1]*z[16];
    z[17]= - n<T>(4,3)*z[26] - n<T>(3,4) + z[43];
    z[17]=z[3]*z[17];
    z[17]= - z[43] + z[17];
    z[17]=z[3]*z[17];
    z[19]=npow(z[7],2);
    z[32]=static_cast<T>(7)+ z[19];
    z[43]=n<T>(5,6) - z[19];
    z[43]=z[9]*z[43];
    z[44]= - 6*z[8] + n<T>(27,4) + z[44];
    z[44]=z[8]*z[44];
    z[16]=z[16] + z[44] + z[17] + n<T>(3,4)*z[32] + z[43];
    z[16]=z[1]*z[16];
    z[17]=n<T>(3,2)*z[7];
    z[32]=static_cast<T>(3)- z[7];
    z[32]=z[32]*z[17];
    z[43]=static_cast<T>(1)+ z[21];
    z[33]=z[43]*z[33];
    z[32]=z[33] + z[32] - static_cast<T>(1)- n<T>(9,2)*z[6];
    z[14]=z[14]*z[7];
    z[33]=static_cast<T>(1)+ z[20];
    z[33]=z[6]*z[33];
    z[43]= - static_cast<T>(1)- z[15];
    z[43]=z[3]*z[43];
    z[43]= - static_cast<T>(1)+ n<T>(2,3)*z[43];
    z[43]=z[3]*z[43];
    z[33]=z[43] + z[33] + z[14];
    z[33]=z[1]*z[33];
    z[32]=n<T>(1,2)*z[32] + z[33];
    z[32]=z[1]*z[32];
    z[32]=z[32] + 4*z[8] - n<T>(2,3) - z[21];
    z[32]=z[1]*z[32];
    z[33]=static_cast<T>(5)+ n<T>(3,2)*z[6];
    z[33]=z[6]*z[33];
    z[43]= - static_cast<T>(3)+ z[22];
    z[43]=z[7]*z[43];
    z[33]=z[43] - n<T>(11,2) + z[33];
    z[43]= - static_cast<T>(1)- z[6];
    z[43]=z[6]*z[43];
    z[14]=z[43] - z[14];
    z[43]=n<T>(1,3)*z[3];
    z[44]=n<T>(1,2) + z[43];
    z[44]=z[3]*z[44];
    z[44]=n<T>(1,2) + z[44];
    z[44]=z[3]*z[44];
    z[14]=n<T>(1,2)*z[14] + z[44];
    z[14]=z[1]*z[14];
    z[14]=z[14] + n<T>(1,2)*z[33] - z[36];
    z[14]=z[1]*z[14];
    z[33]=n<T>(3,2)*z[8] + n<T>(35,6) - z[39];
    z[14]=n<T>(1,2)*z[33] + z[14];
    z[14]=z[1]*z[14];
    z[33]=n<T>(5,3) + z[8];
    z[33]=z[8]*z[33];
    z[33]= - n<T>(7,3) + z[33];
    z[14]=n<T>(1,2)*z[33] + z[14];
    z[14]=z[1]*z[14];
    z[33]= - z[11]*z[18];
    z[27]=z[33] + z[27];
    z[27]=z[8]*z[27];
    z[27]= - z[29] + z[27];
    z[14]=n<T>(7,4)*z[27] + z[14];
    z[14]=z[4]*z[14];
    z[24]= - n<T>(13,2)*z[24] - n<T>(23,3) + 13*z[11];
    z[24]=z[8]*z[24];
    z[24]=z[24] + n<T>(23,3) - n<T>(13,2)*z[11];
    z[14]=z[14] + n<T>(1,4)*z[24] + z[32];
    z[14]=z[4]*z[14];
    z[24]= - static_cast<T>(3)- z[7];
    z[24]=z[24]*z[22];
    z[27]= - n<T>(5,3) + z[19];
    z[27]=z[9]*z[27];
    z[24]=z[27] + z[24] - static_cast<T>(3)- z[23];
    z[24]=z[9]*z[24];
    z[27]=7*z[11];
    z[29]= - static_cast<T>(5)- z[27];
    z[24]=n<T>(1,4)*z[29] + z[24];
    z[29]=n<T>(1,2)*z[9];
    z[32]=n<T>(1,2) - n<T>(5,3)*z[9];
    z[32]=z[32]*z[29];
    z[25]=z[25]*z[43];
    z[25]=z[32] + z[25];
    z[25]=z[3]*z[25];
    z[25]=z[25] + n<T>(1,4) + z[41];
    z[25]=z[3]*z[25];
    z[32]= - static_cast<T>(15)- n<T>(7,4)*z[11];
    z[33]= - static_cast<T>(4)- n<T>(1,8)*z[11];
    z[33]=z[9]*z[33];
    z[13]=z[13] + n<T>(1,2)*z[32] + z[33];
    z[13]=z[8]*z[13];
    z[32]=static_cast<T>(25)+ z[11];
    z[31]=n<T>(1,4)*z[32] + z[31];
    z[31]=z[9]*z[31];
    z[27]=n<T>(39,2) + z[27];
    z[13]=z[13] + n<T>(1,4)*z[27] + z[31];
    z[13]=z[8]*z[13];
    z[12]=z[12] + z[14] + z[16] + z[13] + n<T>(1,2)*z[24] + z[25];
    z[12]=z[5]*z[12];
    z[13]=npow(z[6],2);
    z[14]=z[13] + z[19];
    z[16]= - static_cast<T>(2)+ z[2];
    z[16]=z[16]*z[43];
    z[16]= - n<T>(1,4) + z[16];
    z[24]=npow(z[3],2);
    z[16]=z[16]*z[24];
    z[16]=n<T>(1,4)*z[14] + z[16];
    z[16]=z[1]*z[16];
    z[25]=z[14]*z[8];
    z[27]=n<T>(3,4)*z[25];
    z[31]= - static_cast<T>(7)- z[28];
    z[31]=z[31]*z[37];
    z[32]= - static_cast<T>(7)+ n<T>(11,2)*z[2];
    z[33]= - z[32]*z[43];
    z[33]=n<T>(3,2) + z[33];
    z[21]=z[33]*z[21];
    z[33]=n<T>(7,4) - z[7];
    z[33]=z[7]*z[33];
    z[16]=z[16] - z[27] + z[21] + z[31] + z[33];
    z[16]=z[1]*z[16];
    z[21]=z[43]*z[2];
    z[31]=n<T>(1,4) + z[21];
    z[33]=n<T>(1,2)*z[2];
    z[36]=z[33] - 1;
    z[37]=z[36]*z[2];
    z[37]=z[37] + n<T>(1,2);
    z[31]=z[3]*z[37]*z[31];
    z[31]=z[31] - static_cast<T>(1)+ n<T>(13,12)*z[2];
    z[31]=z[3]*z[31];
    z[39]= - static_cast<T>(2)+ z[22];
    z[39]=z[7]*z[39];
    z[27]=z[27] + 2*z[6] + z[39];
    z[27]=z[8]*z[27];
    z[28]= - n<T>(7,6) + z[28];
    z[16]=z[16] + z[27] + z[31] + n<T>(1,2)*z[28] - z[7];
    z[16]=z[1]*z[16];
    z[27]=n<T>(1,3)*z[2];
    z[28]= - z[32]*z[27];
    z[28]= - n<T>(1,2) + z[28];
    z[31]=n<T>(1,4)*z[2];
    z[28]=z[28]*z[31];
    z[32]=npow(z[2],2);
    z[39]=z[32]*z[43];
    z[37]= - z[37]*z[39];
    z[28]=z[28] + z[37];
    z[28]=z[3]*z[28];
    z[37]=n<T>(3,2) - z[2];
    z[37]=z[2]*z[37];
    z[37]= - n<T>(1,2) + z[37];
    z[28]=n<T>(1,2)*z[37] + z[28];
    z[28]=z[3]*z[28];
    z[37]= - static_cast<T>(3)+ z[6];
    z[37]=z[6]*z[37];
    z[34]= - z[25] + z[37] + z[34];
    z[34]=z[8]*z[34];
    z[34]= - n<T>(1,3) + n<T>(1,4)*z[34];
    z[34]=z[8]*z[34];
    z[37]=7*z[2];
    z[41]=n<T>(19,2) - z[37];
    z[16]=z[16] + z[34] + n<T>(1,6)*z[41] + z[28];
    z[16]=z[1]*z[16];
    z[28]=z[10] + n<T>(1,2);
    z[34]=z[2]*z[10];
    z[41]= - n<T>(1,2)*z[34] + z[28];
    z[41]=z[2]*z[41];
    z[44]=n<T>(1,2)*z[10];
    z[41]=z[41] - static_cast<T>(1)- z[44];
    z[41]=z[2]*z[41];
    z[41]=n<T>(1,2) + z[41];
    z[45]=npow(z[2],3);
    z[41]=z[41]*z[45]*z[43];
    z[46]=7*z[10];
    z[47]= - n<T>(11,2)*z[34] + n<T>(11,2) + z[46];
    z[47]=z[47]*z[27];
    z[44]=z[47] - n<T>(7,3) - z[44];
    z[44]=z[2]*z[44];
    z[44]=n<T>(1,2) + z[44];
    z[44]=z[44]*z[32];
    z[41]=n<T>(1,4)*z[44] + z[41];
    z[41]=z[3]*z[41];
    z[44]= - static_cast<T>(3)- z[10];
    z[47]= - n<T>(2,3)*z[34] + n<T>(2,3) + n<T>(3,4)*z[10];
    z[47]=z[2]*z[47];
    z[44]=n<T>(1,4)*z[44] + z[47];
    z[44]=z[2]*z[44];
    z[44]=n<T>(1,4) + z[44];
    z[44]=z[2]*z[44];
    z[41]=z[44] + z[41];
    z[41]=z[3]*z[41];
    z[44]= - 11*z[34] + static_cast<T>(11)+ 13*z[10];
    z[44]=z[44]*z[27];
    z[47]= - static_cast<T>(23)- z[46];
    z[47]=n<T>(1,3)*z[47] - z[11];
    z[44]=n<T>(1,2)*z[47] + z[44];
    z[44]=z[44]*z[33];
    z[44]=z[44] - n<T>(5,12) + z[11];
    z[46]=n<T>(23,2) + z[46];
    z[46]=n<T>(1,2)*z[46] - 5*z[34];
    z[46]=z[46]*z[27];
    z[47]=static_cast<T>(5)- n<T>(7,2)*z[34];
    z[47]=z[8]*z[47];
    z[46]=n<T>(1,6)*z[47] + z[46] - n<T>(7,6) - z[11];
    z[46]=z[46]*z[18];
    z[16]=z[16] + z[46] + n<T>(1,2)*z[44] + z[41];
    z[16]=z[4]*z[16];
    z[41]= - static_cast<T>(1)- n<T>(7,4)*z[10];
    z[44]=z[8]*z[10];
    z[38]=n<T>(7,12)*z[44] + n<T>(9,4)*z[34] + n<T>(1,3)*z[41] - z[38];
    z[38]=z[38]*z[18];
    z[41]=static_cast<T>(1)- z[27];
    z[15]=z[41]*z[15];
    z[15]=n<T>(1,3) + z[15];
    z[15]=z[15]*z[24];
    z[14]= - n<T>(1,2)*z[14] + z[15];
    z[14]=z[1]*z[14];
    z[15]= - n<T>(23,2) + z[37];
    z[15]=z[3]*z[15];
    z[15]= - n<T>(13,2) + z[15];
    z[15]=z[15]*z[43];
    z[24]= - static_cast<T>(9)+ 5*z[7];
    z[24]=z[7]*z[24];
    z[24]=9*z[6] + z[24];
    z[15]=n<T>(1,2)*z[24] + z[15];
    z[14]=z[14] + n<T>(1,2)*z[15] + z[25];
    z[14]=z[1]*z[14];
    z[15]=static_cast<T>(5)- z[7];
    z[15]=z[15]*z[22];
    z[22]= - n<T>(5,2) + z[6];
    z[22]=z[6]*z[22];
    z[15]= - z[25] + z[22] + z[15];
    z[15]=z[15]*z[18];
    z[18]= - static_cast<T>(1)+ z[2];
    z[18]=z[18]*z[21];
    z[21]= - static_cast<T>(1)+ n<T>(7,3)*z[2];
    z[18]=n<T>(1,4)*z[21] + z[18];
    z[18]=z[3]*z[18];
    z[18]=z[18] - z[36];
    z[18]=z[3]*z[18];
    z[14]=z[14] + z[15] + z[18] + z[35];
    z[14]=z[1]*z[14];
    z[15]=n<T>(35,12)*z[34] - n<T>(5,3)*z[28] - z[23];
    z[15]=z[15]*z[33];
    z[18]=z[34] - z[28];
    z[18]=z[18]*z[32];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[18]*z[39];
    z[21]=5*z[10];
    z[22]= - n<T>(7,2) - z[21];
    z[22]=n<T>(1,3)*z[22] + 3*z[34];
    z[22]=z[22]*z[33];
    z[22]= - n<T>(1,3) + z[22];
    z[22]=z[2]*z[22];
    z[22]=n<T>(1,4) + z[22];
    z[22]=z[22]*z[33];
    z[18]=z[22] + z[18];
    z[18]=z[3]*z[18];
    z[21]=n<T>(25,3)*z[34] - static_cast<T>(3)- z[21];
    z[21]=z[2]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[33];
    z[21]=static_cast<T>(1)+ z[21];
    z[18]=n<T>(1,4)*z[21] + z[18];
    z[18]=z[3]*z[18];
    z[14]=z[16] + z[14] + z[38] + z[18] + z[15] - static_cast<T>(1)+ z[30];
    z[14]=z[4]*z[14];
    z[15]=n<T>(1,2) + z[2];
    z[16]= - static_cast<T>(2)+ z[27];
    z[16]=z[3]*z[16];
    z[16]=n<T>(1,12) + z[16];
    z[16]=z[3]*z[16];
    z[15]=n<T>(1,3)*z[15] + z[16];
    z[15]=z[3]*z[15];
    z[13]=n<T>(1,4)*z[13];
    z[16]=n<T>(1,2)*z[19];
    z[15]=z[15] + z[16] + static_cast<T>(2)+ z[13];
    z[15]=z[1]*z[15];
    z[18]=n<T>(2,3)*z[26] - n<T>(1,6)*z[9] + n<T>(1,3) - z[31];
    z[18]=z[3]*z[18];
    z[21]= - z[29] + static_cast<T>(4)+ n<T>(5,4)*z[2];
    z[18]=n<T>(1,3)*z[21] + z[18];
    z[18]=z[3]*z[18];
    z[13]= - n<T>(3,4)*z[19] - static_cast<T>(4)- z[13];
    z[13]=z[8]*z[13];
    z[21]=z[16] - n<T>(2,3);
    z[21]=z[21]*z[9];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(25,6) + z[20];
    z[13]=z[15] + z[13] + z[18] - z[21] - n<T>(2,3)*z[2] + n<T>(1,2)*z[20] + 
    z[40];
    z[13]=z[1]*z[13];
    z[15]= - static_cast<T>(19)- n<T>(7,3)*z[10];
    z[15]=n<T>(1,2)*z[15] - z[11];
    z[15]=n<T>(1,2)*z[15] - z[40];
    z[18]=static_cast<T>(2)+ n<T>(1,4)*z[19];
    z[18]=z[8]*z[18];
    z[15]=z[18] + z[21] + n<T>(1,2)*z[15] + z[27];
    z[15]=z[8]*z[15];
    z[18]=n<T>(1,6)*z[2];
    z[19]= - static_cast<T>(5)- n<T>(13,2)*z[10];
    z[19]=z[19]*z[18];
    z[17]= - static_cast<T>(1)- z[17];
    z[17]=z[7]*z[17];
    z[16]=z[9]*z[16];
    z[16]=z[16] + n<T>(5,2) + z[17];
    z[16]=z[9]*z[16];
    z[16]=z[16] + z[19] + z[42] + z[7];
    z[17]=z[10]*z[32];
    z[17]= - static_cast<T>(1)- n<T>(7,8)*z[17];
    z[17]=z[17]*z[27];
    z[19]= - z[10]*z[45];
    z[19]=static_cast<T>(1)+ z[19];
    z[18]=z[3]*z[19]*z[18];
    z[17]=z[18] + n<T>(5,12)*z[9] + n<T>(1,8) + z[17];
    z[17]=z[3]*z[17];
    z[18]=static_cast<T>(1)- n<T>(3,2)*z[34];
    z[18]=z[18]*z[31];
    z[17]=z[17] + z[18] - z[9];
    z[17]=z[3]*z[17];

    r += z[12] + z[13] + z[14] + z[15] + n<T>(1,2)*z[16] + z[17];
 
    return r;
}

template double qqb_2lha_r277(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r277(const std::array<dd_real,30>&);
#endif
