#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2318(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[10];
    z[9]=n<T>(1,2)*z[7];
    z[10]=z[9] - 1;
    z[11]=n<T>(1,2)*z[3];
    z[12]=z[10] + z[11];
    z[13]=n<T>(1,2)*z[6];
    z[12]=z[12]*z[13];
    z[14]=n<T>(1,3)*z[7];
    z[15]=n<T>(1,4)*z[7];
    z[16]= - static_cast<T>(2)+ z[15];
    z[16]=z[16]*z[14];
    z[17]=n<T>(13,6) - z[3];
    z[16]=z[12] + n<T>(1,2)*z[17] + z[16];
    z[16]=z[6]*z[16];
    z[17]= - static_cast<T>(3)+ z[9];
    z[17]=z[17]*z[9];
    z[18]=static_cast<T>(5)- z[7];
    z[18]=z[18]*z[14];
    z[18]=z[18] - n<T>(7,3) + z[3];
    z[19]=n<T>(1,4)*z[4];
    z[18]=z[18]*z[19];
    z[20]=z[3] - n<T>(9,4);
    z[17]=z[18] + z[17] - z[20];
    z[17]=z[4]*z[17];
    z[18]=n<T>(1,6)*z[7];
    z[21]=n<T>(13,2) - z[7];
    z[21]=z[21]*z[18];
    z[22]=n<T>(3,4)*z[3];
    z[16]=z[17] + z[16] + z[21] - n<T>(5,3) + z[22];
    z[16]=z[2]*z[16];
    z[17]=static_cast<T>(11)- z[15];
    z[17]=z[17]*z[14];
    z[21]=n<T>(5,4)*z[7];
    z[20]= - z[21] - z[20];
    z[20]=z[6]*z[20];
    z[23]=n<T>(7,2)*z[7];
    z[24]= - z[23] + n<T>(9,2) - z[3];
    z[25]=n<T>(9,2)*z[7] - n<T>(11,2) + z[3];
    z[25]=z[4]*z[25];
    z[24]=3*z[24] + z[25];
    z[25]=n<T>(1,2)*z[4];
    z[24]=z[24]*z[25];
    z[26]=3*z[3];
    z[27]= - n<T>(61,6) + z[26];
    z[16]=z[16] + z[24] + z[20] + n<T>(1,2)*z[27] + z[17];
    z[16]=z[2]*z[16];
    z[17]=n<T>(7,3)*z[7];
    z[20]= - z[17] + n<T>(35,6) - z[26];
    z[20]=z[4]*z[20];
    z[24]=z[17] - n<T>(43,3) + 13*z[3];
    z[20]=n<T>(1,2)*z[24] + z[20];
    z[20]=z[20]*z[25];
    z[24]=n<T>(1,4)*z[3];
    z[27]=static_cast<T>(1)+ z[24];
    z[27]=z[6]*z[27];
    z[16]=z[16] + z[20] + z[27] + n<T>(19,12)*z[7] - n<T>(13,12) - z[3];
    z[16]=z[2]*z[16];
    z[20]=n<T>(1,4)*z[5];
    z[27]= - static_cast<T>(3)+ n<T>(7,3)*z[5];
    z[27]=z[27]*z[20];
    z[28]= - n<T>(5,6) - z[5];
    z[28]=z[5]*z[28];
    z[28]=z[3] + z[28];
    z[29]=n<T>(11,2) - z[5];
    z[29]=z[5]*z[29];
    z[29]=z[29] - n<T>(1,6) - z[3];
    z[29]=z[4]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[28]=z[4]*z[28];
    z[27]=z[27] + z[28];
    z[28]=2*z[3];
    z[29]=n<T>(7,4)*z[5];
    z[30]= - z[29] + static_cast<T>(1)- n<T>(5,4)*z[3];
    z[30]=z[4]*z[30];
    z[30]=z[30] + n<T>(25,12)*z[5] - n<T>(5,12) + z[28];
    z[30]=z[4]*z[30];
    z[31]= - n<T>(5,2) + z[26];
    z[31]=z[31]*z[25];
    z[32]=n<T>(3,4) - z[3];
    z[31]=3*z[32] + z[31];
    z[31]=z[4]*z[31];
    z[31]=z[31] - static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[31]=z[2]*z[31];
    z[30]=z[31] + z[30] - n<T>(11,12)*z[5] - n<T>(1,3) - z[22];
    z[30]=z[2]*z[30];
    z[27]=n<T>(1,2)*z[27] + z[30];
    z[27]=z[2]*z[27];
    z[30]=n<T>(1,2)*z[5];
    z[31]=static_cast<T>(1)+ z[30];
    z[31]=z[5]*z[31];
    z[31]=z[26] + n<T>(5,3)*z[31];
    z[32]=static_cast<T>(1)- z[5];
    z[32]=z[32]*z[30];
    z[32]= - z[3] + z[32];
    z[32]=z[4]*z[32];
    z[33]=z[3] - n<T>(1,12)*z[5];
    z[33]=z[4]*z[33];
    z[33]= - z[3] + z[33];
    z[33]=z[2]*z[33];
    z[31]=z[33] + n<T>(1,4)*z[31] + z[32];
    z[31]=z[2]*z[31];
    z[32]=z[4]*z[5];
    z[33]= - n<T>(5,12) + z[5];
    z[33]=z[33]*z[32];
    z[34]=npow(z[5],2);
    z[31]=z[31] - n<T>(5,24)*z[34] + z[33];
    z[31]=z[2]*z[4]*z[31];
    z[33]=n<T>(1,2)*z[2];
    z[35]=n<T>(1,3)*z[5];
    z[36]=static_cast<T>(1)- z[35];
    z[36]=z[5]*z[36];
    z[36]=z[3] + z[36];
    z[36]=z[36]*z[33];
    z[37]= - n<T>(1,2) + z[35];
    z[37]=z[5]*z[37];
    z[36]=z[36] - z[11] + z[37];
    z[36]=z[2]*z[36]*npow(z[4],2);
    z[32]=npow(z[32],2);
    z[36]= - n<T>(1,6)*z[32] + z[36];
    z[33]=z[1]*z[36]*z[33];
    z[31]=z[33] - n<T>(1,2)*z[32] + z[31];
    z[31]=z[1]*z[31];
    z[32]=z[6] - 1;
    z[33]= - z[32]*z[30];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[5]*z[33];
    z[33]=z[24] + z[33];
    z[33]=z[4]*z[33];
    z[33]=n<T>(3,4)*z[34] + z[33];
    z[33]=z[4]*z[33];
    z[27]=z[31] + z[33] + z[27];
    z[27]=z[1]*z[27];
    z[31]=z[14] + n<T>(97,6) + z[26];
    z[33]= - z[18] - n<T>(7,3) - z[24];
    z[33]=z[4]*z[33];
    z[31]=n<T>(1,4)*z[31] + z[33];
    z[31]=z[4]*z[31];
    z[33]= - z[9] + n<T>(3,2) - z[3];
    z[34]=n<T>(7,12)*z[7] - n<T>(19,12) + z[3];
    z[34]=z[4]*z[34];
    z[33]=3*z[33] + z[34];
    z[33]=z[4]*z[33];
    z[34]= - z[14] + n<T>(4,3) - z[3];
    z[34]=z[6]*z[34];
    z[33]=z[33] + z[34] + n<T>(11,12)*z[7] - n<T>(35,12) + z[28];
    z[33]=z[2]*z[33];
    z[34]=n<T>(5,2) + z[3];
    z[34]=z[34]*z[13];
    z[36]=n<T>(5,6)*z[7];
    z[34]=z[34] + z[36] - n<T>(73,12) + z[3];
    z[31]=z[33] + n<T>(1,2)*z[34] + z[31];
    z[31]=z[2]*z[31];
    z[33]= - 3*z[5] + static_cast<T>(7)- z[26];
    z[25]=z[33]*z[25];
    z[25]=z[25] + z[29] - n<T>(5,4) + z[28];
    z[25]=z[4]*z[25];
    z[28]= - static_cast<T>(5)+ n<T>(3,2)*z[6];
    z[28]=z[5]*z[28];
    z[28]=z[28] - n<T>(11,2) - z[26];
    z[25]=z[31] + n<T>(1,4)*z[28] + z[25];
    z[25]=z[2]*z[25];
    z[28]=z[5]*z[6];
    z[29]=static_cast<T>(3)+ n<T>(19,6)*z[28];
    z[29]=z[29]*z[30];
    z[31]= - n<T>(7,3) + z[26];
    z[33]=n<T>(1,6)*z[5];
    z[34]= - npow(z[6],2)*z[33];
    z[34]=z[34] + static_cast<T>(3)- z[6];
    z[34]=z[5]*z[34];
    z[31]=n<T>(1,2)*z[31] + z[34];
    z[31]=z[4]*z[31];
    z[29]=z[31] - z[3] + z[29];
    z[29]=z[4]*z[29];
    z[31]= - static_cast<T>(9)- n<T>(11,3)*z[5];
    z[20]=z[31]*z[20];
    z[20]=z[20] + z[29];
    z[20]=z[27] + n<T>(1,2)*z[20] + z[25];
    z[20]=z[1]*z[20];
    z[25]=n<T>(3,4) + n<T>(1,3)*z[28];
    z[27]=z[6] + 1;
    z[25]=z[30]*z[27]*z[25];
    z[28]=n<T>(1,6)*z[28];
    z[29]= - z[32]*z[28];
    z[29]=z[29] + n<T>(1,6)*z[6] - n<T>(1,3) + z[22];
    z[29]=z[4]*z[29];
    z[25]=z[29] + z[25] + n<T>(2,3) - z[3];
    z[25]=z[4]*z[25];
    z[29]=z[7] - 1;
    z[30]=n<T>(7,4)*z[29] - z[6];
    z[30]=z[30]*z[35];
    z[31]= - n<T>(43,3) + 9*z[7];
    z[31]=n<T>(1,2)*z[31] + 3*z[6];
    z[30]=n<T>(1,2)*z[31] + z[30];
    z[30]=z[5]*z[30];
    z[30]=n<T>(5,12) + z[30];
    z[16]=z[20] + z[16] + n<T>(1,2)*z[30] + z[25];
    z[16]=z[1]*z[16];
    z[20]= - static_cast<T>(5)+ n<T>(7,4)*z[7];
    z[20]=z[20]*z[14];
    z[25]=n<T>(19,6) - z[3];
    z[12]=z[12] + n<T>(1,2)*z[25] + z[20];
    z[12]=z[6]*z[12];
    z[20]= - static_cast<T>(9)+ z[23];
    z[20]=z[20]*z[9];
    z[25]=7*z[7];
    z[30]=static_cast<T>(17)- z[25];
    z[30]=z[30]*z[14];
    z[30]=z[30] - n<T>(13,3) + z[3];
    z[19]=z[30]*z[19];
    z[19]=z[19] + z[20] + n<T>(15,4) - z[3];
    z[19]=z[4]*z[19];
    z[20]=n<T>(37,2) - z[25];
    z[20]=z[20]*z[18];
    z[12]=z[19] + z[12] + z[20] - n<T>(8,3) + z[22];
    z[12]=z[2]*z[12];
    z[19]=z[23] - n<T>(13,2) + z[26];
    z[20]= - z[36] + n<T>(4,3) - z[11];
    z[20]=z[4]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[4]*z[19];
    z[17]=static_cast<T>(3)- z[17];
    z[15]=z[17]*z[15];
    z[17]=z[6]*z[29];
    z[11]=z[12] + z[19] - n<T>(5,12)*z[17] + z[15] + n<T>(1,3) - z[11];
    z[11]=z[2]*z[11];
    z[9]=z[9]*z[8];
    z[12]=z[9] + n<T>(1,2) + 2*z[8];
    z[12]=z[12]*z[14];
    z[15]=z[6]*z[8];
    z[9]=n<T>(1,3)*z[15] + z[9] + n<T>(1,3) + n<T>(1,2)*z[8];
    z[9]=z[6]*z[9];
    z[17]=static_cast<T>(1)+ n<T>(1,3)*z[8];
    z[9]=z[9] + n<T>(1,2)*z[17] + z[12];
    z[9]=z[6]*z[9];
    z[12]=static_cast<T>(1)+ z[7];
    z[12]=z[8]*z[12];
    z[12]=static_cast<T>(1)+ z[12];
    z[17]=z[12]*z[18];
    z[9]=z[9] + z[17] - n<T>(1,3) + z[24];
    z[9]=z[4]*z[9];
    z[13]= - z[8]*z[13];
    z[12]= - n<T>(1,3)*z[12] + z[13];
    z[12]=z[6]*z[12];
    z[13]=z[27]*z[28];
    z[14]= - z[8]*z[14];
    z[14]=z[14] + n<T>(23,12) - z[3];
    z[9]=z[9] + z[13] + n<T>(1,2)*z[14] + z[12];
    z[9]=z[4]*z[9];
    z[10]= - z[7]*z[10];
    z[10]= - n<T>(1,2) + z[10];
    z[10]=z[10]*z[33];
    z[12]=n<T>(2,3) - n<T>(3,8)*z[7];
    z[12]=z[7]*z[12];
    z[13]=n<T>(5,3) - 3*z[7];
    z[13]=z[6]*z[13];
    z[10]=z[10] + n<T>(1,8)*z[13] - n<T>(7,24) + z[12];
    z[10]=z[5]*z[10];
    z[12]=z[15] - static_cast<T>(1)- z[21];

    r += z[9] + z[10] + z[11] + n<T>(1,6)*z[12] + z[16];
 
    return r;
}

template double qqb_2lha_r2318(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2318(const std::array<dd_real,30>&);
#endif
