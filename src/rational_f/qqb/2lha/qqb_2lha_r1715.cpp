#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1715(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=2*z[1];
    z[7]=3*z[2];
    z[8]= - static_cast<T>(1)- z[7];
    z[8]=z[8]*z[6];
    z[9]=2*z[4];
    z[8]=z[9] + z[8];
    z[10]=npow(z[2],2);
    z[8]=z[10]*z[8];
    z[11]=2*z[2];
    z[12]=z[11] - 1;
    z[13]= - z[12]*z[7];
    z[8]=z[13] + z[8];
    z[8]=z[4]*z[8];
    z[13]=6*z[2];
    z[14]=z[2] - 1;
    z[15]=z[13]*z[14];
    z[15]=z[15] + 1;
    z[16]=4*z[2];
    z[17]= - static_cast<T>(2)+ z[7];
    z[17]=z[17]*z[16];
    z[17]= - static_cast<T>(3)+ z[17];
    z[18]=z[1]*z[2];
    z[17]=z[17]*z[18];
    z[8]=z[8] + z[17] + z[15];
    z[8]=z[4]*z[8];
    z[17]=z[6]*z[10];
    z[19]=static_cast<T>(1)- z[7];
    z[19]=z[19]*z[17];
    z[20]= - static_cast<T>(5)- z[11];
    z[20]=z[2]*z[20];
    z[19]=z[20] + z[19];
    z[19]=z[1]*z[19];
    z[17]= - z[4]*z[17];
    z[17]=z[17] - static_cast<T>(3)+ z[19];
    z[17]=z[4]*z[17];
    z[19]=z[18] + 1;
    z[20]=z[2] - 2;
    z[20]=z[20]*z[2];
    z[20]=z[20] + 1;
    z[20]= - npow(z[1],2)*z[20]*z[19];
    z[6]= - z[6]*z[14];
    z[6]=z[6] - z[4];
    z[6]=z[4]*z[19]*z[6];
    z[6]=z[20] + z[6];
    z[6]=z[3]*z[6];
    z[13]= - static_cast<T>(11)+ z[13];
    z[13]=z[2]*z[13];
    z[13]=static_cast<T>(5)+ z[13];
    z[13]=z[13]*z[18];
    z[14]= - static_cast<T>(7)+ z[16];
    z[14]=z[2]*z[14];
    z[13]=z[13] + static_cast<T>(3)+ z[14];
    z[13]=z[1]*z[13];
    z[6]=2*z[6] + z[13] + z[17];
    z[6]=z[3]*z[6];
    z[13]= - z[2]*z[15];
    z[10]=z[12]*z[10];
    z[9]= - npow(z[2],3)*z[9];
    z[9]=3*z[10] + z[9];
    z[9]=z[4]*z[9];
    z[9]=z[13] + z[9];
    z[9]=z[4]*z[9];
    z[10]=z[11] - 3;
    z[10]=z[10]*z[2];
    z[12]=static_cast<T>(1)+ z[10];
    z[12]=z[2]*z[12];
    z[9]=z[12] + z[9];
    z[9]=z[5]*z[9];
    z[7]=static_cast<T>(5)- z[7];
    z[7]=z[2]*z[7];
    z[7]= - static_cast<T>(2)+ z[7];
    z[7]=z[7]*z[11];
    z[7]= - static_cast<T>(1)+ z[7];
    z[7]=z[1]*z[7];

    r +=  - static_cast<T>(2)+ z[6] + z[7] + z[8] + z[9] - z[10];
 
    return r;
}

template double qqb_2lha_r1715(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1715(const std::array<dd_real,30>&);
#endif
