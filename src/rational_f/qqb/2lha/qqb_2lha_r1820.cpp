#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1820(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[10];
    z[6]= - n<T>(329,6) + 5*z[4];
    z[7]=z[5]*z[6];
    z[7]=static_cast<T>(5)+ z[7];
    z[8]=n<T>(1,2)*z[5];
    z[9]=npow(z[4],2);
    z[7]=z[8]*z[9]*z[7];
    z[10]=z[5]*z[4];
    z[11]=z[6]*z[10];
    z[11]= - n<T>(329,6)*z[4] + z[11];
    z[11]=z[5]*z[11];
    z[6]=z[6]*z[8];
    z[6]= - n<T>(172,3) + z[6];
    z[6]=z[5]*z[6];
    z[8]= - n<T>(389,6) - 5*z[1];
    z[6]=n<T>(5,2)*z[2] + n<T>(1,2)*z[8] + z[6];
    z[6]=z[2]*z[6];
    z[6]=z[11] + z[6];
    z[6]=z[2]*z[6];
    z[8]= - static_cast<T>(1)- z[5];
    z[8]=z[5]*z[9]*z[8];
    z[9]= - z[4] - n<T>(1,2)*z[10];
    z[9]=z[5]*z[9];
    z[9]=z[9] - n<T>(1,2)*z[4];
    z[11]= - static_cast<T>(3)- z[5];
    z[11]=z[5]*z[11];
    z[11]=z[2] + z[11] + z[4] - static_cast<T>(3)- z[1];
    z[11]=z[2]*z[11];
    z[9]=3*z[9] + n<T>(1,2)*z[11];
    z[9]=z[2]*z[9];
    z[8]=n<T>(3,2)*z[8] + z[9];
    z[8]=z[2]*z[8];
    z[9]=npow(z[4],3)*npow(z[5],2);
    z[8]= - n<T>(1,2)*z[9] + z[8];
    z[8]=z[3]*z[8];
    z[6]=5*z[8] + z[7] + z[6];
    z[6]=z[3]*z[6];
    z[7]= - static_cast<T>(299)+ 329*z[4];
    z[8]=z[7]*z[10];
    z[8]= - n<T>(239,2)*z[4] + z[8];
    z[9]=n<T>(1,3)*z[5];
    z[8]=z[8]*z[9];
    z[7]=z[5]*z[7];
    z[7]= - n<T>(1495,2) + z[7];
    z[7]=z[7]*z[9];
    z[7]= - n<T>(299,2) + z[7];
    z[7]=z[2]*z[7];
    z[7]=z[8] + z[7];
    z[6]=n<T>(1,4)*z[7] + z[6];
    z[6]=z[3]*z[6];
    z[7]= - n<T>(1,2) + z[10];
    z[7]=z[5]*z[7];
    z[7]= - n<T>(1,2) + z[7];
    z[6]=n<T>(299,12)*z[7] + z[6];

    r += z[6]*z[3];
 
    return r;
}

template double qqb_2lha_r1820(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1820(const std::array<dd_real,30>&);
#endif
