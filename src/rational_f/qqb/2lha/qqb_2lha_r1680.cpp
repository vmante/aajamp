#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1680(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=npow(z[5],2);
    z[6]=n<T>(1,3)*z[6];
    z[7]= - static_cast<T>(1)+ 4*z[2];
    z[7]=z[7]*z[6];
    z[8]=n<T>(1,3)*z[1];
    z[9]=z[2]*z[3];
    z[10]=z[3] - z[9];
    z[10]=z[10]*z[8];
    z[10]=z[10] - static_cast<T>(1)+ n<T>(4,3)*z[2];
    z[10]=z[1]*z[10];
    z[11]=static_cast<T>(1)- 2*z[2];
    z[11]=z[5]*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[1]*z[10];
    z[7]=z[7] + z[10];
    z[7]=z[1]*z[7];
    z[10]=z[2]*npow(z[5],3);
    z[7]= - n<T>(1,3)*z[10] + z[7];
    z[7]=z[4]*z[7];
    z[10]=n<T>(2,3)*z[5];
    z[11]=z[10] - z[2];
    z[12]=n<T>(2,3) + z[11];
    z[6]=z[12]*z[6];
    z[12]= - static_cast<T>(2)- 5*z[3];
    z[12]=n<T>(1,3)*z[12] + z[9];
    z[12]=z[12]*z[8];
    z[11]=z[12] + n<T>(4,3) + z[11];
    z[11]=z[1]*z[11];
    z[10]=z[10] + 1;
    z[12]=z[2] - z[10];
    z[12]=z[5]*z[12];
    z[11]=z[12] + z[11];
    z[11]=z[1]*z[11];
    z[6]=n<T>(2,3)*z[7] + z[6] + z[11];
    z[6]=z[4]*z[6];
    z[7]= - z[9] + static_cast<T>(1)+ 2*z[3];
    z[7]=z[7]*z[8];
    z[7]=z[7] + n<T>(2,3)*z[2] - z[10];
    z[7]=z[1]*z[7];
    z[8]=z[5] + 1;
    z[9]= - z[2] + z[8];
    z[9]=z[5]*z[9];
    z[6]=z[6] + n<T>(1,3)*z[9] + z[7];
    z[6]=z[4]*z[6];
    z[7]= - static_cast<T>(1)- z[3];
    z[7]=z[1]*z[7];
    z[7]=z[7] + z[8];

    r += z[6] + n<T>(1,3)*z[7];
 
    return r;
}

template double qqb_2lha_r1680(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1680(const std::array<dd_real,30>&);
#endif
