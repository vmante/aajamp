#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r659(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[6];
    z[7]=k[9];
    z[8]=k[4];
    z[9]=z[3]*z[7];
    z[10]=z[7] - 1;
    z[11]=z[9]*z[10];
    z[12]=n<T>(1,4)*z[7];
    z[13]=static_cast<T>(11)- 17*z[7];
    z[13]=z[13]*z[12];
    z[13]=z[13] + 3*z[11];
    z[13]=z[3]*z[13];
    z[14]=npow(z[7],2);
    z[13]=z[14] + z[13];
    z[13]=z[3]*z[13];
    z[15]=z[14]*z[3];
    z[16]= - z[15] + n<T>(3,2)*z[14];
    z[17]=npow(z[3],2);
    z[16]=z[16]*z[17];
    z[18]=n<T>(1,2)*z[14];
    z[16]=z[16] + z[18];
    z[16]=z[16]*z[8];
    z[19]=z[7] - 3;
    z[20]= - z[19]*z[12];
    z[13]= - z[16] + z[20] + z[13];
    z[20]=n<T>(1,4)*z[8];
    z[13]=z[13]*z[20];
    z[21]=z[7] - n<T>(1,2);
    z[22]=z[21]*z[12];
    z[23]=n<T>(5,4) - z[7];
    z[23]=z[7]*z[23];
    z[24]=n<T>(1,2)*z[7];
    z[25]=z[24] - 1;
    z[25]=z[25]*z[7];
    z[26]=z[25] + n<T>(1,2);
    z[27]=z[3]*z[26];
    z[23]=n<T>(3,2)*z[27] - n<T>(1,4) + z[23];
    z[23]=z[3]*z[23];
    z[22]=z[22] + z[23];
    z[22]=z[3]*z[22];
    z[23]=z[7] + 1;
    z[27]=n<T>(1,8)*z[7];
    z[28]=z[23]*z[27];
    z[13]=z[13] + z[28] + z[22];
    z[13]=z[8]*z[13];
    z[22]=static_cast<T>(1)- 3*z[7];
    z[22]=z[22]*z[12];
    z[28]=z[21]*z[7];
    z[11]=z[28] - n<T>(3,4)*z[11];
    z[11]=z[3]*z[11];
    z[15]=z[15] - z[14];
    z[15]=z[15]*z[3];
    z[15]=z[15] + z[14];
    z[20]= - z[15]*z[20];
    z[11]=z[20] + z[22] + z[11];
    z[11]=z[8]*z[11];
    z[20]=3*z[3];
    z[20]=z[26]*z[20];
    z[22]= - static_cast<T>(3)+ n<T>(5,2)*z[7];
    z[22]=z[7]*z[22];
    z[22]= - z[20] + n<T>(1,2) + z[22];
    z[22]=z[3]*z[22];
    z[29]=static_cast<T>(1)- n<T>(3,2)*z[7];
    z[29]=z[7]*z[29];
    z[22]=z[29] + z[22];
    z[11]=n<T>(1,2)*z[22] + z[11];
    z[11]=z[8]*z[11];
    z[22]= - z[10]*z[12];
    z[29]=z[19]*z[24];
    z[30]=z[29] + 1;
    z[31]=n<T>(1,2)*z[3];
    z[31]= - z[30]*z[31];
    z[31]=z[31] + n<T>(1,4) + z[25];
    z[31]=z[3]*z[31];
    z[11]=z[11] + z[22] + z[31];
    z[22]=n<T>(1,4)*z[6];
    z[11]=z[11]*z[22];
    z[30]=z[30]*z[3];
    z[31]=5*z[7];
    z[32]=static_cast<T>(9)- z[31];
    z[32]=z[32]*z[27];
    z[32]=z[32] + z[30];
    z[32]=z[3]*z[32];
    z[21]=n<T>(1,4)*z[21] + z[32];
    z[21]=z[3]*z[21];
    z[27]=z[10]*z[27];
    z[21]=z[27] + z[21];
    z[11]=z[11] + n<T>(1,2)*z[21] + z[13];
    z[11]=z[6]*z[11];
    z[13]=static_cast<T>(3)- z[31];
    z[13]=z[13]*z[12];
    z[21]=static_cast<T>(7)- z[7];
    z[12]=z[3]*z[21]*z[12];
    z[12]= - z[14] + z[12];
    z[12]=z[3]*z[12];
    z[12]= - z[16] + z[13] + z[12];
    z[12]=z[8]*z[12];
    z[13]=z[7] - n<T>(3,2);
    z[9]= - z[13]*z[9];
    z[9]=z[12] - z[29] + z[9];
    z[9]=z[8]*z[9];
    z[12]= - z[3]*z[23]*z[24];
    z[16]=n<T>(1,2)*z[8];
    z[15]= - z[15]*z[16];
    z[12]=z[15] + z[7] + z[12];
    z[12]=z[8]*z[12];
    z[12]= - n<T>(1,2) + z[12];
    z[12]=z[5]*z[12]*z[16];
    z[9]=z[12] + n<T>(1,2)*z[10] + z[9];
    z[9]=z[5]*z[9];
    z[10]= - z[10]*z[24];
    z[12]= - z[20] - n<T>(1,2) + z[28];
    z[12]=z[3]*z[12];
    z[10]=z[10] + z[12];
    z[10]=z[10]*z[16];
    z[12]= - n<T>(1,2) + 3*z[25];
    z[12]=n<T>(1,4)*z[12] - z[30];
    z[12]=z[3]*z[12];
    z[10]=z[10] - n<T>(1,4)*z[26] + z[12];
    z[10]=z[3]*z[10];
    z[12]=z[14]*z[8];
    z[14]=static_cast<T>(1)+ z[16];
    z[14]=z[14]*z[12];
    z[14]=z[18] + z[14];
    z[14]=z[8]*z[14]*npow(z[6],2);
    z[15]=z[7]*z[19];
    z[12]=z[15] + z[12];
    z[12]=z[12]*z[16];
    z[12]=z[12] - z[13];
    z[12]=z[5]*z[8]*z[12];
    z[12]=n<T>(1,2) + z[12];
    z[12]=z[5]*z[12];
    z[13]=z[4]*npow(z[5],2);
    z[12]=n<T>(1,2)*z[13] + z[14] + z[12];
    z[14]=n<T>(1,8)*z[4];
    z[12]=z[12]*z[14];
    z[14]=z[22] - n<T>(1,4) - z[3];
    z[14]=z[6]*z[17]*z[14];
    z[13]=z[13] + z[5];
    z[15]=npow(z[3],3);
    z[13]=z[15] + n<T>(1,2)*z[14] - n<T>(1,8)*z[13];
    z[14]=n<T>(1,2)*z[1];
    z[13]=z[13]*z[14];
    z[16]=n<T>(1,2) + z[30];
    z[16]=z[16]*z[17];
    z[14]= - z[15]*z[14];
    z[14]=z[16] + z[14];
    z[14]=z[2]*z[14];
    z[9]=n<T>(1,2)*z[14] + z[13] + z[12] + n<T>(1,4)*z[9] + z[11] + z[10];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qqb_2lha_r659(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r659(const std::array<dd_real,30>&);
#endif
