#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1121(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[9];
    z[8]=k[11];
    z[9]=k[10];
    z[10]=k[17];
    z[11]=static_cast<T>(1)+ n<T>(1,4)*z[9];
    z[12]=5*z[9];
    z[11]=z[11]*z[12];
    z[13]=npow(z[4],2);
    z[14]= - n<T>(5,2) + z[7];
    z[14]=z[14]*z[13];
    z[15]=z[5]*npow(z[9],2);
    z[15]=z[15] + z[9];
    z[16]=z[13]*z[7];
    z[15]=z[16] + n<T>(5,2)*z[15];
    z[15]=z[5]*z[15];
    z[14]=z[15] + z[11] + z[14];
    z[15]=n<T>(1,2)*z[5];
    z[14]=z[14]*z[15];
    z[17]=z[9] + 3;
    z[18]=n<T>(5,4)*z[9];
    z[17]=z[17]*z[18];
    z[18]=z[4] - n<T>(17,4) + z[11];
    z[18]=z[7]*z[18];
    z[18]= - z[17] + z[18];
    z[19]=npow(z[2],2);
    z[20]=z[19]*z[7];
    z[14]=z[14] + n<T>(1,2)*z[18] - z[20];
    z[11]=n<T>(13,4) - z[11];
    z[11]=z[7]*z[11];
    z[11]=z[17] + z[11];
    z[11]=n<T>(1,2)*z[11] + z[20];
    z[17]=n<T>(1,2)*z[2];
    z[11]=z[11]*z[17];
    z[18]=static_cast<T>(1)+ z[9];
    z[18]=z[2]*z[18];
    z[18]=z[18] + 1;
    z[18]=z[12]*z[18];
    z[12]=z[12] + z[16];
    z[12]=z[5]*z[12];
    z[20]= - static_cast<T>(5)+ z[7];
    z[20]=z[4]*z[20];
    z[20]=z[7] + z[20];
    z[20]=z[4]*z[20];
    z[12]=z[12] + z[20] + z[18];
    z[12]=z[5]*z[12];
    z[18]=n<T>(1,4)*z[4];
    z[20]=n<T>(5,2) + z[7];
    z[20]=z[20]*z[18];
    z[12]=n<T>(1,8)*z[12] - z[2] + z[20] - n<T>(1,4)*z[7] + z[10] + n<T>(5,16)*z[9];
    z[12]=z[5]*z[12];
    z[11]=z[11] + z[12];
    z[11]=z[8]*z[11];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[11]=z[8]*z[11];
    z[12]=z[4] - 1;
    z[14]=z[4]*z[7]*z[12];
    z[16]=z[5]*z[16];
    z[14]=z[14] + z[16];
    z[11]=n<T>(1,8)*z[14] + z[11];
    z[14]=z[5]*z[6];
    z[16]=n<T>(37,12)*z[2] - n<T>(11,3) - z[6];
    z[16]=z[2]*z[16];
    z[16]=z[14] - 3*z[12] + z[16];
    z[20]=n<T>(1,4)*z[5];
    z[16]=z[16]*z[20];
    z[21]=3*z[4];
    z[22]= - z[21] + n<T>(55,6) - z[6];
    z[22]=z[22]*z[15];
    z[23]= - n<T>(37,2) + 13*z[2];
    z[22]=n<T>(1,3)*z[23] + z[22];
    z[23]=npow(z[5],2);
    z[24]=n<T>(1,2)*z[8];
    z[22]=z[22]*z[23]*z[24];
    z[25]=5*z[2] - 9*z[4] + static_cast<T>(7)- 3*z[6];
    z[14]=n<T>(1,2)*z[25] + z[14];
    z[14]=z[14]*z[15];
    z[25]= - n<T>(17,6) + z[2];
    z[25]=z[2]*z[25];
    z[14]=z[25] + z[14];
    z[14]=z[5]*z[14];
    z[14]=z[14] + z[22];
    z[14]=z[14]*z[24];
    z[22]= - static_cast<T>(1)+ n<T>(1,3)*z[2];
    z[22]=z[5]*z[22]*z[19];
    z[23]= - z[2]*z[23];
    z[25]=z[8]*npow(z[5],3);
    z[23]=z[23] - n<T>(1,3)*z[25];
    z[23]=z[8]*z[23];
    z[25]=z[2] - static_cast<T>(1)- z[1];
    z[25]=z[25]*npow(z[2],3);
    z[22]=z[23] + n<T>(1,3)*z[25] + z[22];
    z[22]=z[3]*z[22];
    z[23]=n<T>(1,8)*z[2];
    z[25]=n<T>(37,3) + z[6];
    z[25]=z[25]*z[23];
    z[26]= - static_cast<T>(5)- n<T>(31,8)*z[1];
    z[25]=z[25] + n<T>(1,3)*z[26] - n<T>(1,8)*z[6];
    z[25]=z[2]*z[25];
    z[25]=n<T>(3,8)*z[1] + z[25];
    z[25]=z[2]*z[25];
    z[12]= - z[1]*z[12];
    z[12]=n<T>(5,2)*z[22] + z[14] + z[16] + n<T>(3,8)*z[12] + z[25];
    z[12]=z[3]*z[12];
    z[14]=z[21] - 3;
    z[14]=z[1]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[18];
    z[16]= - static_cast<T>(23)- 11*z[1];
    z[16]=n<T>(1,2)*z[16] - z[6];
    z[18]=n<T>(19,4) + z[6];
    z[18]=z[2]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[16]=z[2]*z[16];
    z[18]=n<T>(1,2) + z[4];
    z[18]=z[18]*z[21];
    z[18]=n<T>(13,3)*z[2] + z[18] - n<T>(11,6) - z[6];
    z[18]=z[18]*z[15];
    z[22]=z[10] - n<T>(5,4);
    z[14]=z[18] + z[16] + z[14] + n<T>(3,2)*z[1] - z[22];
    z[16]=static_cast<T>(1)+ z[2];
    z[16]=z[16]*z[17];
    z[18]=n<T>(5,4)*z[2];
    z[25]=static_cast<T>(1)+ z[4];
    z[25]=z[4]*z[25];
    z[25]=z[18] + z[6] + n<T>(9,2)*z[25];
    z[25]=z[25]*z[20];
    z[16]=z[25] + z[16] - z[4] - z[22];
    z[16]=z[5]*z[16];
    z[22]=n<T>(1,2)*z[4];
    z[25]=static_cast<T>(1)+ z[22];
    z[21]=z[25]*z[21];
    z[21]= - n<T>(5,4) + z[21];
    z[21]=z[21]*z[15];
    z[18]=z[21] - z[18] + n<T>(77,12) - z[4] - z[10];
    z[18]=z[5]*z[18];
    z[21]=n<T>(23,3) - z[2];
    z[21]=z[2]*z[21];
    z[21]= - n<T>(13,6) + z[21];
    z[18]=n<T>(1,2)*z[21] + z[18];
    z[18]=z[8]*z[18]*z[15];
    z[21]=z[19] + n<T>(7,2);
    z[25]=z[2]*z[21];
    z[16]=z[18] + n<T>(1,4)*z[25] + z[16];
    z[16]=z[8]*z[16];
    z[14]=n<T>(1,2)*z[14] + z[16];
    z[12]=n<T>(1,2)*z[14] + z[12];
    z[12]=z[3]*z[12];
    z[14]=n<T>(7,2) + z[4];
    z[14]=z[14]*z[22];
    z[16]=3*z[13];
    z[18]= - n<T>(5,2) - z[16];
    z[18]=z[18]*z[20];
    z[14]=z[18] - n<T>(13,8)*z[2] + z[14] - n<T>(5,8) + z[10];
    z[14]=z[5]*z[14];
    z[18]= - n<T>(7,6) - z[4];
    z[18]=n<T>(1,2)*z[18] - 3*z[19];
    z[14]=n<T>(1,2)*z[18] + z[14];
    z[14]=z[5]*z[14];
    z[17]= - z[21]*z[17];
    z[14]=z[17] + z[14];
    z[14]=z[14]*z[24];
    z[16]= - n<T>(5,4) - z[16];
    z[16]=z[16]*z[15];
    z[17]=n<T>(3,2) + z[4];
    z[17]=z[4]*z[17];
    z[16]=z[16] - n<T>(5,4) + z[17];
    z[15]=z[16]*z[15];
    z[16]=n<T>(1,8)*z[4];
    z[14]=z[14] + z[15] + n<T>(1,4)*z[19] + static_cast<T>(1)- z[16];
    z[14]=z[14]*z[24];
    z[15]= - n<T>(1,2) + z[4];
    z[15]=z[15]*z[16];
    z[16]=n<T>(23,6) + z[6];
    z[16]=z[16]*z[23];
    z[13]=z[13]*z[5];
    z[12]=z[12] + z[14] - n<T>(3,16)*z[13] + z[16] + z[15] - n<T>(11,48)*z[1] - 
   n<T>(2,3) - n<T>(1,4)*z[10];
    z[12]=z[3]*z[12];

    r += n<T>(1,4)*z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r1121(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1121(const std::array<dd_real,30>&);
#endif
