#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r994(const std::array<T,30>& k) {
  T z[76];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[20];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[5];
    z[9]=k[10];
    z[10]=k[24];
    z[11]=k[8];
    z[12]=k[9];
    z[13]=k[13];
    z[14]=npow(z[6],2);
    z[15]=3*z[14];
    z[16]=npow(z[6],3);
    z[17]=z[16]*z[8];
    z[18]=z[17] - z[15];
    z[19]=n<T>(1,2)*z[6];
    z[20]=z[19] + 1;
    z[21]=z[20]*z[6];
    z[22]=z[21] + n<T>(1,2);
    z[23]=n<T>(1,2)*z[8];
    z[18]=z[23]*z[22]*z[18];
    z[24]=z[22]*z[6];
    z[18]=z[18] + z[24];
    z[25]=z[18]*z[10];
    z[26]=z[23]*z[16];
    z[27]= - z[14] + z[26];
    z[28]= - static_cast<T>(43)+ n<T>(11,2)*z[6];
    z[29]=n<T>(1,3)*z[8];
    z[27]=z[29]*z[28]*z[27];
    z[28]=n<T>(1,3)*z[6];
    z[30]=17*z[6];
    z[31]=static_cast<T>(7)+ z[30];
    z[31]=z[31]*z[28];
    z[27]=z[27] + n<T>(19,4) + z[31];
    z[31]=n<T>(1,4)*z[8];
    z[32]=npow(z[6],4);
    z[33]=z[31]*z[32];
    z[33]=z[33] - z[16];
    z[34]=z[33]*z[29];
    z[35]=n<T>(1,2)*z[14];
    z[34]=z[34] + z[35];
    z[34]=z[34]*z[3];
    z[36]=z[14] - n<T>(1,3)*z[17];
    z[36]=z[8]*z[36];
    z[36]= - z[6] + z[36];
    z[37]=z[30] + n<T>(7,4);
    z[36]=z[37]*z[36];
    z[36]=n<T>(1,2)*z[36] + 17*z[34];
    z[36]=z[3]*z[36];
    z[27]= - n<T>(19,4)*z[25] + n<T>(1,4)*z[27] + z[36];
    z[27]=z[4]*z[27];
    z[36]=n<T>(5,3)*z[6];
    z[38]= - n<T>(7,16) - z[36];
    z[38]=z[38]*z[14];
    z[39]=n<T>(7,2) + z[30];
    z[39]=z[39]*z[17];
    z[38]=z[38] + n<T>(1,24)*z[39];
    z[38]=z[8]*z[38];
    z[39]=n<T>(1,4)*z[6];
    z[40]=3*z[6];
    z[41]=n<T>(7,4) + z[40];
    z[41]=z[41]*z[39];
    z[34]= - n<T>(17,4)*z[34] + z[41] + z[38];
    z[34]=z[3]*z[34];
    z[38]=7*z[6];
    z[41]=n<T>(1,2) - z[28];
    z[41]=z[41]*z[38];
    z[42]=z[14]*z[8];
    z[43]=n<T>(17,3)*z[6];
    z[44]=static_cast<T>(1)- z[43];
    z[44]=z[6]*z[44];
    z[44]= - n<T>(55,12) + z[44];
    z[44]=z[44]*z[42];
    z[45]= - n<T>(13,4) + n<T>(29,3)*z[6];
    z[45]=z[6]*z[45];
    z[45]=n<T>(55,6) + z[45];
    z[45]=z[6]*z[45];
    z[44]=z[45] + z[44];
    z[44]=z[8]*z[44];
    z[41]=z[44] - n<T>(55,12) + z[41];
    z[34]=n<T>(1,16)*z[41] + z[34];
    z[34]=z[3]*z[34];
    z[41]=13*z[6];
    z[44]=static_cast<T>(31)- z[41];
    z[44]=z[44]*z[39];
    z[44]=static_cast<T>(11)+ z[44];
    z[45]=5*z[6];
    z[44]=z[44]*z[45];
    z[46]= - n<T>(53,2) + z[6];
    z[46]=z[6]*z[46];
    z[46]= - n<T>(55,2) + z[46];
    z[46]=z[46]*z[42];
    z[44]=z[44] + z[46];
    z[44]=z[8]*z[44];
    z[46]= - static_cast<T>(49)+ 61*z[6];
    z[46]=z[46]*z[19];
    z[46]= - static_cast<T>(55)+ z[46];
    z[44]=n<T>(1,2)*z[46] + z[44];
    z[46]=19*z[10];
    z[18]= - z[18]*z[46];
    z[18]=n<T>(1,3)*z[44] + z[18];
    z[44]=n<T>(1,32)*z[10];
    z[18]=z[18]*z[44];
    z[47]= - static_cast<T>(43)+ 157*z[6];
    z[47]=z[47]*z[39];
    z[48]=n<T>(43,2) - 19*z[6];
    z[48]=z[48]*z[42];
    z[47]=z[47] + z[48];
    z[47]=z[47]*z[29];
    z[48]=n<T>(23,2)*z[6];
    z[47]=z[47] - n<T>(25,3) - z[48];
    z[18]=n<T>(1,8)*z[27] + z[18] + n<T>(1,32)*z[47] + z[34];
    z[18]=z[4]*z[18];
    z[27]=static_cast<T>(7)- z[41];
    z[27]=z[27]*z[19];
    z[34]=n<T>(3,2)*z[6];
    z[41]= - static_cast<T>(7)+ z[34];
    z[41]=z[41]*z[42];
    z[27]=z[27] + z[41];
    z[27]=z[8]*z[27];
    z[41]=z[6] - 1;
    z[47]=z[2]*z[41];
    z[27]=3*z[47] + z[27] + n<T>(9,2) + z[45];
    z[47]=n<T>(1,2)*z[2];
    z[49]= - z[41]*z[47];
    z[49]= - static_cast<T>(1)+ z[49];
    z[49]=z[49]*z[47];
    z[50]=z[6] + 1;
    z[49]=z[49] + n<T>(1,4)*z[50] - z[42];
    z[51]=3*z[10];
    z[49]=z[49]*z[51];
    z[27]=n<T>(1,4)*z[27] + z[49];
    z[27]=z[10]*z[27];
    z[49]=n<T>(5,8)*z[6] - z[42];
    z[49]=z[8]*z[49];
    z[27]=z[49] + z[27];
    z[27]=z[10]*z[27];
    z[49]=z[42] - z[19];
    z[52]=z[8]*z[49];
    z[53]=z[22]*z[10];
    z[54]= - z[53] + n<T>(1,2)*z[50];
    z[46]= - z[54]*z[46];
    z[55]= - z[14] + z[17];
    z[55]=z[8]*z[55];
    z[53]=n<T>(43,6)*z[55] + 19*z[53];
    z[53]=z[4]*z[53];
    z[46]=z[53] - n<T>(43,3)*z[52] + z[46];
    z[46]=z[4]*z[46];
    z[52]=n<T>(1,6)*z[6];
    z[53]=n<T>(1,3)*z[14];
    z[55]=z[53] - n<T>(1,8);
    z[56]= - z[8]*z[55];
    z[56]=z[52] + z[56];
    z[56]=z[8]*z[56];
    z[57]=npow(z[8],2);
    z[58]=z[57]*z[2];
    z[59]=z[58]*z[52];
    z[56]=z[56] + z[59];
    z[56]=z[2]*z[56];
    z[59]=z[8]*z[6];
    z[60]=n<T>(89,32) + n<T>(1,9)*z[14];
    z[60]=z[60]*z[59];
    z[55]= - n<T>(1,3)*z[55] + z[60];
    z[55]=z[55]*z[23];
    z[55]=z[55] + n<T>(1,3)*z[56];
    z[56]=n<T>(1,4)*z[14];
    z[60]= - z[57]*z[56];
    z[61]= - z[41]*z[19];
    z[61]=z[61] - z[42];
    z[61]=z[8]*z[61];
    z[61]=z[19] + z[61];
    z[61]=z[10]*z[61];
    z[60]=z[60] + z[61];
    z[60]=z[10]*z[60];
    z[61]= - z[57]*z[19];
    z[60]=z[61] + z[60];
    z[51]=z[60]*z[51];
    z[60]=z[57]*z[7];
    z[61]= - npow(z[10],3)*z[40]*z[60];
    z[62]=z[6]*z[57]*npow(z[10],2);
    z[61]= - n<T>(13,2)*z[62] + z[61];
    z[62]=n<T>(1,2)*z[7];
    z[61]=z[61]*z[62];
    z[51]=z[51] + z[61];
    z[51]=z[51]*z[62];
    z[27]=z[51] + n<T>(1,64)*z[46] + n<T>(1,2)*z[55] + z[27];
    z[27]=z[7]*z[27];
    z[46]=z[19] - 1;
    z[51]=z[46]*z[14];
    z[55]=z[23]*z[14];
    z[61]=static_cast<T>(3)- z[19];
    z[61]=z[6]*z[61];
    z[61]= - n<T>(3,2) + z[61];
    z[61]=z[61]*z[55];
    z[51]=z[51] + z[61];
    z[51]=z[8]*z[51];
    z[61]=z[6] - n<T>(3,2);
    z[63]=z[61]*z[17];
    z[63]= - z[16] + z[63];
    z[63]=z[8]*z[63];
    z[32]=z[32]*z[57];
    z[64]=n<T>(1,2)*z[3];
    z[65]= - z[64]*z[32];
    z[63]=z[63] + z[65];
    z[63]=z[63]*z[64];
    z[65]=z[60]*z[19];
    z[34]=z[34] - 1;
    z[66]= - z[34]*z[59];
    z[66]=z[6] + z[66];
    z[66]=z[8]*z[66];
    z[66]=z[66] - z[65];
    z[66]=z[66]*z[62];
    z[67]=z[46]*z[40];
    z[68]= - n<T>(1,2) - z[67];
    z[68]=z[8]*z[68]*z[19];
    z[69]=z[6] - n<T>(1,2);
    z[70]=z[6]*z[69];
    z[68]=z[70] + z[68];
    z[68]=z[8]*z[68];
    z[66]=z[66] - z[39] + z[68];
    z[66]=z[7]*z[66];
    z[51]=z[66] + z[63] - z[56] + z[51];
    z[51]=z[13]*z[51];
    z[56]=n<T>(77,2) - z[30];
    z[56]=z[56]*z[53];
    z[63]=3*z[42];
    z[66]=z[6] - 5;
    z[68]=z[6]*z[66];
    z[68]=static_cast<T>(3)+ z[68];
    z[68]=z[68]*z[63];
    z[56]=z[56] + z[68];
    z[68]=n<T>(1,8)*z[8];
    z[56]=z[56]*z[68];
    z[70]=3*z[17];
    z[61]= - z[61]*z[70];
    z[16]=n<T>(13,3)*z[16] + z[61];
    z[16]=z[8]*z[16];
    z[61]=n<T>(3,2)*z[3];
    z[32]=z[32]*z[61];
    z[16]=z[16] + z[32];
    z[32]=n<T>(1,4)*z[3];
    z[16]=z[16]*z[32];
    z[16]=z[16] + z[53] + z[56];
    z[16]=z[3]*z[16];
    z[53]=z[40]*z[8];
    z[46]=z[6]*z[46];
    z[46]=n<T>(1,4) + z[46];
    z[46]=z[46]*z[53];
    z[56]=41*z[6];
    z[71]=static_cast<T>(25)- z[56];
    z[71]=z[6]*z[71];
    z[46]=n<T>(1,24)*z[71] + z[46];
    z[46]=z[8]*z[46];
    z[46]=n<T>(7,24)*z[6] + z[46];
    z[53]=z[41]*z[53];
    z[53]= - n<T>(7,6)*z[6] + z[53];
    z[53]=z[7]*z[53]*z[68];
    z[16]=n<T>(3,4)*z[51] + z[53] + n<T>(1,2)*z[46] + z[16];
    z[16]=z[13]*z[16];
    z[46]= - static_cast<T>(67)+ 229*z[6];
    z[46]=z[6]*z[46];
    z[46]=n<T>(11,3) + z[46];
    z[46]=z[46]*z[52];
    z[51]= - n<T>(187,9) - n<T>(45,8)*z[6];
    z[51]=z[6]*z[51];
    z[51]= - n<T>(11,36) + z[51];
    z[51]=z[51]*z[42];
    z[46]=z[46] + z[51];
    z[46]=z[8]*z[46];
    z[51]= - static_cast<T>(9)- n<T>(239,2)*z[6];
    z[51]=z[51]*z[14];
    z[52]=n<T>(5,2)*z[6];
    z[53]=static_cast<T>(1)+ z[52];
    z[53]=z[53]*z[70];
    z[51]=z[51] + z[53];
    z[51]=z[8]*z[51];
    z[53]=static_cast<T>(3)+ n<T>(185,2)*z[6];
    z[53]=z[53]*z[40];
    z[51]=z[53] + z[51];
    z[51]=z[51]*z[64];
    z[53]=n<T>(173,3) - n<T>(483,8)*z[6];
    z[53]=z[6]*z[53];
    z[46]=z[51] + z[46] - n<T>(11,36) + z[53];
    z[46]=z[3]*z[46];
    z[51]= - n<T>(439,4) + 113*z[6];
    z[51]=z[6]*z[51];
    z[51]=n<T>(11,6) + z[51];
    z[51]=z[51]*z[59];
    z[53]= - n<T>(7,2) - 445*z[6];
    z[53]=z[6]*z[53];
    z[53]= - static_cast<T>(11)+ z[53];
    z[51]=n<T>(1,6)*z[53] + z[51];
    z[53]=n<T>(1,6)*z[8];
    z[51]=z[51]*z[53];
    z[68]= - n<T>(259,36) - z[6];
    z[68]=z[6]*z[68];
    z[46]=z[46] + z[51] + n<T>(529,18) + z[68];
    z[51]=n<T>(1,8)*z[3];
    z[46]=z[46]*z[51];
    z[68]= - static_cast<T>(1)- n<T>(13,12)*z[6];
    z[68]=z[68]*z[28];
    z[68]=n<T>(141,64) + z[68];
    z[68]=z[6]*z[68];
    z[68]= - n<T>(7,24) + z[68];
    z[68]=z[68]*z[59];
    z[71]=n<T>(3,2) + n<T>(13,9)*z[6];
    z[71]=z[6]*z[71];
    z[71]= - n<T>(521,288) + z[71];
    z[71]=z[6]*z[71];
    z[71]= - n<T>(1,12) + z[71];
    z[68]=n<T>(1,2)*z[71] + z[68];
    z[68]=z[8]*z[68];
    z[71]= - static_cast<T>(197)- n<T>(253,2)*z[6];
    z[71]=z[6]*z[71];
    z[71]=n<T>(343,4) + z[71];
    z[16]=z[16] + z[46] + n<T>(1,144)*z[71] + z[68];
    z[46]= - n<T>(2327,12) - 47*z[6];
    z[46]=z[46]*z[39];
    z[46]= - n<T>(11,3) + z[46];
    z[48]= - static_cast<T>(1)+ z[48];
    z[48]=z[48]*z[42];
    z[68]=static_cast<T>(53)- n<T>(911,4)*z[6];
    z[68]=z[6]*z[68];
    z[48]=n<T>(1,9)*z[68] + z[48];
    z[48]=z[48]*z[31];
    z[68]=n<T>(15,2)*z[6];
    z[71]=static_cast<T>(11)+ z[68];
    z[71]=z[71]*z[14];
    z[71]=n<T>(3,8)*z[71] - z[17];
    z[71]=z[8]*z[71];
    z[72]= - n<T>(21,4) - 23*z[6];
    z[72]=z[6]*z[72];
    z[71]=z[72] + z[71];
    z[71]=z[3]*z[71];
    z[46]=z[71] + n<T>(1,3)*z[46] + z[48];
    z[46]=z[3]*z[46];
    z[48]=n<T>(1,4) + n<T>(17,9)*z[6];
    z[48]=z[48]*z[38];
    z[48]=n<T>(131,18) + z[48];
    z[71]= - n<T>(29,9) + z[39];
    z[71]=z[6]*z[71];
    z[71]= - n<T>(71,18) + z[71];
    z[71]=z[71]*z[59];
    z[48]=n<T>(1,2)*z[48] + z[71];
    z[48]=z[8]*z[48];
    z[71]= - n<T>(271,18) - z[40];
    z[71]=z[6]*z[71];
    z[48]=z[48] - n<T>(1795,36) + z[71];
    z[46]=n<T>(1,4)*z[48] + z[46];
    z[46]=z[46]*z[32];
    z[48]=static_cast<T>(1)+ z[45];
    z[48]=z[8]*z[48];
    z[48]= - n<T>(5,2) + z[48];
    z[48]=z[48]*z[53];
    z[53]= - static_cast<T>(35)- n<T>(47,2)*z[6];
    z[71]= - z[3]*z[40];
    z[53]=n<T>(1,8)*z[53] + z[71];
    z[53]=z[53]*z[32];
    z[53]=z[53] - n<T>(5,32) + z[29];
    z[53]=z[3]*z[53];
    z[71]=npow(z[3],2);
    z[72]= - n<T>(5,3)*z[57] + 3*z[71];
    z[72]=z[2]*z[72];
    z[48]=n<T>(1,8)*z[72] + z[48] + z[53];
    z[48]=z[2]*z[48];
    z[53]= - static_cast<T>(13)- z[19];
    z[53]=z[53]*z[39];
    z[53]=z[53] - z[63];
    z[53]=z[8]*z[53];
    z[72]=n<T>(643,9) + n<T>(45,4)*z[6];
    z[72]=z[6]*z[72];
    z[72]=n<T>(601,18) + z[72];
    z[53]=n<T>(1,4)*z[72] + z[53];
    z[72]=n<T>(1,8)*z[6];
    z[68]=static_cast<T>(19)+ z[68];
    z[68]=z[68]*z[72];
    z[68]=z[68] - z[42];
    z[73]=3*z[3];
    z[68]=z[68]*z[73];
    z[53]=n<T>(1,2)*z[53] + z[68];
    z[53]=z[53]*z[32];
    z[68]= - n<T>(7,3) - z[19];
    z[68]=z[68]*z[59];
    z[74]=2*z[6];
    z[75]= - n<T>(13,8) - z[74];
    z[68]=n<T>(1,3)*z[75] + n<T>(1,32)*z[68];
    z[68]=z[8]*z[68];
    z[75]=n<T>(31,9) + n<T>(21,32)*z[6];
    z[53]=z[53] + n<T>(1,2)*z[75] + z[68];
    z[53]=z[3]*z[53];
    z[68]= - static_cast<T>(4)- 11*z[6];
    z[68]=z[68]*z[28];
    z[68]=n<T>(7,32) + z[68];
    z[29]=z[68]*z[29];
    z[29]=z[29] + n<T>(3,8) + n<T>(11,9)*z[6];
    z[29]=z[8]*z[29];
    z[29]=z[48] + z[53] - n<T>(79,192) + z[29];
    z[29]=z[2]*z[29];
    z[48]=static_cast<T>(4)+ z[38];
    z[48]=z[48]*z[28];
    z[48]= - n<T>(139,64) + z[48];
    z[48]=z[6]*z[48];
    z[48]= - n<T>(1,8) + z[48];
    z[48]=z[8]*z[48];
    z[53]=n<T>(7,2)*z[6];
    z[68]= - static_cast<T>(2)- z[53];
    z[68]=z[6]*z[68];
    z[48]=z[48] + n<T>(23,96) + z[68];
    z[48]=z[8]*z[48];
    z[68]=static_cast<T>(181)+ 253*z[6];
    z[48]=n<T>(1,96)*z[68] + z[48];
    z[29]=z[29] + n<T>(1,3)*z[48] + z[46];
    z[29]=z[2]*z[29];
    z[46]= - static_cast<T>(5)+ z[40];
    z[46]=z[46]*z[14];
    z[48]= - z[20]*z[70];
    z[46]=z[46] + z[48];
    z[31]=z[46]*z[31];
    z[46]=n<T>(37,2) + z[6];
    z[48]= - z[2] + z[6] + 3;
    z[68]= - z[3]*z[48];
    z[68]= - static_cast<T>(3)+ z[68];
    z[68]=z[2]*z[68];
    z[46]=z[68] + n<T>(1,2)*z[46] + z[73];
    z[46]=z[2]*z[46];
    z[68]=z[40] - 1;
    z[70]=z[3]*z[68];
    z[46]=z[46] - n<T>(19,2) + z[70];
    z[46]=z[46]*z[47];
    z[15]=z[41]*z[15];
    z[15]=z[15] + z[17];
    z[15]=z[15]*z[23];
    z[23]=z[34]*z[6];
    z[15]=z[15] - z[23];
    z[23]= - z[2]*z[48];
    z[23]=static_cast<T>(3)+ z[23];
    z[23]=z[2]*z[23];
    z[23]=z[23] + z[68];
    z[23]=z[23]*z[47];
    z[23]=z[23] - z[15];
    z[23]=z[10]*z[23];
    z[15]= - z[3]*z[15];
    z[15]=n<T>(3,2)*z[23] + z[46] + z[15] + z[31] + n<T>(13,8) - z[6];
    z[15]=z[10]*z[15];
    z[23]= - static_cast<T>(1)- 15*z[6];
    z[23]=z[23]*z[72];
    z[31]=z[6] + n<T>(1,4);
    z[46]=z[31]*z[42];
    z[23]=z[23] + z[46];
    z[23]=z[8]*z[23];
    z[46]= - z[21] + z[55];
    z[46]=z[8]*z[46];
    z[48]=static_cast<T>(3)- z[45];
    z[46]=n<T>(1,2)*z[48] + z[46];
    z[46]=z[46]*z[64];
    z[48]= - static_cast<T>(3)+ z[6];
    z[68]= - 5*z[2] + static_cast<T>(13)+ z[40];
    z[68]=z[3]*z[68];
    z[68]=static_cast<T>(3)+ z[68];
    z[68]=z[2]*z[68];
    z[48]=z[68] + 3*z[48] - 11*z[3];
    z[48]=z[2]*z[48];
    z[68]=n<T>(7,2) + z[6];
    z[15]=z[15] + n<T>(1,4)*z[48] + z[46] + n<T>(1,2)*z[68] + z[23];
    z[15]=z[10]*z[15];
    z[23]=n<T>(3,4)*z[6];
    z[46]= - static_cast<T>(1)- z[23];
    z[46]=z[6]*z[46];
    z[48]=n<T>(409,2304) + z[21];
    z[48]=z[6]*z[48];
    z[48]=n<T>(217,576) + z[48];
    z[48]=z[8]*z[48];
    z[46]=z[48] - n<T>(1,4) + z[46];
    z[46]=z[8]*z[46];
    z[48]=z[28]*z[8];
    z[68]= - z[50]*z[48];
    z[70]=n<T>(1,3) + z[6];
    z[68]=n<T>(1,4)*z[70] + z[68];
    z[68]=z[8]*z[68];
    z[70]=z[58]*z[6];
    z[68]=z[68] + n<T>(1,12)*z[70];
    z[68]=z[2]*z[68];
    z[70]=n<T>(1,3) + z[19];
    z[46]=z[68] + n<T>(1,2)*z[70] + z[46];
    z[46]=z[2]*z[46];
    z[36]= - static_cast<T>(3)- z[36];
    z[36]=z[36]*z[39];
    z[68]=static_cast<T>(7)+ z[40];
    z[68]=z[6]*z[68];
    z[68]=n<T>(2221,576) + z[68];
    z[68]=z[6]*z[68];
    z[68]=n<T>(701,576) + z[68];
    z[70]=z[28] + 1;
    z[70]=z[70]*z[6];
    z[72]= - n<T>(451,1152) - z[70];
    z[72]=z[6]*z[72];
    z[72]= - n<T>(509,1152) + z[72];
    z[72]=z[6]*z[72];
    z[72]=n<T>(33,128) + z[72];
    z[72]=z[8]*z[72];
    z[68]=n<T>(1,4)*z[68] + z[72];
    z[68]=z[8]*z[68];
    z[36]=z[46] + z[68] - n<T>(1,3) + z[36];
    z[36]=z[2]*z[36];
    z[46]= - static_cast<T>(949)- 977*z[6];
    z[46]=z[46]*z[39];
    z[46]=static_cast<T>(7)+ z[46];
    z[46]=z[6]*z[46];
    z[68]=n<T>(187,2) + 97*z[6];
    z[68]=z[6]*z[68];
    z[68]= - n<T>(7,2) + z[68];
    z[68]=z[68]*z[42];
    z[46]=z[46] + z[68];
    z[46]=z[8]*z[46];
    z[68]=static_cast<T>(575)+ 589*z[6];
    z[68]=z[68]*z[19];
    z[68]= - static_cast<T>(7)+ z[68];
    z[46]=n<T>(1,2)*z[68] + z[46];
    z[25]=n<T>(1,3)*z[46] - 67*z[25];
    z[25]=z[25]*z[44];
    z[44]=z[50]*z[47];
    z[22]=z[44] - z[22];
    z[44]=z[10]*z[54];
    z[22]=n<T>(1,3)*z[22] - n<T>(67,32)*z[44];
    z[22]=z[22]*z[62];
    z[44]=static_cast<T>(1)+ z[39];
    z[44]=z[6]*z[44];
    z[44]= - n<T>(629,768) + z[44];
    z[44]=z[6]*z[44];
    z[44]=n<T>(53,128) + z[44];
    z[44]=z[6]*z[44];
    z[44]=n<T>(5,192) + z[44];
    z[44]=z[44]*z[48];
    z[46]= - n<T>(5,3) - z[19];
    z[46]=z[6]*z[46];
    z[46]=n<T>(599,1152) + z[46];
    z[46]=z[6]*z[46];
    z[46]= - n<T>(131,192) + z[46];
    z[46]=z[6]*z[46];
    z[46]= - n<T>(5,288) + z[46];
    z[44]=n<T>(1,2)*z[46] + z[44];
    z[44]=z[8]*z[44];
    z[46]=n<T>(5,192) + z[70];
    z[46]=z[6]*z[46];
    z[46]=n<T>(13,32) + z[46];
    z[22]=z[22] + z[25] + z[36] + n<T>(1,2)*z[46] + z[44];
    z[22]=z[9]*z[22];
    z[25]= - n<T>(5,2) + z[6];
    z[25]=z[25]*z[35];
    z[35]=static_cast<T>(1)- z[39];
    z[35]=z[35]*z[17];
    z[25]=z[25] + z[35];
    z[25]=z[8]*z[25];
    z[35]=n<T>(3,4) - z[6];
    z[35]=z[35]*z[14];
    z[36]=z[69]*z[26];
    z[35]=z[35] + z[36];
    z[35]=z[8]*z[35];
    z[33]=z[33]*z[8];
    z[33]=z[33] + n<T>(3,2)*z[14];
    z[33]=z[33]*z[3];
    z[23]= - z[33] - z[23] + z[35];
    z[23]=z[3]*z[23];
    z[34]= - z[34]*z[55];
    z[35]= - n<T>(1,4) + z[6];
    z[35]=z[6]*z[35];
    z[34]=z[35] + z[34];
    z[34]=z[8]*z[34];
    z[35]=z[6] - n<T>(3,2)*z[42];
    z[35]=z[8]*z[35];
    z[35]= - z[65] - n<T>(1,2) + z[35];
    z[35]=z[35]*z[62];
    z[36]= - static_cast<T>(1)- z[40];
    z[34]=z[35] + n<T>(1,4)*z[36] + z[34];
    z[34]=z[7]*z[34];
    z[35]= - z[50]*z[19];
    z[23]=z[34] + z[23] + z[35] + z[25];
    z[25]=n<T>(1,2)*z[5];
    z[23]=z[23]*z[25];
    z[34]= - n<T>(13,4) + z[6];
    z[34]=z[34]*z[14];
    z[26]= - z[66]*z[26];
    z[26]=z[34] + z[26];
    z[26]=z[8]*z[26];
    z[21]= - z[21] + z[26];
    z[26]=static_cast<T>(3)- z[52];
    z[14]=z[26]*z[14];
    z[17]=z[41]*z[17];
    z[14]=z[14] + z[17];
    z[14]=z[8]*z[14];
    z[14]=z[67] + z[14];
    z[14]=n<T>(1,2)*z[14] - z[33];
    z[14]=z[3]*z[14];
    z[14]=n<T>(1,2)*z[21] + z[14];
    z[14]=z[3]*z[14];
    z[17]= - static_cast<T>(3)+ z[38];
    z[17]=z[17]*z[19];
    z[19]= - z[41]*z[63];
    z[17]=z[17] + z[19];
    z[17]=z[8]*z[17];
    z[19]=3*z[8];
    z[19]= - z[49]*z[19];
    z[21]= - z[6]*z[60];
    z[19]=z[21] - static_cast<T>(1)+ z[19];
    z[19]=z[7]*z[19];
    z[17]=z[19] - n<T>(3,2)*z[50] + z[17];
    z[14]=z[23] + z[14] + n<T>(1,4)*z[17];
    z[14]=z[5]*z[14];
    z[17]= - static_cast<T>(33)- n<T>(665,6)*z[6];
    z[17]=z[3]*z[17];
    z[17]=z[17] - n<T>(1033,9) - n<T>(69,2)*z[6];
    z[17]=z[3]*z[17];
    z[17]=static_cast<T>(21)+ z[17];
    z[17]=z[17]*z[51];
    z[19]=static_cast<T>(9)+ z[52];
    z[19]=z[19]*z[61];
    z[19]=static_cast<T>(17)+ z[19];
    z[19]=z[19]*z[71];
    z[21]=z[2]*npow(z[3],3);
    z[19]=n<T>(1,4)*z[19] - z[21];
    z[19]=z[2]*z[19];
    z[17]=z[17] + z[19];
    z[17]=z[2]*z[17];
    z[19]=static_cast<T>(17)+ n<T>(553,2)*z[6];
    z[19]=z[19]*z[64];
    z[23]=n<T>(625,3) + z[53];
    z[19]=n<T>(1,3)*z[23] + z[19];
    z[19]=z[19]*z[64];
    z[19]=z[19] - n<T>(73,9) - n<T>(21,4)*z[6];
    z[19]=z[19]*z[64];
    z[17]=z[19] + z[17];
    z[17]=z[2]*z[17];
    z[19]=n<T>(53,3) - 121*z[3];
    z[19]=z[19]*z[71];
    z[23]= - n<T>(21,2) + n<T>(247,3)*z[3];
    z[23]=z[23]*z[71];
    z[21]=z[23] - 21*z[21];
    z[21]=z[2]*z[21];
    z[19]=z[19] + z[21];
    z[19]=z[2]*z[19];
    z[21]=n<T>(35,6) + 79*z[3];
    z[21]=z[21]*z[71];
    z[19]=z[21] + z[19];
    z[21]=n<T>(17,4)*z[4] - static_cast<T>(11)- n<T>(17,2)*z[3];
    z[21]=z[4]*z[71]*z[21];
    z[19]=n<T>(1,4)*z[19] + n<T>(1,3)*z[21];
    z[21]= - static_cast<T>(1)- z[3];
    z[21]=z[21]*z[71];
    z[23]= - static_cast<T>(1)- z[64];
    z[23]=z[3]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[5]*z[23];
    z[21]=z[21] + z[23];
    z[21]=z[5]*z[21];
    z[19]=n<T>(1,2)*z[19] + z[21];
    z[19]=z[1]*z[19];
    z[21]= - static_cast<T>(1)- n<T>(167,2)*z[6];
    z[21]=z[21]*z[73];
    z[21]=z[21] - n<T>(625,9) + n<T>(83,2)*z[6];
    z[21]=z[21]*z[64];
    z[23]= - n<T>(29,6) - z[45];
    z[21]=n<T>(1,3)*z[23] + z[21];
    z[21]=z[21]*z[32];
    z[17]=z[19] + z[21] + z[17];
    z[19]=z[3]*z[6];
    z[21]=n<T>(17,4)*z[19] - n<T>(7,16) + z[74];
    z[21]=z[3]*z[21];
    z[23]= - z[3]*z[30];
    z[23]=n<T>(1,2)*z[37] + z[23];
    z[23]=z[4]*z[23];
    z[26]= - n<T>(1,4) - z[28];
    z[21]=n<T>(1,24)*z[23] + n<T>(5,16)*z[26] + n<T>(1,3)*z[21];
    z[21]=z[4]*z[3]*z[21];
    z[23]=z[19] + z[31];
    z[23]=z[3]*z[23];
    z[26]=n<T>(3,2) + z[6];
    z[23]=z[62] + n<T>(1,2)*z[26] + z[23];
    z[23]=z[23]*z[25];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[3]*z[19];
    z[19]=n<T>(7,8) + z[19];
    z[19]=z[3]*z[19];
    z[19]=z[23] + n<T>(1,4) + z[19];
    z[19]=z[5]*z[19];
    z[17]=z[19] + z[21] + n<T>(1,4)*z[17];
    z[17]=z[1]*z[17];
    z[19]=n<T>(23,4) - z[43];
    z[19]=z[19]*z[28];
    z[19]= - n<T>(33,8) + z[19];
    z[20]= - static_cast<T>(1)+ n<T>(41,96)*z[6];
    z[20]=z[20]*z[28];
    z[20]=z[20] + n<T>(33,64);
    z[21]=z[6]*z[20];
    z[21]= - n<T>(33,64) + z[21];
    z[21]=z[8]*z[21];
    z[19]=n<T>(1,16)*z[19] + z[21];
    z[19]=z[8]*z[19];
    z[21]=static_cast<T>(1)- n<T>(41,144)*z[6];
    z[21]=z[6]*z[21];
    z[21]= - n<T>(217,144) + z[21];
    z[21]=z[21]*z[58];
    z[23]= - n<T>(1,2) - z[8];
    z[23]=z[8]*z[23];
    z[23]=33*z[23] - n<T>(1165,18)*z[58];
    z[23]=z[9]*z[23];
    z[19]=n<T>(1,64)*z[23] + z[19] + n<T>(1,2)*z[21];
    z[19]=z[2]*z[19];
    z[21]= - static_cast<T>(55)+ z[56];
    z[21]=z[21]*z[28];
    z[21]=n<T>(35,2) + z[21];
    z[21]=z[21]*z[59];
    z[20]=z[20]*z[58];
    z[20]=n<T>(1,48)*z[21] + z[20];
    z[20]=z[2]*z[20];
    z[21]= - static_cast<T>(7)+ n<T>(41,2)*z[6];
    z[21]=z[6]*z[21];
    z[21]= - n<T>(5,4) + z[21];
    z[21]=z[21]*z[28];
    z[21]=n<T>(35,4) + z[21];
    z[21]=z[6]*z[21];
    z[23]=z[9]*z[57]*npow(z[2],2);
    z[20]=n<T>(33,64)*z[23] + n<T>(1,48)*z[21] + z[20];
    z[21]=n<T>(1,2)*z[12];
    z[20]=z[20]*z[21];
    z[19]=z[20] - n<T>(41,144)*z[24] + z[19];
    z[19]=z[19]*z[21];
    z[20]=z[2] - 1;
    z[20]=z[8]*z[20];
    z[21]= - z[8] - z[58];
    z[21]=z[7]*z[21];
    z[20]=z[21] + static_cast<T>(1)+ z[20];
    z[20]=z[2]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[11]*z[20];

    r += z[14] + z[15] + n<T>(1,2)*z[16] + z[17] + z[18] + z[19] + n<T>(1,96)*
      z[20] + z[22] + z[27] + z[29];
 
    return r;
}

template double qqb_2lha_r994(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r994(const std::array<dd_real,30>&);
#endif
