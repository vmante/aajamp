#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2009(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[14];
    z[5]=k[24];
    z[6]=k[3];
    z[7]=k[27];
    z[8]=k[4];
    z[9]=k[17];
    z[10]=k[9];
    z[11]=z[5] - z[7];
    z[11]=z[11]*z[9];
    z[12]=z[9] - 1;
    z[12]=z[11]*z[12];
    z[13]=z[7]*z[3];
    z[14]=z[5]*z[3];
    z[13]=z[13] - z[14];
    z[15]=z[11] - z[13];
    z[15]=z[2]*z[15];
    z[13]=z[15] - z[12] - z[13];
    z[13]=z[2]*z[13];
    z[15]= - z[10]*z[12];
    z[13]= - z[15] + z[13];
    z[13]=z[8]*z[13];
    z[16]=z[3] + 1;
    z[16]=z[4]*z[16];
    z[17]=n<T>(3,2)*z[14];
    z[15]= - n<T>(3,2)*z[15] - z[17] - z[16];
    z[11]=z[11] - z[7] - z[5];
    z[11]=z[2]*z[11];
    z[11]=z[12] - z[11];
    z[11]= - n<T>(3,4)*z[14] + n<T>(1,4) + z[3] - n<T>(3,8)*z[11];
    z[11]=z[2]*z[11];
    z[11]=n<T>(3,8)*z[13] + n<T>(1,4)*z[15] + z[11];
    z[11]=z[8]*z[11];
    z[12]= - z[17] + z[16];
    z[13]=z[1] - 1;
    z[12]=z[13]*z[12];
    z[13]=z[2]*z[7]*z[6];
    z[13]= - n<T>(3,2)*z[13] + n<T>(5,2) - z[1];
    z[13]=z[2]*z[13];
    z[12]=z[13] + z[12];

    r += z[11] + n<T>(1,4)*z[12];
 
    return r;
}

template double qqb_2lha_r2009(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2009(const std::array<dd_real,30>&);
#endif
