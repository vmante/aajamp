#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2010(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[3];
    z[2]=k[5];
    z[3]=k[8];
    z[4]=k[27];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[24];
    z[9]=k[10];
    z[10]=k[7];
    z[11]= - z[5] - z[3];
    z[12]=npow(z[2],2);
    z[13]=z[12]*z[1];
    z[13]=z[13] - z[2];
    z[11]=z[4]*z[1]*z[13]*z[11];
    z[13]=z[12]*z[6];
    z[13]=z[13] - z[2];
    z[14]=z[8]*z[6];
    z[15]=z[13]*z[14];
    z[16]=z[6]*z[2];
    z[15]=z[16] + z[15];
    z[15]=z[9]*z[15];
    z[17]=z[1]*z[2];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17];
    z[11]=z[17] + z[11] + z[15] + z[2];
    z[14]=n<T>(3,4)*z[14] - n<T>(1,4);
    z[13]=z[7]*z[13]*z[14];
    z[14]= - n<T>(7,4)*z[1] - z[6];
    z[12]=z[12]*z[14];
    z[12]=n<T>(7,4)*z[2] + z[12];
    z[12]=z[5]*z[12];
    z[14]=z[5]*z[16];
    z[15]= - static_cast<T>(1)+ z[16];
    z[15]=z[7]*z[15];
    z[14]=z[14] + z[15];
    z[14]=z[10]*z[14];
    z[11]=z[14] + z[12] + z[13] + n<T>(3,4)*z[11];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qqb_2lha_r2010(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2010(const std::array<dd_real,30>&);
#endif
