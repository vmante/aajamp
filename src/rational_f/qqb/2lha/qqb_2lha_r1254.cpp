#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1254(const std::array<T,30>& k) {
  T z[61];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[6];
    z[7]=k[4];
    z[8]=k[12];
    z[9]=k[11];
    z[10]=k[8];
    z[11]=k[10];
    z[12]=k[9];
    z[13]=n<T>(1,2)*z[2];
    z[14]=npow(z[3],3);
    z[15]=z[13]*z[14];
    z[16]=npow(z[3],2);
    z[17]= - n<T>(3,4) + z[3];
    z[17]=z[17]*z[16];
    z[17]= - z[15] - n<T>(15,8) + z[17];
    z[17]=z[2]*z[17];
    z[18]=n<T>(3,2) - z[3];
    z[18]=z[3]*z[18];
    z[18]= - n<T>(7,8) + z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(5,4) + z[18];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[2]*z[17];
    z[18]=3*z[6];
    z[19]=5*z[4];
    z[20]= - n<T>(107,12) + z[19];
    z[20]=z[4]*z[20];
    z[20]=z[20] - n<T>(17,3) + z[18];
    z[21]=n<T>(1,3)*z[4];
    z[22]=z[21]*z[5];
    z[23]=n<T>(5,4)*z[4];
    z[24]= - static_cast<T>(11)+ z[23];
    z[24]=z[24]*z[22];
    z[20]=n<T>(1,4)*z[20] + z[24];
    z[24]=n<T>(1,2)*z[5];
    z[20]=z[20]*z[24];
    z[25]=n<T>(7,8) - z[3];
    z[25]=z[25]*z[16];
    z[25]=z[15] + n<T>(5,2) + z[25];
    z[25]=z[2]*z[25];
    z[26]= - n<T>(7,4) + z[3];
    z[26]=z[3]*z[26];
    z[26]=n<T>(1,4) + z[26];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(15,4) + z[26];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[25]=z[2]*z[25];
    z[25]=n<T>(277,96) + z[25];
    z[25]=z[2]*z[25];
    z[26]= - n<T>(157,6) + 13*z[11];
    z[27]= - static_cast<T>(3)- z[11];
    z[27]=z[11]*z[27];
    z[27]=z[27] + n<T>(9,8)*z[12];
    z[27]=z[4]*z[27];
    z[26]=n<T>(1,4)*z[26] + 3*z[27];
    z[27]=n<T>(1,4)*z[4];
    z[26]=z[26]*z[27];
    z[28]=static_cast<T>(17)+ z[23];
    z[28]=z[4]*z[28];
    z[28]= - n<T>(17,2) + z[28];
    z[28]=z[5]*z[28];
    z[25]=n<T>(1,6)*z[28] + z[26] + z[25] + n<T>(265,96) - z[8];
    z[26]=n<T>(1,2)*z[9];
    z[25]=z[25]*z[26];
    z[28]=n<T>(1,4)*z[11];
    z[29]=37*z[11];
    z[30]=static_cast<T>(51)+ z[29];
    z[30]=z[30]*z[28];
    z[31]=n<T>(1,4)*z[6];
    z[30]= - z[31] - n<T>(15,8)*z[12] + static_cast<T>(5)+ z[30];
    z[32]=n<T>(1,8)*z[4];
    z[30]=z[30]*z[32];
    z[33]=z[6]*z[12];
    z[34]=n<T>(3,8)*z[33];
    z[35]=n<T>(3,4)*z[12] - n<T>(11,24) + z[8];
    z[17]=z[25] + z[20] + z[30] - z[34] + n<T>(1,2)*z[35] + z[17];
    z[17]=z[9]*z[17];
    z[20]=z[3] - n<T>(5,8);
    z[25]= - z[20]*z[16];
    z[15]=z[15] + n<T>(5,4) + z[25];
    z[15]=z[2]*z[15];
    z[25]=n<T>(1,2)*z[11];
    z[30]= - static_cast<T>(13)- n<T>(37,2)*z[11];
    z[30]=z[30]*z[25];
    z[30]= - static_cast<T>(1)+ z[30];
    z[35]=n<T>(1,2)*z[1];
    z[36]=z[3] - n<T>(5,4);
    z[37]=z[3]*z[36];
    z[37]=z[37] + static_cast<T>(3)+ z[35];
    z[38]=n<T>(1,2)*z[3];
    z[37]=z[37]*z[38];
    z[39]=n<T>(1,2)*z[12];
    z[40]= - z[39] + z[33];
    z[41]=n<T>(7,8)*z[6];
    z[40]=z[40]*z[41];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[4]*z[40];
    z[42]=3*z[12];
    z[43]= - static_cast<T>(1)- z[42];
    z[43]=n<T>(1,4)*z[43] + z[6];
    z[43]=z[6]*z[43];
    z[15]=z[40] + z[43] + z[15] + z[37] + n<T>(1,8)*z[30] - z[1];
    z[30]=n<T>(1,2)*z[4];
    z[37]= - n<T>(21,2) - 19*z[1];
    z[37]=n<T>(1,2)*z[37] + 7*z[4];
    z[37]=z[37]*z[30];
    z[37]=z[37] + n<T>(13,6) + z[1];
    z[37]=z[4]*z[37];
    z[40]=z[1] + 1;
    z[43]=z[6]*z[1];
    z[44]=n<T>(3,4)*z[43] + n<T>(1,4) + z[1];
    z[44]=z[6]*z[44];
    z[37]=z[37] - n<T>(1,4)*z[40] + z[44];
    z[44]=n<T>(3,4)*z[1];
    z[45]=151*z[4] - static_cast<T>(319)- 181*z[1];
    z[45]=z[4]*z[45];
    z[45]=n<T>(1,48)*z[45] + static_cast<T>(3)+ n<T>(13,3)*z[1];
    z[45]=z[4]*z[45];
    z[45]=z[44] + z[45];
    z[45]=z[4]*z[45];
    z[46]=n<T>(1,3)*z[1];
    z[47]=z[21] - static_cast<T>(1)- z[46];
    z[47]=z[47]*z[30];
    z[47]=n<T>(1,3)*z[40] + z[47];
    z[48]=11*z[5];
    z[47]=z[47]*npow(z[4],3)*z[48];
    z[43]=z[43] - z[1];
    z[45]=z[47] - n<T>(3,4)*z[43] + z[45];
    z[45]=z[5]*z[45];
    z[37]=n<T>(1,2)*z[37] + z[45];
    z[37]=z[5]*z[37];
    z[45]=z[4] - z[1];
    z[45]= - static_cast<T>(1)+ n<T>(15,4)*z[45];
    z[45]=z[45]*z[30];
    z[44]= - static_cast<T>(1)- z[44];
    z[44]=z[6]*z[44];
    z[44]=z[44] + static_cast<T>(1)- n<T>(1,4)*z[1];
    z[44]=z[6]*z[44];
    z[44]=z[45] + n<T>(53,24) + z[44];
    z[37]=n<T>(1,2)*z[44] + z[37];
    z[37]=z[5]*z[37];
    z[15]=z[17] + n<T>(1,2)*z[15] + z[37];
    z[17]= - n<T>(41,3) - z[30];
    z[17]=z[17]*z[30];
    z[37]= - static_cast<T>(17)- z[27];
    z[37]=z[37]*z[21];
    z[37]=n<T>(5,2) + z[37];
    z[37]=z[5]*z[37];
    z[44]=n<T>(3,2)*z[3];
    z[45]=n<T>(3,4)*z[6];
    z[17]=z[37] + z[17] + z[45] - z[44] + n<T>(109,8) - 7*z[8];
    z[17]=z[17]*z[24];
    z[37]=3*z[3];
    z[47]= - n<T>(181,4) + z[42];
    z[47]=n<T>(1,2)*z[47] + z[37];
    z[49]= - n<T>(17,4) + z[37];
    z[49]=z[49]*z[16];
    z[14]=z[14]*z[2];
    z[50]=3*z[14];
    z[49]= - z[50] - n<T>(15,2) + z[49];
    z[49]=z[49]*z[13];
    z[49]=z[49] + n<T>(15,8) + z[16];
    z[49]=z[2]*z[49];
    z[51]=static_cast<T>(1)+ z[25];
    z[51]=z[4]*z[51];
    z[51]=z[51] - 11;
    z[51]=z[11]*z[51];
    z[42]= - z[6] + z[42] + z[51];
    z[42]=z[42]*z[32];
    z[17]=z[17] + z[42] - z[34] + n<T>(1,4)*z[47] + z[49];
    z[17]=z[17]*z[26];
    z[34]=n<T>(103,3) + z[4];
    z[34]=z[34]*z[30];
    z[34]=z[34] - n<T>(61,3) - z[18];
    z[42]=static_cast<T>(37)+ z[30];
    z[22]=z[42]*z[22];
    z[22]=n<T>(1,2)*z[34] + z[22];
    z[22]=z[22]*z[24];
    z[34]=n<T>(1,2)*z[6];
    z[42]=static_cast<T>(1)- z[34];
    z[42]=z[42]*z[18];
    z[47]= - n<T>(3,4)*z[3] + n<T>(1,6) + 11*z[8];
    z[22]=z[22] + n<T>(11,8)*z[4] + n<T>(1,2)*z[47] + z[42];
    z[22]=z[22]*z[24];
    z[36]=z[36]*z[16];
    z[36]=z[36] - z[14];
    z[42]=n<T>(15,8) - z[36];
    z[42]=z[2]*z[42];
    z[47]=7*z[12];
    z[49]=z[47] + 1;
    z[51]=n<T>(3,8)*z[6];
    z[52]=z[49]*z[51];
    z[52]=z[52] + static_cast<T>(1)- n<T>(59,8)*z[12];
    z[52]=z[52]*z[31];
    z[53]=3*z[11];
    z[54]= - n<T>(1,2) - z[11];
    z[54]=z[54]*z[53];
    z[55]=npow(z[6],2);
    z[54]=z[54] + n<T>(1,8)*z[55];
    z[54]=z[54]*z[27];
    z[56]= - n<T>(1,2) + z[12];
    z[56]=5*z[56] + n<T>(9,2)*z[3];
    z[17]=z[17] + z[22] + z[54] + z[52] + n<T>(1,8)*z[56] + z[42];
    z[17]=z[9]*z[17];
    z[22]=n<T>(3,2)*z[6];
    z[42]=static_cast<T>(1)+ n<T>(7,2)*z[12];
    z[42]=z[42]*z[22];
    z[52]=5*z[12];
    z[42]= - z[52] + z[42];
    z[42]=z[42]*z[34];
    z[54]=n<T>(37,4)*z[11];
    z[56]=static_cast<T>(7)+ z[54];
    z[56]=z[11]*z[56];
    z[56]= - n<T>(9,2) + z[56];
    z[57]=z[55]*z[12];
    z[58]=n<T>(7,8)*z[57];
    z[59]=z[4]*z[58];
    z[36]=z[59] + z[42] + n<T>(1,4)*z[56] + z[36];
    z[42]= - static_cast<T>(31)+ 11*z[4];
    z[42]=z[42]*z[27];
    z[56]=3*z[1];
    z[42]=z[42] + n<T>(1,6) + z[56];
    z[42]=z[4]*z[42];
    z[59]=z[6] - 1;
    z[42]= - n<T>(9,2)*z[59] + z[42];
    z[60]= - static_cast<T>(1)+ z[27];
    z[60]=z[60]*z[21];
    z[60]=n<T>(1,2) + z[60];
    z[48]=z[60]*npow(z[4],2)*z[48];
    z[42]=n<T>(1,4)*z[42] + z[48];
    z[42]=z[5]*z[42];
    z[48]= - static_cast<T>(1)+ n<T>(7,2)*z[4];
    z[48]=z[48]*z[27];
    z[60]=static_cast<T>(5)+ n<T>(9,4)*z[6];
    z[60]=z[6]*z[60];
    z[48]=z[48] + z[60] + n<T>(11,12) + z[1];
    z[42]=n<T>(1,4)*z[48] + z[42];
    z[42]=z[5]*z[42];
    z[48]= - static_cast<T>(1)- z[22];
    z[48]=z[48]*z[34];
    z[20]=z[42] + z[27] + z[48] - z[8] + z[20];
    z[20]=z[5]*z[20];
    z[17]=z[17] + n<T>(1,4)*z[36] + z[20];
    z[20]= - static_cast<T>(1)- z[3];
    z[20]=z[20]*z[37];
    z[20]=z[20] - static_cast<T>(5)+ n<T>(13,2)*z[12];
    z[36]=z[50] + static_cast<T>(5)+ n<T>(13,4)*z[16];
    z[36]=z[2]*z[36];
    z[37]=z[49]*z[34];
    z[37]=z[37] + n<T>(5,2) - 13*z[12];
    z[37]=z[37]*z[31];
    z[42]=z[11] + n<T>(1,2)*z[55];
    z[42]=z[42]*z[27];
    z[20]=z[42] + z[37] + n<T>(1,4)*z[20] + z[36];
    z[36]=static_cast<T>(5)+ z[44];
    z[36]=z[3]*z[36];
    z[36]= - static_cast<T>(9)+ z[36];
    z[37]=n<T>(5,2) - z[6];
    z[37]=z[6]*z[37];
    z[36]= - z[27] + n<T>(1,2)*z[36] + z[37];
    z[37]=static_cast<T>(9)- n<T>(29,6)*z[4];
    z[37]=z[5]*z[37];
    z[37]=n<T>(1,8)*z[37] + n<T>(1,24)*z[4] - n<T>(1,16)*z[6] - n<T>(3,16)*z[3] - n<T>(1,2)
    - z[8];
    z[37]=z[5]*z[37];
    z[36]=n<T>(1,8)*z[36] + z[37];
    z[36]=z[5]*z[36];
    z[20]=n<T>(1,8)*z[20] + z[36];
    z[20]=z[9]*z[20];
    z[36]=npow(z[11],2);
    z[37]=z[36]*z[32];
    z[14]=z[14] + n<T>(5,4);
    z[42]= - static_cast<T>(1)+ 21*z[12];
    z[42]=z[6]*z[42];
    z[42]= - n<T>(47,2)*z[12] + z[42];
    z[42]=z[6]*z[42];
    z[37]=z[37] + n<T>(1,8)*z[42] - z[16] - z[14];
    z[42]= - n<T>(7,24)*z[4] - n<T>(25,6) - z[18];
    z[44]=n<T>(11,3)*z[5];
    z[48]=z[4]*z[44];
    z[42]=n<T>(1,2)*z[42] + z[48];
    z[24]=z[42]*z[24];
    z[42]= - n<T>(11,24) + 9*z[8];
    z[48]=static_cast<T>(1)+ z[51];
    z[48]=z[6]*z[48];
    z[24]=z[24] + n<T>(1,4)*z[42] + z[48];
    z[24]=z[5]*z[24];
    z[42]= - static_cast<T>(7)- 9*z[6];
    z[42]=z[42]*z[31];
    z[48]=n<T>(1,4) + z[3];
    z[48]=z[3]*z[48];
    z[42]=z[48] + z[42];
    z[24]=n<T>(1,4)*z[42] + z[24];
    z[24]=z[5]*z[24];
    z[20]=z[20] + n<T>(1,4)*z[37] + z[24];
    z[20]=z[9]*z[20];
    z[24]=z[44] - z[22] - z[38] - n<T>(61,12) - 3*z[8];
    z[24]=z[5]*z[24];
    z[31]=static_cast<T>(1)+ z[31];
    z[18]=z[31]*z[18];
    z[31]=static_cast<T>(1)+ n<T>(1,4)*z[3];
    z[31]=z[3]*z[31];
    z[18]=z[24] + z[18] + n<T>(7,24) + z[31];
    z[18]=z[5]*z[18];
    z[24]= - static_cast<T>(1)- z[6];
    z[22]=z[24]*z[22];
    z[24]= - n<T>(1,2) + z[3];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(1,2) + z[24];
    z[24]=z[3]*z[24];
    z[18]=z[18] + z[24] + z[22];
    z[18]=z[5]*z[18];
    z[22]= - n<T>(3,4) - z[3];
    z[16]=z[22]*z[16];
    z[22]= - n<T>(1,2) + z[47];
    z[22]=z[22]*z[34];
    z[22]= - z[52] + z[22];
    z[22]=z[22]*z[34];
    z[14]=z[18] + z[22] + z[16] - z[14];
    z[14]=z[9]*z[14];
    z[16]=npow(z[5],3);
    z[16]=3*z[16];
    z[16]=z[16]*z[8];
    z[18]=z[58] + z[16];
    z[14]=3*z[18] + z[14];
    z[14]=z[14]*z[26];
    z[18]=z[35] - 1;
    z[16]=z[18]*z[16];
    z[22]=z[7]*npow(z[9],2)*z[57];
    z[14]=n<T>(7,16)*z[22] + z[14] + n<T>(1,16)*z[36] + z[16];
    z[14]=z[7]*z[14];
    z[16]=n<T>(1,2) - z[53];
    z[16]=z[11]*z[16];
    z[16]=z[16] + n<T>(7,2)*z[57];
    z[22]= - static_cast<T>(5)+ z[1];
    z[22]=z[8]*z[22];
    z[18]=z[5]*z[18];
    z[18]= - 3*z[18] + z[22];
    z[18]=z[18]*npow(z[5],2);
    z[14]=z[14] + n<T>(1,4)*z[16] + z[18];
    z[14]=z[20] + n<T>(1,4)*z[14];
    z[14]=z[7]*z[14];
    z[14]=n<T>(1,2)*z[17] + z[14];
    z[14]=z[7]*z[14];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[7]*z[14];
    z[15]=z[40]*z[3];
    z[16]=z[15] + n<T>(25,4) + z[56];
    z[16]=z[1]*z[16];
    z[17]=static_cast<T>(63)+ z[29];
    z[17]=z[11]*z[17];
    z[18]=z[54] + 25;
    z[18]=z[18]*z[11];
    z[20]= - n<T>(29,4) - z[18];
    z[20]=z[12]*z[20];
    z[16]=n<T>(1,4)*z[20] + n<T>(1,16)*z[17] + n<T>(7,8) + z[10] + z[16];
    z[15]= - z[35]*z[15];
    z[17]=static_cast<T>(1)- z[10];
    z[15]= - n<T>(5,8)*z[2] + z[15] + n<T>(1,2)*z[17] + z[1];
    z[15]=z[2]*z[15];
    z[17]=static_cast<T>(1)- z[12];
    z[17]=z[17]*z[41];
    z[17]=z[17] - n<T>(3,4) + z[12];
    z[17]=z[17]*z[34];
    z[20]= - z[12] + z[33];
    z[20]=z[20]*z[41];
    z[22]= - n<T>(19,2) - 7*z[1];
    z[20]=n<T>(7,4)*z[4] + z[20] + n<T>(1,2)*z[22] - z[2];
    z[20]=z[20]*z[30];
    z[15]=z[20] + z[17] + n<T>(1,2)*z[16] + z[15];
    z[16]= - n<T>(163,2) - z[29];
    z[16]=z[16]*z[25];
    z[17]=n<T>(237,2) + z[29];
    z[17]=z[11]*z[17];
    z[17]=n<T>(127,2) + z[17];
    z[17]=z[17]*z[39];
    z[16]=z[17] - static_cast<T>(9)+ z[16];
    z[17]=z[10] - n<T>(5,4);
    z[20]=z[2]*z[17];
    z[16]=z[23] + n<T>(1,4)*z[16] + z[20];
    z[16]=z[16]*z[27];
    z[20]=n<T>(5,4)*z[2];
    z[17]= - z[20] - z[17];
    z[17]=z[2]*z[17];
    z[17]=z[17] + static_cast<T>(1)+ z[10];
    z[17]=z[17]*npow(z[2],2);
    z[22]= - static_cast<T>(137)- z[29];
    z[22]=z[22]*z[28];
    z[22]= - static_cast<T>(25)+ z[22];
    z[22]=z[12]*z[22];
    z[18]=z[18] + z[22];
    z[18]=z[18]*z[32];
    z[22]= - z[10]*z[13];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[2]*z[22];
    z[18]=z[22] + z[18];
    z[18]=z[4]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[17]*z[26];
    z[18]=z[20] - n<T>(5,8) + z[10];
    z[13]=z[18]*z[13];
    z[13]=z[13] - static_cast<T>(1)- n<T>(1,2)*z[10];
    z[13]=z[2]*z[13];
    z[18]= - static_cast<T>(1)+ z[30];
    z[18]=z[18]*z[19];
    z[18]=static_cast<T>(9)+ z[18];
    z[18]=z[5]*z[18]*z[32];
    z[13]=z[17] + z[18] + z[13] + z[16];
    z[13]=z[9]*z[13];
    z[16]=npow(z[1],2);
    z[17]= - z[16]*z[59];
    z[18]=static_cast<T>(1)+ n<T>(37,48)*z[1];
    z[18]=z[1]*z[18];
    z[20]=n<T>(59,6)*z[4] - static_cast<T>(15)- n<T>(133,6)*z[1];
    z[20]=z[4]*z[20];
    z[18]=n<T>(1,16)*z[20] + n<T>(31,96) + z[18];
    z[18]=z[4]*z[18];
    z[20]=n<T>(3,32)*z[16];
    z[18]=z[20] + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[20] + z[18];
    z[18]=z[4]*z[18];
    z[20]=z[30] - z[40];
    z[20]=z[4]*z[20];
    z[22]=static_cast<T>(1)+ z[35];
    z[22]=z[1]*z[22];
    z[20]=z[20] + n<T>(1,2) + z[22];
    z[20]=z[5]*z[20]*npow(z[4],4);
    z[17]=n<T>(11,24)*z[20] + n<T>(3,32)*z[17] + z[18];
    z[17]=z[5]*z[17];
    z[18]=n<T>(91,2) + 47*z[1];
    z[18]=z[18]*z[46];
    z[18]=n<T>(5,4) + z[18];
    z[20]=n<T>(113,8)*z[4] - static_cast<T>(19)- n<T>(143,4)*z[1];
    z[20]=z[20]*z[21];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[18]=z[4]*z[18];
    z[20]= - static_cast<T>(1)+ n<T>(5,2)*z[1];
    z[20]=z[20]*z[35];
    z[18]=z[20] + z[18];
    z[18]=z[4]*z[18];
    z[16]=z[16]*z[45];
    z[20]=z[1] - 1;
    z[20]=z[20]*z[1];
    z[16]= - z[20] + z[16];
    z[16]=z[6]*z[16];
    z[21]= - static_cast<T>(1)+ n<T>(7,4)*z[1];
    z[21]=z[1]*z[21];
    z[16]=z[21] + z[16];
    z[16]=n<T>(1,2)*z[16] + z[18];
    z[16]=n<T>(1,8)*z[16] + z[17];
    z[16]=z[5]*z[16];
    z[17]= - n<T>(1,4) - z[43];
    z[17]=z[6]*z[17];
    z[17]=z[17] + n<T>(1,4) + z[20];
    z[18]= - n<T>(43,2) - 25*z[1];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[18]*z[30];
    z[19]=n<T>(59,8) + 5*z[1];
    z[19]=z[1]*z[19];
    z[18]=z[18] - n<T>(7,4) + z[19];
    z[18]=z[4]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[16]=n<T>(1,8)*z[17] + z[16];
    z[16]=z[5]*z[16];

    r += n<T>(1,4)*z[13] + n<T>(1,2)*z[14] + n<T>(1,8)*z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r1254(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1254(const std::array<dd_real,30>&);
#endif
