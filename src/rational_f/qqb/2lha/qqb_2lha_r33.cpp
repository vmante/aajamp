#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r33(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[17];
    z[5]=k[2];
    z[6]=k[4];
    z[7]=k[11];
    z[8]=k[15];
    z[9]= - z[7] + 1;
    z[10]=z[8] + n<T>(1,2)*z[4];
    z[9]=z[7]*z[10]*z[9];
    z[10]=z[4] + z[8];
    z[11]=npow(z[7],2);
    z[12]=z[10]*z[11];
    z[13]=n<T>(1,2)*z[6];
    z[14]=z[13]*z[12];
    z[9]=z[9] + z[14];
    z[9]=z[6]*z[9];
    z[14]=z[7]*z[4];
    z[9]= - z[14] + z[9];
    z[9]=z[6]*z[9];
    z[15]=z[4]*z[1];
    z[16]= - z[15] - z[2] + z[1];
    z[9]=n<T>(1,2)*z[16] + z[9];
    z[16]=npow(z[2],2);
    z[17]=static_cast<T>(1)+ z[2];
    z[17]=z[1]*z[17];
    z[15]= - z[15] - z[16] + z[17];
    z[16]=n<T>(1,3)*z[6];
    z[12]= - z[16]*z[12];
    z[12]= - z[14] + z[12];
    z[12]=z[12]*z[13];
    z[13]= - z[4] + static_cast<T>(1)- n<T>(1,2)*z[2];
    z[12]=n<T>(1,3)*z[13] + z[12];
    z[12]=z[6]*z[12];
    z[12]=n<T>(1,6)*z[15] + z[12];
    z[12]=z[3]*z[12];
    z[9]=n<T>(1,3)*z[9] + z[12];
    z[9]=z[3]*z[9];
    z[12]= - z[4]*z[5];
    z[12]=z[12] + static_cast<T>(1)- z[8];
    z[12]=z[7]*z[12];
    z[10]=z[12] + z[10];
    z[10]=z[7]*z[10];
    z[12]=n<T>(1,2)*z[8] + z[4];
    z[11]=z[6]*z[12]*z[11];
    z[10]=n<T>(1,2)*z[10] + z[11];
    z[10]=z[10]*z[16];
    z[9]=z[10] + z[9];

    r += n<T>(5,3)*z[9];
 
    return r;
}

template double qqb_2lha_r33(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r33(const std::array<dd_real,30>&);
#endif
