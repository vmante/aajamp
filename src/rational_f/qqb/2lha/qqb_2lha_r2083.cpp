#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2083(const std::array<T,30>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[29];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[6];
    z[9]=k[5];
    z[10]=k[24];
    z[11]=k[10];
    z[12]=n<T>(1,4)*z[1];
    z[13]=n<T>(3,2)*z[8];
    z[14]=static_cast<T>(5)+ z[13];
    z[14]=z[14]*z[12];
    z[15]=n<T>(1,4)*z[5];
    z[16]=9*z[5] - static_cast<T>(5)- n<T>(21,2)*z[1];
    z[16]=z[16]*z[15];
    z[17]=3*z[5];
    z[18]=z[17] - 1;
    z[19]=npow(z[5],2);
    z[20]= - z[18]*z[19];
    z[20]=n<T>(3,2) + z[20];
    z[20]=z[9]*z[20];
    z[21]=z[8] - 1;
    z[22]=n<T>(1,2)*z[1];
    z[23]=z[21]*z[22];
    z[24]=static_cast<T>(5)- z[1];
    z[24]=n<T>(1,2)*z[24] + z[5];
    z[24]=z[5]*z[24];
    z[24]=z[23] + z[24];
    z[25]=z[15] + 1;
    z[25]=z[25]*z[5];
    z[26]=n<T>(5,4) - z[25];
    z[26]=z[9]*z[5]*z[26];
    z[24]=n<T>(1,2)*z[24] + z[26];
    z[24]=z[6]*z[24];
    z[26]=n<T>(5,4)*z[8];
    z[14]=z[24] + n<T>(1,4)*z[20] + z[16] + z[14] + static_cast<T>(1)- z[26];
    z[14]=z[6]*z[14];
    z[16]=n<T>(7,2) - z[17];
    z[16]=z[5]*z[16];
    z[16]=n<T>(3,2) + z[16];
    z[16]=z[9]*z[16];
    z[20]= - n<T>(1,4) - z[5];
    z[20]=z[5]*z[20];
    z[20]=n<T>(5,4) + z[20];
    z[20]=z[9]*z[20];
    z[20]=z[5] + z[20];
    z[20]=z[6]*z[20];
    z[16]=z[20] + z[16] + n<T>(9,2)*z[5] - n<T>(15,4) - z[8];
    z[20]=z[5] - 1;
    z[24]=z[6]*z[9];
    z[27]= - z[20]*z[24];
    z[28]=static_cast<T>(5)- z[17];
    z[28]=z[9]*z[28];
    z[27]=z[28] + z[27];
    z[27]=z[7]*z[27];
    z[16]=n<T>(1,4)*z[27] + n<T>(1,2)*z[16];
    z[16]=z[6]*z[16];
    z[27]=n<T>(1,4)*z[8];
    z[28]=static_cast<T>(3)- z[27];
    z[29]=static_cast<T>(2)+ n<T>(1,4)*z[3];
    z[29]=z[3]*z[29];
    z[30]=n<T>(5,4) - z[5];
    z[30]=z[9]*z[30];
    z[16]=3*z[30] + n<T>(1,2)*z[28] + z[29] + z[16];
    z[16]=z[7]*z[16];
    z[28]=3*z[2];
    z[29]= - static_cast<T>(13)+ z[28];
    z[29]=z[29]*z[12];
    z[30]=n<T>(1,2)*z[2];
    z[31]= - static_cast<T>(1)+ z[30];
    z[31]=z[1]*z[31];
    z[31]= - z[30] + z[31];
    z[32]=n<T>(1,2)*z[3];
    z[31]=z[31]*z[32];
    z[29]=z[31] + z[29] + n<T>(7,4) - 4*z[2];
    z[29]=z[3]*z[29];
    z[31]=n<T>(9,2)*z[1];
    z[33]=9*z[2];
    z[34]= - z[31] - static_cast<T>(5)- z[33];
    z[35]=n<T>(1,2)*z[5];
    z[36]= - z[35] - n<T>(1,2) + z[28];
    z[36]=z[5]*z[36];
    z[37]=5*z[2];
    z[38]=static_cast<T>(3)- z[37];
    z[36]=n<T>(1,2)*z[38] + z[36];
    z[36]=z[9]*z[36];
    z[14]=z[16] + z[14] + n<T>(3,2)*z[36] + z[17] + n<T>(1,2)*z[34] + z[29];
    z[14]=z[7]*z[14];
    z[16]=z[10] + 1;
    z[16]=z[16]*z[8];
    z[17]=n<T>(3,8)*z[16];
    z[29]=z[10]*z[11];
    z[34]=n<T>(1,2)*z[29];
    z[36]= - z[34] + static_cast<T>(3)- n<T>(1,2)*z[11];
    z[38]= - n<T>(13,2) + z[33];
    z[38]=z[2]*z[38];
    z[39]=3*z[1] + n<T>(11,2) + z[28];
    z[39]=z[1]*z[39];
    z[36]=z[39] - z[17] + n<T>(3,4)*z[36] + z[38];
    z[38]=n<T>(5,2) - z[2];
    z[38]=z[38]*z[28];
    z[39]=n<T>(13,2) - z[28];
    z[39]=z[39]*z[22];
    z[38]=z[39] - n<T>(5,2) + z[38];
    z[38]=z[38]*z[22];
    z[39]=z[1] + z[2];
    z[40]=z[2] - 1;
    z[39]= - z[3]*z[12]*z[40]*z[39];
    z[41]= - n<T>(5,2) + 2*z[2];
    z[41]=z[2]*z[41];
    z[38]=z[39] + z[38] + n<T>(3,4) + z[41];
    z[38]=z[3]*z[38];
    z[39]=z[8]*z[10];
    z[41]=z[39] + z[29];
    z[42]= - static_cast<T>(11)+ 3*z[41];
    z[27]= - z[12] - static_cast<T>(1)+ z[27];
    z[27]=z[1]*z[27];
    z[27]=n<T>(1,8)*z[42] + 5*z[27];
    z[42]=n<T>(1,2)*z[9];
    z[19]= - z[20]*z[19]*z[42];
    z[20]= - z[22] + z[5];
    z[20]=z[5]*z[20];
    z[19]=z[19] + z[23] + z[20];
    z[19]=z[6]*z[19];
    z[20]=n<T>(3,4)*z[5];
    z[23]=z[5] + z[1];
    z[43]=n<T>(3,2) - z[23];
    z[43]=z[43]*z[20];
    z[44]=n<T>(3,2)*z[1];
    z[45]=static_cast<T>(2)+ z[44];
    z[45]=z[1]*z[45];
    z[43]=z[43] + static_cast<T>(3)+ z[45];
    z[43]=z[5]*z[43];
    z[25]=n<T>(7,8) - z[25];
    z[25]=z[5]*z[25];
    z[25]= - n<T>(1,4) + z[25];
    z[25]=z[9]*z[25];
    z[19]=n<T>(5,4)*z[19] + z[25] + n<T>(1,2)*z[27] + z[43];
    z[19]=z[6]*z[19];
    z[25]=static_cast<T>(1)- z[34];
    z[23]= - n<T>(1,4)*z[39] + n<T>(1,2)*z[25] - z[2] - z[23];
    z[23]=z[23]*z[20];
    z[25]=z[40]*z[37];
    z[25]=static_cast<T>(1)+ z[25];
    z[27]=n<T>(5,4) - z[28];
    z[27]=z[2]*z[27];
    z[27]= - z[15] - n<T>(5,8) + z[27];
    z[27]=z[5]*z[27];
    z[25]=n<T>(1,4)*z[25] + z[27];
    z[25]=z[9]*z[25];
    z[14]=z[14] + z[19] + z[25] + z[23] + n<T>(1,2)*z[36] + z[38];
    z[14]=z[7]*z[14];
    z[18]=z[31] + z[28] - z[18];
    z[15]=z[18]*z[15];
    z[18]= - static_cast<T>(1)+ n<T>(3,2)*z[2];
    z[19]=z[2]*z[18];
    z[19]=n<T>(1,2) + z[19];
    z[19]=z[2]*z[19];
    z[19]= - z[35] + n<T>(1,2) + z[19];
    z[19]=z[19]*z[42];
    z[23]=static_cast<T>(11)- z[33];
    z[23]=z[1]*z[23];
    z[15]=z[19] + z[15] + n<T>(1,8)*z[23] + z[30] + static_cast<T>(1)- n<T>(3,16)*z[41];
    z[15]=z[5]*z[15];
    z[19]=static_cast<T>(3)+ z[1];
    z[20]= - z[20] + static_cast<T>(1)+ n<T>(9,8)*z[1];
    z[20]=z[5]*z[20];
    z[19]=n<T>(1,4)*z[19] + z[20];
    z[19]=z[5]*z[19];
    z[20]=static_cast<T>(1)- z[35];
    z[20]=z[5]*z[20];
    z[20]= - n<T>(1,2) + z[20];
    z[20]=z[20]*z[42];
    z[23]=z[44] + 5;
    z[25]= - z[1]*z[23];
    z[25]= - n<T>(13,2) + z[25];
    z[25]=z[1]*z[25];
    z[25]= - n<T>(13,4) + z[25];
    z[19]=z[20] + n<T>(1,4)*z[25] + z[19];
    z[19]=z[5]*z[19];
    z[20]=z[29] - 1;
    z[25]=z[21]*z[1];
    z[27]=z[25] + z[16] - z[11] + z[20];
    z[19]=n<T>(3,16)*z[27] + z[19];
    z[19]=z[6]*z[19];
    z[27]=static_cast<T>(1)+ n<T>(3,4)*z[1];
    z[22]=z[22]*z[40]*z[27];
    z[30]= - static_cast<T>(2)+ n<T>(3,4)*z[2];
    z[30]=z[2]*z[30];
    z[30]=static_cast<T>(2)+ z[30];
    z[30]=z[2]*z[30];
    z[22]=z[22] - n<T>(3,4) + z[30];
    z[22]=z[3]*z[22];
    z[12]= - z[23]*z[12];
    z[12]=z[22] + z[12] - n<T>(1,4) - z[2];
    z[12]=z[1]*z[12];
    z[22]=static_cast<T>(5)- z[28];
    z[22]=z[2]*z[22];
    z[22]= - static_cast<T>(3)+ z[22];
    z[22]=z[2]*z[22];
    z[17]= - z[17] - n<T>(3,8)*z[20] + z[22];
    z[12]=z[14] + z[19] + n<T>(1,2)*z[17] + z[15] + z[12];
    z[12]=z[4]*z[12];
    z[14]=z[3]*z[8];
    z[15]=3*z[8];
    z[17]=static_cast<T>(1)- z[15];
    z[17]=z[17]*z[14];
    z[17]= - z[8] + z[17];
    z[17]=z[3]*z[17];
    z[17]= - z[15] + z[17];
    z[17]=z[3]*z[17];
    z[19]=npow(z[3],2);
    z[20]=z[19]*z[9];
    z[22]=npow(z[8],2);
    z[23]=n<T>(3,2)*z[22];
    z[28]=z[23]*z[20];
    z[17]=z[17] + z[28];
    z[17]=z[17]*z[42];
    z[28]=static_cast<T>(1)- z[3];
    z[28]=z[3]*z[28];
    z[28]=static_cast<T>(3)+ z[28];
    z[28]=z[3]*z[28];
    z[15]= - z[15] + z[28];
    z[15]=z[15]*z[42];
    z[20]= - z[8] + n<T>(3,4)*z[20];
    z[20]=z[20]*z[24];
    z[15]=z[15] + z[20];
    z[15]=z[6]*z[15];
    z[20]=z[22]*npow(z[9],2);
    z[24]=npow(z[24],2);
    z[20]=z[20] + z[24];
    z[24]=n<T>(1,2)*z[7];
    z[20]=z[24]*z[20]*npow(z[3],3);
    z[15]=z[20] + z[17] + z[15];
    z[15]=z[15]*z[24];
    z[17]=z[3]*z[42];
    z[17]= - 5*z[8] + z[17];
    z[17]=z[6]*z[17]*z[42];
    z[20]= - static_cast<T>(3)- z[32];
    z[20]=z[3]*z[20];
    z[24]= - n<T>(3,2) - z[3];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(5,2)*z[8] + z[24];
    z[24]=z[9]*z[24];
    z[17]=z[17] + z[20] + z[24];
    z[17]=z[6]*z[17];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[13]*z[14];
    z[13]=n<T>(1,2)*z[8] + z[13];
    z[13]=z[13]*z[19];
    z[13]=z[13] + z[17];
    z[17]=static_cast<T>(1)- z[23];
    z[17]=z[17]*z[32];
    z[17]=z[17] + static_cast<T>(3)- z[26];
    z[17]=z[3]*z[17];
    z[20]=npow(z[10],2);
    z[24]=z[20] + z[39];
    z[24]=z[24]*z[8];
    z[20]=z[20]*z[11];
    z[20]=z[24] + z[20];
    z[22]= - z[3]*z[22];
    z[22]=z[22] - z[20];
    z[22]=z[9]*z[22];
    z[17]=z[17] + n<T>(3,8)*z[22];
    z[17]=z[9]*z[17];
    z[13]=z[15] + z[17] + n<T>(1,2)*z[13];
    z[13]=z[7]*z[13];
    z[15]=n<T>(3,8)*z[8];
    z[15]=z[21]*z[15];
    z[15]=z[15] - z[18];
    z[15]=z[3]*z[15];
    z[15]=n<T>(3,8)*z[20] + z[15];
    z[15]=z[9]*z[15];
    z[14]= - z[14]*z[25];
    z[14]=z[23] + z[14];
    z[14]=z[14]*z[19];
    z[16]=z[16] + z[29];
    z[16]= - 3*z[16] + z[3];
    z[16]=z[9]*z[16];
    z[17]=z[3]*z[27];
    z[16]=n<T>(1,8)*z[16] + n<T>(3,2) + z[17];
    z[16]=z[6]*z[16];
    z[13]=z[13] + z[16] + n<T>(1,4)*z[14] + z[15];

    r += z[12] + n<T>(1,2)*z[13];
 
    return r;
}

template double qqb_2lha_r2083(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2083(const std::array<dd_real,30>&);
#endif
