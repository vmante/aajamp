#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r995(const std::array<T,30>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[20];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[6];
    z[8]=k[5];
    z[9]=k[10];
    z[10]=k[24];
    z[11]=npow(z[3],2);
    z[12]=static_cast<T>(3)+ z[3];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,3) + z[12];
    z[12]=z[12]*z[11];
    z[13]=npow(z[3],3);
    z[14]= - n<T>(1,2) - z[3];
    z[14]=z[14]*z[13];
    z[15]=z[2]*npow(z[3],4);
    z[14]=z[14] + n<T>(1,4)*z[15];
    z[15]=n<T>(1,3)*z[2];
    z[14]=z[14]*z[15];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[12]=z[2]*z[12];
    z[14]=n<T>(1,3)*z[3];
    z[16]= - n<T>(7,2) - z[14];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(1,3) + z[16];
    z[16]=z[16]*z[11];
    z[12]=z[16] + z[12];
    z[12]=z[2]*z[12];
    z[16]=n<T>(1,2)*z[3];
    z[17]=static_cast<T>(19)+ z[16];
    z[17]=z[17]*z[16];
    z[17]=static_cast<T>(2)+ z[17];
    z[17]=z[17]*z[11];
    z[18]= - static_cast<T>(1)- z[3];
    z[18]=z[18]*z[11];
    z[19]= - static_cast<T>(1)- z[16];
    z[19]=z[3]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[4]*z[19];
    z[18]=z[18] + z[19];
    z[18]=z[4]*z[18];
    z[12]=z[18] + n<T>(1,3)*z[17] + z[12];
    z[12]=z[1]*z[12];
    z[17]=z[5] + 5;
    z[18]=z[3]*z[5];
    z[19]=n<T>(1,2)*z[17] + z[18];
    z[19]=z[3]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[11];
    z[20]=n<T>(1,2)*z[2];
    z[13]= - z[13]*z[20];
    z[13]=z[19] + z[13];
    z[13]=z[13]*z[15];
    z[15]=n<T>(5,3)*z[5];
    z[19]= - z[18] - n<T>(3,2) - z[15];
    z[19]=z[3]*z[19];
    z[21]= - static_cast<T>(7)- z[5];
    z[19]=n<T>(1,3)*z[21] + z[19];
    z[19]=z[3]*z[19];
    z[19]= - n<T>(1,3) + z[19];
    z[19]=z[3]*z[19];
    z[13]=z[19] + z[13];
    z[13]=z[2]*z[13];
    z[19]=static_cast<T>(7)+ 41*z[5];
    z[19]=n<T>(1,6)*z[19] + z[18];
    z[19]=z[3]*z[19];
    z[21]=n<T>(3,2)*z[5];
    z[19]=z[19] + n<T>(14,3) + z[21];
    z[19]=z[3]*z[19];
    z[22]=n<T>(1,3)*z[5];
    z[19]=z[19] + n<T>(3,4) + z[22];
    z[19]=z[3]*z[19];
    z[13]=z[19] + z[13];
    z[13]=z[2]*z[13];
    z[19]=z[5] + 1;
    z[23]=4*z[18] + z[19];
    z[23]=z[3]*z[23];
    z[23]=n<T>(3,2) + z[23];
    z[23]=z[3]*z[23];
    z[24]=2*z[5];
    z[25]=2*z[18] + n<T>(1,2) + z[24];
    z[25]=z[3]*z[25];
    z[25]=z[25] + n<T>(3,2) + z[5];
    z[25]=z[4]*z[25];
    z[23]=z[23] + z[25];
    z[23]=z[4]*z[23];
    z[25]=n<T>(1,2)*z[5];
    z[26]= - z[18] - static_cast<T>(1)- 28*z[5];
    z[26]=z[3]*z[26];
    z[26]=z[26] - static_cast<T>(11)- z[25];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(17,4) + z[26];
    z[26]=z[26]*z[14];
    z[27]=n<T>(1,4)*z[7];
    z[28]= - static_cast<T>(1)+ z[7];
    z[28]=z[27]*z[3]*z[28];
    z[12]=z[12] + z[28] + z[23] + z[26] + z[13];
    z[12]=z[1]*z[12];
    z[13]=n<T>(1,2)*z[19];
    z[23]=z[25] + 1;
    z[26]=z[23]*z[5];
    z[26]=z[26] + n<T>(1,2);
    z[28]=z[7]*z[26];
    z[28]= - z[13] + z[28];
    z[28]=z[7]*z[28];
    z[29]=z[10]*z[7];
    z[30]=z[26]*z[29];
    z[31]=z[10]*z[26];
    z[31]= - z[13] + z[31];
    z[31]=z[9]*z[31];
    z[28]=z[31] + z[28] + z[30];
    z[28]=n<T>(3,4)*z[28];
    z[28]=z[10]*z[28];
    z[30]=npow(z[5],2);
    z[31]=z[30]*z[8];
    z[32]=z[31] - z[25];
    z[32]=z[32]*z[8];
    z[33]=static_cast<T>(1)- z[21];
    z[33]=z[33]*z[31];
    z[34]= - n<T>(1,2) + z[24];
    z[34]=z[5]*z[34];
    z[33]=z[34] + z[33];
    z[33]=z[8]*z[33];
    z[34]=static_cast<T>(1)+ 3*z[5];
    z[33]= - n<T>(1,2)*z[34] + z[33];
    z[33]=z[4]*z[33];
    z[33]= - z[32] + z[33];
    z[33]=z[4]*z[33];
    z[35]=npow(z[5],3);
    z[36]=z[35]*z[8];
    z[37]= - z[30] + z[36];
    z[38]=n<T>(1,2)*z[8];
    z[37]=z[7]*z[37]*z[38];
    z[32]= - z[32] + z[37];
    z[27]=z[32]*z[27];
    z[32]=npow(z[8],2);
    z[37]= - z[6]*z[32]*z[25];
    z[39]=z[5] - n<T>(3,2)*z[31];
    z[39]=z[8]*z[39];
    z[37]=z[37] - n<T>(1,2) + z[39];
    z[37]=z[6]*z[37];
    z[37]=z[37] + z[1];
    z[37]=z[37]*npow(z[4],2);
    z[39]=n<T>(5,8)*z[5];
    z[40]=z[32]*z[39];
    z[27]=z[27] + z[40] + z[33] + z[28] + z[37];
    z[27]=z[6]*z[27];
    z[28]= - n<T>(9,2) + z[24];
    z[28]=z[28]*z[30];
    z[33]=5*z[5];
    z[37]=static_cast<T>(3)- z[33];
    z[37]=z[37]*z[30];
    z[40]=z[35]*z[3];
    z[37]=z[37] + 4*z[40];
    z[37]=z[3]*z[37];
    z[28]=z[28] + z[37];
    z[28]=z[3]*z[28];
    z[37]=static_cast<T>(3)- z[5];
    z[37]=z[37]*z[35];
    z[41]=z[24] - 1;
    z[42]=z[41]*z[35];
    z[43]=npow(z[5],4);
    z[44]=z[43]*z[3];
    z[42]=z[42] - z[44];
    z[42]=z[3]*z[42];
    z[37]=z[37] + z[42];
    z[37]=z[3]*z[37];
    z[41]= - z[41]*z[30];
    z[37]=z[41] + z[37];
    z[37]=z[8]*z[37];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[33]*z[25];
    z[28]=z[37] + z[33] + z[28];
    z[28]=z[8]*z[28];
    z[33]= - n<T>(1,2) + z[5];
    z[33]=z[33]*z[35];
    z[37]= - z[43]*z[16];
    z[33]=z[33] + z[37];
    z[33]=z[3]*z[33];
    z[37]=z[25] - 2;
    z[41]= - z[37]*z[35];
    z[33]=z[41] + z[33];
    z[33]=z[8]*z[33];
    z[41]= - n<T>(5,2) + z[5];
    z[41]=z[41]*z[30];
    z[24]=n<T>(3,2) - z[24];
    z[24]=z[24]*z[30];
    z[24]=z[24] + 2*z[40];
    z[24]=z[3]*z[24];
    z[24]=z[33] + z[41] + z[24];
    z[24]=z[8]*z[24];
    z[33]=z[30]*z[3];
    z[41]= - z[25] - z[33];
    z[42]=3*z[3];
    z[41]=z[41]*z[42];
    z[19]= - z[5]*z[19];
    z[19]=z[24] + z[19] + z[41];
    z[19]=z[4]*z[19];
    z[24]= - static_cast<T>(1)+ z[5];
    z[24]=z[5]*z[24];
    z[24]=z[24] - 2*z[33];
    z[24]=z[24]*z[42];
    z[24]= - z[30] + z[24];
    z[24]=z[3]*z[24];
    z[13]=z[19] + z[28] - z[13] + z[24];
    z[13]=z[4]*z[13];
    z[19]=n<T>(8,3)*z[5];
    z[24]= - static_cast<T>(1)- z[19];
    z[24]=z[24]*z[30];
    z[28]= - z[35]*z[14];
    z[24]=z[24] + z[28];
    z[24]=z[3]*z[24];
    z[28]=n<T>(5,3) + z[5];
    z[28]=z[5]*z[28];
    z[28]= - n<T>(1,4) + z[28];
    z[28]=z[5]*z[28];
    z[24]=z[28] + z[24];
    z[24]=z[3]*z[24];
    z[19]=n<T>(7,4) - z[19];
    z[19]=z[5]*z[19];
    z[19]=z[24] + n<T>(1,8) + z[19];
    z[19]=z[3]*z[19];
    z[24]=n<T>(1,4)*z[5];
    z[28]= - static_cast<T>(8)+ z[24];
    z[28]=z[28]*z[22];
    z[28]=n<T>(1,8) + z[28];
    z[28]=z[28]*z[30];
    z[41]=z[25] - 1;
    z[42]= - z[41]*z[35];
    z[42]=z[42] + n<T>(1,4)*z[44];
    z[42]=z[42]*z[14];
    z[28]=z[28] + z[42];
    z[28]=z[3]*z[28];
    z[42]= - n<T>(9,4) + n<T>(7,3)*z[5];
    z[42]=z[5]*z[42];
    z[42]= - n<T>(1,8) + z[42];
    z[42]=z[5]*z[42];
    z[28]=z[42] + z[28];
    z[28]=z[3]*z[28];
    z[28]=n<T>(13,8)*z[30] + z[28];
    z[28]=z[8]*z[28];
    z[19]=z[28] - z[39] + z[19];
    z[19]=z[8]*z[19];
    z[28]=z[16]*z[30];
    z[39]=static_cast<T>(4)+ z[25];
    z[39]=z[39]*z[22];
    z[39]=z[39] + z[28];
    z[39]=z[3]*z[39];
    z[42]=static_cast<T>(5)+ z[24];
    z[42]=z[42]*z[22];
    z[39]=z[39] + n<T>(7,8) + z[42];
    z[39]=z[3]*z[39];
    z[42]=z[5] + n<T>(5,2);
    z[39]=n<T>(1,3)*z[42] + z[39];
    z[39]=z[3]*z[39];
    z[43]=z[5] + z[28];
    z[43]=z[43]*z[11];
    z[44]=n<T>(1,4)*z[33];
    z[45]=z[5] + z[44];
    z[45]=z[8]*z[3]*z[45];
    z[43]=z[43] + z[45];
    z[43]=z[8]*z[43];
    z[45]= - n<T>(1,3)*z[18] - static_cast<T>(1)- z[22];
    z[45]=z[3]*z[45];
    z[45]= - n<T>(1,3) + z[45];
    z[45]=z[3]*z[45];
    z[11]=z[2]*z[11];
    z[11]=z[45] + n<T>(1,6)*z[11];
    z[11]=z[11]*z[20];
    z[11]=z[11] + n<T>(1,3)*z[43] + n<T>(1,6) + z[39];
    z[11]=z[2]*z[11];
    z[37]= - z[37]*z[30];
    z[16]=z[35]*z[16];
    z[16]=z[37] + z[16];
    z[16]=z[16]*z[14];
    z[15]=n<T>(1,4) - z[15];
    z[15]=z[5]*z[15];
    z[15]=z[15] + z[16];
    z[15]=z[3]*z[15];
    z[15]= - n<T>(3,4)*z[5] + z[15];
    z[15]=z[8]*z[15];
    z[16]=static_cast<T>(5)- z[5];
    z[16]=z[16]*z[30];
    z[16]=n<T>(1,2)*z[16] + z[40];
    z[16]=z[3]*z[16];
    z[37]= - n<T>(1,4) - 4*z[5];
    z[37]=z[5]*z[37];
    z[16]=z[37] + z[16];
    z[14]=z[16]*z[14];
    z[14]=z[14] - n<T>(1,8) + n<T>(2,3)*z[5];
    z[14]=z[3]*z[14];
    z[14]=z[14] + z[15];
    z[14]=z[8]*z[14];
    z[15]=19*z[5];
    z[16]= - static_cast<T>(13)- z[15];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,6)*z[16] - z[33];
    z[16]=z[3]*z[16];
    z[37]= - static_cast<T>(19)- 7*z[5];
    z[37]=z[5]*z[37];
    z[37]= - n<T>(7,2) + z[37];
    z[16]=n<T>(1,6)*z[37] + z[16];
    z[16]=z[3]*z[16];
    z[17]= - z[5]*z[17];
    z[17]= - n<T>(17,2) + z[17];
    z[16]=n<T>(1,6)*z[17] + z[16];
    z[16]=z[3]*z[16];
    z[17]=z[5] + n<T>(5,4);
    z[11]=z[11] + z[14] - n<T>(1,3)*z[17] + z[16];
    z[11]=z[2]*z[11];
    z[14]=z[21] + 1;
    z[16]=z[14]*z[30];
    z[14]= - z[14]*z[35];
    z[14]=z[14] - z[40];
    z[14]=z[14]*z[38];
    z[14]=z[14] + z[16] + n<T>(3,2)*z[33];
    z[14]=z[8]*z[14];
    z[16]=z[5] + n<T>(3,4);
    z[14]=z[14] - n<T>(3,2)*z[18] + z[16];
    z[14]=z[7]*z[14];
    z[21]=z[5] + n<T>(1,2);
    z[33]=z[3]*z[21];
    z[14]=z[14] + z[33] + z[41];
    z[33]= - static_cast<T>(1)- 13*z[5];
    z[33]=z[33]*z[24];
    z[18]= - z[23]*z[18];
    z[18]=z[33] + z[18];
    z[23]=n<T>(1,4) + z[5];
    z[23]=z[23]*z[30];
    z[23]=z[23] + z[44];
    z[23]=z[8]*z[23];
    z[18]=n<T>(1,2)*z[18] + z[23];
    z[18]=z[8]*z[18];
    z[14]=z[18] + n<T>(1,2)*z[14];
    z[14]=z[7]*z[14];
    z[18]=z[8]*z[5]*z[21];
    z[18]= - n<T>(1,4)*z[34] + z[18];
    z[18]=z[8]*z[18];
    z[21]= - z[2]*z[32]*z[24];
    z[18]=z[18] + z[21];
    z[18]=z[18]*z[20];
    z[15]=z[15] + 23;
    z[15]=z[15]*z[24];
    z[15]=z[15] + 1;
    z[15]=z[15]*z[25];
    z[17]=z[17]*z[5];
    z[17]=z[17] + n<T>(1,4);
    z[17]=z[17]*z[31];
    z[15]=z[15] - z[17];
    z[15]=z[15]*z[8];
    z[17]=static_cast<T>(13)+ 11*z[5];
    z[17]=z[17]*z[25];
    z[17]=z[17] + 1;
    z[15]=z[15] - n<T>(1,4)*z[17];
    z[17]= - z[36] + 3*z[30];
    z[17]=z[38]*z[17];
    z[17]=z[17] - z[5];
    z[17]=z[26]*z[17];
    z[17]=n<T>(3,2)*z[17];
    z[20]=z[10]*z[17];
    z[20]=z[20] - z[15];
    z[20]=z[10]*z[20];
    z[21]= - static_cast<T>(1)- n<T>(5,2)*z[5];
    z[16]= - z[5]*z[16];
    z[16]= - n<T>(1,8) + z[16];
    z[16]=z[8]*z[16];
    z[16]=z[16] + static_cast<T>(1)+ n<T>(13,8)*z[5];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,8) + z[16];
    z[16]=z[8]*z[16];
    z[16]=z[20] + z[18] + n<T>(1,4)*z[21] + z[16];
    z[16]=z[9]*z[16];
    z[18]=static_cast<T>(14)- n<T>(19,4)*z[5];
    z[18]=z[18]*z[22];
    z[20]=static_cast<T>(1)+ 9*z[5];
    z[20]=z[5]*z[20];
    z[20]=z[20] + z[28];
    z[20]=z[3]*z[20];
    z[18]=z[20] + n<T>(1,8) + z[18];
    z[18]=z[3]*z[18];
    z[20]=static_cast<T>(1)+ n<T>(7,2)*z[5];
    z[18]=n<T>(1,2)*z[20] + z[18];
    z[18]=z[3]*z[18];
    z[20]=z[7]*z[17];
    z[15]=z[20] - z[15];
    z[15]=z[7]*z[15];
    z[17]=z[29]*z[17];
    z[15]=z[15] + z[17];
    z[15]=z[10]*z[15];
    z[17]=z[42]*z[22];
    z[17]=n<T>(7,4) + z[17];

    r += z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + n<T>(1,2)*z[17] + 
      z[18] + z[19] + z[27];
 
    return r;
}

template double qqb_2lha_r995(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r995(const std::array<dd_real,30>&);
#endif
