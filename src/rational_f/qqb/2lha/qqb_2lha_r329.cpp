#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r329(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[2];
    z[5]=k[11];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=k[10];
    z[9]=k[12];
    z[10]=3*z[5];
    z[11]=z[10] - 11;
    z[12]=n<T>(1,4)*z[5];
    z[11]=z[11]*z[12];
    z[13]=z[11] + n<T>(5,2);
    z[13]=z[7]*z[13];
    z[14]=n<T>(1,4)*z[9];
    z[15]=3*z[3];
    z[16]=n<T>(5,2) - z[15];
    z[16]=z[3]*z[16];
    z[16]=z[5] + z[14] + z[16];
    z[16]=z[2]*z[16];
    z[17]=n<T>(1,2)*z[9];
    z[18]=z[7] + z[17];
    z[19]=n<T>(3,2) - z[3];
    z[19]=z[3]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[6]*z[18];
    z[19]=z[4]*z[7];
    z[20]=n<T>(1,2) - z[3];
    z[20]=z[3]*z[20];
    z[13]=z[18] + z[16] + z[20] - n<T>(1,2)*z[19] + z[17] + static_cast<T>(1)+ z[13];
    z[16]=n<T>(1,2)*z[6];
    z[13]=z[13]*z[16];
    z[18]=n<T>(1,2)*z[5];
    z[20]=static_cast<T>(1)+ z[5];
    z[20]=z[20]*z[18];
    z[15]=n<T>(1,2) - z[15];
    z[15]=z[3]*z[15];
    z[15]=z[15] + z[20];
    z[20]=n<T>(1,2)*z[2];
    z[15]=z[15]*z[20];
    z[21]=z[3] + n<T>(1,2);
    z[21]=z[21]*z[3];
    z[15]=z[15] + n<T>(5,4)*z[5] + z[14] - z[21];
    z[15]=z[2]*z[15];
    z[22]=z[14] - 1;
    z[11]= - z[11] - z[3] + z[22];
    z[11]=z[13] + n<T>(1,2)*z[11] + z[15];
    z[11]=z[6]*z[11];
    z[13]= - static_cast<T>(3)+ n<T>(5,2)*z[5];
    z[13]=z[13]*z[18];
    z[15]= - static_cast<T>(1)+ z[5];
    z[15]=z[15]*z[18];
    z[15]= - z[21] + z[15];
    z[15]=z[2]*z[15];
    z[21]=z[3] + n<T>(3,2);
    z[23]= - z[3]*z[21];
    z[13]=z[15] + z[23] + z[13];
    z[13]=z[2]*z[13];
    z[15]= - static_cast<T>(5)+ z[10];
    z[15]=z[15]*z[12];
    z[13]=z[13] + z[15] + z[14] - z[3];
    z[13]=z[13]*z[20];
    z[11]=z[13] + z[11];
    z[11]=z[8]*z[11];
    z[13]= - static_cast<T>(1)+ n<T>(3,8)*z[5];
    z[15]=z[5]*z[13];
    z[11]=z[11] + n<T>(1,4) + z[15];
    z[13]= - z[7]*z[13];
    z[15]=n<T>(3,4)*z[3];
    z[13]= - z[15] - n<T>(1,4)*z[19] + z[14] + n<T>(1,2) + z[13];
    z[14]=z[5] - n<T>(1,2);
    z[19]=z[14]*z[18];
    z[23]=z[3] + n<T>(1,4);
    z[23]=z[23]*z[3];
    z[19]=z[19] - z[23];
    z[24]=z[19]*z[2];
    z[25]= - static_cast<T>(3)+ n<T>(7,2)*z[5];
    z[12]=z[25]*z[12];
    z[25]= - n<T>(7,8) - z[3];
    z[25]=z[3]*z[25];
    z[12]=z[24] + z[12] + n<T>(1,16)*z[9] + z[25];
    z[12]=z[2]*z[12];
    z[25]=z[7] + z[9];
    z[26]=static_cast<T>(1)- z[3];
    z[26]=z[3]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[16]=z[24]*z[16];
    z[12]=z[16] + n<T>(1,2)*z[13] + z[12];
    z[12]=z[6]*z[12];
    z[13]= - z[21]*z[15];
    z[15]=z[19]*z[20];
    z[16]= - n<T>(9,8) + z[5];
    z[16]=z[5]*z[16];
    z[13]=z[15] + z[13] + z[16];
    z[13]=z[2]*z[13];
    z[15]=z[17] - 9*z[3];
    z[16]= - n<T>(27,16) + z[5];
    z[16]=z[5]*z[16];
    z[13]=z[13] + n<T>(1,8)*z[15] + z[16];
    z[13]=z[2]*z[13];
    z[11]=z[12] + z[13] + n<T>(1,2)*z[11];
    z[11]=z[8]*z[11];
    z[12]=z[4] - n<T>(3,2);
    z[13]=z[4] - 3;
    z[13]=z[13]*z[4];
    z[15]=n<T>(5,2) + z[13];
    z[15]=z[5]*z[15];
    z[15]=n<T>(3,2)*z[15] + z[12];
    z[15]=z[5]*z[15];
    z[15]=z[15] - n<T>(1,2)*z[3] + z[22];
    z[14]=z[14]*z[5];
    z[16]= - z[23] + z[14];
    z[16]=z[2]*z[16];
    z[10]= - z[12]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[5]*z[10];
    z[12]=z[6]*npow(z[5],2);
    z[10]=z[10] + z[12];
    z[10]=z[6]*z[10];
    z[10]=n<T>(1,4)*z[10] + n<T>(1,2)*z[15] + z[16];
    z[10]=z[6]*z[10];
    z[12]=n<T>(7,2) - z[4];
    z[12]=z[5]*z[12];
    z[12]=z[12] - static_cast<T>(3)+ n<T>(1,2)*z[4];
    z[12]=z[12]*z[18];
    z[15]= - static_cast<T>(3)- z[1];
    z[15]=z[3]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[3]*z[15];
    z[14]=z[15] + z[14];
    z[14]=z[14]*z[20];
    z[15]= - static_cast<T>(3)- n<T>(5,4)*z[1];
    z[15]=z[3]*z[15];
    z[12]=z[14] + z[12] - n<T>(1,4) + z[15];
    z[12]=z[2]*z[12];
    z[14]=n<T>(9,2) - z[4];
    z[14]=z[4]*z[14];
    z[14]= - n<T>(15,2) + z[14];
    z[14]=z[4]*z[14];
    z[14]=n<T>(11,2) + z[14];
    z[14]=z[5]*z[14];
    z[13]=z[14] - n<T>(9,2) - z[13];
    z[13]=z[13]*z[18];
    z[14]= - n<T>(13,2) - 3*z[1];
    z[13]=z[13] + n<T>(1,2)*z[14] + z[4];
    z[10]=z[10] + n<T>(1,2)*z[13] + z[12];

    r += n<T>(1,2)*z[10] + z[11];
 
    return r;
}

template double qqb_2lha_r329(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r329(const std::array<dd_real,30>&);
#endif
