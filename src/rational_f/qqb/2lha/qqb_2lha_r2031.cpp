#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2031(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[4];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[7];
    z[5]=k[13];
    z[6]=k[24];
    z[7]=k[10];
    z[8]=k[17];
    z[9]=k[9];
    z[10]=n<T>(3,2)*z[3];
    z[11]=z[10]*z[6];
    z[12]=npow(z[8],2);
    z[13]=static_cast<T>(1)- z[6];
    z[13]=z[7]*z[13];
    z[14]=static_cast<T>(1)- z[10];
    z[14]=z[4]*z[14];
    z[15]= - n<T>(1,2)*z[5] + z[8];
    z[15]=z[2]*z[15];
    z[13]=z[15] + z[14] - z[11] + n<T>(3,2)*z[13] - z[12];
    z[13]=z[2]*z[13];
    z[14]=z[3] - 1;
    z[15]=npow(z[4],2);
    z[14]=z[14]*z[15];
    z[16]=z[12]*z[9];
    z[13]=z[13] + z[16] + z[14];
    z[17]=n<T>(3,2)*z[6];
    z[17]=z[7]*z[17];
    z[10]=z[10] + static_cast<T>(1)- n<T>(3,2)*z[5];
    z[10]=z[4]*z[10];
    z[10]=z[10] + z[11] + z[17] + z[8];
    z[10]=z[2]*z[10];
    z[10]=z[12] - z[10];
    z[10]= - z[14] - n<T>(1,2)*z[10];
    z[10]=z[2]*z[10];
    z[11]=n<T>(1,2)*z[1];
    z[12]= - z[5] + z[3];
    z[12]=z[12]*z[15]*npow(z[2],2)*z[11];
    z[10]=z[12] + n<T>(1,2)*z[16] + z[10];
    z[10]=z[1]*z[10];
    z[10]=n<T>(1,2)*z[13] + z[10];

    r += z[11]*z[10];
 
    return r;
}

template double qqb_2lha_r2031(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2031(const std::array<dd_real,30>&);
#endif
