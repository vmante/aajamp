#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r732(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[9];
    z[6]= - static_cast<T>(5)+ z[5];
    z[6]=z[5]*z[6];
    z[7]= - static_cast<T>(1)+ 3*z[4];
    z[7]=z[5]*z[7];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=z[3]*z[7];
    z[6]=z[6] + z[7];
    z[7]=n<T>(1,2)*z[3];
    z[8]= - z[5]*z[4];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[5]*z[8]*z[7];
    z[9]=npow(z[5],2);
    z[8]=z[9] + z[8];
    z[8]=z[2]*z[8];
    z[6]=n<T>(1,2)*z[6] + z[8];
    z[6]=z[2]*z[6];
    z[8]= - z[4] + z[1];
    z[7]=z[8]*z[7];
    z[6]=z[6] + z[7] + static_cast<T>(1)- z[5];
    z[6]=z[2]*z[6];
    z[7]=z[1] - 1;
    z[8]=z[4] + z[7];
    z[8]=z[3]*z[1]*z[8];
    z[7]=z[8] - z[7];
    z[6]=n<T>(1,2)*z[7] + z[6];

    r += n<T>(1,4)*z[6]*z[2];
 
    return r;
}

template double qqb_2lha_r732(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r732(const std::array<dd_real,30>&);
#endif
