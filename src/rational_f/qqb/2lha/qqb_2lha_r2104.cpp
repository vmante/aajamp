#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2104(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[5];
    z[8]=k[13];
    z[9]=npow(z[3],2);
    z[10]= - n<T>(3,4) + z[3];
    z[10]=z[10]*z[9];
    z[11]=n<T>(1,2)*z[5];
    z[12]=npow(z[3],3);
    z[13]=z[11]*z[12];
    z[14]=n<T>(5,4)*z[3];
    z[15]= - static_cast<T>(1)+ z[14];
    z[15]=z[15]*z[13];
    z[10]=z[10] + z[15];
    z[10]=z[5]*z[10];
    z[15]=n<T>(3,8)*z[3];
    z[16]=n<T>(3,2)*z[3];
    z[17]= - static_cast<T>(1)+ z[16];
    z[17]=z[17]*z[15];
    z[10]=z[17] + z[10];
    z[10]=z[5]*z[10];
    z[17]=z[3] - n<T>(1,2);
    z[10]=n<T>(1,8)*z[17] + z[10];
    z[10]=z[4]*z[10];
    z[18]=npow(z[5],2);
    z[19]=npow(z[3],4);
    z[20]=z[18]*z[19];
    z[20]= - z[20] + n<T>(3,2)*z[9];
    z[20]=z[20]*z[5];
    z[20]=z[20] + z[3];
    z[10]=n<T>(1,8)*z[20] + z[10];
    z[10]=z[5]*z[10];
    z[21]=z[3] - 1;
    z[22]=z[21]*z[9];
    z[23]= - z[21]*z[13];
    z[23]= - z[22] + z[23];
    z[23]=z[5]*z[23];
    z[24]=n<T>(3,4)*z[3];
    z[25]= - z[21]*z[24];
    z[23]=z[25] + z[23];
    z[23]=z[5]*z[23];
    z[23]= - n<T>(1,4)*z[21] + z[23];
    z[18]=z[4]*z[23]*z[18];
    z[23]=z[13] + z[9];
    z[23]=z[23]*z[5];
    z[23]=z[23] + z[24];
    z[23]=z[23]*z[5];
    z[23]=z[23] + n<T>(1,4);
    z[25]=z[23]*z[5];
    z[18]= - z[25] + z[18];
    z[18]=z[7]*z[18];
    z[10]=z[10] + n<T>(1,4)*z[18];
    z[10]=z[7]*z[4]*z[10];
    z[18]=5*z[3];
    z[26]=static_cast<T>(3)+ z[18];
    z[26]=z[26]*z[9];
    z[27]=z[12]*z[5];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[27];
    z[14]=z[26] + z[14];
    z[14]=z[5]*z[14];
    z[26]= - static_cast<T>(3)- n<T>(13,2)*z[3];
    z[26]=z[26]*z[27];
    z[28]=z[19]*z[1];
    z[26]=n<T>(15,4)*z[28] - n<T>(39,4)*z[12] + z[26];
    z[29]=n<T>(1,2)*z[1];
    z[26]=z[26]*z[29];
    z[14]=z[26] + n<T>(33,8)*z[9] + z[14];
    z[14]=z[1]*z[14];
    z[26]=z[19]*z[5];
    z[28]= - n<T>(3,2)*z[28] + n<T>(9,2)*z[12] + z[26];
    z[28]=z[28]*z[29];
    z[28]=z[28] - n<T>(9,4)*z[9] - z[27];
    z[28]=z[1]*z[28];
    z[29]=z[9]*z[5];
    z[30]=z[16] + z[29];
    z[28]=n<T>(1,2)*z[30] + z[28];
    z[28]=z[2]*z[28];
    z[30]= - static_cast<T>(1)- z[24];
    z[30]=z[30]*z[29];
    z[31]=n<T>(7,2)*z[3];
    z[32]= - static_cast<T>(3)- z[31];
    z[32]=z[3]*z[32];
    z[30]=n<T>(1,2)*z[32] + z[30];
    z[30]=z[5]*z[30];
    z[14]=z[28] + z[14] - n<T>(9,8)*z[3] + z[30];
    z[14]=z[2]*z[14];
    z[28]=3*z[9];
    z[17]= - z[17]*z[28];
    z[28]=static_cast<T>(3)- z[18];
    z[28]=z[28]*z[13];
    z[17]=z[17] + z[28];
    z[17]=z[5]*z[17];
    z[28]=3*z[3];
    z[30]=static_cast<T>(1)- z[28];
    z[15]=z[30]*z[15];
    z[15]=z[15] + z[17];
    z[15]=z[5]*z[15];
    z[17]=n<T>(1,8)*z[3];
    z[15]= - z[17] + z[15];
    z[15]=z[4]*z[15];
    z[30]=n<T>(3,2)*z[12] + z[26];
    z[30]=z[5]*z[30];
    z[30]=n<T>(3,4)*z[9] + z[30];
    z[30]=z[5]*z[30];
    z[15]=z[15] + z[17] + z[30];
    z[17]=3*z[4];
    z[15]=z[15]*z[17];
    z[30]=z[3] + 1;
    z[16]=z[30]*z[16];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[16]*z[29];
    z[28]=n<T>(7,2) + z[28];
    z[28]=z[3]*z[28];
    z[28]=n<T>(3,2) + z[28];
    z[28]=z[3]*z[28];
    z[16]=z[28] + z[16];
    z[16]=z[5]*z[16];
    z[28]=z[3]*z[30];
    z[16]=n<T>(9,4)*z[28] + z[16];
    z[16]=z[5]*z[16];
    z[15]=z[15] + z[24] + z[16];
    z[10]=z[14] + n<T>(1,2)*z[15] + 3*z[10];
    z[14]=static_cast<T>(1)- z[18];
    z[14]=z[14]*z[13];
    z[14]= - z[12] + z[14];
    z[14]=z[4]*z[14];
    z[14]=n<T>(1,2)*z[14] + n<T>(3,4)*z[12] + z[26];
    z[14]=z[14]*z[17];
    z[15]=static_cast<T>(9)+ 25*z[3];
    z[15]=z[15]*z[13];
    z[15]=9*z[12] + z[15];
    z[16]= - static_cast<T>(1)+ z[4];
    z[16]=z[4]*z[16];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=z[1]*z[19]*z[16];
    z[14]=n<T>(3,4)*z[16] + n<T>(1,2)*z[15] + z[14];
    z[14]=z[1]*z[14];
    z[15]= - n<T>(1,4) + z[3];
    z[15]=z[15]*z[9];
    z[16]= - static_cast<T>(1)+ n<T>(5,2)*z[3];
    z[13]=z[16]*z[13];
    z[13]=z[15] + z[13];
    z[13]=z[5]*z[13];
    z[13]=n<T>(3,16)*z[9] + z[13];
    z[13]=z[4]*z[13];
    z[12]= - z[12] - n<T>(3,4)*z[26];
    z[12]=z[5]*z[12];
    z[12]=z[13] - n<T>(5,16)*z[9] + z[12];
    z[12]=z[4]*z[12];
    z[13]= - static_cast<T>(1)- n<T>(11,8)*z[3];
    z[13]=z[3]*z[13];
    z[13]= - n<T>(3,8) + z[13];
    z[13]=z[13]*z[11];
    z[13]=z[13] - n<T>(9,16) - z[3];
    z[13]=z[13]*z[29];
    z[9]=n<T>(1,8)*z[14] + n<T>(3,4)*z[12] - n<T>(3,8)*z[9] + z[13];
    z[9]=z[1]*z[9];
    z[12]=z[20]*z[11];
    z[13]=z[7]*z[5];
    z[14]= - z[23]*z[13];
    z[12]=z[12] + z[14];
    z[12]=z[7]*z[12];
    z[14]=z[21]*z[27];
    z[14]=3*z[22] + z[14];
    z[14]=z[5]*z[14];
    z[15]=z[21]*z[31];
    z[14]=z[15] + z[14];
    z[11]=z[14]*z[11];
    z[11]=z[11] + z[21];
    z[11]=z[5]*z[11];
    z[11]=n<T>(1,4) + z[11];
    z[11]=z[11]*z[13];
    z[11]=z[25] + z[11];
    z[13]=z[8]*z[7];
    z[11]=z[11]*z[13];
    z[11]=z[12] + z[11];
    z[11]=z[8]*z[11];
    z[12]=z[6]*z[5]*npow(z[13],2);

    r += z[9] + n<T>(1,4)*z[10] + n<T>(1,16)*z[11] - n<T>(1,64)*z[12];
 
    return r;
}

template double qqb_2lha_r2104(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2104(const std::array<dd_real,30>&);
#endif
