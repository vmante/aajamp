#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r927(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[9];
    z[8]=k[10];
    z[9]=n<T>(1,3)*z[7];
    z[10]=n<T>(7,4) - 2*z[7];
    z[10]=z[10]*z[9];
    z[11]=n<T>(1,3)*z[3];
    z[12]=2*z[3];
    z[13]=n<T>(1,2) + z[12];
    z[13]=z[13]*z[11];
    z[10]=z[13] - n<T>(1,4) + z[10];
    z[10]=z[2]*z[10];
    z[13]=z[8] + z[4];
    z[14]=n<T>(1,12)*z[13];
    z[15]=n<T>(5,6) - z[3];
    z[15]=z[3]*z[15];
    z[16]=npow(z[7],2);
    z[17]= - z[2]*z[16];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[5]*z[17];
    z[10]=n<T>(1,6)*z[17] + z[10] + z[14] + z[15];
    z[15]=n<T>(1,3)*z[5];
    z[10]=z[10]*z[15];
    z[17]= - static_cast<T>(1)+ z[12];
    z[17]=z[17]*z[11];
    z[18]= - n<T>(1,3) - z[8];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[18]=n<T>(2,3)*z[3];
    z[19]= - n<T>(1,2) - z[18];
    z[19]=z[19]*z[11];
    z[13]= - static_cast<T>(1)- n<T>(1,3)*z[13];
    z[13]=n<T>(1,3)*z[13] + z[7];
    z[13]=n<T>(1,36)*z[2] + n<T>(1,4)*z[13] + z[19];
    z[13]=z[2]*z[13];
    z[10]=z[10] + n<T>(1,3)*z[17] + z[13];
    z[10]=z[5]*z[10];
    z[13]=n<T>(1,6)*z[2];
    z[17]=n<T>(1,2)*z[7];
    z[19]=static_cast<T>(1)- z[17];
    z[19]=z[7]*z[19];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[19]*z[13];
    z[20]=z[8] + 1;
    z[21]=z[4] + z[20];
    z[21]=n<T>(1,3)*z[21] - z[7];
    z[19]=z[19] + n<T>(1,2)*z[21] + z[18];
    z[19]=z[2]*z[19];
    z[21]=z[7] - 1;
    z[13]= - z[21]*z[13];
    z[22]=z[11] - n<T>(1,2);
    z[22]=z[22]*z[3];
    z[13]= - n<T>(1,12)*z[5] + z[13] - z[14] + z[22];
    z[13]=z[5]*z[13];
    z[14]=n<T>(1,2)*z[8];
    z[13]=z[13] + z[19] + z[14] - z[22];
    z[13]=z[13]*z[15];
    z[19]= - n<T>(7,3)*z[8] + z[7];
    z[18]=n<T>(1,2)*z[19] - z[18];
    z[9]=static_cast<T>(1)- z[9];
    z[9]=z[9]*z[17];
    z[14]= - z[14] - static_cast<T>(1)- n<T>(1,2)*z[4];
    z[9]=n<T>(1,9)*z[14] + z[9];
    z[9]=z[2]*z[9];
    z[9]=n<T>(1,3)*z[18] + n<T>(1,2)*z[9];
    z[9]=z[2]*z[9];
    z[14]=z[3] + z[8];
    z[9]=z[13] + n<T>(1,9)*z[14] + z[9];
    z[9]=z[5]*z[9];
    z[13]=z[20]*z[7];
    z[18]= - static_cast<T>(2)- n<T>(11,4)*z[8];
    z[18]=n<T>(1,3)*z[18] + n<T>(1,4)*z[13];
    z[18]=z[7]*z[18];
    z[18]=n<T>(2,3)*z[8] + z[18];
    z[18]=z[2]*z[18];
    z[13]=z[8] - z[13];
    z[13]=n<T>(1,2)*z[13] + z[18];
    z[18]=n<T>(1,3)*z[2];
    z[13]=z[13]*z[18];
    z[9]=z[13] + z[9];
    z[9]=z[6]*z[9];
    z[13]= - z[20]*z[17];
    z[17]=static_cast<T>(1)+ n<T>(7,2)*z[8];
    z[13]=n<T>(1,3)*z[17] + z[13];
    z[11]=n<T>(1,2)*z[13] + z[11];
    z[11]=z[2]*z[11];
    z[11]= - n<T>(1,3)*z[14] + z[11];
    z[9]=z[9] + n<T>(1,3)*z[11] + z[10];
    z[9]=z[6]*z[9];
    z[10]= - static_cast<T>(1)- 4*z[3];
    z[10]=z[3]*z[10];
    z[11]=z[2]*npow(z[3],2);
    z[10]=z[11] + n<T>(1,4) + z[10];
    z[10]=z[10]*z[18];
    z[11]=n<T>(1,2)*z[5];
    z[13]= - z[16]*z[11];
    z[14]=static_cast<T>(1)- n<T>(5,2)*z[7];
    z[14]=z[7]*z[14];
    z[13]=z[14] + z[13];
    z[11]=z[13]*z[11];
    z[13]= - z[7]*z[21];
    z[11]=z[11] - n<T>(1,2) + z[13];
    z[11]=z[11]*z[15];
    z[13]=n<T>(1,2) + z[7];
    z[14]= - n<T>(1,6) + z[3];
    z[14]=z[3]*z[14];
    z[10]=z[11] + z[10] + n<T>(1,6)*z[13] + z[14];
    z[10]=z[5]*z[10];
    z[11]=z[1] + 1;
    z[12]=z[11]*z[12];
    z[13]= - static_cast<T>(1)+ z[1];
    z[11]=z[11]*z[3];
    z[14]=static_cast<T>(1)- z[11];
    z[14]=z[2]*z[14];
    z[12]=z[14] + n<T>(1,2)*z[13] + z[12];
    z[12]=z[3]*z[12];
    z[12]= - n<T>(1,2) + z[12];
    z[12]=z[2]*z[12];
    z[13]= - n<T>(1,2) - z[3];
    z[11]=z[13]*z[11];
    z[11]=z[12] + static_cast<T>(1)+ z[11];
    z[10]=n<T>(1,3)*z[11] + z[10];

    r += z[9] + n<T>(1,3)*z[10];
 
    return r;
}

template double qqb_2lha_r927(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r927(const std::array<dd_real,30>&);
#endif
