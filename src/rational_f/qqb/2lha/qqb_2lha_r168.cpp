#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r168(const std::array<T,30>& k) {
  T z[30];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[17];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=k[9];
    z[10]=k[10];
    z[11]=n<T>(1,2)*z[8];
    z[12]=npow(z[4],2);
    z[13]=z[11]*z[12];
    z[13]=z[13] - z[8];
    z[14]=n<T>(1,2)*z[5];
    z[13]=z[13]*z[14];
    z[15]=n<T>(1,2)*z[4];
    z[16]= - n<T>(3,2) - z[8];
    z[16]=z[16]*z[15];
    z[16]=z[8] + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[13] + static_cast<T>(1)+ z[16];
    z[16]=z[5]*z[16];
    z[17]=n<T>(1,4)*z[4];
    z[18]=z[17] - 1;
    z[19]=z[8] + 3;
    z[18]=z[4]*z[19]*z[18];
    z[19]=z[6] + n<T>(1,4)*z[9];
    z[20]=z[12]*z[1];
    z[16]=z[16] + n<T>(5,4)*z[20] + z[18] - z[19];
    z[18]=static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[18]=z[4]*z[18];
    z[18]=z[18] - n<T>(13,4) - z[8];
    z[18]=z[18]*z[15];
    z[11]= - static_cast<T>(1)- z[11];
    z[11]=z[4]*z[11];
    z[11]=z[8] + z[11];
    z[11]=z[4]*z[11];
    z[11]=z[13] + n<T>(3,2) + z[11];
    z[11]=z[11]*z[14];
    z[13]= - n<T>(1,2) + z[12];
    z[13]=z[1]*z[13];
    z[11]=z[11] + z[18] + z[13];
    z[11]=z[5]*z[11];
    z[13]=z[17] + n<T>(5,4) - z[19];
    z[17]=z[6] + n<T>(19,4)*z[4];
    z[17]=n<T>(1,2)*z[17] - z[20];
    z[17]=z[1]*z[17];
    z[11]=z[11] + n<T>(1,2)*z[13] + z[17];
    z[11]=z[3]*z[11];
    z[11]=n<T>(1,2)*z[16] + z[11];
    z[13]=n<T>(1,2)*z[3];
    z[11]=z[11]*z[13];
    z[16]=z[14] - z[1];
    z[17]=5*z[9];
    z[18]= - n<T>(1,2) + z[10];
    z[18]=3*z[18] + z[17] + z[16];
    z[19]=n<T>(1,4)*z[5];
    z[18]=z[18]*z[19];
    z[20]=n<T>(3,2) - z[17];
    z[21]=static_cast<T>(1)+ n<T>(5,8)*z[1];
    z[21]=z[1]*z[21];
    z[18]=z[18] + n<T>(1,2)*z[20] + z[21];
    z[18]=z[5]*z[18];
    z[17]=static_cast<T>(1)+ z[17];
    z[20]=3*z[4];
    z[21]=n<T>(5,2) - z[20];
    z[21]=z[1]*z[21];
    z[21]= - n<T>(31,3) + n<T>(3,2)*z[21];
    z[21]=z[1]*z[21];
    z[17]=n<T>(1,2)*z[17] + z[21];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[18]=3*z[9];
    z[21]=n<T>(1,2)*z[1];
    z[22]=z[18] - z[21];
    z[22]=z[22]*z[14];
    z[23]=n<T>(1,2) - z[9];
    z[24]= - n<T>(11,3) + z[1];
    z[24]=z[1]*z[24];
    z[22]=z[22] + 3*z[23] + n<T>(5,2)*z[24];
    z[23]=n<T>(1,8)*z[5];
    z[22]=z[22]*z[23];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[20];
    z[15]= - n<T>(5,4) + z[15];
    z[24]=n<T>(1,8)*z[1];
    z[15]=z[15]*z[24];
    z[25]=n<T>(73,12) - z[6];
    z[15]=z[15] + n<T>(1,4)*z[25] - z[4];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(3,16) + z[15];
    z[15]=z[1]*z[15];
    z[25]=z[9] - 1;
    z[15]=z[22] + n<T>(3,16)*z[25] + z[15];
    z[15]=z[3]*z[15];
    z[15]=n<T>(1,4)*z[17] + z[15];
    z[15]=z[3]*z[15];
    z[17]= - n<T>(37,3) + 5*z[1];
    z[17]=z[1]*z[17];
    z[17]=z[17] - n<T>(7,2) + z[18];
    z[18]=z[9] + 3*z[10];
    z[22]=z[18]*z[19];
    z[22]= - z[22] - static_cast<T>(1)+ n<T>(3,4)*z[9];
    z[26]=n<T>(1,4)*z[1];
    z[27]= - z[26] - z[22];
    z[27]=z[5]*z[27];
    z[17]=n<T>(1,4)*z[17] + z[27];
    z[17]=z[17]*z[14];
    z[22]= - z[5]*z[22];
    z[27]=n<T>(3,2)*z[9];
    z[28]= - static_cast<T>(1)+ z[27];
    z[29]=static_cast<T>(1)- n<T>(43,12)*z[1];
    z[29]=z[1]*z[29];
    z[22]=z[22] + n<T>(1,2)*z[28] + z[29];
    z[22]=z[22]*z[14];
    z[20]=n<T>(55,6) - z[20];
    z[20]=z[20]*z[26];
    z[20]= - n<T>(5,3) + z[20];
    z[20]=z[1]*z[20];
    z[20]= - n<T>(1,8) + z[20];
    z[20]=z[1]*z[20];
    z[25]=n<T>(1,8)*z[25];
    z[20]=z[22] - z[25] + z[20];
    z[20]=z[20]*z[13];
    z[22]=n<T>(5,3) - n<T>(37,16)*z[1];
    z[22]=z[1]*z[22];
    z[17]=z[20] + z[17] - z[25] + z[22];
    z[17]=z[3]*z[17];
    z[20]= - n<T>(1,3)*z[5] + 1;
    z[20]=z[7]*z[20];
    z[20]= - z[1] + z[20];
    z[20]=z[20]*z[14];
    z[20]=z[20] - n<T>(1,2)*z[7] + z[1];
    z[20]=z[5]*z[20];
    z[22]=z[5] - 1;
    z[25]= - z[22]*npow(z[1],2);
    z[26]=z[3]*npow(z[1],3);
    z[25]= - n<T>(1,3)*z[26] + z[25];
    z[13]=z[25]*z[13];
    z[25]=n<T>(1,3)*z[7] - z[1];
    z[13]=z[13] + n<T>(1,2)*z[25] + z[20];
    z[13]=z[2]*z[13];
    z[18]=n<T>(37,3) + z[18];
    z[18]=z[18]*z[14];
    z[18]=z[18] - n<T>(25,6)*z[1] - z[27] - static_cast<T>(19)- n<T>(37,6)*z[7];
    z[18]=z[5]*z[18];
    z[18]=z[18] - n<T>(43,3)*z[1] + z[27] + n<T>(39,2) + n<T>(37,3)*z[7];
    z[18]=z[18]*z[19];
    z[20]= - static_cast<T>(5)- n<T>(37,8)*z[7];
    z[18]=z[18] + n<T>(37,8)*z[1] + n<T>(1,3)*z[20] - n<T>(1,8)*z[9];
    z[13]=5*z[13] + n<T>(1,2)*z[18] + z[17];
    z[13]=z[2]*z[13];
    z[17]=z[9] + n<T>(17,3) + n<T>(3,2)*z[10];
    z[14]=z[17]*z[14];
    z[17]=n<T>(37,2)*z[7];
    z[18]= - static_cast<T>(83)- z[17];
    z[14]=z[14] - n<T>(11,6)*z[1] + n<T>(1,12)*z[18] - z[9];
    z[14]=z[5]*z[14];
    z[17]=static_cast<T>(49)+ z[17];
    z[17]= - n<T>(31,12)*z[1] + n<T>(1,6)*z[17] + z[9];
    z[14]=n<T>(1,2)*z[17] + z[14];
    z[13]=z[13] + n<T>(1,4)*z[14] + z[15];
    z[13]=z[2]*z[13];
    z[12]=3*z[12];
    z[14]= - n<T>(5,2) - z[12];
    z[14]=z[14]*z[24];
    z[15]=z[6] - n<T>(5,8);
    z[17]=n<T>(13,8) + z[4];
    z[17]=z[4]*z[17];
    z[14]=z[14] + n<T>(1,2)*z[15] + z[17];
    z[14]=z[14]*z[21];
    z[17]=n<T>(9,4) - z[6];
    z[14]=z[14] + n<T>(1,2)*z[17] - z[4];
    z[14]=z[1]*z[14];
    z[17]=static_cast<T>(5)+ n<T>(7,2)*z[1];
    z[17]=z[17]*z[21];
    z[18]= - 3*z[1] + z[22];
    z[18]=z[18]*z[19];
    z[20]=n<T>(1,2)*z[9];
    z[17]=z[18] + z[17] - static_cast<T>(3)+ z[20];
    z[17]=z[17]*z[23];
    z[18]= - static_cast<T>(1)- z[20];
    z[14]=z[17] + n<T>(1,8)*z[18] + z[14];
    z[14]=z[3]*z[14];
    z[12]=static_cast<T>(5)+ z[12];
    z[12]=z[12]*z[24];
    z[12]=z[12] - n<T>(7,4)*z[4] - z[15];
    z[12]=z[1]*z[12];
    z[15]= - n<T>(1,2) + z[16];
    z[15]=z[15]*z[19];
    z[12]=z[15] - n<T>(5,4) + z[12];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[12]=z[3]*z[12];
    z[14]=n<T>(37,6) - z[9];
    z[14]=z[5]*z[14];
    z[14]=z[14] - n<T>(37,6)*z[1] - n<T>(49,6) + z[9];
    z[12]=z[13] + n<T>(1,16)*z[14] + z[12];
    z[12]=z[2]*z[12];

    r += z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r168(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r168(const std::array<dd_real,30>&);
#endif
