#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r394(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=k[5];
    z[7]=k[12];
    z[8]=n<T>(1,2)*z[5];
    z[9]=z[5]*z[4];
    z[10]=n<T>(1,2)*z[9];
    z[11]=static_cast<T>(1)- z[10];
    z[8]=z[11]*z[8];
    z[11]=n<T>(1,2)*z[1];
    z[12]=n<T>(1,4)*z[4];
    z[13]=n<T>(1,2)*z[2];
    z[8]= - z[13] + z[8] - z[12] + static_cast<T>(1)+ z[11];
    z[8]=z[2]*z[8];
    z[14]=z[5]*npow(z[4],2);
    z[15]=z[4] - z[14];
    z[15]=z[5]*z[15];
    z[15]=z[4] + z[15];
    z[8]=n<T>(1,2)*z[15] + z[8];
    z[8]=z[2]*z[8];
    z[15]=static_cast<T>(1)+ z[5];
    z[14]=z[15]*z[14];
    z[15]=static_cast<T>(3)+ z[5];
    z[15]=z[5]*z[15];
    z[15]= - z[2] + z[15] - z[4] + static_cast<T>(3)+ z[1];
    z[13]=z[15]*z[13];
    z[10]=z[4] + z[10];
    z[10]=z[5]*z[10];
    z[10]=z[10] + n<T>(1,2)*z[4];
    z[10]=3*z[10] + z[13];
    z[10]=z[2]*z[10];
    z[10]=n<T>(3,2)*z[14] + z[10];
    z[10]=z[2]*z[10];
    z[13]=npow(z[5],2)*npow(z[4],3);
    z[10]=n<T>(1,2)*z[13] + z[10];
    z[10]=z[3]*z[10];
    z[8]=n<T>(1,2)*z[10] - n<T>(1,4)*z[13] + z[8];
    z[8]=z[3]*z[8];
    z[10]= - z[2] - static_cast<T>(3)+ z[1];
    z[13]= - static_cast<T>(25)- 13*z[5];
    z[13]=z[5]*z[13];
    z[10]=z[13] - z[4] + 3*z[10];
    z[10]=z[2]*z[10];
    z[9]= - 3*z[4] - n<T>(13,4)*z[9];
    z[9]=z[5]*z[9];
    z[9]=n<T>(1,4)*z[10] + z[12] + z[9];
    z[8]=n<T>(1,2)*z[9] + z[8];
    z[8]=z[3]*z[8];
    z[9]= - static_cast<T>(3)+ z[11];
    z[10]= - static_cast<T>(1)+ 13*z[4];
    z[10]=z[5]*z[10];
    z[10]= - static_cast<T>(1)+ n<T>(1,8)*z[10];
    z[10]=z[5]*z[10];
    z[8]=z[8] - n<T>(1,8)*z[2] + n<T>(1,4)*z[9] + z[10];
    z[8]=z[3]*z[8];
    z[9]=3*z[7];
    z[10]=5*z[6] - z[9];
    z[10]=z[4]*z[10];
    z[10]= - z[9] + z[10];
    z[10]=z[5]*z[10];
    z[9]= - z[9] + z[10];
    z[9]=z[5]*z[9];
    z[8]=n<T>(1,16)*z[9] + z[8];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r394(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r394(const std::array<dd_real,30>&);
#endif
