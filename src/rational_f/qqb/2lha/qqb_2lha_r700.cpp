#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r700(const std::array<T,30>& k) {
  T z[56];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=k[9];
    z[8]=k[5];
    z[9]=k[3];
    z[10]=k[15];
    z[11]=k[12];
    z[12]=z[3] + n<T>(1,2);
    z[13]=npow(z[3],3);
    z[14]=z[12]*z[13];
    z[15]=z[7]*z[3];
    z[16]=z[3] - n<T>(15,4);
    z[17]=z[3]*z[16];
    z[17]=n<T>(11,4) + z[17];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[17]=z[17]*z[15];
    z[14]=z[14] + z[17];
    z[17]=n<T>(1,2)*z[7];
    z[18]=z[17]*z[5];
    z[14]=z[14]*z[18];
    z[19]=npow(z[3],2);
    z[20]=3*z[3];
    z[21]= - static_cast<T>(5)+ z[20];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[21]*z[19];
    z[22]= - n<T>(43,4) + z[20];
    z[22]=z[22]*z[20];
    z[22]=n<T>(113,4) + z[22];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(39,8) + z[22];
    z[23]=z[17]*z[3];
    z[22]=z[22]*z[23];
    z[24]=6*z[3];
    z[25]=n<T>(143,8) - z[24];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(55,8) + z[25];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(7,16) + z[25];
    z[25]=z[3]*z[25];
    z[22]=z[25] + z[22];
    z[22]=z[7]*z[22];
    z[14]=z[14] + z[21] + z[22];
    z[14]=z[5]*z[14];
    z[21]=13*z[3];
    z[22]= - n<T>(189,4) + z[21];
    z[22]=z[3]*z[22];
    z[22]=n<T>(203,4) + z[22];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(67,4) + z[22];
    z[22]=z[3]*z[22];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[17];
    z[25]=static_cast<T>(49)- n<T>(33,2)*z[3];
    z[25]=z[3]*z[25];
    z[25]= - static_cast<T>(37)+ z[25];
    z[25]=z[3]*z[25];
    z[25]=n<T>(89,16) + z[25];
    z[25]=z[3]*z[25];
    z[22]=z[25] + z[22];
    z[22]=z[7]*z[22];
    z[25]=10*z[3];
    z[26]= - n<T>(167,8) + z[25];
    z[26]=z[3]*z[26];
    z[26]=n<T>(49,8) + z[26];
    z[26]=z[3]*z[26];
    z[26]=n<T>(3,2) + z[26];
    z[26]=z[3]*z[26];
    z[14]=z[14] + z[26] + z[22];
    z[14]=z[5]*z[14];
    z[22]=2*z[3];
    z[26]=n<T>(3,2) - z[22];
    z[26]=z[3]*z[26];
    z[26]=n<T>(3,16) + z[26];
    z[26]=z[26]*z[19];
    z[27]=4*z[3];
    z[28]= - n<T>(7,8) + z[27];
    z[28]=z[3]*z[28];
    z[29]=n<T>(9,8) - z[22];
    z[29]=z[3]*z[29];
    z[29]=n<T>(1,16) + z[29];
    z[29]=z[7]*z[29];
    z[28]=z[29] - n<T>(1,8) + z[28];
    z[28]=z[28]*z[19]*z[7];
    z[29]=n<T>(1,2)*z[2];
    z[30]=npow(z[3],4);
    z[31]=z[30]*z[29];
    z[26]=z[31] + z[26] + z[28];
    z[26]=z[2]*z[26];
    z[28]=static_cast<T>(2)- z[20];
    z[28]=z[3]*z[28];
    z[28]=n<T>(1,4) + z[28];
    z[28]=z[28]*z[19];
    z[31]= - n<T>(29,8) + z[20];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,2) + z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,8) + z[31];
    z[31]=z[31]*z[15];
    z[28]=z[28] + z[31];
    z[28]=z[7]*z[28];
    z[31]=n<T>(1,2)*z[19];
    z[32]=n<T>(7,4)*z[3];
    z[33]= - static_cast<T>(1)- z[32];
    z[33]=z[33]*z[31];
    z[28]=z[33] + z[28];
    z[28]=z[5]*z[28];
    z[33]=7*z[3];
    z[34]= - n<T>(57,4) + z[33];
    z[34]=z[3]*z[34];
    z[34]=n<T>(9,2) + z[34];
    z[34]=z[3]*z[34];
    z[34]=n<T>(1,2) + z[34];
    z[34]=z[34]*z[23];
    z[35]=n<T>(121,8) - 8*z[3];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(15,8) + z[35];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(3,8) + z[35];
    z[35]=z[3]*z[35];
    z[34]=z[35] + z[34];
    z[34]=z[7]*z[34];
    z[35]= - static_cast<T>(7)+ n<T>(9,2)*z[3];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(1,4) + z[35];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(1,4) + z[35];
    z[35]=z[3]*z[35];
    z[26]=z[26] + z[28] + z[35] + z[34];
    z[26]=z[2]*z[26];
    z[28]=11*z[3];
    z[34]= - static_cast<T>(1)- z[28];
    z[34]=z[34]*z[19];
    z[35]=n<T>(33,8) - z[22];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(25,16) + z[35];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(1,4) + z[35];
    z[35]=z[35]*z[15];
    z[34]=n<T>(1,8)*z[34] + z[35];
    z[34]=z[7]*z[34];
    z[12]=z[12]*z[19];
    z[12]=n<T>(3,4)*z[12] + z[34];
    z[12]=z[5]*z[12];
    z[24]=n<T>(65,4) - z[24];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(19,2) + z[24];
    z[24]=z[3]*z[24];
    z[24]=n<T>(3,8) + z[24];
    z[24]=z[24]*z[15];
    z[25]= - n<T>(49,2) + z[25];
    z[25]=z[3]*z[25];
    z[25]=n<T>(25,4) + z[25];
    z[25]=z[3]*z[25];
    z[25]=n<T>(3,4) + z[25];
    z[25]=z[3]*z[25];
    z[24]=z[25] + z[24];
    z[24]=z[7]*z[24];
    z[25]=n<T>(33,4) - z[27];
    z[25]=z[3]*z[25];
    z[25]=n<T>(9,4) + z[25];
    z[25]=z[3]*z[25];
    z[25]=n<T>(3,8) + z[25];
    z[25]=z[3]*z[25];
    z[12]=z[12] + z[25] + z[24];
    z[12]=z[5]*z[12];
    z[24]=5*z[3];
    z[25]=n<T>(31,2) - z[24];
    z[25]=z[25]*z[20];
    z[25]= - n<T>(157,4) + z[25];
    z[25]=z[3]*z[25];
    z[25]=n<T>(29,4) + z[25];
    z[25]=z[25]*z[15];
    z[34]= - n<T>(241,8) + z[28];
    z[34]=z[3]*z[34];
    z[34]=static_cast<T>(20)+ z[34];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(23,16) + z[34];
    z[34]=z[3]*z[34];
    z[25]=z[34] + n<T>(1,4)*z[25];
    z[25]=z[7]*z[25];
    z[34]=static_cast<T>(15)- n<T>(29,4)*z[3];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(57,16) + z[34];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(1,8) + z[34];
    z[34]=z[3]*z[34];
    z[12]=z[26] + z[12] + z[34] + z[25];
    z[12]=z[2]*z[12];
    z[25]= - n<T>(77,4) + z[24];
    z[25]=z[3]*z[25];
    z[25]=n<T>(97,4) + z[25];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(89,8) + z[25];
    z[25]=z[3]*z[25];
    z[25]=n<T>(3,2) + z[25];
    z[25]=z[25]*z[17];
    z[26]=9*z[3];
    z[34]=n<T>(233,8) - z[26];
    z[34]=z[3]*z[34];
    z[34]= - n<T>(231,8) + z[34];
    z[34]=z[3]*z[34];
    z[34]=n<T>(19,2) + z[34];
    z[34]=z[3]*z[34];
    z[25]=z[25] - n<T>(5,8) + z[34];
    z[25]=z[7]*z[25];
    z[21]= - static_cast<T>(31)+ z[21];
    z[21]=z[3]*z[21];
    z[21]=n<T>(67,4) + z[21];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(9,8) + z[21];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(1,8) + z[21];
    z[12]=z[12] + z[14] + n<T>(1,2)*z[21] + z[25];
    z[12]=z[2]*z[12];
    z[14]= - z[16]*z[20];
    z[14]= - static_cast<T>(16)+ z[14];
    z[14]=z[3]*z[14];
    z[14]=n<T>(21,2) + z[14];
    z[14]=z[3]*z[14];
    z[16]=z[3] - n<T>(9,2);
    z[21]=n<T>(1,2)*z[3];
    z[25]=z[16]*z[21];
    z[25]=z[25] + 4;
    z[25]=z[25]*z[3];
    z[25]=z[25] - n<T>(7,2);
    z[25]=z[25]*z[3];
    z[25]=z[25] + n<T>(3,2);
    z[34]=3*z[7];
    z[35]=z[25]*z[34];
    z[14]=z[35] - static_cast<T>(3)+ z[14];
    z[14]=z[7]*z[14];
    z[35]=n<T>(7,2) - z[3];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(9,2) + z[35];
    z[35]=z[3]*z[35];
    z[35]=n<T>(5,2) + z[35];
    z[35]=z[3]*z[35];
    z[36]=z[3] - n<T>(17,4);
    z[36]=z[36]*z[3];
    z[36]=z[36] + 7;
    z[36]=z[36]*z[3];
    z[36]=z[36] - n<T>(11,2);
    z[36]=z[36]*z[3];
    z[36]=z[36] + 2;
    z[37]=z[7]*z[36];
    z[35]=z[37] - n<T>(1,2) + z[35];
    z[35]=z[7]*z[35];
    z[37]=npow(z[7],2);
    z[38]=z[37]*z[5];
    z[39]= - static_cast<T>(1)+ n<T>(1,4)*z[3];
    z[39]=z[3]*z[39];
    z[39]=n<T>(3,2) + z[39];
    z[39]=z[3]*z[39];
    z[39]= - static_cast<T>(1)+ z[39];
    z[39]=z[3]*z[39];
    z[39]=n<T>(1,4) + z[39];
    z[39]=z[39]*z[38];
    z[35]=z[35] + z[39];
    z[35]=z[5]*z[35];
    z[39]=z[3] - 3;
    z[40]=z[39]*z[20];
    z[40]=z[40] + n<T>(19,2);
    z[40]=z[40]*z[21];
    z[40]=z[40] - 2;
    z[40]=z[40]*z[3];
    z[40]=z[40] + n<T>(1,4);
    z[14]=z[35] + z[14] + z[40];
    z[14]=z[5]*z[14];
    z[35]=z[3] - 4;
    z[41]= - z[35]*z[20];
    z[41]= - n<T>(37,2) + z[41];
    z[41]=z[3]*z[41];
    z[41]=n<T>(27,2) + z[41];
    z[41]=z[3]*z[41];
    z[42]= - n<T>(19,4) + z[3];
    z[42]=z[3]*z[42];
    z[42]=static_cast<T>(9)+ z[42];
    z[42]=z[3]*z[42];
    z[42]= - n<T>(17,2) + z[42];
    z[42]=z[3]*z[42];
    z[42]=static_cast<T>(4)+ z[42];
    z[42]=z[7]*z[42];
    z[41]=z[42] - n<T>(9,2) + z[41];
    z[41]=z[7]*z[41];
    z[42]= - n<T>(25,4) + z[22];
    z[42]=z[3]*z[42];
    z[42]=static_cast<T>(7)+ z[42];
    z[42]=z[3]*z[42];
    z[42]= - n<T>(13,4) + z[42];
    z[42]=z[3]*z[42];
    z[14]=z[14] + z[41] + n<T>(1,2) + z[42];
    z[14]=z[5]*z[14];
    z[41]=n<T>(3,2)*z[3];
    z[42]=z[41] - 5;
    z[42]=z[42]*z[21];
    z[42]=z[42] + 3;
    z[42]=z[42]*z[3];
    z[42]=z[42] - n<T>(3,2);
    z[43]=z[3]*z[42];
    z[44]= - static_cast<T>(5)+ z[3];
    z[44]=z[44]*z[21];
    z[44]=static_cast<T>(5)+ z[44];
    z[44]=z[3]*z[44];
    z[44]= - static_cast<T>(5)+ z[44];
    z[44]=z[3]*z[44];
    z[44]=n<T>(5,2) + z[44];
    z[44]=z[44]*z[17];
    z[36]=z[44] - z[36];
    z[36]=z[7]*z[36];
    z[14]=z[14] + z[36] + n<T>(1,4) + z[43];
    z[14]=z[4]*z[14];
    z[36]=z[7]*z[42];
    z[39]=z[39]*z[3];
    z[39]=z[39] + 3;
    z[39]=z[39]*z[3];
    z[39]=z[39] - 1;
    z[36]= - n<T>(1,4)*z[39] + z[36];
    z[36]=z[7]*z[36];
    z[42]=z[21]*z[38];
    z[43]= - z[3] + z[15];
    z[43]=z[7]*z[43];
    z[42]=z[43] + z[42];
    z[39]=z[5]*z[39]*z[42];
    z[42]=z[21] - 1;
    z[42]=z[42]*z[3];
    z[42]=z[42] + n<T>(1,2);
    z[43]=z[42]*z[3];
    z[36]=z[39] - z[43] + z[36];
    z[36]=z[5]*z[36];
    z[39]= - static_cast<T>(11)+ z[20];
    z[39]=z[3]*z[39];
    z[39]=n<T>(61,4) + z[39];
    z[39]=z[3]*z[39];
    z[39]= - n<T>(39,4) + z[39];
    z[39]=z[3]*z[39];
    z[16]=z[16]*z[3];
    z[44]= - static_cast<T>(8)- z[16];
    z[44]=z[3]*z[44];
    z[44]=static_cast<T>(7)+ z[44];
    z[44]=z[3]*z[44];
    z[44]= - static_cast<T>(3)+ z[44];
    z[44]=z[7]*z[44];
    z[39]=z[44] + n<T>(11,4) + z[39];
    z[39]=z[7]*z[39];
    z[44]=n<T>(11,2) - z[22];
    z[44]=z[3]*z[44];
    z[44]= - n<T>(21,4) + z[44];
    z[44]=z[3]*z[44];
    z[44]=static_cast<T>(2)+ z[44];
    z[44]=z[3]*z[44];
    z[36]=z[36] + z[39] - n<T>(1,4) + z[44];
    z[36]=z[5]*z[36];
    z[25]= - z[7]*z[25];
    z[39]= - n<T>(31,4) + z[22];
    z[39]=z[3]*z[39];
    z[39]=n<T>(23,2) + z[39];
    z[39]=z[3]*z[39];
    z[39]= - static_cast<T>(8)+ z[39];
    z[39]=z[3]*z[39];
    z[25]=z[25] + n<T>(5,2) + z[39];
    z[25]=z[7]*z[25];
    z[14]=z[14] + z[36] + z[25] - z[40];
    z[14]=z[4]*z[14];
    z[25]=z[3] - 1;
    z[36]= - z[25]*z[19];
    z[39]=z[7] - 3;
    z[40]=n<T>(1,4)*z[7];
    z[44]= - z[39]*z[40];
    z[45]=z[25]*z[13];
    z[46]= - z[45] - n<T>(1,4)*z[37];
    z[46]=z[5]*z[46];
    z[36]=z[46] + z[44] - n<T>(3,4) + z[36];
    z[36]=z[5]*z[36];
    z[44]= - static_cast<T>(3)- z[7];
    z[44]=z[44]*z[40];
    z[44]=static_cast<T>(1)+ z[44];
    z[36]=n<T>(1,2)*z[44] + z[36];
    z[36]=z[5]*z[36];
    z[44]= - z[13] + n<T>(1,2)*z[37];
    z[44]=z[5]*z[44];
    z[46]=z[17] - 1;
    z[47]=z[46]*z[7];
    z[44]=z[44] + n<T>(3,2)*z[47] + n<T>(3,4) - z[19];
    z[44]=z[5]*z[44];
    z[39]=z[39]*z[17];
    z[48]=z[39] + 1;
    z[49]=n<T>(1,2)*z[5];
    z[50]=z[49]*z[37];
    z[51]=n<T>(3,2) - z[7];
    z[51]=z[7]*z[51];
    z[51]= - z[50] - n<T>(1,2) + z[51];
    z[51]=z[51]*z[29];
    z[44]=z[51] - n<T>(1,4)*z[48] + z[44];
    z[44]=z[2]*z[44];
    z[51]=5*z[7];
    z[52]=z[51] - 9;
    z[40]=z[52]*z[40];
    z[40]=z[40] + 1;
    z[36]=z[44] - n<T>(1,4)*z[40] + z[36];
    z[36]=z[2]*z[36];
    z[44]= - static_cast<T>(1)+ z[5];
    z[44]=z[5]*z[44];
    z[44]=z[44] + static_cast<T>(1)- n<T>(5,4)*z[37];
    z[44]=z[5]*z[44];
    z[44]=z[44] - z[40];
    z[48]=z[5]*z[48];
    z[47]=z[47] + n<T>(1,2);
    z[52]= - z[47]*z[29];
    z[48]=z[48] + z[52];
    z[48]=z[2]*z[48];
    z[52]=npow(z[5],2);
    z[53]=z[17] - 3;
    z[53]=z[53]*z[7];
    z[54]= - static_cast<T>(3)- z[53];
    z[54]=z[54]*z[52];
    z[54]= - n<T>(1,2)*z[47] + z[54];
    z[48]=n<T>(1,2)*z[54] + z[48];
    z[48]=z[2]*z[48];
    z[46]= - z[46]*z[52];
    z[54]=z[7] - 1;
    z[46]= - n<T>(1,4)*z[54] + z[46];
    z[46]=z[5]*z[46];
    z[46]=z[48] - n<T>(1,4)*z[47] + z[46];
    z[46]=z[2]*z[46];
    z[37]=static_cast<T>(1)- z[37];
    z[48]= - n<T>(1,2) - z[52];
    z[48]=z[5]*z[48];
    z[37]=n<T>(1,2)*z[37] + z[48];
    z[37]=z[5]*z[37];
    z[37]=z[37] - z[47];
    z[37]=n<T>(1,4)*z[37] + z[46];
    z[46]=n<T>(1,2)*z[8];
    z[37]=z[37]*z[46];
    z[36]=z[37] + n<T>(1,4)*z[44] + z[36];
    z[36]=z[36]*z[46];
    z[37]=n<T>(11,8) - z[3];
    z[37]=z[3]*z[37];
    z[37]=n<T>(7,8) + z[37];
    z[37]=z[37]*z[19];
    z[44]=z[13]*z[5];
    z[48]=z[2]*z[13];
    z[37]= - n<T>(1,8)*z[48] + z[37] + n<T>(1,4)*z[44];
    z[37]=z[2]*z[37];
    z[33]= - n<T>(39,4) + z[33];
    z[33]=z[3]*z[33];
    z[33]= - n<T>(9,4) + z[33];
    z[33]=z[33]*z[31];
    z[48]=z[19]*z[5];
    z[55]= - n<T>(9,4) + z[22];
    z[55]=z[3]*z[55];
    z[55]= - n<T>(3,4) + z[55];
    z[55]=z[55]*z[48];
    z[33]=z[37] + z[33] + z[55];
    z[33]=z[2]*z[33];
    z[37]=n<T>(27,4) - z[24];
    z[37]=z[37]*z[44];
    z[27]=n<T>(51,8) - z[27];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(7,8) + z[27];
    z[27]=z[3]*z[27];
    z[27]=n<T>(3,16) + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[33] + z[27] + z[37];
    z[27]=z[2]*z[27];
    z[32]=static_cast<T>(2)- z[32];
    z[32]=z[32]*z[13];
    z[29]=z[29]*z[45];
    z[29]=z[32] + z[29];
    z[29]=z[2]*z[29];
    z[32]= - n<T>(5,2) + z[22];
    z[32]=z[32]*z[13];
    z[29]=z[32] + z[29];
    z[29]=z[2]*z[29];
    z[32]= - z[3]*z[25];
    z[32]=n<T>(1,2) + z[32];
    z[32]=z[32]*z[31];
    z[31]=z[4]*z[31];
    z[31]= - z[19] + z[31];
    z[31]=z[4]*z[42]*z[31];
    z[29]=z[31] + z[32] + z[29];
    z[29]=z[1]*z[29];
    z[31]=z[3] - n<T>(5,2);
    z[32]= - z[31]*z[21];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[3]*z[32];
    z[31]= - z[3]*z[31];
    z[31]= - static_cast<T>(2)+ z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,2) + z[31];
    z[31]=z[5]*z[31];
    z[31]=z[31] + n<T>(1,4) + z[32];
    z[31]=z[4]*z[31];
    z[32]= - n<T>(9,4) + z[3];
    z[32]=z[3]*z[32];
    z[32]=n<T>(3,2) + z[32];
    z[32]=z[3]*z[32];
    z[31]=z[31] - n<T>(1,4) + z[32];
    z[31]=z[3]*z[31];
    z[32]= - static_cast<T>(2)+ z[3];
    z[32]=z[3]*z[32];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[32]*z[48];
    z[31]=z[32] + z[31];
    z[31]=z[4]*z[31];
    z[32]=z[3] - n<T>(7,4);
    z[33]=z[32]*z[20];
    z[33]=n<T>(7,4) + z[33];
    z[33]=z[33]*z[48];
    z[32]=z[3]*z[32];
    z[32]=n<T>(5,8) + z[32];
    z[32]=z[3]*z[32];
    z[32]= - n<T>(3,16) + z[32];
    z[32]=z[3]*z[32];
    z[27]=z[29] + z[31] + z[27] + z[32] + z[33];
    z[27]=z[1]*z[27];
    z[29]= - n<T>(9,2) + z[51];
    z[29]=z[7]*z[29];
    z[29]=z[29] + n<T>(5,2)*z[38];
    z[29]=z[29]*z[49];
    z[31]=z[54]*z[7];
    z[32]=z[31] + z[50];
    z[32]=z[5]*z[32];
    z[32]=z[32] + z[47];
    z[32]=z[32]*z[46];
    z[29]=z[32] + z[29] + z[40];
    z[29]=z[29]*z[46];
    z[32]=z[7] - n<T>(1,2);
    z[32]=z[32]*z[7];
    z[32]=z[32] + z[50];
    z[33]=z[32]*z[5];
    z[37]=z[54]*z[17];
    z[33]=z[33] + z[37];
    z[40]=z[9]*z[33];
    z[45]=n<T>(3,4) + z[7];
    z[18]=z[45]*z[18];
    z[45]= - n<T>(1,8) + z[7];
    z[45]=z[7]*z[45];
    z[18]=z[18] - n<T>(3,8) + z[45];
    z[18]=z[5]*z[18];
    z[18]=n<T>(3,4)*z[40] + z[29] + z[37] + z[18];
    z[18]=z[11]*z[18];
    z[29]= - n<T>(21,4) + z[22];
    z[29]=z[3]*z[29];
    z[29]=n<T>(5,2) + z[29];
    z[29]=z[29]*z[19];
    z[40]=n<T>(33,4) - z[22];
    z[40]=z[3]*z[40];
    z[40]= - n<T>(75,8) + z[40];
    z[40]=z[3]*z[40];
    z[40]=n<T>(45,16) + z[40];
    z[40]=z[3]*z[40];
    z[40]= - n<T>(3,16) + z[40];
    z[40]=z[7]*z[40];
    z[29]=z[40] + n<T>(1,8) + z[29];
    z[29]=z[7]*z[29];
    z[40]= - n<T>(3,2) + z[3];
    z[40]=z[40]*z[19];
    z[40]= - n<T>(1,4) + z[40];
    z[40]=z[40]*z[38];
    z[29]=z[29] + n<T>(1,4)*z[40];
    z[29]=z[5]*z[29];
    z[40]=n<T>(37,2) - z[26];
    z[40]=z[3]*z[40];
    z[40]= - n<T>(37,4) + z[40];
    z[40]=z[3]*z[40];
    z[40]= - static_cast<T>(1)+ z[40];
    z[40]=z[40]*z[21];
    z[45]=n<T>(23,2) - z[20];
    z[45]=z[45]*z[41];
    z[45]= - static_cast<T>(22)+ z[45];
    z[45]=z[3]*z[45];
    z[45]=n<T>(79,8) + z[45];
    z[45]=z[3]*z[45];
    z[45]= - n<T>(43,32) + z[45];
    z[45]=z[7]*z[45];
    z[46]= - n<T>(53,2) + z[26];
    z[46]=z[3]*z[46];
    z[46]=n<T>(197,8) + z[46];
    z[46]=z[3]*z[46];
    z[46]= - n<T>(99,16) + z[46];
    z[46]=z[3]*z[46];
    z[45]=z[45] + n<T>(1,4) + z[46];
    z[45]=z[7]*z[45];
    z[29]=z[29] + z[40] + z[45];
    z[29]=z[5]*z[29];
    z[40]= - n<T>(13,4) + z[3];
    z[26]=z[40]*z[26];
    z[26]=n<T>(127,4) + z[26];
    z[26]=z[3]*z[26];
    z[26]= - n<T>(23,2) + z[26];
    z[26]=z[3]*z[26];
    z[35]= - z[3]*z[35];
    z[35]= - n<T>(45,8) + z[35];
    z[35]=z[35]*z[20];
    z[35]=n<T>(143,16) + z[35];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(29,16) + z[35];
    z[35]=z[7]*z[35];
    z[26]=z[35] + n<T>(51,32) + z[26];
    z[26]=z[7]*z[26];
    z[35]=n<T>(19,4) - z[22];
    z[35]=z[35]*z[20];
    z[35]= - n<T>(77,8) + z[35];
    z[35]=z[3]*z[35];
    z[35]=n<T>(21,16) + z[35];
    z[35]=z[3]*z[35];
    z[26]=z[29] + z[26] + n<T>(3,16) + z[35];
    z[26]=z[5]*z[26];
    z[23]= - z[25]*z[23];
    z[25]= - static_cast<T>(1)+ z[41];
    z[25]=z[3]*z[25];
    z[23]=z[25] + z[23];
    z[23]=z[7]*z[23];
    z[25]=z[38]*z[43];
    z[23]=z[23] + z[25];
    z[23]=z[5]*z[23];
    z[25]= - static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[25]=z[25]*z[7];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=n<T>(1,4)*z[20] - z[25];
    z[20]=z[7]*z[20];
    z[20]=z[23] + z[21] + z[20];
    z[20]=z[5]*z[20];
    z[21]= - n<T>(7,4) + z[7];
    z[21]=z[7]*z[21];
    z[20]=z[20] + n<T>(3,4) + z[21];
    z[20]=z[5]*z[20];
    z[21]= - static_cast<T>(5)+ z[34];
    z[21]=z[21]*z[17];
    z[21]=static_cast<T>(1)+ z[21];
    z[23]=z[42]*z[50];
    z[25]= - z[25] - n<T>(1,2) + z[3];
    z[25]=z[7]*z[25];
    z[23]=z[25] + z[23];
    z[23]=z[5]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[10]*z[21]*z[52];
    z[20]=z[20] + z[21];
    z[20]=z[10]*z[20];
    z[21]=static_cast<T>(1)- z[34];
    z[21]=z[7]*z[21];
    z[21]=z[21] - z[38];
    z[21]=z[21]*z[49];
    z[23]=static_cast<T>(1)- n<T>(3,2)*z[7];
    z[23]=z[7]*z[23];
    z[21]=z[23] + z[21];
    z[21]=z[5]*z[21];
    z[21]= - z[37] + z[21];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[33];
    z[21]=z[4]*z[21];
    z[23]= - z[10]*z[33];
    z[23]=z[23] - z[32];
    z[23]=z[5]*z[23];
    z[23]= - z[37] + z[23];
    z[23]=z[10]*z[23];
    z[25]=z[31] + z[38];
    z[21]=z[23] + n<T>(1,4)*z[25] + z[21];
    z[21]=z[9]*z[21];
    z[23]=static_cast<T>(13)- z[34];
    z[23]=z[17]*z[23];
    z[23]= - static_cast<T>(5)+ z[23];
    z[13]=z[13]*z[23];
    z[13]=n<T>(7,2)*z[44] + z[13];
    z[23]= - z[39] + z[49] - 1;
    z[23]=z[2]*z[30]*z[23];
    z[13]=n<T>(1,2)*z[13] + z[23];
    z[13]=z[2]*z[13];
    z[23]=z[48] - z[19];
    z[19]= - z[19]*z[53];
    z[13]=z[13] + z[19] + n<T>(5,2)*z[23];
    z[13]=z[2]*z[13];
    z[15]=z[28] - z[15];
    z[15]=z[15]*z[17];
    z[19]=z[5]*z[3];
    z[15]=n<T>(15,2)*z[19] - z[24] + z[15];
    z[13]=n<T>(1,4)*z[15] + z[13];
    z[13]=z[2]*z[13];
    z[15]=3*z[5] + z[54];
    z[13]=n<T>(1,4)*z[15] + z[13];
    z[13]=z[2]*z[13];
    z[15]=z[9]*z[5];
    z[13]=z[13] + n<T>(1,8)*z[15];
    z[13]=z[6]*z[13];
    z[15]= - n<T>(29,4) - z[16];
    z[15]=z[3]*z[15];
    z[15]=n<T>(19,4) + z[15];
    z[15]=z[3]*z[15];
    z[15]= - n<T>(19,16) + z[15];
    z[15]=z[15]*z[17];
    z[16]= - n<T>(29,4) + z[22];
    z[16]=z[3]*z[16];
    z[16]=n<T>(71,8) + z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(63,16) + z[16];
    z[16]=z[3]*z[16];
    z[15]=z[15] + n<T>(1,2) + z[16];
    z[15]=z[7]*z[15];
    z[16]=static_cast<T>(4)- z[41];
    z[16]=z[3]*z[16];
    z[16]= - static_cast<T>(3)+ z[16];
    z[16]=z[3]*z[16];
    z[16]=n<T>(7,16) + z[16];
    z[16]=z[3]*z[16];

    r += n<T>(1,32) + z[12] + z[13] + z[14] + z[15] + z[16] + n<T>(1,4)*z[18]
       + z[20] + n<T>(1,2)*z[21] + z[26] + z[27] + z[36];
 
    return r;
}

template double qqb_2lha_r700(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r700(const std::array<dd_real,30>&);
#endif
