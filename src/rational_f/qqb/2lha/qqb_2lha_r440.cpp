#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r440(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[7];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[8];
    z[9]=k[10];
    z[10]=k[12];
    z[11]=k[15];
    z[12]=k[5];
    z[13]=npow(z[7],2);
    z[14]=npow(z[9],2);
    z[15]=z[13]*z[14];
    z[16]=static_cast<T>(1)+ n<T>(3,2)*z[9];
    z[16]=z[16]*z[9];
    z[16]=z[16] - z[15];
    z[17]=n<T>(1,2)*z[7];
    z[18]=z[17]*z[14];
    z[19]=z[9] - n<T>(1,2);
    z[19]=z[19]*z[9];
    z[20]=z[18] + z[19];
    z[21]= - z[2]*z[20];
    z[21]=z[21] + z[16];
    z[21]=z[2]*z[21];
    z[22]=z[7]*z[9];
    z[23]= - n<T>(3,2) - z[9];
    z[23]=z[23]*z[22];
    z[24]=n<T>(1,2)*z[9];
    z[25]=z[9] + 1;
    z[26]= - z[2]*z[25]*z[24];
    z[23]=z[26] + z[23] - n<T>(1,2) + z[19];
    z[23]=z[2]*z[23];
    z[26]= - static_cast<T>(3)- z[9];
    z[26]=z[26]*z[22];
    z[26]=n<T>(1,2)*z[26] - n<T>(3,2) + z[14];
    z[26]=z[7]*z[26];
    z[27]= - static_cast<T>(3)- 11*z[9];
    z[27]=z[9]*z[27];
    z[23]=z[23] + n<T>(1,4)*z[27] + z[26];
    z[23]=z[2]*z[23];
    z[26]= - z[22] - static_cast<T>(3)+ z[9];
    z[26]=z[26]*z[17];
    z[26]=z[26] + static_cast<T>(1)- n<T>(7,4)*z[9];
    z[26]=z[7]*z[26];
    z[27]= - static_cast<T>(7)+ 11*z[14];
    z[23]=z[23] + n<T>(1,4)*z[27] + z[26];
    z[23]=z[6]*z[23];
    z[26]= - static_cast<T>(11)- 17*z[9];
    z[26]=z[26]*z[24];
    z[26]=static_cast<T>(5)+ z[26];
    z[19]=z[19] - z[18];
    z[19]=z[7]*z[19];
    z[27]=static_cast<T>(3)- n<T>(11,4)*z[9];
    z[27]=z[9]*z[27];
    z[19]=z[19] - static_cast<T>(1)+ z[27];
    z[19]=z[7]*z[19];
    z[19]=z[23] + z[21] + n<T>(1,2)*z[26] + z[19];
    z[19]=z[6]*z[19];
    z[21]=z[14]*z[7];
    z[23]=z[12]*z[21];
    z[19]=z[19] - z[23];
    z[23]= - static_cast<T>(1)- 3*z[9];
    z[23]=z[9]*z[23];
    z[23]=z[23] - z[21];
    z[23]=z[23]*z[17];
    z[26]= - static_cast<T>(1)- n<T>(19,48)*z[9];
    z[26]=z[9]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[7]*z[23];
    z[26]=z[25]*z[9];
    z[27]=z[26] + z[21];
    z[28]=static_cast<T>(7)- n<T>(11,4)*z[6];
    z[28]=z[6]*z[27]*z[28];
    z[23]=n<T>(1,4)*z[28] + n<T>(29,48)*z[26] + z[23];
    z[28]=npow(z[7],3);
    z[29]=z[28]*z[4];
    z[29]=n<T>(13,4)*z[13] + z[29];
    z[29]=z[4]*z[29];
    z[29]=z[7] + n<T>(1,4)*z[29];
    z[29]=z[4]*z[27]*z[29];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[23]=z[10]*z[23];
    z[15]=z[15]*z[3];
    z[18]=z[26] + z[18];
    z[18]=z[7]*z[18];
    z[18]=z[18] - n<T>(1,2)*z[15];
    z[18]=z[3]*z[18];
    z[18]= - n<T>(1,2)*z[27] + z[18];
    z[18]=z[11]*z[18];
    z[18]=z[23] + z[18];
    z[23]= - static_cast<T>(5)- n<T>(9,2)*z[9];
    z[23]=z[9]*z[23];
    z[23]= - z[21] - n<T>(3,2) + z[23];
    z[23]=z[7]*z[23];
    z[15]=z[23] + z[15];
    z[15]=z[3]*z[15];
    z[23]=z[9] + z[21];
    z[23]=z[23]*z[17];
    z[27]=z[13]*z[3];
    z[29]= - static_cast<T>(3)- 7*z[9];
    z[29]=z[9]*z[29]*z[27];
    z[23]=z[23] + z[29];
    z[28]=z[28]*z[14]*z[3];
    z[29]= - z[4]*z[28];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[23]=z[4]*z[23];
    z[15]=z[23] + z[15] - static_cast<T>(1)+ n<T>(1,4)*z[21];
    z[15]=z[4]*z[15];
    z[23]=npow(z[3],2);
    z[25]=z[25]*z[23];
    z[24]=z[24] + 1;
    z[29]=z[24]*z[9];
    z[30]=z[29] + n<T>(1,2);
    z[31]=npow(z[3],3);
    z[32]=z[2]*z[30]*z[31];
    z[25]=n<T>(1,2)*z[25] + z[32];
    z[25]=z[8]*z[25]*npow(z[2],2);
    z[15]=z[15] + z[25];
    z[25]= - static_cast<T>(1)- n<T>(19,4)*z[9];
    z[25]=z[9]*z[25];
    z[25]=z[25] + n<T>(29,16)*z[21];
    z[13]=z[25]*z[13];
    z[13]=z[13] - n<T>(11,4)*z[28];
    z[13]=z[3]*z[13];
    z[25]=n<T>(43,3)*z[14] + n<T>(5,2)*z[21];
    z[25]=z[7]*z[25];
    z[28]= - static_cast<T>(19)- n<T>(11,2)*z[9];
    z[28]=z[9]*z[28];
    z[25]=n<T>(1,8)*z[25] - static_cast<T>(1)+ n<T>(1,12)*z[28];
    z[17]=z[25]*z[17];
    z[13]=z[17] + n<T>(1,3)*z[13];
    z[17]=n<T>(1,2)*z[3];
    z[13]=z[13]*z[17];
    z[17]=n<T>(1,3)*z[9];
    z[25]= - static_cast<T>(7)- n<T>(13,4)*z[9];
    z[25]=z[25]*z[17];
    z[25]= - n<T>(17,4) + z[25];
    z[28]=static_cast<T>(5)+ 27*z[9];
    z[28]=z[9]*z[28];
    z[21]=z[28] + 5*z[21];
    z[21]=z[7]*z[21];
    z[28]=n<T>(7,16) + z[17];
    z[28]=z[9]*z[28];
    z[21]=n<T>(1,64)*z[21] + n<T>(1,8) + z[28];
    z[21]=z[7]*z[21];
    z[13]=z[13] + n<T>(1,8)*z[25] + z[21];
    z[13]=z[3]*z[13];
    z[21]= - static_cast<T>(17)- n<T>(73,8)*z[9];
    z[21]=z[21]*z[17];
    z[21]= - n<T>(5,2) + z[21];
    z[25]= - n<T>(5,16) + n<T>(2,3)*z[9];
    z[25]=z[25]*z[22];
    z[21]=n<T>(1,2)*z[21] + z[25];
    z[21]=z[7]*z[21];
    z[25]=z[26]*z[27];
    z[21]=z[21] - n<T>(11,8)*z[25];
    z[21]=z[3]*z[21];
    z[25]= - static_cast<T>(1)+ z[9];
    z[25]=z[25]*z[22];
    z[27]= - static_cast<T>(7)+ n<T>(25,6)*z[9];
    z[27]=z[9]*z[27];
    z[25]=n<T>(5,4)*z[25] + n<T>(3,2) + z[27];
    z[25]=z[7]*z[25];
    z[27]= - static_cast<T>(101)+ z[9];
    z[27]=z[9]*z[27];
    z[25]=z[25] - static_cast<T>(31)+ n<T>(1,6)*z[27];
    z[21]=n<T>(1,16)*z[25] + z[21];
    z[21]=z[3]*z[21];
    z[16]=static_cast<T>(7)+ z[16];
    z[16]=n<T>(1,16)*z[16] + z[21];
    z[16]=z[3]*z[16];
    z[21]=z[24]*z[22];
    z[22]= - static_cast<T>(3)+ n<T>(41,6)*z[9];
    z[22]=z[9]*z[22];
    z[22]=n<T>(15,2) + z[22];
    z[24]=z[3]*z[30];
    z[22]= - n<T>(11,4)*z[24] + n<T>(1,16)*z[22];
    z[22]=z[7]*z[22];
    z[24]= - n<T>(61,8) - 2*z[9];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(71,8) + z[24];
    z[22]=n<T>(1,3)*z[24] + z[22];
    z[22]=z[3]*z[22];
    z[14]=z[22] + n<T>(1,16)*z[21] + static_cast<T>(1)- n<T>(3,16)*z[14];
    z[14]=z[3]*z[14];
    z[14]= - n<T>(1,16)*z[20] + z[14];
    z[14]=z[3]*z[14];
    z[17]= - static_cast<T>(1)- z[17];
    z[17]=z[9]*z[17];
    z[17]=n<T>(1,3)*z[7] - static_cast<T>(1)+ z[17];
    z[17]=z[3]*z[17];
    z[17]=11*z[17] + n<T>(55,6) + z[29];
    z[17]=z[3]*z[17];
    z[17]=n<T>(1,4)*z[26] + z[17];
    z[17]=z[17]*z[23];
    z[20]=z[2]*npow(z[3],4);
    z[17]=z[17] + n<T>(11,3)*z[20];
    z[17]=z[2]*z[17];
    z[14]=z[14] + n<T>(1,8)*z[17];
    z[14]=z[2]*z[14];
    z[14]=z[16] + z[14];
    z[14]=z[2]*z[14];
    z[16]= - 13*z[31] - n<T>(11,2)*z[20];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(7,2)*z[23] + n<T>(1,3)*z[16];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(1,2)*z[4] - n<T>(3,2)*z[3] + z[16];
    z[16]=z[1]*z[16];
    z[17]=z[2] - static_cast<T>(1)+ 3*z[7];
    z[17]=z[6]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[6]*z[17];
    z[20]= - z[5]*npow(z[6],2);
    z[17]=z[17] + z[20];
    z[17]=z[5]*z[17];

    r += n<T>(1,8) + z[13] + z[14] + n<T>(1,8)*z[15] + n<T>(1,4)*z[16] + n<T>(1,32)*
      z[17] + n<T>(1,2)*z[18] + n<T>(1,16)*z[19];
 
    return r;
}

template double qqb_2lha_r440(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r440(const std::array<dd_real,30>&);
#endif
