#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1396(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[29];
    z[6]=k[2];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[7];
    z[10]=k[9];
    z[11]=n<T>(1,2)*z[4];
    z[12]=z[11] - 1;
    z[13]=n<T>(1,2)*z[9];
    z[14]=z[13] - z[12];
    z[14]=z[4]*z[14];
    z[14]=n<T>(1,4) + z[14];
    z[15]=z[13] - 1;
    z[15]=z[15]*z[4];
    z[16]=z[9] - 3;
    z[17]=n<T>(1,2)*z[16] + z[15];
    z[17]=z[17]*z[11];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[2]*z[17];
    z[18]=static_cast<T>(5)- z[13] + z[11];
    z[18]=z[2]*z[18];
    z[19]=z[4] + z[9];
    z[18]=z[18] + n<T>(1,2) - z[19];
    z[20]=n<T>(1,4)*z[3];
    z[18]=z[18]*z[20];
    z[14]=z[18] + n<T>(1,2)*z[14] + z[17];
    z[17]=n<T>(1,4)*z[4];
    z[18]= - z[9] + z[11];
    z[18]=z[18]*z[17];
    z[21]=n<T>(1,2)*z[2];
    z[22]=z[4] - 1;
    z[22]=z[22]*z[4];
    z[23]=static_cast<T>(1)+ z[22];
    z[23]=z[23]*z[21];
    z[23]=z[23] + static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[20]=z[23]*z[20];
    z[23]=z[11] + 1;
    z[23]=z[23]*z[4];
    z[23]=z[23] + n<T>(1,2);
    z[24]= - z[2]*z[23];
    z[24]=z[4] + z[24];
    z[24]=z[3]*z[24];
    z[23]=z[24] + z[23];
    z[24]=n<T>(1,4)*z[1];
    z[23]=z[23]*z[24];
    z[18]=z[23] + z[20] + z[18] + static_cast<T>(1)- n<T>(3,4)*z[6];
    z[18]=z[1]*z[18];
    z[14]=n<T>(1,2)*z[14] + z[18];
    z[14]=z[1]*z[14];
    z[18]=3*z[2];
    z[20]=n<T>(1,4) - z[6];
    z[23]=z[17] + 1;
    z[25]=z[4]*z[23];
    z[20]=z[18] + 3*z[20] + z[25];
    z[20]=n<T>(1,2)*z[20] - z[3];
    z[20]=z[1]*z[20];
    z[12]= - z[12]*z[11];
    z[12]=z[12] - static_cast<T>(1)- n<T>(3,2)*z[6];
    z[25]=3*z[6];
    z[26]= - z[11] + n<T>(5,2) + z[25];
    z[26]=n<T>(1,2)*z[26] - z[18];
    z[27]=n<T>(1,2)*z[7];
    z[26]=z[26]*z[27];
    z[28]=static_cast<T>(3)- z[17];
    z[28]=z[2]*z[28];
    z[12]=z[26] + z[20] - z[3] + n<T>(1,2)*z[12] + z[28];
    z[12]=z[7]*z[12];
    z[20]= - z[2] + static_cast<T>(3)- z[4];
    z[17]=z[17]*z[20];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[2]*z[17];
    z[20]= - n<T>(1,4) + z[22];
    z[22]=static_cast<T>(1)+ n<T>(1,8)*z[10];
    z[26]= - z[2]*z[22];
    z[26]=n<T>(9,4) + z[26];
    z[26]=z[3]*z[26];
    z[17]=z[26] + n<T>(1,2)*z[20] + z[17];
    z[20]=z[2]*z[23];
    z[20]=z[20] - 1;
    z[20]=z[4]*z[20];
    z[20]= - static_cast<T>(1)+ z[25] + z[20];
    z[23]= - n<T>(3,2) + z[6];
    z[25]= - n<T>(5,2) - z[4];
    z[25]=z[4]*z[25];
    z[23]=3*z[23] + z[25];
    z[25]=n<T>(1,4) - z[2];
    z[25]=z[3]*z[25];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[25]=n<T>(1,2)*z[1];
    z[23]=z[23]*z[25];
    z[26]=n<T>(5,4) - z[2];
    z[26]=z[3]*z[26];
    z[20]=z[23] + n<T>(1,2)*z[20] + z[26];
    z[20]=z[1]*z[20];
    z[12]=z[12] + n<T>(1,2)*z[17] + z[20];
    z[12]=z[7]*z[12];
    z[17]=z[2] - 1;
    z[20]=z[17] + z[3];
    z[20]=z[20]*z[25];
    z[23]=n<T>(1,4)*z[10];
    z[26]=z[23] - z[17];
    z[26]=z[2]*z[26];
    z[28]=z[3] - 1;
    z[29]=z[17]*z[2];
    z[29]=z[29] - z[28];
    z[29]=z[29]*z[27];
    z[23]=z[29] + z[20] + z[26] + z[23] + z[28];
    z[23]=z[7]*z[23];
    z[26]= - z[10] + z[17];
    z[26]=z[26]*z[21];
    z[29]=n<T>(1,2)*z[10];
    z[20]=z[23] - z[20] - n<T>(3,4)*z[3] + z[26] - z[29] + 1;
    z[20]=z[7]*z[20];
    z[23]=n<T>(1,2)*z[3];
    z[26]=z[23] - 1;
    z[30]=z[23]*z[1];
    z[31]=z[10]*z[21];
    z[31]=z[30] + z[31] + z[29] + z[26];
    z[20]=n<T>(1,2)*z[31] + z[20];
    z[20]=z[8]*z[20];
    z[31]=9*z[2];
    z[32]=n<T>(13,2) - z[31];
    z[32]=z[2]*z[32];
    z[33]=n<T>(5,2)*z[3] - static_cast<T>(3)+ n<T>(13,2)*z[2];
    z[33]=z[1]*z[33];
    z[32]=z[33] + n<T>(9,2)*z[3] - n<T>(11,2) + z[32];
    z[17]=z[17]*z[18];
    z[17]=n<T>(5,2) + z[17];
    z[17]=n<T>(1,2)*z[17] - z[3];
    z[17]=z[7]*z[17];
    z[17]=n<T>(1,2)*z[32] + z[17];
    z[17]=z[17]*z[27];
    z[18]= - 3*z[3] + n<T>(7,2) - z[31];
    z[18]=z[18]*z[24];
    z[24]= - n<T>(1,2) + z[2];
    z[24]=z[2]*z[24];
    z[17]=z[17] + z[18] - n<T>(13,16)*z[3] + n<T>(17,8)*z[24] + z[22];
    z[17]=z[7]*z[17];
    z[18]=z[1] - z[2];
    z[22]= - n<T>(3,2) + 5*z[2];
    z[18]=z[22]*z[18];
    z[18]=z[23] - z[10] + z[18];
    z[17]=n<T>(1,4)*z[20] + n<T>(1,8)*z[18] + z[17];
    z[17]=z[8]*z[17];
    z[18]= - static_cast<T>(1)+ z[10];
    z[20]=z[9] - z[4];
    z[20]=z[4]*z[20];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[15]=n<T>(1,4)*z[16] - z[15];
    z[15]=z[4]*z[15];
    z[16]=z[2]*z[11];
    z[15]=z[15] + z[16];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(5)- z[9];
    z[16]=z[16]*z[21];
    z[16]=z[16] - static_cast<T>(1)- z[9];
    z[16]=z[16]*z[23];
    z[15]=z[16] + n<T>(1,2)*z[18] + z[15];
    z[12]=z[17] + z[12] + n<T>(1,4)*z[15] + z[14];
    z[12]=z[5]*z[12];
    z[14]=npow(z[9],2);
    z[15]=z[14]*z[28];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[3]*z[15];
    z[16]=npow(z[4],2);
    z[17]=z[14] + z[16];
    z[18]=z[25]*npow(z[3],2);
    z[17]=z[17]*z[18];
    z[15]=z[15] + z[17];
    z[15]=z[1]*z[15];
    z[17]=z[14] + 1;
    z[20]=z[17]*z[26];
    z[20]= - z[29] + z[20];
    z[20]=z[3]*z[20];
    z[21]=z[7]*z[28];
    z[15]=z[21] + z[15] + n<T>(1,2)*z[17] + z[20];
    z[17]=z[9] + n<T>(1,2);
    z[17]= - z[3]*z[17]*z[28];
    z[18]= - z[19]*z[18];
    z[17]=z[17] + z[18];
    z[17]=z[1]*z[17];
    z[18]=z[26]*z[3];
    z[19]= - z[9]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[7]*z[18];
    z[17]=z[18] + z[17] - z[13] + z[19];
    z[17]=z[8]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[8]*z[15];
    z[17]=z[9] - 1;
    z[18]=z[17]*z[13];
    z[19]= - z[9]*z[17];
    z[19]=z[4] + z[10] + z[19];
    z[19]=z[19]*z[23];
    z[11]=z[11] - 3;
    z[18]=z[19] + z[18] + z[11];
    z[18]=z[18]*z[23];
    z[14]= - z[14] + z[16];
    z[14]=z[14]*z[23];
    z[14]=z[14] + z[4] - z[17];
    z[14]=z[14]*z[30];
    z[11]=z[15] + z[14] + z[18] - z[11];
    z[14]= - n<T>(3,2) + z[3];
    z[14]=z[7]*z[14];
    z[11]=z[14] + n<T>(1,2)*z[11];
    z[11]=z[8]*z[11];
    z[14]=z[4]*z[9];
    z[15]= - z[9] - z[14];
    z[15]=z[1]*z[15];
    z[13]=z[15] - z[13] + z[14];
    z[13]=z[13]*z[3]*z[4];
    z[11]=n<T>(1,4)*z[13] + z[11];

    r += n<T>(1,2)*z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r1396(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1396(const std::array<dd_real,30>&);
#endif
