#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2023(const std::array<T,30>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[20];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[3];
    z[8]=k[15];
    z[9]=n<T>(1,2)*z[5];
    z[10]=15*z[3];
    z[11]=n<T>(1,2) - z[10];
    z[9]=z[11]*z[9];
    z[9]= - static_cast<T>(5)+ z[9];
    z[11]=npow(z[3],3);
    z[9]=z[11]*z[9];
    z[12]=npow(z[3],2);
    z[13]=z[12]*z[5];
    z[14]=z[13] + z[3];
    z[15]=n<T>(1,2)*z[6];
    z[14]=z[14]*z[15];
    z[16]=5*z[3];
    z[17]=z[16] - n<T>(1,2);
    z[17]= - z[17]*z[14];
    z[9]=z[9] + z[17];
    z[17]=z[6]*z[5];
    z[9]=z[9]*z[17];
    z[18]=3*z[3];
    z[19]=z[18] + 1;
    z[20]= - z[5]*z[19];
    z[20]=z[2] + z[20];
    z[20]=z[11]*z[20];
    z[21]=z[18] - 1;
    z[22]= - z[21]*z[12];
    z[20]=z[22] + z[20];
    z[20]=z[2]*z[20];
    z[10]=z[10] - n<T>(41,4);
    z[10]=z[10]*z[3];
    z[10]=z[10] + n<T>(1,2);
    z[22]=n<T>(1,2)*z[3];
    z[23]=z[10]*z[22];
    z[24]= - n<T>(7,4) + z[16];
    z[24]=z[24]*z[18];
    z[24]= - n<T>(5,2) + z[24];
    z[24]=z[24]*z[13];
    z[9]=n<T>(5,2)*z[20] + z[9] + z[23] + z[24];
    z[9]=z[2]*z[9];
    z[10]= - z[10]*z[12];
    z[11]=z[21]*z[11];
    z[20]= - z[2]*npow(z[3],4);
    z[11]=z[11] + z[20];
    z[11]=z[2]*z[11];
    z[10]=z[10] + 5*z[11];
    z[10]=z[2]*z[10];
    z[11]=z[16] - n<T>(11,2);
    z[11]=z[11]*z[3];
    z[20]=z[11] - n<T>(3,4);
    z[21]=z[20]*z[12];
    z[23]=z[22] + 3;
    z[23]=z[23]*z[3];
    z[23]=z[23] + n<T>(5,2);
    z[24]=z[4]*z[23]*z[22];
    z[10]=z[24] + z[21] + z[10];
    z[10]=z[1]*z[10];
    z[21]=z[5]*z[3];
    z[19]= - z[19]*z[21];
    z[24]= - static_cast<T>(1)- z[16];
    z[19]=n<T>(1,2)*z[24] + z[19];
    z[19]=z[5]*z[19];
    z[12]=z[12] - 1;
    z[12]=z[12]*z[22]*z[5];
    z[24]= - n<T>(1,2) - z[3];
    z[24]=z[3]*z[24];
    z[24]= - z[12] + n<T>(1,2) + z[24];
    z[24]=z[5]*z[24];
    z[25]= - static_cast<T>(1)- z[3];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[24]*z[17];
    z[19]=z[19] + z[24];
    z[19]=z[19]*z[15];
    z[19]= - z[21] + z[19];
    z[19]=z[8]*z[19];
    z[24]=n<T>(13,4) - z[18];
    z[24]=z[24]*z[16];
    z[24]=n<T>(7,4) + z[24];
    z[24]=z[3]*z[24];
    z[24]= - n<T>(1,2) + z[24];
    z[24]=z[24]*z[21];
    z[20]= - z[3]*z[20];
    z[10]=z[19] + z[10] + z[20] + z[24];
    z[19]=static_cast<T>(1)- z[3];
    z[19]=z[19]*z[22];
    z[12]= - z[12] + static_cast<T>(1)+ z[19];
    z[12]=z[5]*z[12];
    z[12]=n<T>(1,2) + z[12];
    z[12]=z[12]*z[17];
    z[17]=static_cast<T>(1)+ z[22];
    z[17]=z[17]*z[18];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[17]*z[21];
    z[19]=n<T>(5,2) + z[3];
    z[19]=z[3]*z[19];
    z[17]=z[17] - n<T>(1,2) + z[19];
    z[17]=z[5]*z[17];
    z[12]=z[17] + z[12];
    z[12]=z[12]*z[15];
    z[15]= - static_cast<T>(1)- n<T>(1,4)*z[3];
    z[15]=z[15]*z[18];
    z[15]= - n<T>(5,4) + z[15];
    z[15]=z[15]*z[21];
    z[12]=z[12] - n<T>(1,2)*z[23] + z[15];
    z[15]=z[7]*z[5]*npow(z[6],2);
    z[12]=n<T>(1,8)*z[15] + n<T>(1,2)*z[12];
    z[12]=z[4]*z[12];
    z[15]= - static_cast<T>(8)+ n<T>(15,2)*z[3];
    z[15]=z[3]*z[15];
    z[15]=n<T>(1,2) + z[15];
    z[13]=z[15]*z[13];
    z[15]= - n<T>(21,4) + z[16];
    z[15]=z[3]*z[15];
    z[15]=n<T>(1,4) + z[15];
    z[15]=z[3]*z[15];
    z[13]=z[15] + z[13];
    z[13]=z[5]*z[13];
    z[11]=z[11] + n<T>(1,2);
    z[11]= - npow(z[5],2)*z[11]*z[14];
    z[11]=z[13] + z[11];
    z[11]=z[6]*z[11];

    r += z[9] + n<T>(1,2)*z[10] + z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r2023(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2023(const std::array<dd_real,30>&);
#endif
