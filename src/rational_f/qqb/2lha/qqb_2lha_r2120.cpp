#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2120(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=n<T>(7,2)*z[1];
    z[6]=z[1] - 1;
    z[7]= - z[6]*z[5];
    z[8]=z[1] - 2;
    z[8]=z[8]*z[1];
    z[8]=z[8] + 1;
    z[9]=2*z[1];
    z[10]=z[9]*z[2];
    z[11]=z[8]*z[10];
    z[7]=z[7] + z[11];
    z[7]=z[2]*z[7];
    z[8]=z[8]*z[2];
    z[11]=2*z[6] - z[8];
    z[12]=npow(z[1],2);
    z[13]=z[12]*z[2];
    z[11]=z[11]*z[13];
    z[11]= - z[12] + z[11];
    z[11]=z[3]*z[11];
    z[7]=z[11] + n<T>(3,2)*z[1] + z[7];
    z[7]=z[3]*z[7];
    z[10]=z[6]*z[10];
    z[5]= - z[5] + z[10];
    z[5]=z[2]*z[5];
    z[10]= - z[6]*z[13];
    z[11]= - static_cast<T>(1)+ z[9];
    z[11]=z[1]*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[2]*z[10];
    z[10]= - z[1] + z[10];
    z[10]=z[3]*z[10];
    z[5]=2*z[10] + n<T>(3,2) + z[5];
    z[5]=z[3]*z[5];
    z[9]=z[9] - z[13];
    z[9]=z[2]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[4]*z[9]*npow(z[3],2);
    z[5]=z[5] + z[9];
    z[5]=z[4]*z[5];
    z[6]=n<T>(3,2)*z[6] - z[8];
    z[6]=z[2]*z[6];

    r +=  - static_cast<T>(1)+ z[5] + z[6] + z[7];
 
    return r;
}

template double qqb_2lha_r2120(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2120(const std::array<dd_real,30>&);
#endif
