#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1711(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=z[2] - 1;
    z[8]=n<T>(1,2)*z[1];
    z[9]=npow(z[2],2);
    z[10]= - z[7]*z[9]*z[8];
    z[11]=n<T>(3,4) - z[2];
    z[11]=z[2]*z[11];
    z[10]=z[11] + z[10];
    z[10]=z[1]*z[10];
    z[11]=z[2] - n<T>(1,2);
    z[12]=n<T>(1,2)*z[11];
    z[10]= - z[12] + z[10];
    z[13]=z[3]*z[1];
    z[10]=z[10]*z[13];
    z[14]=n<T>(5,4)*z[1];
    z[15]= - z[9]*z[14];
    z[15]= - z[2] + z[15];
    z[15]=z[1]*z[15];
    z[16]=z[1]*z[2];
    z[17]=n<T>(1,2)*z[16];
    z[7]= - z[7]*z[17];
    z[7]=z[7] - z[11];
    z[7]=z[1]*z[7];
    z[7]= - n<T>(1,2) + z[7];
    z[7]=z[7]*z[13];
    z[18]= - n<T>(1,2) - z[16];
    z[18]=z[1]*z[18];
    z[7]=z[18] + z[7];
    z[18]=n<T>(1,2)*z[4];
    z[7]=z[7]*z[18];
    z[7]=z[7] + z[15] + z[10];
    z[7]=z[3]*z[7];
    z[7]= - z[17] + z[7];
    z[7]=z[7]*z[18];
    z[10]=npow(z[2],3);
    z[14]=z[10]*z[14];
    z[14]=z[9] + z[14];
    z[14]=z[1]*z[14];
    z[15]=z[1]*z[9];
    z[15]=z[2] + z[15];
    z[13]=z[15]*z[13];
    z[13]=z[14] + n<T>(1,4)*z[13];
    z[13]=z[3]*z[13];
    z[14]=static_cast<T>(3)+ 7*z[2];
    z[8]=z[14]*z[8];
    z[8]= - n<T>(3,2)*z[5] + z[8];
    z[8]=z[9]*z[8];
    z[14]=z[2] - n<T>(1,4);
    z[15]=3*z[2];
    z[15]=z[14]*z[15];
    z[8]=z[15] + z[8];
    z[15]=n<T>(1,2)*z[5];
    z[8]=z[8]*z[15];
    z[14]= - z[14]*z[9];
    z[10]=z[10]*z[15];
    z[10]=z[14] + z[10];
    z[10]=z[5]*z[10];
    z[9]=z[9]*z[12];
    z[9]=z[9] + z[10];
    z[9]=z[6]*z[9];
    z[10]=z[11]*z[2];
    z[11]=n<T>(3,2) - 7*z[10];
    z[11]=z[11]*z[16];
    z[10]= - 3*z[10] + z[11];

    r += z[7] + z[8] + n<T>(3,2)*z[9] + n<T>(1,4)*z[10] + z[13];
 
    return r;
}

template double qqb_2lha_r1711(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1711(const std::array<dd_real,30>&);
#endif
