#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1224(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[8];
    z[9]=k[11];
    z[10]=k[15];
    z[11]=n<T>(1,2)*z[4];
    z[12]=n<T>(91,6) + z[4];
    z[12]=z[12]*z[11];
    z[13]=n<T>(1,2)*z[5];
    z[14]=n<T>(1,4)*z[4];
    z[15]=z[14] - 1;
    z[16]=z[4]*z[15];
    z[16]=n<T>(25,2) + n<T>(53,3)*z[16];
    z[16]=z[16]*z[13];
    z[17]=n<T>(7,4)*z[3];
    z[12]=z[16] + z[12] - z[17] + n<T>(15,2)*z[10] - static_cast<T>(13)- n<T>(19,4)*z[7];
    z[12]=z[12]*z[13];
    z[13]= - static_cast<T>(2)+ n<T>(1,2)*z[10];
    z[16]=3*z[10];
    z[18]=z[13]*z[16];
    z[19]=npow(z[3],2);
    z[20]= - n<T>(13,4) + 3*z[3];
    z[20]=z[20]*z[19];
    z[21]=npow(z[3],3);
    z[22]=z[21]*z[2];
    z[20]= - 3*z[22] - static_cast<T>(5)+ z[20];
    z[23]=n<T>(1,4)*z[2];
    z[20]=z[20]*z[23];
    z[24]=n<T>(1,2)*z[3];
    z[25]=static_cast<T>(1)+ z[24];
    z[25]=z[3]*z[25];
    z[20]=z[20] - n<T>(1,16) + z[25];
    z[20]=z[2]*z[20];
    z[25]= - n<T>(5,4) + z[10];
    z[25]=z[10]*z[25];
    z[25]= - z[23] + n<T>(1,4) + z[25];
    z[25]=z[4]*z[25];
    z[12]=z[12] + z[25] + z[20] + n<T>(7,8)*z[3] + n<T>(109,16) + z[18];
    z[12]=z[9]*z[12];
    z[18]=n<T>(11,3) + n<T>(31,2)*z[7];
    z[17]=n<T>(1,8)*z[4] + z[17] + n<T>(1,2)*z[18] - 11*z[10];
    z[18]=n<T>(1,3)*z[4];
    z[20]=z[18]*z[5];
    z[25]=static_cast<T>(16)- n<T>(53,8)*z[4];
    z[25]=z[25]*z[20];
    z[26]= - n<T>(55,6) - z[4];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(5,3) + z[26];
    z[25]=n<T>(1,4)*z[26] + z[25];
    z[25]=z[5]*z[25];
    z[17]=n<T>(1,2)*z[17] + z[25];
    z[17]=z[5]*z[17];
    z[25]=npow(z[10],2);
    z[26]=z[25]*z[11];
    z[27]=n<T>(5,4) - z[3];
    z[27]=z[27]*z[19];
    z[27]=z[22] + n<T>(7,8) + z[27];
    z[27]=z[2]*z[27];
    z[28]=static_cast<T>(7)- 5*z[10];
    z[28]=z[10]*z[28];
    z[28]= - z[3] + n<T>(3,8) + z[28];
    z[12]=z[12] + z[17] - z[26] + n<T>(1,2)*z[28] + z[27];
    z[12]=z[9]*z[12];
    z[17]= - static_cast<T>(11)+ z[16];
    z[17]=z[10]*z[17];
    z[27]=n<T>(1,4)*z[3];
    z[28]=static_cast<T>(1)+ z[27];
    z[28]=z[3]*z[28];
    z[17]=z[11] + z[28] + n<T>(55,8) + z[17];
    z[28]=n<T>(41,12)*z[4] - z[24] + n<T>(13,2)*z[10] - n<T>(47,4) - 5*z[7];
    z[29]=static_cast<T>(3)- n<T>(47,24)*z[4];
    z[29]=z[5]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[28]=z[5]*z[28];
    z[17]=n<T>(1,2)*z[17] + z[28];
    z[17]=z[5]*z[17];
    z[28]=z[3] + 1;
    z[29]=n<T>(1,8)*z[3];
    z[30]= - z[28]*z[29];
    z[31]=n<T>(3,2)*z[22] + static_cast<T>(1)+ n<T>(7,4)*z[19];
    z[32]=n<T>(1,2)*z[2];
    z[31]=z[31]*z[32];
    z[33]=n<T>(11,4) - 2*z[10];
    z[33]=z[10]*z[33];
    z[17]=z[17] - z[26] + z[31] + z[30] + n<T>(3,16) + z[33];
    z[17]=z[9]*z[17];
    z[26]= - static_cast<T>(1)- n<T>(35,2)*z[4];
    z[30]=11*z[5];
    z[31]=z[4]*z[30];
    z[26]=n<T>(1,4)*z[26] + z[31];
    z[31]=n<T>(1,3)*z[5];
    z[26]=z[26]*z[31];
    z[16]=z[26] - z[16] - n<T>(2,3) + n<T>(23,4)*z[7];
    z[16]=z[5]*z[16];
    z[26]=n<T>(5,8)*z[19];
    z[33]=z[10] - 1;
    z[34]=z[33]*z[10];
    z[16]=z[16] - 2*z[34] + z[26];
    z[16]=z[5]*z[16];
    z[35]=z[21]*z[32];
    z[36]= - z[25] + z[35] + n<T>(1,8);
    z[16]=z[17] + z[16] - z[26] - z[36];
    z[16]=z[9]*z[16];
    z[17]=n<T>(1,2)*z[19];
    z[26]= - z[28]*z[17];
    z[26]=z[26] - z[36];
    z[13]=z[10]*z[13];
    z[28]= - n<T>(41,3) - 7*z[7];
    z[28]=n<T>(11,12)*z[5] + n<T>(1,8)*z[28] + z[10];
    z[28]=z[5]*z[28];
    z[13]=z[28] + n<T>(35,48) + z[13];
    z[13]=z[5]*z[13];
    z[13]=z[13] - z[34] + n<T>(1,4)*z[21];
    z[13]=z[5]*z[13];
    z[13]=n<T>(1,2)*z[26] + z[13];
    z[13]=z[9]*z[13];
    z[21]=z[7]*npow(z[5],3);
    z[13]=n<T>(21,8)*z[21] + z[13];
    z[13]=z[9]*z[13];
    z[26]=n<T>(1,2)*z[1];
    z[28]=z[26] - 1;
    z[21]=z[28]*z[21];
    z[13]=n<T>(7,4)*z[21] + z[13];
    z[13]=z[6]*z[13];
    z[21]=3*z[7];
    z[34]=z[1]*z[21];
    z[28]=z[5]*z[28];
    z[28]= - 7*z[28] - 13*z[7] + z[34];
    z[28]=z[28]*npow(z[5],2);
    z[13]=z[13] + n<T>(1,4)*z[28] + z[16];
    z[13]=z[6]*z[13];
    z[15]=z[15]*z[18];
    z[15]=n<T>(1,2) + z[15];
    z[16]=npow(z[4],2);
    z[15]=z[15]*z[16]*z[30];
    z[28]=7*z[1];
    z[34]=n<T>(85,3) + z[28];
    z[36]= - static_cast<T>(19)+ 5*z[4];
    z[36]=z[4]*z[36];
    z[34]=n<T>(1,2)*z[34] + z[36];
    z[34]=z[34]*z[14];
    z[15]=z[34] + z[15];
    z[15]=z[5]*z[15];
    z[34]=n<T>(5,6) + z[1];
    z[36]=z[11] - 1;
    z[37]=z[4]*z[36];
    z[15]=z[15] + n<T>(1,8)*z[34] + z[37];
    z[15]=z[5]*z[15];
    z[34]=n<T>(3,4)*z[3];
    z[37]= - n<T>(5,4) - z[21];
    z[15]=z[15] + z[14] + z[34] + n<T>(1,2)*z[37] + z[10];
    z[15]=z[5]*z[15];
    z[37]= - n<T>(3,2) + z[3];
    z[37]=z[37]*z[19];
    z[22]=z[22] - z[37];
    z[12]=z[13] + z[12] + z[15] - n<T>(1,16) + z[25] - n<T>(1,4)*z[22];
    z[12]=z[6]*z[12];
    z[13]= - n<T>(1,4) + z[3];
    z[13]=z[13]*z[19];
    z[13]= - z[35] - n<T>(13,8) + z[13];
    z[13]=z[2]*z[13];
    z[15]=z[3] - n<T>(1,2);
    z[22]= - z[15]*z[24];
    z[22]=static_cast<T>(2)+ z[22];
    z[22]=z[3]*z[22];
    z[13]=z[13] + n<T>(3,8) + z[22];
    z[13]=z[2]*z[13];
    z[22]= - n<T>(1,8) - z[3];
    z[22]=z[22]*z[19];
    z[22]=z[35] + n<T>(11,4) + z[22];
    z[22]=z[2]*z[22];
    z[25]=n<T>(1,4) + z[3];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(31,4) + z[25];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(9,4) + z[25];
    z[22]=n<T>(1,2)*z[25] + z[22];
    z[22]=z[2]*z[22];
    z[22]=z[22] - n<T>(179,24) + z[3];
    z[22]=z[2]*z[22];
    z[25]=npow(z[2],2);
    z[37]=static_cast<T>(3)- z[10];
    z[37]=z[10]*z[37];
    z[37]=n<T>(5,4)*z[25] + n<T>(11,24) + z[37];
    z[37]=z[4]*z[37];
    z[21]=9*z[10] - n<T>(101,12) - z[21];
    z[21]=z[37] + n<T>(1,2)*z[21] + z[22];
    z[22]= - static_cast<T>(25)+ n<T>(59,4)*z[4];
    z[22]=z[22]*z[14];
    z[22]=static_cast<T>(2)+ z[22];
    z[22]=z[22]*z[31];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[21]=z[9]*z[21];
    z[22]= - n<T>(3,2) + z[10];
    z[22]=z[10]*z[22];
    z[22]= - z[32] - n<T>(1,4) + z[22];
    z[22]=z[22]*z[11];
    z[31]=static_cast<T>(11)- n<T>(31,2)*z[4];
    z[20]=z[31]*z[20];
    z[31]= - n<T>(7,3) - z[11];
    z[31]=z[4]*z[31];
    z[20]=z[20] - n<T>(11,6) + z[31];
    z[20]=z[5]*z[20];
    z[13]=z[21] + n<T>(1,4)*z[20] + z[22] + z[13] - n<T>(13,4)*z[10] + n<T>(10,3) + 
   n<T>(3,4)*z[7];
    z[13]=z[9]*z[13];
    z[15]= - z[15]*z[19];
    z[15]=z[35] + n<T>(3,4) + z[15];
    z[15]=z[15]*z[32];
    z[19]=n<T>(1,3)*z[1];
    z[20]=z[18] - static_cast<T>(1)- z[19];
    z[20]=z[20]*z[11];
    z[21]=z[1] + 1;
    z[20]=n<T>(1,3)*z[21] + z[20];
    z[22]=npow(z[4],3);
    z[20]=z[20]*z[22]*z[30];
    z[30]=55*z[4] - static_cast<T>(145)- n<T>(107,2)*z[1];
    z[30]=z[30]*z[18];
    z[30]=z[30] + static_cast<T>(31)+ n<T>(73,3)*z[1];
    z[30]=z[30]*z[16];
    z[20]=n<T>(1,4)*z[30] + z[20];
    z[20]=z[5]*z[20];
    z[30]=15*z[4] - static_cast<T>(33)- n<T>(29,2)*z[1];
    z[30]=z[30]*z[11];
    z[30]=z[30] + n<T>(43,6) + 5*z[1];
    z[30]=z[30]*z[11];
    z[20]=z[30] + z[20];
    z[20]=z[5]*z[20];
    z[30]=3*z[4];
    z[31]=z[30] - n<T>(47,8) - 3*z[1];
    z[31]=z[4]*z[31];
    z[31]=z[31] + n<T>(41,24) + z[1];
    z[20]=n<T>(1,2)*z[31] + z[20];
    z[20]=z[5]*z[20];
    z[31]= - static_cast<T>(1)+ z[3];
    z[31]=z[3]*z[31];
    z[31]=z[26] + z[31];
    z[31]=z[31]*z[27];
    z[35]= - n<T>(7,4) - z[1];
    z[12]=z[12] + z[13] + z[20] + z[11] + z[15] + z[31] + n<T>(1,2)*z[35] + 
    z[10];
    z[12]=z[6]*z[12];
    z[13]= - z[2]*z[8];
    z[13]=static_cast<T>(3)+ z[13];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,2) + z[13];
    z[13]=z[13]*z[32];
    z[15]= - z[36]*z[11];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];
    z[13]=z[15] - z[14] + z[13] - n<T>(3,4) + z[10];
    z[13]=z[14]*z[13];
    z[14]=z[17] - 1;
    z[14]=z[8]*z[14];
    z[15]=z[14]*z[32];
    z[17]=z[8] + n<T>(9,2);
    z[17]=n<T>(1,2)*z[17];
    z[20]= - static_cast<T>(1)- n<T>(3,4)*z[8];
    z[20]=z[3]*z[20];
    z[20]=n<T>(9,8)*z[8] + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[15] + z[17] + z[20];
    z[20]=z[20]*z[32];
    z[31]=3*z[8];
    z[35]= - n<T>(11,2) - z[31];
    z[36]=static_cast<T>(1)+ n<T>(3,8)*z[8];
    z[36]=z[3]*z[36];
    z[35]=n<T>(3,8)*z[35] + z[36];
    z[35]=z[3]*z[35];
    z[36]= - n<T>(13,4) + z[8];
    z[20]=z[20] + n<T>(1,4)*z[36] + z[35];
    z[20]=z[2]*z[20];
    z[35]=static_cast<T>(11)+ z[31];
    z[36]= - static_cast<T>(1)- n<T>(1,4)*z[8];
    z[36]=z[3]*z[36];
    z[35]=n<T>(3,8)*z[35] + z[36];
    z[35]=z[3]*z[35];
    z[36]=z[8] + 5;
    z[35]= - n<T>(1,2)*z[36] + z[35];
    z[20]=n<T>(1,2)*z[35] + z[20];
    z[20]=z[2]*z[20];
    z[35]=5*z[8];
    z[37]= - static_cast<T>(5)- n<T>(3,2)*z[8];
    z[37]=z[3]*z[37];
    z[37]=z[37] + static_cast<T>(11)+ z[35];
    z[37]=z[37]*z[24];
    z[31]=static_cast<T>(5)+ z[31];
    z[31]=z[3]*z[31];
    z[31]= - z[35] + z[31];
    z[24]=z[31]*z[24];
    z[14]= - z[2]*z[14];
    z[14]=z[14] + z[24] - n<T>(11,2) - z[8];
    z[14]=z[14]*z[32];
    z[14]=z[14] + z[37] + n<T>(9,4) - z[8];
    z[14]=z[14]*z[32];
    z[24]=z[36]*z[29];
    z[24]=z[24] - static_cast<T>(3)- n<T>(5,8)*z[8];
    z[24]=z[3]*z[24];
    z[14]=z[14] + z[17] + z[24];
    z[14]=z[2]*z[14];
    z[17]=z[27] - 1;
    z[14]=z[14] + z[17];
    z[14]=z[2]*z[14];
    z[23]=z[8]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[25];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[2]*z[23];
    z[23]= - n<T>(1,2)*z[33] + z[23];
    z[23]=z[4]*z[23];
    z[14]=z[14] + z[23];
    z[14]=z[9]*z[14];
    z[13]=n<T>(1,2)*z[14] + n<T>(1,16) + z[20] + z[13];
    z[13]=z[9]*z[13];
    z[14]= - z[21]*z[26];
    z[20]= - static_cast<T>(1)- n<T>(1,2)*z[8];
    z[20]=z[20]*z[34];
    z[14]=z[20] + z[14] + n<T>(3,2) + z[8];
    z[14]=z[3]*z[14];
    z[20]=static_cast<T>(1)+ z[8];
    z[20]=z[20]*z[34];
    z[20]= - z[8] + z[20];
    z[20]=z[3]*z[20];
    z[23]= - n<T>(7,2) - z[8];
    z[15]= - z[15] + n<T>(1,2)*z[23] + z[20];
    z[15]=z[15]*z[32];
    z[14]=z[15] + z[14] + z[21];
    z[14]=z[2]*z[14];
    z[15]=z[8] + 3;
    z[15]=z[15]*z[17];
    z[17]=z[21]*z[1];
    z[15]=z[17] + z[15];
    z[15]=z[3]*z[15];
    z[17]=static_cast<T>(9)+ 13*z[17];
    z[15]=n<T>(1,4)*z[17] + z[15];
    z[17]=n<T>(7,2)*z[4] - z[2] - n<T>(19,4) - z[28];
    z[17]=z[17]*z[11];
    z[14]=z[17] + n<T>(1,2)*z[15] + z[14];
    z[15]=static_cast<T>(163)+ n<T>(203,2)*z[1];
    z[15]=z[15]*z[19];
    z[15]=n<T>(35,2) + z[15];
    z[17]=n<T>(109,4)*z[4] - n<T>(169,4) - 53*z[1];
    z[17]=z[17]*z[18];
    z[15]=n<T>(1,4)*z[15] + z[17];
    z[15]=z[15]*z[16];
    z[16]=static_cast<T>(23)+ n<T>(77,6)*z[1];
    z[16]=z[1]*z[16];
    z[16]=n<T>(61,6) + z[16];
    z[17]= - static_cast<T>(47)- n<T>(157,3)*z[1];
    z[17]=n<T>(1,8)*z[17] + n<T>(10,3)*z[4];
    z[17]=z[4]*z[17];
    z[16]=n<T>(1,4)*z[16] + z[17];
    z[16]=z[16]*z[22];
    z[11]=z[11] - z[21];
    z[11]=z[4]*z[11];
    z[17]=static_cast<T>(1)+ z[26];
    z[17]=z[1]*z[17];
    z[11]=z[11] + n<T>(1,2) + z[17];
    z[11]=z[5]*z[11]*npow(z[4],4);
    z[11]=z[16] + n<T>(11,6)*z[11];
    z[11]=z[5]*z[11];
    z[11]=n<T>(1,2)*z[15] + z[11];
    z[11]=z[5]*z[11];
    z[15]= - n<T>(71,2) - 47*z[1];
    z[15]=n<T>(1,8)*z[15] + z[30];
    z[15]=z[4]*z[15];
    z[16]=n<T>(67,4) + 11*z[1];
    z[16]=z[1]*z[16];
    z[16]=n<T>(17,4) + z[16];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[15]=z[4]*z[15];
    z[11]=z[15] + z[11];
    z[11]=z[5]*z[11];

    r += z[11] + z[12] + z[13] + n<T>(1,2)*z[14];
 
    return r;
}

template double qqb_2lha_r1224(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1224(const std::array<dd_real,30>&);
#endif
