#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1877(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=4*z[6];
    z[8]=static_cast<T>(7)+ z[7];
    z[8]=z[8]*z[7];
    z[9]=5*z[6];
    z[10]= - static_cast<T>(2)- z[6];
    z[10]=z[10]*z[9];
    z[10]= - static_cast<T>(2)+ z[10];
    z[11]=2*z[4];
    z[10]=z[10]*z[11];
    z[8]=z[10] + static_cast<T>(3)+ z[8];
    z[8]=z[4]*z[8];
    z[10]=3*z[6];
    z[12]=2*z[6];
    z[13]= - static_cast<T>(3)- z[12];
    z[13]=z[13]*z[10];
    z[14]=z[6] + 1;
    z[15]=z[4]*z[6];
    z[16]=z[14]*z[15];
    z[17]=static_cast<T>(3)+ z[7];
    z[17]=z[6]*z[17];
    z[16]=z[17] - 5*z[16];
    z[16]=z[4]*z[16];
    z[17]=z[5]*npow(z[15],2);
    z[16]=z[16] - z[17];
    z[16]=z[5]*z[16];
    z[8]=z[16] + z[13] + z[8];
    z[8]=z[5]*z[8];
    z[13]=static_cast<T>(11)+ z[7];
    z[13]=z[13]*z[12];
    z[16]=z[1] + 6;
    z[13]=z[13] + z[16];
    z[18]=z[6] + 3;
    z[18]=z[18]*z[6];
    z[19]=z[1] + 4;
    z[20]= - 3*z[19] - 10*z[18];
    z[20]=z[4]*z[20];
    z[13]=3*z[13] + z[20];
    z[13]=z[4]*z[13];
    z[20]=2*z[2];
    z[21]= - static_cast<T>(7)- z[20];
    z[21]=z[21]*z[12];
    z[22]=z[2] + 4;
    z[21]= - 9*z[22] + z[21];
    z[21]=z[6]*z[21];
    z[8]=z[8] + z[13] - static_cast<T>(6)+ z[21];
    z[8]=z[5]*z[8];
    z[13]=static_cast<T>(15)+ z[7];
    z[13]=z[13]*z[7];
    z[21]=z[6] + 4;
    z[23]= - z[21]*z[9];
    z[24]= - static_cast<T>(3)- z[1];
    z[24]=z[1]*z[24];
    z[24]= - static_cast<T>(6)+ z[24];
    z[23]=2*z[24] + z[23];
    z[23]=z[4]*z[23];
    z[24]=3*z[1];
    z[25]=static_cast<T>(10)+ z[24];
    z[25]=z[1]*z[25];
    z[13]=z[23] + z[13] + static_cast<T>(27)+ z[25];
    z[13]=z[4]*z[13];
    z[23]=z[1] + 2;
    z[23]=z[23]*z[1];
    z[25]=z[2]*z[1];
    z[26]= - z[25] - static_cast<T>(3)+ z[23];
    z[26]=z[2]*z[26];
    z[27]=3*z[2];
    z[28]= - static_cast<T>(20)- z[27];
    z[28]=z[2]*z[28];
    z[29]=z[2] + 6;
    z[30]= - z[2]*z[29];
    z[30]= - static_cast<T>(11)+ z[30];
    z[30]=z[6]*z[30];
    z[28]=z[30] - static_cast<T>(40)+ z[28];
    z[28]=z[6]*z[28];
    z[19]= - z[1]*z[19];
    z[8]=z[8] + z[13] + z[28] + z[26] - static_cast<T>(15)+ z[19];
    z[8]=z[5]*z[8];
    z[13]= - static_cast<T>(3)- z[23];
    z[13]=z[1]*z[13];
    z[19]= - static_cast<T>(5)- z[6];
    z[19]=z[6]*z[19];
    z[13]=z[19] - static_cast<T>(4)+ z[13];
    z[13]=z[4]*z[13];
    z[19]=static_cast<T>(4)+ z[24];
    z[19]=z[1]*z[19];
    z[19]=static_cast<T>(7)+ z[19];
    z[19]=z[1]*z[19];
    z[23]=static_cast<T>(19)+ z[7];
    z[23]=z[6]*z[23];
    z[13]=z[13] + z[23] + static_cast<T>(12)+ z[19];
    z[13]=z[4]*z[13];
    z[19]=z[1] + 1;
    z[23]=2*z[1];
    z[24]=z[19]*z[23];
    z[25]=z[24] - z[25];
    z[25]=z[2]*z[25];
    z[26]=npow(z[1],2);
    z[28]= - static_cast<T>(1)- z[26];
    z[28]=z[1]*z[28];
    z[25]=z[25] - static_cast<T>(4)+ z[28];
    z[25]=z[2]*z[25];
    z[28]= - static_cast<T>(9)- 4*z[2];
    z[28]=z[2]*z[28];
    z[30]= - static_cast<T>(2)- z[2];
    z[30]=z[2]*z[30];
    z[30]= - static_cast<T>(3)+ z[30];
    z[30]=z[6]*z[30];
    z[28]=z[30] - static_cast<T>(14)+ z[28];
    z[28]=z[6]*z[28];
    z[30]= - z[1]*z[19];
    z[30]= - static_cast<T>(2)+ z[30];
    z[30]=z[1]*z[30];
    z[30]= - static_cast<T>(4)+ z[30];
    z[8]=z[8] + z[13] + z[28] + 2*z[30] + z[25];
    z[8]=z[3]*z[8];
    z[13]=29*z[6];
    z[14]= - z[14]*z[13];
    z[25]=static_cast<T>(19)+ 13*z[6];
    z[25]=z[6]*z[25];
    z[25]=static_cast<T>(3)+ z[25];
    z[25]=z[25]*z[11];
    z[14]=z[25] - static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[25]=static_cast<T>(1)+ z[10];
    z[25]=z[25]*z[12];
    z[14]=z[25] + z[14];
    z[25]=6*z[6];
    z[28]= - static_cast<T>(1)- z[25];
    z[28]=z[28]*z[12];
    z[30]=23*z[6];
    z[31]=static_cast<T>(16)+ z[30];
    z[31]=z[31]*z[15];
    z[28]=z[28] + z[31];
    z[28]=z[4]*z[28];
    z[28]=z[28] + 4*z[17];
    z[28]=z[5]*z[28];
    z[14]=2*z[14] + z[28];
    z[14]=z[5]*z[14];
    z[28]= - static_cast<T>(32)- 17*z[6];
    z[28]=z[28]*z[10];
    z[28]=z[28] - static_cast<T>(15)- z[1];
    z[13]=static_cast<T>(66)+ z[13];
    z[13]=z[13]*z[12];
    z[13]=z[13] + static_cast<T>(39)+ 8*z[1];
    z[13]=z[4]*z[13];
    z[13]=2*z[28] + z[13];
    z[13]=z[4]*z[13];
    z[28]=z[2] + 9;
    z[7]=z[28]*z[7];
    z[7]=z[7] + static_cast<T>(47)+ z[20];
    z[7]=z[6]*z[7];
    z[7]=z[14] + z[13] + static_cast<T>(2)+ z[7];
    z[7]=z[5]*z[7];
    z[13]=8*z[6];
    z[14]=static_cast<T>(25)+ z[13];
    z[14]=z[14]*z[12];
    z[31]=static_cast<T>(7)+ z[23];
    z[31]=z[1]*z[31];
    z[14]=z[14] + static_cast<T>(21)+ z[31];
    z[14]=z[4]*z[14];
    z[31]= - static_cast<T>(5)- z[1];
    z[31]=z[1]*z[31];
    z[32]= - static_cast<T>(109)- 39*z[6];
    z[32]=z[6]*z[32];
    z[14]=z[14] + z[32] - static_cast<T>(30)+ z[31];
    z[11]=z[14]*z[11];
    z[14]=static_cast<T>(17)+ 5*z[2];
    z[14]=z[6]*z[14];
    z[14]=z[14] + static_cast<T>(39)+ 8*z[2];
    z[12]=z[14]*z[12];
    z[14]=z[2] - 2;
    z[31]= - z[2]*z[14];
    z[7]=z[7] + z[11] + z[12] + static_cast<T>(12)+ z[31];
    z[7]=z[5]*z[7];
    z[11]= - static_cast<T>(4)+ z[1];
    z[11]=z[1]*z[11];
    z[12]= - static_cast<T>(41)- 11*z[6];
    z[12]=z[6]*z[12];
    z[11]=z[12] - static_cast<T>(16)+ z[11];
    z[12]=7*z[6];
    z[21]=z[21]*z[12];
    z[16]=z[1]*z[16];
    z[16]=z[21] + static_cast<T>(15)+ z[16];
    z[16]=z[4]*z[16];
    z[11]=2*z[11] + z[16];
    z[11]=z[4]*z[11];
    z[16]=z[23] + 3;
    z[21]= - z[16]*z[26];
    z[23]=static_cast<T>(1)+ z[24];
    z[14]= - z[1] - z[14];
    z[14]=z[2]*z[14];
    z[14]=2*z[23] + z[14];
    z[14]=z[2]*z[14];
    z[23]=static_cast<T>(10)+ z[27];
    z[23]=z[2]*z[23];
    z[24]=z[2]*z[22];
    z[24]=static_cast<T>(11)+ z[24];
    z[24]=z[6]*z[24];
    z[23]=z[24] + static_cast<T>(36)+ z[23];
    z[23]=z[6]*z[23];
    z[7]=2*z[8] + z[7] + z[11] + z[23] + z[14] + static_cast<T>(9)+ z[21];
    z[7]=z[3]*z[7];
    z[8]=static_cast<T>(25)+ z[30];
    z[8]=z[8]*z[10];
    z[11]=z[1] + 8;
    z[14]= - static_cast<T>(93)- 59*z[6];
    z[14]=z[6]*z[14];
    z[14]= - 2*z[11] + z[14];
    z[14]=z[4]*z[14];
    z[8]=z[14] + static_cast<T>(4)+ z[8];
    z[8]=z[4]*z[8];
    z[9]=static_cast<T>(2)+ z[9];
    z[9]=z[9]*z[25];
    z[14]= - static_cast<T>(14)- 15*z[6];
    z[14]=z[14]*z[10];
    z[14]= - static_cast<T>(4)+ z[14];
    z[14]=z[4]*z[14];
    z[9]=z[9] + z[14];
    z[9]=z[4]*z[9];
    z[14]= - static_cast<T>(3)- z[13];
    z[14]=z[14]*z[15];
    z[21]=npow(z[6],2);
    z[23]=2*z[21];
    z[14]=z[23] + z[14];
    z[14]=z[4]*z[14];
    z[14]=z[14] - z[17];
    z[14]=z[5]*z[14];
    z[9]=2*z[14] - z[23] + z[9];
    z[9]=z[5]*z[9];
    z[14]= - static_cast<T>(5)- 12*z[6];
    z[14]=z[6]*z[14];
    z[8]=z[9] + z[14] + z[8];
    z[8]=z[5]*z[8];
    z[9]=static_cast<T>(15)+ z[13];
    z[9]=z[9]*z[13];
    z[13]= - static_cast<T>(10)- z[1];
    z[14]= - static_cast<T>(84)- 37*z[6];
    z[14]=z[6]*z[14];
    z[13]=2*z[13] + z[14];
    z[13]=z[4]*z[13];
    z[9]=z[13] + static_cast<T>(15)+ z[9];
    z[9]=z[4]*z[9];
    z[13]=z[20] + 15;
    z[13]=z[13]*z[6];
    z[13]=z[13] + z[2];
    z[14]= - static_cast<T>(17)- z[13];
    z[14]=z[6]*z[14];
    z[8]=z[8] + z[9] - z[2] + z[14];
    z[8]=z[5]*z[8];
    z[9]=static_cast<T>(19)+ z[12];
    z[9]=z[9]*z[10];
    z[11]= - 9*z[18] - z[11];
    z[11]=z[4]*z[11];
    z[9]=z[11] + z[9] + static_cast<T>(11)+ z[1];
    z[9]=z[4]*z[9];
    z[11]= - z[20] + z[16];
    z[11]=z[2]*z[11];
    z[12]=z[29]*z[6];
    z[14]= - static_cast<T>(12)- z[12];
    z[14]=z[6]*z[14];
    z[7]=z[7] + z[8] + z[9] + z[14] - static_cast<T>(1)+ z[11];
    z[7]=z[3]*z[7];
    z[8]=z[2] + 12;
    z[9]= - z[12] - z[8];
    z[9]=z[6]*z[9];
    z[11]=z[6]*z[22];
    z[11]=z[11] + z[28];
    z[11]=z[6]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[4]*z[11];
    z[9]=z[11] - static_cast<T>(1)+ z[9];
    z[9]=z[4]*z[9];
    z[11]=static_cast<T>(7)+ z[2];
    z[11]=z[11]*z[10];
    z[11]=z[11] + static_cast<T>(21)+ z[2];
    z[11]=z[6]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[4]*z[11];
    z[12]= - z[6]*z[8];
    z[12]= - static_cast<T>(6)+ z[12];
    z[12]=z[6]*z[12];
    z[11]=z[12] + z[11];
    z[11]=z[4]*z[11];
    z[12]=static_cast<T>(13)+ z[2];
    z[12]=z[6]*z[12];
    z[12]=static_cast<T>(6)+ z[12];
    z[12]=z[12]*z[15];
    z[12]= - 3*z[21] + z[12];
    z[12]=z[4]*z[12];
    z[12]=z[12] + 3*z[17];
    z[12]=z[5]*z[12];
    z[11]=z[11] + z[12];
    z[11]=z[5]*z[11];
    z[12]=static_cast<T>(5)+ z[2];
    z[10]=z[12]*z[10];
    z[8]=2*z[8] + z[10];
    z[8]=z[6]*z[8];
    z[8]=static_cast<T>(2)+ z[8];
    z[8]=z[4]*z[8];
    z[10]= - static_cast<T>(18)- z[13];
    z[10]=z[6]*z[10];
    z[8]=z[8] - static_cast<T>(1)+ z[10];
    z[8]=z[4]*z[8];
    z[8]=z[11] - z[6] + z[8];
    z[8]=z[5]*z[8];

    r +=  - z[2] + z[7] + z[8] + z[9] + z[19];
 
    return r;
}

template double qqb_2lha_r1877(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1877(const std::array<dd_real,30>&);
#endif
