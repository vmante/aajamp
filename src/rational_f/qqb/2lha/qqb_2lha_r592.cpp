#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r592(const std::array<T,30>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[9];
    z[6]=k[10];
    z[7]=k[4];
    z[8]=k[21];
    z[9]=k[11];
    z[10]=static_cast<T>(1)+ 3*z[4];
    z[11]=n<T>(1,2)*z[4];
    z[12]= - z[10]*z[11];
    z[13]= - static_cast<T>(1)- z[11];
    z[13]=z[7]*z[4]*z[13];
    z[12]=z[13] + static_cast<T>(1)+ z[12];
    z[13]=n<T>(1,2)*z[7];
    z[12]=z[12]*z[13];
    z[14]= - static_cast<T>(1)+ n<T>(3,4)*z[4];
    z[15]=z[14]*z[4];
    z[16]= - n<T>(3,2) + z[2];
    z[12]=z[12] + n<T>(1,2)*z[16] - z[15];
    z[12]=z[7]*z[12];
    z[16]=z[4] - 3;
    z[17]=z[16]*z[11];
    z[18]=n<T>(5,2) - z[2];
    z[18]=z[2]*z[18];
    z[18]= - z[17] - n<T>(5,2) + z[18];
    z[12]=n<T>(1,2)*z[18] + z[12];
    z[18]=n<T>(1,2)*z[5];
    z[12]=z[12]*z[18];
    z[19]=n<T>(3,2)*z[4];
    z[16]=z[16]*z[19];
    z[20]=z[2]*z[6];
    z[21]= - n<T>(9,2) - z[20];
    z[21]=z[2]*z[21];
    z[16]=z[16] + n<T>(13,2) + z[21];
    z[10]=z[7]*z[10];
    z[10]=n<T>(1,8)*z[10] + z[14];
    z[10]=z[4]*z[10];
    z[10]=n<T>(3,4) + z[10];
    z[10]=z[7]*z[10];
    z[10]=z[12] + n<T>(1,4)*z[16] + z[10];
    z[10]=z[5]*z[10];
    z[12]=n<T>(1,2)*z[2];
    z[16]=static_cast<T>(3)+ z[20];
    z[16]=z[16]*z[12];
    z[21]=n<T>(1,2)*z[1];
    z[16]=z[16] - static_cast<T>(3)+ z[21];
    z[22]=n<T>(1,4)*z[4];
    z[21]=z[21] - 1;
    z[23]=z[21]*z[22];
    z[23]=z[23] + static_cast<T>(1)- n<T>(3,8)*z[1];
    z[23]=z[4]*z[23];
    z[15]= - n<T>(1,2) - z[15];
    z[15]=z[15]*z[13];
    z[10]=z[10] + z[15] + n<T>(1,2)*z[16] + z[23];
    z[15]=npow(z[2],2);
    z[16]=static_cast<T>(1)+ n<T>(1,2)*z[20];
    z[16]=z[16]*z[15];
    z[23]=z[11] - 1;
    z[16]=z[16] + z[23];
    z[24]=z[19] - 1;
    z[25]=z[7]*z[11];
    z[25]=z[25] + z[24];
    z[25]=z[25]*z[13];
    z[25]=z[25] + z[14];
    z[25]=z[7]*z[25];
    z[16]=n<T>(1,2)*z[16] + z[25];
    z[16]=z[16]*z[18];
    z[18]= - static_cast<T>(1)- z[20];
    z[18]=z[2]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[2]*z[18];
    z[18]= - z[19] + static_cast<T>(3)+ z[18];
    z[19]=n<T>(1,4)*z[7];
    z[20]= - z[24]*z[19];
    z[14]=z[14] + n<T>(1,4)*z[2];
    z[20]=z[20] - z[14];
    z[20]=z[7]*z[20];
    z[16]=z[16] + n<T>(1,4)*z[18] + z[20];
    z[16]=z[5]*z[16];
    z[18]= - z[21]*z[23];
    z[15]=z[6]*z[15];
    z[15]=n<T>(1,2)*z[15] - z[21];
    z[12]=z[15]*z[12];
    z[14]=z[7]*z[14];
    z[12]=z[14] + z[12] + z[18];
    z[12]=n<T>(1,2)*z[12] + z[16];
    z[12]=z[3]*z[12];
    z[10]=n<T>(1,2)*z[10] + z[12];
    z[10]=z[3]*z[10];
    z[12]=npow(z[4],2);
    z[14]=z[12]*z[13];
    z[15]=z[14] - static_cast<T>(1)- z[17];
    z[12]=z[12]*z[19];
    z[16]=z[23]*z[4];
    z[12]=z[16] + z[12];
    z[12]=z[7]*z[12];
    z[17]= - static_cast<T>(1)+ z[22];
    z[17]=z[4]*z[17];
    z[12]=z[12] + n<T>(3,4) + z[17];
    z[12]=z[5]*z[12];
    z[12]=z[12] - z[16] - z[14];
    z[14]=z[5]*z[7];
    z[12]=z[12]*z[14];
    z[12]=n<T>(1,2)*z[15] + z[12];
    z[10]=n<T>(1,4)*z[12] + z[10];
    z[10]=z[3]*z[10];
    z[12]=static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[11]=z[12] - z[11];
    z[15]=z[4]*z[8];
    z[11]=z[11]*z[15];
    z[12]=z[12]*z[8]*z[9];
    z[11]=z[11] - z[12];
    z[12]=npow(z[8],2);
    z[16]=z[15] - z[12];
    z[16]=z[16]*z[4];
    z[12]=z[12]*z[9];
    z[12]=z[16] + z[12];
    z[16]=z[12]*z[19];
    z[17]= - z[16] + z[11];
    z[17]=z[7]*z[17];
    z[18]=static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[18]=z[18]*z[8];
    z[15]=z[18] - n<T>(1,4)*z[15];
    z[15]=z[4]*z[15];
    z[18]= - n<T>(3,4) - z[18];
    z[18]=z[9]*z[18];
    z[15]=z[17] + z[15] + z[18];
    z[15]=z[5]*z[15];
    z[12]=z[12]*z[13];
    z[11]=z[15] + z[12] - z[11];
    z[11]=z[11]*z[14];
    z[11]= - z[16] + z[11];

    r += z[10] + n<T>(1,4)*z[11];
 
    return r;
}

template double qqb_2lha_r592(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r592(const std::array<dd_real,30>&);
#endif
