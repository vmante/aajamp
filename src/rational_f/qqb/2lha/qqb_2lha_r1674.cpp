#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1674(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=3*z[4];
    z[7]=static_cast<T>(1)+ 2*z[4];
    z[7]=z[7]*z[6];
    z[8]=2*z[2];
    z[9]=static_cast<T>(1)- z[6];
    z[9]=z[9]*z[8];
    z[7]=z[7] + z[9];
    z[7]=z[2]*z[7];
    z[9]=npow(z[2],2);
    z[10]=z[9]*z[1];
    z[11]= - static_cast<T>(1)+ z[2];
    z[12]=npow(z[4],2);
    z[11]=z[10]*z[12]*z[11];
    z[13]= - z[8] + static_cast<T>(1)+ 4*z[4];
    z[13]=z[2]*z[4]*z[13];
    z[13]= - 4*z[12] + z[13];
    z[13]=z[2]*z[13];
    z[11]=z[13] + z[11];
    z[11]=z[1]*z[11];
    z[7]=z[11] - 6*z[12] + z[7];
    z[7]=z[1]*z[7];
    z[11]=static_cast<T>(2)- z[6];
    z[11]=z[11]*z[8];
    z[13]=z[4]*z[5];
    z[14]=static_cast<T>(7)+ 4*z[13];
    z[14]=z[4]*z[14];
    z[7]=z[7] + z[14] + z[11];
    z[7]=z[1]*z[7];
    z[11]=z[5] - 1;
    z[13]= - z[11]*z[13];
    z[13]= - 3*z[5] + z[13];
    z[13]=z[4]*z[13];
    z[7]=z[13] + z[7];
    z[7]=z[3]*z[7];
    z[6]= - z[6] + z[8];
    z[6]=z[6]*z[8];
    z[6]=3*z[12] + z[6];
    z[6]=z[2]*z[6];
    z[13]= - z[4]*z[8];
    z[13]=z[12] + z[13];
    z[10]=z[13]*z[10];
    z[6]=z[6] + z[10];
    z[6]=z[1]*z[6];
    z[10]= - z[4] + z[2];
    z[10]=z[10]*z[8];
    z[10]=z[12] + z[10];
    z[6]=3*z[10] + z[6];
    z[6]=z[1]*z[6];
    z[10]= - z[4]*z[11];
    z[10]= - static_cast<T>(3)+ z[10];
    z[10]=z[4]*z[10];
    z[6]=z[7] + z[6] + z[10] + z[8];
    z[6]=z[3]*z[6];
    z[7]=z[2]*z[11];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=z[7]*z[8];
    z[8]=z[11]*z[8];
    z[8]=static_cast<T>(3)+ z[8];
    z[8]=z[8]*z[9];
    z[9]=z[1]*npow(z[2],3);
    z[8]=z[8] + z[9];
    z[8]=z[1]*z[8];
    z[6]=z[6] + z[7] + z[8];

    r += 2*z[6];
 
    return r;
}

template double qqb_2lha_r1674(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1674(const std::array<dd_real,30>&);
#endif
