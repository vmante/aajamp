#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r181(const std::array<T,30>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[12];
    z[4]=k[6];
    z[5]=k[8];
    z[6]=k[13];
    z[7]=k[17];
    z[8]=k[2];
    z[9]=k[3];
    z[10]=k[24];
    z[11]=k[11];
    z[12]=k[4];
    z[13]=k[5];
    z[14]=k[10];
    z[15]=k[14];
    z[16]=n<T>(1,3)*z[5];
    z[17]=static_cast<T>(37)+ z[5];
    z[17]=z[17]*z[16];
    z[18]=npow(z[5],2);
    z[19]=z[18]*z[1];
    z[20]=n<T>(7,3) - n<T>(9,2)*z[5];
    z[20]=z[5]*z[20];
    z[20]=z[20] - n<T>(11,18)*z[19];
    z[20]=z[1]*z[20];
    z[17]=z[17] + z[20];
    z[20]=n<T>(1,2)*z[1];
    z[17]=z[17]*z[20];
    z[21]=n<T>(1,2)*z[5];
    z[22]= - static_cast<T>(1)+ z[21];
    z[22]=z[22]*z[16];
    z[23]=9*z[18] + n<T>(17,6)*z[19];
    z[23]=z[23]*z[20];
    z[23]=n<T>(7,3)*z[18] + z[23];
    z[23]=z[9]*z[23];
    z[24]=z[2]*z[1];
    z[25]=static_cast<T>(1)+ z[1];
    z[25]=z[25]*z[24];
    z[17]=n<T>(5,3)*z[25] + z[23] + z[22] + z[17];
    z[22]=2*z[5];
    z[23]=z[1]*z[5];
    z[23]=z[22] + z[23];
    z[23]=z[1]*z[23];
    z[23]=z[5] + z[23];
    z[25]=z[18] + z[19];
    z[25]=z[9]*z[25];
    z[23]=2*z[23] + z[25];
    z[23]=z[1]*z[23];
    z[25]=n<T>(1,3)*z[1];
    z[26]=static_cast<T>(1)+ z[25];
    z[26]=z[1]*z[26];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[1]*z[26];
    z[26]=n<T>(1,3) + z[26];
    z[26]=z[26]*z[24];
    z[23]=n<T>(1,3)*z[23] + z[26];
    z[23]=z[3]*z[23];
    z[26]= - static_cast<T>(1)- z[20];
    z[26]=z[1]*z[26];
    z[26]= - n<T>(1,2) + z[26];
    z[26]=z[26]*z[24];
    z[23]=z[26] + z[23];
    z[26]=5*z[5];
    z[27]= - n<T>(11,8) + z[26];
    z[27]=z[1]*z[27]*z[16];
    z[28]= - static_cast<T>(8)+ n<T>(5,3)*z[5];
    z[28]=z[5]*z[28];
    z[27]=z[28] + z[27];
    z[27]=z[1]*z[27];
    z[27]= - n<T>(175,24)*z[5] + z[27];
    z[27]=z[1]*z[27];
    z[28]= - n<T>(121,3)*z[18] - 17*z[19];
    z[28]=z[28]*z[20];
    z[28]=z[18] + z[28];
    z[28]=z[9]*z[28];
    z[23]=n<T>(1,4)*z[28] + z[21] + z[27] + 5*z[23];
    z[23]=z[3]*z[23];
    z[17]=n<T>(1,2)*z[17] + n<T>(1,3)*z[23];
    z[17]=z[3]*z[17];
    z[23]=n<T>(1,3) - z[21];
    z[23]=z[23]*z[21];
    z[27]=z[5] + 2;
    z[28]=z[27]*z[22];
    z[28]=n<T>(23,2) + z[28];
    z[28]=z[4]*z[28];
    z[29]=n<T>(1,4)*z[5];
    z[28]=z[28] - n<T>(11,3) - z[29];
    z[28]=z[4]*z[28];
    z[30]=n<T>(1,2)*z[18];
    z[31]=z[21] - n<T>(47,3)*z[4];
    z[31]=z[4]*z[31];
    z[31]=z[30] + z[31];
    z[31]=z[9]*z[31];
    z[23]=n<T>(1,6)*z[31] + z[23] + z[28];
    z[23]=z[4]*z[23];
    z[28]= - static_cast<T>(1)- z[21];
    z[28]=z[28]*z[21];
    z[31]=22*z[4];
    z[32]=z[31] + n<T>(28,3) - z[5];
    z[32]=z[4]*z[32];
    z[28]=z[28] + z[32];
    z[32]=n<T>(1,3)*z[4];
    z[28]=z[28]*z[32];
    z[28]= - n<T>(1,4)*z[18] + z[28];
    z[33]=z[1]*z[4];
    z[28]=z[28]*z[33];
    z[34]=2*z[18];
    z[23]=z[28] + z[34] + z[23];
    z[23]=z[9]*z[23];
    z[28]=n<T>(11,2)*z[5];
    z[35]= - static_cast<T>(1)+ z[28];
    z[35]=z[35]*z[16];
    z[36]=n<T>(22,3)*z[4];
    z[37]=n<T>(58,9) + z[5];
    z[37]=z[5]*z[37];
    z[37]= - z[36] + n<T>(10,3) + z[37];
    z[38]=2*z[4];
    z[37]=z[37]*z[38];
    z[39]=static_cast<T>(4)+ n<T>(37,3)*z[5];
    z[39]=z[5]*z[39];
    z[39]=n<T>(25,2) + z[39];
    z[37]=n<T>(1,3)*z[39] + z[37];
    z[37]=z[4]*z[37];
    z[35]=z[35] + z[37];
    z[35]=z[4]*z[35];
    z[37]=z[4]*z[5];
    z[39]=22*z[37];
    z[40]=static_cast<T>(4)+ n<T>(41,6)*z[5];
    z[40]=z[5]*z[40];
    z[40]=z[40] + z[39];
    z[40]=z[4]*z[40];
    z[41]=z[22] + 1;
    z[42]=z[5]*z[41];
    z[40]=z[42] + z[40];
    z[40]=z[40]*z[32];
    z[40]=z[30] + z[40];
    z[40]=z[40]*z[33];
    z[35]=z[40] - n<T>(59,24)*z[18] + z[35];
    z[35]=z[1]*z[35];
    z[40]=n<T>(181,4) + 20*z[5];
    z[40]=z[5]*z[40];
    z[40]=n<T>(89,2) + z[40];
    z[42]= - n<T>(91,6) - z[22];
    z[42]=z[4]*z[42];
    z[40]=n<T>(1,3)*z[40] + z[42];
    z[40]=z[4]*z[40];
    z[42]= - n<T>(1,3) - z[29];
    z[42]=z[5]*z[42];
    z[40]=z[40] - n<T>(17,12) + z[42];
    z[40]=z[4]*z[40];
    z[23]=z[23] + z[35] + n<T>(4,3)*z[5] + z[40];
    z[23]=z[9]*z[23];
    z[35]= - n<T>(5,2) - z[5];
    z[35]=z[35]*z[16];
    z[35]=z[35] + z[37];
    z[35]=z[4]*z[35];
    z[40]=z[18]*z[4];
    z[42]=z[40] - z[18];
    z[43]=z[32]*z[1];
    z[44]= - z[42]*z[43];
    z[35]=z[44] + n<T>(2,9)*z[18] + z[35];
    z[35]=z[1]*z[35];
    z[44]=z[5] + 1;
    z[45]=n<T>(2,3)*z[44] - z[4];
    z[45]=z[4]*z[45];
    z[35]=z[35] - n<T>(7,18)*z[5] + z[45];
    z[35]=z[1]*z[35];
    z[45]=z[42]*z[25];
    z[46]= - z[27]*z[16];
    z[45]= - z[45] + z[46] + z[37];
    z[45]=z[1]*z[45];
    z[45]=z[45] + n<T>(1,3)*z[41] - z[4];
    z[45]=z[1]*z[45];
    z[45]= - n<T>(1,3) + z[45];
    z[46]=n<T>(1,3)*z[2];
    z[45]=z[45]*z[46];
    z[47]=z[4] - n<T>(1,2);
    z[35]=z[45] - n<T>(1,3)*z[47] + z[35];
    z[35]=z[2]*z[35];
    z[45]=npow(z[4],2);
    z[48]=z[42]*z[45];
    z[49]=z[48]*z[1];
    z[49]=n<T>(2,3)*z[49];
    z[50]=z[44]*z[16];
    z[50]=z[50] - z[37];
    z[50]=z[50]*z[38];
    z[50]= - z[18] + z[50];
    z[50]=z[4]*z[50];
    z[50]=z[50] + z[49];
    z[50]=z[1]*z[50];
    z[51]=static_cast<T>(1)- z[22];
    z[51]=n<T>(1,3)*z[51] + z[4];
    z[51]=z[51]*z[38];
    z[52]= - n<T>(1,2) + z[5];
    z[51]=n<T>(1,3)*z[52] + z[51];
    z[51]=z[4]*z[51];
    z[52]=static_cast<T>(1)- z[18];
    z[50]=z[50] + n<T>(1,3)*z[52] + z[51];
    z[50]=z[1]*z[50];
    z[51]=z[4] + 1;
    z[38]=z[51]*z[38];
    z[38]=z[38] - n<T>(1,2);
    z[35]=z[35] + n<T>(1,3)*z[38] + z[50];
    z[35]=z[2]*z[35];
    z[50]=z[27]*z[5];
    z[50]=z[50] + 1;
    z[51]=z[50]*z[9];
    z[52]=npow(z[4],3);
    z[53]=z[52]*z[51];
    z[54]=static_cast<T>(5)+ z[22];
    z[54]=z[5]*z[54];
    z[54]=static_cast<T>(3)+ z[54];
    z[54]=z[54]*z[45];
    z[54]=z[54] + z[53];
    z[55]=3*z[9];
    z[54]=z[54]*z[55];
    z[56]=static_cast<T>(11)+ z[22];
    z[56]=z[5]*z[56];
    z[56]=static_cast<T>(9)+ z[56];
    z[56]=z[4]*z[56];
    z[54]=z[54] + z[18] + z[56];
    z[54]=z[9]*z[54];
    z[50]=z[50]*z[45]*z[55];
    z[26]=static_cast<T>(13)+ z[26];
    z[26]=z[5]*z[26];
    z[26]=static_cast<T>(8)+ z[26];
    z[26]=z[4]*z[26];
    z[26]=z[26] + z[50];
    z[26]=z[9]*z[26];
    z[50]=z[44]*z[5];
    z[26]= - 13*z[50] + z[26];
    z[26]=z[9]*z[26];
    z[56]=z[22]*z[44];
    z[51]=z[4]*z[51];
    z[51]= - z[56] + z[51];
    z[57]=3*z[10];
    z[51]=z[51]*npow(z[9],2)*z[57];
    z[26]=z[26] + z[51];
    z[26]=z[10]*z[26];
    z[26]=z[26] - 12*z[50] + z[54];
    z[51]=2*z[10];
    z[26]=z[26]*z[51];
    z[54]=z[18]*z[9];
    z[58]=z[50] + z[54];
    z[55]=z[10]*z[58]*z[55];
    z[55]=z[55] + 5*z[50] + 8*z[54];
    z[55]=z[10]*z[55];
    z[59]=z[50]*z[2];
    z[60]=z[34] + z[59];
    z[57]= - z[54]*z[57];
    z[57]= - 5*z[18] + z[57];
    z[57]=z[10]*z[57];
    z[61]=z[2]*z[18];
    z[57]= - 4*z[61] + z[57];
    z[57]=z[8]*z[57];
    z[55]=z[57] + 4*z[60] + z[55];
    z[51]=z[51]*z[55];
    z[55]= - z[5] + z[19];
    z[24]=z[55]*z[24];
    z[19]= - z[19] + z[24];
    z[19]=z[19]*npow(z[2],2);
    z[19]=n<T>(1,9)*z[19] + z[51];
    z[19]=z[8]*z[19];
    z[24]= - z[41]*z[37];
    z[24]=n<T>(5,3)*z[18] + z[24];
    z[24]=z[4]*z[24];
    z[24]=z[24] - z[49];
    z[24]=z[1]*z[24];
    z[41]=z[56]*z[4];
    z[41]=z[41] - z[18];
    z[24]=z[24] - z[41];
    z[24]=z[1]*z[24];
    z[33]= - z[42]*z[33];
    z[33]=z[33] - z[41];
    z[33]=z[1]*z[33];
    z[33]= - z[50] + z[33];
    z[33]=z[7]*z[33]*z[25];
    z[24]=z[33] - n<T>(2,3)*z[50] + z[24];
    z[24]=z[7]*z[24];
    z[33]=n<T>(1,3)*z[45];
    z[41]= - static_cast<T>(23)+ z[31];
    z[41]=z[4]*z[41];
    z[41]=n<T>(17,4) + z[41];
    z[41]=z[41]*z[33];
    z[45]=n<T>(7,2) - z[36];
    z[45]=z[45]*z[52];
    z[49]=z[9]*npow(z[4],4);
    z[45]=z[45] + n<T>(22,9)*z[49];
    z[45]=z[9]*z[45];
    z[41]=z[41] + z[45];
    z[41]=z[9]*z[41];
    z[38]= - z[4]*z[38];
    z[45]=z[4]*z[47];
    z[47]=z[2]*z[32];
    z[45]=z[45] + z[47];
    z[45]=z[2]*z[45];
    z[38]=z[38] + z[45];
    z[38]=z[38]*z[46];
    z[36]=n<T>(29,2) - z[36];
    z[36]=z[4]*z[36];
    z[36]= - n<T>(13,4) + z[36];
    z[36]=z[4]*z[36];
    z[36]=static_cast<T>(1)+ z[36];
    z[36]=z[36]*z[32];
    z[36]=z[38] + z[36] + z[41];
    z[36]=z[12]*z[36];
    z[38]=n<T>(5,2) + z[22];
    z[38]=z[38]*z[37];
    z[30]= - z[30] + n<T>(1,3)*z[38];
    z[30]=z[4]*z[30];
    z[25]=z[25]*z[48];
    z[25]=z[30] + z[25];
    z[25]=z[1]*z[25];
    z[27]=z[27]*z[37];
    z[27]= - z[18] + z[27];
    z[25]=n<T>(1,3)*z[27] + z[25];
    z[25]=z[1]*z[25];
    z[27]= - n<T>(1,2) - z[5];
    z[27]=z[27]*z[37];
    z[30]= - z[4]*z[42]*z[20];
    z[27]=z[30] + z[18] + z[27];
    z[27]=z[1]*z[27];
    z[21]= - z[21] + z[27];
    z[21]=z[1]*z[21];
    z[20]= - z[54]*z[20];
    z[20]=z[21] + z[20];
    z[20]=z[6]*z[20];
    z[20]=z[25] + n<T>(1,3)*z[20];
    z[20]=z[6]*z[20];
    z[21]= - z[8]*z[18];
    z[21]=z[21] + z[50] - z[54];
    z[21]=z[8]*z[21];
    z[25]=z[9]*z[5];
    z[21]=z[25] + z[21];
    z[21]=z[11]*z[21];
    z[25]=n<T>(5,2)*z[5];
    z[27]= - n<T>(61,3) - z[25];
    z[27]=z[5]*z[27];
    z[27]=z[27] + n<T>(23,6)*z[54];
    z[30]=z[8]*z[2];
    z[30]=8*z[30] + n<T>(131,12);
    z[30]=z[18]*z[30];
    z[30]= - 8*z[59] + z[30];
    z[30]=z[8]*z[30];
    z[37]=z[54]*z[3];
    z[21]=z[21] + z[30] + n<T>(1,2)*z[27] - n<T>(5,3)*z[37];
    z[21]=z[11]*z[21];
    z[27]=n<T>(1,2)*z[3];
    z[30]=z[27] - 1;
    z[38]=z[54] + z[5];
    z[27]=z[27]*z[38]*z[30];
    z[30]= - z[9]*z[38];
    z[30]= - z[34] + z[30];
    z[30]=z[9]*z[30];
    z[22]= - z[22] + z[30];
    z[22]=2*z[22] + z[27];
    z[22]=z[13]*z[22];
    z[22]=z[22] - static_cast<T>(1)- n<T>(25,2)*z[18];
    z[27]=n<T>(1,6)*z[5];
    z[28]= - static_cast<T>(1)- z[28];
    z[28]=z[28]*z[27];
    z[30]=113*z[5];
    z[34]=static_cast<T>(163)+ z[30];
    z[27]=z[34]*z[27];
    z[27]=z[27] - z[39];
    z[27]=z[27]*z[32];
    z[34]=static_cast<T>(1)- n<T>(34,9)*z[5];
    z[34]=z[5]*z[34];
    z[27]=z[34] + z[27];
    z[27]=z[4]*z[27];
    z[27]=z[28] + z[27];
    z[27]=z[4]*z[27];
    z[28]= - 8*z[18] + 11*z[40];
    z[28]=z[4]*z[28];
    z[28]= - z[18] + n<T>(2,3)*z[28];
    z[28]=z[4]*z[28];
    z[28]=n<T>(2,3)*z[18] + z[28];
    z[28]=z[28]*z[43];
    z[18]=z[28] + n<T>(9,8)*z[18] + z[27];
    z[18]=z[1]*z[18];
    z[27]= - static_cast<T>(106)- z[30];
    z[27]=n<T>(1,3)*z[27] + z[31];
    z[27]=z[27]*z[32];
    z[25]=n<T>(61,9) + z[25];
    z[25]=z[5]*z[25];
    z[25]=n<T>(1,6) + z[25];
    z[25]=n<T>(5,2)*z[25] + z[27];
    z[25]=z[4]*z[25];
    z[27]=static_cast<T>(5)- 7*z[5];
    z[27]=z[5]*z[27];
    z[27]=n<T>(9,2) + z[27];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[25]=z[4]*z[25];
    z[18]=z[18] - n<T>(9,8)*z[5] + z[25];
    z[18]=z[1]*z[18];
    z[25]= - static_cast<T>(11)- 4*z[5];
    z[25]=z[5]*z[25];
    z[25]= - static_cast<T>(7)+ z[25];
    z[25]=z[25]*z[33];
    z[25]=z[25] - z[53];
    z[25]=z[9]*z[25];
    z[16]= - static_cast<T>(2)- z[16];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(5,3) + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[16] + z[25];
    z[16]=z[9]*z[16];
    z[16]= - n<T>(1,3)*z[44] + z[16];
    z[16]=z[14]*z[16];
    z[25]=static_cast<T>(181)+ 41*z[5];
    z[25]=z[25]*z[29];
    z[25]=static_cast<T>(22)+ z[25];
    z[27]=n<T>(113,9)*z[4] - n<T>(71,3) - n<T>(25,2)*z[5];
    z[27]=z[4]*z[27];
    z[25]=n<T>(1,3)*z[25] + n<T>(1,2)*z[27];
    z[25]=z[4]*z[25];
    z[27]=z[37] - z[58];
    z[27]=z[15]*z[27];

    r += 4*z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + n<T>(1,3)*z[22]
       + z[23] + z[24] + z[25] + z[26] + n<T>(5,12)*z[27] + z[35] + z[36];
 
    return r;
}

template double qqb_2lha_r181(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r181(const std::array<dd_real,30>&);
#endif
