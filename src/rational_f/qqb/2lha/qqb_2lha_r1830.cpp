#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1830(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[27];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=k[12];
    z[9]=k[7];
    z[10]=n<T>(1,2)*z[6];
    z[11]=z[10] + 1;
    z[11]=z[11]*z[3];
    z[12]=n<T>(1,2) + z[6];
    z[12]=n<T>(1,2)*z[12] - z[11];
    z[12]=z[4]*z[12];
    z[13]=static_cast<T>(3)+ z[6];
    z[11]=z[12] + n<T>(1,4)*z[13] + z[11];
    z[11]=z[3]*z[11];
    z[12]=npow(z[3],2);
    z[13]=z[12]*z[2];
    z[14]=n<T>(1,4)*z[6];
    z[15]=static_cast<T>(1)- z[14];
    z[15]=z[3]*z[15];
    z[15]= - n<T>(1,8) + z[15];
    z[15]=z[3]*z[15];
    z[16]=z[2]*z[3];
    z[17]= - static_cast<T>(1)- z[16];
    z[17]=z[5]*z[17];
    z[15]=n<T>(1,8)*z[17] + z[15] - n<T>(1,4)*z[13];
    z[15]=z[2]*z[15];
    z[17]=static_cast<T>(1)+ z[3];
    z[17]=z[3]*z[17];
    z[18]= - z[4]*z[12];
    z[13]=z[18] + z[17] + z[13];
    z[13]=z[1]*z[13];
    z[11]=n<T>(1,2)*z[13] + n<T>(1,8) + z[15] + z[11];
    z[11]=z[1]*z[11];
    z[13]=n<T>(1,2)*z[7];
    z[15]=static_cast<T>(3)- z[7];
    z[15]=z[15]*z[13];
    z[17]=npow(z[7],2);
    z[18]=z[17]*z[10];
    z[19]=n<T>(3,2) - z[7];
    z[19]=z[7]*z[19];
    z[19]=z[19] - z[18];
    z[19]=z[6]*z[19];
    z[15]=z[15] + z[19];
    z[19]=n<T>(1,2)*z[2];
    z[15]=z[15]*z[19];
    z[20]=n<T>(1,4) - z[7];
    z[20]=z[7]*z[20];
    z[20]=z[20] - z[18];
    z[20]=z[6]*z[20];
    z[21]=n<T>(1,2) - z[7];
    z[21]=z[7]*z[21];
    z[21]=n<T>(3,2) + z[21];
    z[20]=n<T>(1,2)*z[21] + z[20];
    z[20]=z[6]*z[20];
    z[21]=z[7] + 1;
    z[22]=z[21]*z[7];
    z[22]=z[22] + z[18];
    z[22]=z[22]*z[6];
    z[23]=z[13] + 1;
    z[24]=z[23]*z[7];
    z[22]=z[22] + z[24] + n<T>(1,2);
    z[24]=npow(z[6],2);
    z[25]=n<T>(1,2)*z[24];
    z[26]= - z[9]*z[22]*z[25];
    z[15]=z[26] + z[20] + z[15];
    z[15]=z[8]*z[15];
    z[17]=z[17]*z[6];
    z[20]= - n<T>(1,4)*z[7] + z[17];
    z[20]=z[6]*z[20];
    z[15]=z[15] + n<T>(5,4) + z[20];
    z[20]=3*z[7];
    z[23]= - z[23]*z[20];
    z[26]=z[21]*z[13];
    z[27]= - z[6]*z[26];
    z[23]=z[27] - n<T>(1,2) + z[23];
    z[23]=z[6]*z[23];
    z[27]=z[7] + 3;
    z[27]=z[27]*z[7];
    z[28]=static_cast<T>(2)+ n<T>(3,2)*z[27];
    z[23]=z[23] - z[28];
    z[23]=z[6]*z[23];
    z[29]=z[13] + 2;
    z[29]=z[29]*z[7];
    z[29]=z[29] + n<T>(3,2);
    z[23]=z[23] - z[29];
    z[23]=z[3]*z[23];
    z[30]=static_cast<T>(17)+ 13*z[7];
    z[30]=z[30]*z[13];
    z[30]=static_cast<T>(1)+ z[30];
    z[31]=z[7] + n<T>(1,2);
    z[32]=z[31]*z[6];
    z[33]=z[7]*z[32];
    z[30]=n<T>(1,2)*z[30] + z[33];
    z[30]=z[6]*z[30];
    z[34]=7*z[7];
    z[35]=static_cast<T>(15)+ z[34];
    z[35]=z[7]*z[35];
    z[35]=n<T>(9,2) + z[35];
    z[30]=n<T>(1,2)*z[35] + z[30];
    z[30]=z[6]*z[30];
    z[35]=static_cast<T>(3)+ n<T>(5,2)*z[27];
    z[23]=z[23] + n<T>(1,2)*z[35] + z[30];
    z[23]=z[3]*z[23];
    z[30]=z[31]*z[7];
    z[18]= - z[30] - z[18];
    z[18]=z[6]*z[18];
    z[18]= - z[26] + z[18];
    z[18]=z[18]*z[19];
    z[31]= - static_cast<T>(5)- z[34];
    z[13]=z[31]*z[13];
    z[13]=z[13] - z[17];
    z[13]=z[13]*z[10];
    z[31]= - static_cast<T>(3)- 2*z[7];
    z[31]=z[7]*z[31];
    z[13]=z[13] - n<T>(1,4) + z[31];
    z[13]=z[6]*z[13];
    z[31]=z[20] + 7;
    z[34]=z[31]*z[7];
    z[35]= - static_cast<T>(1)- z[34];
    z[13]=z[18] + z[23] + n<T>(1,4)*z[35] + z[13];
    z[13]=z[4]*z[13];
    z[18]= - z[20]*z[32];
    z[23]= - static_cast<T>(13)- n<T>(17,2)*z[7];
    z[23]=z[7]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[18]=n<T>(1,4)*z[23] + z[18];
    z[18]=z[6]*z[18];
    z[23]=z[20] + 5;
    z[32]=n<T>(3,2)*z[7];
    z[32]=z[23]*z[32];
    z[32]=static_cast<T>(1)+ z[32];
    z[10]=z[32]*z[10];
    z[10]=z[10] + z[28];
    z[10]=z[6]*z[10];
    z[10]=z[10] + z[29];
    z[10]=z[3]*z[10];
    z[10]=z[10] - n<T>(1,4)*z[34] + z[18];
    z[10]=z[3]*z[10];
    z[18]= - n<T>(1,4) - z[7];
    z[18]=z[7]*z[18];
    z[28]= - n<T>(1,4) - z[30];
    z[28]=z[6]*z[28];
    z[18]=3*z[28] + static_cast<T>(1)+ z[18];
    z[20]=z[31]*z[20];
    z[20]= - z[6] + static_cast<T>(5)+ z[20];
    z[14]=z[20]*z[14];
    z[14]=z[14] + z[29];
    z[14]=z[3]*z[14];
    z[14]=n<T>(1,2)*z[18] + z[14];
    z[14]=z[3]*z[14];
    z[12]= - z[12]*z[19];
    z[18]=static_cast<T>(1)+ z[27];
    z[18]=n<T>(3,2)*z[18] - z[6];
    z[18]=z[3]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[3]*z[18];
    z[12]=z[18] + z[12];
    z[12]=z[12]*z[19];
    z[12]=z[12] - n<T>(1,8) + z[14];
    z[12]=z[2]*z[12];
    z[14]= - static_cast<T>(3)- z[27];
    z[14]=z[3]*z[14];
    z[14]=z[14] + z[16];
    z[14]=z[14]*z[19];
    z[16]=z[7]*z[23];
    z[16]=static_cast<T>(1)+ z[16];
    z[14]=z[14] + n<T>(1,2)*z[16] + z[33];
    z[14]=z[14]*z[19];
    z[16]=z[30] + n<T>(1,4)*z[17];
    z[16]=z[6]*z[16];
    z[18]=static_cast<T>(1)+ n<T>(3,4)*z[7];
    z[18]=z[7]*z[18];
    z[16]=z[16] + n<T>(1,4) + z[18];
    z[16]=z[6]*z[16];
    z[14]=z[16] + z[14];
    z[16]=n<T>(1,2)*z[5];
    z[14]=z[14]*z[16];
    z[16]= - n<T>(1,2)*z[21] - z[33];
    z[16]=z[16]*z[24];
    z[18]=z[3]*npow(z[6],3)*z[26];
    z[16]=z[16] + z[18];
    z[16]=z[3]*z[16];
    z[18]=z[5]*z[22];
    z[17]=z[18] + z[7] + z[17];
    z[17]=z[25]*z[17];
    z[16]=z[16] + z[17];
    z[17]=n<T>(1,2)*z[9];
    z[16]=z[16]*z[17];

    r += z[10] + z[11] + z[12] + z[13] + z[14] + n<T>(1,2)*z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r1830(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1830(const std::array<dd_real,30>&);
#endif
