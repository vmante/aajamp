#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2001(const std::array<T,30>& k) {
  T z[59];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[21];
    z[9]=k[18];
    z[10]=k[13];
    z[11]=npow(z[3],3);
    z[12]=static_cast<T>(89)- n<T>(169,3)*z[3];
    z[12]=z[12]*z[11];
    z[13]=z[11]*z[5];
    z[14]=n<T>(37,2) + n<T>(41,3)*z[3];
    z[14]=z[14]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[14]=npow(z[3],4);
    z[15]=z[14]*z[2];
    z[16]=z[14]*z[5];
    z[17]=z[15] - z[16];
    z[18]=11*z[3];
    z[19]= - n<T>(89,8) + z[18];
    z[19]=z[19]*z[11];
    z[17]=z[19] - n<T>(11,4)*z[17];
    z[17]=z[2]*z[17];
    z[12]=n<T>(1,4)*z[12] + n<T>(1,3)*z[17];
    z[12]=z[2]*z[12];
    z[17]=n<T>(1,3)*z[11];
    z[19]=59*z[3];
    z[20]= - n<T>(133,2) + z[19];
    z[20]=z[20]*z[17];
    z[21]= - n<T>(37,3) - n<T>(89,4)*z[3];
    z[21]=z[21]*z[13];
    z[20]=z[20] + z[21];
    z[12]=n<T>(1,2)*z[20] + z[12];
    z[12]=z[2]*z[12];
    z[20]=z[3] - n<T>(1,3);
    z[20]=z[20]*z[13];
    z[21]=37*z[3];
    z[22]= - static_cast<T>(1)- z[21];
    z[17]=z[22]*z[17];
    z[17]=z[17] - 37*z[20];
    z[22]=n<T>(1,4)*z[4];
    z[17]=z[17]*z[22];
    z[23]=n<T>(37,3)*z[3];
    z[24]=n<T>(1,4) + z[23];
    z[24]=z[24]*z[11];
    z[25]=n<T>(185,12)*z[16];
    z[17]=z[17] + z[24] + z[25];
    z[17]=z[4]*z[17];
    z[24]=61*z[3];
    z[26]=n<T>(43,2) - z[24];
    z[26]=z[26]*z[11];
    z[26]=z[26] + n<T>(89,4)*z[16];
    z[17]=n<T>(1,3)*z[26] + z[17];
    z[26]=n<T>(1,2)*z[4];
    z[27]= - static_cast<T>(1)+ z[26];
    z[27]=z[4]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[14]*z[27];
    z[15]= - n<T>(7,2)*z[14] + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[14] + n<T>(1,4)*z[15];
    z[15]=z[2]*z[15];
    z[15]=n<T>(1,4)*z[27] + z[15];
    z[15]=z[1]*z[15];
    z[12]=n<T>(37,6)*z[15] + n<T>(1,2)*z[17] + z[12];
    z[12]=z[1]*z[12];
    z[15]=npow(z[3],2);
    z[17]= - n<T>(53,3) + z[21];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(1,3) + z[17];
    z[17]=z[17]*z[15];
    z[27]=n<T>(1,2)*z[3];
    z[28]=z[27] - n<T>(1,3);
    z[29]=5*z[3];
    z[28]=z[28]*z[29];
    z[28]=z[28] + n<T>(1,6);
    z[30]=z[15]*z[5];
    z[31]=n<T>(37,2)*z[30];
    z[32]=z[28]*z[31];
    z[17]=z[17] + z[32];
    z[17]=z[5]*z[17];
    z[32]=n<T>(1,2)*z[15];
    z[33]=n<T>(37,2)*z[3];
    z[34]= - n<T>(35,3) + z[33];
    z[34]=z[3]*z[34];
    z[34]=n<T>(35,12) + z[34];
    z[34]=z[34]*z[32];
    z[17]=z[34] + z[17];
    z[17]=z[17]*z[26];
    z[34]=n<T>(259,6)*z[3];
    z[35]=static_cast<T>(11)- z[34];
    z[35]=z[3]*z[35];
    z[35]= - n<T>(1,6) + z[35];
    z[35]=z[35]*z[32];
    z[36]=n<T>(7,2) - z[23];
    z[36]=z[36]*z[29];
    z[36]= - static_cast<T>(3)+ z[36];
    z[36]=z[36]*z[15];
    z[36]=z[36] - n<T>(185,6)*z[16];
    z[36]=z[5]*z[36];
    z[17]=z[17] + z[35] + z[36];
    z[17]=z[17]*z[22];
    z[35]= - n<T>(55,3) - n<T>(27,4)*z[3];
    z[35]=z[3]*z[35];
    z[35]=n<T>(15,4) + z[35];
    z[35]=z[35]*z[32];
    z[36]=n<T>(11,3)*z[3];
    z[37]= - n<T>(13,2) - z[36];
    z[37]=z[37]*z[13];
    z[38]=n<T>(1,6)*z[3];
    z[39]=z[38] + 1;
    z[40]=z[2]*z[39]*z[11];
    z[35]=n<T>(11,4)*z[40] + z[35] + z[37];
    z[35]=z[2]*z[35];
    z[37]=n<T>(239,2) + 169*z[3];
    z[37]=z[37]*z[38];
    z[37]= - static_cast<T>(13)+ z[37];
    z[37]=z[37]*z[15];
    z[40]=static_cast<T>(1)+ n<T>(7,6)*z[3];
    z[13]=z[40]*z[13];
    z[13]=z[37] + n<T>(11,2)*z[13];
    z[13]=z[5]*z[13];
    z[37]=n<T>(1,4)*z[15];
    z[40]=static_cast<T>(101)+ n<T>(539,6)*z[3];
    z[40]=z[3]*z[40];
    z[40]= - n<T>(75,2) + z[40];
    z[40]=z[40]*z[37];
    z[13]=z[40] + z[13];
    z[13]=n<T>(1,2)*z[13] + z[35];
    z[13]=z[2]*z[13];
    z[19]= - static_cast<T>(1)- z[19];
    z[19]=z[3]*z[19];
    z[19]=n<T>(115,6) + z[19];
    z[19]=z[19]*z[15];
    z[35]=n<T>(1,3)*z[3];
    z[40]=31*z[3];
    z[41]=static_cast<T>(119)+ z[40];
    z[41]=z[41]*z[35];
    z[41]=static_cast<T>(37)+ z[41];
    z[42]=n<T>(1,4)*z[30];
    z[41]=z[41]*z[42];
    z[19]=z[19] + z[41];
    z[41]=n<T>(1,2)*z[5];
    z[19]=z[19]*z[41];
    z[43]= - n<T>(107,4) - z[24];
    z[43]=z[43]*z[35];
    z[43]=n<T>(71,16) + z[43];
    z[43]=z[43]*z[15];
    z[13]=z[13] + z[43] + z[19];
    z[19]=n<T>(1,2)*z[2];
    z[13]=z[13]*z[19];
    z[24]= - n<T>(175,16) + z[24];
    z[24]=z[24]*z[35];
    z[24]= - n<T>(3,4) + z[24];
    z[24]=z[24]*z[15];
    z[43]= - static_cast<T>(37)- 31*z[15];
    z[43]=z[43]*z[30];
    z[24]=z[24] + n<T>(1,96)*z[43];
    z[24]=z[5]*z[24];
    z[43]=n<T>(17,6) + 41*z[3];
    z[43]=z[3]*z[43];
    z[43]= - n<T>(187,48) + z[43];
    z[37]=z[43]*z[37];
    z[12]=n<T>(1,2)*z[12] + z[13] + z[17] + z[37] + z[24];
    z[12]=z[1]*z[12];
    z[13]= - static_cast<T>(31)+ z[34];
    z[13]=z[13]*z[29];
    z[13]=n<T>(139,6) + z[13];
    z[13]=z[3]*z[13];
    z[13]=n<T>(37,12) + z[13];
    z[17]=n<T>(1,4)*z[3];
    z[13]=z[13]*z[17];
    z[24]= - n<T>(29,4) + z[23];
    z[24]=z[24]*z[29];
    z[24]=n<T>(91,12) + z[24];
    z[24]=z[24]*z[15];
    z[24]=z[24] + z[25];
    z[24]=z[5]*z[24];
    z[13]=z[13] + z[24];
    z[13]=z[5]*z[13];
    z[24]=z[3] - 1;
    z[25]=z[24]*z[29];
    z[25]=z[25] + 1;
    z[25]=z[25]*z[30];
    z[37]=n<T>(109,3) - z[21];
    z[37]=z[37]*z[29];
    z[37]= - n<T>(103,3) + z[37];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(1,3) + z[37];
    z[37]=z[37]*z[27];
    z[37]=z[37] - n<T>(37,3)*z[25];
    z[37]=z[37]*z[41];
    z[43]=n<T>(397,3) - 111*z[3];
    z[43]=z[43]*z[17];
    z[43]= - n<T>(35,3) + z[43];
    z[43]=z[3]*z[43];
    z[43]=n<T>(35,24) + z[43];
    z[43]=z[3]*z[43];
    z[37]=z[43] + z[37];
    z[37]=z[5]*z[37];
    z[43]=static_cast<T>(9)- n<T>(37,6)*z[3];
    z[43]=z[3]*z[43];
    z[43]= - n<T>(35,8) + z[43];
    z[43]=z[3]*z[43];
    z[43]=n<T>(131,48) + z[43];
    z[43]=z[3]*z[43];
    z[37]=z[43] + z[37];
    z[37]=z[37]*z[26];
    z[43]=n<T>(185,3)*z[3];
    z[44]= - n<T>(119,2) + z[43];
    z[44]=z[3]*z[44];
    z[44]=n<T>(35,2) + z[44];
    z[44]=z[3]*z[44];
    z[44]= - n<T>(227,24) + z[44];
    z[44]=z[44]*z[17];
    z[13]=z[37] + z[44] + z[13];
    z[13]=z[13]*z[26];
    z[37]=n<T>(1,8)*z[3];
    z[44]=n<T>(35,2) + z[23];
    z[44]=z[44]*z[37];
    z[44]=n<T>(11,3) + z[44];
    z[44]=z[3]*z[44];
    z[44]=n<T>(29,48) + z[44];
    z[44]=z[3]*z[44];
    z[39]=z[39]*z[18];
    z[39]=n<T>(41,2) + z[39];
    z[39]=z[39]*z[42];
    z[42]= - static_cast<T>(1)- z[35];
    z[42]=z[2]*z[42]*z[15];
    z[39]=n<T>(11,8)*z[42] + z[44] + z[39];
    z[39]=z[2]*z[39];
    z[42]= - static_cast<T>(5)- z[23];
    z[42]=z[42]*z[29];
    z[42]= - n<T>(167,6) + z[42];
    z[42]=z[3]*z[42];
    z[42]= - n<T>(29,3) + z[42];
    z[42]=z[3]*z[42];
    z[36]= - static_cast<T>(9)- z[36];
    z[36]=z[3]*z[36];
    z[36]= - static_cast<T>(9)+ z[36];
    z[36]=z[36]*z[30];
    z[44]= - n<T>(141,2) - n<T>(181,3)*z[3];
    z[44]=z[3]*z[44];
    z[44]= - n<T>(167,2) + z[44];
    z[44]=z[3]*z[44];
    z[44]= - n<T>(7,2) + z[44];
    z[44]=z[3]*z[44];
    z[36]=z[44] + 5*z[36];
    z[36]=z[5]*z[36];
    z[36]=z[42] + z[36];
    z[36]=n<T>(1,8)*z[36] + z[39];
    z[36]=z[2]*z[36];
    z[39]=n<T>(157,3) + n<T>(429,2)*z[3];
    z[39]=z[3]*z[39];
    z[39]=n<T>(323,6) + z[39];
    z[39]=z[39]*z[37];
    z[39]= - n<T>(1,3) + z[39];
    z[39]=z[3]*z[39];
    z[42]=z[3] + 1;
    z[44]=z[42]*z[35];
    z[44]=n<T>(1,4) + z[44];
    z[44]=z[44]*z[30];
    z[45]=static_cast<T>(11)+ n<T>(295,24)*z[3];
    z[45]=z[3]*z[45];
    z[45]=static_cast<T>(5)+ z[45];
    z[45]=z[3]*z[45];
    z[45]= - n<T>(15,16) + z[45];
    z[45]=z[3]*z[45];
    z[44]=z[45] + n<T>(11,2)*z[44];
    z[44]=z[5]*z[44];
    z[39]=z[39] + z[44];
    z[39]=z[5]*z[39];
    z[44]= - static_cast<T>(17)+ 185*z[3];
    z[44]=z[3]*z[44];
    z[44]=n<T>(35,2) + z[44];
    z[44]=z[44]*z[35];
    z[44]=n<T>(15,8) + z[44];
    z[44]=z[44]*z[17];
    z[36]=z[36] + z[44] + z[39];
    z[36]=z[2]*z[36];
    z[39]=n<T>(1,24)*z[3];
    z[44]=static_cast<T>(67)- 1405*z[3];
    z[44]=z[44]*z[39];
    z[44]=static_cast<T>(3)+ z[44];
    z[44]=z[3]*z[44];
    z[44]=n<T>(37,12) + z[44];
    z[44]=z[3]*z[44];
    z[45]=z[35]*z[5];
    z[46]=n<T>(37,8) - 11*z[15];
    z[46]=z[3]*z[46];
    z[46]=n<T>(37,8) + z[46];
    z[46]=z[46]*z[45];
    z[44]=z[44] + z[46];
    z[44]=z[5]*z[44];
    z[46]=static_cast<T>(85)- n<T>(1909,6)*z[3];
    z[46]=z[3]*z[46];
    z[46]= - n<T>(67,6) + z[46];
    z[46]=z[3]*z[46];
    z[46]=n<T>(109,12) + z[46];
    z[46]=z[46]*z[17];
    z[44]=z[46] + z[44];
    z[44]=z[44]*z[41];
    z[46]=n<T>(185,12)*z[3];
    z[47]=static_cast<T>(8)- z[46];
    z[47]=z[3]*z[47];
    z[47]= - n<T>(131,96) + z[47];
    z[47]=z[3]*z[47];
    z[47]=n<T>(61,96) + z[47];
    z[47]=z[3]*z[47];
    z[12]=z[12] + z[36] + z[13] + z[47] + z[44];
    z[12]=z[1]*z[12];
    z[13]=z[5] + 1;
    z[36]=z[41] + 1;
    z[36]=z[36]*z[5];
    z[36]=z[36] + n<T>(1,2);
    z[44]=z[6]*z[36];
    z[44]= - n<T>(1,2)*z[13] + z[44];
    z[47]=n<T>(1,2)*z[6];
    z[48]=n<T>(1,3)*z[5];
    z[49]= - static_cast<T>(1)- z[48];
    z[49]=z[5]*z[49];
    z[49]= - static_cast<T>(1)+ z[49];
    z[49]=z[5]*z[49];
    z[49]= - n<T>(1,3) + z[49];
    z[49]=z[49]*z[47];
    z[49]=n<T>(1,3)*z[36] + z[49];
    z[49]=z[4]*z[49];
    z[44]=n<T>(1,3)*z[44] + z[49];
    z[44]=z[4]*z[6]*z[44];
    z[49]=npow(z[5],2);
    z[50]=z[13] - z[2];
    z[51]= - z[50]*npow(z[2],2);
    z[51]= - z[49] + z[51];
    z[51]=z[8]*z[51];
    z[52]=z[49]*z[4];
    z[53]= - z[5] - z[52];
    z[50]= - z[2]*z[50];
    z[50]=z[51] + n<T>(1,2)*z[53] + z[50];
    z[50]=z[8]*z[50];
    z[51]= - z[5] + z[52];
    z[26]=z[51]*z[26];
    z[26]=z[26] + z[50];
    z[50]=3*z[8];
    z[26]=z[26]*z[50];
    z[51]=z[49]*z[6];
    z[52]=z[5] - z[51];
    z[52]=z[52]*z[47];
    z[53]=npow(z[5],3);
    z[54]= - z[53]*z[47];
    z[54]=z[49] + z[54];
    z[54]=z[6]*z[54];
    z[54]= - z[41] + z[54];
    z[54]=z[10]*z[54];
    z[52]=z[52] + z[54];
    z[52]=z[10]*z[52];
    z[26]=n<T>(19,8)*z[52] + n<T>(59,8)*z[44] + z[26];
    z[26]=z[7]*z[26];
    z[44]= - n<T>(649,2) + 247*z[3];
    z[44]=z[3]*z[44];
    z[44]=n<T>(2033,16) + z[44];
    z[44]=z[44]*z[35];
    z[44]= - n<T>(83,8) + z[44];
    z[44]=z[3]*z[44];
    z[26]=z[26] - n<T>(3,2) + z[44];
    z[44]= - static_cast<T>(71)+ z[34];
    z[44]=z[44]*z[29];
    z[44]=n<T>(1037,6) + z[44];
    z[44]=z[3]*z[44];
    z[44]= - n<T>(539,24) + z[44];
    z[44]=z[3]*z[44];
    z[52]= - n<T>(59,4) + z[23];
    z[52]=z[52]*z[29];
    z[52]=n<T>(43,2) + z[52];
    z[52]=z[52]*z[15];
    z[52]=z[52] + n<T>(37,12)*z[16];
    z[52]=z[5]*z[52];
    z[44]=z[44] + z[52];
    z[44]=z[5]*z[44];
    z[43]= - n<T>(249,2) + z[43];
    z[43]=z[43]*z[29];
    z[43]=n<T>(1663,4) + z[43];
    z[43]=z[3]*z[43];
    z[43]= - n<T>(5071,48) + z[43];
    z[43]=z[3]*z[43];
    z[43]=z[44] + n<T>(177,16) + z[43];
    z[43]=z[5]*z[43];
    z[44]= - static_cast<T>(95)+ n<T>(481,12)*z[3];
    z[44]=z[44]*z[29];
    z[44]=static_cast<T>(399)+ z[44];
    z[44]=z[3]*z[44];
    z[44]= - n<T>(3847,24) + z[44];
    z[44]=z[3]*z[44];
    z[43]=z[43] + n<T>(1475,48) + z[44];
    z[44]=n<T>(1,4)*z[5];
    z[43]=z[43]*z[44];
    z[52]=z[21] - n<T>(221,3);
    z[54]=n<T>(5,2)*z[3];
    z[52]=z[52]*z[54];
    z[52]=z[52] + n<T>(329,3);
    z[52]=z[52]*z[27];
    z[52]=z[52] - 9;
    z[52]=z[52]*z[3];
    z[54]=z[3] - n<T>(5,3);
    z[54]=z[54]*z[27];
    z[54]=z[54] + n<T>(1,3);
    z[31]=z[54]*z[31];
    z[31]=z[52] + z[31];
    z[31]=z[31]*z[5];
    z[52]=z[21] - n<T>(257,3);
    z[52]=z[52]*z[29];
    z[52]=z[52] + n<T>(1997,6);
    z[52]=z[52]*z[3];
    z[52]=z[52] - n<T>(393,4);
    z[52]=z[52]*z[3];
    z[52]=z[52] + n<T>(59,4);
    z[31]=z[31] + n<T>(1,2)*z[52];
    z[31]=z[31]*z[5];
    z[52]=z[21] - n<T>(293,3);
    z[52]=z[52]*z[29];
    z[52]=z[52] + n<T>(2783,6);
    z[52]=z[52]*z[3];
    z[52]=z[52] - n<T>(1625,8);
    z[52]=z[52]*z[3];
    z[52]=z[52] + n<T>(649,12);
    z[31]=z[31] + n<T>(1,2)*z[52];
    z[31]=z[31]*z[5];
    z[52]=z[21] - n<T>(329,3);
    z[52]=z[52]*z[29];
    z[52]=z[52] + n<T>(1837,3);
    z[52]=z[52]*z[3];
    z[52]=z[52] - 367;
    z[52]=z[52]*z[3];
    z[52]=z[52] + n<T>(767,6);
    z[31]=z[31] + n<T>(1,4)*z[52];
    z[31]=z[31]*z[5];
    z[52]=z[21] - n<T>(365,3);
    z[52]=z[52]*z[3];
    z[52]=z[52] + n<T>(467,3);
    z[52]=z[52]*z[3];
    z[52]=z[52] - n<T>(485,4);
    z[52]=z[52]*z[3];
    z[52]=z[52] + n<T>(295,6);
    z[31]=z[31] + n<T>(1,4)*z[52];
    z[52]=n<T>(1,4)*z[6];
    z[55]= - z[31]*z[52];
    z[56]= - n<T>(535,16) + z[23];
    z[56]=z[3]*z[56];
    z[56]=n<T>(1615,48) + z[56];
    z[56]=z[3]*z[56];
    z[56]= - n<T>(3701,192) + z[56];
    z[56]=z[3]*z[56];
    z[43]=z[55] + z[43] + n<T>(59,12) + z[56];
    z[43]=z[6]*z[43];
    z[55]= - static_cast<T>(43)+ z[33];
    z[55]=z[3]*z[55];
    z[55]=n<T>(61,2) + z[55];
    z[55]=z[55]*z[27];
    z[55]= - static_cast<T>(3)+ z[55];
    z[55]=z[3]*z[55];
    z[56]=z[27] - 1;
    z[57]=z[56]*z[3];
    z[57]=z[57] + n<T>(1,2);
    z[58]=z[57]*z[30];
    z[55]=z[55] + n<T>(37,12)*z[58];
    z[55]=z[5]*z[55];
    z[58]= - static_cast<T>(49)+ z[33];
    z[58]=z[3]*z[58];
    z[58]=n<T>(177,4) + z[58];
    z[58]=z[3]*z[58];
    z[58]= - n<T>(31,2) + z[58];
    z[58]=z[58]*z[29];
    z[58]=n<T>(59,4) + z[58];
    z[55]=n<T>(1,4)*z[58] + z[55];
    z[55]=z[5]*z[55];
    z[58]= - static_cast<T>(55)+ z[33];
    z[58]=z[3]*z[58];
    z[58]=n<T>(239,4) + z[58];
    z[58]=z[58]*z[29];
    z[58]= - n<T>(2411,16) + z[58];
    z[58]=z[3]*z[58];
    z[58]=n<T>(413,8) + z[58];
    z[55]=n<T>(1,3)*z[58] + z[55];
    z[55]=z[5]*z[55];
    z[58]= - static_cast<T>(61)+ z[33];
    z[58]=z[3]*z[58];
    z[58]=static_cast<T>(77)+ z[58];
    z[58]=z[58]*z[29];
    z[58]= - n<T>(1031,4) + z[58];
    z[58]=z[58]*z[27];
    z[58]=static_cast<T>(59)+ z[58];
    z[55]=n<T>(1,2)*z[58] + z[55];
    z[55]=z[55]*z[44];
    z[58]= - static_cast<T>(67)+ z[33];
    z[37]=z[58]*z[37];
    z[37]=static_cast<T>(12)+ z[37];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(651,64) + z[37];
    z[37]=z[3]*z[37];
    z[37]=z[55] + n<T>(177,32) + z[37];
    z[37]=z[5]*z[37];
    z[33]= - static_cast<T>(73)+ z[33];
    z[33]=z[3]*z[33];
    z[33]=n<T>(467,4) + z[33];
    z[33]=z[3]*z[33];
    z[33]= - n<T>(485,4) + z[33];
    z[33]=z[3]*z[33];
    z[33]=n<T>(295,4) + z[33];
    z[33]=n<T>(1,48)*z[33] + z[37];
    z[33]=z[6]*z[33];
    z[31]= - n<T>(1,4)*z[31] + z[33];
    z[31]=z[6]*z[31];
    z[33]= - n<T>(73,3) + n<T>(111,8)*z[3];
    z[33]=z[33]*z[29];
    z[33]=n<T>(537,8) + z[33];
    z[33]=z[3]*z[33];
    z[33]= - n<T>(105,8) + z[33];
    z[33]=z[3]*z[33];
    z[33]=n<T>(35,48) + z[33];
    z[37]=z[17] - n<T>(1,3);
    z[37]=z[37]*z[29];
    z[37]=z[37] + n<T>(1,2);
    z[37]=z[37]*z[30];
    z[55]= - static_cast<T>(55)+ z[21];
    z[55]=z[3]*z[55];
    z[55]=static_cast<T>(9)+ n<T>(5,12)*z[55];
    z[55]=z[3]*z[55];
    z[55]= - n<T>(17,24) + z[55];
    z[55]=z[3]*z[55];
    z[55]=z[55] + n<T>(37,8)*z[37];
    z[55]=z[5]*z[55];
    z[33]=n<T>(1,4)*z[33] + z[55];
    z[33]=z[5]*z[33];
    z[21]= - n<T>(455,6) + z[21];
    z[21]=z[3]*z[21];
    z[21]=n<T>(107,2) + z[21];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(341,16) + z[21];
    z[21]=z[3]*z[21];
    z[21]=n<T>(131,24) + z[21];
    z[21]=n<T>(1,4)*z[21] + z[33];
    z[21]=z[5]*z[21];
    z[33]= - static_cast<T>(109)+ n<T>(185,4)*z[3];
    z[33]=z[33]*z[35];
    z[33]=n<T>(249,8) + z[33];
    z[33]=z[3]*z[33];
    z[33]= - n<T>(59,3) + z[33];
    z[33]=z[3]*z[33];
    z[33]=n<T>(59,24) + z[33];
    z[21]=z[31] + n<T>(1,8)*z[33] + z[21];
    z[21]=z[4]*z[21];
    z[31]=static_cast<T>(51)- z[34];
    z[31]=z[31]*z[29];
    z[31]= - n<T>(161,2) + z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(11,4) + z[31];
    z[31]=z[31]*z[27];
    z[23]=static_cast<T>(11)- z[23];
    z[23]=z[3]*z[23];
    z[23]= - n<T>(11,4) + z[23];
    z[23]=z[23]*z[15];
    z[23]=z[23] - n<T>(37,24)*z[16];
    z[33]=5*z[5];
    z[23]=z[23]*z[33];
    z[23]=z[31] + z[23];
    z[23]=z[5]*z[23];
    z[31]=static_cast<T>(23)- z[46];
    z[31]=z[31]*z[29];
    z[31]= - n<T>(427,8) + z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(577,48) + z[31];
    z[31]=z[3]*z[31];
    z[23]=z[23] - n<T>(35,96) + z[31];
    z[23]=z[5]*z[23];
    z[31]=static_cast<T>(145)- n<T>(481,6)*z[3];
    z[31]=z[31]*z[27];
    z[31]= - n<T>(133,3) + z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(485,24) + z[31];
    z[31]=z[3]*z[31];
    z[31]=n<T>(13,48) + z[31];
    z[23]=n<T>(1,2)*z[31] + z[23];
    z[21]=z[21] + n<T>(1,2)*z[23] + z[43];
    z[21]=z[4]*z[21];
    z[23]=7*z[3];
    z[31]=static_cast<T>(13)+ z[23];
    z[31]=z[31]*z[11];
    z[20]=n<T>(1,6)*z[31] + 11*z[20];
    z[20]=z[20]*z[47];
    z[31]= - n<T>(83,12) + z[29];
    z[31]=z[31]*z[11];
    z[34]=n<T>(55,6)*z[16];
    z[20]=z[20] + z[31] - z[34];
    z[20]=z[20]*z[52];
    z[31]=z[15] - 1;
    z[43]= - z[31]*z[27];
    z[43]=static_cast<T>(1)+ z[43];
    z[43]=z[3]*z[43];
    z[55]=static_cast<T>(1)- z[47];
    z[14]=z[6]*z[14]*z[55];
    z[14]=z[43] + z[14];
    z[14]=z[2]*z[14];
    z[43]= - static_cast<T>(1)- n<T>(67,4)*z[3];
    z[38]=z[43]*z[38];
    z[38]= - static_cast<T>(1)+ z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(11,6) + z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(11,12) + z[38];
    z[31]=z[3]*z[31];
    z[31]= - static_cast<T>(7)+ n<T>(11,4)*z[31];
    z[31]=z[31]*z[45];
    z[14]=n<T>(11,24)*z[14] + z[20] + n<T>(1,2)*z[38] + z[31];
    z[14]=z[2]*z[14];
    z[20]=static_cast<T>(253)- n<T>(571,3)*z[3];
    z[20]=z[20]*z[27];
    z[20]= - static_cast<T>(35)+ z[20];
    z[20]=z[20]*z[32];
    z[31]=n<T>(517,2) - 205*z[3];
    z[31]=z[3]*z[31];
    z[31]= - n<T>(35,2) + z[31];
    z[31]=z[31]*z[32];
    z[31]=z[31] + 55*z[16];
    z[31]=z[31]*z[48];
    z[20]=z[20] + z[31];
    z[31]= - n<T>(11,2) + z[3];
    z[31]=z[31]*z[27];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[31]*z[15];
    z[28]=z[28]*z[30];
    z[28]=z[31] - n<T>(11,8)*z[28];
    z[28]=z[5]*z[28];
    z[31]= - n<T>(185,3) + n<T>(67,2)*z[3];
    z[31]=z[3]*z[31];
    z[31]=n<T>(71,3) + z[31];
    z[31]=z[31]*z[15];
    z[28]=n<T>(1,16)*z[31] + z[28];
    z[28]=z[6]*z[28];
    z[20]=n<T>(1,4)*z[20] + z[28];
    z[20]=z[6]*z[20];
    z[28]=static_cast<T>(13)- n<T>(55,6)*z[15];
    z[28]=z[28]*z[27];
    z[28]=static_cast<T>(17)+ z[28];
    z[31]=z[27]*z[5];
    z[28]=z[28]*z[31];
    z[32]=193*z[3];
    z[38]= - n<T>(23,2) + z[32];
    z[38]=z[3]*z[38];
    z[38]=static_cast<T>(3)+ n<T>(1,12)*z[38];
    z[38]=z[3]*z[38];
    z[38]=n<T>(15,2) + z[38];
    z[38]=z[3]*z[38];
    z[28]=z[28] + n<T>(11,4) + z[38];
    z[28]=z[5]*z[28];
    z[38]= - static_cast<T>(17)+ n<T>(941,24)*z[3];
    z[38]=z[3]*z[38];
    z[38]=n<T>(13,8) + z[38];
    z[38]=z[3]*z[38];
    z[38]=n<T>(19,4) + z[38];
    z[38]=z[3]*z[38];
    z[38]=n<T>(11,4) + z[38];
    z[28]=n<T>(1,2)*z[38] + z[28];
    z[14]=z[14] + n<T>(1,2)*z[28] + z[20];
    z[14]=z[2]*z[14];
    z[20]= - static_cast<T>(287)+ n<T>(815,4)*z[3];
    z[20]=z[3]*z[20];
    z[20]=n<T>(391,4) + z[20];
    z[20]=z[3]*z[20];
    z[20]= - n<T>(27,4) + z[20];
    z[20]=z[20]*z[17];
    z[28]= - n<T>(619,8) + 65*z[3];
    z[28]=z[28]*z[35];
    z[28]=n<T>(5,2) + z[28];
    z[28]=z[28]*z[15];
    z[28]=z[28] - n<T>(55,12)*z[16];
    z[28]=z[5]*z[28];
    z[20]=z[20] + z[28];
    z[20]=z[5]*z[20];
    z[28]=3*z[3];
    z[38]=n<T>(23,3) - z[28];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(13,3) + z[38];
    z[38]=z[38]*z[29];
    z[38]=n<T>(7,3) + z[38];
    z[38]=z[3]*z[38];
    z[25]=n<T>(5,4)*z[38] + n<T>(11,3)*z[25];
    z[25]=z[5]*z[25];
    z[32]=n<T>(1171,3) - z[32];
    z[32]=z[3]*z[32];
    z[32]= - n<T>(659,3) + z[32];
    z[32]=z[32]*z[27];
    z[32]=n<T>(47,3) + z[32];
    z[32]=z[32]*z[27];
    z[25]=z[32] + z[25];
    z[25]=z[5]*z[25];
    z[32]=n<T>(275,6) - 21*z[3];
    z[32]=z[3]*z[32];
    z[32]= - n<T>(751,24) + z[32];
    z[32]=z[3]*z[32];
    z[32]=n<T>(17,6) + z[32];
    z[32]=z[3]*z[32];
    z[25]=z[32] + z[25];
    z[25]=z[25]*z[52];
    z[32]=n<T>(311,4)*z[3];
    z[38]= - static_cast<T>(134)+ z[32];
    z[38]=z[38]*z[35];
    z[38]=n<T>(367,16) + z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(91,48) + z[38];
    z[38]=z[3]*z[38];
    z[20]=z[25] + z[38] + z[20];
    z[20]=z[6]*z[20];
    z[25]=static_cast<T>(73)- 815*z[3];
    z[25]=z[25]*z[39];
    z[25]= - static_cast<T>(5)+ z[25];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(19,2) + z[25];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(11,4) + z[25];
    z[38]= - n<T>(29,6) - z[28];
    z[38]=z[38]*z[27];
    z[38]= - n<T>(10,3) + z[38];
    z[39]=z[5]*z[3];
    z[38]=z[38]*z[39];
    z[25]=n<T>(1,2)*z[25] + z[38];
    z[25]=z[5]*z[25];
    z[38]=n<T>(493,3) - 311*z[3];
    z[38]=z[38]*z[17];
    z[38]= - static_cast<T>(5)+ z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(15,4) + z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(11,4) + z[38];
    z[25]=n<T>(1,2)*z[38] + z[25];
    z[25]=z[5]*z[25];
    z[38]=n<T>(427,8) - 62*z[3];
    z[38]=z[38]*z[35];
    z[38]= - n<T>(113,32) + z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(35,48) + z[38];
    z[38]=z[3]*z[38];
    z[14]=z[14] + z[20] + z[25] - n<T>(11,48) + z[38];
    z[14]=z[2]*z[14];
    z[20]=z[5] + 3;
    z[20]=z[20]*z[5];
    z[20]=z[20] + 3;
    z[20]=z[20]*z[5];
    z[20]=z[20] + 1;
    z[20]=z[20]*z[47];
    z[20]=z[20] - 3*z[36];
    z[20]=z[6]*z[49]*z[20];
    z[25]=z[13]*z[49];
    z[20]=z[20] + z[25];
    z[25]=z[4]*z[20];
    z[38]= - n<T>(21,2) - z[5];
    z[38]=z[38]*z[41];
    z[43]=static_cast<T>(1)+ z[44];
    z[43]=z[5]*z[43];
    z[43]=n<T>(3,4) + z[43];
    z[45]= - static_cast<T>(7)- n<T>(5,4)*z[5];
    z[45]=z[5]*z[45];
    z[45]= - n<T>(41,4) + z[45];
    z[45]=z[5]*z[45];
    z[45]= - n<T>(9,2) + z[45];
    z[45]=z[6]*z[45];
    z[43]=13*z[43] + z[45];
    z[43]=z[6]*z[5]*z[43];
    z[25]= - n<T>(3,2)*z[25] + z[38] + z[43];
    z[38]=static_cast<T>(1)- z[5];
    z[38]=z[38]*z[41];
    z[38]=static_cast<T>(1)+ z[38];
    z[38]=z[5]*z[38];
    z[43]= - static_cast<T>(3)+ z[49];
    z[43]=z[43]*z[41];
    z[43]= - static_cast<T>(1)+ z[43];
    z[43]=z[6]*z[43]*z[41];
    z[38]=z[38] + z[43];
    z[38]=z[6]*z[38];
    z[13]= - z[5]*z[13];
    z[13]=static_cast<T>(3)+ z[13];
    z[43]=n<T>(1,2) + z[5];
    z[43]=z[5]*z[43];
    z[43]= - n<T>(1,2) + z[43];
    z[43]=z[6]*z[43];
    z[13]=n<T>(1,2)*z[13] + z[43];
    z[13]=z[6]*z[13];
    z[43]= - static_cast<T>(3)+ z[6];
    z[43]=z[43]*z[47];
    z[45]=z[41] - 1;
    z[43]=z[43] - z[45];
    z[43]=z[2]*z[43];
    z[13]=z[43] + z[13] + z[45];
    z[13]=z[13]*z[19];
    z[19]=z[45]*z[41];
    z[13]=z[13] + z[19] + z[38];
    z[13]=z[2]*z[13];
    z[13]= - n<T>(1,2)*z[20] + z[13];
    z[13]=z[13]*z[50];
    z[19]=n<T>(19,4) + z[5];
    z[19]=z[6]*z[19];
    z[19]=z[19] - static_cast<T>(11)- z[41];
    z[19]=z[6]*z[19];
    z[38]=n<T>(31,2) - z[5];
    z[19]=n<T>(1,2)*z[38] + z[19];
    z[38]= - z[6]*z[3];
    z[38]=z[28] + z[38];
    z[38]=z[38]*z[47];
    z[31]=z[31] - n<T>(5,4);
    z[38]=z[38] - z[3] + z[31];
    z[38]=z[2]*z[38];
    z[19]=n<T>(1,2)*z[19] + z[38];
    z[19]=z[2]*z[19];
    z[38]= - n<T>(1,4) - z[5];
    z[38]=z[38]*z[33];
    z[38]=static_cast<T>(9)+ z[38];
    z[43]=n<T>(11,4) + z[5];
    z[43]=z[5]*z[43];
    z[43]= - n<T>(1,8) + z[43];
    z[43]=z[5]*z[43];
    z[43]= - n<T>(15,8) + z[43];
    z[43]=z[6]*z[43];
    z[38]=n<T>(1,2)*z[38] + z[43];
    z[38]=z[6]*z[38];
    z[43]= - n<T>(7,4) + z[5];
    z[19]=z[19] + n<T>(3,2)*z[43] + z[38];
    z[19]=z[2]*z[19];
    z[13]=z[13] + n<T>(1,2)*z[25] + z[19];
    z[13]=z[8]*z[13];
    z[19]=z[20]*z[22];
    z[20]=n<T>(1,8)*z[5];
    z[22]= - static_cast<T>(1)- 3*z[5];
    z[22]=z[22]*z[20];
    z[25]=z[36]*z[51];
    z[36]=static_cast<T>(1)+ n<T>(7,8)*z[5];
    z[36]=z[5]*z[36];
    z[36]=n<T>(1,8) + z[36];
    z[36]=z[5]*z[36];
    z[25]=z[36] - n<T>(3,4)*z[25];
    z[25]=z[6]*z[25];
    z[19]=z[19] + z[22] + z[25];
    z[19]=z[4]*z[19];
    z[22]= - z[6]*z[28];
    z[22]=z[23] + z[22];
    z[22]=z[22]*z[47];
    z[25]=z[2]*z[27];
    z[22]=z[25] + z[22] - 2*z[3] + z[31];
    z[22]=z[2]*z[22];
    z[25]=static_cast<T>(5)+ z[5];
    z[25]=z[5]*z[25];
    z[25]=n<T>(17,2) + z[25];
    z[25]=z[25]*z[47];
    z[25]=z[25] - n<T>(27,4) - z[5];
    z[25]=z[25]*z[47];
    z[22]=z[22] + z[25] + static_cast<T>(2)+ z[41];
    z[22]=z[2]*z[22];
    z[25]=z[5] - n<T>(3,2);
    z[25]=z[25]*z[5];
    z[28]= - n<T>(19,4) + z[25];
    z[28]=z[5]*z[28];
    z[28]= - n<T>(9,4) + z[28];
    z[28]=z[28]*z[47];
    z[25]=z[28] + n<T>(15,8) - z[25];
    z[25]=z[6]*z[25];
    z[28]= - static_cast<T>(3)+ n<T>(7,2)*z[5];
    z[13]=z[13] + z[22] + 3*z[19] + n<T>(1,4)*z[28] + z[25];
    z[13]=z[8]*z[13];
    z[19]=static_cast<T>(963)- n<T>(1925,3)*z[3];
    z[19]=z[19]*z[17];
    z[19]= - static_cast<T>(99)+ z[19];
    z[19]=z[3]*z[19];
    z[19]=n<T>(41,3) + z[19];
    z[19]=z[3]*z[19];
    z[22]=n<T>(103,6) - 15*z[3];
    z[22]=z[22]*z[23];
    z[22]= - n<T>(25,2) + z[22];
    z[22]=z[22]*z[15];
    z[22]=z[22] + z[34];
    z[22]=z[22]*z[41];
    z[19]=z[19] + z[22];
    z[19]=z[5]*z[19];
    z[22]=static_cast<T>(3491)- n<T>(3775,2)*z[3];
    z[22]=z[22]*z[35];
    z[22]= - n<T>(1363,2) + z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(1757,12) + z[22];
    z[22]=z[3]*z[22];
    z[22]= - static_cast<T>(5)+ z[22];
    z[19]=n<T>(1,4)*z[22] + z[19];
    z[19]=z[5]*z[19];
    z[22]=n<T>(677,3) - 103*z[3];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(2039,12) + z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(483,8) + z[22];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(59,8) + z[22];
    z[19]=n<T>(1,2)*z[22] + z[19];
    z[22]= - static_cast<T>(45)+ n<T>(163,8)*z[3];
    z[22]=z[3]*z[22];
    z[22]=n<T>(123,4) + z[22];
    z[22]=z[22]*z[29];
    z[22]= - n<T>(131,4) + z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(5,2) + z[22];
    z[25]= - static_cast<T>(31)+ 13*z[3];
    z[25]=z[3]*z[25];
    z[25]=static_cast<T>(18)+ n<T>(5,6)*z[25];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(41,12) + z[25];
    z[25]=z[3]*z[25];
    z[25]=z[25] - n<T>(11,4)*z[37];
    z[25]=z[5]*z[25];
    z[22]=n<T>(1,4)*z[22] + z[25];
    z[22]=z[5]*z[22];
    z[25]= - n<T>(565,3) + z[32];
    z[17]=z[25]*z[17];
    z[17]=static_cast<T>(39)+ z[17];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(445,32) + z[17];
    z[17]=z[3]*z[17];
    z[17]=z[22] + n<T>(59,24) + z[17];
    z[17]=z[5]*z[17];
    z[22]= - n<T>(335,4) + z[40];
    z[22]=z[22]*z[35];
    z[22]=n<T>(873,32) + z[22];
    z[22]=z[3]*z[22];
    z[22]= - n<T>(649,48) + z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(295,96) + z[22];
    z[17]=n<T>(1,2)*z[22] + z[17];
    z[17]=z[6]*z[17];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[6]*z[17];
    z[19]=static_cast<T>(1)- n<T>(5,8)*z[3];
    z[19]=z[19]*z[35];
    z[19]= - n<T>(1,8) + z[19];
    z[22]=29*z[3];
    z[19]=z[19]*z[22];
    z[25]=z[54]*z[30];
    z[19]=z[19] - n<T>(59,8)*z[25];
    z[19]=z[5]*z[19];
    z[25]=n<T>(3,2) - z[3];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(1,2) + z[25];
    z[19]=n<T>(57,16)*z[25] + z[19];
    z[19]=z[5]*z[19];
    z[25]=n<T>(3,4)*z[3];
    z[28]=static_cast<T>(1)- z[25];
    z[19]=n<T>(19,16)*z[28] + z[19];
    z[19]=z[19]*z[51];
    z[28]=static_cast<T>(89)- z[40];
    z[28]=z[28]*z[15];
    z[16]=z[28] + n<T>(59,2)*z[16];
    z[16]=z[16]*z[20];
    z[18]=n<T>(523,32) - z[18];
    z[18]=z[3]*z[18];
    z[16]=z[18] + z[16];
    z[16]=z[16]*z[48];
    z[18]=static_cast<T>(9)- z[23];
    z[16]=n<T>(19,64)*z[18] + z[16];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(19,64) + z[16];
    z[16]=z[5]*z[16];
    z[16]=z[16] + z[19];
    z[16]=z[6]*z[16];
    z[18]=z[22] + n<T>(59,4)*z[30];
    z[18]=z[18]*z[48];
    z[18]=z[18] + n<T>(57,8);
    z[19]= - z[5]*z[24]*z[18];
    z[19]= - n<T>(19,8) + z[19];
    z[19]=z[19]*z[49];
    z[20]= - z[5]*z[57]*z[18];
    z[20]= - n<T>(19,8)*z[56] + z[20];
    z[20]=z[6]*z[20]*z[53];
    z[19]=z[19] + z[20];
    z[19]=z[6]*z[19];
    z[18]= - z[18]*z[49];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[10]*z[18];
    z[19]= - n<T>(35,24)*z[3] - z[30];
    z[19]=z[19]*z[33];
    z[19]= - n<T>(57,16) + z[19];
    z[19]=z[19]*z[44];
    z[16]=n<T>(1,4)*z[18] + z[19] + z[16];
    z[16]=z[10]*z[16];
    z[18]= - static_cast<T>(1673)+ n<T>(5255,2)*z[3];
    z[18]=z[18]*z[27];
    z[18]=static_cast<T>(179)+ z[18];
    z[18]=z[18]*z[35];
    z[18]=n<T>(11,2) + z[18];
    z[18]=z[3]*z[18];
    z[18]=n<T>(11,2) + z[18];
    z[19]=n<T>(1,2) + z[15];
    z[19]=z[3]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[39];
    z[20]=static_cast<T>(1)+ z[46];
    z[20]=z[3]*z[20];
    z[20]=n<T>(1,2) + z[20];
    z[20]=z[3]*z[20];
    z[20]=n<T>(11,12) + z[20];
    z[20]=z[3]*z[20];
    z[19]=n<T>(11,24)*z[19] + n<T>(11,24) + z[20];
    z[19]=z[5]*z[19];
    z[18]=n<T>(1,8)*z[18] + z[19];
    z[18]=z[5]*z[18];
    z[19]= - n<T>(97,2) + 49*z[3];
    z[19]=z[19]*z[22];
    z[19]=n<T>(731,2) + z[19];
    z[19]=z[3]*z[19];
    z[19]= - n<T>(59,4) + z[19];
    z[19]=z[3]*z[19];
    z[19]=n<T>(11,2) + z[19];
    z[18]=n<T>(1,24)*z[19] + z[18];
    z[18]=z[5]*z[18];
    z[11]= - static_cast<T>(1)- z[11];
    z[11]=z[11]*z[41];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[19]= - z[42]*z[39];
    z[19]= - z[25] + z[19];
    z[19]=z[2]*z[19];
    z[11]=z[19] + n<T>(1,2)*z[56] + z[11];
    z[11]=z[49]*z[11];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];
    z[15]=z[15] - z[24];
    z[19]=z[2]*z[3];
    z[15]=n<T>(1,2)*z[15] + z[19];
    z[15]=z[9]*z[53]*z[15];
    z[11]=n<T>(1,2)*z[15] + z[11];
    z[11]=z[9]*z[11];

    r += z[11] + z[12] + z[13] + z[14] + z[16] + z[17] + z[18] + z[21]
       + n<T>(1,4)*z[26];
 
    return r;
}

template double qqb_2lha_r2001(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2001(const std::array<dd_real,30>&);
#endif
