#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r88(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[2];
    z[4]=k[3];
    z[5]=k[11];
    z[6]=z[1] + 3;
    z[6]=z[1]*z[6];
    z[7]=z[3] - 3;
    z[8]= - z[1] + z[7];
    z[8]=z[3]*z[8];
    z[6]=z[8] + static_cast<T>(3)+ z[6];
    z[8]= - static_cast<T>(1)+ n<T>(1,2)*z[3];
    z[9]=n<T>(1,2)*z[4];
    z[10]=z[9] - n<T>(1,2)*z[1] + z[8];
    z[10]=z[4]*z[10];
    z[11]=z[3] - 1;
    z[12]= - 3*z[11] - z[4];
    z[12]=z[12]*z[9];
    z[8]=z[8]*z[3];
    z[8]=z[8] + n<T>(1,2);
    z[12]= - 3*z[8] + z[12];
    z[12]=z[4]*z[12];
    z[7]= - z[3]*z[7];
    z[7]= - static_cast<T>(3)+ z[7];
    z[7]=z[3]*z[7];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=n<T>(1,2)*z[7] + z[12];
    z[7]=z[5]*z[7];
    z[6]=z[7] + n<T>(1,2)*z[6] + 3*z[10];
    z[6]=z[2]*z[6];
    z[7]=z[9] + z[11];
    z[7]=z[4]*z[7];
    z[7]=z[7] + z[8];
    z[7]=z[5]*z[7];
    z[8]=z[4] - static_cast<T>(1)- z[1];
    z[6]=z[6] + n<T>(1,2)*z[8] + z[7];

    r += n<T>(1,4)*z[6]*z[2];
 
    return r;
}

template double qqb_2lha_r88(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r88(const std::array<dd_real,30>&);
#endif
