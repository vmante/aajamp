#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1631(const std::array<T,30>& k) {
  T z[52];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[10];
    z[9]=k[5];
    z[10]=k[6];
    z[11]=z[5]*z[6];
    z[12]=3*z[6];
    z[13]= - z[12] + static_cast<T>(7)- z[10];
    z[14]= - static_cast<T>(5)+ n<T>(3,2)*z[10];
    z[14]=z[8]*z[14];
    z[13]=n<T>(9,4)*z[11] + n<T>(1,2)*z[13] + z[14];
    z[14]=n<T>(1,2)*z[9];
    z[13]=z[13]*z[14];
    z[15]=n<T>(1,2)*z[10];
    z[16]= - z[15] + z[6];
    z[17]= - n<T>(3,2) + z[10];
    z[17]=z[8]*z[17];
    z[16]=n<T>(1,2)*z[16] + z[17];
    z[16]=z[9]*z[16];
    z[17]=n<T>(1,4)*z[10];
    z[18]=z[7]*z[8];
    z[19]=z[18]*z[9];
    z[20]=z[19]*z[17];
    z[21]=z[8]*z[10];
    z[22]=n<T>(1,4)*z[21];
    z[16]=z[20] - z[22] + z[16];
    z[20]=n<T>(1,2)*z[7];
    z[16]=z[16]*z[20];
    z[23]=n<T>(1,2)*z[8];
    z[24]= - z[10]*z[23];
    z[13]=z[16] + z[13] - z[6] + z[24];
    z[13]=z[13]*z[20];
    z[16]=static_cast<T>(7)+ 7*z[11];
    z[24]= - n<T>(3,2)*z[21] + n<T>(9,2)*z[6] - z[16];
    z[25]=n<T>(7,2)*z[6];
    z[26]=z[25] + static_cast<T>(1)- z[15];
    z[15]= - static_cast<T>(3)+ z[15];
    z[15]=z[8]*z[15];
    z[15]=n<T>(1,4)*z[26] + z[15];
    z[26]=z[11] + 1;
    z[27]= - n<T>(11,8)*z[6] + z[26];
    z[27]=z[5]*z[27];
    z[15]=n<T>(1,2)*z[15] + z[27];
    z[15]=z[9]*z[15];
    z[27]=z[1]*z[6];
    z[13]=z[13] + n<T>(3,4)*z[27] + n<T>(1,4)*z[24] + z[15];
    z[13]=z[7]*z[13];
    z[15]=z[25] - 3;
    z[21]= - z[21] - z[15];
    z[24]=z[6] - n<T>(3,4);
    z[25]= - n<T>(3,4)*z[11] + z[24];
    z[28]=3*z[5];
    z[25]=z[25]*z[28];
    z[29]=n<T>(5,4)*z[6];
    z[16]= - 15*z[6] + z[16];
    z[16]=z[5]*z[16];
    z[16]=n<T>(1,8)*z[16] - static_cast<T>(1)+ z[29];
    z[16]=z[5]*z[16];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[8]*z[17];
    z[17]=z[17] + n<T>(7,4) - z[6];
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[9]*z[16];
    z[17]=static_cast<T>(5)+ 5*z[11];
    z[30]= - z[12] + z[17];
    z[27]=n<T>(3,4)*z[30] - z[27];
    z[30]=n<T>(1,2)*z[1];
    z[27]=z[27]*z[30];
    z[13]=z[13] + z[27] + z[16] + n<T>(1,4)*z[21] + z[25];
    z[13]=z[7]*z[13];
    z[16]=n<T>(1,2)*z[18];
    z[21]=z[1]*z[4];
    z[25]=3*z[4] - static_cast<T>(3)- z[8];
    z[25]= - z[16] + n<T>(1,2)*z[25] + 3*z[21];
    z[25]=z[2]*z[25];
    z[27]=z[4] + n<T>(3,2);
    z[31]= - n<T>(15,2)*z[27] + z[21];
    z[31]=z[1]*z[31];
    z[32]=7*z[8];
    z[33]=z[32] + n<T>(9,2)*z[18];
    z[34]=static_cast<T>(9)+ z[33];
    z[34]=z[34]*z[20];
    z[35]=z[9] + 1;
    z[36]=n<T>(7,2)*z[4];
    z[25]=z[25] + z[34] + z[31] - z[36] + n<T>(5,4)*z[8] + z[35];
    z[31]=n<T>(1,4)*z[2];
    z[25]=z[25]*z[31];
    z[34]=3*z[9];
    z[37]= - static_cast<T>(17)- 11*z[8];
    z[37]=n<T>(1,2)*z[37] - z[34];
    z[37]=n<T>(1,2)*z[37] + 7*z[1];
    z[38]=z[18] + 1;
    z[39]=n<T>(17,8)*z[8];
    z[40]= - z[39] - z[38];
    z[40]=z[7]*z[40];
    z[37]=n<T>(1,2)*z[37] + z[40];
    z[37]=z[7]*z[37];
    z[40]=n<T>(1,8)*z[21] - z[27];
    z[40]=z[1]*z[40];
    z[41]=static_cast<T>(23)+ n<T>(25,4)*z[4];
    z[40]=n<T>(1,2)*z[41] + z[40];
    z[40]=z[1]*z[40];
    z[41]=z[23] - 1;
    z[40]=z[40] - z[9] + n<T>(5,4)*z[4] - z[41];
    z[25]=z[25] + n<T>(1,2)*z[40] + z[37];
    z[25]=z[2]*z[25];
    z[37]=z[4] + 1;
    z[40]= - z[37]*z[30];
    z[40]=z[40] + n<T>(9,2) + z[4];
    z[40]=z[40]*z[30];
    z[40]=z[40] - n<T>(59,8) - z[4];
    z[40]=z[1]*z[40];
    z[42]=n<T>(5,2)*z[9];
    z[43]=z[42] - static_cast<T>(5)- z[4];
    z[40]=n<T>(1,4)*z[43] + z[40];
    z[43]= - n<T>(21,2) + z[1];
    z[43]=z[43]*z[30];
    z[44]=z[14] + 1;
    z[45]= - z[1] + z[44];
    z[45]=z[7]*z[45];
    z[43]=n<T>(3,2)*z[45] + z[43] + n<T>(3,4) + z[9];
    z[43]=z[7]*z[43];
    z[25]=z[25] + n<T>(1,2)*z[40] + z[43];
    z[25]=z[2]*z[25];
    z[40]=z[4] - 1;
    z[43]= - n<T>(3,4)*z[40] - z[21];
    z[43]=z[1]*z[43];
    z[16]=z[16] + static_cast<T>(1)+ z[23];
    z[16]=z[16]*z[20];
    z[16]=z[16] + n<T>(1,2) + z[43];
    z[16]=z[2]*z[16];
    z[33]= - n<T>(9,2) - z[33];
    z[33]=z[7]*z[33];
    z[33]=z[33] - n<T>(9,2)*z[1] - static_cast<T>(7)- n<T>(5,2)*z[8];
    z[33]=z[7]*z[33];
    z[43]=z[30]*z[4];
    z[45]= - z[43] + n<T>(23,2) + 13*z[4];
    z[45]=z[1]*z[45];
    z[45]=z[45] - static_cast<T>(7)+ n<T>(19,2)*z[4];
    z[45]=z[1]*z[45];
    z[33]=z[33] - n<T>(21,2) + z[45];
    z[16]=n<T>(1,4)*z[33] + z[16];
    z[33]=n<T>(1,2)*z[2];
    z[16]=z[16]*z[33];
    z[45]=static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[46]=z[45]*z[30];
    z[47]=n<T>(1,2)*z[4];
    z[48]=z[47] + 1;
    z[46]= - 15*z[48] + z[46];
    z[46]=z[1]*z[46];
    z[36]=static_cast<T>(1)- z[36];
    z[36]=n<T>(3,2)*z[36] + z[46];
    z[36]=z[1]*z[36];
    z[46]=static_cast<T>(3)- z[14];
    z[49]=n<T>(9,4) - z[1];
    z[49]=z[1]*z[49];
    z[46]=n<T>(1,2)*z[46] + z[49];
    z[46]=z[7]*z[46];
    z[16]=z[16] + z[46] + static_cast<T>(2)+ n<T>(1,4)*z[36];
    z[16]=z[2]*z[16];
    z[36]=3*z[1];
    z[46]=z[9] - z[36];
    z[46]=z[46]*z[20];
    z[49]= - n<T>(7,2) + z[9];
    z[50]= - static_cast<T>(3)+ n<T>(7,4)*z[1];
    z[50]=z[1]*z[50];
    z[46]=z[46] + n<T>(1,2)*z[49] + z[50];
    z[46]=z[7]*z[46];
    z[49]=7*z[4];
    z[50]=n<T>(51,2) + z[49];
    z[51]= - static_cast<T>(1)- n<T>(3,4)*z[4];
    z[51]=z[1]*z[51];
    z[50]=n<T>(1,2)*z[50] + z[51];
    z[50]=z[1]*z[50];
    z[45]=n<T>(3,2)*z[45] + z[50];
    z[45]=z[1]*z[45];
    z[45]= - n<T>(17,4) + z[45];
    z[16]=z[16] + n<T>(1,4)*z[45] + z[46];
    z[16]=z[2]*z[16];
    z[37]=z[1]*z[37];
    z[37]=z[37] + z[40];
    z[37]=z[37]*z[30];
    z[37]= - static_cast<T>(1)+ z[37];
    z[37]=z[1]*z[37];
    z[45]=npow(z[1],2);
    z[45]=static_cast<T>(1)- z[45];
    z[45]=z[45]*z[20];
    z[37]=z[37] + z[45];
    z[45]= - n<T>(1,2) - z[4];
    z[45]=z[1]*z[45];
    z[45]=z[45] - z[40];
    z[45]=z[1]*z[45];
    z[45]=n<T>(3,2) + z[45];
    z[45]=z[1]*z[45];
    z[46]=z[21] + z[40];
    z[46]=z[1]*z[46];
    z[46]= - static_cast<T>(1)+ z[46];
    z[46]=z[1]*z[46];
    z[38]= - z[8] - z[38];
    z[38]=z[7]*z[38];
    z[38]=z[38] - static_cast<T>(1)- z[1];
    z[38]=z[7]*z[38];
    z[38]=z[46] + z[38];
    z[38]=z[38]*z[31];
    z[38]=z[45] + z[38];
    z[38]=z[2]*z[38];
    z[37]=3*z[37] + z[38];
    z[37]=z[37]*z[33];
    z[38]=static_cast<T>(1)+ z[36];
    z[38]=z[38]*z[30];
    z[45]= - n<T>(3,2)*z[1] - static_cast<T>(1)+ z[14];
    z[45]=z[45]*z[20];
    z[38]=z[45] - static_cast<T>(1)+ z[38];
    z[38]=z[7]*z[38];
    z[27]= - z[1]*z[27];
    z[27]=z[27] - z[40];
    z[27]=z[1]*z[27];
    z[27]=n<T>(5,2) + z[27];
    z[27]=z[27]*z[30];
    z[27]=z[37] + z[27] + z[38];
    z[27]=z[2]*z[27];
    z[37]=static_cast<T>(5)- z[9];
    z[35]= - z[7]*z[35];
    z[35]=z[35] + n<T>(1,2)*z[37] + z[36];
    z[35]=z[7]*z[35];
    z[37]= - n<T>(1,2) - z[1];
    z[37]=z[1]*z[37];
    z[37]=n<T>(1,2) + z[37];
    z[35]=3*z[37] + z[35];
    z[35]=z[7]*z[35];
    z[37]=z[48]*z[1];
    z[38]=n<T>(1,2)*z[40] + z[37];
    z[38]=z[1]*z[38];
    z[38]= - n<T>(3,2) + z[38];
    z[38]=z[1]*z[38];
    z[35]=z[38] + z[35];
    z[27]=n<T>(1,4)*z[35] + z[27];
    z[27]=z[3]*z[27];
    z[35]=n<T>(7,4) - z[1];
    z[35]=z[35]*z[36];
    z[36]=static_cast<T>(7)- z[14];
    z[35]=n<T>(1,2)*z[36] + z[35];
    z[36]= - z[9] + n<T>(9,4)*z[1];
    z[38]= - z[7]*z[44];
    z[38]=z[38] - n<T>(1,2) + z[36];
    z[38]=z[7]*z[38];
    z[35]=n<T>(1,2)*z[35] + z[38];
    z[35]=z[7]*z[35];
    z[38]=z[37] - static_cast<T>(7)- z[4];
    z[38]=z[38]*z[30];
    z[40]=n<T>(1,4)*z[4];
    z[44]=z[40] + 1;
    z[38]=z[38] - z[44];
    z[38]=z[1]*z[38];
    z[38]=n<T>(1,2) + z[38];
    z[35]=n<T>(1,2)*z[38] + z[35];
    z[16]=z[27] + n<T>(1,2)*z[35] + z[16];
    z[16]=z[3]*z[16];
    z[27]= - static_cast<T>(3)- z[9];
    z[27]=z[27]*z[20];
    z[27]=z[27] - n<T>(9,4) + z[36];
    z[27]=z[7]*z[27];
    z[35]= - static_cast<T>(1)- n<T>(5,4)*z[9];
    z[36]=n<T>(27,8) - z[1];
    z[36]=z[1]*z[36];
    z[27]=z[27] + n<T>(1,2)*z[35] + z[36];
    z[27]=z[7]*z[27];
    z[35]=z[37] - 3;
    z[36]=z[35]*z[30];
    z[36]=z[36] + static_cast<T>(3)+ z[40];
    z[36]=z[1]*z[36];
    z[36]=static_cast<T>(1)+ z[36];
    z[27]=n<T>(1,2)*z[36] + z[27];
    z[16]=z[16] + n<T>(1,2)*z[27] + z[25];
    z[16]=z[3]*z[16];
    z[25]=z[49] + n<T>(37,2) - z[32];
    z[27]=z[4] - z[8];
    z[36]=n<T>(1,4)*z[9];
    z[37]= - z[27]*z[36];
    z[37]=z[37] + n<T>(1,4)*z[8] - z[4];
    z[33]=z[37]*z[33];
    z[37]=n<T>(3,16)*z[4] + static_cast<T>(1)- n<T>(5,16)*z[8];
    z[37]=z[9]*z[37];
    z[25]=z[33] - n<T>(9,16)*z[18] - n<T>(3,8)*z[21] + n<T>(1,8)*z[25] + z[37];
    z[25]=z[2]*z[25];
    z[33]=n<T>(1,8)*z[4];
    z[37]= - z[33] - static_cast<T>(3)+ z[23];
    z[37]=z[9]*z[37];
    z[38]=3*z[44] - z[43];
    z[38]=z[1]*z[38];
    z[44]= - static_cast<T>(13)+ n<T>(11,2)*z[8];
    z[37]=z[38] + z[37] + n<T>(1,2)*z[44] - z[4];
    z[38]=z[18] - n<T>(3,2)*z[9] - static_cast<T>(4)+ z[39];
    z[38]=z[7]*z[38];
    z[25]=z[25] + n<T>(1,2)*z[37] + z[38];
    z[25]=z[2]*z[25];
    z[23]=static_cast<T>(5)- z[23];
    z[23]=z[23]*z[36];
    z[37]= - n<T>(1,4)*z[21] + static_cast<T>(3)+ z[4];
    z[37]=z[1]*z[37];
    z[37]= - n<T>(9,2) + z[37];
    z[37]=z[37]*z[30];
    z[23]=z[37] + z[23] - 3*z[41] + z[40];
    z[37]=static_cast<T>(33)- n<T>(35,2)*z[8];
    z[34]= - n<T>(15,4)*z[1] + n<T>(1,4)*z[37] + z[34];
    z[37]=19*z[8];
    z[38]=static_cast<T>(25)- z[37];
    z[18]= - n<T>(7,8)*z[18] + n<T>(1,8)*z[38] + z[9];
    z[18]=z[7]*z[18];
    z[18]=n<T>(1,2)*z[34] + z[18];
    z[18]=z[7]*z[18];
    z[18]=z[25] + n<T>(1,2)*z[23] + z[18];
    z[18]=z[2]*z[18];
    z[23]= - static_cast<T>(11)- z[42];
    z[25]=n<T>(3,2) - z[1];
    z[25]=z[1]*z[25];
    z[23]=n<T>(1,4)*z[23] + z[25];
    z[25]= - static_cast<T>(1)- z[36];
    z[25]=z[7]*z[25];
    z[25]=z[25] - static_cast<T>(2)- z[14] + n<T>(3,4)*z[1];
    z[25]=z[7]*z[25];
    z[23]=n<T>(1,2)*z[23] + z[25];
    z[23]=z[7]*z[23];
    z[25]=z[1]*z[35];
    z[25]=n<T>(1,2) + z[25];
    z[25]=z[1]*z[25];
    z[25]=z[25] - static_cast<T>(3)- z[14];
    z[16]=z[16] + z[18] + n<T>(1,8)*z[25] + z[23];
    z[16]=z[3]*z[16];
    z[18]=z[4]*z[5];
    z[23]=z[6] - z[26];
    z[23]=z[23]*z[18];
    z[25]=z[26]*z[47];
    z[25]=z[6] + z[25];
    z[25]=z[25]*z[30];
    z[34]=n<T>(1,2)*z[6] - z[26];
    z[23]=z[25] + 3*z[34] + z[23];
    z[23]=z[23]*z[30];
    z[25]= - z[29] + z[26];
    z[25]=z[25]*z[28];
    z[28]=z[6] - n<T>(1,2);
    z[29]=n<T>(1,2)*z[11] - z[28];
    z[29]=z[5]*z[29];
    z[30]=z[6] - 1;
    z[34]=n<T>(1,2)*z[30];
    z[29]=z[34] + z[29];
    z[29]=z[29]*z[18];
    z[15]=z[23] + n<T>(3,2)*z[29] + n<T>(1,4)*z[15] + z[25];
    z[15]=z[1]*z[15];
    z[23]=static_cast<T>(11)- 13*z[6];
    z[17]=n<T>(21,2)*z[6] - z[17];
    z[17]=z[5]*z[17];
    z[17]=n<T>(1,2)*z[23] + z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] - z[22] + z[30];
    z[12]=z[12] - z[26];
    z[22]=n<T>(3,2)*z[5];
    z[22]= - z[12]*z[22];
    z[22]=z[22] - static_cast<T>(3)+ n<T>(19,4)*z[6];
    z[23]=n<T>(1,2)*z[5];
    z[22]=z[22]*z[23];
    z[22]=z[22] + n<T>(7,8) - z[6];
    z[22]=z[5]*z[22];
    z[11]=n<T>(1,4)*z[11] + n<T>(1,4) - z[6];
    z[11]=z[5]*z[11];
    z[11]=n<T>(3,2)*z[28] + z[11];
    z[11]=z[5]*z[11];
    z[11]=z[11] - z[24];
    z[11]=z[5]*z[11];
    z[11]=n<T>(1,4)*z[30] + z[11];
    z[24]=z[47]*z[5];
    z[11]=z[11]*z[24];
    z[25]=z[34] - z[8];
    z[11]=z[11] + n<T>(1,4)*z[25] + z[22];
    z[11]=z[9]*z[11];
    z[12]=z[12]*z[23];
    z[12]=z[12] + static_cast<T>(1)- n<T>(3,2)*z[6];
    z[12]=z[5]*z[12];
    z[12]=z[34] + z[12];
    z[12]=z[12]*z[18];
    z[11]=z[15] + z[11] + n<T>(1,2)*z[17] + z[12];
    z[12]=z[5] - 1;
    z[15]=z[12]*z[18];
    z[17]=static_cast<T>(1)- z[23];
    z[17]=z[5]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[17]*z[24];
    z[12]=z[12]*z[5];
    z[17]=z[17] - n<T>(3,2)*z[12] - static_cast<T>(1)+ 3*z[8];
    z[17]=z[9]*z[17];
    z[15]=z[17] + z[15] - n<T>(1,2) + 5*z[5];
    z[17]= - static_cast<T>(1)+ n<T>(7,2)*z[8];
    z[17]=5*z[17] - 7*z[5];
    z[17]=z[17]*z[14];
    z[22]= - static_cast<T>(13)+ z[37];
    z[22]=z[9]*z[22];
    z[22]=z[22] + 7*z[19];
    z[22]=z[22]*z[20];
    z[17]=z[22] + static_cast<T>(9)+ z[17];
    z[17]=z[17]*z[20];
    z[20]= - static_cast<T>(1)- n<T>(1,4)*z[18];
    z[20]=3*z[20] + z[43];
    z[20]=z[1]*z[20];
    z[15]=z[17] + n<T>(1,2)*z[15] + z[20];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[12]*z[33];
    z[12]=z[12] + n<T>(3,4)*z[5] + static_cast<T>(1)- n<T>(11,4)*z[8];
    z[12]=z[9]*z[12];
    z[17]= - static_cast<T>(5)- z[18];
    z[12]=n<T>(3,4)*z[21] + n<T>(1,2)*z[17] + z[12];
    z[17]= - static_cast<T>(1)- z[23];
    z[17]=z[4]*z[17];
    z[18]=z[2]*z[27];
    z[17]=z[18] + z[17] - n<T>(11,2) + z[32];
    z[17]=z[14]*z[17];
    z[17]=n<T>(9,4)*z[19] + z[4] + z[17];
    z[17]=z[17]*z[31];
    z[18]=static_cast<T>(3)- n<T>(17,4)*z[8];
    z[14]=z[18]*z[14];
    z[14]=z[14] - z[19];
    z[14]=z[7]*z[14];
    z[12]=z[17] + n<T>(1,2)*z[12] + z[14];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,2)*z[15] + z[12];
    z[12]=z[2]*z[12];

    r += n<T>(1,2)*z[11] + z[12] + z[13] + z[16];
 
    return r;
}

template double qqb_2lha_r1631(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1631(const std::array<dd_real,30>&);
#endif
