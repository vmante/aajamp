#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1196(const std::array<T,30>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[6];
    z[7]=k[17];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[10];
    z[11]=k[15];
    z[12]=n<T>(403,4)*z[11];
    z[13]=z[12] - 61;
    z[14]=n<T>(1,3)*z[11];
    z[13]=z[13]*z[14];
    z[13]=z[13] - n<T>(13,2);
    z[15]=n<T>(1,2)*z[4];
    z[16]=z[13]*z[15];
    z[17]=n<T>(1,4)*z[6];
    z[18]=403*z[11];
    z[19]= - n<T>(3067,3) + z[18];
    z[19]=z[11]*z[19];
    z[16]= - n<T>(65,8)*z[2] + z[16] - z[17] + n<T>(106,3) + n<T>(1,16)*z[19];
    z[19]=n<T>(47,3)*z[7];
    z[20]=n<T>(1,2)*z[2];
    z[21]=n<T>(1,3) + z[20];
    z[21]=z[21]*z[19];
    z[22]= - n<T>(103,9) + n<T>(5,2)*z[2];
    z[21]=z[21] + n<T>(5,4)*z[22];
    z[21]=z[2]*z[21];
    z[21]= - n<T>(67,9) + z[21];
    z[21]=z[7]*z[21];
    z[22]=n<T>(3,2)*z[3];
    z[23]= - n<T>(1,2) + z[2];
    z[23]=z[2]*z[23];
    z[23]= - n<T>(3,4) + z[23];
    z[23]=z[23]*z[22];
    z[24]=n<T>(1,2)*z[6];
    z[25]=z[24] - n<T>(73,3) + n<T>(563,8)*z[11];
    z[25]=n<T>(1,3)*z[25] + n<T>(9,4)*z[3];
    z[25]=z[5]*z[25];
    z[16]=n<T>(1,2)*z[25] + z[23] + n<T>(1,3)*z[16] + z[21];
    z[16]=z[9]*z[16];
    z[21]=static_cast<T>(53)+ n<T>(403,3)*z[11];
    z[21]=z[21]*z[14];
    z[23]=z[21] + 9*z[6];
    z[15]= - z[23]*z[15];
    z[25]=static_cast<T>(203)- n<T>(2015,6)*z[11];
    z[25]=z[11]*z[25];
    z[25]=n<T>(121,2) + z[25];
    z[15]=z[15] + n<T>(1,3)*z[25] - n<T>(15,2)*z[6];
    z[25]= - n<T>(5,2) - 2*z[2];
    z[25]=z[25]*z[19];
    z[26]=25*z[2];
    z[27]=n<T>(343,4) - z[26];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[27]=n<T>(1,3)*z[7];
    z[25]=z[25]*z[27];
    z[28]=z[2] - 1;
    z[29]=z[3]*z[2];
    z[30]= - z[28]*z[29];
    z[31]=n<T>(43,3) - n<T>(39,2)*z[2];
    z[30]=n<T>(1,2)*z[31] + z[30];
    z[30]=z[3]*z[30];
    z[31]= - n<T>(111,4) + z[19];
    z[31]=z[7]*z[31];
    z[31]= - n<T>(83,24)*z[3] + z[31] - n<T>(25,12)*z[6] + n<T>(13,6) - 29*z[11];
    z[32]=n<T>(67,6)*z[7];
    z[33]=z[32] + n<T>(76,9) + n<T>(35,16)*z[6];
    z[33]=z[5]*z[33];
    z[31]=n<T>(1,2)*z[31] + z[33];
    z[31]=z[5]*z[31];
    z[15]=z[16] + z[31] + n<T>(1,4)*z[30] + n<T>(1,8)*z[15] + z[25];
    z[15]=z[9]*z[15];
    z[16]= - n<T>(2581,3) + z[18];
    z[16]=z[11]*z[16];
    z[16]=z[16] - 37*z[6];
    z[25]=n<T>(47,9)*z[7];
    z[30]= - n<T>(69,4) + z[25];
    z[30]=z[7]*z[30];
    z[16]= - z[22] + n<T>(1,24)*z[16] + z[30];
    z[22]=n<T>(67,3)*z[7];
    z[30]=35*z[6];
    z[31]=563*z[11] + z[30];
    z[31]=n<T>(1,8)*z[31] + z[22];
    z[31]=n<T>(1,2)*z[31] + 4*z[3];
    z[33]=n<T>(1,3)*z[5];
    z[31]=z[31]*z[33];
    z[16]=n<T>(1,2)*z[16] + z[31];
    z[16]=z[5]*z[16];
    z[12]=static_cast<T>(71)- z[12];
    z[12]=z[12]*z[14];
    z[12]=n<T>(1,8)*z[6] + n<T>(13,2) + z[12];
    z[31]=z[4]*z[23];
    z[34]= - n<T>(2,3) - z[20];
    z[34]=z[34]*z[19];
    z[35]=n<T>(173,9) - n<T>(25,4)*z[2];
    z[34]=n<T>(1,2)*z[35] + z[34];
    z[34]=z[7]*z[34];
    z[35]=n<T>(1,2)*z[3];
    z[36]= - n<T>(7,6) - 3*z[2];
    z[36]=z[36]*z[35];
    z[12]=z[16] + z[36] + z[34] + n<T>(1,3)*z[12] - n<T>(1,16)*z[31];
    z[12]=z[9]*z[12];
    z[16]=94*z[7];
    z[31]= - n<T>(305,2) + z[16];
    z[31]=z[31]*z[27];
    z[34]= - 107*z[11] - 17*z[6];
    z[22]=z[22] + 4*z[6];
    z[36]=z[5]*z[22];
    z[31]=2*z[36] - n<T>(11,4)*z[3] + n<T>(1,4)*z[34] + z[31];
    z[31]=z[5]*z[31];
    z[34]=static_cast<T>(41)- n<T>(403,2)*z[11];
    z[34]=z[34]*z[14];
    z[36]=13*z[6];
    z[34]=z[34] - z[36];
    z[16]=n<T>(77,2) - z[16];
    z[16]=z[16]*z[27];
    z[16]=z[31] - z[35] + n<T>(1,2)*z[34] + z[16];
    z[16]=z[16]*z[33];
    z[31]=z[19] + n<T>(25,4);
    z[31]=z[31]*z[27];
    z[21]=z[21] + n<T>(19,2)*z[6];
    z[34]=n<T>(55,4) + z[29];
    z[34]=z[3]*z[34];
    z[12]=z[12] + z[16] + n<T>(1,12)*z[34] + n<T>(1,8)*z[21] + z[31];
    z[12]=z[9]*z[12];
    z[16]=n<T>(245,2) - z[18];
    z[14]=z[16]*z[14];
    z[14]=z[14] - n<T>(29,2)*z[6];
    z[16]=47*z[7];
    z[21]=n<T>(59,4) - z[16];
    z[21]=z[21]*z[27];
    z[14]= - 2*z[3] + n<T>(1,8)*z[14] + z[21];
    z[21]=n<T>(1,9)*z[7];
    z[34]= - n<T>(461,4) + z[16];
    z[34]=z[34]*z[21];
    z[37]= - static_cast<T>(967)+ z[18];
    z[37]=z[11]*z[37];
    z[37]=n<T>(1,18)*z[37] - 5*z[6];
    z[34]= - n<T>(5,8)*z[3] + n<T>(1,4)*z[37] + z[34];
    z[37]=2*z[6];
    z[38]=n<T>(23,16)*z[3] + n<T>(563,48)*z[11] + z[37] + z[32];
    z[38]=z[38]*z[33];
    z[34]=n<T>(1,2)*z[34] + z[38];
    z[34]=z[5]*z[34];
    z[14]=n<T>(1,3)*z[14] + z[34];
    z[14]=z[5]*z[14];
    z[23]=z[3] + n<T>(1,8)*z[23] + z[31];
    z[14]=n<T>(1,2)*z[23] + z[14];
    z[14]=z[8]*z[14]*npow(z[9],2);
    z[23]=n<T>(7,4) + z[29];
    z[23]=z[3]*z[23];
    z[17]=z[17] + n<T>(1,3)*z[23];
    z[17]=z[10]*z[17];
    z[12]=z[14] + n<T>(1,4)*z[17] + z[12];
    z[12]=z[8]*z[12];
    z[14]=static_cast<T>(9)+ z[10];
    z[14]=z[14]*z[24];
    z[17]=static_cast<T>(1)+ n<T>(31,6)*z[11];
    z[17]=z[11]*z[17];
    z[14]=n<T>(13,3)*z[17] + z[14];
    z[17]=static_cast<T>(19)+ n<T>(13,2)*z[10];
    z[17]=n<T>(1,3)*z[17] - n<T>(7,2)*z[6];
    z[17]=n<T>(1,4)*z[17] + n<T>(1,3)*z[29];
    z[17]=z[3]*z[17];
    z[14]=z[17] + n<T>(1,2)*z[14] + z[31];
    z[17]=n<T>(1,2)*z[7];
    z[23]= - n<T>(253,4) + z[16];
    z[23]=z[23]*z[17];
    z[31]= - n<T>(203,3) - 19*z[6];
    z[34]=static_cast<T>(5)- n<T>(13,8)*z[4];
    z[34]=z[4]*z[34];
    z[23]=z[23] + n<T>(1,8)*z[31] + z[34];
    z[31]=n<T>(79,6)*z[4];
    z[34]=n<T>(1,3)*z[4];
    z[38]=static_cast<T>(1)- z[34];
    z[38]=z[38]*z[31];
    z[37]=z[37] - n<T>(79,6);
    z[38]=z[32] + z[38] + z[37];
    z[38]=z[5]*z[38];
    z[23]=n<T>(1,3)*z[23] + z[38];
    z[23]=z[5]*z[23];
    z[38]=n<T>(95,4) - z[16];
    z[38]=z[38]*z[21];
    z[39]=static_cast<T>(11)- n<T>(83,3)*z[11];
    z[39]=n<T>(1,3)*z[39] - n<T>(25,2)*z[6];
    z[23]=z[23] + n<T>(1,16)*z[3] + n<T>(1,8)*z[39] + z[38];
    z[23]=z[5]*z[23];
    z[12]=z[12] + z[15] + n<T>(1,2)*z[14] + z[23];
    z[12]=z[8]*z[12];
    z[14]= - n<T>(721,6) + z[26];
    z[14]=z[14]*z[20];
    z[14]= - n<T>(67,3) + z[14];
    z[15]=n<T>(1,2) + z[2];
    z[15]=z[2]*z[15]*z[19];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[14]=z[14]*z[27];
    z[15]=static_cast<T>(647)- z[18];
    z[15]=z[11]*z[15];
    z[15]= - static_cast<T>(61)+ n<T>(1,4)*z[15];
    z[15]=z[15]*z[34];
    z[18]= - static_cast<T>(157)+ 161*z[11];
    z[23]=13*z[4];
    z[26]= - static_cast<T>(137)+ z[23];
    z[26]=n<T>(1,2)*z[26] + 13*z[2];
    z[26]=z[2]*z[26];
    z[15]=z[26] + n<T>(1,2)*z[18] + z[15];
    z[18]=static_cast<T>(19)- n<T>(25,8)*z[2];
    z[18]=z[18]*npow(z[2],2);
    z[26]=z[7]*npow(z[2],3);
    z[18]=z[18] - n<T>(47,6)*z[26];
    z[18]=z[7]*z[18];
    z[15]=n<T>(1,4)*z[15] + z[18];
    z[18]=n<T>(7,6) - z[2];
    z[18]=z[2]*z[18];
    z[18]=n<T>(1,6) + z[18];
    z[18]=z[3]*z[18]*z[20];
    z[15]=n<T>(1,3)*z[15] + z[18];
    z[15]=z[9]*z[15];
    z[13]=z[4]*z[13];
    z[18]= - n<T>(1207,6)*z[11] + n<T>(545,3) + n<T>(11,2)*z[10];
    z[13]=n<T>(1,2)*z[18] + z[13];
    z[18]=n<T>(23,4)*z[2];
    z[13]=n<T>(1,3)*z[13] - z[18];
    z[26]=z[20] - 1;
    z[38]=z[2]*z[26];
    z[38]=n<T>(1,2) + z[38];
    z[38]=z[38]*z[29];
    z[18]= - n<T>(25,3) + z[18];
    z[18]=z[2]*z[18];
    z[18]= - n<T>(1,4) + z[18];
    z[18]=n<T>(1,2)*z[18] + z[38];
    z[18]=z[18]*z[35];
    z[35]=n<T>(301,12) + z[6];
    z[35]=z[5]*z[35];
    z[13]=z[15] + n<T>(1,6)*z[35] + z[18] + n<T>(1,4)*z[13] + z[14];
    z[13]=z[9]*z[13];
    z[14]=n<T>(1,3)*z[1];
    z[15]=n<T>(7,4) + z[6];
    z[15]=z[15]*z[14];
    z[18]= - z[6] + z[2];
    z[18]=z[18]*z[14];
    z[35]=n<T>(1,3)*z[2];
    z[38]=n<T>(1,2) - z[35];
    z[38]=z[2]*z[38];
    z[18]=z[18] - n<T>(1,6)*z[6] + z[38];
    z[18]=z[3]*z[18];
    z[38]=static_cast<T>(71)- z[36];
    z[38]=n<T>(1,6)*z[38] - 7*z[2];
    z[15]=z[18] + n<T>(1,4)*z[38] + z[15];
    z[15]=z[3]*z[15];
    z[18]=static_cast<T>(13)+ n<T>(241,3)*z[11];
    z[38]= - static_cast<T>(9)+ z[10];
    z[38]=z[6]*z[38];
    z[18]=n<T>(1,3)*z[18] + z[38];
    z[38]= - static_cast<T>(1)- z[2];
    z[38]=z[38]*z[19];
    z[39]=n<T>(53,3) - 5*z[2];
    z[38]=n<T>(5,4)*z[39] + z[38];
    z[38]=z[38]*z[27];
    z[15]=z[15] + n<T>(1,8)*z[18] + z[38];
    z[18]= - n<T>(481,3) + z[30];
    z[38]= - n<T>(235,3) - z[23];
    z[38]=z[4]*z[38];
    z[38]=n<T>(679,4) + z[38];
    z[38]=z[4]*z[38];
    z[18]=n<T>(1,2)*z[18] + z[38];
    z[38]=n<T>(13,2)*z[4];
    z[39]=n<T>(137,3) + z[38];
    z[39]=z[39]*z[34];
    z[40]=n<T>(1,3)*z[6];
    z[39]=z[39] - n<T>(9,2) - z[40];
    z[39]=z[1]*z[39];
    z[18]=n<T>(1,6)*z[18] + z[39];
    z[39]=z[4] - 1;
    z[41]=z[4]*z[39];
    z[22]=n<T>(79,3)*z[41] - n<T>(79,3) + z[22];
    z[22]=z[1]*z[22];
    z[41]=npow(z[4],2);
    z[42]=static_cast<T>(2)- z[4];
    z[42]=z[42]*z[41];
    z[22]=n<T>(79,3)*z[42] + z[22];
    z[22]=z[22]*z[33];
    z[16]=z[16] - n<T>(149,4);
    z[16]=z[1]*z[16];
    z[16]=static_cast<T>(47)+ z[16];
    z[16]=z[16]*z[21];
    z[16]=z[22] + n<T>(1,2)*z[18] + z[16];
    z[16]=z[5]*z[16];
    z[18]= - static_cast<T>(49)- 23*z[6];
    z[18]=n<T>(1,2)*z[18] + z[23];
    z[18]=z[18]*z[14];
    z[21]= - static_cast<T>(103)- z[36];
    z[22]=n<T>(59,4) - n<T>(13,3)*z[4];
    z[22]=z[4]*z[22];
    z[18]=z[18] + n<T>(1,12)*z[21] + z[22];
    z[21]= - n<T>(10,9) + z[1];
    z[21]=z[7]*z[21];
    z[16]=z[16] + n<T>(1,4)*z[18] + z[21];
    z[16]=z[5]*z[16];
    z[12]=z[12] + z[13] + n<T>(1,2)*z[15] + z[16];
    z[12]=z[8]*z[12];
    z[13]=npow(z[4],3);
    z[15]=z[39]*z[13];
    z[16]= - static_cast<T>(1)- z[4];
    z[16]=z[16]*z[31];
    z[16]=z[16] + z[37];
    z[16]=z[1]*z[16];
    z[13]=n<T>(79,3)*z[13] + z[16];
    z[13]=z[1]*z[13];
    z[16]=npow(z[1],2);
    z[18]=z[16]*z[32];
    z[13]=z[18] - n<T>(79,6)*z[15] + z[13];
    z[13]=z[13]*z[33];
    z[15]= - n<T>(295,3) - z[38];
    z[15]=z[15]*z[34];
    z[15]=n<T>(123,4) + z[15];
    z[15]=z[15]*z[41];
    z[18]= - static_cast<T>(107)+ z[30];
    z[21]=n<T>(629,3) + z[23];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(107,4) + z[21];
    z[21]=z[4]*z[21];
    z[18]=n<T>(1,4)*z[18] + z[21];
    z[21]= - n<T>(167,3) - n<T>(13,4)*z[4];
    z[21]=z[4]*z[21];
    z[21]=n<T>(1,6)*z[21] - static_cast<T>(6)+ n<T>(11,24)*z[6];
    z[21]=z[1]*z[21];
    z[18]=n<T>(1,12)*z[18] + z[21];
    z[18]=z[1]*z[18];
    z[16]=z[16]*z[25];
    z[21]=static_cast<T>(3)+ n<T>(163,36)*z[1];
    z[21]=z[1]*z[21];
    z[16]=z[21] + z[16];
    z[16]=z[16]*z[17];
    z[13]=z[13] + z[16] + n<T>(1,4)*z[15] + z[18];
    z[13]=z[5]*z[13];
    z[15]=n<T>(1039,24) + z[23];
    z[15]=z[15]*z[34];
    z[16]= - n<T>(13,6)*z[4] - n<T>(319,36) - z[6];
    z[16]=z[1]*z[16];
    z[15]=z[16] + z[15] - n<T>(1139,72) + z[6];
    z[15]=z[1]*z[15];
    z[16]= - n<T>(401,12) - z[23];
    z[16]=z[4]*z[16];
    z[16]=n<T>(385,6) + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[16] + n<T>(91,3) + z[6];
    z[15]=n<T>(1,6)*z[16] + z[15];
    z[16]= - n<T>(1,2) + z[1];
    z[16]=z[1]*z[16]*z[19];
    z[17]=n<T>(223,2) - 17*z[1];
    z[17]=z[1]*z[17];
    z[17]= - n<T>(47,3) + n<T>(1,2)*z[17];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[16]=z[16]*z[27];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[16];
    z[13]=z[5]*z[13];
    z[15]=n<T>(7,2)*z[2];
    z[16]= - static_cast<T>(5)+ z[15];
    z[16]=z[16]*z[20];
    z[16]=z[16] - n<T>(1,4) + z[6];
    z[17]= - z[20] + n<T>(3,2) - z[6];
    z[18]=static_cast<T>(1)- z[6];
    z[18]=z[1]*z[18];
    z[17]=n<T>(5,4)*z[17] + z[18];
    z[17]=z[1]*z[17];
    z[16]=n<T>(1,6)*z[16] + z[17];
    z[17]=z[35] - 1;
    z[17]=z[17]*z[2];
    z[17]=z[17] + 1;
    z[18]=z[40] - z[17];
    z[21]=z[24] + z[26];
    z[14]=z[21]*z[14];
    z[14]=n<T>(1,4)*z[18] + z[14];
    z[14]=z[1]*z[14];
    z[17]=z[17]*z[2];
    z[17]=z[17] - n<T>(1,3);
    z[14]=n<T>(1,4)*z[17] + z[14];
    z[14]=z[3]*z[14];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[3]*z[14];
    z[16]=static_cast<T>(1)- n<T>(7,6)*z[2];
    z[16]=z[16]*z[20];
    z[16]=n<T>(1,3) + z[16];
    z[16]=z[2]*z[16];
    z[17]= - z[17]*z[29];
    z[16]=z[17] - n<T>(1,4) + z[16];
    z[16]=z[3]*z[16];
    z[17]=z[28]*z[2];
    z[18]=z[4]*z[10];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=n<T>(5,4)*z[18] + z[17];
    z[20]= - n<T>(1,4) + z[35];
    z[20]=z[2]*z[20];
    z[20]= - n<T>(1,12) + z[20];
    z[20]=z[20]*z[29];
    z[18]=n<T>(1,3)*z[18] + z[20];
    z[18]=z[9]*z[18];
    z[16]=z[18] - n<T>(1,4)*z[17] + z[16];
    z[16]=z[9]*z[16];
    z[17]=n<T>(49,2) - z[23];
    z[17]=z[4]*z[17];
    z[15]=z[15] + n<T>(137,3) + z[17];
    z[17]= - n<T>(13,2)*z[1] - n<T>(71,4) + z[23];
    z[17]=z[1]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[17]= - static_cast<T>(1)+ z[1];
    z[17]=z[17]*z[19];
    z[18]=n<T>(373,3) + z[1];
    z[17]=z[17] + n<T>(1,4)*z[18];
    z[17]=z[1]*z[17];
    z[17]= - n<T>(47,3) + z[17];
    z[17]=z[7]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];

    r += z[12] + z[13] + z[14] + n<T>(1,6)*z[15] + n<T>(1,4)*z[16];
 
    return r;
}

template double qqb_2lha_r1196(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1196(const std::array<dd_real,30>&);
#endif
