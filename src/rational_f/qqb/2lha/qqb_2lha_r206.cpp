#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r206(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[24];
    z[11]= - n<T>(61,3) + 15*z[7];
    z[11]=z[7]*z[11];
    z[11]=n<T>(17,3) + z[11];
    z[12]= - n<T>(13,12) + z[2];
    z[12]=z[2]*z[12];
    z[11]=n<T>(1,4)*z[11] + z[12];
    z[11]=z[3]*z[11];
    z[12]=n<T>(1,2)*z[5];
    z[13]=z[5]*z[6];
    z[14]=n<T>(3,2)*z[13];
    z[15]=z[14] + n<T>(3,2) - z[6];
    z[15]=z[15]*z[12];
    z[16]=5*z[7];
    z[17]= - z[16] + 5;
    z[17]=z[6]*z[17];
    z[11]=z[15] + z[11] - n<T>(3,4)*z[2] + n<T>(1,4) + z[17];
    z[15]=z[7] - 1;
    z[17]= - z[15]*z[16];
    z[18]=z[2] - 1;
    z[17]=z[17] + z[18];
    z[19]=n<T>(1,2)*z[2];
    z[20]=z[19] - 1;
    z[21]=z[20]*z[2];
    z[21]=z[21] + n<T>(1,2);
    z[22]=n<T>(1,3)*z[3];
    z[23]= - z[21]*z[22];
    z[17]=n<T>(1,8)*z[17] + z[23];
    z[17]=z[3]*z[17];
    z[23]=z[7]*z[6];
    z[24]=z[23] - z[6];
    z[25]= - z[24]*z[16];
    z[25]=z[13] + static_cast<T>(1)- z[25];
    z[17]=z[17] - n<T>(1,8)*z[25];
    z[17]=z[1]*z[17];
    z[11]=n<T>(1,2)*z[11] + z[17];
    z[11]=z[1]*z[11];
    z[17]=n<T>(1,6) - z[2];
    z[17]=z[17]*z[19];
    z[25]=n<T>(3,2)*z[2] + n<T>(1,2) - 13*z[6];
    z[26]= - n<T>(3,4)*z[13] - n<T>(3,4) + z[6];
    z[26]=z[5]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[12]=z[25]*z[12];
    z[25]=3*z[7];
    z[15]=z[3]*z[15];
    z[11]=z[11] + z[12] - n<T>(1,6)*z[15] + z[17] - n<T>(29,12) + z[25];
    z[12]=n<T>(1,2)*z[1];
    z[11]=z[11]*z[12];
    z[15]=n<T>(2,3)*z[9];
    z[17]=n<T>(3,8)*z[10];
    z[26]=z[15] + z[17];
    z[26]=z[2]*z[26];
    z[27]=n<T>(3,2)*z[10];
    z[28]= - static_cast<T>(1)- z[9];
    z[26]=z[26] + n<T>(2,3)*z[28] - z[27];
    z[26]=z[2]*z[26];
    z[28]=2*z[9];
    z[29]=z[28] - n<T>(1,8);
    z[30]=n<T>(3,4)*z[10];
    z[29]= - z[30] + n<T>(1,3)*z[29];
    z[31]=n<T>(1,3)*z[9];
    z[32]=z[31] - z[17];
    z[33]=z[2]*z[32];
    z[33]=z[33] - z[29];
    z[33]=z[2]*z[33];
    z[34]= - n<T>(1,4) + z[9];
    z[33]=z[33] + n<T>(1,3)*z[34] - z[17];
    z[33]=z[2]*z[33];
    z[33]=n<T>(1,24) + z[33];
    z[33]=z[3]*z[33];
    z[34]=n<T>(17,3) + n<T>(9,2)*z[10];
    z[26]=z[33] + n<T>(1,4)*z[34] + z[26];
    z[26]=z[2]*z[26];
    z[33]=n<T>(1,2)*z[6];
    z[23]=z[6] - n<T>(1,2)*z[23];
    z[23]=z[7]*z[23];
    z[34]=n<T>(1,2)*z[7];
    z[35]=static_cast<T>(1)- z[34];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(1,2) + z[35];
    z[35]=z[3]*z[35];
    z[23]=z[35] - z[33] + z[23];
    z[23]=z[1]*z[23];
    z[24]=z[13] + static_cast<T>(1)- z[24];
    z[20]=z[34] + z[20];
    z[20]=z[20]*z[22];
    z[20]=n<T>(5,2)*z[23] + z[20] + n<T>(15,4)*z[24];
    z[20]=z[1]*z[20];
    z[23]=n<T>(1,12)*z[3];
    z[21]= - z[21]*z[23];
    z[24]=z[13] + 1;
    z[35]=z[5]*z[24];
    z[20]=n<T>(1,4)*z[20] - n<T>(15,16)*z[35] + z[21] + n<T>(7,24)*z[2] - n<T>(2,3) + n<T>(3,8)*z[7];
    z[20]=z[1]*z[20];
    z[21]=z[2]*z[31];
    z[21]=z[21] - n<T>(1,3) + z[30];
    z[21]=z[2]*z[21];
    z[31]=z[13] + static_cast<T>(1)- z[6];
    z[35]=z[25]*z[10];
    z[31]=z[35] + n<T>(5,2)*z[31];
    z[36]=n<T>(1,8)*z[5];
    z[31]=z[31]*z[36];
    z[37]= - 3*z[10] + z[7];
    z[21]=z[31] + n<T>(3,8)*z[37] + z[21];
    z[21]=z[5]*z[21];
    z[20]=z[20] + z[21] - n<T>(3,4) + z[26];
    z[20]=z[4]*z[20];
    z[21]= - static_cast<T>(1)+ 11*z[6];
    z[26]=9*z[7];
    z[31]=z[10]*z[26];
    z[37]=n<T>(1,2)*z[13] + n<T>(1,2) - z[6];
    z[37]=z[5]*z[37];
    z[19]=z[37] - z[19] + n<T>(1,2)*z[21] + z[31];
    z[19]=z[19]*z[36];
    z[21]=n<T>(3,2) - 5*z[10];
    z[21]=z[26] + 3*z[21] - 5*z[6];
    z[26]=n<T>(1,12)*z[2] + z[17] + n<T>(1,16) - z[15];
    z[26]=z[2]*z[26];
    z[19]=z[19] + n<T>(1,8)*z[21] + z[26];
    z[19]=z[5]*z[19];
    z[21]=n<T>(25,8) + z[28];
    z[26]= - n<T>(4,3)*z[9] - z[30];
    z[26]=z[2]*z[26];
    z[21]=z[26] + n<T>(1,3)*z[21] + z[27];
    z[21]=z[2]*z[21];
    z[15]= - z[15] + z[30];
    z[15]=z[2]*z[15];
    z[15]=z[15] + z[29];
    z[26]=npow(z[2],2);
    z[15]=z[15]*z[26];
    z[15]=n<T>(1,24) + z[15];
    z[15]=z[3]*z[15];
    z[29]= - static_cast<T>(1)- z[34];
    z[11]=z[20] + z[11] + z[19] + z[15] + n<T>(3,4)*z[29] + z[21];
    z[11]=z[4]*z[11];
    z[15]=static_cast<T>(3)+ z[8];
    z[15]=z[15]*z[35];
    z[19]= - static_cast<T>(5)+ 3*z[8];
    z[19]=z[6]*z[19];
    z[19]=static_cast<T>(3)+ z[19];
    z[14]=z[14] + n<T>(1,2)*z[19] + z[15];
    z[14]=z[14]*z[36];
    z[15]=n<T>(1,4)*z[8];
    z[19]= - n<T>(1,4)*z[2] + z[15];
    z[20]=z[27] + 1;
    z[19]=z[20]*z[19];
    z[20]=z[8] + 1;
    z[21]=z[16]*z[20]*z[8];
    z[27]=n<T>(1,3)*z[8];
    z[29]= - static_cast<T>(3)- z[27];
    z[29]=z[8]*z[29];
    z[29]=z[21] + n<T>(43,3) + z[29];
    z[29]=z[7]*z[29];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[8]*z[15];
    z[15]=n<T>(7,4) + z[15];
    z[15]=z[6]*z[15];
    z[14]=z[14] + n<T>(1,16)*z[29] + n<T>(1,4)*z[15] + n<T>(1,16) + z[32] + z[19];
    z[14]=z[5]*z[14];
    z[15]= - n<T>(11,2) + z[8];
    z[15]=z[15]*z[33];
    z[19]=z[20]*z[7];
    z[20]=n<T>(35,2) + 13*z[8];
    z[20]=n<T>(1,3)*z[20] + n<T>(15,2)*z[19];
    z[20]=z[20]*z[34];
    z[15]=z[20] + z[15] - n<T>(1,3) - z[8];
    z[15]=n<T>(1,2)*z[15] + z[2];
    z[20]=z[2] + n<T>(5,8) - z[8];
    z[29]= - n<T>(15,16)*z[19] + n<T>(2,3) - n<T>(3,16)*z[8];
    z[29]=z[7]*z[29];
    z[23]= - z[8]*z[23];
    z[20]=z[23] + z[29] + n<T>(1,3)*z[20];
    z[20]=z[3]*z[20];
    z[23]= - static_cast<T>(1)+ n<T>(5,2)*z[7];
    z[23]=z[23]*z[25];
    z[23]=n<T>(19,6) + z[23];
    z[23]=n<T>(1,2)*z[23] - z[2];
    z[18]= - z[18]*z[22];
    z[18]=n<T>(1,2)*z[23] + z[18];
    z[18]=z[3]*z[18];
    z[16]= - z[16] - 1;
    z[16]=z[6]*z[16];
    z[16]=static_cast<T>(3)+ z[16];
    z[13]=n<T>(3,8)*z[13] + n<T>(1,8)*z[16] + z[18];
    z[12]=z[13]*z[12];
    z[13]=static_cast<T>(1)- n<T>(1,2)*z[8];
    z[13]=z[6]*z[13];
    z[13]=z[13] - z[24];
    z[13]=z[5]*z[13];
    z[12]=z[12] + n<T>(3,8)*z[13] + n<T>(1,2)*z[15] + z[20];
    z[12]=z[1]*z[12];
    z[13]=static_cast<T>(5)- n<T>(11,2)*z[8];
    z[13]=z[13]*z[27];
    z[13]= - z[21] - n<T>(9,2) + z[13];
    z[13]=z[7]*z[13];
    z[15]= - n<T>(1,2) + z[28];
    z[15]=n<T>(1,3)*z[15] + z[17];
    z[15]=z[2]*z[15];
    z[16]=z[32]*z[26];
    z[17]=n<T>(5,4)*z[19] - n<T>(1,12) + z[8];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,6) + z[17];
    z[17]=z[7]*z[17];
    z[17]= - z[8] + z[17];
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[3]*z[16];
    z[17]=n<T>(1,8) + z[8];
    z[18]=static_cast<T>(1)- z[8];
    z[18]=z[8]*z[18];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[6]*z[18];

    r += z[11] + z[12] + n<T>(1,8)*z[13] + z[14] + z[15] + z[16] + n<T>(1,3)*
      z[17] + n<T>(1,16)*z[18];
 
    return r;
}

template double qqb_2lha_r206(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r206(const std::array<dd_real,30>&);
#endif
