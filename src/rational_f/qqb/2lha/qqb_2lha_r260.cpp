#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r260(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[8];
    z[4]=k[29];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[12];
    z[9]=k[6];
    z[10]=k[7];
    z[11]=k[5];
    z[12]=k[11];
    z[13]=k[9];
    z[14]=k[10];
    z[15]=k[17];
    z[16]=z[7] + 1;
    z[17]=n<T>(1,2)*z[9];
    z[18]=z[16]*z[17];
    z[19]= - z[18] - static_cast<T>(5)- n<T>(13,2)*z[7];
    z[20]=n<T>(1,4)*z[9];
    z[19]=z[19]*z[20];
    z[21]=n<T>(3,2)*z[2];
    z[22]=n<T>(3,2)*z[7];
    z[19]=z[19] - z[22] - static_cast<T>(2)+ z[21];
    z[19]=z[1]*z[19];
    z[23]=n<T>(1,2)*z[7];
    z[24]=n<T>(1,2)*z[1];
    z[25]= - n<T>(17,4) + z[7];
    z[25]=n<T>(1,2)*z[25] - z[1];
    z[25]=z[6]*z[25];
    z[25]=z[25] + z[24] + static_cast<T>(5)- z[23];
    z[26]=n<T>(1,2)*z[6];
    z[25]=z[25]*z[26];
    z[27]= - z[7]*z[18];
    z[28]=npow(z[7],2);
    z[27]=z[27] + static_cast<T>(1)- 3*z[28];
    z[27]=z[27]*z[17];
    z[28]=n<T>(1,2) - z[2];
    z[28]=n<T>(1,2)*z[28] + z[7];
    z[29]= - n<T>(7,4) + z[6];
    z[29]=z[6]*z[29];
    z[27]=z[29] + 3*z[28] + z[27];
    z[28]=n<T>(1,2)*z[5];
    z[27]=z[27]*z[28];
    z[18]=z[18] + static_cast<T>(1)+ n<T>(9,2)*z[7];
    z[18]=z[7]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[17];
    z[29]= - n<T>(5,2) + z[2];
    z[29]=n<T>(1,4)*z[29] - z[7];
    z[18]=z[27] + z[25] + z[19] + 3*z[29] + z[18];
    z[18]=z[5]*z[18];
    z[19]=n<T>(5,4)*z[11];
    z[25]=z[19] + 1;
    z[27]=n<T>(1,8)*z[9];
    z[25]= - z[27]*z[25]*z[16];
    z[25]=z[25] - n<T>(17,8)*z[7] - static_cast<T>(1)+ n<T>(5,32)*z[11];
    z[25]=z[7]*z[25];
    z[29]=5*z[11];
    z[30]=static_cast<T>(1)+ z[29];
    z[25]=n<T>(1,16)*z[30] + z[25];
    z[25]=z[9]*z[25];
    z[16]=z[16]*z[20];
    z[22]=static_cast<T>(1)+ z[22];
    z[16]=3*z[22] + z[16];
    z[16]=z[9]*z[16];
    z[22]=3*z[2];
    z[30]=n<T>(13,2) - z[22];
    z[30]=z[30]*z[24];
    z[16]=z[30] + z[16] + static_cast<T>(5)- z[22];
    z[16]=z[16]*z[24];
    z[30]=n<T>(5,2)*z[11];
    z[31]=z[30] + 11;
    z[32]=n<T>(1,2)*z[31] + z[7];
    z[32]=z[7]*z[32];
    z[32]=z[32] - static_cast<T>(17)+ z[30];
    z[33]=static_cast<T>(1)- z[7];
    z[34]=z[1] + static_cast<T>(3)- z[23];
    z[34]=z[1]*z[34];
    z[33]=n<T>(5,2)*z[33] + z[34];
    z[33]=z[33]*z[26];
    z[34]=n<T>(5,4)*z[1] - n<T>(7,4) - z[7];
    z[34]=z[1]*z[34];
    z[32]=z[33] + n<T>(1,4)*z[32] + z[34];
    z[32]=z[32]*z[26];
    z[33]=n<T>(7,8) + z[2];
    z[33]=z[11]*z[33];
    z[33]=n<T>(15,2) + z[33];
    z[34]=n<T>(1,4)*z[11];
    z[35]=static_cast<T>(3)- z[34];
    z[35]=z[7]*z[35];
    z[33]=n<T>(1,2)*z[33] + z[35];
    z[16]=z[18] + z[32] + z[16] + n<T>(1,2)*z[33] + z[25];
    z[16]=z[5]*z[16];
    z[18]=z[2] + 1;
    z[25]=n<T>(1,2)*z[10];
    z[32]=z[18]*z[25];
    z[33]=3*z[9];
    z[32]= - z[33] + z[32] + static_cast<T>(17)+ z[22];
    z[35]= - static_cast<T>(3)+ z[2];
    z[35]=z[35]*z[24];
    z[35]=z[35] - n<T>(5,4) + z[22];
    z[35]=z[1]*z[35];
    z[32]=n<T>(1,4)*z[32] + z[35];
    z[32]=z[1]*z[32];
    z[35]=z[9] - 1;
    z[36]=n<T>(5,4)*z[9];
    z[37]=z[35]*z[36];
    z[38]= - n<T>(11,2) - z[1];
    z[38]=z[1]*z[38];
    z[37]=z[38] - static_cast<T>(7)+ z[37];
    z[37]=z[1]*z[37];
    z[38]=z[17] - 1;
    z[37]=5*z[38] + z[37];
    z[37]=z[37]*z[26];
    z[39]=n<T>(7,2) + z[2];
    z[39]=z[2]*z[39];
    z[32]=z[37] + z[32] - z[36] + static_cast<T>(1)+ n<T>(1,2)*z[39];
    z[37]=n<T>(3,4) - z[2];
    z[37]=z[1]*z[37];
    z[37]=z[37] + z[17] - n<T>(7,4) - z[22];
    z[37]=z[1]*z[37];
    z[39]=n<T>(7,4) - z[1];
    z[39]=z[39]*z[24];
    z[39]=static_cast<T>(3)+ z[39];
    z[39]=z[1]*z[39];
    z[40]=n<T>(5,2) + z[1];
    z[40]=z[40]*z[24];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[1]*z[40];
    z[40]=n<T>(1,4) + z[40];
    z[40]=z[40]*z[26];
    z[31]=z[40] + n<T>(1,8)*z[31] + z[39];
    z[31]=z[6]*z[31];
    z[39]=z[2] + n<T>(7,4);
    z[40]= - z[2]*z[39];
    z[40]= - n<T>(5,4) + z[40];
    z[40]=z[11]*z[40];
    z[21]=z[40] - static_cast<T>(5)- z[21];
    z[21]=z[31] + n<T>(1,4)*z[21] + z[37];
    z[18]=5*z[18] - z[9];
    z[18]=z[18]*z[24];
    z[18]=z[18] + n<T>(19,4) + z[22];
    z[22]= - n<T>(7,2) - 3*z[1];
    z[22]=z[22]*z[26];
    z[31]=n<T>(9,4) + z[1];
    z[22]=3*z[31] + z[22];
    z[22]=z[6]*z[22];
    z[22]=z[22] - n<T>(5,2) - z[2];
    z[31]= - static_cast<T>(1)+ z[26];
    z[31]=z[5]*z[6]*z[31];
    z[22]=n<T>(1,2)*z[22] + z[31];
    z[22]=z[22]*z[28];
    z[31]=static_cast<T>(1)+ z[1];
    z[31]=z[6]*z[31];
    z[31]=n<T>(1,8)*z[31] - n<T>(25,16) - 2*z[1];
    z[31]=z[6]*z[31];
    z[18]=z[22] + n<T>(1,4)*z[18] + z[31];
    z[18]=z[5]*z[18];
    z[18]=n<T>(1,2)*z[21] + z[18];
    z[18]=z[5]*z[18];
    z[18]=n<T>(1,4)*z[32] + z[18];
    z[18]=z[3]*z[18];
    z[19]=z[19] - 1;
    z[21]=z[7]*z[19];
    z[21]=z[21] + z[30] - static_cast<T>(5)- z[10];
    z[21]=z[7]*z[21];
    z[22]=static_cast<T>(1)+ z[23];
    z[22]=z[9]*z[22];
    z[19]=5*z[22] + z[21] + z[19];
    z[21]=n<T>(1,4)*z[8];
    z[22]=z[21] + 1;
    z[31]=3*z[22] - z[10];
    z[31]=z[31]*z[23];
    z[32]=z[38]*z[36];
    z[37]= - static_cast<T>(1)- 3*z[8];
    z[31]=z[32] + z[31] + n<T>(1,2)*z[37] + z[10];
    z[32]=z[23] - 1;
    z[32]=z[8]*z[32];
    z[32]=z[25] - n<T>(1,4) + z[32];
    z[32]=z[1]*z[32];
    z[31]=n<T>(1,2)*z[31] + z[32];
    z[31]=z[1]*z[31];
    z[32]=z[7]*z[8];
    z[37]= - static_cast<T>(7)+ 3*z[32];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[1]*z[32];
    z[32]=n<T>(1,4)*z[37] + z[32];
    z[32]=z[1]*z[32];
    z[37]=z[25] - 1;
    z[38]= - z[7]*z[37];
    z[32]=z[32] - static_cast<T>(1)+ z[38];
    z[32]=z[32]*z[24];
    z[38]=n<T>(1,4)*z[10];
    z[40]=static_cast<T>(1)- z[38];
    z[40]=z[7]*z[40];
    z[32]=z[32] - n<T>(1,8) + z[40];
    z[32]=z[6]*z[32];
    z[19]=z[32] + n<T>(1,4)*z[19] + z[31];
    z[19]=z[19]*z[26];
    z[29]=static_cast<T>(3)+ z[29];
    z[31]=static_cast<T>(1)+ z[34];
    z[31]=z[7]*z[31];
    z[29]=n<T>(1,2)*z[29] + 5*z[31];
    z[29]=z[29]*z[23];
    z[23]=z[23] + n<T>(1,2);
    z[23]=z[11]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[7]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[36];
    z[23]=z[23] - static_cast<T>(3)+ z[29];
    z[20]=z[23]*z[20];
    z[23]=z[10]*z[8];
    z[23]=static_cast<T>(3)+ n<T>(1,16)*z[23];
    z[23]=z[7]*z[23];
    z[29]= - static_cast<T>(1)- n<T>(5,4)*z[7];
    z[29]=z[9]*z[29];
    z[31]= - n<T>(3,2) + z[2];
    z[31]=z[1]*z[31];
    z[23]=n<T>(3,2)*z[31] + z[29] + z[23] - n<T>(1,16)*z[10] + n<T>(21,8) - z[8];
    z[23]=z[23]*z[24];
    z[29]=z[11]*z[39];
    z[31]=z[21]*z[10];
    z[32]=z[31] + z[11];
    z[32]=z[7]*z[32];
    z[29]=z[2] + z[29] - z[32];
    z[16]=z[18] + z[16] + z[19] + z[23] + z[20] - n<T>(1,32)*z[10] - z[22]
    - n<T>(1,8)*z[29];
    z[16]=z[4]*z[16];
    z[18]=z[8] - 3;
    z[19]= - z[21]*z[12]*z[18];
    z[20]=z[2]*z[12];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[25];
    z[20]= - z[12] + z[20];
    z[20]=z[20]*z[25];
    z[21]=n<T>(3,4)*z[8];
    z[18]= - z[18]*z[21];
    z[18]= - z[15] + z[18];
    z[18]=z[11]*z[18];
    z[22]= - static_cast<T>(1)+ z[13];
    z[22]=z[9]*z[22];
    z[22]= - z[13] + z[22];
    z[22]=z[9]*z[22];
    z[22]=z[22] + z[25] - n<T>(1,2)*z[14] - z[13];
    z[22]=z[9]*z[22];
    z[23]=z[9]*z[10];
    z[29]=npow(z[10],2);
    z[32]= - n<T>(1,2)*z[29] + z[23];
    z[39]=npow(z[9],2);
    z[32]=z[32]*z[39]*npow(z[1],2);
    z[40]=z[13]*z[15];
    z[18]=z[32] + z[22] + z[18] + z[20] + z[40] + z[19];
    z[19]=npow(z[9],3);
    z[20]=z[19]*z[24];
    z[20]=z[39] + z[20];
    z[20]=z[1]*z[20];
    z[22]= - z[1]*z[39];
    z[22]= - z[33] + z[22];
    z[22]=z[1]*z[22];
    z[22]=z[22] - static_cast<T>(3)- z[11];
    z[22]=z[6]*z[22];
    z[24]=n<T>(1,2)*z[11] + z[9];
    z[20]=n<T>(1,4)*z[22] + n<T>(1,2)*z[24] + z[20];
    z[20]=z[6]*z[20];
    z[22]=z[13] - z[14];
    z[19]=z[28]*z[19]*z[22];
    z[24]=z[9]*z[13];
    z[32]=n<T>(1,2)*z[22] - z[24];
    z[32]=z[32]*z[39];
    z[33]=npow(z[6],2)*z[34];
    z[32]=z[19] + z[32] + z[33];
    z[32]=z[5]*z[32];
    z[18]=z[32] + n<T>(1,2)*z[18] + z[20];
    z[18]=z[3]*z[18];
    z[20]= - static_cast<T>(1)+ z[29];
    z[32]= - n<T>(1,2) - z[10];
    z[32]=z[9]*z[32];
    z[20]=n<T>(1,2)*z[20] + z[32];
    z[20]=z[9]*z[20];
    z[32]= - static_cast<T>(1)+ z[10];
    z[20]=n<T>(1,2)*z[32] + z[20];
    z[20]=z[20]*z[1];
    z[32]=z[17] - n<T>(1,2);
    z[32]=z[13]*z[32];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[9]*z[32];
    z[20]=z[20] + z[32] - n<T>(1,2)*z[13] - n<T>(1,4)*z[14] + z[37];
    z[20]=z[9]*z[20];
    z[32]=z[13] - z[11];
    z[32]=z[15]*z[32];
    z[18]=z[18] + n<T>(1,2)*z[32] + z[20];
    z[20]= - z[8] + z[25];
    z[20]=z[1]*z[20];
    z[20]=z[20] - z[21] + z[37];
    z[20]=z[20]*z[26];
    z[21]=static_cast<T>(1)- n<T>(1,4)*z[12];
    z[21]=z[21]*z[38];
    z[25]= - z[9] - 1;
    z[25]=z[30]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[27];
    z[27]= - static_cast<T>(1)+ n<T>(1,8)*z[12];
    z[27]= - n<T>(3,16)*z[11] + n<T>(1,2)*z[27];
    z[27]=z[8]*z[27];
    z[30]=z[10] - z[9];
    z[30]=z[1]*z[30];
    z[20]=z[20] + n<T>(1,4)*z[30] + z[25] + z[21] + static_cast<T>(1)+ z[27];
    z[20]=z[6]*z[20];
    z[21]=static_cast<T>(1)+ z[22];
    z[21]=n<T>(1,2)*z[21] - z[24];
    z[21]=z[9]*z[21];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[9]*z[21];
    z[19]=z[21] + z[19];
    z[19]=z[19]*z[28];
    z[18]=z[19] + z[20] + n<T>(1,2)*z[18];
    z[18]=z[3]*z[18];
    z[19]= - static_cast<T>(1)- z[29];
    z[19]=n<T>(1,2)*z[19] + z[23];
    z[19]=z[19]*z[17];
    z[17]= - z[35]*z[17];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] - static_cast<T>(3)+ z[19];
    z[17]=z[9]*z[17];
    z[19]=z[12]*z[31];
    z[20]= - z[11]*z[36];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[9]*z[20];
    z[19]=z[19] + z[20];
    z[19]=z[19]*z[26];
    z[17]=z[19] + z[17];
    z[17]=n<T>(1,2)*z[17] + z[18];

    r += z[16] + n<T>(1,2)*z[17];
 
    return r;
}

template double qqb_2lha_r260(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r260(const std::array<dd_real,30>&);
#endif
