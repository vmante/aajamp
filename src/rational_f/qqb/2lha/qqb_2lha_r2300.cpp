#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2300(const std::array<T,30>& k) {
  T z[53];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[17];
    z[7]=k[20];
    z[8]=k[6];
    z[9]=k[2];
    z[10]=k[3];
    z[11]=k[15];
    z[12]=n<T>(3,2)*z[3];
    z[13]=n<T>(3,2)*z[7];
    z[14]=z[13] - 1;
    z[15]=z[14]*z[7];
    z[16]=z[12] - z[15];
    z[17]=n<T>(1,2)*z[6];
    z[18]=3*z[6];
    z[19]= - static_cast<T>(11)+ z[18];
    z[19]=z[19]*z[17];
    z[20]=z[18] + n<T>(5,2);
    z[20]=z[20]*z[6];
    z[21]=z[20] + z[16];
    z[21]=z[2]*z[21];
    z[16]=z[21] + z[19] - static_cast<T>(1)- z[16];
    z[19]=n<T>(1,4)*z[2];
    z[16]=z[16]*z[19];
    z[21]=n<T>(1,2)*z[7];
    z[22]= - static_cast<T>(19)+ 15*z[7];
    z[22]=z[22]*z[21];
    z[23]= - static_cast<T>(1)+ n<T>(3,2)*z[6];
    z[24]=z[23]*z[6];
    z[22]=z[22] - z[24] + static_cast<T>(7)- z[12];
    z[25]=3*z[7];
    z[26]=n<T>(13,2) - z[25];
    z[26]=z[26]*z[21];
    z[27]=n<T>(1,4)*z[3];
    z[28]=z[27] - 1;
    z[26]=3*z[28] + z[26];
    z[29]=n<T>(3,4)*z[7];
    z[30]=z[29] + z[27];
    z[31]=z[30] - 1;
    z[32]= - z[8]*z[31];
    z[26]=n<T>(1,2)*z[26] + z[32];
    z[32]=3*z[8];
    z[26]=z[26]*z[32];
    z[16]=z[16] + n<T>(1,4)*z[22] + z[26];
    z[16]=z[4]*z[16];
    z[22]=z[18] + 1;
    z[26]=z[22]*z[17];
    z[33]=z[25] + 1;
    z[34]=z[33]*z[7];
    z[35]=z[34] + z[5];
    z[36]=n<T>(1,2)*z[3];
    z[26]=z[36] + z[26] - z[35];
    z[37]=n<T>(1,2)*z[2];
    z[38]= - z[26]*z[37];
    z[39]=z[18] - 1;
    z[40]=n<T>(1,4)*z[6];
    z[41]= - z[39]*z[40];
    z[42]= - static_cast<T>(1)+ n<T>(15,2)*z[7];
    z[42]=z[42]*z[21];
    z[38]=z[38] + z[42] + z[41] + z[5] + z[27];
    z[38]=z[38]*z[37];
    z[41]=9*z[6];
    z[42]= - static_cast<T>(5)+ z[41];
    z[42]=z[42]*z[17];
    z[43]=9*z[7];
    z[44]=static_cast<T>(13)- z[43];
    z[44]=z[44]*z[21];
    z[42]=z[44] + static_cast<T>(1)+ z[42];
    z[44]= - n<T>(19,4) + z[25];
    z[44]=z[44]*z[25];
    z[44]=z[44] + static_cast<T>(7)- n<T>(9,4)*z[3];
    z[45]=z[13] + z[36];
    z[46]= - static_cast<T>(2)+ z[45];
    z[46]=z[46]*z[32];
    z[44]=n<T>(1,2)*z[44] + z[46];
    z[44]=z[8]*z[44];
    z[16]=z[16] + z[38] + n<T>(1,4)*z[42] + z[44];
    z[16]=z[4]*z[16];
    z[22]=z[22]*z[6];
    z[38]= - z[35] + z[22] + z[3];
    z[38]=z[38]*z[37];
    z[42]=3*z[3];
    z[44]= - z[5] - z[42];
    z[46]=n<T>(1,2) - z[25];
    z[46]=z[7]*z[46];
    z[20]=z[38] + z[46] + n<T>(1,2)*z[44] - z[20];
    z[20]=z[20]*z[37];
    z[38]=5*z[3] - z[18] - z[5];
    z[44]=n<T>(23,4) - z[25];
    z[44]=z[7]*z[44];
    z[46]=z[25] + z[3];
    z[47]= - z[8]*z[46];
    z[38]=z[47] + n<T>(1,4)*z[38] + z[44];
    z[38]=z[8]*z[38];
    z[44]=z[18] - z[3];
    z[47]=z[5] + z[44];
    z[48]= - n<T>(11,4) + z[25];
    z[48]=z[7]*z[48];
    z[20]=z[20] + z[38] + n<T>(1,4)*z[47] + z[48];
    z[20]=z[4]*z[20];
    z[38]=z[3] + z[5];
    z[47]= - z[25] - z[18] - z[38];
    z[47]=z[8]*z[47];
    z[48]=z[25] - 5;
    z[49]=z[48]*z[7];
    z[50]=static_cast<T>(5)- z[18];
    z[50]=z[6]*z[50];
    z[47]=z[47] - z[49] + z[50] + z[38];
    z[50]=n<T>(1,2)*z[8];
    z[47]=z[47]*z[50];
    z[34]=z[38] + z[22] + z[34];
    z[51]= - z[34]*z[37];
    z[24]=z[51] + z[47] + z[24] + z[15];
    z[24]=z[4]*z[24];
    z[24]=n<T>(1,2)*z[34] + z[24];
    z[24]=z[1]*z[24];
    z[34]=z[46]*z[50];
    z[38]= - n<T>(1,2)*z[38] - z[6];
    z[47]= - static_cast<T>(2)+ z[13];
    z[47]=z[7]*z[47];
    z[34]=z[34] + n<T>(1,2)*z[38] + z[47];
    z[34]=z[8]*z[34];
    z[35]=z[35]*z[37];
    z[38]=z[3] + 3*z[5];
    z[47]=5*z[7] + 7*z[6] + z[38];
    z[20]=n<T>(1,4)*z[24] + n<T>(1,2)*z[20] + z[35] + n<T>(1,8)*z[47] + z[34];
    z[20]=z[4]*z[20];
    z[24]= - z[33]*z[25];
    z[22]=z[24] - z[22] - z[38];
    z[20]=n<T>(1,8)*z[22] + z[20];
    z[20]=z[1]*z[20];
    z[22]=z[26]*z[19];
    z[24]= - static_cast<T>(1)- z[9];
    z[24]=n<T>(1,2)*z[24] + z[5];
    z[24]=z[24]*z[27];
    z[26]=n<T>(1,2)*z[9];
    z[33]=z[26] + 1;
    z[33]=z[33]*z[18];
    z[34]= - static_cast<T>(1)+ z[9];
    z[34]=n<T>(1,2)*z[34] - z[33];
    z[34]=z[34]*z[40];
    z[35]= - n<T>(1,2) + z[10];
    z[35]=z[35]*z[25];
    z[38]= - static_cast<T>(5)+ z[10];
    z[35]=n<T>(1,2)*z[38] + z[35];
    z[35]=z[35]*z[21];
    z[31]= - z[31]*z[32];
    z[38]=static_cast<T>(1)- z[7];
    z[38]=z[7]*z[38];
    z[31]=z[31] + static_cast<T>(1)+ n<T>(9,4)*z[38];
    z[31]=z[8]*z[31];
    z[38]=n<T>(1,2)*z[10];
    z[47]=z[38]*z[5];
    z[51]=npow(z[11],2);
    z[52]=n<T>(3,2) + z[51];
    z[16]=z[20] + z[16] + z[22] + z[31] + z[35] + z[47] + z[34] + z[24]
    + n<T>(1,2)*z[52] - z[5];
    z[16]=z[1]*z[16];
    z[20]=2*z[10];
    z[22]=z[20] + z[45] - 4;
    z[24]=z[22]*z[8];
    z[31]=n<T>(29,4) - z[25];
    z[31]=z[31]*z[21];
    z[34]= - static_cast<T>(13)+ n<T>(7,4)*z[3];
    z[31]= - z[24] + z[31] + n<T>(1,2)*z[34] + z[20];
    z[31]=z[8]*z[31];
    z[34]= - n<T>(1,2) + z[18];
    z[34]=z[34]*z[17];
    z[35]=z[48]*z[21];
    z[48]=static_cast<T>(7)+ z[18];
    z[48]=z[6]*z[48];
    z[48]=z[42] + z[48];
    z[48]=z[48]*z[19];
    z[34]=z[48] + z[35] + z[34] - static_cast<T>(1)- z[27];
    z[34]=z[34]*z[37];
    z[12]=static_cast<T>(11)- z[12];
    z[35]= - static_cast<T>(1)+ n<T>(3,8)*z[6];
    z[35]=z[6]*z[35];
    z[12]=z[34] + z[31] + n<T>(1,8)*z[49] - z[38] + n<T>(1,4)*z[12] + z[35];
    z[12]=z[4]*z[12];
    z[14]=z[14]*z[21];
    z[31]=n<T>(3,4)*z[6];
    z[34]=z[31] + 1;
    z[34]=z[34]*z[6];
    z[14]=z[14] - z[36] - z[34];
    z[14]=z[2]*z[14];
    z[35]=static_cast<T>(1)+ z[3];
    z[39]= - z[6]*z[39];
    z[14]=z[14] + z[21] + n<T>(1,2)*z[35] + z[39];
    z[14]=z[14]*z[37];
    z[35]=static_cast<T>(23)- z[41];
    z[35]=z[35]*z[40];
    z[39]= - static_cast<T>(1)+ z[29];
    z[39]=z[39]*z[25];
    z[35]=z[39] + z[35] - static_cast<T>(5)+ z[27];
    z[39]= - n<T>(11,2) + z[25];
    z[39]=z[39]*z[25];
    z[39]=z[39] - 5*z[10] + n<T>(49,2) - z[42];
    z[22]=z[22]*z[32];
    z[22]=n<T>(1,2)*z[39] + z[22];
    z[22]=z[8]*z[22];
    z[12]=z[12] + z[14] + n<T>(1,2)*z[35] + z[22];
    z[12]=z[4]*z[12];
    z[14]=z[36]*z[5];
    z[22]=z[11] + 1;
    z[22]=z[22]*z[11];
    z[14]=z[14] + z[22];
    z[35]=z[5] - 1;
    z[34]=z[34] + z[35] - z[14];
    z[39]=3*z[10];
    z[40]= - static_cast<T>(3)+ z[3];
    z[40]=n<T>(1,2)*z[40] + z[39];
    z[15]=z[24] + n<T>(1,2)*z[40] + z[15];
    z[15]=z[8]*z[15];
    z[24]=n<T>(1,4)*z[5];
    z[40]=n<T>(1,2) + z[11];
    z[40]=z[11]*z[40];
    z[40]=z[40] - z[24];
    z[40]=z[10]*z[40];
    z[41]=z[10] - n<T>(3,2);
    z[42]= - z[41]*z[25];
    z[42]=z[10] + z[42];
    z[48]=n<T>(1,4)*z[7];
    z[42]=z[42]*z[48];
    z[15]=z[15] + z[42] + n<T>(1,2)*z[34] + z[40];
    z[15]=z[2]*z[15];
    z[26]= - z[26] + z[35];
    z[26]=z[26]*z[36];
    z[34]= - static_cast<T>(15)- 7*z[9];
    z[34]=n<T>(1,2)*z[34] + z[33];
    z[17]=z[34]*z[17];
    z[34]= - n<T>(1,2) + z[11];
    z[34]=z[11]*z[34];
    z[17]=z[17] + z[26] - n<T>(7,4)*z[5] + z[34] + static_cast<T>(3)+ z[9];
    z[26]= - static_cast<T>(7)+ z[36];
    z[26]=n<T>(1,2)*z[26] - z[39];
    z[34]=n<T>(25,8) - z[25];
    z[34]=z[7]*z[34];
    z[40]= - 4*z[10] + static_cast<T>(8)- z[46];
    z[40]=z[8]*z[40];
    z[26]=z[40] + n<T>(1,2)*z[26] + z[34];
    z[26]=z[8]*z[26];
    z[34]=z[11] - 1;
    z[34]=z[34]*z[11];
    z[40]=z[34] + 3;
    z[42]= - n<T>(3,8)*z[10] + 1;
    z[42]=z[5]*z[42];
    z[42]= - n<T>(1,2)*z[40] + z[42];
    z[42]=z[10]*z[42];
    z[39]=static_cast<T>(5)- z[39];
    z[39]=z[10]*z[39];
    z[39]= - static_cast<T>(1)+ n<T>(1,8)*z[39];
    z[39]=z[39]*z[25];
    z[46]=static_cast<T>(1)+ n<T>(1,4)*z[10];
    z[46]=z[10]*z[46];
    z[46]=n<T>(5,2) + z[46];
    z[39]=n<T>(1,2)*z[46] + z[39];
    z[39]=z[7]*z[39];
    z[12]=z[16] + z[12] + z[15] + z[26] + z[39] + n<T>(1,2)*z[17] + z[42];
    z[12]=z[1]*z[12];
    z[15]= - z[41]*z[38];
    z[16]= - n<T>(5,2) + z[10];
    z[16]=z[10]*z[16];
    z[16]=n<T>(5,2) + z[16];
    z[13]=z[16]*z[13];
    z[13]=z[13] - static_cast<T>(1)+ z[15];
    z[13]=z[7]*z[13];
    z[15]= - z[35]*z[28];
    z[16]=n<T>(1,2)*z[5];
    z[17]= - z[51] + z[16];
    z[17]=z[10]*z[17];
    z[17]=z[17] - n<T>(5,4)*z[5] + static_cast<T>(1)+ n<T>(3,2)*z[51];
    z[17]=z[10]*z[17];
    z[13]=z[13] + z[17] + z[31] - n<T>(1,2)*z[51] + z[15];
    z[15]=z[10] - 3;
    z[17]=z[15]*z[10];
    z[26]= - z[17] + z[30] - 3;
    z[30]=z[26]*z[8];
    z[31]=z[25] - 1;
    z[35]= - z[31]*z[48];
    z[39]=static_cast<T>(1)- z[36];
    z[41]=z[38] - 1;
    z[42]=z[10]*z[41];
    z[35]= - z[30] + z[35] + n<T>(1,2)*z[39] + z[42];
    z[35]=z[8]*z[35];
    z[31]=z[21]*z[41]*z[31];
    z[14]= - z[5] + z[14];
    z[22]=z[22]*z[41];
    z[22]=z[24] + z[22];
    z[22]=z[10]*z[22];
    z[14]=z[35] + z[31] + n<T>(1,2)*z[14] + z[22];
    z[14]=z[14]*z[37];
    z[22]= - n<T>(1,2) + z[7];
    z[22]=z[22]*z[29];
    z[24]=n<T>(7,4) - z[10];
    z[24]=z[10]*z[24];
    z[22]=z[30] + z[22] + n<T>(1,2)*z[28] + z[24];
    z[22]=z[8]*z[22];
    z[13]=z[14] + n<T>(1,2)*z[13] + z[22];
    z[13]=z[2]*z[13];
    z[14]= - z[15]*z[20];
    z[14]=z[14] - static_cast<T>(6)+ z[45];
    z[14]=z[8]*z[14];
    z[15]= - n<T>(19,2) + z[10];
    z[15]=z[15]*z[38];
    z[20]= - n<T>(25,4) + z[25];
    z[20]=z[20]*z[21];
    z[14]=z[14] + z[20] + z[15] + static_cast<T>(8)- n<T>(5,8)*z[3];
    z[14]=z[8]*z[14];
    z[15]=z[18] + z[3];
    z[15]=z[15]*z[19];
    z[19]=static_cast<T>(3)+ z[36];
    z[18]= - z[15] + n<T>(1,4)*z[49] + n<T>(1,2)*z[19] - z[18];
    z[18]=z[18]*z[37];
    z[19]= - z[26]*z[50];
    z[20]= - static_cast<T>(1)+ n<T>(3,8)*z[7];
    z[20]=z[20]*z[7];
    z[22]=static_cast<T>(7)- z[10];
    z[22]=z[10]*z[22];
    z[22]=z[22] - static_cast<T>(11)+ z[3];
    z[19]=z[19] + n<T>(1,4)*z[22] - z[20];
    z[19]=z[8]*z[19];
    z[15]=z[15] + z[23];
    z[15]=z[15]*z[37];
    z[22]=n<T>(1,4)*z[44] - z[10];
    z[15]=z[15] + z[19] + n<T>(1,2)*z[22] - z[20];
    z[15]=z[4]*z[15];
    z[19]= - static_cast<T>(19)+ z[43];
    z[19]=z[19]*z[21];
    z[19]=z[19] + n<T>(3,2)*z[10] - n<T>(9,4)*z[6] + static_cast<T>(3)+ z[27];
    z[14]=z[15] + z[18] + n<T>(1,2)*z[19] + z[14];
    z[14]=z[4]*z[14];
    z[15]=n<T>(3,2)*z[5];
    z[18]=z[47] - z[15] + z[40];
    z[18]=z[10]*z[18];
    z[15]=z[18] + z[15] - z[34] - static_cast<T>(5)- z[9];
    z[15]=z[10]*z[15];
    z[17]=static_cast<T>(5)+ z[17];
    z[17]=z[10]*z[17];
    z[17]= - static_cast<T>(7)+ z[17];
    z[17]=z[17]*z[25];
    z[18]=static_cast<T>(1)- z[10];
    z[18]=z[10]*z[18];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[10]*z[18];
    z[17]=z[17] + static_cast<T>(21)+ z[18];
    z[17]=z[17]*z[21];
    z[15]=z[17] + z[15] + z[33] - z[16] - n<T>(5,2) - z[9];
    z[16]=static_cast<T>(11)- z[43];
    z[16]=z[16]*z[21];
    z[17]=npow(z[10],2);
    z[16]=z[16] + 3*z[17] - static_cast<T>(9)+ z[36];
    z[17]= - z[26]*z[32];
    z[16]=n<T>(1,2)*z[16] + z[17];
    z[16]=z[8]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];

    r += z[12] + z[13] + z[14] + n<T>(1,2)*z[15];
 
    return r;
}

template double qqb_2lha_r2300(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2300(const std::array<dd_real,30>&);
#endif
