#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2141(const std::array<T,30>& k) {
  T z[30];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[20];
    z[5]=k[6];
    z[6]=k[7];
    z[7]=k[17];
    z[8]=k[2];
    z[9]=k[4];
    z[10]=k[11];
    z[11]=k[15];
    z[12]=n<T>(1,2)*z[2];
    z[13]= - z[11]*z[12];
    z[13]=z[13] + 1;
    z[14]= - static_cast<T>(1)+ n<T>(3,2)*z[11];
    z[13]=z[14]*z[13];
    z[15]=n<T>(1,2)*z[8];
    z[16]=z[15] - 1;
    z[17]=n<T>(1,2)*z[6];
    z[16]=z[16]*z[17];
    z[18]= - z[8]*z[16];
    z[19]=z[15] - 3;
    z[20]= - z[8]*z[19];
    z[21]=n<T>(3,2)*z[7];
    z[22]= - npow(z[8],2)*z[21];
    z[20]=z[20] + z[22];
    z[22]=n<T>(1,2)*z[7];
    z[20]=z[20]*z[22];
    z[13]=z[20] + z[18] + z[13];
    z[13]=z[10]*z[13];
    z[18]=3*z[7];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[18];
    z[15]=z[15] + z[19];
    z[15]=z[15]*z[22];
    z[19]=z[18] + z[5];
    z[20]=n<T>(1,2)*z[3];
    z[23]=z[19]*z[20];
    z[24]=3*z[11];
    z[25]=z[24] + 1;
    z[25]=z[25]*z[11];
    z[25]=z[25] + z[5];
    z[26]=z[2]*z[25];
    z[13]=z[13] + z[23] + z[15] + n<T>(1,4)*z[26] + z[16] - z[11];
    z[13]=z[10]*z[13];
    z[15]=z[24] + z[6];
    z[16]=n<T>(1,2)*z[5];
    z[23]=z[21] + z[16] + z[15];
    z[23]=z[3]*z[23];
    z[26]=z[25]*z[12];
    z[27]= - static_cast<T>(1)+ z[8];
    z[27]=z[6]*z[27];
    z[24]= - n<T>(7,2) + z[24];
    z[24]=z[11]*z[24];
    z[23]=z[23] + z[26] + z[24] + z[27] - z[16];
    z[24]=n<T>(3,4)*z[7];
    z[26]=n<T>(1,2) + z[8];
    z[26]=z[26]*z[24];
    z[26]=z[26] - static_cast<T>(1)+ n<T>(1,4)*z[8];
    z[26]=z[7]*z[26];
    z[23]=z[26] + n<T>(1,4)*z[23];
    z[23]=z[10]*z[23];
    z[26]=z[18] + 1;
    z[27]= - z[26]*z[22];
    z[28]= - static_cast<T>(5)+ 9*z[7];
    z[28]=z[7]*z[28];
    z[29]=z[3]*z[19];
    z[28]=3*z[29] + z[28] - z[11] - z[6] + z[5];
    z[28]=z[28]*z[20];
    z[27]=z[28] + z[27] - z[17] - z[25];
    z[23]=n<T>(1,4)*z[27] + z[23];
    z[23]=z[10]*z[23];
    z[15]=z[19] + z[15];
    z[15]=z[15]*z[20];
    z[14]=z[11]*z[14];
    z[19]= - static_cast<T>(1)+ z[21];
    z[19]=z[7]*z[19];
    z[14]=z[15] + z[14] + z[19];
    z[14]=z[3]*z[14];
    z[15]= - z[7]*z[26];
    z[15]=z[15] - z[6] - z[25];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[9]*z[14]*npow(z[10],2);
    z[14]=z[23] + n<T>(1,4)*z[14];
    z[14]=z[9]*z[14];
    z[15]= - n<T>(1,2) + z[7];
    z[15]=z[15]*z[18];
    z[15]=z[15] + z[2] - static_cast<T>(1)+ z[16];
    z[16]= - static_cast<T>(1)+ n<T>(1,4)*z[5];
    z[19]=z[24] + z[16];
    z[20]=z[12] + z[19];
    z[20]=z[3]*z[20];
    z[15]=n<T>(1,4)*z[15] + z[20];
    z[15]=z[3]*z[15];
    z[20]=z[4] + 1;
    z[21]=z[20]*z[2];
    z[23]=z[21] - z[20];
    z[23]=z[4]*z[23];
    z[24]= - z[5]*z[17];
    z[23]= - z[11] + z[24] + z[23];
    z[13]=z[14] + n<T>(1,2)*z[13] + n<T>(1,4)*z[23] + z[15];
    z[13]=z[9]*z[13];
    z[14]=npow(z[4],2);
    z[14]=z[14] - z[17];
    z[15]=z[1] + 1;
    z[14]= - z[14]*z[15];
    z[17]= - z[17]*z[15];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[5]*z[17];
    z[20]= - z[1]*z[20];
    z[20]=z[21] - static_cast<T>(1)+ z[20];
    z[20]=z[2]*z[4]*z[20];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[1]*z[18];
    z[18]=static_cast<T>(3)+ z[18];
    z[21]=z[18]*z[22];
    z[14]=z[21] + z[20] + z[17] - static_cast<T>(1)+ z[14];
    z[15]=z[15]*z[16];
    z[16]=z[2] - z[1];
    z[17]=static_cast<T>(1)+ z[16];
    z[12]=z[17]*z[12];
    z[17]=z[1]*z[19];
    z[16]=z[2]*z[16];
    z[16]=z[16] + z[17];
    z[16]=z[3]*z[16];
    z[17]=z[7]*z[18];
    z[12]=z[16] + n<T>(1,4)*z[17] + z[12] + z[15];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,2)*z[14] + z[12];

    r += n<T>(1,2)*z[12] + z[13];
 
    return r;
}

template double qqb_2lha_r2141(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2141(const std::array<dd_real,30>&);
#endif
