#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1018(const std::array<T,30>& k) {
  T z[77];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[11];
    z[8]=k[9];
    z[9]=k[3];
    z[10]=k[5];
    z[11]=k[8];
    z[12]=k[10];
    z[13]=k[18];
    z[14]=k[27];
    z[15]=k[13];
    z[16]=k[24];
    z[17]=z[5] - 3;
    z[18]=n<T>(1,4)*z[5];
    z[19]=z[17]*z[18];
    z[19]=z[19] - 1;
    z[19]=z[19]*z[5];
    z[20]=z[5] + 1;
    z[21]=npow(z[5],2);
    z[22]=z[20]*z[21];
    z[23]=n<T>(1,2)*z[10];
    z[24]=z[22]*z[23];
    z[19]=z[19] + z[24];
    z[19]=z[19]*z[10];
    z[24]=n<T>(1,2)*z[5];
    z[25]=z[24] + 1;
    z[26]=z[25]*z[5];
    z[27]=z[26] + n<T>(1,2);
    z[28]=3*z[27];
    z[29]=z[28]*z[21];
    z[30]=npow(z[5],3);
    z[31]=z[10]*z[27]*z[30];
    z[29]=z[29] - z[31];
    z[29]=z[29]*z[23];
    z[31]=n<T>(1,2)*z[9];
    z[32]=z[5] - z[31];
    z[32]=z[27]*z[32];
    z[29]=z[29] - z[32];
    z[32]=z[29]*z[16];
    z[33]=z[5] - 1;
    z[34]=z[33]*z[24];
    z[34]=z[34] - 1;
    z[35]=n<T>(1,4)*z[20];
    z[36]=z[35]*z[9];
    z[19]= - z[32] + z[19] + z[36] - n<T>(1,2)*z[34];
    z[32]=z[16]*z[19];
    z[34]=z[5] - n<T>(1,2);
    z[36]= - z[5]*z[34];
    z[36]= - n<T>(1,2) + z[36];
    z[36]=z[10]*z[36];
    z[36]=n<T>(1,4)*z[33] + z[36];
    z[36]=z[10]*z[36];
    z[37]= - static_cast<T>(1)+ n<T>(3,4)*z[5];
    z[38]=npow(z[10],2);
    z[39]=z[38]*z[2];
    z[40]=z[37]*z[39];
    z[36]=z[36] + z[40];
    z[36]=z[2]*z[36];
    z[40]= - z[21]*z[23];
    z[41]=static_cast<T>(1)+ z[18];
    z[41]=z[5]*z[41];
    z[40]=z[41] + z[40];
    z[40]=z[10]*z[40];
    z[41]=z[10] + n<T>(1,2);
    z[41]=z[41]*z[10];
    z[42]=z[41] + n<T>(5,2)*z[39];
    z[42]=z[2]*z[42];
    z[43]=n<T>(1,2)*z[38];
    z[44]= - z[8]*npow(z[2],2)*z[43];
    z[42]=z[42] + z[44];
    z[44]=n<T>(1,2)*z[8];
    z[42]=z[42]*z[44];
    z[32]=z[32] + z[42] + z[36] - n<T>(1,2)*z[25] + z[40];
    z[32]=z[12]*z[32];
    z[29]= - z[4]*z[29];
    z[19]=z[29] + z[19];
    z[19]=z[16]*z[4]*z[19];
    z[29]=z[10] + n<T>(3,2);
    z[29]=z[29]*z[10];
    z[36]=z[43]*z[9];
    z[40]=z[36] - z[29] - z[39];
    z[40]=z[9]*z[40];
    z[42]=n<T>(1,2)*z[2];
    z[45]= - z[10] - 3*z[39];
    z[45]=z[45]*z[42];
    z[40]=z[40] + static_cast<T>(1)+ z[45];
    z[31]=z[40]*z[31];
    z[40]=static_cast<T>(3)- 7*z[10];
    z[45]=n<T>(3,4) - z[10];
    z[45]=z[2]*z[10]*z[45];
    z[40]=n<T>(1,4)*z[40] + z[45];
    z[40]=z[2]*z[40];
    z[45]=n<T>(1,2)*z[39];
    z[46]= - z[10] - z[45];
    z[46]=z[2]*z[46];
    z[46]= - n<T>(1,2) + z[46];
    z[46]=z[46]*z[44];
    z[47]=9*z[10] + 5*z[39];
    z[47]=z[2]*z[47];
    z[46]=z[46] + static_cast<T>(1)+ n<T>(1,4)*z[47];
    z[46]=z[8]*z[46];
    z[31]=z[46] + z[31] - n<T>(3,4) + z[40];
    z[31]=z[11]*z[31];
    z[38]=z[38]*z[9];
    z[40]=n<T>(7,2)*z[5];
    z[46]= - static_cast<T>(3)+ z[40];
    z[46]=z[10]*z[46];
    z[46]= - n<T>(13,4) + z[46];
    z[46]=z[10]*z[46];
    z[45]=n<T>(7,4)*z[38] + z[46] - z[45];
    z[45]=z[9]*z[45];
    z[19]=z[45] + z[19] + z[32] + z[31];
    z[31]=z[5] + 3;
    z[32]= - z[31]*z[24];
    z[45]=z[3]*z[5];
    z[46]=z[20]*z[45];
    z[47]=static_cast<T>(2)+ z[5];
    z[47]=z[5]*z[47];
    z[47]=n<T>(3,2)*z[46] + n<T>(3,2) + z[47];
    z[47]=z[3]*z[47];
    z[32]=z[47] - static_cast<T>(1)+ z[32];
    z[47]=npow(z[3],2);
    z[32]=z[32]*z[47];
    z[48]=npow(z[3],3);
    z[49]=z[48]*z[42];
    z[50]= - z[45] - z[20];
    z[50]=z[50]*z[49];
    z[51]=z[5] + n<T>(3,2);
    z[51]=z[51]*z[24];
    z[51]=z[51] + 1;
    z[43]=z[51]*z[43];
    z[32]=z[50] + z[32] + z[43];
    z[32]=z[2]*z[32];
    z[28]= - z[45]*z[28];
    z[43]=z[31]*z[5];
    z[50]= - static_cast<T>(5)- z[43];
    z[50]=z[5]*z[50];
    z[50]= - static_cast<T>(3)+ z[50];
    z[28]=n<T>(1,2)*z[50] + z[28];
    z[28]=z[3]*z[28];
    z[50]=n<T>(7,2) + z[5];
    z[50]=z[5]*z[50];
    z[50]=n<T>(9,2) + z[50];
    z[50]=z[5]*z[50];
    z[28]=z[28] + static_cast<T>(2)+ z[50];
    z[28]=z[3]*z[28];
    z[28]=z[28] - z[27];
    z[28]=z[3]*z[28];
    z[50]=z[10]*z[5];
    z[52]=z[20]*z[5];
    z[52]=z[52] + 1;
    z[53]= - z[52]*z[50];
    z[54]=5*z[5];
    z[55]=static_cast<T>(9)+ z[54];
    z[55]=z[5]*z[55];
    z[55]=static_cast<T>(7)+ z[55];
    z[53]=n<T>(1,2)*z[55] + z[53];
    z[55]=n<T>(1,4)*z[10];
    z[53]=z[53]*z[55];
    z[28]=z[32] + z[28] + z[53];
    z[28]=z[2]*z[28];
    z[32]= - z[31]*z[18];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[5]*z[32];
    z[32]= - n<T>(3,4) + z[32];
    z[32]=z[5]*z[32];
    z[39]=z[52]*z[39];
    z[26]= - static_cast<T>(1)- z[26];
    z[26]=z[5]*z[26];
    z[26]= - n<T>(1,2) + z[26];
    z[26]=z[10]*z[26];
    z[26]=z[26] - n<T>(1,4)*z[39];
    z[26]=z[2]*z[26];
    z[26]=z[26] - n<T>(5,16) + z[32];
    z[26]=z[26]*z[44];
    z[32]=n<T>(7,2) + z[43];
    z[32]=z[32]*z[24];
    z[32]=static_cast<T>(1)+ z[32];
    z[39]=z[43] + 3;
    z[39]=z[39]*z[5];
    z[39]=z[39] + 1;
    z[43]=z[24]*z[3];
    z[44]=z[39]*z[43];
    z[44]=z[44] + z[27];
    z[44]=z[3]*z[44];
    z[53]= - static_cast<T>(2)- z[24];
    z[53]=z[5]*z[53];
    z[53]= - n<T>(7,2) + z[53];
    z[53]=z[5]*z[53];
    z[53]= - static_cast<T>(3)+ z[53];
    z[53]=z[5]*z[53];
    z[44]=z[44] - static_cast<T>(1)+ z[53];
    z[44]=z[3]*z[44];
    z[39]=n<T>(1,2)*z[39] + z[44];
    z[39]=z[3]*z[39];
    z[26]=z[26] + z[28] + n<T>(1,2)*z[32] + z[39];
    z[26]=z[8]*z[26];
    z[28]=n<T>(1,8)*z[5];
    z[32]=static_cast<T>(33)- 19*z[5];
    z[32]=z[32]*z[28];
    z[39]=n<T>(3,4)*z[21];
    z[44]=n<T>(9,2) - z[5];
    z[44]=z[44]*z[39];
    z[53]=z[30]*z[3];
    z[44]=z[44] + z[53];
    z[44]=z[3]*z[44];
    z[32]=z[32] + z[44];
    z[44]=z[5] - n<T>(5,2);
    z[56]=z[44]*z[30];
    z[57]=npow(z[5],4);
    z[58]=n<T>(1,2)*z[3];
    z[59]=z[57]*z[58];
    z[56]=z[56] - z[59];
    z[56]=z[56]*z[3];
    z[60]= - static_cast<T>(9)+ z[5];
    z[60]=z[60]*z[24];
    z[60]=static_cast<T>(5)+ z[60];
    z[60]=z[60]*z[21];
    z[60]=z[60] - z[56];
    z[60]=z[3]*z[60];
    z[18]=z[18] - 1;
    z[61]=z[18]*z[5];
    z[62]=n<T>(5,8) + z[61];
    z[62]=z[5]*z[62];
    z[60]=z[62] + n<T>(1,8)*z[60];
    z[60]=z[10]*z[60];
    z[32]=n<T>(1,4)*z[32] + z[60];
    z[32]=z[10]*z[32];
    z[60]=static_cast<T>(5)- z[24];
    z[60]=z[5]*z[60];
    z[60]= - static_cast<T>(5)+ z[60];
    z[60]=z[60]*z[21];
    z[56]=z[60] + z[56];
    z[56]=z[56]*z[55];
    z[18]=z[18]*z[21];
    z[18]=z[56] + z[18] - n<T>(1,4)*z[53];
    z[18]=z[10]*z[18];
    z[56]=n<T>(5,2)*z[10];
    z[60]=z[24] - 1;
    z[62]=z[60]*z[5];
    z[63]= - n<T>(1,4) - z[62];
    z[63]=z[63]*z[56];
    z[64]=n<T>(3,2)*z[5];
    z[65]=z[64] - 1;
    z[63]=z[63] + z[65];
    z[63]=z[10]*z[63];
    z[66]=z[10]*z[33];
    z[66]=static_cast<T>(1)- n<T>(5,4)*z[66];
    z[66]=z[10]*z[66];
    z[66]=z[66] - n<T>(5,8)*z[38];
    z[66]=z[9]*z[66];
    z[63]=z[66] - n<T>(3,8) + z[63];
    z[63]=z[9]*z[63];
    z[66]= - n<T>(1,2) - z[61];
    z[56]=z[66]*z[56];
    z[66]=z[5] - n<T>(3,2);
    z[56]=z[66] + z[56];
    z[50]=z[56]*z[50];
    z[56]=n<T>(3,8)*z[5];
    z[50]=z[63] - z[56] + z[50];
    z[50]=z[9]*z[50];
    z[63]=n<T>(1,8)*z[21];
    z[18]=z[50] - z[63] + z[18];
    z[18]=z[15]*z[18];
    z[50]=static_cast<T>(17)- 21*z[5];
    z[67]=3*z[5];
    z[68]= - static_cast<T>(7)+ z[67];
    z[68]=z[5]*z[68];
    z[68]=n<T>(5,2) + z[68];
    z[68]=z[10]*z[68];
    z[50]=n<T>(1,4)*z[50] + z[68];
    z[50]=z[10]*z[50];
    z[50]=n<T>(3,2) + z[50];
    z[66]=z[10]*z[66];
    z[66]= - n<T>(9,8) + z[66];
    z[66]=z[10]*z[66];
    z[38]=z[66] + n<T>(1,4)*z[38];
    z[38]=z[9]*z[38];
    z[38]=n<T>(1,2)*z[50] + z[38];
    z[50]=n<T>(1,4)*z[9];
    z[38]=z[38]*z[50];
    z[50]=z[21]*z[3];
    z[66]=n<T>(9,4)*z[5] + z[50];
    z[18]=n<T>(1,4)*z[18] + z[38] + n<T>(1,8)*z[66] + z[32];
    z[18]=z[15]*z[18];
    z[32]=n<T>(1,2)*z[47];
    z[38]= - static_cast<T>(1)+ 9*z[3];
    z[38]=z[38]*z[32];
    z[66]=static_cast<T>(1)- 7*z[3];
    z[66]=z[66]*z[32];
    z[68]=z[48]*z[2];
    z[66]=z[66] + z[68];
    z[66]=z[2]*z[66];
    z[38]=z[38] + z[66];
    z[38]=z[2]*z[38];
    z[66]= - n<T>(3,2) - 5*z[3];
    z[66]=z[66]*z[32];
    z[69]=n<T>(1,4)*z[4];
    z[70]= - z[69] + static_cast<T>(1)+ z[58];
    z[70]=z[4]*z[47]*z[70];
    z[38]=z[70] + z[66] + z[38];
    z[38]=z[1]*z[38];
    z[17]= - z[17]*z[45];
    z[66]=static_cast<T>(27)+ z[5];
    z[17]=n<T>(1,8)*z[66] + z[17];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(1,4) + z[17];
    z[17]=z[3]*z[17];
    z[17]= - n<T>(1,4) + z[17];
    z[17]=z[3]*z[17];
    z[66]= - n<T>(9,4) - z[5];
    z[66]=n<T>(1,2)*z[66] - z[45];
    z[66]=z[3]*z[66];
    z[66]= - n<T>(1,2) + z[66];
    z[66]=z[2]*z[66]*z[47];
    z[17]=z[17] + z[66];
    z[17]=z[17]*z[42];
    z[66]= - n<T>(3,2) - z[62];
    z[66]=z[66]*z[45];
    z[70]=static_cast<T>(3)- n<T>(17,2)*z[5];
    z[70]=z[5]*z[70];
    z[70]= - n<T>(27,2) + z[70];
    z[66]=n<T>(1,8)*z[70] + z[66];
    z[66]=z[3]*z[66];
    z[70]=n<T>(17,2) - z[67];
    z[66]=n<T>(1,8)*z[70] + z[66];
    z[66]=z[3]*z[66];
    z[60]=n<T>(1,4)*z[60] + z[66];
    z[60]=z[3]*z[60];
    z[17]=z[60] + z[17];
    z[17]=z[2]*z[17];
    z[60]=z[5] - n<T>(1,4);
    z[66]= - z[45] - z[60];
    z[66]=z[3]*z[66];
    z[70]=z[5] + n<T>(1,2);
    z[66]=n<T>(1,4)*z[70] + z[66];
    z[66]=z[3]*z[66];
    z[71]= - n<T>(1,2)*z[70] + z[45];
    z[71]=z[4]*z[71]*z[58];
    z[66]=z[66] + z[71];
    z[66]=z[66]*z[69];
    z[71]=z[33]*z[5];
    z[72]=z[71] + 1;
    z[73]= - z[5]*z[72];
    z[73]=static_cast<T>(1)+ z[73];
    z[73]=z[73]*z[45];
    z[71]=static_cast<T>(1)- 17*z[71];
    z[71]=z[5]*z[71];
    z[71]=static_cast<T>(9)+ z[71];
    z[71]=n<T>(1,8)*z[71] + z[73];
    z[71]=z[3]*z[71];
    z[73]= - static_cast<T>(1)+ n<T>(5,4)*z[5];
    z[73]=z[73]*z[5];
    z[71]=z[71] - n<T>(3,2) - z[73];
    z[71]=z[3]*z[71];
    z[71]=n<T>(3,4) + z[71];
    z[71]=z[71]*z[58];
    z[17]=n<T>(1,8)*z[38] + z[66] + z[71] + z[17];
    z[17]=z[1]*z[17];
    z[38]=npow(z[5],5);
    z[66]=3*z[3];
    z[71]=z[38]*z[66];
    z[74]=n<T>(83,8)*z[57] + z[71];
    z[74]=z[3]*z[74];
    z[74]=n<T>(53,4)*z[30] + z[74];
    z[74]=z[3]*z[74];
    z[74]=n<T>(15,2)*z[21] + z[74];
    z[74]=z[3]*z[74];
    z[38]=z[38]*z[3];
    z[75]=n<T>(33,8)*z[57] + z[38];
    z[75]=z[3]*z[75];
    z[75]=n<T>(13,2)*z[30] + z[75];
    z[75]=z[3]*z[75];
    z[75]=n<T>(19,4)*z[21] + z[75];
    z[75]=z[3]*z[75];
    z[64]=z[64] + z[75];
    z[64]=z[10]*z[64];
    z[64]=z[64] + n<T>(7,4)*z[5] + z[74];
    z[64]=z[64]*z[23];
    z[71]=n<T>(67,8)*z[57] + z[71];
    z[71]=z[71]*z[58];
    z[71]=4*z[30] + z[71];
    z[71]=z[3]*z[71];
    z[71]=n<T>(23,16)*z[21] + z[71];
    z[71]=z[3]*z[71];
    z[38]=n<T>(17,8)*z[57] + z[38];
    z[38]=z[3]*z[38];
    z[38]=n<T>(5,4)*z[30] + z[38];
    z[38]=z[3]*z[38];
    z[38]=z[63] + z[38];
    z[38]=z[1]*z[38]*z[58];
    z[63]=z[10] + 1;
    z[63]=z[63]*z[10];
    z[74]=z[9]*z[63];
    z[38]=z[38] + n<T>(1,16)*z[74] + z[64] + z[28] + z[71];
    z[38]=z[6]*z[38];
    z[40]= - static_cast<T>(13)+ z[40];
    z[40]=z[40]*z[21];
    z[64]=n<T>(1,2)*z[30];
    z[71]= - n<T>(33,4) + z[5];
    z[71]=z[71]*z[64];
    z[74]=z[57]*z[3];
    z[71]=z[71] - 2*z[74];
    z[71]=z[3]*z[71];
    z[40]=n<T>(1,4)*z[40] + z[71];
    z[40]=z[3]*z[40];
    z[71]= - n<T>(11,2) - z[5];
    z[71]=z[71]*z[28];
    z[40]=z[71] + z[40];
    z[40]=z[3]*z[40];
    z[71]= - static_cast<T>(9)+ z[24];
    z[71]=z[71]*z[30];
    z[75]= - z[57]*z[66];
    z[71]=z[71] + z[75];
    z[71]=z[3]*z[71];
    z[75]=n<T>(1,2)*z[21];
    z[54]= - static_cast<T>(21)+ z[54];
    z[54]=z[54]*z[75];
    z[54]=z[54] + z[71];
    z[54]=z[3]*z[54];
    z[71]=z[67] - 5;
    z[76]=z[5]*z[71];
    z[54]=z[76] + z[54];
    z[54]=z[54]*z[55];
    z[76]= - static_cast<T>(1)- n<T>(21,4)*z[5];
    z[40]=z[54] + n<T>(1,8)*z[76] + z[40];
    z[40]=z[10]*z[40];
    z[54]= - n<T>(9,2)*z[21] - z[53];
    z[54]=z[54]*z[58];
    z[54]=z[73] + z[54];
    z[54]=z[3]*z[54];
    z[73]= - z[25]*z[50];
    z[60]= - n<T>(1,2)*z[60] + z[73];
    z[60]=z[10]*z[60];
    z[54]=z[60] - n<T>(19,16) + z[54];
    z[54]=z[54]*z[23];
    z[60]= - z[71]*z[24];
    z[60]= - static_cast<T>(3)+ z[60];
    z[71]= - n<T>(59,4) - z[67];
    z[71]=z[5]*z[71];
    z[71]= - n<T>(21,4) + z[71];
    z[46]=n<T>(1,2)*z[71] - 3*z[46];
    z[46]=z[3]*z[46];
    z[46]=n<T>(1,2)*z[60] + z[46];
    z[46]=z[3]*z[46];
    z[46]=n<T>(9,8) + z[46];
    z[46]=z[3]*z[46];
    z[60]=z[21]*z[47];
    z[71]= - n<T>(9,8) + z[50];
    z[71]=z[10]*z[71];
    z[60]=z[60] + z[71];
    z[60]=z[60]*z[23];
    z[46]=z[60] + n<T>(1,8) + z[46];
    z[60]=n<T>(5,2) + z[5];
    z[43]=z[43] + n<T>(5,16) + z[5];
    z[43]=z[3]*z[43];
    z[43]=n<T>(1,4)*z[60] + z[43];
    z[43]=z[3]*z[43];
    z[43]=n<T>(1,8) + z[43];
    z[43]=z[3]*z[43];
    z[43]=z[43] + n<T>(1,16)*z[68];
    z[43]=z[2]*z[43];
    z[43]=n<T>(1,2)*z[46] + z[43];
    z[43]=z[2]*z[43];
    z[46]=3*z[45];
    z[60]=n<T>(1,2) - z[62];
    z[46]=z[60]*z[46];
    z[60]=static_cast<T>(35)- n<T>(37,2)*z[5];
    z[60]=z[5]*z[60];
    z[60]=n<T>(23,2) + z[60];
    z[46]=n<T>(1,8)*z[60] + z[46];
    z[46]=z[3]*z[46];
    z[60]= - static_cast<T>(5)- z[56];
    z[60]=z[5]*z[60];
    z[46]=z[46] - n<T>(3,8) + z[60];
    z[46]=z[3]*z[46];
    z[60]= - z[5]*z[37];
    z[60]= - n<T>(17,8) + z[60];
    z[46]=n<T>(1,2)*z[60] + z[46];
    z[46]=z[3]*z[46];
    z[43]=z[43] + z[54] - n<T>(1,8)*z[44] + z[46];
    z[43]=z[2]*z[43];
    z[44]= - z[51]*z[24];
    z[46]=n<T>(3,4) + z[5];
    z[46]=z[46]*z[21];
    z[46]=z[46] - z[53];
    z[46]=z[3]*z[46];
    z[44]=z[44] + z[46];
    z[44]=z[3]*z[44];
    z[46]=z[70]*z[30];
    z[46]=z[46] - z[59];
    z[51]= - z[3]*z[46];
    z[52]=z[52]*z[75];
    z[51]=z[52] + z[51];
    z[52]=z[58]*z[10];
    z[51]=z[51]*z[52];
    z[44]=z[44] + z[51];
    z[44]=z[10]*z[44];
    z[46]=z[46]*z[52];
    z[51]=z[70]*z[21];
    z[51]= - n<T>(3,2)*z[51] + z[53];
    z[51]=z[3]*z[51];
    z[46]=z[51] + z[46];
    z[46]=z[10]*z[46];
    z[51]=z[5]*z[70];
    z[51]=z[51] - z[50];
    z[51]=z[51]*z[66];
    z[27]=z[51] - z[27];
    z[27]=n<T>(1,2)*z[27] + z[46];
    z[27]=z[4]*z[27];
    z[24]= - z[24] + z[50];
    z[24]=z[24]*z[66];
    z[46]=static_cast<T>(1)- z[21];
    z[24]=n<T>(1,2)*z[46] + z[24];
    z[24]=z[3]*z[24];
    z[24]=z[27] + z[35] + z[24];
    z[24]=z[44] + n<T>(1,2)*z[24];
    z[24]=z[24]*z[69];
    z[27]=z[41]*z[9];
    z[27]= - z[29] + z[27] - n<T>(1,2);
    z[27]=z[27]*npow(z[9],2);
    z[29]=z[63] - z[36];
    z[29]=z[29]*z[9];
    z[23]=z[23] + 1;
    z[23]=z[23]*z[10];
    z[23]= - z[29] + z[23] + n<T>(1,2);
    z[23]=z[23]*npow(z[9],3);
    z[29]= - z[15]*z[23];
    z[29]= - z[27] + z[29];
    z[29]=z[15]*z[29];
    z[27]= - z[11]*z[27];
    z[35]= - z[15] - z[11];
    z[23]=z[14]*z[35]*z[23];
    z[23]=z[23] + z[29] + z[27];
    z[23]=z[14]*z[23];
    z[27]=z[57]*z[48]*z[55];
    z[29]=z[37]*z[53];
    z[29]=z[64] + z[29];
    z[29]=z[29]*z[47];
    z[27]=z[29] + z[27];
    z[27]=z[10]*z[27];
    z[29]=z[31]*z[58];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[47];
    z[29]=z[29] - z[49];
    z[29]=z[29]*z[42];
    z[25]= - n<T>(3,4)*z[3] + z[25];
    z[25]=z[3]*z[25];
    z[25]= - n<T>(1,4) + z[25];
    z[25]=z[3]*z[25];
    z[25]=z[25] + z[29];
    z[25]=z[2]*z[25];
    z[29]=static_cast<T>(1)- z[67];
    z[29]=z[29]*z[58];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[3]*z[29];
    z[29]=n<T>(1,2)*z[20] + z[29];
    z[29]=z[29]*z[58];
    z[25]=z[29] + z[25];
    z[25]=z[2]*z[25];
    z[29]= - z[65]*z[45];
    z[29]= - z[5] + z[29];
    z[29]=z[29]*z[32];
    z[25]=z[29] + z[25];
    z[25]=z[2]*z[25];
    z[29]=z[33]*z[50];
    z[29]= - z[21] - n<T>(3,2)*z[29];
    z[29]=z[29]*z[32];
    z[25]=z[25] + z[29] + z[27];
    z[25]=z[7]*z[25];
    z[21]= - z[34]*z[21];
    z[27]=z[57] - z[74];
    z[27]=z[3]*z[27];
    z[21]=z[21] + z[27];
    z[21]=z[3]*z[21];
    z[20]=z[20]*z[30];
    z[27]= - z[30] + z[74];
    z[27]=z[3]*z[27];
    z[20]=z[20] + z[27];
    z[27]=z[2]*z[53];
    z[20]=n<T>(1,2)*z[20] + z[27];
    z[20]=z[13]*z[20];
    z[20]=z[20] - z[22] + z[21];
    z[21]= - z[39] - z[53];
    z[21]=z[2]*z[3]*z[21];
    z[20]=z[21] + n<T>(1,2)*z[20];
    z[20]=z[13]*z[20];
    z[21]=static_cast<T>(43)- 67*z[5];
    z[21]=z[5]*z[21];
    z[21]= - static_cast<T>(27)+ z[21];
    z[21]=z[21]*z[28];
    z[22]= - z[72]*z[67];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[22]*z[45];
    z[21]=z[22] - static_cast<T>(1)+ z[21];
    z[21]=z[3]*z[21];
    z[22]=static_cast<T>(23)- 17*z[5];
    z[22]=z[22]*z[56];
    z[21]=z[21] + static_cast<T>(1)+ z[22];
    z[21]=z[3]*z[21];
    z[22]= - static_cast<T>(43)- z[67];
    z[22]=z[5]*z[22];
    z[22]=static_cast<T>(5)+ z[22];
    z[21]=n<T>(1,8)*z[22] + z[21];
    z[21]=z[3]*z[21];
    z[22]= - n<T>(25,8) + z[61];
    z[21]=n<T>(1,2)*z[22] + z[21];

    r += z[17] + z[18] + n<T>(1,8)*z[19] + z[20] + n<T>(1,2)*z[21] + n<T>(1,16)*
      z[23] + z[24] + n<T>(1,4)*z[25] + z[26] + z[38] + z[40] + z[43];
 
    return r;
}

template double qqb_2lha_r1018(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1018(const std::array<dd_real,30>&);
#endif
