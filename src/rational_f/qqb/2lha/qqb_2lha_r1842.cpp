#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1842(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[10];
    z[10]=k[27];
    z[11]=z[4] + 1;
    z[12]=5*z[6];
    z[13]=z[11]*z[12];
    z[14]= - n<T>(13,2)*z[11] - z[13];
    z[14]=z[6]*z[14];
    z[13]= - n<T>(23,2) - z[13];
    z[13]=z[6]*z[13];
    z[15]=z[5]*npow(z[6],2);
    z[13]=z[13] - 5*z[15];
    z[13]=z[5]*z[13];
    z[13]=z[13] - n<T>(13,2) + z[14];
    z[14]=n<T>(5,3)*z[6];
    z[16]=n<T>(1,2) + z[14];
    z[16]=z[6]*z[11]*z[16];
    z[17]=z[11]*z[3];
    z[18]=n<T>(1,2)*z[11] + n<T>(1,3)*z[17];
    z[18]=z[3]*z[18];
    z[16]=z[16] + z[18];
    z[16]=z[7]*z[16];
    z[18]=n<T>(1,3)*z[4];
    z[19]=z[18] + n<T>(1,2);
    z[19]=z[19]*z[3];
    z[20]=n<T>(1,2)*z[4];
    z[21]=z[20] + 1;
    z[22]= - n<T>(1,2)*z[21] - z[19];
    z[22]=z[3]*z[22];
    z[23]=n<T>(1,3)*z[2];
    z[24]=npow(z[3],2);
    z[25]= - z[24]*z[23];
    z[26]=z[18] + 1;
    z[27]=z[3]*z[26];
    z[27]=n<T>(1,2) + z[27];
    z[27]=z[3]*z[27];
    z[25]=z[27] + z[25];
    z[27]=n<T>(1,2)*z[2];
    z[25]=z[25]*z[27];
    z[13]=z[25] + n<T>(1,2)*z[16] + z[22] + n<T>(1,6)*z[13];
    z[13]=z[1]*z[13];
    z[16]=3*z[4];
    z[22]= - static_cast<T>(1)- z[16];
    z[22]=z[22]*z[12];
    z[25]= - static_cast<T>(43)- 71*z[4];
    z[22]=n<T>(1,6)*z[25] + z[22];
    z[25]=n<T>(1,4)*z[6];
    z[22]=z[22]*z[25];
    z[28]= - n<T>(1,4) - z[4];
    z[28]=z[28]*z[12];
    z[29]= - static_cast<T>(5)- 17*z[4];
    z[28]=n<T>(1,4)*z[29] + z[28];
    z[29]=z[9]*z[6];
    z[28]=z[28]*z[29];
    z[22]=z[22] + n<T>(1,3)*z[28];
    z[22]=z[9]*z[22];
    z[28]=n<T>(1,2)*z[6];
    z[30]= - n<T>(21,2) - z[12];
    z[30]=z[30]*z[28];
    z[31]= - static_cast<T>(3)- n<T>(5,6)*z[6];
    z[29]=z[31]*z[29];
    z[29]=z[30] + z[29];
    z[29]=z[9]*z[29];
    z[30]= - static_cast<T>(1)+ z[4];
    z[30]=z[30]*z[12];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=z[6]*z[30];
    z[15]=n<T>(5,6)*z[15] + n<T>(1,3)*z[30] + z[29];
    z[15]=z[5]*z[15];
    z[29]=n<T>(1,4)*z[4];
    z[30]= - static_cast<T>(1)+ z[29];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(1,2) + z[30];
    z[30]=z[30]*z[12];
    z[31]= - static_cast<T>(43)- z[4];
    z[30]=n<T>(1,8)*z[31] + z[30];
    z[30]=z[6]*z[30];
    z[30]= - n<T>(7,4) + z[30];
    z[15]=n<T>(1,2)*z[15] + n<T>(1,3)*z[30] + z[22];
    z[15]=z[5]*z[15];
    z[22]= - static_cast<T>(5)- z[4];
    z[22]=z[22]*z[16];
    z[30]=7*z[4];
    z[31]= - static_cast<T>(17)- z[30];
    z[31]=z[4]*z[31];
    z[31]= - static_cast<T>(13)+ z[31];
    z[31]=z[31]*z[18];
    z[31]= - static_cast<T>(1)+ z[31];
    z[31]=z[3]*z[31];
    z[22]=z[31] - n<T>(31,3) + z[22];
    z[31]= - n<T>(31,6) - z[16];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(13,6) + z[31];
    z[31]=z[9]*z[31];
    z[22]=z[31] + n<T>(1,2)*z[22];
    z[31]=n<T>(1,4)*z[9];
    z[32]= - n<T>(5,3) - n<T>(3,2)*z[4];
    z[32]=z[32]*z[31];
    z[32]=z[32] - n<T>(5,3) - n<T>(9,4)*z[4];
    z[32]=z[9]*z[32];
    z[29]=z[29] + 1;
    z[33]= - static_cast<T>(3)- z[9];
    z[33]=z[9]*z[33];
    z[33]= - n<T>(1,2) + z[33];
    z[33]=z[5]*z[33];
    z[32]=n<T>(3,4)*z[33] - n<T>(3,2)*z[29] + z[32];
    z[32]=z[5]*z[32];
    z[22]=n<T>(1,4)*z[22] + z[32];
    z[22]=z[8]*z[22];
    z[21]=z[21]*z[4];
    z[21]=z[21] + n<T>(1,2);
    z[14]=z[21]*z[14];
    z[32]= - static_cast<T>(1)- n<T>(7,4)*z[4];
    z[32]=z[32]*z[18];
    z[32]=n<T>(1,4) + z[32];
    z[32]=n<T>(1,2)*z[32] + z[14];
    z[32]=z[6]*z[32];
    z[29]=z[29]*z[18];
    z[29]=z[29] + n<T>(1,2);
    z[29]=z[29]*z[4];
    z[29]=z[29] + n<T>(1,3);
    z[29]=z[29]*z[4];
    z[29]=z[29] + n<T>(1,12);
    z[33]=z[6] - 1;
    z[29]=z[9]*z[12]*z[29]*z[33];
    z[33]=z[4] + 3;
    z[34]=z[4]*z[33];
    z[34]=static_cast<T>(3)+ z[34];
    z[34]=z[4]*z[34];
    z[34]=static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[12];
    z[26]=z[26]*z[4];
    z[26]=z[26] + 1;
    z[35]=z[26]*z[4];
    z[35]=z[35] + n<T>(1,3);
    z[34]= - n<T>(17,2)*z[35] + z[34];
    z[25]=z[34]*z[25];
    z[25]=z[25] + z[29];
    z[25]=z[9]*z[25];
    z[29]= - z[17] + n<T>(5,2) + z[4];
    z[18]=z[18]*z[29];
    z[18]=n<T>(1,2) + z[18];
    z[29]=n<T>(1,4)*z[3];
    z[18]=z[18]*z[29];
    z[18]=z[18] + z[32] + z[25];
    z[18]=z[7]*z[18];
    z[25]=n<T>(17,2) + z[30];
    z[26]=z[3]*z[26];
    z[25]=n<T>(1,3)*z[25] + z[26];
    z[25]=z[3]*z[25];
    z[26]=n<T>(7,2)*z[4];
    z[32]=n<T>(17,3) + z[26];
    z[32]=z[4]*z[32];
    z[34]=z[3]*z[35];
    z[32]=z[34] + n<T>(13,6) + z[32];
    z[32]=z[3]*z[32];
    z[34]=n<T>(3,2) + z[4];
    z[32]=n<T>(3,2)*z[5] + 3*z[34] + z[32];
    z[32]=z[8]*z[32];
    z[25]=z[32] + n<T>(5,2) + z[25];
    z[32]= - n<T>(17,3) - z[30];
    z[21]= - z[3]*z[21];
    z[21]=n<T>(1,4)*z[32] + z[21];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(3,4) + z[21];
    z[21]=z[8]*z[21];
    z[23]= - z[8]*z[23];
    z[23]=z[23] + n<T>(1,3);
    z[23]=z[24]*z[23];
    z[17]=n<T>(7,6) + z[17];
    z[17]=z[8]*z[3]*z[17];
    z[17]=z[17] + z[23];
    z[17]=z[17]*z[27];
    z[19]= - n<T>(7,12) - z[19];
    z[19]=z[3]*z[19];
    z[17]=z[17] + z[19] + z[21];
    z[17]=z[2]*z[17];
    z[17]=n<T>(1,2)*z[25] + z[17];
    z[17]=z[17]*z[27];
    z[19]=z[9]*z[11];
    z[19]=static_cast<T>(1)+ z[19];
    z[21]=n<T>(1,2)*z[9];
    z[23]=npow(z[4],2);
    z[19]=z[21]*z[23]*z[19];
    z[24]=z[9]*z[33];
    z[24]=static_cast<T>(1)+ z[24];
    z[24]=z[21]*z[4]*z[24];
    z[21]=z[21] + 1;
    z[21]=z[21]*z[9];
    z[25]= - z[6]*z[21];
    z[25]= - z[28] + z[25];
    z[25]=z[5]*z[25];
    z[27]=n<T>(1,2) + z[9];
    z[27]=z[9]*z[27];
    z[25]=z[25] - n<T>(1,2) + z[27];
    z[25]=z[5]*z[25];
    z[24]=z[24] + z[25];
    z[24]=z[5]*z[24];
    z[25]= - z[5] + 1;
    z[21]=z[21] + n<T>(1,2);
    z[21]=z[8]*npow(z[5],2)*z[21]*z[25];
    z[19]=z[21] + z[19] + z[24];
    z[19]=z[10]*z[19];
    z[11]= - z[11]*z[16];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[12];
    z[16]= - static_cast<T>(13)- n<T>(25,3)*z[4];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(13,3) + z[16];
    z[11]=n<T>(1,2)*z[16] + z[11];
    z[11]=z[6]*z[11];
    z[11]=n<T>(1,6)*z[4] + z[11];
    z[16]= - n<T>(1,3) - z[20];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(1,12) + z[16];
    z[16]=z[16]*z[12];
    z[16]=z[23] + z[16];
    z[16]=z[6]*z[16];
    z[16]= - n<T>(1,24)*z[23] + z[16];
    z[16]=z[9]*z[16];
    z[11]=n<T>(1,4)*z[11] + z[16];
    z[11]=z[9]*z[11];
    z[16]=npow(z[4],3);
    z[21]=n<T>(11,2) - 5*z[4];
    z[21]=z[21]*z[16];
    z[12]=npow(z[4],4)*z[12];
    z[12]=z[21] + z[12];
    z[12]=z[6]*z[12];
    z[12]= - n<T>(11,2)*z[16] + z[12];
    z[12]=z[12]*z[31];
    z[16]=z[6]*z[16];
    z[12]=z[12] - n<T>(17,8)*z[23] - 2*z[16];
    z[12]=z[9]*z[12];
    z[16]= - static_cast<T>(19)- z[30];
    z[16]=z[4]*z[16];
    z[16]= - static_cast<T>(13)+ z[16];
    z[12]= - z[29] + n<T>(1,8)*z[16] + z[12];
    z[12]=z[3]*z[12];
    z[16]= - static_cast<T>(3)- z[20];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(23,12) + z[16];
    z[14]=n<T>(1,2)*z[16] - z[14];
    z[14]=z[6]*z[14];
    z[16]= - n<T>(19,3) - z[26];

    r += z[11] + n<T>(1,3)*z[12] + n<T>(1,2)*z[13] + z[14] + z[15] + n<T>(1,4)*
      z[16] + z[17] + z[18] + n<T>(3,4)*z[19] + z[22];
 
    return r;
}

template double qqb_2lha_r1842(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1842(const std::array<dd_real,30>&);
#endif
