#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1750(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[17];
    z[6]=k[6];
    z[7]=k[20];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=3*z[5];
    z[11]=z[10] + 1;
    z[12]=z[11]*z[5];
    z[12]=z[12] + z[3];
    z[13]=n<T>(1,4)*z[2];
    z[13]=z[12]*z[13];
    z[14]=n<T>(1,2)*z[5];
    z[15]= - static_cast<T>(1)+ n<T>(15,2)*z[5];
    z[15]=z[15]*z[14];
    z[15]= - z[13] + z[15] + z[3];
    z[15]=z[2]*z[15];
    z[16]= - z[3] - static_cast<T>(1)- n<T>(7,2)*z[5];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[16]=n<T>(1,2)*z[7];
    z[17]= - static_cast<T>(23)+ n<T>(33,4)*z[7];
    z[17]=z[17]*z[16];
    z[18]=5*z[8];
    z[19]= - static_cast<T>(5)+ n<T>(3,4)*z[3];
    z[19]=n<T>(57,8)*z[7] + n<T>(1,2)*z[19] - z[18];
    z[19]=z[6]*z[19];
    z[20]=n<T>(1,2)*z[3];
    z[17]=z[19] + z[17] - static_cast<T>(3)- z[20];
    z[17]=z[6]*z[17];
    z[19]=n<T>(3,2)*z[5];
    z[21]=z[19] - 1;
    z[21]=z[21]*z[2]*z[5];
    z[22]= - static_cast<T>(1)+ z[5];
    z[22]=z[5]*z[22];
    z[22]=static_cast<T>(1)+ n<T>(9,2)*z[22];
    z[22]=n<T>(1,2)*z[22] - z[21];
    z[22]=z[2]*z[22];
    z[23]=11*z[7];
    z[24]=n<T>(3,2)*z[8];
    z[25]= - static_cast<T>(1)- z[24];
    z[25]=z[25]*z[23];
    z[26]=3*z[8];
    z[25]=z[25] + n<T>(83,2) + z[26];
    z[25]=z[8]*z[25];
    z[25]=static_cast<T>(19)+ z[25];
    z[25]=z[25]*z[16];
    z[27]=z[8] - n<T>(1,2);
    z[22]=z[25] + z[22] - z[27];
    z[25]=5*z[6];
    z[28]=n<T>(1,2)*z[8];
    z[29]= - static_cast<T>(1)- z[28];
    z[29]=z[29]*z[25];
    z[22]=n<T>(1,2)*z[22] + z[29];
    z[22]=z[4]*z[22];
    z[29]=n<T>(11,2)*z[7];
    z[30]= - n<T>(5,4) - z[8];
    z[30]=z[30]*z[29];
    z[30]=z[30] + n<T>(101,8) + z[8];
    z[30]=z[7]*z[30];
    z[15]=z[22] + z[17] + n<T>(1,2)*z[15] + z[30];
    z[15]=z[4]*z[15];
    z[17]=n<T>(1,2) - z[10];
    z[17]=z[5]*z[17];
    z[13]=z[13] + z[17] - z[20];
    z[13]=z[2]*z[13];
    z[17]= - static_cast<T>(1)+ z[10];
    z[17]=z[17]*z[19];
    z[17]=static_cast<T>(1)+ z[17];
    z[19]=static_cast<T>(1)+ n<T>(3,4)*z[8];
    z[19]=z[19]*z[23];
    z[19]=z[19] - static_cast<T>(19)- z[24];
    z[19]=z[7]*z[19];
    z[13]=z[19] + n<T>(1,2)*z[17] + z[13];
    z[17]=n<T>(19,8)*z[7];
    z[19]=n<T>(1,8)*z[3];
    z[22]= - static_cast<T>(5)- z[19] - z[17];
    z[22]=z[6]*z[22];
    z[30]=static_cast<T>(11)+ z[20];
    z[31]=n<T>(11,8)*z[7];
    z[32]=static_cast<T>(7)- z[31];
    z[32]=z[7]*z[32];
    z[22]=z[22] + n<T>(1,2)*z[30] + z[32];
    z[22]=z[6]*z[22];
    z[13]=n<T>(1,2)*z[13] + z[22];
    z[13]=z[4]*z[13];
    z[22]=n<T>(1,2) + z[5];
    z[22]=z[5]*z[22];
    z[22]=z[22] + z[20];
    z[30]=n<T>(1,2)*z[2];
    z[32]= - z[12]*z[30];
    z[33]=z[29] - 1;
    z[33]=z[33]*z[7];
    z[22]=z[33] + 3*z[22] + z[32];
    z[32]=n<T>(3,2)*z[3];
    z[34]= - static_cast<T>(27)+ z[23];
    z[34]=z[7]*z[34];
    z[34]=z[34] + static_cast<T>(11)- z[32];
    z[35]=z[20] + n<T>(19,2)*z[7];
    z[36]= - static_cast<T>(10)+ z[35];
    z[36]=z[6]*z[36];
    z[34]=n<T>(1,2)*z[34] + z[36];
    z[34]=z[6]*z[34];
    z[36]=n<T>(1,4)*z[3];
    z[37]=static_cast<T>(6)- z[29];
    z[37]=z[7]*z[37];
    z[34]=z[34] + z[37] - z[5] - z[36];
    z[34]=z[6]*z[34];
    z[13]=z[13] + n<T>(1,2)*z[22] + z[34];
    z[13]=z[4]*z[13];
    z[22]=static_cast<T>(5)- z[31];
    z[22]=z[7]*z[22];
    z[31]= - z[3] - 19*z[7];
    z[31]=z[6]*z[31];
    z[22]=n<T>(1,8)*z[31] + z[36] + z[22];
    z[22]=z[6]*z[22];
    z[11]=z[11]*z[14];
    z[31]= - n<T>(23,2) + z[23];
    z[31]=z[7]*z[31];
    z[11]=z[11] + z[31];
    z[11]=n<T>(1,4)*z[11] + z[22];
    z[11]=z[6]*z[11];
    z[22]=z[30] - 1;
    z[12]=z[12]*z[22];
    z[12]= - z[33] + z[12];
    z[11]=n<T>(1,4)*z[12] + z[11];
    z[11]=z[1]*z[11]*npow(z[4],2);
    z[11]=z[13] + z[11];
    z[11]=z[1]*z[11];
    z[12]=npow(z[5],2);
    z[12]=3*z[12] + z[3];
    z[12]=z[12]*z[30];
    z[13]=static_cast<T>(3)- z[23];
    z[13]=z[13]*z[16];
    z[22]=z[20] + 1;
    z[31]= - z[5] - z[22];
    z[12]=z[13] + 3*z[31] + z[12];
    z[13]=static_cast<T>(7)- z[29];
    z[13]=z[7]*z[13];
    z[13]=3*z[13] - n<T>(19,2)*z[8] - static_cast<T>(25)+ z[32];
    z[31]= - n<T>(19,4)*z[7] - z[18] + static_cast<T>(10)- z[36];
    z[31]=z[6]*z[31];
    z[13]=n<T>(1,2)*z[13] + 3*z[31];
    z[13]=z[6]*z[13];
    z[31]=3*z[3];
    z[32]= - static_cast<T>(1)+ z[31];
    z[32]=n<T>(1,2)*z[32] - z[18];
    z[13]=z[13] + n<T>(1,2)*z[32] - 2*z[7];
    z[13]=z[6]*z[13];
    z[11]=z[11] + z[15] + n<T>(1,2)*z[12] + z[13];
    z[11]=z[1]*z[11];
    z[12]=n<T>(1,2)*z[9];
    z[13]=static_cast<T>(17)- z[20];
    z[13]=z[13]*z[12];
    z[15]= - static_cast<T>(7)+ z[12];
    z[32]=z[9] - 1;
    z[33]= - z[8]*z[32];
    z[15]=n<T>(1,2)*z[15] + z[33];
    z[15]=z[15]*z[18];
    z[23]=z[23] - 1;
    z[23]=z[9]*z[23];
    z[23]= - n<T>(57,4) + z[23];
    z[23]=z[23]*z[16];
    z[33]=3*z[9];
    z[34]= - z[8]*z[9];
    z[34]=z[33] + z[34];
    z[34]=z[8]*z[34];
    z[35]= - static_cast<T>(30)+ z[35];
    z[35]=z[9]*z[35];
    z[34]=10*z[34] + z[35];
    z[34]=z[6]*z[34];
    z[13]=z[34] + z[23] + z[15] + z[13] + static_cast<T>(20)- n<T>(3,8)*z[3];
    z[13]=z[6]*z[13];
    z[15]=static_cast<T>(7)- z[31];
    z[15]=z[15]*z[12];
    z[15]=z[15] - static_cast<T>(7)+ z[20];
    z[20]= - n<T>(9,4) - z[9];
    z[20]=3*z[20] + z[18];
    z[20]=z[8]*z[20];
    z[23]= - n<T>(3,4) + z[9];
    z[23]=z[23]*z[29];
    z[23]=z[23] + static_cast<T>(2)- n<T>(5,2)*z[9];
    z[23]=z[7]*z[23];
    z[13]=z[13] + z[23] + n<T>(1,2)*z[15] + z[20];
    z[13]=z[6]*z[13];
    z[10]= - static_cast<T>(5)+ z[10];
    z[10]=z[10]*z[14];
    z[10]=static_cast<T>(1)+ z[10];
    z[10]=z[10]*npow(z[2],2);
    z[15]= - static_cast<T>(15)- z[8];
    z[15]=z[15]*npow(z[8],2);
    z[20]=npow(z[8],3)*z[29];
    z[15]=z[15] + z[20];
    z[15]=z[7]*z[15];
    z[20]=z[8]*z[27];
    z[10]=z[15] + z[10] + z[20];
    z[15]=n<T>(1,2)*z[4];
    z[10]=z[10]*z[15];
    z[14]=z[21] - static_cast<T>(1)+ z[14];
    z[14]=z[2]*z[14];
    z[14]=z[14] + z[27];
    z[20]=n<T>(1,2) + z[8];
    z[20]=z[20]*z[29];
    z[20]=z[20] - n<T>(53,4) - z[8];
    z[20]=z[8]*z[20];
    z[20]= - n<T>(19,4) + z[20];
    z[20]=z[7]*z[20];
    z[21]=static_cast<T>(1)- z[8];
    z[21]=z[21]*z[25];
    z[10]=z[10] + z[21] + n<T>(1,2)*z[14] + z[20];
    z[10]=z[10]*z[15];
    z[14]= - z[36] - static_cast<T>(1)+ n<T>(1,4)*z[5];
    z[14]=z[2]*z[14];
    z[15]=z[9]*z[22];
    z[14]=z[15] + z[14] - n<T>(7,2) + z[3];
    z[15]=z[24] + z[32];
    z[20]=n<T>(11,4)*z[7];
    z[15]=z[15]*z[20];
    z[15]=z[15] - n<T>(5,4)*z[8] - n<T>(29,8) - z[9];
    z[15]=z[7]*z[15];
    z[10]=z[11] + z[10] + z[13] + z[15] + n<T>(1,2)*z[14] + z[26];
    z[10]=z[1]*z[10];
    z[11]=static_cast<T>(45)+ 7*z[9];
    z[11]=z[11]*z[12];
    z[13]= - static_cast<T>(35)- z[33];
    z[13]=z[13]*z[12];
    z[14]=z[9]*z[18];
    z[13]=z[13] + z[14];
    z[13]=z[8]*z[13];
    z[11]=z[11] + z[13];
    z[11]=z[8]*z[11];
    z[13]=static_cast<T>(2)- z[28];
    z[13]=z[8]*z[13];
    z[13]= - static_cast<T>(3)+ z[13];
    z[14]=npow(z[9],2);
    z[13]=z[18]*z[14]*z[13];
    z[15]= - z[17] + static_cast<T>(10)- z[19];
    z[15]=z[14]*z[15];
    z[13]=z[13] + z[15];
    z[13]=z[6]*z[13];
    z[15]= - z[33] - static_cast<T>(25)+ z[36];
    z[15]=z[15]*z[12];
    z[14]= - z[14]*z[20];
    z[17]=n<T>(19,4) - z[33];
    z[17]=z[9]*z[17];
    z[14]=z[17] + z[14];
    z[14]=z[14]*z[16];
    z[11]=z[13] + z[14] + z[15] + z[11];
    z[11]=z[6]*z[11];
    z[13]=z[9] - n<T>(1,2);
    z[14]= - z[13]*z[29];
    z[14]=z[14] + static_cast<T>(5)+ n<T>(11,4)*z[9];
    z[14]=z[16]*z[14];
    z[15]= - static_cast<T>(1)+ z[36];
    z[15]=z[9]*z[15];
    z[14]=z[14] + static_cast<T>(1)+ z[15];
    z[14]=z[9]*z[14];
    z[15]= - static_cast<T>(11)+ z[12];
    z[15]=z[9]*z[15];
    z[15]= - static_cast<T>(15)+ z[15];
    z[16]=n<T>(5,2) + z[9];
    z[16]=3*z[16] - n<T>(5,2)*z[8];
    z[16]=z[8]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[8]*z[15];
    z[11]=z[11] + z[15] + n<T>(5,2) + z[14];
    z[11]=z[6]*z[11];
    z[14]=z[9] - 3;
    z[15]= - z[26] + z[30] - z[14];
    z[15]=z[8]*z[15];
    z[16]=static_cast<T>(3)- z[3];
    z[16]=z[9]*z[16];
    z[16]= - z[2] + z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[12]= - z[14]*z[12];
    z[13]= - z[28] - z[13];
    z[13]=z[8]*z[13];
    z[12]=z[12] + z[13];
    z[12]=z[12]*z[20];
    z[13]=static_cast<T>(1)+ z[9];
    z[13]=z[9]*z[13];
    z[13]= - n<T>(19,4) + z[13];
    z[14]=n<T>(15,8) + z[28] + z[9];
    z[14]=z[8]*z[14];
    z[12]=z[12] + n<T>(1,2)*z[13] + z[14];
    z[12]=z[7]*z[12];

    r += z[10] + z[11] + z[12] + n<T>(1,2)*z[15];
 
    return r;
}

template double qqb_2lha_r1750(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1750(const std::array<dd_real,30>&);
#endif
