#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1868(const std::array<T,30>& k) {
  T z[65];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[10];
    z[9]=k[15];
    z[10]=z[7] + n<T>(1,2);
    z[11]=3*z[7];
    z[12]=z[10]*z[11];
    z[12]= - n<T>(1,2) + z[12];
    z[12]=z[7]*z[12];
    z[13]=npow(z[7],2);
    z[14]=z[13]*z[8];
    z[15]=z[7] + n<T>(5,2);
    z[16]=z[15]*z[7];
    z[17]=static_cast<T>(1)+ z[16];
    z[17]=z[17]*z[14];
    z[12]=z[12] + z[17];
    z[17]=n<T>(1,2)*z[8];
    z[12]=z[12]*z[17];
    z[18]=z[13]*z[17];
    z[19]=z[7] + n<T>(3,2);
    z[19]=z[19]*z[7];
    z[20]= - n<T>(1,2) - z[19];
    z[20]=z[20]*z[18];
    z[21]=z[7] + n<T>(9,4);
    z[22]=z[21]*z[7];
    z[23]=n<T>(1,2) - z[22];
    z[23]=z[7]*z[23];
    z[20]=z[23] + z[20];
    z[20]=z[8]*z[20];
    z[23]=z[7] - n<T>(1,2);
    z[24]=z[23]*z[18];
    z[25]=z[7] - n<T>(1,4);
    z[26]=z[25]*z[7];
    z[24]=z[24] + z[26];
    z[24]=z[24]*z[8];
    z[26]=n<T>(1,2)*z[7];
    z[24]=z[24] + z[26];
    z[27]=z[2]*z[24];
    z[28]=z[7] + 3;
    z[29]=z[28]*z[7];
    z[30]= - n<T>(1,2) - z[29];
    z[20]=z[27] + n<T>(1,2)*z[30] + z[20];
    z[20]=z[2]*z[20];
    z[12]=z[20] + z[12] + n<T>(1,4) + z[19];
    z[12]=z[2]*z[12];
    z[20]=z[26] + 1;
    z[27]=z[20]*z[13];
    z[30]=npow(z[7],3);
    z[31]=z[30]*z[8];
    z[32]=z[7] + 1;
    z[33]=z[32]*z[31];
    z[27]=z[27] + n<T>(1,4)*z[33];
    z[27]=z[8]*z[27];
    z[33]=n<T>(1,4)*z[7];
    z[34]=z[28]*z[33];
    z[35]=z[30]*z[17];
    z[36]= - z[13] - z[35];
    z[36]=z[8]*z[36];
    z[36]= - z[26] + z[36];
    z[37]=n<T>(1,2)*z[2];
    z[36]=z[36]*z[37];
    z[27]=z[36] + z[34] + z[27];
    z[27]=z[2]*z[27];
    z[34]=z[18]*z[32];
    z[20]=z[20]*z[7];
    z[27]=z[27] - z[20] - z[34];
    z[27]=z[2]*z[27];
    z[36]=z[32]*z[26];
    z[38]=z[1] + 1;
    z[39]=z[37] - 1;
    z[39]=z[39]*z[2];
    z[39]=z[39] + n<T>(1,2);
    z[38]=z[1]*z[39]*z[38];
    z[27]=n<T>(1,2)*z[38] + z[36] + z[27];
    z[27]=z[3]*z[27];
    z[38]=z[7] - 1;
    z[39]=z[38]*z[13];
    z[40]=z[7] - 3;
    z[35]= - z[40]*z[35];
    z[35]=z[39] + z[35];
    z[35]=z[8]*z[35];
    z[35]= - z[36] + z[35];
    z[39]= - z[23]*z[13];
    z[41]= - n<T>(5,4) + z[7];
    z[41]=z[41]*z[31];
    z[39]=z[39] + z[41];
    z[39]=z[8]*z[39];
    z[41]= - static_cast<T>(1)+ n<T>(5,2)*z[7];
    z[42]= - z[41]*z[31];
    z[42]=z[30] + z[42];
    z[42]=z[8]*z[42];
    z[43]=npow(z[8],2)*npow(z[7],4);
    z[44]=z[43]*z[6];
    z[42]=z[42] + z[44];
    z[45]=n<T>(1,2)*z[6];
    z[42]=z[42]*z[45];
    z[39]=z[39] + z[42];
    z[39]=z[6]*z[39];
    z[42]= - z[2] + z[23];
    z[42]=z[2]*z[42];
    z[42]=n<T>(3,2) + z[42];
    z[46]=static_cast<T>(1)- z[2];
    z[46]=z[1]*z[46];
    z[42]=n<T>(1,2)*z[42] + z[46];
    z[42]=z[1]*z[42];
    z[12]=z[27] + z[42] + z[12] + n<T>(1,2)*z[35] + z[39];
    z[12]=z[3]*z[12];
    z[27]=z[20] + n<T>(1,2);
    z[35]=z[8]*z[7];
    z[39]= - z[27]*z[35];
    z[39]= - z[36] + z[39];
    z[39]=z[5]*z[39];
    z[42]= - z[15]*z[13];
    z[42]=n<T>(3,2) + z[42];
    z[42]=z[8]*z[42]*z[26];
    z[46]= - n<T>(5,4) - z[7];
    z[46]=z[7]*z[46];
    z[46]=n<T>(5,4) + z[46];
    z[46]=z[7]*z[46];
    z[39]=z[39] + z[46] + z[42];
    z[39]=z[8]*z[39];
    z[42]=z[26] - 3;
    z[42]=z[42]*z[7];
    z[46]= - n<T>(9,2) + z[42];
    z[46]=z[46]*z[14];
    z[46]= - n<T>(19,2)*z[13] + z[46];
    z[46]=z[46]*z[17];
    z[21]=z[21]*z[31];
    z[21]=n<T>(7,2)*z[30] + z[21];
    z[21]=z[8]*z[21];
    z[21]=z[21] - n<T>(3,4)*z[44];
    z[21]=z[6]*z[21];
    z[44]=3*z[13];
    z[21]=z[21] - z[44] + z[46];
    z[21]=z[6]*z[21];
    z[46]=z[11] - 1;
    z[47]= - z[46]*z[26];
    z[21]=z[21] + z[47] + z[39];
    z[21]=z[9]*z[21];
    z[12]=z[12] + z[21];
    z[21]=n<T>(3,2)*z[6];
    z[39]=static_cast<T>(5)- z[21];
    z[39]=z[39]*z[45];
    z[47]=z[6] + 3;
    z[48]=n<T>(3,2)*z[4];
    z[49]= - z[47]*z[48];
    z[39]=z[49] + static_cast<T>(3)+ z[39];
    z[39]=z[4]*z[39];
    z[49]=5*z[6];
    z[50]= - static_cast<T>(9)- z[49];
    z[50]=z[6]*z[50];
    z[50]= - static_cast<T>(27)+ z[50];
    z[50]=z[6]*z[50];
    z[50]= - static_cast<T>(17)+ z[50];
    z[39]=n<T>(1,4)*z[50] + z[39];
    z[39]=z[4]*z[39];
    z[50]=3*z[4];
    z[51]= - static_cast<T>(1)+ z[50];
    z[51]=z[51]*z[48];
    z[51]=static_cast<T>(1)+ z[51];
    z[51]=z[4]*z[51];
    z[52]=n<T>(1,2) - z[50];
    z[52]=z[52]*npow(z[4],2)*z[37];
    z[51]=z[51] + z[52];
    z[51]=z[2]*z[51];
    z[52]=static_cast<T>(3)+ n<T>(5,4)*z[6];
    z[52]=z[6]*z[52];
    z[52]=n<T>(23,4) + z[52];
    z[52]=z[6]*z[52];
    z[39]=z[51] + z[39] + n<T>(11,4) + z[52];
    z[51]=npow(z[6],2);
    z[52]= - static_cast<T>(49)+ z[49];
    z[52]=z[52]*z[51];
    z[53]=z[5]*npow(z[6],4);
    z[54]=n<T>(5,2)*z[53];
    z[55]=npow(z[6],3);
    z[56]= - 7*z[55] - z[54];
    z[56]=z[5]*z[56];
    z[52]=n<T>(1,8)*z[52] + z[56];
    z[52]=z[5]*z[52];
    z[56]=n<T>(1,4)*z[6];
    z[47]=z[6]*z[47];
    z[47]= - static_cast<T>(1)+ n<T>(5,2)*z[47];
    z[47]=z[47]*z[56];
    z[47]=z[47] + z[52];
    z[47]=z[5]*z[47];
    z[39]=n<T>(1,2)*z[39] + z[47];
    z[39]=z[1]*z[39];
    z[47]=z[32]*z[6];
    z[52]=2*z[7];
    z[57]= - n<T>(3,4)*z[47] + n<T>(19,8) + z[52];
    z[57]=z[6]*z[57];
    z[15]= - z[47] - z[15];
    z[15]=z[15]*z[48];
    z[58]=n<T>(17,2) + z[11];
    z[15]=z[15] + n<T>(1,4)*z[58] + z[57];
    z[15]=z[4]*z[15];
    z[57]= - z[32]*z[49];
    z[57]=z[57] - n<T>(11,2) - z[7];
    z[57]=z[6]*z[57];
    z[57]=z[57] - n<T>(27,2) - z[7];
    z[56]=z[57]*z[56];
    z[15]=z[15] + z[56] + z[25];
    z[15]=z[4]*z[15];
    z[56]= - n<T>(1,2) - z[4];
    z[56]=z[56]*z[50];
    z[56]=static_cast<T>(1)+ z[56];
    z[56]=z[2]*z[56];
    z[56]=z[56] - n<T>(7,2) - z[7];
    z[25]=n<T>(1,2)*z[25] + z[50];
    z[25]=z[4]*z[25];
    z[25]=z[25] + n<T>(1,4)*z[56];
    z[25]=z[4]*z[25];
    z[25]=static_cast<T>(2)+ z[25];
    z[25]=z[2]*z[25];
    z[56]= - static_cast<T>(8)+ z[11];
    z[57]=10*z[6];
    z[58]= - z[7]*z[57];
    z[56]=3*z[56] + z[58];
    z[56]=z[6]*z[56];
    z[56]=n<T>(49,4) + z[56];
    z[56]=z[56]*z[51];
    z[58]=z[7] - 2;
    z[58]=z[58]*z[49];
    z[59]=static_cast<T>(14)+ z[58];
    z[59]=z[59]*z[55];
    z[53]=z[59] + 5*z[53];
    z[53]=z[5]*z[53];
    z[53]=z[56] + z[53];
    z[53]=z[5]*z[53];
    z[56]=23*z[7];
    z[59]=n<T>(5,2) - z[56];
    z[59]=z[59]*z[45];
    z[59]=z[59] - static_cast<T>(17)+ n<T>(13,4)*z[7];
    z[59]=z[6]*z[59];
    z[59]=n<T>(1,2) + z[59];
    z[59]=z[6]*z[59];
    z[53]=z[59] + z[53];
    z[53]=z[5]*z[53];
    z[59]=11*z[7];
    z[60]= - n<T>(9,2) - z[59];
    z[47]=n<T>(5,4)*z[47];
    z[61]=z[47] + n<T>(23,8) - z[7];
    z[61]=z[6]*z[61];
    z[60]=n<T>(1,4)*z[60] + z[61];
    z[60]=z[6]*z[60];
    z[53]=z[53] - n<T>(11,4) + z[60];
    z[53]=z[5]*z[53];
    z[60]=static_cast<T>(5)+ z[11];
    z[47]=z[47] + n<T>(17,8) + z[7];
    z[47]=z[6]*z[47];
    z[47]=n<T>(1,2)*z[60] + z[47];
    z[47]=z[6]*z[47];
    z[15]=z[39] + z[53] + z[25] + z[15] + z[47] - n<T>(1,8) - z[52];
    z[15]=z[1]*z[15];
    z[25]=static_cast<T>(31)+ z[59];
    z[25]=z[25]*z[33];
    z[39]=7*z[7];
    z[47]=n<T>(129,4) + z[39];
    z[47]=z[7]*z[47];
    z[47]=n<T>(87,2) + z[47];
    z[47]=z[7]*z[47];
    z[53]=n<T>(13,2) + z[7];
    z[53]=z[7]*z[53];
    z[53]=n<T>(27,2) + z[53];
    z[53]=z[7]*z[53];
    z[53]=n<T>(23,2) + z[53];
    z[53]=z[7]*z[53];
    z[53]=n<T>(7,2) + z[53];
    z[53]=z[8]*z[53];
    z[47]=n<T>(3,2)*z[53] + n<T>(73,4) + z[47];
    z[47]=z[47]*z[17];
    z[53]=z[33] + 1;
    z[53]=z[53]*z[7];
    z[60]=z[53] + n<T>(3,2);
    z[60]=z[60]*z[7];
    z[61]=z[60] + 1;
    z[61]=z[61]*z[7];
    z[61]=z[61] + n<T>(1,4);
    z[61]=z[61]*z[8];
    z[62]=z[29] + 3;
    z[62]=z[62]*z[7];
    z[61]=z[61] + z[62] + 1;
    z[61]=z[61]*z[8];
    z[61]=z[61] + n<T>(3,2)*z[27];
    z[21]= - z[61]*z[21];
    z[21]=z[21] + z[47] + static_cast<T>(5)+ z[25];
    z[21]=z[6]*z[21];
    z[25]=z[32]*z[7];
    z[32]= - static_cast<T>(3)+ z[25];
    z[32]=z[7]*z[32];
    z[32]= - static_cast<T>(5)+ z[32];
    z[32]=z[32]*z[26];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[8]*z[32];
    z[32]= - 9*z[27] + z[32];
    z[32]=z[32]*z[17];
    z[47]= - z[6]*z[61];
    z[62]= - static_cast<T>(2)- z[33];
    z[62]=z[7]*z[62];
    z[32]=z[47] + z[32] - n<T>(7,4) + z[62];
    z[32]=z[32]*z[50];
    z[47]=n<T>(53,2) + z[11];
    z[47]=z[47]*z[33];
    z[62]=n<T>(15,2) + z[7];
    z[62]=z[62]*z[33];
    z[62]=static_cast<T>(3)+ z[62];
    z[62]=z[7]*z[62];
    z[62]=n<T>(11,8) + z[62];
    z[63]=3*z[8];
    z[62]=z[62]*z[63];
    z[64]=n<T>(77,2) + z[11];
    z[64]=z[7]*z[64];
    z[64]=static_cast<T>(95)+ z[64];
    z[64]=z[7]*z[64];
    z[64]=n<T>(119,2) + z[64];
    z[62]=n<T>(1,4)*z[64] + z[62];
    z[62]=z[8]*z[62];
    z[21]=z[32] + z[21] + z[62] + static_cast<T>(7)+ z[47];
    z[21]=z[4]*z[21];
    z[32]=static_cast<T>(25)+ z[39];
    z[32]=z[7]*z[32];
    z[32]=static_cast<T>(33)+ z[32];
    z[32]=z[7]*z[32];
    z[32]=static_cast<T>(19)+ z[32];
    z[32]=z[32]*z[33];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[8]*z[32];
    z[39]=n<T>(3,2) + z[22];
    z[39]=z[7]*z[39];
    z[39]=n<T>(1,4) + z[39];
    z[32]=3*z[39] + z[32];
    z[32]=z[8]*z[32];
    z[39]= - z[61]*z[49];
    z[41]=z[7]*z[41];
    z[41]= - n<T>(7,2) + z[41];
    z[32]=z[39] + n<T>(1,2)*z[41] + z[32];
    z[32]=z[6]*z[32];
    z[39]= - static_cast<T>(19)- z[26];
    z[39]=z[39]*z[26];
    z[40]=z[40]*z[7];
    z[41]= - static_cast<T>(39)+ z[40];
    z[41]=z[7]*z[41];
    z[41]= - static_cast<T>(65)+ z[41];
    z[41]=z[41]*z[26];
    z[41]= - static_cast<T>(15)+ z[41];
    z[41]=z[8]*z[41];
    z[47]= - static_cast<T>(83)- n<T>(59,2)*z[7];
    z[47]=z[7]*z[47];
    z[41]=z[41] - n<T>(107,2) + z[47];
    z[41]=z[41]*z[17];
    z[32]=z[32] + z[41] - static_cast<T>(13)+ z[39];
    z[32]=z[6]*z[32];
    z[39]=5*z[7];
    z[41]= - static_cast<T>(13)- z[39];
    z[41]=z[41]*z[26];
    z[41]= - static_cast<T>(13)+ z[41];
    z[41]=z[7]*z[41];
    z[41]= - n<T>(47,2) + z[41];
    z[41]=z[7]*z[41];
    z[41]= - n<T>(29,2) + z[41];
    z[41]=z[8]*z[41];
    z[47]=z[11] + n<T>(19,2);
    z[61]= - z[7]*z[47];
    z[61]= - n<T>(85,2) + z[61];
    z[61]=z[7]*z[61];
    z[41]=z[41] - static_cast<T>(43)+ z[61];
    z[41]=z[41]*z[17];
    z[32]=z[32] + z[41] - n<T>(11,2) - z[53];
    z[21]=n<T>(1,2)*z[32] + z[21];
    z[21]=z[4]*z[21];
    z[32]=n<T>(1,2) + z[39];
    z[32]=z[32]*z[26];
    z[32]= - static_cast<T>(7)+ z[32];
    z[41]=n<T>(1,8) + z[60];
    z[41]=z[7]*z[41];
    z[41]= - n<T>(5,8) + z[41];
    z[41]=z[41]*z[63];
    z[53]=n<T>(29,8) + z[52];
    z[53]=z[7]*z[53];
    z[53]= - n<T>(33,8) + z[53];
    z[53]=z[7]*z[53];
    z[41]=z[41] - n<T>(67,8) + z[53];
    z[41]=z[8]*z[41];
    z[53]= - static_cast<T>(5)- z[7];
    z[53]=z[53]*z[26];
    z[53]= - static_cast<T>(3)+ z[53];
    z[53]=z[7]*z[53];
    z[53]= - n<T>(1,2) + z[53];
    z[53]=z[7]*z[53];
    z[53]=n<T>(1,2) + z[53];
    z[53]=z[53]*z[17];
    z[60]=static_cast<T>(1)- z[20];
    z[60]=z[7]*z[60];
    z[53]=z[53] + n<T>(3,2) + z[60];
    z[53]=z[8]*z[53];
    z[60]= - z[7]*z[38];
    z[60]=static_cast<T>(5)+ z[60];
    z[53]=n<T>(1,4)*z[60] + z[53];
    z[53]=z[53]*z[50];
    z[32]=z[53] + n<T>(1,2)*z[32] + z[41];
    z[32]=z[4]*z[32];
    z[16]=n<T>(3,2) + z[16];
    z[16]=z[16]*z[33];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[7]*z[16];
    z[16]=n<T>(11,8) + z[16];
    z[16]=z[8]*z[16];
    z[41]=n<T>(7,2) + z[11];
    z[41]=z[7]*z[41];
    z[41]=static_cast<T>(5)+ z[41];
    z[41]=z[7]*z[41];
    z[41]=n<T>(31,2) + z[41];
    z[16]=n<T>(1,4)*z[41] + z[16];
    z[16]=z[8]*z[16];
    z[41]=n<T>(1,4) + z[25];
    z[16]=z[32] + n<T>(1,2)*z[41] + z[16];
    z[16]=z[4]*z[16];
    z[32]= - z[47]*z[33];
    z[32]= - static_cast<T>(2)+ z[32];
    z[32]=z[7]*z[32];
    z[20]= - n<T>(5,2) - z[20];
    z[20]=z[7]*z[20];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[20]*z[35];
    z[20]=n<T>(3,4)*z[20] + n<T>(9,8) + z[32];
    z[20]=z[8]*z[20];
    z[32]=static_cast<T>(1)- z[36];
    z[32]=z[7]*z[32];
    z[32]=static_cast<T>(1)+ z[32];
    z[32]=z[32]*z[35];
    z[36]=z[10]*z[7];
    z[41]=n<T>(1,2) - z[36];
    z[41]=z[7]*z[41];
    z[32]=z[32] - n<T>(1,2) + z[41];
    z[32]=z[8]*z[32];
    z[41]= - static_cast<T>(1)- z[13];
    z[32]=n<T>(1,2)*z[41] + z[32];
    z[32]=z[32]*z[48];
    z[41]= - static_cast<T>(13)- z[11];
    z[41]=z[7]*z[41];
    z[41]=static_cast<T>(3)+ z[41];
    z[20]=z[32] + n<T>(1,8)*z[41] + z[20];
    z[20]=z[4]*z[20];
    z[18]=z[38]*z[18];
    z[32]=z[7]*z[23];
    z[18]=z[32] + z[18];
    z[18]=z[8]*z[18];
    z[18]=z[26] + z[18];
    z[32]=z[36] + z[34];
    z[32]=z[8]*z[32];
    z[32]=z[26] + z[32];
    z[32]=z[4]*z[32];
    z[18]=n<T>(1,2)*z[18] + z[32];
    z[18]=z[18]*z[50];
    z[18]=z[18] + z[24];
    z[18]=z[18]*z[37];
    z[19]=n<T>(3,2) - z[19];
    z[19]=z[19]*z[26];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[19]*z[35];
    z[22]=n<T>(3,2) - z[22];
    z[22]=z[7]*z[22];
    z[19]=z[19] - n<T>(1,2) + z[22];
    z[19]=z[8]*z[19];
    z[22]=n<T>(1,2) - z[29];
    z[19]=n<T>(1,2)*z[22] + z[19];
    z[18]=z[18] + n<T>(1,2)*z[19] + z[20];
    z[18]=z[4]*z[18];
    z[18]=n<T>(1,4) + z[18];
    z[18]=z[2]*z[18];
    z[19]= - n<T>(1,2) - z[25];
    z[19]=z[8]*z[19];
    z[10]=z[19] - z[10];
    z[10]=z[8]*z[10];
    z[10]=z[10] + n<T>(3,2) - z[7];
    z[10]=z[18] + n<T>(1,2)*z[10] + z[16];
    z[10]=z[2]*z[10];
    z[16]=z[46]*z[59];
    z[16]=n<T>(5,4) + z[16];
    z[16]=z[8]*z[16];
    z[18]= - n<T>(83,2) + z[56];
    z[18]=z[7]*z[18];
    z[16]=z[16] + static_cast<T>(5)+ z[18];
    z[16]=z[8]*z[16];
    z[18]= - static_cast<T>(117)+ n<T>(49,2)*z[7];
    z[18]=z[7]*z[18];
    z[18]=n<T>(15,2) + z[18];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[14]= - 2*z[13] - z[14];
    z[14]=z[8]*z[14];
    z[13]= - z[13] + z[14];
    z[13]=z[6]*z[13];
    z[13]=n<T>(1,2)*z[16] + 15*z[13];
    z[13]=z[6]*z[13];
    z[14]=static_cast<T>(2)+ n<T>(1,8)*z[7];
    z[14]=z[14]*z[11];
    z[16]=n<T>(19,2)*z[7];
    z[18]=static_cast<T>(1)+ z[16];
    z[19]=n<T>(19,4) - 4*z[7];
    z[19]=z[7]*z[19];
    z[19]=n<T>(7,8) + z[19];
    z[19]=z[8]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[8]*z[18];
    z[13]=z[13] + z[18] - n<T>(95,8) + z[14];
    z[13]=z[6]*z[13];
    z[14]=z[26] - 1;
    z[18]=z[7]*z[14];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[8]*z[18];
    z[18]=n<T>(1,2)*z[28] + z[18];
    z[18]=z[8]*z[18];
    z[19]= - static_cast<T>(5)+ z[59];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[13]=n<T>(1,2)*z[18] + z[13];
    z[13]=z[6]*z[13];
    z[18]=13*z[7];
    z[19]= - n<T>(51,2) + z[18];
    z[20]=z[8]*z[46];
    z[19]=n<T>(1,2)*z[19] + 2*z[20];
    z[19]=z[8]*z[19];
    z[20]= - z[11] - z[35];
    z[20]=z[8]*z[20];
    z[20]=z[40] + z[20];
    z[20]=z[20]*z[57];
    z[22]=n<T>(119,4) - z[52];
    z[22]=z[7]*z[22];
    z[19]=z[20] + z[19] - n<T>(227,8) + z[22];
    z[19]=z[6]*z[19];
    z[20]=n<T>(125,2) - z[18];
    z[22]= - z[8]*z[23];
    z[22]=n<T>(3,2) + z[22];
    z[22]=z[8]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[6]*z[19];
    z[19]= - n<T>(1,4) + z[19];
    z[19]=z[6]*z[19];
    z[20]=static_cast<T>(1)+ z[17];
    z[20]=z[20]*z[17];
    z[22]=static_cast<T>(4)- z[26];
    z[22]=z[7]*z[22];
    z[23]= - static_cast<T>(2)- z[17];
    z[23]=z[8]*z[23];
    z[22]=z[23] - static_cast<T>(3)+ z[22];
    z[22]=z[22]*z[49];
    z[23]=n<T>(21,8) - z[7];
    z[20]=z[22] + 9*z[23] + z[20];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(49,8) + z[20];
    z[20]=z[20]*z[51];
    z[22]= - static_cast<T>(7)- z[58];
    z[22]=z[22]*z[55];
    z[22]=z[22] - z[54];
    z[22]=z[5]*z[22];
    z[20]=z[20] + z[22];
    z[20]=z[5]*z[20];
    z[19]=z[19] + z[20];
    z[19]=z[5]*z[19];
    z[13]=z[19] + n<T>(11,8) + z[13];
    z[13]=z[5]*z[13];
    z[19]= - z[30] - z[31];
    z[19]=z[19]*z[57];
    z[20]= - n<T>(13,2) + z[59];
    z[20]=z[7]*z[20];
    z[20]=static_cast<T>(15)+ z[20];
    z[20]=z[7]*z[20];
    z[20]=static_cast<T>(5)+ z[20];
    z[22]= - n<T>(9,2) + 16*z[7];
    z[22]=z[7]*z[22];
    z[22]=n<T>(5,2) + z[22];
    z[22]=z[7]*z[22];
    z[22]=n<T>(5,8) + z[22];
    z[22]=z[8]*z[22];
    z[19]=z[19] + n<T>(1,2)*z[20] + z[22];
    z[19]=z[8]*z[19];
    z[20]=static_cast<T>(15)- n<T>(7,2)*z[7];
    z[20]=z[7]*z[20];
    z[20]=n<T>(15,2) + z[20];
    z[19]=n<T>(1,4)*z[20] + z[19];
    z[19]=z[6]*z[19];
    z[18]=n<T>(31,2) - z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(7,2) + z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(1,2) + z[18];
    z[18]=z[18]*z[17];
    z[20]=static_cast<T>(9)- z[33];
    z[20]=z[7]*z[20];
    z[18]=z[18] + n<T>(21,8) + z[20];
    z[18]=z[8]*z[18];
    z[20]=static_cast<T>(23)- 17*z[7];
    z[20]=z[7]*z[20];
    z[20]=static_cast<T>(25)+ z[20];
    z[18]=z[19] + n<T>(1,8)*z[20] + z[18];
    z[18]=z[6]*z[18];
    z[11]=z[14]*z[11];
    z[11]=n<T>(41,4) + z[11];
    z[14]= - n<T>(3,4) + z[42];
    z[14]=z[7]*z[14];
    z[14]=n<T>(3,4) + z[14];
    z[14]=z[8]*z[14];
    z[11]=n<T>(1,2)*z[11] + z[14];
    z[11]=z[8]*z[11];
    z[14]= - n<T>(53,8) + z[7];
    z[14]=z[7]*z[14];
    z[11]=z[18] + z[11] + n<T>(27,8) + z[14];
    z[11]=z[6]*z[11];
    z[14]=n<T>(1,2) - z[35];
    z[14]=z[14]*z[17];
    z[11]=z[13] - z[2] + z[11] + z[14] - n<T>(9,4) + z[52];
    z[11]=z[5]*z[11];
    z[13]=static_cast<T>(15)+ z[16];
    z[13]=z[7]*z[13];
    z[13]=static_cast<T>(15)+ z[13];
    z[13]=z[7]*z[13];
    z[14]= - static_cast<T>(1)+ n<T>(21,2)*z[7];
    z[14]=z[7]*z[14];
    z[14]=n<T>(15,2) + z[14];
    z[14]=z[7]*z[14];
    z[14]=static_cast<T>(5)+ z[14];
    z[14]=z[7]*z[14];
    z[14]=n<T>(5,4) + z[14];
    z[14]=z[8]*z[14];
    z[13]=z[14] + static_cast<T>(5)+ z[13];
    z[13]=z[8]*z[13];
    z[14]= - z[49]*z[43];
    z[13]=z[14] + n<T>(15,2)*z[27] + z[13];
    z[13]=z[13]*z[45];
    z[14]=static_cast<T>(5)+ n<T>(9,2)*z[7];
    z[14]=z[14]*z[26];
    z[16]=z[7] - n<T>(3,4);
    z[17]= - z[16]*z[44];
    z[17]= - n<T>(7,8) + z[17];
    z[17]=z[7]*z[17];
    z[17]= - n<T>(1,8) + z[17];
    z[17]=z[8]*z[17];
    z[18]=static_cast<T>(9)- n<T>(17,4)*z[7];
    z[18]=z[7]*z[18];
    z[18]=n<T>(9,4) + z[18];
    z[18]=z[7]*z[18];
    z[17]=z[17] + n<T>(9,8) + z[18];
    z[17]=z[8]*z[17];
    z[13]=z[13] + z[17] + static_cast<T>(2)+ z[14];
    z[13]=z[6]*z[13];
    z[14]= - n<T>(21,2) + z[7];
    z[14]=z[7]*z[14];
    z[14]=n<T>(69,4) + z[14];
    z[14]=z[7]*z[14];
    z[14]=n<T>(29,2) + z[14];
    z[17]= - static_cast<T>(2)+ z[33];
    z[17]=z[7]*z[17];
    z[17]= - n<T>(17,8) + z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(17,8) + z[17];
    z[17]=z[7]*z[17];
    z[17]=n<T>(15,8) + z[17];
    z[17]=z[8]*z[17];
    z[14]=n<T>(1,2)*z[14] + z[17];
    z[14]=z[8]*z[14];
    z[17]=static_cast<T>(11)- z[39];
    z[17]=z[7]*z[17];
    z[17]=static_cast<T>(15)+ z[17];
    z[13]=z[13] + n<T>(1,4)*z[17] + z[14];
    z[13]=z[6]*z[13];
    z[14]=n<T>(7,4) + z[7];
    z[14]=z[7]*z[14];
    z[14]=n<T>(3,2) + z[14];
    z[14]=z[14]*z[26];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[8]*z[14];
    z[16]=z[7]*z[16];
    z[16]=n<T>(11,2) + z[16];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[8]*z[14];
    z[16]= - static_cast<T>(11)+ z[7];
    z[16]=z[7]*z[16];
    z[16]=n<T>(9,2) + z[16];

    r += z[10] + z[11] + n<T>(1,2)*z[12] + z[13] + z[14] + z[15] + n<T>(1,4)*
      z[16] + z[21];
 
    return r;
}

template double qqb_2lha_r1868(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1868(const std::array<dd_real,30>&);
#endif
