#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1924(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[2];
    z[5]=k[9];
    z[6]=k[4];
    z[7]=k[13];
    z[8]= - 3*z[4] + static_cast<T>(5)- z[1];
    z[9]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[9]=z[2]*z[9];
    z[10]=static_cast<T>(1)- z[2];
    z[10]=z[6]*z[10];
    z[8]=n<T>(3,2)*z[10] + n<T>(1,2)*z[8] + z[9];
    z[9]=n<T>(1,2)*z[4];
    z[10]= - n<T>(5,2) + z[4];
    z[10]=z[10]*z[9];
    z[11]=n<T>(1,2)*z[6];
    z[12]=z[7] - z[2];
    z[12]=z[12]*z[11];
    z[13]=n<T>(1,2)*z[2];
    z[14]=static_cast<T>(1)- z[13];
    z[12]=3*z[14] + z[12];
    z[12]=z[6]*z[12];
    z[13]= - z[13] + n<T>(3,2) - z[4];
    z[12]=3*z[13] + z[12];
    z[12]=z[12]*z[11];
    z[13]= - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[10]=z[12] + z[10] - z[13];
    z[10]=z[5]*z[10];
    z[12]=n<T>(3,2)*z[2];
    z[14]=z[12] - static_cast<T>(1)- n<T>(1,2)*z[7];
    z[14]=z[14]*z[11];
    z[12]=z[14] + z[12] - static_cast<T>(3)+ z[4];
    z[12]=z[12]*z[11];
    z[14]=static_cast<T>(1)- n<T>(1,4)*z[4];
    z[14]=z[4]*z[14];
    z[10]=n<T>(1,2)*z[10] + z[12] + n<T>(3,8)*z[2] - n<T>(9,8) + z[14];
    z[10]=z[5]*z[10];
    z[8]=n<T>(1,4)*z[8] + z[10];
    z[8]=z[3]*z[8];
    z[9]=static_cast<T>(1)- z[9];
    z[10]=n<T>(3,2) - z[2];
    z[10]=z[6]*z[10];
    z[9]=z[10] + 3*z[9] - z[2];
    z[10]=z[11] + 1;
    z[10]=z[2]*z[10];
    z[10]= - n<T>(5,2) + z[10];
    z[10]=z[10]*z[11];
    z[10]=z[10] + n<T>(3,4)*z[4] + z[13];
    z[10]=z[5]*z[10];
    z[9]=n<T>(1,2)*z[9] + z[10];
    z[9]=z[5]*z[9];
    z[9]= - n<T>(1,4) + z[9];
    z[8]=n<T>(1,2)*z[9] + z[8];
    z[8]=z[3]*z[8];
    z[9]= - static_cast<T>(1)+ z[5];
    z[9]=z[5]*z[9];
    z[8]=n<T>(1,8)*z[9] + z[8];

    r += 3*z[8];
 
    return r;
}

template double qqb_2lha_r1924(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1924(const std::array<dd_real,30>&);
#endif
