#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r325(const std::array<T,30>& k) {
  T z[19];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[10];
    z[5]=k[3];
    z[6]=k[12];
    z[7]=k[11];
    z[8]=2*z[7];
    z[9]=z[8] - 9;
    z[9]=z[9]*z[7];
    z[10]=3*z[5];
    z[11]=z[10] + 2;
    z[12]=z[9] - z[11];
    z[12]=z[6]*z[12];
    z[13]=3*z[2];
    z[14]= - z[3] - 2*z[6];
    z[14]=z[14]*z[13];
    z[15]=z[7]*z[3];
    z[16]=3*z[1];
    z[17]= - static_cast<T>(4)+ z[16];
    z[17]=z[3]*z[17];
    z[12]=z[14] + z[12] + z[15] + static_cast<T>(6)+ z[17];
    z[12]=z[2]*z[12];
    z[14]=z[7] - 5;
    z[17]=z[14]*z[7];
    z[11]=z[17] - z[11];
    z[11]=z[6]*z[11];
    z[18]= - z[3] - z[6];
    z[18]=z[18]*z[13];
    z[16]= - static_cast<T>(7)+ z[16];
    z[16]=z[3]*z[16];
    z[15]=5*z[3] - z[15];
    z[15]=z[7]*z[15];
    z[11]=z[18] + z[11] + z[15] + static_cast<T>(6)+ z[16];
    z[11]=z[2]*z[11];
    z[14]=z[14]*z[8];
    z[14]=z[14] + static_cast<T>(5)- 6*z[5];
    z[14]=z[6]*z[14];
    z[11]=z[11] + static_cast<T>(6)+ z[14];
    z[11]=z[2]*z[11];
    z[10]=z[10] - 4;
    z[14]=z[17] - z[10];
    z[14]=z[6]*z[14];
    z[15]=z[7] - 4;
    z[16]=z[15]*z[7];
    z[11]=z[11] - z[16] + z[14];
    z[11]=z[4]*z[11];
    z[8]= - z[15]*z[8];
    z[9]=z[9] - z[10];
    z[9]=z[6]*z[9];
    z[8]=z[11] + z[12] + z[9] - static_cast<T>(3)+ z[8];
    z[8]=z[4]*z[8];
    z[9]=z[6] - 1;
    z[9]=z[9]*z[16];
    z[10]= - z[6]*z[13];

    r +=  - static_cast<T>(3)+ z[8] + z[9] + z[10];
 
    return r;
}

template double qqb_2lha_r325(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r325(const std::array<dd_real,30>&);
#endif
