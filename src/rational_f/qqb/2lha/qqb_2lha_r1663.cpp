#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1663(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[13];
    z[6]=k[17];
    z[7]=k[3];
    z[8]=k[11];
    z[9]=3*z[6];
    z[10]= - n<T>(3,2) - z[3];
    z[10]=z[2]*z[10];
    z[10]=z[9] + z[10];
    z[11]=z[1]*z[2];
    z[10]=z[10]*z[11];
    z[12]= - n<T>(1,3)*z[3] + n<T>(3,2)*z[6];
    z[13]=n<T>(1,2)*z[2];
    z[13]= - z[3]*z[13];
    z[10]=z[10] + z[13] + z[12];
    z[10]=z[1]*z[10];
    z[13]=z[9] + z[5];
    z[14]=z[3] + z[13];
    z[14]=z[2]*z[14];
    z[9]= - z[9] + z[14];
    z[9]=z[9]*z[11];
    z[11]= - n<T>(3,2) + z[13] + n<T>(1,2)*z[3];
    z[11]=z[2]*z[11];
    z[9]=n<T>(1,2)*z[9] + z[11] - z[12];
    z[9]=z[1]*z[9];
    z[11]= - n<T>(13,3) + z[13];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[1]*z[9];
    z[9]=n<T>(1,3)*z[7] + z[9];
    z[9]=z[4]*z[9];
    z[11]=static_cast<T>(1)- z[8];
    z[9]=z[9] + n<T>(1,3)*z[11] + z[10];
    z[9]=z[4]*z[9];
    z[10]=n<T>(1,2)*z[1];
    z[11]= - static_cast<T>(1)+ z[3];
    z[10]=z[11]*npow(z[2],2)*z[10];
    z[9]=z[10] + z[9];

    r += n<T>(1,2)*z[9];
 
    return r;
}

template double qqb_2lha_r1663(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1663(const std::array<dd_real,30>&);
#endif
