#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1808(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=2*z[5];
    z[7]=static_cast<T>(1)+ z[6];
    z[7]=z[7]*z[6];
    z[7]=static_cast<T>(3)+ z[7];
    z[7]=z[4]*z[7];
    z[8]=z[2] - z[1];
    z[9]= - static_cast<T>(9)- z[6];
    z[9]=z[5]*z[9];
    z[7]=z[7] + z[9] - static_cast<T>(12)+ 5*z[8];
    z[7]=z[2]*z[7];
    z[9]=z[5] + 2;
    z[9]=z[9]*z[5];
    z[9]=z[9] + 1;
    z[10]=4*z[5];
    z[11]=z[10] + 1;
    z[11]=z[11]*z[5];
    z[12]=z[4]*z[11];
    z[12]= - 2*z[9] + z[12];
    z[13]=2*z[4];
    z[12]=z[12]*z[13];
    z[7]=z[12] + z[7];
    z[7]=z[2]*z[7];
    z[12]=3*z[5];
    z[14]=npow(z[4],2);
    z[15]= - static_cast<T>(1)- z[5];
    z[15]=z[15]*z[14]*z[12];
    z[16]= - static_cast<T>(3)- z[5];
    z[16]=z[5]*z[16];
    z[16]=z[4] + z[16] - static_cast<T>(3)+ z[8];
    z[16]=z[2]*z[16];
    z[9]=z[4]*z[9];
    z[9]= - 3*z[9] + z[16];
    z[9]=z[2]*z[9];
    z[9]=z[15] + z[9];
    z[9]=z[2]*z[9];
    z[15]=npow(z[5],2);
    z[16]= - z[15]*npow(z[4],3);
    z[9]=z[16] + z[9];
    z[9]=z[3]*z[9];
    z[6]=static_cast<T>(1)- z[6];
    z[6]=z[5]*z[6];
    z[16]=z[4]*z[15];
    z[6]=z[6] + 4*z[16];
    z[6]=z[6]*z[14];
    z[6]=2*z[9] + z[6] + z[7];
    z[6]=z[3]*z[6];
    z[7]=z[13]*z[15];
    z[9]= - z[7] + static_cast<T>(1)+ z[11];
    z[9]=z[4]*z[9];
    z[9]=z[9] - z[12] - static_cast<T>(7)+ 4*z[8];
    z[9]=z[2]*z[9];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[5]*z[10];
    z[10]=z[10] - z[7];
    z[10]=z[4]*z[10];
    z[10]=z[5] + z[10];
    z[10]=z[4]*z[10];
    z[6]=z[6] + z[10] + z[9];
    z[6]=z[3]*z[6];
    z[7]= - z[5] - z[7];
    z[7]=z[4]*z[7];

    r +=  - static_cast<T>(1)+ z[6] + z[7] + z[8];
 
    return r;
}

template double qqb_2lha_r1808(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1808(const std::array<dd_real,30>&);
#endif
