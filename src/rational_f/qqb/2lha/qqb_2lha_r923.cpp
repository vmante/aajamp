#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r923(const std::array<T,30>& k) {
  T z[43];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[20];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[12];
    z[9]=k[5];
    z[10]=k[11];
    z[11]=k[9];
    z[12]=static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[13]=n<T>(1,2)*z[4];
    z[14]=npow(z[7],2);
    z[15]= - static_cast<T>(1)+ n<T>(3,2)*z[14];
    z[15]=z[5]*z[15];
    z[15]=z[15] - 3*z[12] - z[13];
    z[16]=n<T>(1,4)*z[2];
    z[17]= - static_cast<T>(3)- z[11];
    z[17]=z[17]*z[16];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[2]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[9]*z[15];
    z[17]= - static_cast<T>(5)- z[11];
    z[17]=z[17]*z[16];
    z[18]=static_cast<T>(1)- n<T>(1,4)*z[11];
    z[19]=z[7] - 1;
    z[20]= - z[5]*z[19];
    z[15]=z[15] + z[17] + z[20] + 3*z[18] + z[13];
    z[15]=z[9]*z[15];
    z[17]=z[5] - n<T>(1,2);
    z[15]=n<T>(1,2)*z[17] + z[15];
    z[18]=z[5] + z[4];
    z[20]=n<T>(1,4)*z[18];
    z[21]=n<T>(1,4)*z[10];
    z[22]=n<T>(1,8)*z[1];
    z[23]=z[22] - static_cast<T>(1)+ z[21] + z[20];
    z[24]= - static_cast<T>(5)+ 3*z[10] - z[18];
    z[25]=z[2] - 1;
    z[26]= - z[2]*z[25];
    z[24]=n<T>(1,4)*z[24] + z[26];
    z[26]=n<T>(1,2)*z[9];
    z[27]=static_cast<T>(1)- n<T>(7,2)*z[2];
    z[27]=z[2]*z[27];
    z[27]=n<T>(5,2) + z[27];
    z[27]=z[27]*z[26];
    z[24]=3*z[24] + z[27];
    z[24]=z[24]*z[26];
    z[27]=n<T>(1,2)*z[2];
    z[28]= - static_cast<T>(3)+ n<T>(13,4)*z[2];
    z[28]=z[28]*z[27];
    z[29]=z[2] - 3;
    z[29]=z[29]*z[27];
    z[30]=z[27] - 1;
    z[31]= - z[2]*z[30];
    z[31]= - n<T>(1,2) + z[31];
    z[31]=z[9]*z[31];
    z[31]=z[31] + static_cast<T>(1)+ z[29];
    z[31]=z[3]*z[31];
    z[23]=n<T>(9,2)*z[31] + z[24] + 3*z[23] + z[28];
    z[23]=z[3]*z[23];
    z[24]=n<T>(3,4)*z[2];
    z[28]=static_cast<T>(1)- n<T>(5,2)*z[2];
    z[28]=z[28]*z[24];
    z[31]=3*z[2];
    z[32]=static_cast<T>(1)- n<T>(1,8)*z[2];
    z[32]=z[32]*z[31];
    z[33]=z[10] - 7;
    z[34]=z[4] + z[33];
    z[32]=n<T>(1,8)*z[34] + z[32];
    z[32]=z[9]*z[32];
    z[34]=n<T>(1,8)*z[4];
    z[35]=static_cast<T>(1)- n<T>(1,4)*z[8];
    z[28]=z[32] + z[28] - n<T>(1,8)*z[5] + 5*z[35] - z[34];
    z[28]=z[9]*z[28];
    z[32]= - n<T>(1,2) - z[8];
    z[32]=3*z[32] - z[13];
    z[35]= - z[1]*z[8];
    z[23]=z[23] + z[28] - z[16] + n<T>(3,4)*z[32] + z[35];
    z[23]=z[3]*z[23];
    z[28]=n<T>(3,2)*z[5];
    z[19]=z[19]*z[28];
    z[32]=z[11] + 1;
    z[35]=z[32]*z[27];
    z[36]=z[13] + 1;
    z[19]=z[35] + z[19] - z[36];
    z[19]=z[9]*z[19];
    z[35]= - z[11]*z[27];
    z[19]=z[19] + z[35] + n<T>(1,2)*z[32] - z[5];
    z[19]=z[9]*z[19];
    z[32]=z[9]*z[5];
    z[32]=z[11] + z[32];
    z[32]=z[32]*z[26];
    z[35]=z[18] - 5;
    z[37]=3*z[3];
    z[38]=n<T>(1,2)*z[35] + z[37];
    z[38]=z[3]*z[38];
    z[38]=z[38] - z[5] - z[36];
    z[38]=z[3]*z[38]*npow(z[9],2);
    z[32]=z[32] + z[38];
    z[32]=z[6]*z[32];
    z[38]=n<T>(1,2)*z[11];
    z[19]=z[32] - z[38] + z[19];
    z[32]=n<T>(3,4)*z[10];
    z[20]=z[31] + static_cast<T>(1)- z[32] + z[20];
    z[20]=z[20]*z[26];
    z[39]=n<T>(3,2)*z[10];
    z[40]= - z[39] - n<T>(3,4)*z[8] - z[35];
    z[41]=n<T>(1,4)*z[9];
    z[25]= - z[25]*z[41];
    z[24]=z[25] - static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[37];
    z[20]=z[24] + z[20] + n<T>(1,2)*z[40] + z[2];
    z[20]=z[9]*z[20];
    z[24]=n<T>(1,2)*z[1];
    z[25]= - static_cast<T>(1)- z[24];
    z[25]=z[8]*z[25];
    z[20]=n<T>(3,4)*z[25] + z[20];
    z[20]=z[3]*z[20];
    z[25]=5*z[2] - static_cast<T>(13)- z[5];
    z[25]=z[25]*z[41];
    z[40]=n<T>(3,2)*z[2];
    z[42]=static_cast<T>(7)+ 3*z[4];
    z[25]=z[25] + z[40] + n<T>(1,4)*z[42] + z[5];
    z[25]=z[25]*z[26];
    z[20]=z[25] + z[20];
    z[20]=z[3]*z[20];
    z[19]=z[20] + n<T>(1,4)*z[19];
    z[19]=z[6]*z[19];
    z[15]=z[19] + n<T>(1,2)*z[15] + z[23];
    z[15]=z[6]*z[15];
    z[19]=n<T>(1,2)*z[5];
    z[20]=z[19] - z[10] + z[36];
    z[23]=n<T>(17,4) - z[18];
    z[23]=z[1]*z[23];
    z[20]=n<T>(3,2)*z[20] + z[23];
    z[23]=z[24] + z[33];
    z[25]=n<T>(9,2) - z[1];
    z[25]=n<T>(1,2)*z[25] - z[2];
    z[25]=z[2]*z[25];
    z[23]=n<T>(1,4)*z[23] + z[25];
    z[23]=z[23]*z[31];
    z[25]= - n<T>(9,2) + z[2];
    z[25]=z[25]*z[27];
    z[25]=static_cast<T>(3)+ z[25];
    z[25]=z[2]*z[25];
    z[25]= - n<T>(5,4) + z[25];
    z[25]=z[9]*z[25];
    z[26]= - static_cast<T>(3)+ z[1];
    z[26]=z[26]*z[27];
    z[31]=n<T>(3,2) - z[1];
    z[26]=3*z[31] + z[26];
    z[26]=z[2]*z[26];
    z[31]=z[1] - 1;
    z[26]=n<T>(9,2)*z[31] + z[26];
    z[26]=z[26]*z[27];
    z[26]=z[26] + n<T>(3,4) - z[1];
    z[26]=z[26]*z[37];
    z[20]=z[26] + z[25] + n<T>(1,2)*z[20] + z[23];
    z[20]=z[3]*z[20];
    z[23]=n<T>(5,2)*z[5] - n<T>(23,2) + z[4];
    z[25]=static_cast<T>(13)+ z[4];
    z[25]=n<T>(1,4)*z[25] - z[5];
    z[25]=z[1]*z[25];
    z[26]=z[27] + n<T>(13,4) - z[1];
    z[26]=z[2]*z[26];
    z[23]=z[26] + n<T>(1,2)*z[23] + z[25];
    z[25]=n<T>(13,2) - z[4] - z[10];
    z[26]= - static_cast<T>(3)+ n<T>(13,8)*z[2];
    z[26]=z[2]*z[26];
    z[26]= - n<T>(1,2) + z[26];
    z[26]=z[2]*z[26];
    z[33]=n<T>(7,8)*z[2];
    z[36]=static_cast<T>(1)- z[33];
    z[36]=z[2]*z[36];
    z[36]= - n<T>(1,8) + z[36];
    z[36]=z[9]*z[36];
    z[25]=z[36] + n<T>(1,4)*z[25] + z[26];
    z[25]=z[9]*z[25];
    z[20]=z[20] + n<T>(1,2)*z[23] + z[25];
    z[20]=z[3]*z[20];
    z[23]=z[7] - z[11];
    z[25]=static_cast<T>(31)- 11*z[8];
    z[26]=n<T>(1,2) - z[14];
    z[26]=z[5]*z[26];
    z[23]=z[26] + z[13] + n<T>(1,2)*z[25] + z[10] + n<T>(5,2)*z[23];
    z[25]=z[7] + 1;
    z[25]=z[25]*z[5];
    z[14]=z[14]*z[25];
    z[26]= - static_cast<T>(5)- z[7];
    z[26]=z[7]*z[26];
    z[14]=z[14] + static_cast<T>(3)+ z[26];
    z[26]=z[40] - static_cast<T>(3)+ z[38];
    z[26]=z[2]*z[26];
    z[36]=z[7] + static_cast<T>(13)- 3*z[11];
    z[26]=n<T>(1,2)*z[36] + z[26];
    z[26]=z[2]*z[26];
    z[14]=n<T>(1,2)*z[14] + z[26];
    z[14]=z[14]*z[41];
    z[26]=z[33] - static_cast<T>(2)+ z[38];
    z[26]=z[2]*z[26];
    z[14]=z[14] + n<T>(1,4)*z[23] + z[26];
    z[14]=z[9]*z[14];
    z[23]= - z[8] - z[19];
    z[23]=z[23]*z[24];
    z[25]=z[11] + z[25] + z[2];
    z[14]=z[15] + z[20] + z[14] + z[23] - z[34] - n<T>(11,8) - 2*z[8] + n<T>(3,8)
   *z[25];
    z[14]=z[6]*z[14];
    z[15]= - z[30]*z[40];
    z[20]= - z[29] - n<T>(5,2) + z[11];
    z[20]=z[20]*z[27];
    z[23]=n<T>(3,2) - z[11];
    z[20]=z[20] + n<T>(1,2)*z[23] + z[7];
    z[20]=z[9]*z[20];
    z[15]=z[20] + z[15] - n<T>(3,4)*z[7] - n<T>(7,4) + z[11];
    z[15]=z[2]*z[15];
    z[20]=static_cast<T>(1)- z[11];
    z[23]=n<T>(11,4) - z[8];
    z[23]=z[7]*z[23];
    z[15]=n<T>(1,2)*z[20] + z[23] + z[15];
    z[15]=z[9]*z[15];
    z[20]=z[7]*z[12];
    z[20]=n<T>(1,2) + z[20];
    z[19]=z[20]*z[19];
    z[20]=n<T>(1,4)*z[1];
    z[12]=z[20] - z[12];
    z[12]=z[5]*z[12];
    z[12]=n<T>(3,2) + z[12];
    z[12]=z[1]*z[12];
    z[23]=z[32] - z[30];
    z[23]=z[2]*z[23];
    z[25]=z[11] - static_cast<T>(3)- z[10];
    z[21]=z[21] - n<T>(5,4) - z[8];
    z[21]=z[7]*z[21];
    z[12]=z[15] + z[23] + z[12] + z[19] + n<T>(1,2)*z[25] + z[21];
    z[15]= - static_cast<T>(25)- z[1];
    z[15]=z[15]*z[24];
    z[19]=n<T>(17,2) + z[10];
    z[21]=n<T>(11,2)*z[2] + 5*z[1] - static_cast<T>(19)- z[39];
    z[21]=z[2]*z[21];
    z[15]=z[21] + 3*z[19] + z[15];
    z[15]=z[15]*z[16];
    z[19]=n<T>(9,2) + z[1];
    z[19]=z[19]*z[24];
    z[15]=z[15] + z[19] - static_cast<T>(4)- n<T>(3,8)*z[10];
    z[15]=z[2]*z[15];
    z[19]=z[31]*z[20];
    z[16]= - z[16] + static_cast<T>(1)- z[20];
    z[16]=z[2]*z[16];
    z[20]= - static_cast<T>(1)+ z[24];
    z[16]=n<T>(3,2)*z[20] + z[16];
    z[16]=z[2]*z[1]*z[16];
    z[20]=static_cast<T>(1)- n<T>(3,4)*z[1];
    z[20]=z[1]*z[20];
    z[16]=z[20] + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[19] + z[16];
    z[16]=z[16]*z[37];
    z[19]=z[1]*z[35];
    z[18]=z[19] - static_cast<T>(1)- z[18];
    z[18]=z[18]*z[22];
    z[15]=z[16] + z[15] + static_cast<T>(1)+ z[18];
    z[15]=z[3]*z[15];
    z[13]=z[13] - 3;
    z[16]=z[1]*z[17];
    z[16]=z[16] - z[28] - z[13];
    z[16]=z[1]*z[16];
    z[17]= - z[2] - z[10] + 3*z[1];
    z[17]=z[17]*z[27];
    z[17]= - 3*z[31] + z[17];
    z[17]=z[2]*z[17];
    z[13]=z[17] + z[16] + n<T>(1,2)*z[10] + z[13];
    z[16]=n<T>(11,4) - z[2];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(9,4) + z[16];
    z[16]=z[2]*z[16];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[2]*z[16];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[9]*z[16];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[3]*z[13];

    r += n<T>(1,2)*z[12] + z[13] + z[14];
 
    return r;
}

template double qqb_2lha_r923(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r923(const std::array<dd_real,30>&);
#endif
