#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2286(const std::array<T,30>& k) {
  T z[7];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=z[4]*z[2];
    z[6]=z[2] + static_cast<T>(1)+ z[1];
    z[6]=z[2]*z[6];
    z[6]=z[1] + z[6];
    z[6]=z[4]*z[6];
    z[6]= - z[2] + z[6];
    z[6]=z[3]*z[6];
    z[5]=z[6] + n<T>(1,2) + z[5];

    r += z[5]*z[4]*z[3];
 
    return r;
}

template double qqb_2lha_r2286(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2286(const std::array<dd_real,30>&);
#endif
