#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r783(const std::array<T,30>& k) {
  T z[8];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[8];
    z[4]=k[4];
    z[5]=npow(z[4],2);
    z[6]=z[1]*z[3];
    z[7]= - static_cast<T>(3)+ z[6];
    z[7]=z[1]*z[7];
    z[7]=3*z[4] + z[7];
    z[7]=z[1]*z[7];
    z[5]= - z[5] + z[7];
    z[7]=n<T>(1,2)*z[2];
    z[5]=z[5]*z[7];
    z[6]= - static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[6]=z[1]*z[6];
    z[5]=z[5] + n<T>(1,2)*z[4] + z[6];

    r += z[7]*z[5];
 
    return r;
}

template double qqb_2lha_r783(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r783(const std::array<dd_real,30>&);
#endif
