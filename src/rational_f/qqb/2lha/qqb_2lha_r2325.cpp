#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2325(const std::array<T,30>& k) {
  T z[67];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[17];
    z[6]=k[6];
    z[7]=k[20];
    z[8]=k[2];
    z[9]=k[3];
    z[10]=n<T>(1,2)*z[3];
    z[11]=3*z[3];
    z[12]= - static_cast<T>(13)+ z[11];
    z[12]=z[12]*z[10];
    z[13]=z[5] - n<T>(1,2);
    z[13]=z[13]*z[5];
    z[13]=z[13] - n<T>(1,2);
    z[14]=15*z[5];
    z[15]=z[13]*z[14];
    z[12]=z[12] + z[15];
    z[15]=n<T>(1,4)*z[3];
    z[16]=static_cast<T>(7)- z[11];
    z[16]=z[16]*z[15];
    z[17]=3*z[5];
    z[18]=static_cast<T>(1)+ z[5];
    z[18]=z[18]*z[17];
    z[18]=n<T>(7,4) + z[18];
    z[18]=z[5]*z[18];
    z[16]=z[16] + z[18];
    z[16]=z[2]*z[16];
    z[12]=n<T>(1,2)*z[12] + z[16];
    z[12]=z[2]*z[12];
    z[16]= - static_cast<T>(9)+ z[5];
    z[16]=z[5]*z[16];
    z[16]=n<T>(5,2) + z[16];
    z[16]=z[16]*z[17];
    z[18]=static_cast<T>(1)+ n<T>(7,2)*z[3];
    z[16]=z[16] - z[7] + z[18];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[2]*z[12];
    z[16]=n<T>(3,2) - z[5];
    z[16]=z[16]*z[17];
    z[16]=n<T>(7,2) + z[16];
    z[16]=z[5]*z[16];
    z[19]= - 15*z[3] - static_cast<T>(9)+ z[7];
    z[19]=n<T>(1,2)*z[19] + z[9];
    z[12]=z[12] + n<T>(1,2)*z[19] + z[16];
    z[16]=3*z[7];
    z[19]=static_cast<T>(49)- z[16];
    z[20]= - static_cast<T>(35)+ 11*z[9];
    z[20]=z[9]*z[20];
    z[19]=z[20] + n<T>(1,2)*z[19] + z[3];
    z[20]=z[9] - 2;
    z[20]=z[20]*z[9];
    z[21]=z[20] + 1;
    z[22]=5*z[6];
    z[23]= - z[21]*z[22];
    z[19]=n<T>(1,2)*z[19] + z[23];
    z[19]=z[6]*z[19];
    z[23]=n<T>(1,2)*z[7];
    z[24]= - static_cast<T>(33)- z[23];
    z[24]=n<T>(1,2)*z[24] - z[11];
    z[25]=4*z[9];
    z[26]=n<T>(109,4) - z[25];
    z[26]=z[9]*z[26];
    z[19]=3*z[19] + n<T>(3,2)*z[24] + z[26];
    z[19]=z[6]*z[19];
    z[24]=39*z[3] + static_cast<T>(43)+ 15*z[7];
    z[26]=n<T>(1,2)*z[9];
    z[27]= - static_cast<T>(9)+ z[26];
    z[27]=z[9]*z[27];
    z[24]=n<T>(1,4)*z[24] + z[27];
    z[19]=z[19] + n<T>(1,2)*z[24] - z[17];
    z[19]=z[6]*z[19];
    z[12]=n<T>(1,2)*z[12] + z[19];
    z[12]=z[4]*z[12];
    z[19]=z[5] + n<T>(1,2);
    z[24]=z[19]*z[17];
    z[27]=z[24] + 1;
    z[27]=z[27]*z[5];
    z[28]=z[10] - 1;
    z[29]=z[28]*z[3];
    z[27]=z[27] - z[29];
    z[29]=n<T>(1,4)*z[2];
    z[30]=z[27]*z[29];
    z[31]=z[3] - 3;
    z[32]=z[31]*z[15];
    z[33]= - static_cast<T>(1)- n<T>(15,4)*z[5];
    z[33]=z[5]*z[33];
    z[33]= - n<T>(3,4) + z[33];
    z[33]=z[5]*z[33];
    z[32]= - z[30] + z[32] + z[33];
    z[32]=z[2]*z[32];
    z[31]=z[31]*z[10];
    z[33]=z[7] - z[31];
    z[34]=static_cast<T>(1)+ n<T>(7,2)*z[5];
    z[34]=z[5]*z[34];
    z[32]=z[32] + n<T>(1,2)*z[33] + z[34];
    z[32]=z[2]*z[32];
    z[33]=n<T>(271,2) - 23*z[9];
    z[33]=z[33]*z[26];
    z[34]=30*z[6];
    z[35]=z[21]*z[34];
    z[36]= - n<T>(77,2) + z[16];
    z[36]=n<T>(1,2)*z[36] - z[3];
    z[33]=z[35] + 3*z[36] + z[33];
    z[33]=z[6]*z[33];
    z[35]=static_cast<T>(1)+ n<T>(1,8)*z[7];
    z[35]=7*z[35] + z[11];
    z[33]=z[33] + 3*z[35] - n<T>(77,4)*z[9];
    z[33]=z[6]*z[33];
    z[35]=3*z[9];
    z[36]=19*z[5] + z[35] - n<T>(77,2)*z[3] - static_cast<T>(9)- n<T>(11,2)*z[7];
    z[33]=n<T>(1,4)*z[36] + z[33];
    z[33]=z[6]*z[33];
    z[36]=n<T>(13,4)*z[3];
    z[37]=21*z[5];
    z[38]= - static_cast<T>(25)+ z[37];
    z[38]=z[5]*z[38];
    z[38]=n<T>(11,2) + z[38];
    z[38]=z[5]*z[38];
    z[12]=z[12] + z[33] + z[32] + n<T>(1,4)*z[38] + z[7] + z[36];
    z[12]=z[4]*z[12];
    z[32]=13*z[9];
    z[33]=n<T>(3,4)*z[5];
    z[38]=11*z[3];
    z[39]= - z[38] - static_cast<T>(57)+ n<T>(7,2)*z[7];
    z[39]=z[33] + n<T>(1,2)*z[39] + z[32];
    z[40]=z[9] - 1;
    z[41]=10*z[6];
    z[40]=z[40]*z[41];
    z[40]=z[40] + z[3];
    z[42]=static_cast<T>(43)- z[16];
    z[42]= - 16*z[9] + n<T>(1,2)*z[42] + z[40];
    z[42]=z[6]*z[42];
    z[39]=n<T>(1,2)*z[39] + z[42];
    z[39]=z[6]*z[39];
    z[42]=n<T>(1,8)*z[5];
    z[14]=static_cast<T>(7)- z[14];
    z[14]=z[14]*z[42];
    z[43]=n<T>(5,4)*z[7];
    z[14]=z[39] + z[14] - z[26] + n<T>(21,8)*z[3] + static_cast<T>(3)+ z[43];
    z[14]=z[6]*z[14];
    z[39]=5*z[5];
    z[44]=n<T>(3,2)*z[5];
    z[45]= - static_cast<T>(1)+ z[44];
    z[45]=z[45]*z[39];
    z[46]=n<T>(5,2)*z[7];
    z[18]=z[45] - z[46] - z[18];
    z[45]= - static_cast<T>(1)+ n<T>(3,8)*z[3];
    z[45]=z[45]*z[3];
    z[47]= - static_cast<T>(1)- n<T>(15,8)*z[5];
    z[47]=z[5]*z[47];
    z[30]=z[30] + z[45] + z[47];
    z[30]=z[2]*z[30];
    z[47]= - n<T>(1,8) - z[5];
    z[47]=z[47]*z[17];
    z[47]=n<T>(5,8) + z[47];
    z[47]=z[5]*z[47];
    z[48]= - z[7] + n<T>(3,2)*z[3];
    z[30]=z[30] + n<T>(1,4)*z[48] + z[47];
    z[30]=z[2]*z[30];
    z[14]=z[14] + n<T>(1,4)*z[18] + z[30];
    z[14]=z[4]*z[14];
    z[18]=n<T>(1,2)*z[5];
    z[30]=static_cast<T>(7)+ 11*z[5];
    z[30]=z[30]*z[18];
    z[47]=n<T>(5,2)*z[3];
    z[30]=z[30] + z[16] + z[47];
    z[48]=n<T>(1,2)*z[2];
    z[49]=z[27]*z[48];
    z[50]=n<T>(7,2) - z[3];
    z[50]=z[3]*z[50];
    z[51]=n<T>(13,2) + z[17];
    z[51]=z[5]*z[51];
    z[51]=n<T>(7,2) + z[51];
    z[51]=z[5]*z[51];
    z[50]= - z[49] + z[50] + z[51];
    z[50]=z[2]*z[50];
    z[30]=n<T>(1,2)*z[30] + z[50];
    z[50]= - n<T>(65,2) + z[16];
    z[40]=n<T>(11,2)*z[9] + n<T>(1,2)*z[50] - z[40];
    z[40]=z[6]*z[40];
    z[50]=z[38] + static_cast<T>(31)- z[7];
    z[40]=z[40] + n<T>(1,4)*z[50] - 2*z[5];
    z[40]=z[6]*z[40];
    z[50]=z[5] - 1;
    z[51]=z[5]*z[50];
    z[51]=z[51] - 5*z[3] - static_cast<T>(1)- z[7];
    z[40]=n<T>(1,2)*z[51] + z[40];
    z[40]=z[6]*z[40];
    z[14]=z[14] + n<T>(1,2)*z[30] + z[40];
    z[14]=z[4]*z[14];
    z[30]=static_cast<T>(7)- z[17];
    z[30]=z[30]*z[42];
    z[40]=n<T>(5,2)*z[5];
    z[42]=static_cast<T>(7)- z[23];
    z[42]= - z[40] + 3*z[42] + z[3];
    z[42]=n<T>(1,2)*z[42] - z[22];
    z[42]=z[6]*z[42];
    z[51]=n<T>(5,8)*z[3];
    z[30]=n<T>(1,2)*z[42] + z[30] - z[51] - static_cast<T>(3)+ z[23];
    z[30]=z[6]*z[30];
    z[42]= - z[50]*z[17];
    z[42]=n<T>(1,2) + z[42];
    z[42]=z[42]*z[18];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[42] + n<T>(1,2)*z[23] + z[3];
    z[23]=n<T>(1,2)*z[23] + z[30];
    z[23]=z[6]*z[23];
    z[13]=z[13]*z[17];
    z[30]=z[27]*z[2];
    z[42]=npow(z[5],3);
    z[50]= - 3*z[42] - z[30];
    z[50]=z[2]*z[50];
    z[13]=z[50] + z[13] - z[7] - z[10];
    z[13]=n<T>(1,4)*z[13] + z[23];
    z[13]=z[4]*z[13];
    z[23]=n<T>(3,2)*z[42] + z[30];
    z[13]=n<T>(1,2)*z[23] + z[13];
    z[13]=z[4]*z[13];
    z[13]= - n<T>(1,4)*z[27] + z[13];
    z[13]=z[1]*z[13];
    z[23]=n<T>(1,2)*z[8];
    z[27]=z[23] + 1;
    z[30]=z[15]*z[8];
    z[42]=z[30] - z[27];
    z[42]=z[42]*z[10];
    z[50]=z[8] + 1;
    z[52]= - z[50]*z[33];
    z[53]=n<T>(3,8)*z[8];
    z[52]=z[52] - static_cast<T>(1)- z[53];
    z[52]=z[5]*z[52];
    z[52]= - n<T>(1,2)*z[27] + z[52];
    z[52]=z[5]*z[52];
    z[13]=z[13] + z[14] + z[42] + z[52];
    z[13]=z[1]*z[13];
    z[14]=z[8] - n<T>(3,2);
    z[42]=z[23] - 1;
    z[52]= - z[3]*z[42];
    z[52]=z[52] + z[14];
    z[52]=z[52]*z[10];
    z[27]=z[27]*z[17];
    z[54]= - static_cast<T>(7)+ 3*z[8];
    z[27]=n<T>(1,4)*z[54] + z[27];
    z[27]=z[5]*z[27];
    z[14]=n<T>(1,2)*z[14] + z[27];
    z[14]=z[5]*z[14];
    z[14]=z[49] + z[52] + z[14];
    z[14]=z[14]*z[48];
    z[27]= - static_cast<T>(31)- n<T>(19,2)*z[9];
    z[27]=z[27]*z[26];
    z[49]= - z[10] + n<T>(3,4)*z[7];
    z[52]=static_cast<T>(7)- z[49];
    z[21]=z[6]*z[21];
    z[21]= - 15*z[21] + 3*z[52] + z[27];
    z[21]=z[6]*z[21];
    z[27]=5*z[9];
    z[52]= - n<T>(1,2) - z[9];
    z[52]=z[52]*z[27];
    z[54]=9*z[3];
    z[52]=z[52] - z[54] - static_cast<T>(1)- n<T>(9,2)*z[7];
    z[21]=n<T>(1,2)*z[52] + z[21];
    z[21]=z[6]*z[21];
    z[52]=n<T>(25,2) - z[16];
    z[52]=n<T>(1,2)*z[52] + z[54];
    z[54]=2*z[9];
    z[21]=z[21] - z[17] + n<T>(1,2)*z[52] - z[54];
    z[21]=z[6]*z[21];
    z[52]=z[8] + 3;
    z[52]=z[52]*z[8];
    z[52]=z[52] + 5;
    z[55]=z[52]*z[33];
    z[56]=n<T>(1,4)*z[8];
    z[57]=static_cast<T>(3)+ z[23];
    z[57]=z[57]*z[56];
    z[57]= - z[55] + static_cast<T>(3)+ z[57];
    z[57]=z[5]*z[57];
    z[57]=z[57] - n<T>(21,8) - z[8];
    z[57]=z[5]*z[57];
    z[58]=npow(z[8],2);
    z[59]=static_cast<T>(1)+ z[58];
    z[59]=z[59]*z[15];
    z[59]= - static_cast<T>(1)+ z[59];
    z[59]=z[3]*z[59];
    z[60]=5*z[7];
    z[61]=n<T>(3,2) - z[60];
    z[59]=n<T>(1,2)*z[61] + z[59];
    z[12]=z[13] + z[12] + z[21] + z[14] + n<T>(1,2)*z[59] + z[57];
    z[12]=z[1]*z[12];
    z[13]=19*z[9];
    z[14]=n<T>(33,2) + z[13];
    z[14]=z[14]*z[26];
    z[14]= - static_cast<T>(46)+ z[14];
    z[14]=z[9]*z[14];
    z[21]= - n<T>(47,2) + z[16];
    z[57]= - n<T>(3,4) - z[27];
    z[57]=z[9]*z[57];
    z[57]=static_cast<T>(17)+ z[57];
    z[57]=z[9]*z[57];
    z[21]=z[57] + n<T>(1,2)*z[21] - z[3];
    z[21]=z[2]*z[21];
    z[57]=z[9] - 3;
    z[57]=z[57]*z[9];
    z[57]=z[57] + 3;
    z[57]=z[57]*z[9];
    z[57]=z[57] - 1;
    z[59]=z[57]*z[41];
    z[61]=static_cast<T>(2)- z[2];
    z[61]=z[61]*z[59];
    z[62]=2*z[3];
    z[63]=n<T>(39,4) - z[7];
    z[14]=z[61] + z[21] + z[14] + 3*z[63] + z[62];
    z[14]=z[6]*z[14];
    z[21]=static_cast<T>(69)- 49*z[7];
    z[21]=n<T>(1,4)*z[21] - z[38];
    z[38]=npow(z[9],2);
    z[38]= - static_cast<T>(1)+ z[38];
    z[38]=z[38]*z[27];
    z[61]= - static_cast<T>(25)+ 13*z[7];
    z[63]=13*z[3];
    z[64]=z[63] + z[61];
    z[65]=n<T>(21,4) - z[25];
    z[65]=z[9]*z[65];
    z[64]=n<T>(1,4)*z[64] + z[65];
    z[64]=z[2]*z[64];
    z[14]=z[14] + z[64] + n<T>(1,2)*z[21] + z[38];
    z[14]=z[6]*z[14];
    z[21]=7*z[3];
    z[38]=n<T>(3,2)*z[9];
    z[64]=z[38] - z[21] + static_cast<T>(3)+ z[60];
    z[64]=z[64]*z[48];
    z[65]=n<T>(37,2)*z[3] - static_cast<T>(11)- 19*z[7];
    z[14]=z[14] + z[64] + n<T>(1,4)*z[65] + 4*z[20];
    z[14]=z[6]*z[14];
    z[64]= - static_cast<T>(5)+ z[10];
    z[64]=z[64]*z[15];
    z[65]=n<T>(7,8) + z[5];
    z[65]=z[5]*z[65];
    z[65]= - n<T>(3,4) + z[65];
    z[65]=z[65]*z[17];
    z[66]=n<T>(5,2) + z[5];
    z[66]=z[66]*z[33];
    z[66]=static_cast<T>(1)+ z[66];
    z[66]=z[5]*z[66];
    z[45]= - z[45] + z[66];
    z[45]=z[2]*z[45];
    z[45]=z[45] + z[64] + z[65];
    z[45]=z[2]*z[45];
    z[40]= - static_cast<T>(3)+ z[40];
    z[40]=z[5]*z[40];
    z[40]= - n<T>(7,4) + z[40];
    z[40]=z[40]*z[44];
    z[40]=z[45] + z[40] + static_cast<T>(1)+ z[51];
    z[40]=z[2]*z[40];
    z[45]=static_cast<T>(55)- z[16];
    z[51]=n<T>(77,2) - 6*z[9];
    z[51]=z[9]*z[51];
    z[51]= - n<T>(119,2) + z[51];
    z[51]=z[9]*z[51];
    z[45]=z[59] + z[51] + n<T>(1,2)*z[45] + z[3];
    z[45]=z[6]*z[45];
    z[51]= - static_cast<T>(13)- z[7];
    z[51]=n<T>(11,2)*z[51] - z[63];
    z[59]= - n<T>(51,2) + z[9];
    z[59]=z[9]*z[59];
    z[59]=n<T>(133,2) + z[59];
    z[59]=z[9]*z[59];
    z[51]=n<T>(1,2)*z[51] + z[59];
    z[45]=n<T>(1,2)*z[51] + z[45];
    z[45]=z[6]*z[45];
    z[51]=n<T>(31,4)*z[3] + n<T>(5,4) + z[7];
    z[59]= - n<T>(27,4) + z[9];
    z[59]=z[9]*z[59];
    z[45]=z[45] + n<T>(1,2)*z[51] + z[59];
    z[45]=z[6]*z[45];
    z[51]=z[7] - n<T>(1,2);
    z[59]= - z[36] + z[51];
    z[63]= - n<T>(7,2) + z[5];
    z[63]=z[63]*z[17];
    z[63]=n<T>(23,2) + z[63];
    z[63]=z[63]*z[18];
    z[40]=z[45] + z[40] + z[63] + n<T>(1,2)*z[59] + z[9];
    z[40]=z[4]*z[40];
    z[28]=z[28]*z[10];
    z[45]=z[5] + n<T>(3,2);
    z[45]=z[45]*z[17];
    z[45]=z[45] + n<T>(5,2);
    z[45]=z[45]*z[5];
    z[59]=z[3] - n<T>(5,2);
    z[59]=z[59]*z[3];
    z[45]=z[45] - z[59];
    z[59]= - z[45]*z[48];
    z[63]=n<T>(21,2)*z[5];
    z[64]= - static_cast<T>(5)- z[63];
    z[64]=z[64]*npow(z[5],2);
    z[28]=z[59] + z[28] + z[64];
    z[28]=z[2]*z[28];
    z[59]= - static_cast<T>(15)+ 13*z[5];
    z[17]= - z[59]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[5]*z[17];
    z[64]= - z[7] + n<T>(9,2)*z[3];
    z[17]=z[17] - static_cast<T>(1)- z[64];
    z[17]=n<T>(1,2)*z[17] + z[28];
    z[17]=z[2]*z[17];
    z[28]=37*z[3] - static_cast<T>(3)- z[7];
    z[65]=static_cast<T>(67)- z[37];
    z[65]=z[5]*z[65];
    z[65]= - n<T>(125,2) + z[65];
    z[65]=z[5]*z[65];
    z[28]=z[65] + n<T>(1,2)*z[28] - z[9];
    z[17]=n<T>(1,2)*z[28] + z[17];
    z[28]= - n<T>(307,2) + z[32];
    z[28]=z[9]*z[28];
    z[28]=static_cast<T>(271)+ z[28];
    z[28]=z[28]*z[26];
    z[32]= - z[57]*z[34];
    z[34]= - n<T>(89,2) + z[16];
    z[34]=n<T>(1,2)*z[34] - z[3];
    z[28]=z[32] + 3*z[34] + z[28];
    z[28]=z[6]*z[28];
    z[32]= - static_cast<T>(15)+ n<T>(17,4)*z[9];
    z[32]=z[9]*z[32];
    z[32]=z[32] + z[36] + static_cast<T>(7)+ 2*z[7];
    z[28]=3*z[32] + z[28];
    z[28]=z[6]*z[28];
    z[32]=n<T>(1,4)*z[9];
    z[34]=static_cast<T>(6)- z[32];
    z[34]=z[9]*z[34];
    z[28]=z[28] + z[34] - n<T>(47,4)*z[3] + n<T>(79,8) + z[7];
    z[28]=z[6]*z[28];
    z[17]=z[40] + n<T>(1,2)*z[17] + z[28];
    z[17]=z[4]*z[17];
    z[28]=static_cast<T>(3)+ 5*z[8];
    z[28]=n<T>(1,2)*z[28] - z[3];
    z[28]=z[28]*z[15];
    z[34]=z[8] + 5;
    z[36]=z[34]*z[44];
    z[36]=z[36] - static_cast<T>(1)+ n<T>(9,4)*z[8];
    z[36]=z[5]*z[36];
    z[40]= - static_cast<T>(17)+ z[8];
    z[36]=n<T>(1,4)*z[40] + z[36];
    z[36]=z[36]*z[18];
    z[40]=z[45]*z[29];
    z[45]=z[7] + 2;
    z[28]=z[40] + z[36] + z[28] + z[45];
    z[28]=z[2]*z[28];
    z[36]= - static_cast<T>(25)- n<T>(13,2)*z[8];
    z[23]=z[36]*z[23];
    z[36]=z[52]*z[44];
    z[23]=z[36] - static_cast<T>(23)+ z[23];
    z[23]=z[5]*z[23];
    z[36]=static_cast<T>(15)+ z[8];
    z[36]=z[8]*z[36];
    z[36]=n<T>(77,2) + z[36];
    z[23]=n<T>(1,2)*z[36] + z[23];
    z[23]=z[23]*z[18];
    z[36]=z[50]*z[30];
    z[40]= - z[8]*z[42];
    z[36]=z[36] + static_cast<T>(1)+ z[40];
    z[36]=z[36]*z[10];
    z[40]= - static_cast<T>(3)+ z[60];
    z[40]=z[40]*z[32];
    z[42]=n<T>(23,8)*z[7];
    z[12]=z[12] + z[17] + z[14] + z[28] + z[23] + z[40] + z[36] - z[42]
    - n<T>(27,8) - 2*z[8];
    z[12]=z[1]*z[12];
    z[14]= - n<T>(21,2) + z[9];
    z[14]=z[9]*z[14];
    z[14]=static_cast<T>(17)+ z[14];
    z[14]=z[14]*z[38];
    z[17]=n<T>(7,4)*z[3];
    z[14]=z[14] - z[17] - static_cast<T>(9)- z[43];
    z[23]=z[26] - 2;
    z[23]=z[23]*z[9];
    z[23]=z[23] + 3;
    z[23]=z[23]*z[9];
    z[23]=z[23] - 2;
    z[23]=z[23]*z[9];
    z[23]=z[23] + n<T>(1,2);
    z[28]= - z[23]*z[22];
    z[36]=static_cast<T>(61)- z[16];
    z[36]=n<T>(1,2)*z[36] + z[3];
    z[40]= - static_cast<T>(8)+ z[32];
    z[40]=z[9]*z[40];
    z[40]=n<T>(181,8) + z[40];
    z[40]=z[9]*z[40];
    z[40]= - n<T>(179,8) + z[40];
    z[40]=z[9]*z[40];
    z[28]=z[28] + n<T>(1,4)*z[36] + z[40];
    z[28]=z[6]*z[28];
    z[14]=n<T>(1,2)*z[14] + z[28];
    z[14]=z[6]*z[14];
    z[28]= - n<T>(1,2) + z[64];
    z[14]=z[14] + n<T>(1,4)*z[28] + z[20];
    z[14]=z[6]*z[14];
    z[20]=z[31] - z[24];
    z[24]= - z[20]*z[29];
    z[28]=n<T>(1,8)*z[3];
    z[29]= - n<T>(3,8) + z[5];
    z[29]=z[5]*z[29];
    z[29]= - z[28] + z[29];
    z[24]=3*z[29] + z[24];
    z[24]=z[2]*z[24];
    z[29]= - static_cast<T>(7)+ z[39];
    z[29]=z[29]*z[33];
    z[24]=z[24] + z[29] + static_cast<T>(1)+ z[28];
    z[24]=z[2]*z[24];
    z[29]= - n<T>(5,2) + z[5];
    z[29]=z[29]*z[44];
    z[14]=z[14] + z[24] + z[29] + z[9] + static_cast<T>(3)- z[10];
    z[14]=z[4]*z[14];
    z[20]=z[2]*z[20];
    z[24]=static_cast<T>(5)- z[37];
    z[24]=z[5]*z[24];
    z[20]=z[20] + z[3] + z[24];
    z[20]=z[20]*z[48];
    z[24]= - z[59]*z[44];
    z[29]=n<T>(5,4)*z[3];
    z[20]=z[20] + z[24] - static_cast<T>(3)- z[29];
    z[20]=z[20]*z[48];
    z[24]=n<T>(101,2) - z[9];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(293,2) + z[24];
    z[24]=z[9]*z[24];
    z[24]=n<T>(293,2) + z[24];
    z[24]=z[24]*z[26];
    z[31]=z[9] - 4;
    z[31]=z[31]*z[9];
    z[31]=z[31] + 6;
    z[31]=z[31]*z[9];
    z[31]=z[31] - 4;
    z[31]=z[31]*z[9];
    z[31]=z[31] + 1;
    z[36]=z[31]*z[41];
    z[37]= - n<T>(101,2) + z[16];
    z[24]=z[36] + z[24] + n<T>(1,2)*z[37] - z[3];
    z[24]=z[6]*z[24];
    z[36]=static_cast<T>(23)+ n<T>(25,2)*z[7];
    z[21]=n<T>(1,2)*z[36] + z[21];
    z[36]=static_cast<T>(17)- n<T>(5,4)*z[9];
    z[36]=z[9]*z[36];
    z[36]= - n<T>(213,8) + z[36];
    z[36]=z[9]*z[36];
    z[21]=z[24] + n<T>(1,2)*z[21] + z[36];
    z[21]=z[6]*z[21];
    z[24]= - n<T>(5,4) - z[35];
    z[24]=z[9]*z[24];
    z[24]=z[24] - n<T>(37,4)*z[3] + static_cast<T>(13)+ n<T>(15,4)*z[7];
    z[21]=n<T>(1,2)*z[24] + z[21];
    z[21]=z[6]*z[21];
    z[24]=static_cast<T>(23)- z[63];
    z[18]=z[24]*z[18];
    z[24]= - n<T>(79,2) + z[7];
    z[14]=z[14] + z[21] + z[20] + z[18] - z[38] + n<T>(1,4)*z[24] + z[62];
    z[14]=z[4]*z[14];
    z[18]= - static_cast<T>(1)+ z[3];
    z[15]=z[18]*z[15];
    z[18]=z[19]*z[44];
    z[15]=z[18] + z[15] - z[51];
    z[15]=z[15]*z[48];
    z[18]= - z[30] - static_cast<T>(1)+ n<T>(3,4)*z[8];
    z[10]=z[18]*z[10];
    z[18]=z[34]*z[33];
    z[18]=z[18] - static_cast<T>(2)+ z[53];
    z[18]=z[5]*z[18];
    z[19]=n<T>(5,2) - z[8];
    z[20]= - z[9]*z[45];
    z[10]=z[15] + z[18] + z[20] + z[10] + n<T>(1,2)*z[19] + z[16];
    z[10]=z[2]*z[10];
    z[15]=static_cast<T>(101)- 9*z[7];
    z[13]=static_cast<T>(11)- z[13];
    z[13]=z[9]*z[13];
    z[13]=n<T>(165,2) + z[13];
    z[13]=z[9]*z[13];
    z[13]= - n<T>(247,2) + z[13];
    z[13]=z[9]*z[13];
    z[11]=z[13] + n<T>(1,2)*z[15] + z[11];
    z[13]=static_cast<T>(19)- z[16];
    z[13]=n<T>(1,2)*z[13] + z[3];
    z[15]=n<T>(49,8) - z[54];
    z[15]=z[9]*z[15];
    z[15]= - n<T>(51,8) + z[15];
    z[15]=z[9]*z[15];
    z[13]=n<T>(1,4)*z[13] + z[15];
    z[13]=z[2]*z[13];
    z[15]= - n<T>(43,4) + z[27];
    z[15]=z[9]*z[15];
    z[15]=static_cast<T>(2)+ z[15];
    z[15]=z[9]*z[15];
    z[15]=n<T>(17,2) + z[15];
    z[15]=z[9]*z[15];
    z[13]=z[13] + z[15] - static_cast<T>(5)+ z[49];
    z[13]=z[2]*z[13];
    z[15]= - z[2]*z[23];
    z[15]=z[15] + z[31];
    z[15]=z[2]*z[15];
    z[15]= - 3*z[23] + z[15];
    z[15]=z[15]*z[22];
    z[11]=z[15] + n<T>(1,4)*z[11] + z[13];
    z[11]=z[6]*z[11];
    z[13]= - static_cast<T>(17)+ z[35];
    z[13]=z[13]*z[32];
    z[13]=z[13] - z[17] + static_cast<T>(7)- z[46];
    z[13]=z[13]*z[48];
    z[15]= - n<T>(21,2) + z[25];
    z[15]=z[9]*z[15];
    z[15]=n<T>(91,8) + z[15];
    z[15]=z[9]*z[15];
    z[13]=z[13] + z[15] + z[29] - static_cast<T>(8)+ n<T>(21,8)*z[7];
    z[13]=z[2]*z[13];
    z[15]=n<T>(1,2) - z[9];
    z[15]=z[15]*z[27];
    z[15]=n<T>(31,4) + z[15];
    z[15]=z[9]*z[15];
    z[15]= - n<T>(37,4) + z[15];
    z[15]=z[9]*z[15];
    z[16]= - n<T>(17,2)*z[3] - z[61];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[11]=z[11] + n<T>(1,2)*z[15] + z[13];
    z[11]=z[6]*z[11];
    z[13]=n<T>(31,2)*z[7];
    z[15]=n<T>(13,2) - z[35];
    z[15]=z[9]*z[15];
    z[15]=z[15] - z[47] - static_cast<T>(9)+ z[13];
    z[16]= - static_cast<T>(3)- n<T>(11,4)*z[7];
    z[16]=z[9] + n<T>(1,2)*z[16] + z[3];
    z[16]=z[2]*z[16];
    z[15]=n<T>(1,4)*z[15] + z[16];
    z[15]=z[2]*z[15];
    z[13]= - static_cast<T>(3)- z[13];
    z[16]=n<T>(39,8) - z[54];
    z[16]=z[9]*z[16];
    z[16]= - n<T>(19,8) + z[16];
    z[16]=z[9]*z[16];
    z[11]=z[11] + z[15] + z[16] + n<T>(1,4)*z[13] + z[62];
    z[11]=z[6]*z[11];
    z[13]=z[34]*z[56];
    z[15]= - z[58]*z[28];
    z[16]=n<T>(3,4) - z[7];
    z[16]=z[16]*z[26];
    z[16]=z[16] + n<T>(13,8)*z[7] + n<T>(3,2) + z[8];
    z[16]=z[9]*z[16];
    z[17]= - static_cast<T>(4)- n<T>(7,8)*z[8];
    z[17]=z[8]*z[17];
    z[17]=z[55] - n<T>(31,4) + z[17];
    z[17]=z[5]*z[17];

    r += static_cast<T>(4)+ z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + z[16] + 
      z[17] - z[42];
 
    return r;
}

template double qqb_2lha_r2325(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2325(const std::array<dd_real,30>&);
#endif
