#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2150(const std::array<T,30>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[11];
    z[9]=static_cast<T>(5)+ 3*z[4];
    z[9]=z[4]*z[9];
    z[10]=static_cast<T>(5)- 3*z[5];
    z[10]=z[5]*z[10];
    z[9]=z[9] + z[10];
    z[10]=n<T>(1,2)*z[4];
    z[11]=n<T>(1,2)*z[5];
    z[12]=z[10] + z[11];
    z[13]= - static_cast<T>(1)- z[12];
    z[13]=z[8]*z[13];
    z[9]=n<T>(1,4)*z[9] + z[13];
    z[9]=z[8]*z[9];
    z[13]=z[11] - 1;
    z[14]=z[13]*z[5];
    z[15]=z[10] + 1;
    z[15]=z[15]*z[4];
    z[14]=z[14] - z[15];
    z[15]=npow(z[8],2);
    z[16]=n<T>(1,2)*z[7];
    z[17]=z[15]*z[16];
    z[18]= - z[14]*z[17];
    z[9]=z[9] + z[18];
    z[9]=z[9]*z[16];
    z[18]=z[2] - 1;
    z[19]=n<T>(3,2) + z[4];
    z[19]=z[4]*z[19];
    z[20]=n<T>(3,2) - z[5];
    z[20]=z[5]*z[20];
    z[19]=z[20] + z[19] + z[18];
    z[20]=z[13] + z[10];
    z[21]=z[20]*z[8];
    z[9]=z[9] + n<T>(1,4)*z[19] - z[21];
    z[9]=z[7]*z[9];
    z[19]=z[1] - 1;
    z[22]=z[4]*z[1];
    z[22]=z[22] + z[19];
    z[22]=z[22]*z[10];
    z[23]=z[5]*z[1];
    z[19]= - z[23] + z[19];
    z[19]=z[19]*z[11];
    z[18]=z[1] - z[18];
    z[18]=z[2]*z[18];
    z[18]=z[19] + z[22] + static_cast<T>(1)+ z[18];
    z[19]= - z[4] - z[5];
    z[19]=z[19]*z[17];
    z[19]= - 3*z[21] + z[19];
    z[19]=z[19]*z[16];
    z[21]=static_cast<T>(1)- n<T>(1,2)*z[2];
    z[21]=z[2]*z[21];
    z[19]=z[19] + z[21] - z[20];
    z[19]=z[7]*z[19];
    z[20]=static_cast<T>(1)+ z[2];
    z[20]=z[2]*z[20];
    z[12]= - z[12] + static_cast<T>(1)+ z[20];
    z[12]=z[1]*z[12];
    z[20]=npow(z[2],3);
    z[12]= - z[20] + z[12];
    z[12]=n<T>(1,2)*z[12] + z[19];
    z[12]=z[3]*z[12];
    z[9]=z[12] + n<T>(1,4)*z[18] + z[9];
    z[9]=z[3]*z[9];
    z[12]=npow(z[5],2);
    z[18]=npow(z[4],2);
    z[19]=z[12] - z[18];
    z[17]=z[19]*z[17];
    z[20]=static_cast<T>(3)+ z[4];
    z[20]=z[4]*z[20];
    z[21]=static_cast<T>(3)- z[5];
    z[21]=z[5]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[8]*z[20];
    z[20]=z[12] + z[20];
    z[20]= - z[18] + n<T>(1,2)*z[20];
    z[20]=z[8]*z[20];
    z[17]=z[20] + z[17];
    z[16]=z[17]*z[16];
    z[14]= - z[8] - z[14];
    z[14]=z[8]*z[14];
    z[14]=z[16] - n<T>(1,4)*z[18] + z[14];
    z[14]=z[7]*z[14];
    z[16]=static_cast<T>(1)+ z[4];
    z[16]=z[16]*z[10];
    z[13]= - z[1]*z[13];
    z[13]= - n<T>(1,2) + z[13];
    z[13]=z[5]*z[13];
    z[13]=z[13] + z[16] + z[2] - z[1];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[9]=n<T>(1,2)*z[13] + z[9];
    z[9]=z[3]*z[9];
    z[11]= - z[6]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[5]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[8]*z[10];
    z[10]=n<T>(1,2)*z[19] + z[10];
    z[10]=z[8]*z[10];
    z[11]= - n<T>(1,2)*z[18] + z[12];
    z[11]=z[7]*z[11]*z[15];
    z[10]=z[10] + z[11];
    z[10]=z[7]*z[10];

    r += z[9] + n<T>(1,4)*z[10];
 
    return r;
}

template double qqb_2lha_r2150(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2150(const std::array<dd_real,30>&);
#endif
