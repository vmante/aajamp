#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1000(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[4];
    z[6]=k[5];
    z[7]=k[10];
    z[8]=k[3];
    z[9]=k[24];
    z[10]=z[5] - 3;
    z[11]=n<T>(1,4)*z[5];
    z[10]=z[10]*z[11];
    z[10]=z[10] - 1;
    z[10]=z[10]*z[5];
    z[12]=n<T>(1,2)*z[5];
    z[13]=z[12] + 1;
    z[14]=z[13]*z[5];
    z[15]=z[14] + n<T>(1,2);
    z[16]=npow(z[5],2);
    z[17]=z[16]*z[4];
    z[18]=z[15]*z[17];
    z[18]=z[10] - n<T>(3,2)*z[18];
    z[18]=z[4]*z[18];
    z[19]=z[5] + 1;
    z[20]=z[19]*z[16];
    z[21]=npow(z[5],3);
    z[22]=z[21]*z[4];
    z[23]=z[15]*z[22];
    z[23]=z[20] + z[23];
    z[24]=n<T>(1,2)*z[4];
    z[23]=z[6]*z[23]*z[24];
    z[18]=z[18] + z[23];
    z[18]=z[6]*z[18];
    z[23]=z[5] - 1;
    z[25]=z[23]*z[12];
    z[25]=z[25] - 1;
    z[25]=n<T>(1,2)*z[25];
    z[26]=z[4]*z[5];
    z[27]=z[15]*z[26];
    z[28]= - z[25] + z[27];
    z[28]=z[4]*z[28];
    z[29]=n<T>(1,2)*z[6];
    z[30]=z[15]*z[29];
    z[31]=3*z[17];
    z[32]=z[6]*z[22];
    z[32]= - z[31] + z[32];
    z[32]=z[32]*z[30];
    z[27]=z[27] + z[32];
    z[27]=z[9]*z[27];
    z[18]=z[27] + z[28] + z[18];
    z[18]=z[9]*z[18];
    z[27]=static_cast<T>(1)- 7*z[5];
    z[11]=z[27]*z[11];
    z[27]=z[12] - 1;
    z[28]=z[27]*z[17];
    z[11]=z[11] + z[28];
    z[11]=z[4]*z[11];
    z[28]=z[5] - n<T>(1,2);
    z[32]=z[28]*z[16];
    z[33]=z[24]*z[21];
    z[27]= - z[27]*z[33];
    z[27]=z[32] + z[27];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(5,4)*z[16] + z[27];
    z[27]=z[6]*z[27];
    z[11]=z[27] + n<T>(5,4)*z[5] + z[11];
    z[11]=z[6]*z[11];
    z[27]=z[29]*z[20];
    z[10]=z[10] + z[27];
    z[10]=z[6]*z[10];
    z[27]=z[6]*z[21];
    z[27]=z[27] - 3*z[16];
    z[27]=z[27]*z[30];
    z[30]=z[5]*z[15];
    z[27]=z[30] + z[27];
    z[27]=z[9]*z[27];
    z[10]=z[27] - z[25] + z[10];
    z[10]=z[9]*z[10];
    z[25]=z[16]*z[6];
    z[27]= - static_cast<T>(1)- n<T>(3,4)*z[5];
    z[27]=z[27]*z[25];
    z[30]=n<T>(3,2) + z[5];
    z[30]=z[5]*z[30];
    z[27]=z[30] + z[27];
    z[27]=z[6]*z[27];
    z[25]= - z[5] + z[25];
    z[25]=z[2]*z[25]*z[29];
    z[13]=n<T>(1,2)*z[13];
    z[10]=z[25] + z[10] - z[13] + z[27];
    z[10]=z[7]*z[10];
    z[25]= - n<T>(1,2) - z[16];
    z[25]=z[25]*z[24];
    z[27]=z[5] + n<T>(1,2);
    z[25]=z[25] + z[27];
    z[25]=z[4]*z[25];
    z[10]=z[10] + z[18] + z[11] + z[25] + n<T>(1,4) + z[14];
    z[11]=n<T>(5,2)*z[21] + z[22];
    z[11]=z[4]*z[11];
    z[18]=n<T>(1,4)*z[4];
    z[25]=npow(z[5],4);
    z[30]=z[18]*z[25];
    z[32]= - z[25] - z[30];
    z[32]=z[4]*z[32];
    z[32]=z[21] + z[32];
    z[32]=z[6]*z[32];
    z[11]=z[32] - z[20] + z[11];
    z[11]=z[11]*z[29];
    z[20]=z[6]*z[30];
    z[20]=z[20] + z[21] - z[22];
    z[20]=z[6]*z[20];
    z[30]=z[24] - 1;
    z[32]=z[16]*z[30];
    z[20]=3*z[32] + z[20];
    z[20]=z[3]*z[20];
    z[32]= - z[16] - z[17];
    z[32]=z[4]*z[32];
    z[28]=z[5]*z[28];
    z[11]=z[20] + z[11] + z[28] + n<T>(3,4)*z[32];
    z[11]=z[3]*z[11];
    z[20]=z[23]*z[5];
    z[20]=z[20] + n<T>(1,2);
    z[28]= - z[5]*z[20];
    z[22]=z[28] - n<T>(3,2)*z[22];
    z[22]=z[4]*z[22];
    z[28]=z[20]*z[16];
    z[25]=z[25]*z[4];
    z[25]=z[28] + z[25];
    z[25]=z[25]*z[24];
    z[21]= - z[21] + z[25];
    z[21]=z[6]*z[21];
    z[25]=z[5]*z[27];
    z[21]=z[21] + z[25] + z[22];
    z[21]=z[6]*z[21];
    z[20]=z[31] + z[20];
    z[20]=z[4]*z[20];
    z[20]=z[20] + z[23];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[11]=n<T>(1,2)*z[20] + z[11];
    z[11]=z[3]*z[11];
    z[20]=npow(z[6],2);
    z[21]=z[16]*z[20];
    z[21]=n<T>(1,2)*z[21] + n<T>(3,4) + z[14];
    z[22]=n<T>(3,2)*z[3] + z[29];
    z[22]=z[16]*z[22];
    z[14]=z[14] + z[22];
    z[14]=z[3]*z[14];
    z[14]=n<T>(1,2)*z[21] + z[14];
    z[14]=z[3]*z[14];
    z[21]=n<T>(1,2)*z[2];
    z[22]=z[3]*z[5];
    z[25]=z[21] - z[22] - z[19];
    z[25]=z[3]*z[25];
    z[25]=n<T>(1,4) + z[25];
    z[25]=z[25]*z[21];
    z[20]=z[20]*z[12];
    z[20]=z[20] - z[19];
    z[14]=z[25] + n<T>(1,4)*z[20] + z[14];
    z[14]=z[2]*z[14];
    z[20]=static_cast<T>(3)+ z[5];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[3]*z[20];
    z[20]= - n<T>(1,4) + z[20];
    z[20]=z[3]*z[20];
    z[25]=npow(z[3],2);
    z[21]= - z[25]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]= - static_cast<T>(3)- z[12];
    z[21]=n<T>(1,2)*z[21] - 3*z[22];
    z[21]=z[3]*z[21];
    z[13]=z[13] + z[21];
    z[13]=z[3]*z[13];
    z[13]=z[13] + z[20];
    z[13]=z[2]*z[13];
    z[20]= - n<T>(1,2) + 3*z[3];
    z[20]=z[20]*z[25];
    z[21]=n<T>(1,8) - z[3];
    z[21]=z[21]*z[25];
    z[22]=z[2]*npow(z[3],3);
    z[21]=z[21] + n<T>(1,4)*z[22];
    z[21]=z[2]*z[21];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]= - z[30]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[3]*z[18];
    z[18]=z[21] + z[18];
    z[18]=z[18]*z[25];
    z[18]=z[18] + z[20];
    z[18]=z[1]*z[18];
    z[20]= - z[12] + z[26];
    z[20]=z[4]*z[20];
    z[20]=z[20] - z[23];
    z[21]=3*z[5] - z[26];
    z[21]=z[3]*z[21];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[20]=z[3]*z[20];
    z[21]= - z[5]*npow(z[4],2);
    z[21]= - static_cast<T>(1)+ z[21];
    z[20]=n<T>(1,4)*z[21] + z[20];
    z[20]=z[3]*z[20];
    z[13]=z[18] + z[20] + z[13];
    z[13]=z[1]*z[13];
    z[18]=z[15]*z[9];
    z[18]=z[18] - n<T>(1,2)*z[19];
    z[15]= - z[4]*z[15];
    z[15]=z[15] - z[18];
    z[15]=z[4]*z[15];
    z[18]= - z[7]*z[18];
    z[15]=z[18] + z[15];
    z[15]=z[9]*z[15];
    z[17]= - z[5] + z[17];
    z[17]=z[17]*z[24];
    z[16]=z[16] - z[33];
    z[16]=z[4]*z[16];
    z[12]= - z[12] + z[16];
    z[12]=z[6]*z[12];
    z[12]=z[17] + z[12];
    z[12]=z[6]*z[12];
    z[12]=z[12] + z[15];
    z[12]=z[8]*z[12];

    r += n<T>(1,4)*z[10] + z[11] + n<T>(1,8)*z[12] + z[13] + z[14];
 
    return r;
}

template double qqb_2lha_r1000(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1000(const std::array<dd_real,30>&);
#endif
