#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2116(const std::array<T,30>& k) {
  T z[45];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[6];
    z[6]=k[3];
    z[7]=k[27];
    z[8]=k[5];
    z[9]=k[8];
    z[10]=k[13];
    z[11]=17*z[5];
    z[12]= - n<T>(1,8) + z[5];
    z[12]=z[12]*z[11];
    z[13]=n<T>(1,4)*z[10];
    z[14]= - n<T>(109,2) + 143*z[10];
    z[14]=z[14]*z[13];
    z[12]=z[12] + z[14];
    z[14]=n<T>(1,4)*z[5];
    z[15]=n<T>(1,3) + z[14];
    z[15]=z[15]*z[11];
    z[16]=n<T>(1,3)*z[10];
    z[17]= - static_cast<T>(143)+ n<T>(713,4)*z[10];
    z[17]=z[17]*z[16];
    z[15]=z[17] - static_cast<T>(1)+ z[15];
    z[17]=n<T>(1,4)*z[8];
    z[15]=z[15]*z[17];
    z[12]=n<T>(1,3)*z[12] + z[15];
    z[12]=z[8]*z[12];
    z[15]=n<T>(17,4)*z[5];
    z[18]=n<T>(5,3)*z[5];
    z[19]=static_cast<T>(1)- z[18];
    z[19]=z[19]*z[15];
    z[20]=7*z[8];
    z[19]=z[20] - n<T>(59,4)*z[10] - static_cast<T>(7)+ z[19];
    z[21]=n<T>(1,2)*z[8];
    z[19]=z[19]*z[21];
    z[22]=n<T>(7,2)*z[8];
    z[23]=static_cast<T>(1)- z[21];
    z[23]=z[23]*z[22];
    z[23]= - static_cast<T>(3)+ z[23];
    z[23]=z[3]*z[23];
    z[19]=z[19] + z[23];
    z[19]=z[3]*z[19];
    z[23]=npow(z[10],2);
    z[24]= - static_cast<T>(1)+ z[23];
    z[12]=z[19] + n<T>(3,2)*z[24] + z[12];
    z[12]=z[3]*z[12];
    z[19]=n<T>(1,2)*z[5];
    z[24]= - n<T>(1,3) - z[19];
    z[24]=z[24]*z[11];
    z[25]=static_cast<T>(143)- n<T>(713,2)*z[10];
    z[25]=z[25]*z[16];
    z[24]=z[24] + z[25];
    z[24]=z[8]*z[24];
    z[25]=npow(z[5],2);
    z[26]=n<T>(17,2)*z[25];
    z[24]=z[24] - z[26] - n<T>(143,3)*z[23];
    z[17]=z[24]*z[17];
    z[12]=z[12] - static_cast<T>(3)+ z[17];
    z[12]=z[3]*z[12];
    z[17]= - static_cast<T>(3)+ n<T>(179,24)*z[10];
    z[17]=z[10]*z[17];
    z[17]=n<T>(17,24)*z[25] + z[17];
    z[17]=z[8]*z[17];
    z[24]=3*z[23];
    z[27]=z[3]*z[10];
    z[17]= - 3*z[27] + z[24] + z[17];
    z[17]=z[3]*z[8]*z[17];
    z[27]=static_cast<T>(3)- n<T>(179,12)*z[10];
    z[27]=z[10]*z[27];
    z[27]= - n<T>(17,12)*z[25] + z[27];
    z[27]=z[8]*z[27];
    z[24]= - z[24] + z[27];
    z[24]=z[8]*z[24];
    z[17]=z[24] + z[17];
    z[17]=z[3]*z[17];
    z[24]=17*z[25];
    z[27]=z[24] + 179*z[23];
    z[28]=npow(z[8],2);
    z[27]=z[27]*z[28];
    z[17]=n<T>(1,24)*z[27] + z[17];
    z[17]=z[3]*z[17];
    z[27]=n<T>(1,2)*z[3];
    z[29]= - static_cast<T>(1)+ z[27];
    z[29]=z[3]*z[29];
    z[29]=n<T>(1,2) + z[29];
    z[29]=z[4]*npow(z[3],2)*z[29]*z[28]*z[23];
    z[17]=z[17] + 3*z[29];
    z[17]=z[4]*z[17];
    z[29]=z[24] + n<T>(713,3)*z[23];
    z[28]=z[29]*z[28];
    z[12]=z[17] + n<T>(1,16)*z[28] + z[12];
    z[12]=z[4]*z[12];
    z[17]=n<T>(1,3)*z[5];
    z[28]= - static_cast<T>(1)- z[17];
    z[28]=z[28]*z[11];
    z[29]=static_cast<T>(427)- 695*z[10];
    z[29]=z[29]*z[16];
    z[28]=z[28] + z[29];
    z[29]=z[24] + 695*z[23];
    z[29]=z[6]*z[29];
    z[30]=npow(z[7],3);
    z[31]=3*z[30];
    z[28]=n<T>(1,48)*z[29] + n<T>(1,8)*z[28] - z[31];
    z[28]=z[8]*z[28];
    z[29]= - z[24] - n<T>(427,3)*z[23];
    z[28]=z[28] + n<T>(1,8)*z[29] - 9*z[30];
    z[28]=z[8]*z[28];
    z[29]=3*z[1];
    z[32]= - z[29] - 9;
    z[32]=z[30]*z[32];
    z[12]=z[12] + z[28] + n<T>(1,4) + z[32];
    z[28]=n<T>(17,2)*z[5];
    z[32]=static_cast<T>(1)+ n<T>(1,6)*z[5];
    z[32]=z[32]*z[28];
    z[33]=n<T>(1,6)*z[10];
    z[34]= - static_cast<T>(427)+ n<T>(695,2)*z[10];
    z[34]=z[34]*z[33];
    z[32]=z[34] + n<T>(41,3) + z[32];
    z[34]=npow(z[7],2);
    z[32]=n<T>(1,8)*z[32] - z[34];
    z[32]=z[8]*z[32];
    z[35]=n<T>(1,3) + n<T>(3,4)*z[5];
    z[35]=z[35]*z[11];
    z[36]= - static_cast<T>(27)+ n<T>(427,12)*z[10];
    z[36]=z[10]*z[36];
    z[35]=z[35] + z[36];
    z[36]=3*z[34];
    z[32]=z[32] + n<T>(1,4)*z[35] - z[36];
    z[32]=z[8]*z[32];
    z[35]= - z[1]*z[34];
    z[32]=z[32] + n<T>(25,4)*z[2] + z[35] - z[36] + n<T>(107,48)*z[23] - static_cast<T>(3)+ n<T>(17,16)*z[25];
    z[35]= - static_cast<T>(1)+ n<T>(5,6)*z[5];
    z[35]=z[35]*z[11];
    z[35]=n<T>(33,2) + z[35];
    z[37]=z[2] + n<T>(1,2);
    z[38]= - z[37]*z[21];
    z[39]=z[2] - 1;
    z[38]=z[38] + z[39];
    z[38]=z[8]*z[38];
    z[40]= - n<T>(7,4)*z[8] - z[29] + n<T>(7,2);
    z[40]=z[3]*z[39]*z[40];
    z[41]=n<T>(7,8) - z[29];
    z[41]=z[2]*z[41];
    z[35]=z[40] + n<T>(7,4)*z[38] + n<T>(1,8)*z[35] + z[41];
    z[35]=z[3]*z[35];
    z[38]= - n<T>(1,6) - z[5];
    z[38]=z[38]*z[28];
    z[40]=static_cast<T>(1)+ z[2];
    z[40]=z[40]*z[20];
    z[38]=z[40] - n<T>(103,12)*z[10] - n<T>(5,3) + z[38];
    z[21]=z[38]*z[21];
    z[38]=n<T>(1,4) - z[5];
    z[38]=z[38]*z[11];
    z[38]=n<T>(37,12)*z[10] - n<T>(5,3) + z[38];
    z[40]=9*z[1];
    z[41]=n<T>(13,2) - z[40];
    z[41]=z[2]*z[41];
    z[21]=z[21] + n<T>(1,2)*z[38] + z[41];
    z[21]=n<T>(1,4)*z[21] + z[35];
    z[21]=z[3]*z[21];
    z[21]=n<T>(1,2)*z[32] + z[21];
    z[21]=z[3]*z[21];
    z[12]=z[21] + n<T>(1,4)*z[12];
    z[12]=z[4]*z[12];
    z[21]=n<T>(1,2)*z[2];
    z[32]= - n<T>(41,2) + 57*z[1];
    z[32]=z[2]*z[32];
    z[32]=z[32] + n<T>(47,3) - n<T>(95,2)*z[1];
    z[32]=z[32]*z[21];
    z[35]= - static_cast<T>(1)- z[21];
    z[35]=z[8]*z[35]*z[21];
    z[38]=n<T>(1,4)*z[2];
    z[41]=z[38] - 1;
    z[35]=z[35] + z[41];
    z[20]=z[35]*z[20];
    z[35]= - static_cast<T>(1)+ z[5];
    z[35]=z[5]*z[35];
    z[35]=n<T>(155,3) + 51*z[35];
    z[42]= - n<T>(1,8) + z[17];
    z[42]=z[5]*z[42];
    z[42]= - n<T>(1,12) + z[42];
    z[42]=z[1]*z[42];
    z[20]=z[20] + z[32] + n<T>(1,8)*z[35] + 17*z[42];
    z[18]=static_cast<T>(3)- z[18];
    z[18]=z[18]*z[11];
    z[18]= - n<T>(49,3) + z[18];
    z[32]=n<T>(1,4)*z[1];
    z[18]=z[18]*z[32];
    z[35]= - n<T>(373,12) + z[40];
    z[35]=z[1]*z[35];
    z[40]=n<T>(59,2) - z[40];
    z[40]=z[1]*z[40];
    z[40]= - static_cast<T>(7)+ z[40];
    z[40]=z[2]*z[40];
    z[35]=z[40] + static_cast<T>(7)+ z[35];
    z[35]=z[2]*z[35];
    z[37]= - z[2]*z[37];
    z[37]=n<T>(3,2) + z[37];
    z[22]=z[37]*z[22];
    z[18]=z[22] + z[18] + z[35];
    z[22]= - z[21] + 1;
    z[22]=z[2]*z[22];
    z[22]= - n<T>(1,2) + z[22];
    z[35]=n<T>(1,2)*z[1];
    z[37]=z[35] - 1;
    z[37]=z[37]*z[1];
    z[37]=z[37] + n<T>(1,2);
    z[22]=z[3]*z[37]*z[22];
    z[18]=n<T>(1,2)*z[18] + 21*z[22];
    z[18]=z[3]*z[18];
    z[18]=n<T>(1,2)*z[20] + z[18];
    z[18]=z[18]*z[27];
    z[20]= - n<T>(1,3) - n<T>(3,2)*z[5];
    z[20]=z[20]*z[15];
    z[20]=n<T>(73,24)*z[10] - n<T>(7,3) + z[20];
    z[22]=n<T>(3,4)*z[1];
    z[37]= - n<T>(39,2)*z[2] + static_cast<T>(19)+ z[22];
    z[37]=z[37]*z[38];
    z[38]=n<T>(7,8)*z[2];
    z[40]=z[38]*z[8];
    z[42]= - n<T>(7,3) - z[5];
    z[42]=z[5]*z[42];
    z[42]=n<T>(109,96)*z[10] - static_cast<T>(1)+ n<T>(17,32)*z[42];
    z[42]=z[40] + n<T>(1,2)*z[42] + z[7];
    z[42]=z[8]*z[42];
    z[43]= - n<T>(17,96)*z[25] + z[7];
    z[43]=z[1]*z[43];
    z[18]=z[18] + z[42] + z[37] + z[43] + n<T>(1,4)*z[20] + 2*z[7];
    z[18]=z[3]*z[18];
    z[20]=static_cast<T>(67)- n<T>(295,4)*z[10];
    z[20]=z[10]*z[20];
    z[20]= - z[15] + z[20];
    z[37]=static_cast<T>(5)- n<T>(9,2)*z[7];
    z[37]=z[37]*z[34];
    z[31]=n<T>(295,96)*z[23] + z[31];
    z[31]=z[6]*z[31];
    z[20]=z[31] + n<T>(1,12)*z[20] + z[37];
    z[31]=n<T>(1,2)*z[6];
    z[20]=z[20]*z[31];
    z[37]= - static_cast<T>(67)+ n<T>(295,8)*z[10];
    z[37]=z[37]*z[16];
    z[42]=n<T>(17,12)*z[5];
    z[37]=z[37] + static_cast<T>(11)+ z[42];
    z[43]=static_cast<T>(1)- n<T>(15,8)*z[7];
    z[43]=z[7]*z[43];
    z[20]=z[20] + n<T>(1,8)*z[37] + z[43];
    z[20]=z[8]*z[20];
    z[25]= - n<T>(17,8)*z[25] - 67*z[23];
    z[37]=3*z[7];
    z[43]=static_cast<T>(1)- z[37];
    z[43]=z[43]*z[34];
    z[30]=z[30]*z[6];
    z[44]=3*z[30];
    z[25]=z[44] + n<T>(1,6)*z[25] + 5*z[43];
    z[25]=z[25]*z[31];
    z[43]=n<T>(3,4) + z[17];
    z[28]=z[43]*z[28];
    z[43]= - n<T>(215,8) + n<T>(67,3)*z[10];
    z[43]=z[10]*z[43];
    z[28]=z[28] + z[43];
    z[43]=static_cast<T>(1)- n<T>(7,2)*z[7];
    z[43]=z[7]*z[43];
    z[25]=z[25] + n<T>(1,4)*z[28] + z[43];
    z[20]=n<T>(1,2)*z[25] + z[20];
    z[20]=z[8]*z[20];
    z[25]=n<T>(71,2)*z[23] + static_cast<T>(3)+ z[26];
    z[26]= - static_cast<T>(5)- z[37];
    z[26]=z[26]*z[34];
    z[26]=z[26] - z[44];
    z[26]=z[26]*z[31];
    z[28]= - static_cast<T>(1)+ n<T>(17,4)*z[7];
    z[28]=z[7]*z[28];
    z[25]=z[26] + n<T>(1,16)*z[25] + z[28];
    z[26]=n<T>(3,4)*z[30];
    z[28]=2*z[34] + z[26];
    z[28]=z[1]*z[28];
    z[12]=z[12] + z[18] + z[20] - n<T>(3,16)*z[2] + n<T>(1,2)*z[25] + z[28];
    z[12]=z[4]*z[12];
    z[18]= - static_cast<T>(5)+ n<T>(37,12)*z[1];
    z[18]=z[18]*z[35];
    z[18]=static_cast<T>(1)+ z[18];
    z[20]= - static_cast<T>(7)+ n<T>(13,2)*z[1];
    z[20]=z[20]*z[29];
    z[20]=n<T>(7,2) + z[20];
    z[20]=z[2]*z[20];
    z[25]=static_cast<T>(49)- n<T>(163,4)*z[1];
    z[25]=z[1]*z[25];
    z[20]=z[20] - n<T>(49,4) + z[25];
    z[20]=z[20]*z[21];
    z[18]=7*z[18] + z[20];
    z[18]=z[2]*z[18];
    z[20]= - static_cast<T>(1)+ n<T>(5,12)*z[5];
    z[20]=z[20]*z[11];
    z[20]=n<T>(37,4) + z[20];
    z[20]=z[20]*z[35];
    z[20]=static_cast<T>(7)+ z[20];
    z[20]=z[1]*z[20];
    z[20]= - n<T>(21,4) + z[20];
    z[25]=z[2]*z[1];
    z[28]=z[29] - z[25];
    z[28]=z[2]*z[28];
    z[28]= - z[29] + z[28];
    z[43]= - static_cast<T>(7)+ n<T>(3,2)*z[1];
    z[43]=z[43]*z[1];
    z[43]=z[43] + n<T>(7,2);
    z[28]=z[2]*z[43]*z[28];
    z[43]=z[1]*z[43];
    z[28]=z[43] + z[28];
    z[28]=z[3]*z[28];
    z[18]=z[28] + n<T>(1,2)*z[20] + z[18];
    z[18]=z[3]*z[18];
    z[20]=n<T>(13,6) - z[5];
    z[20]=z[20]*z[11];
    z[28]=n<T>(1,2) - z[5];
    z[28]=z[5]*z[28];
    z[28]=n<T>(1,2) + z[28];
    z[28]=z[1]*z[28];
    z[20]=n<T>(17,3)*z[28] - n<T>(199,6) + z[20];
    z[20]=z[20]*z[35];
    z[20]=static_cast<T>(7)+ z[20];
    z[28]=static_cast<T>(69)+ z[1];
    z[28]=z[28]*z[29];
    z[28]= - static_cast<T>(49)+ z[28];
    z[43]=n<T>(7,2) - 15*z[1];
    z[43]=z[2]*z[43];
    z[28]=n<T>(1,8)*z[28] + z[43];
    z[28]=z[2]*z[28];
    z[43]= - n<T>(221,3) - z[29];
    z[43]=z[1]*z[43];
    z[43]=static_cast<T>(7)+ z[43];
    z[28]=n<T>(1,8)*z[43] + z[28];
    z[28]=z[2]*z[28];
    z[40]= - z[39]*z[40];
    z[18]=z[18] + z[40] + n<T>(1,4)*z[20] + z[28];
    z[18]=z[18]*z[27];
    z[14]= - n<T>(1,3) + z[14];
    z[14]=z[14]*z[11];
    z[20]= - n<T>(65,3) + z[24];
    z[20]=z[20]*z[35];
    z[14]=z[20] - n<T>(1,2) + z[14];
    z[20]=n<T>(35,3) + z[29];
    z[24]=n<T>(27,8)*z[2] - static_cast<T>(5)- n<T>(3,8)*z[1];
    z[24]=z[2]*z[24];
    z[20]=n<T>(1,8)*z[20] + z[24];
    z[20]=z[2]*z[20];
    z[24]= - z[8]*npow(z[2],2);
    z[24]= - z[2] + z[24];
    z[24]=z[8]*z[24];
    z[14]=z[18] + n<T>(7,16)*z[24] + n<T>(1,16)*z[14] + z[20];
    z[14]=z[3]*z[14];
    z[18]=static_cast<T>(1)- z[7];
    z[18]=z[6]*z[18]*z[34];
    z[20]=n<T>(5,2) - z[7];
    z[20]=z[7]*z[20];
    z[18]=n<T>(3,2)*z[18] + n<T>(55,96)*z[23] + z[20];
    z[18]=z[6]*z[18];
    z[20]=5*z[7];
    z[24]= - z[6]*z[36];
    z[24]= - z[20] + z[24];
    z[24]=z[24]*z[35];
    z[28]= - n<T>(1,2) - z[17];
    z[28]=z[5]*z[28];
    z[28]=static_cast<T>(1)+ z[28];
    z[28]=17*z[28] + n<T>(73,6)*z[10];
    z[35]=z[2]*z[39];
    z[18]=n<T>(3,8)*z[35] + z[24] + n<T>(1,16)*z[28] + z[18];
    z[24]=n<T>(5,2) - z[37];
    z[24]=z[24]*z[34];
    z[24]=n<T>(3,2)*z[30] - n<T>(9,32)*z[23] + z[24];
    z[24]=z[6]*z[24];
    z[28]= - static_cast<T>(2)+ z[37];
    z[28]=z[28]*z[34];
    z[26]= - z[26] - n<T>(13,192)*z[23] + z[28];
    z[26]=z[6]*z[26];
    z[28]=static_cast<T>(9)+ n<T>(13,3)*z[10];
    z[28]=z[10]*z[28];
    z[30]=static_cast<T>(23)- 9*z[7];
    z[30]=z[7]*z[30];
    z[30]= - static_cast<T>(7)+ z[30];
    z[30]=z[7]*z[30];
    z[28]=n<T>(1,8)*z[28] + z[30];
    z[26]=n<T>(1,4)*z[28] + z[26];
    z[26]=z[6]*z[26];
    z[28]= - static_cast<T>(9)- n<T>(13,6)*z[10];
    z[28]=z[28]*z[13];
    z[28]= - static_cast<T>(1)+ z[28];
    z[30]=static_cast<T>(11)- n<T>(21,2)*z[7];
    z[30]=z[7]*z[30];
    z[28]=n<T>(1,2)*z[28] + z[30];
    z[26]=n<T>(1,4)*z[28] + z[26];
    z[26]=z[8]*z[26];
    z[28]= - n<T>(167,6) + 9*z[10];
    z[28]=z[10]*z[28];
    z[28]=n<T>(17,6)*z[5] + z[28];
    z[30]=static_cast<T>(1)- n<T>(19,8)*z[7];
    z[30]=z[7]*z[30];
    z[24]=z[26] + z[24] + n<T>(1,32)*z[28] + z[30];
    z[24]=z[6]*z[24];
    z[15]=n<T>(113,4)*z[10] - static_cast<T>(31)- z[15];
    z[15]=n<T>(1,12)*z[15] + z[20];
    z[15]=n<T>(1,4)*z[15] + z[24];
    z[15]=z[8]*z[15];
    z[12]=z[12] + z[14] + n<T>(1,2)*z[18] + z[15];
    z[12]=z[4]*z[12];
    z[14]=n<T>(1,2)*z[10];
    z[15]=static_cast<T>(3)- n<T>(1,2)*z[9];
    z[15]= - 23*z[7] + 7*z[15] - z[14];
    z[18]=n<T>(1,2)*z[7];
    z[15]=z[15]*z[18];
    z[24]=n<T>(7,4)*z[9];
    z[26]=n<T>(7,4) - n<T>(5,3)*z[10];
    z[26]=z[10]*z[26];
    z[15]=z[15] + z[24] + z[26];
    z[26]=7*z[9];
    z[28]=z[26] + z[10];
    z[30]=n<T>(1,4)*z[28];
    z[34]=z[30] + z[37];
    z[34]=z[34]*z[7];
    z[34]=z[34] + n<T>(1,4)*z[23];
    z[35]=z[34]*z[6]*z[7];
    z[36]=static_cast<T>(29)- z[28];
    z[36]=n<T>(1,4)*z[36] - z[37];
    z[36]=z[7]*z[36];
    z[39]=n<T>(3,2) - z[10];
    z[39]=z[10]*z[39];
    z[39]=n<T>(21,2)*z[9] + z[39];
    z[36]=n<T>(1,4)*z[39] + z[36];
    z[36]=z[7]*z[36];
    z[36]=z[35] + n<T>(5,6)*z[23] + z[36];
    z[36]=z[6]*z[36];
    z[15]=n<T>(1,2)*z[15] + z[36];
    z[15]=z[6]*z[15];
    z[36]=static_cast<T>(7)- z[13];
    z[18]=n<T>(1,3)*z[36] - z[18];
    z[15]=n<T>(1,2)*z[18] + z[15];
    z[15]=z[15]*z[31];
    z[18]= - static_cast<T>(1)+ z[10];
    z[18]=z[10]*z[18];
    z[18]= - z[26] + z[18];
    z[26]= - static_cast<T>(19)+ z[28];
    z[26]=n<T>(1,4)*z[26] + z[37];
    z[26]=z[7]*z[26];
    z[28]= - z[34]*z[31];
    z[18]=z[28] + n<T>(1,4)*z[18] + z[26];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(13,24)*z[23] + z[18];
    z[18]=z[18]*z[31];
    z[26]= - static_cast<T>(5)+ n<T>(13,2)*z[10];
    z[16]=z[26]*z[16];
    z[16]= - z[24] + z[16];
    z[14]=static_cast<T>(1)- z[14];
    z[13]=z[14]*z[13];
    z[13]=z[13] - static_cast<T>(5)+ z[24];
    z[14]= - n<T>(3,4)*z[7] - n<T>(1,16)*z[10] + static_cast<T>(4)- n<T>(7,16)*z[9];
    z[14]=z[7]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[13]=z[7]*z[13];
    z[13]=z[18] + n<T>(1,4)*z[16] + z[13];
    z[13]=z[6]*z[13];
    z[14]=static_cast<T>(5)- n<T>(13,4)*z[10];
    z[14]=z[10]*z[14];
    z[14]= - n<T>(49,4) + z[14];
    z[16]=static_cast<T>(5)- n<T>(13,4)*z[7];
    z[16]=z[7]*z[16];
    z[14]=n<T>(1,6)*z[14] + z[16];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[8]*z[13]*npow(z[6],2);
    z[13]=z[15] + z[13];
    z[13]=z[8]*z[13];
    z[14]= - static_cast<T>(7)- n<T>(1,8)*z[1];
    z[14]=z[1]*z[14];
    z[14]=n<T>(21,8) + z[14];
    z[14]=z[14]*z[29];
    z[15]=static_cast<T>(1)- z[29];
    z[15]=z[15]*z[25];
    z[16]=static_cast<T>(35)+ z[32];
    z[16]=z[1]*z[16];
    z[16]= - n<T>(49,4) + z[16];
    z[16]=z[1]*z[16];
    z[15]=z[16] + n<T>(7,2)*z[15];
    z[15]=z[15]*z[21];
    z[14]=z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]=static_cast<T>(3)+ n<T>(5,24)*z[1];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(5,4) + z[15];
    z[15]=z[1]*z[15];
    z[14]=n<T>(7,2)*z[15] + z[14];
    z[14]=z[2]*z[14];
    z[15]=z[1] - n<T>(1,2);
    z[16]=npow(z[1],2);
    z[15]=z[15]*z[16];
    z[18]=z[2]*z[41];
    z[18]=n<T>(3,2) + z[18];
    z[18]=z[2]*z[15]*z[18];
    z[18]= - z[15] + z[18];
    z[18]=z[2]*z[18];
    z[15]=n<T>(1,4)*z[15] + z[18];
    z[15]=z[3]*z[15];
    z[17]=static_cast<T>(1)- z[17];
    z[17]=z[17]*z[11];
    z[17]= - static_cast<T>(19)+ z[17];
    z[17]=z[17]*z[32];
    z[17]= - static_cast<T>(7)+ z[17];
    z[17]=z[1]*z[17];
    z[17]=n<T>(7,2) + z[17];
    z[17]=z[17]*z[32];
    z[14]=7*z[15] + z[17] + z[14];
    z[14]=z[14]*z[27];
    z[15]= - n<T>(5,3) + z[19];
    z[15]=z[15]*z[11];
    z[15]=n<T>(205,6) + z[15];
    z[15]=z[15]*z[16];
    z[16]= - n<T>(1,2) + z[29];
    z[16]=z[16]*z[38];
    z[17]= - static_cast<T>(7)- n<T>(3,16)*z[1];
    z[17]=z[1]*z[17];
    z[16]=z[16] + n<T>(21,16) + z[17];
    z[16]=z[2]*z[16];
    z[17]=static_cast<T>(49)+ n<T>(15,4)*z[1];
    z[17]=z[1]*z[17];
    z[17]= - n<T>(21,2) + z[17];
    z[16]=n<T>(1,8)*z[17] + z[16];
    z[16]=z[2]*z[16];
    z[17]= - static_cast<T>(1)- n<T>(5,12)*z[1];
    z[17]=z[1]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[16]=n<T>(7,4)*z[17] + z[16];
    z[16]=z[2]*z[16];
    z[14]=z[14] + n<T>(1,32)*z[15] + z[16];
    z[14]=z[3]*z[14];
    z[15]=static_cast<T>(5)- z[5];
    z[15]=z[15]*z[42];
    z[15]= - static_cast<T>(9)+ z[15];
    z[15]=z[15]*z[32];
    z[16]= - static_cast<T>(7)- z[29];
    z[17]= - n<T>(7,2)*z[2] + static_cast<T>(7)+ z[22];
    z[17]=z[2]*z[17];
    z[16]=n<T>(1,2)*z[16] + z[17];
    z[16]=z[2]*z[16];
    z[16]=n<T>(19,12)*z[1] + z[16];
    z[16]=z[2]*z[16];
    z[15]=z[15] + z[16];
    z[14]=n<T>(1,4)*z[15] + z[14];
    z[14]=z[3]*z[14];
    z[15]= - z[30] - z[20];
    z[15]=z[7]*z[15];
    z[15]= - z[35] - n<T>(7,12)*z[23] + z[15];
    z[15]=z[15]*z[31];
    z[15]=z[15] - z[33] - z[7];
    z[15]=z[6]*z[15];
    z[11]= - static_cast<T>(67)- z[11];
    z[16]=n<T>(3,2) - z[2];
    z[16]=z[2]*z[16];
    z[16]= - n<T>(1,2) + z[16];
    z[16]=z[2]*z[16];
    z[11]=n<T>(1,8)*z[16] + n<T>(1,96)*z[11] + z[15];

    r += n<T>(1,2)*z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qqb_2lha_r2116(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2116(const std::array<dd_real,30>&);
#endif
