#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1786(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[3];
    z[2]=k[10];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=z[4] + z[1];
    z[7]=npow(z[5],2);
    z[8]=z[7]*z[6];
    z[9]= - static_cast<T>(1)+ z[5];
    z[9]=z[5]*z[9];
    z[8]=z[9] + z[8];
    z[8]=z[4]*z[8];
    z[9]=z[1]*z[7];
    z[8]=z[9] + z[8];
    z[9]=npow(z[1],2);
    z[10]= - 2*z[1] - z[4];
    z[10]=z[4]*z[10];
    z[10]= - z[9] + z[10];
    z[10]=z[3]*z[10];
    z[6]= - static_cast<T>(1)+ z[6];
    z[6]=z[4]*z[6];
    z[6]=z[10] - z[1] + z[6];
    z[6]=z[3]*z[6];
    z[6]=z[4] + z[6];
    z[10]=2*z[3];
    z[6]=z[6]*z[10];
    z[6]=n<T>(3,8)*z[8] + z[6];
    z[6]=z[2]*z[6];
    z[8]= - z[4]*z[1];
    z[8]= - z[9] + z[8];
    z[8]=z[3]*z[8];
    z[8]=4*z[8] - 5*z[1] - z[4];
    z[8]=z[3]*z[8];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[3]*z[8];
    z[7]=z[4]*z[7];
    z[7]=n<T>(1,2)*z[5] + z[7];
    z[6]=z[6] + n<T>(3,8)*z[7] + z[8];
    z[6]=z[2]*z[6];
    z[7]= - z[9]*z[10];
    z[7]= - 3*z[1] + z[7];
    z[7]=z[3]*z[7];
    z[7]= - static_cast<T>(1)+ z[7];
    z[7]=z[3]*z[7];

    r += z[6] + z[7];
 
    return r;
}

template double qqb_2lha_r1786(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1786(const std::array<dd_real,30>&);
#endif
