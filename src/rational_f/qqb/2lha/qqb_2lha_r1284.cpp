#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1284(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[9];
    z[6]=k[10];
    z[7]=k[4];
    z[8]= - z[6]*z[7];
    z[9]= - z[6] + z[5];
    z[9]=z[4]*z[9];
    z[8]=z[8] + z[9];
    z[9]=2*z[2];
    z[10]=z[4] - 1;
    z[11]=z[1] - z[10];
    z[11]=z[2]*z[11];
    z[11]=z[11] - z[1] - 1;
    z[11]=z[11]*z[9];
    z[8]=n<T>(1,2)*z[8] + z[11];
    z[8]=z[3]*z[8];
    z[11]=2*z[4] - static_cast<T>(2)- z[1];
    z[11]=z[11]*z[9];
    z[11]=static_cast<T>(3)+ z[11];
    z[11]=z[2]*z[11];
    z[8]=z[8] - n<T>(1,2)*z[5] + z[11];
    z[8]=z[3]*z[8];
    z[9]= - z[10]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=z[2]*z[9];
    z[8]=z[9] + z[8];

    r += n<T>(3,5)*z[8];
 
    return r;
}

template double qqb_2lha_r1284(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1284(const std::array<dd_real,30>&);
#endif
