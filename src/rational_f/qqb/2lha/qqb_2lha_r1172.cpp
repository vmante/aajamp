#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1172(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[6];
    z[6]=k[2];
    z[7]=k[7];
    z[8]=k[8];
    z[9]=k[11];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=z[6]*z[7];
    z[13]=z[12]*z[8];
    z[14]=5*z[8];
    z[15]=3*z[8];
    z[16]= - n<T>(5,2) - z[15];
    z[16]=z[7]*z[16];
    z[16]=n<T>(3,2)*z[13] + z[14] + z[16];
    z[16]=z[6]*z[16];
    z[17]=z[15] + 5;
    z[18]=n<T>(1,2)*z[7];
    z[17]=z[17]*z[18];
    z[19]=n<T>(1,2)*z[11];
    z[20]=static_cast<T>(1)- z[10];
    z[20]=z[20]*z[19];
    z[20]=z[20] - n<T>(5,3) - n<T>(3,2)*z[10];
    z[20]=z[11]*z[20];
    z[21]=z[5] - 1;
    z[22]= - z[3]*z[21];
    z[22]=z[22] - n<T>(7,3) + z[1];
    z[22]=z[1]*z[22];
    z[23]= - n<T>(101,3) - z[5];
    z[14]=z[20] + z[16] + z[17] - z[14] - z[10] + n<T>(1,2)*z[23] + z[22];
    z[16]=n<T>(1,2)*z[5];
    z[18]=z[16] + z[18];
    z[20]=z[1] + 1;
    z[22]=n<T>(1,4)*z[10];
    z[23]=z[22]*z[5];
    z[24]= - static_cast<T>(3)- n<T>(5,6)*z[11];
    z[24]=z[11]*z[24];
    z[24]=z[24] - z[23] - z[20] + z[18];
    z[25]= - n<T>(7,3) - n<T>(1,4)*z[5];
    z[25]=z[3]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[25]=npow(z[3],2);
    z[26]=3*z[25];
    z[27]= - n<T>(1,2) + z[3];
    z[27]=z[27]*z[26];
    z[28]=n<T>(1,4) - z[3];
    z[26]=z[28]*z[26];
    z[28]=z[2]*npow(z[3],3);
    z[26]=z[26] + z[28];
    z[26]=z[2]*z[26];
    z[26]=z[27] + z[26];
    z[26]=z[2]*z[26];
    z[27]=z[11] + 1;
    z[29]=n<T>(1,2)*z[4];
    z[30]= - z[27]*z[29];
    z[31]=n<T>(1,4)*z[11];
    z[30]=z[30] - static_cast<T>(1)- z[31];
    z[30]=z[11]*z[30];
    z[32]= - static_cast<T>(1)- z[23];
    z[26]=z[26] - n<T>(3,4)*z[25] + n<T>(1,2)*z[32] + z[30];
    z[26]=z[26]*z[29];
    z[20]=z[20]*z[3];
    z[30]=3*z[1];
    z[32]=3*z[20] - static_cast<T>(5)- z[30];
    z[32]=z[3]*z[32];
    z[32]=n<T>(3,2) + z[32];
    z[32]=z[3]*z[32];
    z[33]= - n<T>(5,2) - z[1];
    z[33]=z[3]*z[33];
    z[33]=n<T>(3,2) + z[33];
    z[33]=z[33]*z[25];
    z[33]=z[33] + z[28];
    z[33]=z[2]*z[33];
    z[32]=n<T>(1,2)*z[32] + z[33];
    z[32]=z[2]*z[32];
    z[33]=static_cast<T>(7)+ z[30];
    z[33]=z[3]*z[33];
    z[33]=z[33] + n<T>(1,6) - z[30];
    z[33]=z[3]*z[33];
    z[33]=static_cast<T>(1)+ z[33];
    z[32]=n<T>(1,4)*z[33] + z[32];
    z[32]=z[2]*z[32];
    z[24]=z[26] + n<T>(1,2)*z[24] + z[32];
    z[24]=z[4]*z[24];
    z[26]= - static_cast<T>(1)- n<T>(9,8)*z[8];
    z[26]=z[7]*z[26];
    z[13]=n<T>(3,8)*z[13] + n<T>(13,8)*z[8] + z[26];
    z[26]=n<T>(1,2)*z[6];
    z[13]=z[13]*z[26];
    z[32]= - n<T>(1,2) - n<T>(1,3)*z[8];
    z[33]=static_cast<T>(1)+ n<T>(9,16)*z[8];
    z[33]=z[7]*z[33];
    z[13]=z[13] + n<T>(19,4)*z[32] + z[33];
    z[13]=z[6]*z[13];
    z[32]=static_cast<T>(67)+ n<T>(35,2)*z[8];
    z[33]= - static_cast<T>(1)- n<T>(3,8)*z[8];
    z[33]=z[7]*z[33];
    z[32]=n<T>(1,12)*z[32] + z[33];
    z[13]=n<T>(1,2)*z[32] + z[13];
    z[13]=z[6]*z[13];
    z[32]=z[6]*z[8];
    z[32]=5*z[32];
    z[33]= - n<T>(21,2) + z[32];
    z[33]=z[33]*z[26];
    z[33]=z[33] - n<T>(35,12)*z[8] - static_cast<T>(3)+ z[22];
    z[34]=n<T>(1,2)*z[10];
    z[35]= - n<T>(7,6)*z[8] - static_cast<T>(1)- z[34];
    z[31]=z[35]*z[31];
    z[31]=z[31] - n<T>(35,24)*z[8] - static_cast<T>(1)- n<T>(5,8)*z[10];
    z[31]=z[11]*z[31];
    z[31]= - z[34] + z[31];
    z[35]=n<T>(1,2)*z[2];
    z[31]=z[31]*z[35];
    z[35]= - n<T>(5,48)*z[8] - n<T>(2,3) + n<T>(1,16)*z[10];
    z[35]=z[11]*z[35];
    z[31]=z[31] + n<T>(1,4)*z[33] + z[35];
    z[31]=z[2]*z[31];
    z[33]=5*z[3];
    z[35]= - static_cast<T>(17)- z[12];
    z[35]=n<T>(1,2)*z[35] + z[33];
    z[21]= - z[10]*z[21];
    z[36]=z[4]*z[10]*z[5];
    z[21]= - z[36] + z[5] + z[21];
    z[36]=z[3] - n<T>(3,2);
    z[37]=z[3]*z[36];
    z[21]=z[37] + n<T>(1,8)*z[21];
    z[21]=z[21]*z[29];
    z[29]= - z[5] + z[10];
    z[29]=n<T>(1,8)*z[29] - z[11];
    z[29]=n<T>(1,2)*z[29] - z[3];
    z[29]=z[2]*z[29];
    z[21]=z[21] + n<T>(1,4)*z[35] + z[29];
    z[21]=z[4]*z[21];
    z[29]=static_cast<T>(3)- z[10];
    z[35]= - static_cast<T>(3)- z[11];
    z[35]=z[11]*z[35];
    z[35]=n<T>(3,4)*z[10] + z[35];
    z[35]=z[2]*z[35];
    z[29]=z[35] + n<T>(1,4)*z[29] - 3*z[11];
    z[29]=z[2]*z[29];
    z[35]=z[12] - z[7];
    z[35]=static_cast<T>(47)+ 7*z[35];
    z[35]=z[6]*z[35];
    z[29]=z[29] - n<T>(13,3) + n<T>(1,4)*z[35];
    z[21]=n<T>(1,4)*z[29] + z[21];
    z[21]=z[4]*z[21];
    z[29]= - static_cast<T>(5)+ n<T>(1,2)*z[8];
    z[13]=z[21] + z[31] + n<T>(1,12)*z[29] + z[13];
    z[13]=z[9]*z[13];
    z[18]=z[33] - static_cast<T>(9)- z[18];
    z[18]=z[3]*z[18];
    z[21]=n<T>(1,2) - z[5];
    z[21]=z[10]*z[21];
    z[21]=z[7] + n<T>(3,2)*z[5] + z[21];
    z[18]=n<T>(1,2)*z[21] + z[18];
    z[21]=z[27]*z[11];
    z[27]=static_cast<T>(1)+ z[3];
    z[27]=z[3]*z[27];
    z[27]= - z[21] + z[27];
    z[27]=z[2]*z[27];
    z[29]=z[36]*z[25];
    z[23]= - z[23] + z[29];
    z[23]=z[4]*z[23];
    z[18]=z[23] + n<T>(1,2)*z[18] + z[27];
    z[18]=z[4]*z[18];
    z[23]= - n<T>(9,2)*z[12] + z[7] + z[34] - n<T>(91,3) - z[16];
    z[23]=n<T>(1,2)*z[23] + n<T>(17,3)*z[3];
    z[16]= - z[16] - z[10];
    z[16]=n<T>(1,2)*z[16] - z[21];
    z[16]=n<T>(1,2)*z[16] - z[3];
    z[16]=z[2]*z[16];
    z[16]=z[18] + n<T>(1,2)*z[23] + z[16];
    z[16]=z[4]*z[16];
    z[12]= - z[12]*z[15];
    z[15]=static_cast<T>(13)+ 15*z[8];
    z[15]=z[7]*z[15];
    z[15]= - 23*z[8] + z[15];
    z[12]=n<T>(1,2)*z[15] + z[12];
    z[12]=z[12]*z[26];
    z[15]=n<T>(173,2) + 53*z[8];
    z[18]= - n<T>(3,2) - z[8];
    z[18]=z[7]*z[18];
    z[12]=z[12] + n<T>(1,6)*z[15] + 3*z[18];
    z[12]=z[6]*z[12];
    z[15]=z[17] - n<T>(37,6)*z[8] - n<T>(47,3) - z[34];
    z[17]=static_cast<T>(1)- z[22];
    z[17]=z[11]*z[17];
    z[12]=z[17] + n<T>(1,2)*z[15] + z[12];
    z[15]= - n<T>(5,3) + z[34];
    z[15]=z[15]*z[19];
    z[15]=z[15] - n<T>(1,12)*z[8] - n<T>(9,4) + z[10];
    z[15]=z[11]*z[15];
    z[17]= - z[32] + n<T>(11,2) + 3*z[10];
    z[15]=n<T>(1,4)*z[17] + z[15];
    z[15]=z[2]*z[15];
    z[12]=z[16] + n<T>(1,2)*z[12] + z[15];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[9]*z[12];
    z[13]= - n<T>(17,12) - z[30];
    z[15]=static_cast<T>(2)+ n<T>(9,8)*z[1];
    z[15]=z[1]*z[15];
    z[15]=n<T>(7,8) + z[15];
    z[15]=z[3]*z[15];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[3]*z[13];
    z[15]=n<T>(9,8) - z[20];
    z[15]=z[15]*z[25];
    z[15]=z[15] + n<T>(1,2)*z[28];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[16]=z[1]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[16]=z[3]*z[16];
    z[16]=z[16] - static_cast<T>(2)- n<T>(9,4)*z[1];
    z[16]=z[3]*z[16];
    z[16]=n<T>(3,4) + z[16];
    z[16]=z[3]*z[16];
    z[15]=z[16] + z[15];
    z[15]=z[2]*z[15];
    z[13]=z[15] + n<T>(1,8) + z[13];
    z[13]=z[2]*z[13];
    z[15]=n<T>(17,6) + z[30];
    z[15]=z[1]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[3]*z[15];
    z[15]=z[15] + n<T>(7,6) - z[1];
    z[13]=n<T>(1,4)*z[15] + z[13];
    z[13]=z[2]*z[13];

    r += z[12] + z[13] + n<T>(1,8)*z[14] + z[24];
 
    return r;
}

template double qqb_2lha_r1172(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1172(const std::array<dd_real,30>&);
#endif
