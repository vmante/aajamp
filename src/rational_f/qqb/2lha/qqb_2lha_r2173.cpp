#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2173(const std::array<T,30>& k) {
  T z[44];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[4];
    z[7]=k[28];
    z[8]=k[2];
    z[9]=k[7];
    z[10]=k[8];
    z[11]=k[11];
    z[12]=k[10];
    z[13]=k[18];
    z[14]=z[12] + n<T>(1,3);
    z[14]=z[14]*z[12];
    z[15]=n<T>(1,3)*z[12];
    z[16]=z[15]*z[9];
    z[17]=n<T>(3,2)*z[5];
    z[18]= - z[16] + z[14] - z[17];
    z[19]=3*z[5];
    z[20]=z[19] + z[4];
    z[21]=n<T>(1,2)*z[9];
    z[22]=z[21] + z[20];
    z[23]=static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[24]=z[7]*z[23];
    z[25]=z[1] - z[20];
    z[25]=z[3]*z[25];
    z[22]=n<T>(3,4)*z[25] + n<T>(1,2)*z[22] + z[24];
    z[22]=z[3]*z[22];
    z[24]=npow(z[7],2);
    z[25]= - z[3]*z[7];
    z[25]=z[24] + z[25];
    z[26]=n<T>(1,2)*z[1];
    z[27]=z[26] - 1;
    z[28]=3*z[3];
    z[25]=z[28]*z[27]*z[25];
    z[27]=n<T>(1,3)*z[9];
    z[29]=z[8]*z[27];
    z[29]=z[29] + n<T>(1,3);
    z[29]=z[29]*npow(z[12],2);
    z[30]=n<T>(3,2)*z[24];
    z[25]=z[25] - z[30] + z[29];
    z[29]=n<T>(1,2)*z[6];
    z[25]=z[25]*z[29];
    z[31]=3*z[7];
    z[23]= - z[23]*z[31];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[7]*z[23];
    z[16]=z[8]*z[16];
    z[16]=z[25] + z[16] + z[22] + n<T>(1,2)*z[18] + z[23];
    z[16]=z[6]*z[16];
    z[18]=z[4] + 1;
    z[18]=n<T>(1,2)*z[18];
    z[22]=z[17] + z[18] - z[15];
    z[23]=n<T>(9,2)*z[24] + z[17] + z[27];
    z[25]=n<T>(1,2)*z[8];
    z[23]=z[23]*z[25];
    z[32]=n<T>(1,4)*z[7];
    z[33]= - 9*z[7] - static_cast<T>(1)- z[1];
    z[33]=z[33]*z[32];
    z[34]=n<T>(1,2)*z[4];
    z[17]=z[34] + z[17];
    z[35]= - z[2] + static_cast<T>(2)- z[17];
    z[35]=z[3]*z[35];
    z[36]=n<T>(1,4)*z[4];
    z[37]=z[36] - 1;
    z[35]=z[35] + z[37];
    z[35]=z[1]*z[35];
    z[18]= - z[18] + z[35];
    z[18]=z[3]*z[18];
    z[16]=z[16] + z[23] + z[18] + z[33] + n<T>(1,2)*z[22] + z[27];
    z[16]=z[6]*z[16];
    z[18]=n<T>(1,2)*z[3];
    z[22]= - z[9] - z[20];
    z[22]=z[22]*z[18];
    z[22]=z[22] + z[21] + z[34] + z[19];
    z[22]=z[3]*z[22];
    z[23]=n<T>(1,2)*z[5];
    z[27]= - z[23] - z[24];
    z[22]=3*z[27] + z[22];
    z[22]=z[22]*z[29];
    z[27]=z[9] + z[31];
    z[27]=z[27]*z[18];
    z[17]=z[27] - z[30] - n<T>(5,2)*z[9] - static_cast<T>(3)- z[17];
    z[17]=z[17]*z[18];
    z[18]=static_cast<T>(7)+ z[31];
    z[18]=z[18]*z[32];
    z[27]=z[19] + 7*z[24];
    z[29]=n<T>(3,4)*z[8];
    z[27]=z[27]*z[29];
    z[30]= - static_cast<T>(3)+ z[4];
    z[17]=z[22] + z[27] + z[17] + z[18] + z[9] + n<T>(1,4)*z[30] + z[19];
    z[17]=z[6]*z[17];
    z[18]=z[5] - 1;
    z[22]= - z[9] - z[18];
    z[27]= - n<T>(11,2) + z[31];
    z[27]=z[7]*z[27];
    z[30]= - z[5] - 3*z[24];
    z[30]=z[8]*z[30];
    z[22]=n<T>(9,2)*z[30] + 3*z[22] + z[27];
    z[22]=z[22]*z[25];
    z[25]=5*z[7];
    z[27]=z[25] + 3*z[9];
    z[30]= - n<T>(1,3) - z[27];
    z[32]=n<T>(5,4)*z[7] - static_cast<T>(2)+ n<T>(3,4)*z[9];
    z[32]=z[3]*z[32];
    z[17]=z[17] + z[22] + n<T>(1,4)*z[30] + z[32];
    z[17]=z[6]*z[17];
    z[22]=n<T>(3,4)*z[7];
    z[25]= - static_cast<T>(1)- z[25];
    z[25]=z[25]*z[22];
    z[30]=z[5] + 5*z[24];
    z[29]=z[30]*z[29];
    z[25]=z[29] + z[25] - n<T>(3,4) + z[9];
    z[25]=z[8]*z[25];
    z[29]= - static_cast<T>(9)- z[9];
    z[29]=n<T>(1,2)*z[29] + z[7];
    z[25]=n<T>(1,2)*z[29] + z[25];
    z[25]=z[8]*z[25];
    z[29]=n<T>(1,2)*z[2];
    z[30]= - z[29] - 1;
    z[30]=z[13]*z[30];
    z[30]=z[15] - n<T>(2,3) + z[30];
    z[30]=z[2]*z[30];
    z[32]=n<T>(1,2)*z[13];
    z[17]=z[17] + z[25] + z[30] + n<T>(1,3) - z[32];
    z[17]=z[6]*z[17];
    z[25]=z[9]*z[10];
    z[22]=z[22] - z[9];
    z[30]=z[22]*z[7];
    z[25]=z[25] + z[30];
    z[25]=z[25]*z[8];
    z[30]=2*z[10];
    z[33]=z[30] - n<T>(1,4);
    z[33]=z[33]*z[9];
    z[25]=z[33] - z[25];
    z[33]=2*z[9];
    z[35]= - z[33] + n<T>(3,2)*z[7];
    z[38]=n<T>(11,4) + z[35];
    z[38]=z[7]*z[38];
    z[38]=z[38] - n<T>(7,6)*z[10] + z[25];
    z[38]=z[8]*z[38];
    z[39]=z[10] - n<T>(1,4);
    z[39]=z[39]*z[9];
    z[40]=z[2]*z[10];
    z[41]= - n<T>(11,4) - z[22];
    z[41]=z[7]*z[41];
    z[38]=z[38] + z[41] - z[39] - n<T>(1,6)*z[40] + n<T>(13,4) + n<T>(1,3)*z[10];
    z[38]=z[8]*z[38];
    z[41]=n<T>(1,2)*z[10];
    z[42]= - static_cast<T>(1)+ z[41];
    z[42]=5*z[42] + z[2];
    z[38]=n<T>(1,3)*z[42] + z[38];
    z[38]=z[8]*z[38];
    z[42]=n<T>(5,6)*z[10];
    z[32]= - z[42] + n<T>(1,6)*z[12] + n<T>(1,3) + z[32];
    z[32]=z[12]*z[32];
    z[32]=z[13] + z[32];
    z[32]=z[2]*z[32];
    z[43]=z[15] + z[13];
    z[32]=z[32] - z[42] + n<T>(4,3) + z[43];
    z[32]=z[2]*z[32];
    z[17]=z[17] + z[38] + n<T>(2,3) + z[32];
    z[17]=z[11]*z[17];
    z[32]=n<T>(9,4)*z[7];
    z[20]= - z[32] - n<T>(1,4)*z[9] + n<T>(3,2) - z[20];
    z[20]=z[3]*z[20];
    z[36]=z[36] + z[5];
    z[20]=z[20] + n<T>(9,4)*z[24] + 3*z[36] + z[21];
    z[20]=z[3]*z[20];
    z[36]= - z[5] - n<T>(5,2)*z[24];
    z[20]=n<T>(3,2)*z[36] + z[20];
    z[20]=z[6]*z[20];
    z[36]= - 9*z[5] - static_cast<T>(5)- 3*z[4];
    z[28]=z[28] - z[32] + n<T>(1,4)*z[36] - z[9];
    z[28]=z[3]*z[28];
    z[15]=n<T>(15,2)*z[5] + z[15] - n<T>(3,2) + z[4] + z[27];
    z[27]=3*z[8];
    z[32]=z[5] + n<T>(11,4)*z[24];
    z[32]=z[32]*z[27];
    z[15]=z[20] + z[32] + n<T>(1,2)*z[15] + z[28];
    z[15]=z[6]*z[15];
    z[14]=z[13] + z[14];
    z[14]=z[2]*z[14];
    z[14]=z[21] + z[14] - n<T>(1,2) + z[43];
    z[20]= - z[23] - 2*z[24];
    z[20]=z[20]*z[27];
    z[18]= - n<T>(1,2)*z[18] - z[9];
    z[21]=n<T>(1,2) + 6*z[7];
    z[21]=z[7]*z[21];
    z[18]=z[20] + n<T>(3,2)*z[18] + z[21];
    z[18]=z[8]*z[18];
    z[20]=static_cast<T>(1)- z[2];
    z[20]=z[3]*z[20];
    z[14]=z[15] + z[18] + n<T>(1,2)*z[14] + z[20];
    z[14]=z[6]*z[14];
    z[15]=z[10]*z[33];
    z[18]=z[7]*z[35];
    z[15]=z[15] + z[18];
    z[15]=z[8]*z[15];
    z[18]=n<T>(1,2) - 4*z[10];
    z[18]=z[9]*z[18];
    z[20]= - z[31] - n<T>(17,4) + 4*z[9];
    z[20]=z[7]*z[20];
    z[15]=z[15] + z[20] + n<T>(4,3)*z[10] + z[18];
    z[15]=z[8]*z[15];
    z[18]=n<T>(1,3)*z[40] - n<T>(29,6) - z[10];
    z[20]= - n<T>(1,2) + z[30];
    z[20]=z[9]*z[20];
    z[21]=n<T>(17,4) + z[35];
    z[21]=z[7]*z[21];
    z[15]=z[15] + z[21] + n<T>(1,2)*z[18] + z[20];
    z[15]=z[8]*z[15];
    z[18]= - n<T>(5,3)*z[12] - n<T>(1,3) - 3*z[13];
    z[18]=z[18]*z[29];
    z[14]=z[17] + z[14] + z[15] + z[18] - static_cast<T>(1)- z[42];
    z[14]=z[11]*z[14];
    z[15]=n<T>(3,4)*z[5];
    z[17]= - z[37] - z[15];
    z[18]=npow(z[1],2);
    z[17]=z[18]*z[17];
    z[20]= - z[2]*z[1];
    z[18]=z[18] + z[20];
    z[18]=z[2]*z[18];
    z[17]=z[18] + z[17];
    z[17]=z[3]*z[17];
    z[18]=n<T>(1,2) - z[1];
    z[18]=z[18]*z[19];
    z[18]=z[18] + n<T>(7,2)*z[1] - static_cast<T>(1)- z[34];
    z[18]=z[1]*z[18];
    z[19]= - 3*z[1] - z[29];
    z[19]=z[2]*z[19];
    z[18]=z[19] + z[18];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[3]*z[17];
    z[18]=static_cast<T>(1)- z[1];
    z[15]=z[1]*z[18]*z[15];
    z[18]=n<T>(3,2) + z[35];
    z[18]=z[7]*z[18];
    z[18]=z[18] - n<T>(1,6)*z[10] + z[25];
    z[18]=z[8]*z[18];
    z[19]=static_cast<T>(1)+ z[41];
    z[20]= - n<T>(3,2) - z[22];
    z[20]=z[7]*z[20];

    r +=  - n<T>(1,4)*z[2] + z[14] + z[15] + z[16] + z[17] + z[18] + n<T>(1,3)*
      z[19] + z[20] - z[26] - z[39];
 
    return r;
}

template double qqb_2lha_r2173(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2173(const std::array<dd_real,30>&);
#endif
