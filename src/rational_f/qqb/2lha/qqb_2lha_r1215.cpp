#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1215(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[13];
    z[7]=k[12];
    z[8]=k[6];
    z[9]=k[8];
    z[10]=k[11];
    z[11]=k[10];
    z[12]=n<T>(1,2)*z[8];
    z[13]=static_cast<T>(1)- z[12];
    z[14]=n<T>(1,4)*z[8];
    z[15]=n<T>(5,2)*z[7] + static_cast<T>(1)+ z[14];
    z[15]=n<T>(1,2)*z[15] - z[6];
    z[15]=z[6]*z[15];
    z[13]=n<T>(1,2)*z[13] + 3*z[15];
    z[13]=z[6]*z[13];
    z[15]= - static_cast<T>(5)+ n<T>(17,4)*z[6];
    z[15]=z[6]*z[15];
    z[16]=z[6] - 1;
    z[17]=z[3]*z[16];
    z[15]=z[15] - n<T>(5,3)*z[17];
    z[17]=n<T>(1,2)*z[3];
    z[15]=z[15]*z[17];
    z[18]=npow(z[6],3);
    z[19]=z[5]*z[7]*z[18];
    z[13]=n<T>(3,2)*z[19] + z[15] - n<T>(1,2)*z[11] + z[13];
    z[15]=n<T>(1,2)*z[5];
    z[13]=z[13]*z[15];
    z[19]=17*z[7] + static_cast<T>(3)+ n<T>(5,2)*z[8];
    z[20]=n<T>(1,2)*z[4];
    z[21]= - static_cast<T>(3)+ z[20];
    z[21]=z[6]*z[21];
    z[19]=n<T>(1,8)*z[19] + z[21];
    z[19]=z[6]*z[19];
    z[21]=z[3]*z[2];
    z[22]= - n<T>(221,2) + 77*z[6];
    z[22]=n<T>(1,4)*z[22] - 7*z[21];
    z[23]=n<T>(1,6)*z[3];
    z[22]=z[22]*z[23];
    z[24]= - z[11] - z[12];
    z[13]=z[13] + z[22] + n<T>(3,8)*z[24] + z[19];
    z[13]=z[5]*z[13];
    z[19]=n<T>(1,4)*z[4];
    z[22]=z[11] + z[14];
    z[22]=z[22]*z[19];
    z[24]=n<T>(1,2)*z[6];
    z[25]= - n<T>(43,12) + z[4];
    z[25]=z[25]*z[24];
    z[26]=z[2] - 1;
    z[27]=z[26]*z[21];
    z[28]= - n<T>(65,4) + 79*z[2];
    z[27]=n<T>(1,12)*z[28] + z[27];
    z[27]=z[3]*z[27];
    z[28]=n<T>(1,8)*z[8];
    z[29]=z[28] + n<T>(41,8) - z[11];
    z[13]=z[13] + z[27] + z[25] + z[22] + n<T>(1,2)*z[29] + z[7];
    z[13]=z[5]*z[13];
    z[22]=z[4]*z[9];
    z[22]=n<T>(5,2)*z[22];
    z[25]=z[2]*z[9];
    z[27]=n<T>(3,4) + n<T>(1,3)*z[9];
    z[27]= - n<T>(29,6)*z[25] + 19*z[27] - z[22];
    z[27]=z[2]*z[27];
    z[29]= - n<T>(49,3) + n<T>(19,2)*z[4];
    z[27]=n<T>(1,2)*z[29] + z[27];
    z[29]=n<T>(1,2)*z[2];
    z[27]=z[27]*z[29];
    z[30]=n<T>(59,6) + 3*z[9];
    z[31]= - 13*z[25] + n<T>(109,2) + 35*z[9];
    z[31]=z[2]*z[31];
    z[31]=n<T>(1,12)*z[31] - static_cast<T>(7)- n<T>(31,12)*z[9];
    z[31]=z[2]*z[31];
    z[30]=n<T>(1,4)*z[30] + z[31];
    z[30]=z[2]*z[30];
    z[31]=z[9] + n<T>(5,2);
    z[32]= - z[9] + n<T>(1,3)*z[25];
    z[33]= - n<T>(5,6) + z[32];
    z[33]=z[2]*z[33];
    z[33]=z[33] + n<T>(5,3) + z[9];
    z[33]=z[2]*z[33];
    z[33]= - n<T>(1,3)*z[31] + z[33];
    z[34]=z[3]*npow(z[2],2);
    z[33]=z[33]*z[34];
    z[30]=z[30] + n<T>(1,4)*z[33];
    z[30]=z[3]*z[30];
    z[33]=static_cast<T>(1)+ n<T>(3,4)*z[9];
    z[35]= - z[11]*z[33];
    z[35]=n<T>(1,4)*z[7] - n<T>(1,4) + z[35];
    z[35]=z[4]*z[35];
    z[27]=z[30] + z[27] + z[35] - z[33];
    z[30]= - z[6]*z[7];
    z[30]=z[14] + z[30];
    z[30]=z[6]*z[30];
    z[30]= - z[14] + z[30];
    z[30]=z[6]*z[30];
    z[33]= - static_cast<T>(1)+ z[24];
    z[33]=z[6]*z[33];
    z[33]=n<T>(1,2) + z[33];
    z[17]=z[33]*z[17];
    z[33]=n<T>(13,4) - z[6];
    z[33]=z[6]*z[33];
    z[33]= - n<T>(9,4) + z[33];
    z[33]=z[6]*z[33];
    z[17]=z[33] + z[17];
    z[17]=z[3]*z[17];
    z[17]=z[30] + z[17];
    z[17]=z[5]*z[17];
    z[30]= - static_cast<T>(5)+ z[12];
    z[33]=static_cast<T>(1)- n<T>(7,8)*z[7];
    z[33]=z[6]*z[33];
    z[30]=n<T>(1,4)*z[30] + z[33];
    z[30]=z[6]*z[30];
    z[33]=n<T>(1,4)*z[6];
    z[35]=static_cast<T>(13)- n<T>(9,2)*z[6];
    z[35]=z[35]*z[33];
    z[16]= - z[2] + z[16];
    z[16]=z[3]*z[16];
    z[16]=n<T>(5,24)*z[16] - n<T>(7,3) + z[35];
    z[16]=z[3]*z[16];
    z[16]=n<T>(1,4)*z[17] + z[16] - z[28] + z[30];
    z[16]=z[5]*z[16];
    z[17]=static_cast<T>(1)- n<T>(3,8)*z[7];
    z[17]=3*z[17] - z[20];
    z[17]=z[6]*z[17];
    z[28]=n<T>(5,4)*z[34] - 13*z[6] + static_cast<T>(13)+ 25*z[2];
    z[23]=z[28]*z[23];
    z[14]= - z[11] + z[14];
    z[14]=z[4]*z[14];
    z[14]=n<T>(19,12) + z[14];
    z[14]=z[16] + z[23] + n<T>(1,4)*z[14] + z[17];
    z[14]=z[5]*z[14];
    z[16]=3*z[11];
    z[17]= - static_cast<T>(5)- z[16];
    z[17]=z[17]*z[19];
    z[19]=static_cast<T>(5)- 11*z[2];
    z[19]=z[19]*z[21];
    z[17]=n<T>(3,4)*z[19] - n<T>(27,4)*z[2] + z[17] + n<T>(7,6) - z[7];
    z[14]=n<T>(1,2)*z[17] + z[14];
    z[14]=z[5]*z[14];
    z[14]=n<T>(1,2)*z[27] + z[14];
    z[14]=z[10]*z[14];
    z[17]=z[9]*z[16];
    z[17]=z[17] - 5*z[7] - n<T>(1,2) + z[11];
    z[17]=z[17]*z[20];
    z[19]=z[9] - z[11];
    z[17]=z[17] + n<T>(7,3) + n<T>(3,2)*z[19];
    z[19]= - static_cast<T>(43)- n<T>(53,2)*z[9];
    z[19]=n<T>(1,3)*z[19] + z[22];
    z[19]=n<T>(1,2)*z[19] + n<T>(11,3)*z[25];
    z[19]=z[2]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[19]=9*z[25] - n<T>(209,6) - 21*z[9];
    z[19]=z[2]*z[19];
    z[19]=z[19] + n<T>(127,3) + 15*z[9];
    z[19]=z[2]*z[19];
    z[19]= - 3*z[31] + z[19];
    z[20]=n<T>(1,2)*z[9];
    z[22]= - n<T>(1,6)*z[25] - n<T>(1,3) + z[20];
    z[22]=z[22]*z[29];
    z[22]=z[22] + n<T>(1,3) - n<T>(1,4)*z[9];
    z[22]=z[2]*z[22];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=n<T>(1,6)*z[20] + z[22];
    z[20]=z[20]*z[21];
    z[19]=n<T>(1,8)*z[19] + z[20];
    z[19]=z[3]*z[19];
    z[13]=z[14] + z[13] + n<T>(1,2)*z[17] + z[19];
    z[13]=z[10]*z[13];
    z[14]=5*z[9];
    z[17]=static_cast<T>(7)+ z[7];
    z[17]=z[4]*z[17];
    z[19]= - n<T>(13,3) - z[14];
    z[19]=z[2]*z[19];
    z[14]=z[19] + z[17] + z[14] - 7*z[1] + z[12] + n<T>(61,6) + z[16];
    z[16]=z[4] - z[1];
    z[17]=z[16] - 1;
    z[17]=z[6]*z[17]*npow(z[4],2);
    z[19]=z[8] - 1;
    z[19]=z[19]*z[1];
    z[20]= - n<T>(7,8) + z[16];
    z[20]=z[4]*z[20];
    z[20]=z[17] + n<T>(1,8)*z[19] + z[20];
    z[20]=z[6]*z[20];
    z[14]=n<T>(1,4)*z[14] + z[20];
    z[16]=n<T>(25,2) + z[8] - z[16];
    z[20]=z[4] - static_cast<T>(5)+ z[1];
    z[20]=z[6]*z[4]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[16]=z[16]*z[33];
    z[20]=z[1]*z[7];
    z[12]=n<T>(19,3) - z[12];
    z[12]=z[16] + n<T>(3,4)*z[20] + n<T>(1,8)*z[12] - z[7];
    z[12]=z[6]*z[12];
    z[16]= - z[7] + n<T>(1,2)*z[20];
    z[15]=z[16]*z[18]*z[15];
    z[16]=static_cast<T>(1)- n<T>(1,2)*z[1];
    z[16]=z[16]*z[24];
    z[16]=z[16] - z[7] + n<T>(1,8)*z[20];
    z[16]=z[16]*npow(z[6],2);
    z[15]=z[16] + z[15];
    z[15]=z[5]*z[15];
    z[16]= - 73*z[6] + 17*z[3];
    z[16]=z[3]*z[16];
    z[12]=z[15] + n<T>(1,48)*z[16] + n<T>(1,4)*z[11] + z[12];
    z[12]=z[5]*z[12];
    z[15]=3*z[4] - n<T>(27,4) - z[1];
    z[15]=z[4]*z[15];
    z[15]=n<T>(1,4)*z[19] + z[15];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[6]*z[15];
    z[16]=n<T>(25,3) + 3*z[8];
    z[16]=n<T>(31,3)*z[4] + n<T>(1,2)*z[16] - n<T>(25,3)*z[1];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[15]=z[15]*z[24];
    z[16]= - n<T>(157,2) - 13*z[1];
    z[17]=z[3]*z[26];
    z[16]=n<T>(1,8)*z[16] - 2*z[17];
    z[16]=z[3]*z[16];
    z[12]=z[12] + n<T>(1,3)*z[16] + z[15] + n<T>(7,8)*z[20] - n<T>(1,2)*z[7] - n<T>(1,16)
   *z[8] + n<T>(4,3) + n<T>(1,8)*z[11];
    z[12]=z[5]*z[12];
    z[15]=static_cast<T>(7)+ n<T>(13,8)*z[1];
    z[16]= - n<T>(7,4)*z[25] + n<T>(7,2)*z[9] + z[15];
    z[16]=z[2]*z[16];
    z[15]=z[16] - n<T>(7,4)*z[9] - z[15];
    z[16]=n<T>(13,2) - z[9];
    z[17]=n<T>(13,6) + z[32];
    z[17]=z[2]*z[17];
    z[17]=z[17] - n<T>(13,3) + z[9];
    z[17]=z[2]*z[17];
    z[16]=n<T>(1,3)*z[16] + z[17];
    z[16]=z[3]*z[16];
    z[15]=n<T>(1,3)*z[15] + n<T>(1,8)*z[16];
    z[15]=z[3]*z[15];

    r += z[12] + z[13] + n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qqb_2lha_r1215(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1215(const std::array<dd_real,30>&);
#endif
