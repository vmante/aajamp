#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2327(const std::array<T,30>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[20];
    z[6]=k[6];
    z[7]=k[17];
    z[8]=k[3];
    z[9]=z[3]*z[1];
    z[10]=n<T>(1,4)*z[1];
    z[11]=z[10] + 1;
    z[11]=z[11]*z[1];
    z[12]= - n<T>(1,4) - z[11];
    z[12]=z[1]*z[12];
    z[12]=n<T>(1,2) + z[12];
    z[12]=z[12]*z[9];
    z[13]=n<T>(1,2)*z[1];
    z[14]=z[13] + 1;
    z[15]=z[14]*z[1];
    z[16]=static_cast<T>(1)+ 5*z[15];
    z[16]=z[1]*z[16];
    z[16]=n<T>(1,2) + z[16];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[3]*z[12];
    z[16]=n<T>(1,4)*z[6];
    z[17]=z[1] + n<T>(1,2);
    z[18]=z[17]*z[1];
    z[19]= - n<T>(3,2) - z[18];
    z[12]= - z[16] + n<T>(1,2)*z[19] + z[12];
    z[19]=3*z[5];
    z[12]=z[12]*z[19];
    z[20]=z[1] - n<T>(1,2);
    z[21]= - z[20]*z[13];
    z[21]=static_cast<T>(1)+ z[21];
    z[22]=z[1] - 1;
    z[23]= - z[22]*z[10];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[1]*z[23];
    z[23]= - n<T>(3,2) + z[23];
    z[23]=z[23]*z[9];
    z[24]= - static_cast<T>(1)+ n<T>(15,8)*z[1];
    z[24]=z[1]*z[24];
    z[24]= - n<T>(11,8) + z[24];
    z[24]=z[1]*z[24];
    z[23]=n<T>(3,2)*z[23] - n<T>(9,8) + z[24];
    z[23]=z[3]*z[23];
    z[24]=static_cast<T>(5)- n<T>(3,2)*z[6];
    z[24]=z[24]*z[16];
    z[12]=z[12] + z[24] + n<T>(3,2)*z[21] + z[23];
    z[12]=z[5]*z[12];
    z[21]=npow(z[1],2);
    z[23]= - n<T>(3,2) - z[21];
    z[23]=z[23]*z[13];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[9];
    z[24]=npow(z[1],3);
    z[25]=static_cast<T>(1)+ 5*z[24];
    z[23]=n<T>(1,2)*z[25] + z[23];
    z[23]=z[3]*z[23];
    z[25]=5*z[6];
    z[26]= - static_cast<T>(9)- z[25];
    z[26]=z[6]*z[26];
    z[26]= - static_cast<T>(27)+ z[26];
    z[16]=z[26]*z[16];
    z[26]=z[1] - n<T>(1,4);
    z[26]=z[26]*z[1];
    z[16]=z[16] + z[23] - n<T>(17,4) - z[26];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[5]*z[12];
    z[16]=npow(z[6],2);
    z[23]=15*z[6];
    z[27]=n<T>(77,8) - z[23];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,8) + z[27];
    z[27]=z[27]*z[16];
    z[28]=npow(z[6],3);
    z[29]=10*z[6];
    z[30]= - static_cast<T>(3)+ z[29];
    z[30]=z[30]*z[28];
    z[31]=z[8]*npow(z[6],4);
    z[30]=z[30] - n<T>(5,2)*z[31];
    z[30]=z[8]*z[30];
    z[31]=z[19] - n<T>(1,2);
    z[31]=z[31]*npow(z[5],2);
    z[32]=n<T>(1,4)*z[31];
    z[27]=z[30] + z[27] - z[32];
    z[27]=z[8]*z[27];
    z[30]=n<T>(1,2)*z[5];
    z[33]= - static_cast<T>(1)+ z[19];
    z[33]=z[5]*z[33];
    z[33]=static_cast<T>(1)+ n<T>(3,2)*z[33];
    z[33]=z[33]*z[30];
    z[34]= - n<T>(87,8) + z[29];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(3,8) + z[34];
    z[34]=z[6]*z[34];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[6]*z[34];
    z[27]=z[27] + z[34] + z[33];
    z[27]=z[8]*z[27];
    z[33]=n<T>(1,2)*z[3];
    z[34]=z[21]*z[33];
    z[35]= - z[22]*z[34];
    z[36]=z[20]*z[1];
    z[35]=z[36] + z[35];
    z[35]=z[35]*z[33];
    z[37]=z[1] + 1;
    z[38]= - z[37]*z[34];
    z[38]=z[18] + z[38];
    z[38]=z[5]*z[3]*z[38];
    z[35]=z[35] + z[38];
    z[35]=z[35]*z[19];
    z[38]=z[34]*z[20];
    z[38]=z[38] - z[26];
    z[38]=z[38]*z[3];
    z[35]= - z[38] + z[35];
    z[35]=z[2]*z[35]*z[30];
    z[39]=n<T>(1,2)*z[6];
    z[40]=n<T>(39,4) - z[25];
    z[40]=z[6]*z[40];
    z[40]=n<T>(11,4) + z[40];
    z[40]=z[6]*z[40];
    z[40]=n<T>(21,4) + z[40];
    z[40]=z[40]*z[39];
    z[12]=z[35] + z[27] + z[40] + z[12];
    z[12]=z[2]*z[12];
    z[27]=n<T>(3,2)*z[3];
    z[35]= - static_cast<T>(15)+ z[1];
    z[35]=z[1]*z[35];
    z[35]= - static_cast<T>(21)+ z[35];
    z[35]=z[1]*z[35];
    z[35]=static_cast<T>(5)+ z[35];
    z[35]=z[35]*z[27];
    z[40]=3*z[1];
    z[41]= - static_cast<T>(1)+ z[40];
    z[41]=z[1]*z[41];
    z[41]= - n<T>(23,2) + z[41];
    z[41]=z[1]*z[41];
    z[41]=static_cast<T>(1)+ z[41];
    z[41]=z[1]*z[41];
    z[35]=z[35] - n<T>(97,2) + z[41];
    z[35]=z[35]*z[33];
    z[41]= - n<T>(1,4) - z[40];
    z[41]=z[1]*z[41];
    z[41]= - static_cast<T>(7)+ z[41];
    z[41]=z[1]*z[41];
    z[17]=z[17]*z[6];
    z[42]=3*z[17] - n<T>(25,4) - 9*z[1];
    z[42]=z[6]*z[42];
    z[35]=z[42] + z[35] - static_cast<T>(7)+ z[41];
    z[41]=static_cast<T>(5)+ z[1];
    z[41]=z[1]*z[41];
    z[41]=static_cast<T>(3)+ z[41];
    z[41]=z[1]*z[41];
    z[41]=n<T>(1,2) + z[41];
    z[41]=z[41]*z[13];
    z[42]=n<T>(1,4)*z[3];
    z[43]=static_cast<T>(7)- z[21];
    z[43]=z[1]*z[43];
    z[43]=static_cast<T>(5)+ z[43];
    z[43]=z[1]*z[43];
    z[43]= - static_cast<T>(1)+ z[43];
    z[43]=z[43]*z[42];
    z[41]=z[43] + static_cast<T>(2)+ z[41];
    z[41]=z[3]*z[41];
    z[43]=z[1] + n<T>(7,4);
    z[44]= - z[1]*z[43];
    z[44]=n<T>(7,4) + z[44];
    z[44]=z[1]*z[44];
    z[41]=z[17] + z[41] + n<T>(3,2) + z[44];
    z[41]=z[41]*z[19];
    z[35]=n<T>(1,2)*z[35] + z[41];
    z[35]=z[5]*z[35];
    z[41]=n<T>(5,4) + z[18];
    z[41]=z[1]*z[41];
    z[41]= - n<T>(9,4) + z[41];
    z[41]=z[1]*z[41];
    z[44]=static_cast<T>(9)- n<T>(5,4)*z[21];
    z[44]=z[1]*z[44];
    z[44]= - n<T>(11,4) + z[44];
    z[44]=z[3]*z[44];
    z[41]=z[44] + n<T>(53,4) + z[41];
    z[41]=z[41]*z[33];
    z[44]=7*z[1];
    z[17]=z[17] + n<T>(3,4) + z[1];
    z[17]=z[6]*z[17];
    z[17]=n<T>(5,2)*z[17] + n<T>(65,8) + z[44];
    z[17]=z[6]*z[17];
    z[45]=z[1] + n<T>(3,8);
    z[45]=z[45]*z[1];
    z[46]=n<T>(7,2) - z[45];
    z[46]=z[1]*z[46];
    z[17]=z[35] + z[17] + z[41] + n<T>(27,4) + z[46];
    z[17]=z[5]*z[17];
    z[35]=z[1] + 2;
    z[41]=z[6]*z[35];
    z[41]= - 2*z[41] - z[43];
    z[41]=z[41]*z[25];
    z[41]=static_cast<T>(6)+ z[41];
    z[41]=z[41]*z[16];
    z[43]=static_cast<T>(1)+ z[6];
    z[28]=z[8]*z[43]*z[28];
    z[28]=5*z[28] + z[41] - n<T>(1,2)*z[31];
    z[28]=z[8]*z[28];
    z[31]=6*z[1];
    z[41]=z[6]*z[37];
    z[41]=30*z[41] - static_cast<T>(5)- n<T>(19,4)*z[1];
    z[41]=z[6]*z[41];
    z[41]=z[41] - n<T>(29,2) - z[31];
    z[41]=z[6]*z[41];
    z[41]=n<T>(1,4) + z[41];
    z[41]=z[6]*z[41];
    z[43]=z[1] + n<T>(7,2);
    z[46]=z[43]*z[19];
    z[47]=z[1] + n<T>(9,2);
    z[46]= - n<T>(1,2)*z[47] + z[46];
    z[46]=z[5]*z[46];
    z[46]=n<T>(3,2) + z[46];
    z[46]=z[46]*z[30];
    z[28]=z[28] + z[41] + z[46];
    z[28]=z[8]*z[28];
    z[41]= - static_cast<T>(2)- z[40];
    z[41]=z[41]*z[29];
    z[46]=27*z[1];
    z[41]=z[41] + n<T>(35,2) + z[46];
    z[41]=z[6]*z[41];
    z[48]=n<T>(95,2) + 21*z[1];
    z[41]=n<T>(1,4)*z[48] + z[41];
    z[41]=z[6]*z[41];
    z[48]=n<T>(13,2) - z[1];
    z[41]=n<T>(1,4)*z[48] + z[41];
    z[41]=z[6]*z[41];
    z[48]= - n<T>(5,2) + z[1];
    z[48]=z[1]*z[48];
    z[48]= - static_cast<T>(5)+ z[48];
    z[48]=z[48]*z[19];
    z[49]=n<T>(5,2) + z[40];
    z[49]=z[1]*z[49];
    z[49]=n<T>(25,2) + z[49];
    z[48]=n<T>(1,2)*z[49] + z[48];
    z[48]=z[5]*z[48];
    z[49]= - n<T>(7,4) + z[1];
    z[49]=z[1]*z[49];
    z[48]=z[48] - n<T>(27,4) + z[49];
    z[30]=z[48]*z[30];
    z[28]=z[28] + z[41] + z[30];
    z[28]=z[8]*z[28];
    z[30]=z[21] + 1;
    z[41]=z[30]*z[33];
    z[41]=z[41] - z[14];
    z[41]=z[41]*z[33];
    z[48]=2*z[1];
    z[49]=static_cast<T>(1)+ z[48];
    z[49]=z[49]*z[25];
    z[49]=z[49] - static_cast<T>(10)- n<T>(79,4)*z[1];
    z[49]=z[6]*z[49];
    z[50]= - static_cast<T>(19)- z[40];
    z[49]=n<T>(1,4)*z[50] + z[49];
    z[49]=z[6]*z[49];
    z[50]=4*z[1];
    z[49]=z[49] - n<T>(25,4) - z[50];
    z[49]=z[6]*z[49];
    z[12]=z[12] + z[28] + z[17] + z[41] + z[49];
    z[12]=z[2]*z[12];
    z[17]=z[13] - 1;
    z[28]= - z[17]*z[21];
    z[41]=z[24]*z[33];
    z[49]=z[1] - 3;
    z[51]= - z[49]*z[41];
    z[28]=z[28] + z[51];
    z[28]=z[3]*z[28];
    z[51]=z[1] + n<T>(5,2);
    z[52]= - z[1]*z[51];
    z[52]=n<T>(3,2) + z[52];
    z[52]=z[1]*z[52];
    z[52]=n<T>(3,2) + z[52];
    z[52]=z[1]*z[52];
    z[28]=z[52] + z[28];
    z[20]=z[20]*z[21];
    z[52]=z[24]*z[3];
    z[53]= - n<T>(5,4) + z[1];
    z[53]=z[53]*z[52];
    z[20]=z[20] + z[53];
    z[20]=z[3]*z[20];
    z[53]=n<T>(5,2)*z[1];
    z[54]=static_cast<T>(1)- z[53];
    z[54]=z[54]*z[52];
    z[54]= - z[24] + z[54];
    z[54]=z[3]*z[54];
    z[55]=z[6]*npow(z[3],2)*npow(z[1],4);
    z[54]=z[54] + z[55];
    z[39]=z[54]*z[39];
    z[20]=z[20] + z[39];
    z[20]=z[6]*z[20];
    z[20]=n<T>(1,2)*z[28] + z[20];
    z[28]=z[21] + n<T>(1,2);
    z[39]=z[21]*z[3];
    z[54]= - z[28]*z[39];
    z[56]= - static_cast<T>(1)+ 5*z[21];
    z[56]=z[1]*z[56];
    z[54]=z[56] + z[54];
    z[54]=z[54]*z[33];
    z[38]= - z[2]*z[38];
    z[26]=z[38] - z[8] + z[54] + static_cast<T>(1)- z[26];
    z[38]=n<T>(1,2)*z[2];
    z[26]=z[26]*z[38];
    z[54]=n<T>(3,2)*z[1];
    z[56]=z[54] + 1;
    z[34]= - z[56]*z[34];
    z[18]= - n<T>(3,4) + z[18];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(1,4) + z[18];
    z[18]=z[1]*z[18];
    z[18]=z[18] + z[34];
    z[18]=z[18]*z[33];
    z[34]=n<T>(1,2)*z[8];
    z[57]=n<T>(5,4) + z[1];
    z[57]=z[1]*z[57];
    z[57]= - n<T>(3,2)*z[8] + n<T>(9,4) + z[57];
    z[57]=z[57]*z[34];
    z[45]= - n<T>(3,4) - z[45];
    z[45]=z[1]*z[45];
    z[18]=z[26] + z[57] + z[18] - n<T>(3,8) + z[45];
    z[18]=z[2]*z[18];
    z[26]=n<T>(5,4)*z[1];
    z[45]= - static_cast<T>(1)- z[26];
    z[45]=z[45]*z[21];
    z[57]=n<T>(1,4)*z[37];
    z[58]=z[52]*z[57];
    z[45]=z[45] + z[58];
    z[45]=z[3]*z[45];
    z[58]= - static_cast<T>(1)+ z[34];
    z[58]=z[58]*z[34];
    z[41]= - z[21] + z[41];
    z[41]=z[2]*z[41]*z[33];
    z[28]=z[41] + z[58] + n<T>(1,2)*z[28] + z[45];
    z[28]=z[2]*z[28];
    z[41]=z[15] + n<T>(1,2);
    z[45]= - z[41]*z[39];
    z[58]=z[8] - z[51];
    z[58]=z[58]*z[34];
    z[17]=z[17]*z[1];
    z[58]=z[58] + static_cast<T>(1)- z[17];
    z[58]=z[8]*z[58];
    z[59]=z[1]*z[37];
    z[59]= - n<T>(1,2) + z[59];
    z[59]=z[1]*z[59];
    z[28]=z[28] + z[58] + z[45] - n<T>(1,4) + z[59];
    z[28]=z[28]*z[38];
    z[38]= - 3*z[37] + z[8];
    z[38]=z[8]*z[38];
    z[38]=n<T>(1,8)*z[38] + n<T>(3,8) + z[15];
    z[38]=z[8]*z[38];
    z[45]= - static_cast<T>(1)- n<T>(3,8)*z[1];
    z[45]=z[1]*z[45];
    z[45]= - static_cast<T>(1)+ z[45];
    z[45]=z[1]*z[45];
    z[38]=z[38] - n<T>(1,8) + z[45];
    z[38]=z[8]*z[38];
    z[45]=z[1] + 3;
    z[45]=z[45]*z[1];
    z[58]=z[45] + 5;
    z[58]=z[58]*z[1];
    z[59]=static_cast<T>(3)+ z[58];
    z[59]=z[1]*z[59];
    z[28]=z[28] + n<T>(1,8)*z[59] + z[38];
    z[28]=z[4]*z[28];
    z[38]= - n<T>(1,2) - z[40];
    z[38]=z[38]*z[13];
    z[59]=z[22]*z[34];
    z[38]=z[59] + static_cast<T>(1)+ z[38];
    z[34]=z[38]*z[34];
    z[38]=static_cast<T>(1)+ n<T>(3,4)*z[1];
    z[59]=z[38]*z[1];
    z[60]= - n<T>(5,8) + z[59];
    z[60]=z[1]*z[60];
    z[34]=z[34] - n<T>(1,4) + z[60];
    z[34]=z[8]*z[34];
    z[18]=z[28] + z[18] + n<T>(1,2)*z[20] + z[34];
    z[18]=z[4]*z[18];
    z[20]=n<T>(31,8) + z[48];
    z[20]=z[20]*z[40];
    z[28]=z[1] + n<T>(13,2);
    z[28]=z[28]*z[1];
    z[34]=n<T>(27,2) + z[28];
    z[34]=z[1]*z[34];
    z[34]=n<T>(23,2) + z[34];
    z[34]=z[1]*z[34];
    z[34]=n<T>(7,2) + z[34];
    z[27]=z[34]*z[27];
    z[34]= - n<T>(129,4) - z[44];
    z[34]=z[1]*z[34];
    z[34]= - n<T>(87,2) + z[34];
    z[34]=z[1]*z[34];
    z[27]=z[27] - n<T>(73,4) + z[34];
    z[27]=z[27]*z[33];
    z[34]=z[11] + n<T>(3,2);
    z[34]=z[34]*z[1];
    z[34]=z[34] + 1;
    z[34]=z[34]*z[1];
    z[34]=z[34] + n<T>(1,4);
    z[34]=z[34]*z[3];
    z[60]=z[45] + 3;
    z[60]=z[60]*z[1];
    z[60]=z[60] + 1;
    z[34]=z[34] - z[60];
    z[61]=z[34]*z[33];
    z[59]=z[61] + z[59] + n<T>(3,8);
    z[61]=z[6]*z[59];
    z[20]= - 3*z[61] + z[27] + n<T>(23,4) + z[20];
    z[20]=z[6]*z[20];
    z[27]= - static_cast<T>(19)- z[58];
    z[27]=z[27]*z[10];
    z[30]=z[1]*z[30];
    z[30]=static_cast<T>(17)+ z[30];
    z[30]=z[1]*z[30];
    z[30]=static_cast<T>(17)+ z[30];
    z[58]=z[11] + 1;
    z[61]=z[1]*z[58];
    z[61]= - n<T>(1,2) + z[61];
    z[61]=z[1]*z[61];
    z[61]= - n<T>(3,4) + z[61];
    z[61]=z[3]*z[61];
    z[30]=n<T>(1,4)*z[30] + z[61];
    z[30]=z[3]*z[30];
    z[34]= - z[3]*z[34];
    z[61]= - static_cast<T>(2)- z[54];
    z[61]=z[1]*z[61];
    z[34]=z[34] - n<T>(3,4) + z[61];
    z[34]=z[6]*z[34];
    z[27]=z[34] + z[30] - static_cast<T>(4)+ z[27];
    z[27]=z[27]*z[19];
    z[30]= - static_cast<T>(5)- z[54];
    z[30]=z[1]*z[30];
    z[30]=n<T>(3,2) + z[30];
    z[30]=z[1]*z[30];
    z[30]=static_cast<T>(57)+ z[30];
    z[30]=z[30]*z[10];
    z[34]= - static_cast<T>(9)- z[1];
    z[34]=z[1]*z[34];
    z[34]= - static_cast<T>(15)+ z[34];
    z[34]=z[1]*z[34];
    z[34]=static_cast<T>(7)+ z[34];
    z[34]=z[34]*z[13];
    z[34]=static_cast<T>(9)+ z[34];
    z[54]=3*z[3];
    z[34]=z[34]*z[54];
    z[17]= - n<T>(177,2) + z[17];
    z[17]=z[1]*z[17];
    z[17]=z[34] - static_cast<T>(129)+ z[17];
    z[17]=z[17]*z[42];
    z[17]=z[27] + z[20] + z[17] + static_cast<T>(20)+ z[30];
    z[17]=z[5]*z[17];
    z[20]= - z[51]*z[13];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[1]*z[20];
    z[20]= - n<T>(57,4) + z[20];
    z[20]=z[1]*z[20];
    z[27]= - z[1]*z[56];
    z[27]=static_cast<T>(13)+ z[27];
    z[27]=z[27]*z[21];
    z[27]= - n<T>(55,2) + z[27];
    z[27]=z[27]*z[33];
    z[26]= - static_cast<T>(1)+ z[26];
    z[26]=z[1]*z[26];
    z[26]=n<T>(37,2) + z[26];
    z[26]=z[1]*z[26];
    z[26]=z[27] + n<T>(221,4) + z[26];
    z[26]=z[3]*z[26];
    z[20]=z[26] - n<T>(55,2) + z[20];
    z[26]= - z[43]*z[40];
    z[26]= - n<T>(11,2) + z[26];
    z[27]=static_cast<T>(25)+ z[44];
    z[27]=z[1]*z[27];
    z[27]=static_cast<T>(33)+ z[27];
    z[27]=z[1]*z[27];
    z[27]=static_cast<T>(19)+ z[27];
    z[27]=z[27]*z[10];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[3]*z[27];
    z[30]=z[1] + n<T>(9,4);
    z[34]= - z[1]*z[30];
    z[34]= - n<T>(3,2) + z[34];
    z[34]=z[1]*z[34];
    z[34]= - n<T>(1,4) + z[34];
    z[27]=3*z[34] + z[27];
    z[27]=z[3]*z[27];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[27]= - z[59]*z[25];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[26]=z[6]*z[26];
    z[27]=z[1]*z[49];
    z[27]= - static_cast<T>(39)+ z[27];
    z[27]=z[1]*z[27];
    z[27]= - static_cast<T>(65)+ z[27];
    z[27]=z[27]*z[13];
    z[27]= - static_cast<T>(15)+ z[27];
    z[27]=z[3]*z[27];
    z[34]=static_cast<T>(83)+ n<T>(59,2)*z[1];
    z[34]=z[1]*z[34];
    z[27]=z[27] + n<T>(107,2) + z[34];
    z[27]=z[3]*z[27];
    z[34]= - n<T>(121,2) - 15*z[1];
    z[34]=z[1]*z[34];
    z[27]=z[27] - n<T>(87,2) + z[34];
    z[26]=n<T>(1,4)*z[27] + z[26];
    z[26]=z[6]*z[26];
    z[17]=z[17] + n<T>(1,2)*z[20] + z[26];
    z[17]=z[5]*z[17];
    z[20]= - static_cast<T>(23)- n<T>(31,4)*z[1];
    z[20]=z[20]*z[40];
    z[26]=n<T>(133,2) + 13*z[1];
    z[26]=z[1]*z[26];
    z[26]=n<T>(181,2) + z[26];
    z[26]=z[1]*z[26];
    z[26]=static_cast<T>(39)+ z[26];
    z[26]=z[26]*z[33];
    z[20]=z[26] - n<T>(389,8) + z[20];
    z[20]=z[3]*z[20];
    z[26]=z[1] + n<T>(29,2);
    z[27]=11*z[1];
    z[34]=z[26]*z[27];
    z[34]=static_cast<T>(331)+ z[34];
    z[34]=z[1]*z[34];
    z[34]=n<T>(365,2) + z[34];
    z[42]= - n<T>(117,2) - 16*z[1];
    z[42]=z[1]*z[42];
    z[42]= - n<T>(139,2) + z[42];
    z[42]=z[1]*z[42];
    z[42]= - n<T>(215,8) + z[42];
    z[42]=z[3]*z[42];
    z[34]=n<T>(1,2)*z[34] + z[42];
    z[34]=z[3]*z[34];
    z[42]= - static_cast<T>(6)- z[1];
    z[42]=z[1]*z[42];
    z[42]= - static_cast<T>(9)+ z[42];
    z[42]=z[1]*z[42];
    z[43]=z[3]*z[60];
    z[42]=z[43] - static_cast<T>(4)+ z[42];
    z[42]=z[3]*z[42];
    z[35]=z[35]*z[1];
    z[43]=z[35] + 1;
    z[42]=3*z[43] + z[42];
    z[42]=z[42]*z[29];
    z[51]= - static_cast<T>(66)- n<T>(43,2)*z[1];
    z[51]=z[1]*z[51];
    z[34]=z[42] + z[34] - n<T>(355,8) + z[51];
    z[34]=z[6]*z[34];
    z[42]= - static_cast<T>(3)- n<T>(13,4)*z[1];
    z[42]=z[1]*z[42];
    z[20]=z[34] + z[20] + n<T>(13,8) + z[42];
    z[20]=z[6]*z[20];
    z[34]=static_cast<T>(3)+ z[10];
    z[34]=z[34]*z[40];
    z[42]= - static_cast<T>(11)- z[1];
    z[42]=z[1]*z[42];
    z[42]= - n<T>(39,2) + z[42];
    z[42]=z[42]*z[13];
    z[42]= - static_cast<T>(5)+ z[42];
    z[42]=z[3]*z[42];
    z[34]=z[42] + n<T>(71,8) + z[34];
    z[34]=z[3]*z[34];
    z[42]= - static_cast<T>(8)- z[40];
    z[42]=z[1]*z[42];
    z[20]=z[20] + z[34] - n<T>(39,8) + z[42];
    z[20]=z[6]*z[20];
    z[34]=z[48] + 3;
    z[42]= - z[34]*z[54];
    z[42]=n<T>(13,2)*z[47] + z[42];
    z[42]=z[3]*z[42];
    z[47]=z[40] + 4;
    z[48]=z[3]*z[37];
    z[48]=z[48] - z[47];
    z[48]=z[3]*z[48];
    z[34]=z[48] + z[34];
    z[29]=z[34]*z[29];
    z[34]=19*z[1];
    z[48]= - n<T>(1,2) + z[34];
    z[29]=z[29] + n<T>(1,2)*z[48] + z[42];
    z[29]=z[6]*z[29];
    z[42]=z[1] + n<T>(3,2);
    z[48]=z[3]*z[42];
    z[48]= - n<T>(5,2) + z[48];
    z[48]=z[48]*z[33];
    z[29]=z[29] + z[48] - n<T>(3,4) + 5*z[1];
    z[29]=z[6]*z[29];
    z[29]= - static_cast<T>(3)+ z[29];
    z[29]=z[6]*z[29];
    z[38]=z[38]*z[19];
    z[38]=n<T>(1,8)*z[49] + z[38];
    z[38]=z[5]*z[38];
    z[38]=z[57] + z[38];
    z[38]=z[5]*z[38];
    z[48]=static_cast<T>(2)- z[33];
    z[48]=z[3]*z[48];
    z[48]= - n<T>(3,2) + z[48];
    z[48]=z[48]*z[25];
    z[49]= - static_cast<T>(1)+ z[33];
    z[49]=z[3]*z[49];
    z[49]= - n<T>(19,2) + z[49];
    z[48]=n<T>(1,2)*z[49] + z[48];
    z[48]=z[6]*z[48];
    z[48]= - n<T>(5,2) + z[48];
    z[16]=z[48]*z[16];
    z[16]=z[16] - z[32];
    z[16]=z[8]*z[16];
    z[16]=z[16] + z[29] + z[38];
    z[16]=z[8]*z[16];
    z[29]= - n<T>(355,2) - 23*z[1];
    z[29]=z[1]*z[29];
    z[32]=static_cast<T>(85)+ 33*z[1];
    z[32]=z[1]*z[32];
    z[32]=n<T>(209,4) + z[32];
    z[32]=z[3]*z[32];
    z[29]=z[32] - n<T>(349,2) + z[29];
    z[29]=z[3]*z[29];
    z[32]=static_cast<T>(65)- z[34];
    z[32]=z[1]*z[32];
    z[32]=n<T>(249,2) + z[32];
    z[29]=n<T>(1,2)*z[32] + z[29];
    z[32]=static_cast<T>(2)+ z[45];
    z[34]= - z[3]*z[43];
    z[32]=2*z[32] + z[34];
    z[32]=z[3]*z[32];
    z[34]= - static_cast<T>(4)- z[1];
    z[34]=z[1]*z[34];
    z[32]=z[32] - static_cast<T>(3)+ z[34];
    z[23]=z[32]*z[23];
    z[23]=n<T>(1,2)*z[29] + z[23];
    z[23]=z[6]*z[23];
    z[29]=static_cast<T>(4)- z[53];
    z[29]=z[1]*z[29];
    z[32]= - n<T>(59,4) - z[50];
    z[32]=z[1]*z[32];
    z[32]= - n<T>(83,8) + z[32];
    z[32]=z[3]*z[32];
    z[32]=z[32] + static_cast<T>(25)+ n<T>(59,4)*z[1];
    z[32]=z[3]*z[32];
    z[23]=z[23] + z[32] + n<T>(43,8) + z[29];
    z[23]=z[6]*z[23];
    z[29]= - static_cast<T>(3)- z[13];
    z[32]=z[3]*z[58];
    z[29]=n<T>(1,2)*z[29] + z[32];
    z[29]=z[3]*z[29];
    z[23]=z[23] + z[29] + n<T>(43,8) + z[31];
    z[23]=z[6]*z[23];
    z[29]= - static_cast<T>(2)- z[35];
    z[29]=z[29]*z[19];
    z[31]= - n<T>(5,8) - z[1];
    z[31]=z[1]*z[31];
    z[29]=z[29] + static_cast<T>(2)+ z[31];
    z[29]=z[5]*z[29];
    z[31]= - z[1]*z[42];
    z[31]= - n<T>(3,2) + z[31];
    z[29]=n<T>(3,4)*z[31] + z[29];
    z[29]=z[5]*z[29];
    z[16]=z[16] + z[29] - n<T>(7,8) + z[23];
    z[16]=z[8]*z[16];
    z[15]= - n<T>(23,2) + 9*z[15];
    z[15]=z[15]*z[10];
    z[23]=z[14]*z[40];
    z[29]=n<T>(13,2) + z[23];
    z[29]=z[29]*z[13];
    z[29]=static_cast<T>(3)+ z[29];
    z[19]=z[29]*z[19];
    z[15]=z[19] - static_cast<T>(8)+ z[15];
    z[15]=z[5]*z[15];
    z[19]=n<T>(19,4) + z[23];
    z[19]=z[19]*z[13];
    z[15]=z[15] + static_cast<T>(4)+ z[19];
    z[15]=z[5]*z[15];
    z[11]=n<T>(1,2) + z[11];
    z[11]=z[3]*z[11];
    z[11]= - n<T>(1,2)*z[14] + z[11];
    z[11]=z[3]*z[11];
    z[14]=n<T>(11,2) + z[44];
    z[11]=z[16] + z[15] + z[20] + n<T>(1,4)*z[14] + z[11];
    z[11]=z[8]*z[11];
    z[14]= - z[37]*z[13];
    z[15]=z[41]*z[9];
    z[14]=z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]= - n<T>(9,2) + z[36];
    z[15]=z[15]*z[13];
    z[16]= - z[42]*z[13];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[1]*z[16];
    z[16]=n<T>(5,4) + z[16];
    z[9]=z[16]*z[9];
    z[9]=z[14] + z[15] + z[9];
    z[9]=z[3]*z[9];
    z[14]= - static_cast<T>(3)+ z[13];
    z[14]=z[1]*z[14];
    z[14]= - n<T>(9,2) + z[14];
    z[14]=z[14]*z[39];
    z[14]=n<T>(19,2)*z[21] + z[14];
    z[14]=z[14]*z[33];
    z[15]=z[30]*z[52];
    z[15]= - n<T>(7,2)*z[24] + z[15];
    z[15]=z[3]*z[15];
    z[15]=z[15] - n<T>(3,4)*z[55];
    z[15]=z[6]*z[15];
    z[14]=z[15] - 3*z[21] + z[14];
    z[14]=z[6]*z[14];
    z[15]= - z[1]*z[22];
    z[9]=z[14] + z[15] + z[9];
    z[9]=z[7]*z[9];
    z[14]= - static_cast<T>(15)- z[44];
    z[14]=z[14]*z[10];
    z[15]= - static_cast<T>(9)- z[28];
    z[15]=z[15]*z[13];
    z[15]=static_cast<T>(3)+ z[15];
    z[15]=z[3]*z[15];
    z[16]= - static_cast<T>(5)+ z[1];
    z[15]=n<T>(9,4)*z[16] + z[15];
    z[15]=z[3]*z[15];
    z[9]=z[9] + z[14] + z[15];
    z[14]=static_cast<T>(17)+ n<T>(7,2)*z[1];
    z[14]=z[1]*z[14];
    z[14]=n<T>(61,2) + z[14];
    z[14]=z[14]*z[40];
    z[14]=static_cast<T>(71)+ z[14];
    z[14]=z[1]*z[14];
    z[14]=n<T>(81,4) + z[14];
    z[14]=z[3]*z[14];
    z[15]= - static_cast<T>(101)- z[46];
    z[15]=z[1]*z[15];
    z[15]= - static_cast<T>(121)+ z[15];
    z[15]=z[1]*z[15];
    z[15]= - static_cast<T>(47)+ z[15];
    z[14]=n<T>(3,2)*z[15] + z[14];
    z[14]=z[14]*z[33];
    z[13]= - static_cast<T>(2)- z[13];
    z[13]=z[1]*z[13];
    z[13]= - static_cast<T>(3)+ z[13];
    z[13]=z[1]*z[13];
    z[13]= - static_cast<T>(2)+ z[13];
    z[13]=z[1]*z[13];
    z[13]= - n<T>(1,2) + z[13];
    z[13]=z[3]*z[13];
    z[13]=2*z[60] + z[13];
    z[13]=z[3]*z[13];
    z[15]= - z[1]*z[47];
    z[13]=z[13] - n<T>(3,2) + z[15];
    z[13]=z[13]*z[25];
    z[15]=n<T>(181,4) + 30*z[1];
    z[15]=z[1]*z[15];
    z[13]=z[13] + z[14] + n<T>(161,8) + z[15];
    z[13]=z[6]*z[13];
    z[14]= - n<T>(27,4) - z[1];
    z[14]=z[1]*z[14];
    z[14]= - n<T>(53,4) + z[14];
    z[14]=z[14]*z[40];
    z[14]= - n<T>(275,8) + z[14];
    z[14]=z[1]*z[14];
    z[14]= - n<T>(85,8) + z[14];
    z[14]=z[3]*z[14];
    z[15]=static_cast<T>(41)+ n<T>(39,4)*z[1];
    z[15]=z[1]*z[15];
    z[15]=n<T>(119,2) + z[15];
    z[15]=z[1]*z[15];
    z[14]=z[14] + n<T>(105,4) + z[15];
    z[14]=z[3]*z[14];
    z[15]= - n<T>(43,4) - z[27];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(25,4) + z[15];
    z[13]=z[13] + n<T>(1,2)*z[15] + z[14];
    z[13]=z[6]*z[13];
    z[14]= - z[1]*z[26];
    z[14]= - n<T>(141,4) + z[14];
    z[14]=z[1]*z[14];
    z[14]= - static_cast<T>(29)+ z[14];
    z[10]=static_cast<T>(5)+ z[10];
    z[10]=z[1]*z[10];
    z[10]=n<T>(95,8) + z[10];
    z[10]=z[1]*z[10];
    z[10]=n<T>(109,8) + z[10];
    z[10]=z[1]*z[10];
    z[10]=n<T>(25,4) + z[10];
    z[10]=z[3]*z[10];
    z[10]=n<T>(1,2)*z[14] + z[10];
    z[10]=z[3]*z[10];
    z[14]=static_cast<T>(25)+ n<T>(21,2)*z[1];
    z[14]=z[1]*z[14];
    z[14]=n<T>(65,2) + z[14];
    z[10]=z[13] + n<T>(1,4)*z[14] + z[10];
    z[10]=z[6]*z[10];

    r += n<T>(1,2)*z[9] + z[10] + z[11] + z[12] + z[17] + z[18];
 
    return r;
}

template double qqb_2lha_r2327(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2327(const std::array<dd_real,30>&);
#endif
