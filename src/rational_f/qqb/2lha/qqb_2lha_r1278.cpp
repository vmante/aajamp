#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1278(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[6];
    z[4]=k[12];
    z[5]=k[5];
    z[6]=k[11];
    z[7]=n<T>(1,2)*z[2];
    z[8]=z[6] + 5*z[5];
    z[8]=z[8]*z[7];
    z[9]=z[5] + n<T>(1,2);
    z[8]= - 5*z[9] + z[8];
    z[8]=z[8]*z[7];
    z[10]=3*z[5];
    z[11]=n<T>(3,2)*z[5];
    z[12]=z[11] + 1;
    z[13]= - z[2]*z[12];
    z[13]=z[13] + z[10] + n<T>(7,2) + z[1];
    z[13]=z[2]*z[13];
    z[11]=z[11] + z[1] + n<T>(5,2);
    z[13]=z[13] - z[11];
    z[13]=z[4]*z[13];
    z[8]=n<T>(1,2)*z[13] + z[8] + static_cast<T>(1)+ n<T>(5,4)*z[5];
    z[8]=z[4]*z[8];
    z[13]=z[5] + z[6];
    z[14]=z[13]*z[7];
    z[14]=z[14] - z[12];
    z[14]=z[2]*z[14];
    z[15]=n<T>(5,2) + z[10];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[2]*z[14];
    z[15]=static_cast<T>(1)+ n<T>(3,4)*z[5];
    z[13]=z[13]*z[2];
    z[16]= - n<T>(1,4)*z[13] + z[15];
    z[16]=z[2]*z[16];
    z[11]= - n<T>(1,2)*z[11] + z[16];
    z[11]=z[2]*z[11];
    z[16]=n<T>(1,2)*z[5];
    z[17]=z[16] + static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[11]=n<T>(1,2)*z[17] + z[11];
    z[11]=z[4]*z[11];
    z[17]=z[5] + 1;
    z[11]=z[11] - n<T>(1,2)*z[17] + z[14];
    z[11]=z[4]*z[11];
    z[13]=z[10] - z[13];
    z[13]=z[2]*z[13];
    z[10]= - z[10] + z[13];
    z[10]=z[2]*z[10];
    z[10]=z[5] + z[10];
    z[10]=n<T>(1,4)*z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]= - n<T>(1,2)*z[6] - z[5];
    z[11]=z[11]*z[7];
    z[11]=z[5] + z[11];
    z[11]=z[2]*z[11];
    z[8]=z[10] + z[8] - z[16] + z[11];
    z[8]=z[3]*z[8];
    z[7]= - z[12]*z[7];
    z[7]=z[7] + n<T>(1,4)*z[1] + z[15];
    z[7]=z[4]*z[7];
    z[10]=z[2]*z[5];
    z[7]=z[7] + z[10] - z[9];
    z[7]=z[4]*z[7];
    z[9]=z[5] - z[10];
    z[7]=z[8] + n<T>(1,4)*z[9] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - z[4]*z[17];
    z[8]=z[5] + z[8];
    z[8]=z[4]*z[8];
    z[7]=n<T>(1,4)*z[8] + z[7];

    r += n<T>(1,8)*z[7];
 
    return r;
}

template double qqb_2lha_r1278(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1278(const std::array<dd_real,30>&);
#endif
