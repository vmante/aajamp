#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1245(const std::array<T,30>& k) {
  T z[82];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[17];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[6];
    z[7]=k[7];
    z[8]=k[4];
    z[9]=k[15];
    z[10]=k[12];
    z[11]=k[8];
    z[12]=k[11];
    z[13]=k[25];
    z[14]=k[5];
    z[15]=k[9];
    z[16]=k[18];
    z[17]=k[10];
    z[18]=npow(z[3],3);
    z[19]=static_cast<T>(4)- z[3];
    z[19]=z[19]*z[18];
    z[20]=z[2]*npow(z[3],4);
    z[21]=npow(z[7],3);
    z[19]= - 36*z[20] + n<T>(13,3)*z[21] + 24*z[19];
    z[19]=z[2]*z[19];
    z[22]=npow(z[7],2);
    z[23]=n<T>(63,8) - n<T>(13,3)*z[7];
    z[23]=z[23]*z[22];
    z[24]=npow(z[3],2);
    z[25]= - n<T>(199,2) + 48*z[3];
    z[25]=z[25]*z[24];
    z[19]=z[19] + z[23] + z[25];
    z[19]=z[2]*z[19];
    z[23]=n<T>(1,3)*z[7];
    z[25]=14*z[7];
    z[26]=n<T>(25,2) - z[25];
    z[26]=z[26]*z[23];
    z[27]=5*z[3];
    z[28]=static_cast<T>(13)- 7*z[3];
    z[28]=z[28]*z[27];
    z[19]=z[19] + z[26] + z[28];
    z[19]=z[2]*z[19];
    z[26]=z[9] - 1;
    z[28]=npow(z[9],3);
    z[29]=z[28]*z[4];
    z[26]=z[26]*z[29];
    z[30]=3*z[9];
    z[31]=n<T>(27,2)*z[9];
    z[32]=static_cast<T>(49)- z[31];
    z[32]=z[32]*z[30];
    z[32]= - n<T>(209,2) + z[32];
    z[33]=npow(z[9],2);
    z[32]=z[32]*z[33];
    z[34]=npow(z[6],2);
    z[32]= - 27*z[26] + z[32] + n<T>(4,3)*z[34];
    z[32]=z[4]*z[32];
    z[35]=n<T>(3,4)*z[15];
    z[36]=9*z[9];
    z[37]= - static_cast<T>(23)+ z[36];
    z[37]=z[9]*z[37];
    z[37]=static_cast<T>(153)+ 11*z[37];
    z[37]=z[9]*z[37];
    z[38]=n<T>(4,3)*z[6];
    z[39]=n<T>(29,4) - z[38];
    z[39]=z[6]*z[39];
    z[32]=z[32] + z[39] - z[35] + z[37];
    z[32]=z[4]*z[32];
    z[37]=n<T>(1,4)*z[7];
    z[39]=n<T>(1,4)*z[6];
    z[40]= - n<T>(5,2)*z[14] + n<T>(503,3) + n<T>(37,2)*z[10];
    z[41]=static_cast<T>(9)- n<T>(13,6)*z[4];
    z[41]=z[4]*z[41];
    z[41]= - n<T>(41,6) + z[41];
    z[41]=z[5]*z[41];
    z[40]=z[41] - 11*z[4] - z[3] - z[37] - z[39] + n<T>(1,4)*z[40] - 65*z[9]
   ;
    z[40]=z[5]*z[40];
    z[41]=n<T>(1,2)*z[15];
    z[42]=n<T>(1,2)*z[14];
    z[43]=z[41] + z[42];
    z[44]=static_cast<T>(22)+ z[43];
    z[44]=z[3]*z[44];
    z[45]=static_cast<T>(217)- n<T>(177,2)*z[9];
    z[45]=z[9]*z[45];
    z[46]= - n<T>(8,3) + n<T>(1,4)*z[15];
    z[46]=z[6]*z[46];
    z[19]=z[40] + z[32] + z[19] + z[44] - n<T>(3,4)*z[7] + z[46] + z[45] - 
   n<T>(344,3) - z[35];
    z[19]=z[12]*z[19];
    z[32]=static_cast<T>(149)+ 21*z[6];
    z[32]=z[32]*z[39];
    z[40]=n<T>(1,2)*z[3];
    z[44]=static_cast<T>(91)- 33*z[3];
    z[44]=z[44]*z[40];
    z[45]=z[5]*z[4];
    z[46]=13*z[4];
    z[47]= - static_cast<T>(34)+ z[46];
    z[47]=z[47]*z[45];
    z[48]= - static_cast<T>(77)+ 3*z[14];
    z[47]=n<T>(1,3)*z[47] + n<T>(16,3)*z[4] - 30*z[3] + n<T>(1,2)*z[48] - 24*z[6];
    z[47]=z[5]*z[47];
    z[48]=n<T>(7,4)*z[14];
    z[49]=z[48] - static_cast<T>(11)- n<T>(67,4)*z[10];
    z[32]=z[47] + z[44] - z[37] + z[32] + n<T>(1,2)*z[49] + 94*z[9];
    z[32]=z[5]*z[32];
    z[44]= - static_cast<T>(31)+ 15*z[3];
    z[44]=z[44]*z[18];
    z[47]=12*z[20];
    z[44]=z[47] - z[21] + z[44];
    z[49]=4*z[2];
    z[44]=z[44]*z[49];
    z[50]=4*z[7];
    z[51]= - n<T>(19,3) + z[50];
    z[51]=z[51]*z[22];
    z[52]=4*z[3];
    z[53]=9*z[3];
    z[54]= - static_cast<T>(29)+ z[53];
    z[54]=z[54]*z[52];
    z[54]=static_cast<T>(115)+ z[54];
    z[54]=z[54]*z[24];
    z[44]=z[44] + z[51] + z[54];
    z[44]=z[2]*z[44];
    z[51]=static_cast<T>(85)- 89*z[9];
    z[51]=z[51]*z[33];
    z[54]=2*z[6];
    z[55]= - n<T>(79,24) - z[54];
    z[55]=z[55]*z[34];
    z[56]= - static_cast<T>(26)+ n<T>(45,2)*z[9];
    z[56]=z[56]*z[28];
    z[57]=z[4]*npow(z[9],4);
    z[56]=z[56] + n<T>(9,2)*z[57];
    z[58]=3*z[4];
    z[56]=z[56]*z[58];
    z[51]=z[56] + n<T>(3,2)*z[51] + z[55];
    z[51]=z[4]*z[51];
    z[55]=2*z[9];
    z[56]=51*z[9];
    z[59]= - static_cast<T>(55)+ z[56];
    z[59]=z[59]*z[55];
    z[60]=n<T>(1,8)*z[6];
    z[61]=3*z[15];
    z[62]= - static_cast<T>(55)- z[61];
    z[62]=z[62]*z[60];
    z[62]=z[62] - static_cast<T>(20)+ n<T>(13,8)*z[15];
    z[62]=z[6]*z[62];
    z[63]=z[15] + z[14];
    z[64]= - static_cast<T>(127)- z[63];
    z[65]=36*z[3];
    z[66]=n<T>(161,2) - z[65];
    z[66]=z[3]*z[66];
    z[64]=n<T>(1,2)*z[64] + z[66];
    z[64]=z[3]*z[64];
    z[19]=z[19] + z[32] + z[51] + z[44] + z[64] - n<T>(9,4)*z[7] + z[59] + 
    z[62];
    z[19]=z[12]*z[19];
    z[32]=n<T>(1,3)*z[5];
    z[44]= - static_cast<T>(20)+ z[46];
    z[44]=z[44]*z[32];
    z[46]=n<T>(13,3)*z[4];
    z[44]=z[44] - z[46] - 10*z[3] - 8*z[6] - 69*z[9] + z[42] + n<T>(52,3) + 
   n<T>(9,2)*z[10];
    z[44]=z[5]*z[44];
    z[51]=17*z[9];
    z[59]=static_cast<T>(57)- z[51];
    z[59]=z[9]*z[59];
    z[59]= - n<T>(19,3) + z[59];
    z[62]=static_cast<T>(3)+ z[39];
    z[62]=z[6]*z[62];
    z[64]=static_cast<T>(29)- n<T>(11,2)*z[3];
    z[64]=z[3]*z[64];
    z[44]=z[44] + z[64] - n<T>(1,4)*z[22] + 2*z[59] + 7*z[62];
    z[44]=z[5]*z[44];
    z[59]= - static_cast<T>(7)+ z[52];
    z[59]=z[59]*z[18];
    z[62]=n<T>(2,3)*z[21];
    z[59]=9*z[20] - z[62] + 3*z[59];
    z[59]=z[59]*z[49];
    z[64]=12*z[3];
    z[66]= - static_cast<T>(6)+ z[3];
    z[66]=z[66]*z[64];
    z[66]=n<T>(149,2) + z[66];
    z[66]=z[66]*z[24];
    z[59]=z[59] - n<T>(15,4)*z[22] + z[66];
    z[59]=z[2]*z[59];
    z[28]=3*z[28];
    z[66]=18*z[9];
    z[67]= - n<T>(49,2) + z[66];
    z[67]=z[67]*z[28];
    z[68]=n<T>(27,2)*z[57];
    z[69]=npow(z[6],3);
    z[67]=z[68] + z[67] - n<T>(2,3)*z[69];
    z[67]=z[4]*z[67];
    z[70]= - static_cast<T>(53)+ z[31];
    z[70]=z[70]*z[30];
    z[70]=n<T>(257,2) + z[70];
    z[70]=z[70]*z[33];
    z[67]=z[67] + z[70] - n<T>(45,8)*z[34];
    z[67]=z[4]*z[67];
    z[70]=n<T>(1,2)*z[6];
    z[71]= - static_cast<T>(13)- z[15];
    z[71]=z[71]*z[70];
    z[71]=z[71] - n<T>(443,6) + z[61];
    z[71]=z[71]*z[39];
    z[72]= - n<T>(13,3) + z[7];
    z[37]=z[72]*z[37];
    z[72]=3*z[3];
    z[73]=static_cast<T>(11)- z[72];
    z[73]=z[73]*z[52];
    z[73]= - n<T>(91,2) + z[73];
    z[73]=z[3]*z[73];
    z[74]=n<T>(117,2)*z[9];
    z[75]=static_cast<T>(161)- z[74];
    z[75]=z[9]*z[75];
    z[75]= - n<T>(227,2) + z[75];
    z[75]=z[9]*z[75];
    z[37]=z[44] + z[67] + z[59] + z[73] + z[37] + z[71] - n<T>(3,8)*z[15] + 
    z[75];
    z[37]=z[12]*z[37];
    z[44]= - n<T>(26,3)*z[45] + z[46] - z[65] - 44*z[6] - n<T>(8,3) + z[14];
    z[44]=z[5]*z[44];
    z[46]=2*z[3];
    z[59]=static_cast<T>(5)- z[46];
    z[59]=z[59]*z[53];
    z[65]=5*z[6];
    z[67]=static_cast<T>(31)+ z[65];
    z[67]=z[6]*z[67];
    z[67]=z[67] + 34*z[9] - z[14] + n<T>(1,3) - 5*z[10];
    z[44]=z[44] + 2*z[67] + z[59];
    z[44]=z[5]*z[44];
    z[59]=n<T>(1,3)*z[6];
    z[67]= - static_cast<T>(35)- 16*z[6];
    z[67]=z[67]*z[59];
    z[67]= - static_cast<T>(23)+ z[67];
    z[67]=z[6]*z[67];
    z[71]=n<T>(5,3)*z[22];
    z[73]= - static_cast<T>(44)+ 31*z[9];
    z[73]=z[9]*z[73];
    z[75]=z[3] - 1;
    z[76]=z[3]*z[75];
    z[44]=z[44] + 18*z[76] - z[71] + z[73] + z[67];
    z[44]=z[5]*z[44];
    z[67]=n<T>(1,3)*z[21];
    z[73]= - z[67] + 6*z[20];
    z[76]=2*z[18];
    z[77]=static_cast<T>(7)- 6*z[3];
    z[77]=z[77]*z[76];
    z[77]=z[77] - z[73];
    z[77]=z[77]*z[49];
    z[66]=static_cast<T>(19)- z[66];
    z[66]=z[66]*z[28];
    z[69]=n<T>(4,3)*z[69];
    z[57]= - 27*z[57] + z[66] + z[69];
    z[57]=z[4]*z[57];
    z[66]=6*z[9];
    z[78]= - static_cast<T>(7)+ z[66];
    z[78]=z[78]*z[33];
    z[79]=static_cast<T>(5)- z[72];
    z[79]=z[3]*z[79];
    z[79]= - static_cast<T>(41)+ 16*z[79];
    z[79]=z[79]*z[24];
    z[61]=n<T>(71,3) - z[61];
    z[61]=n<T>(1,4)*z[61] + n<T>(16,3)*z[6];
    z[61]=z[6]*z[61];
    z[61]=n<T>(9,8)*z[15] + z[61];
    z[61]=z[6]*z[61];
    z[37]=z[37] + z[44] + z[57] + z[77] + z[79] + z[71] + 7*z[78] + 
    z[61];
    z[37]=z[12]*z[37];
    z[44]=z[9] - 4;
    z[57]=12*z[9];
    z[61]= - z[44]*z[57];
    z[71]=static_cast<T>(2)- z[40];
    z[71]=z[71]*z[53];
    z[77]=z[53] + 11*z[6];
    z[78]=n<T>(1,4)*z[14];
    z[79]= - n<T>(13,6)*z[5] - 24*z[9] + z[78] + n<T>(13,3) + n<T>(7,4)*z[10] - 
    z[77];
    z[79]=z[5]*z[79];
    z[80]=n<T>(5,2)*z[6];
    z[81]=static_cast<T>(22)+ z[80];
    z[81]=z[6]*z[81];
    z[61]=z[79] + z[71] + z[81] - n<T>(13,6) + z[61];
    z[61]=z[5]*z[61];
    z[71]=z[75]*z[53];
    z[44]= - z[9]*z[44];
    z[44]= - static_cast<T>(4)+ z[44];
    z[44]=z[44]*z[66];
    z[66]= - static_cast<T>(5)- z[38];
    z[66]=z[6]*z[66];
    z[66]= - static_cast<T>(11)+ z[66];
    z[66]=z[6]*z[66];
    z[44]=z[61] + z[71] - z[62] + z[44] + z[66];
    z[44]=z[5]*z[44];
    z[61]=static_cast<T>(13)- n<T>(9,2)*z[9];
    z[61]=z[9]*z[61];
    z[61]= - n<T>(21,2) + z[61];
    z[61]=z[61]*z[33];
    z[62]=static_cast<T>(2)- z[3];
    z[62]=z[62]*z[52];
    z[62]= - n<T>(11,2) + z[62];
    z[62]=z[62]*z[24];
    z[61]=z[61] + z[62];
    z[62]=z[75]*z[18];
    z[62]= - 12*z[62] - z[73];
    z[66]=2*z[2];
    z[62]=z[62]*z[66];
    z[36]=static_cast<T>(11)- z[36];
    z[28]=z[36]*z[28];
    z[28]= - z[68] + z[28] + z[69];
    z[28]=z[4]*z[28];
    z[36]=n<T>(95,6) - z[15];
    z[36]=n<T>(1,4)*z[36] + z[38];
    z[36]=z[6]*z[36];
    z[36]=z[41] + z[36];
    z[36]=z[6]*z[36];
    z[68]=static_cast<T>(1)+ z[7];
    z[68]=z[68]*z[22];
    z[28]=z[44] + z[28] + z[62] + n<T>(2,3)*z[68] + z[36] + 3*z[61];
    z[28]=z[12]*z[28];
    z[36]= - z[34]*z[41];
    z[44]=npow(z[5],3);
    z[61]= - 7*z[10] - z[14];
    z[61]=z[61]*z[44];
    z[36]=z[36] + z[61];
    z[28]=n<T>(3,4)*z[36] + z[28];
    z[28]=z[12]*z[28];
    z[36]=n<T>(1,2)*z[1];
    z[61]=static_cast<T>(1)- z[36];
    z[44]=z[44]*z[10]*z[61];
    z[34]=z[34]*z[15];
    z[61]=n<T>(1,8)*z[34];
    z[62]= - z[8]*npow(z[12],2)*z[61];
    z[28]=z[62] + n<T>(7,2)*z[44] + z[28];
    z[28]=z[8]*z[28];
    z[44]=z[1]*z[10];
    z[62]= - n<T>(7,2)*z[1] + n<T>(3,2)*z[14];
    z[68]= - static_cast<T>(7)- z[62];
    z[68]=z[5]*z[68];
    z[68]=n<T>(1,2)*z[68] + n<T>(11,2)*z[10] - z[44];
    z[68]=z[68]*npow(z[5],2);
    z[28]=z[28] + z[37] - n<T>(1,4)*z[34] + z[68];
    z[28]=z[8]*z[28];
    z[34]= - n<T>(403,3) + z[62];
    z[37]=static_cast<T>(27)- z[4];
    z[37]=z[4]*z[37];
    z[34]=n<T>(1,2)*z[34] + z[37];
    z[34]=z[4]*z[34];
    z[37]=n<T>(1,3)*z[4];
    z[62]=n<T>(1,2)*z[4];
    z[68]=static_cast<T>(2)- z[62];
    z[68]=z[68]*z[37];
    z[68]= - static_cast<T>(1)+ z[68];
    z[69]=13*z[5];
    z[68]=z[68]*npow(z[4],2)*z[69];
    z[75]=z[77] - 20;
    z[34]=z[68] - 3*z[75] + z[34];
    z[34]=z[5]*z[34];
    z[68]=static_cast<T>(8)+ n<T>(3,2)*z[6];
    z[68]=z[68]*z[65];
    z[40]=static_cast<T>(1)- z[40];
    z[40]=z[3]*z[40];
    z[77]=static_cast<T>(5)- z[62];
    z[77]=z[4]*z[77];
    z[34]=z[34] + z[77] + 27*z[40] + z[68] - n<T>(3,4)*z[1] + n<T>(13,3) - n<T>(5,4)
   *z[14];
    z[34]=z[5]*z[34];
    z[40]=4*z[6];
    z[68]= - n<T>(5,3) - z[6];
    z[68]=z[6]*z[68];
    z[68]= - static_cast<T>(3)+ z[68];
    z[68]=z[68]*z[40];
    z[77]=2*z[7];
    z[34]=z[34] + z[71] - z[77] + z[68] - n<T>(7,8)*z[44] - 28*z[9] + static_cast<T>(2)+ 15.
   /4.*z[10];
    z[34]=z[5]*z[34];
    z[44]=static_cast<T>(4)- z[72];
    z[44]=z[44]*z[18];
    z[44]=4*z[44] - z[73];
    z[44]=z[44]*z[66];
    z[57]= - static_cast<T>(5)+ z[57];
    z[57]=z[57]*z[33];
    z[68]=static_cast<T>(1)- z[30];
    z[29]=z[68]*z[29];
    z[29]=9*z[29] + z[57] - z[61];
    z[29]=z[4]*z[29];
    z[57]=n<T>(11,3) - z[35];
    z[57]=n<T>(1,2)*z[57] + z[40];
    z[57]=z[6]*z[57];
    z[57]=z[41] + z[57];
    z[57]=z[6]*z[57];
    z[61]=n<T>(2,3)*z[7];
    z[68]=z[61] - 1;
    z[71]= - z[68]*z[22];
    z[73]=static_cast<T>(14)- z[53];
    z[73]=z[73]*z[52];
    z[73]= - n<T>(49,2) + z[73];
    z[73]=z[73]*z[24];
    z[79]=n<T>(5,4)*z[17];
    z[81]= - static_cast<T>(1)- z[79];
    z[81]=z[17]*z[81];
    z[31]= - static_cast<T>(1)- z[31];
    z[31]=z[9]*z[31];
    z[19]=z[28] + z[19] + z[34] + z[29] + z[44] + z[73] + z[71] + z[57]
    + z[81] + z[31];
    z[19]=z[8]*z[19];
    z[28]=z[33]*z[4];
    z[29]= - static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[31]=z[9]*z[29];
    z[31]=n<T>(1,2) + z[31];
    z[31]=z[31]*z[28];
    z[34]=n<T>(2,3)*z[6];
    z[44]=z[34] + z[16];
    z[57]=n<T>(93,2)*z[9];
    z[71]=static_cast<T>(89)- z[57];
    z[71]=z[9]*z[71];
    z[71]= - n<T>(89,2) + z[71];
    z[71]=z[9]*z[71];
    z[31]=27*z[31] + z[71] - z[35] - z[44];
    z[31]=z[4]*z[31];
    z[71]=2*z[16];
    z[73]=n<T>(5,2)*z[17];
    z[74]= - static_cast<T>(115)+ z[74];
    z[74]=z[9]*z[74];
    z[31]=z[31] + z[38] + z[74] - z[73] + n<T>(201,4) - z[71];
    z[31]=z[4]*z[31];
    z[38]=z[47] - n<T>(11,3)*z[21] - 36*z[18];
    z[38]=z[2]*z[38];
    z[74]= - n<T>(65,8) + n<T>(22,3)*z[7];
    z[74]=z[74]*z[22];
    z[38]=z[38] + z[74] + n<T>(83,2)*z[24];
    z[38]=z[2]*z[38];
    z[74]=n<T>(259,8) - 11*z[7];
    z[74]=z[7]*z[74];
    z[74]= - n<T>(149,8) + z[74];
    z[74]=z[74]*z[23];
    z[38]=z[38] + z[74] - n<T>(57,2)*z[3];
    z[38]=z[2]*z[38];
    z[68]= - z[68]*z[50];
    z[38]=z[38] + n<T>(469,12) + z[68];
    z[38]=z[2]*z[38];
    z[68]=static_cast<T>(2)- z[4];
    z[68]=z[4]*z[68];
    z[68]= - n<T>(25,4) + 7*z[68];
    z[32]=z[68]*z[32];
    z[31]=z[32] + z[31] + z[38] - z[23] - z[57] + n<T>(15,8)*z[10] + n<T>(142,3)
    - z[44];
    z[31]=z[12]*z[31];
    z[32]=static_cast<T>(17)- z[72];
    z[32]=z[32]*z[76];
    z[32]= - z[47] + n<T>(7,3)*z[21] + z[32];
    z[32]=z[32]*z[66];
    z[38]=n<T>(107,4) - 28*z[7];
    z[38]=z[38]*z[22];
    z[44]= - static_cast<T>(37)+ z[64];
    z[24]=z[44]*z[24];
    z[24]=z[32] + n<T>(1,3)*z[38] + 2*z[24];
    z[24]=z[2]*z[24];
    z[25]= - n<T>(107,4) + z[25];
    z[25]=z[25]*z[23];
    z[25]=n<T>(23,4) + z[25];
    z[25]=z[7]*z[25];
    z[27]=static_cast<T>(13)- z[27];
    z[27]=z[3]*z[27];
    z[24]=z[24] + z[25] + n<T>(7,2)*z[27];
    z[24]=z[2]*z[24];
    z[25]= - static_cast<T>(49)+ z[56];
    z[25]=z[25]*z[33];
    z[25]=z[25] - n<T>(27,2)*z[26];
    z[25]=z[4]*z[25];
    z[26]=n<T>(47,8) + z[54];
    z[26]=z[26]*z[59];
    z[27]= - static_cast<T>(2)- z[73];
    z[27]=z[17]*z[27];
    z[32]=static_cast<T>(45)- 43*z[9];
    z[32]=z[9]*z[32];
    z[25]=z[25] + z[26] + n<T>(3,2)*z[32] + z[16] + z[27];
    z[25]=z[4]*z[25];
    z[26]= - static_cast<T>(1)+ z[4];
    z[26]=z[26]*z[45];
    z[27]=5*z[14];
    z[32]= - n<T>(139,3) - z[27];
    z[32]=n<T>(1,2)*z[32] - z[6];
    z[26]=n<T>(7,3)*z[26] - z[37] + n<T>(1,2)*z[32] - z[46];
    z[26]=z[5]*z[26];
    z[32]= - z[34] + n<T>(1,3) + z[41];
    z[32]=z[6]*z[32];
    z[38]=n<T>(3,2)*z[15];
    z[44]=static_cast<T>(11)+ z[63];
    z[44]=z[3]*z[44];
    z[24]=z[31] + z[26] + z[25] + z[24] + z[44] - n<T>(3,2)*z[7] + z[32] + 
   58*z[9] - z[38] + z[78] - n<T>(29,8)*z[10] - n<T>(595,12) + z[16];
    z[24]=z[12]*z[24];
    z[25]= - static_cast<T>(1)- z[9];
    z[25]=z[25]*z[30];
    z[26]=9*z[1];
    z[31]= - z[33]*z[26];
    z[32]=z[6]*z[15];
    z[33]=z[41] - z[32];
    z[33]=z[33]*z[39];
    z[28]=z[29]*z[28];
    z[25]= - 9*z[28] + z[33] + z[31] - z[42] + z[25];
    z[25]=z[4]*z[25];
    z[28]= - static_cast<T>(61)+ z[42];
    z[28]=3*z[28] - n<T>(595,6)*z[1];
    z[29]= - n<T>(19,3) + z[78];
    z[29]=z[4]*z[29];
    z[29]=z[29] + n<T>(19,3)*z[1] + static_cast<T>(58)- z[14];
    z[29]=z[4]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[28]=z[4]*z[28];
    z[28]=40*z[1] + z[28];
    z[28]=z[4]*z[28];
    z[29]=n<T>(1,3)*z[1];
    z[31]= - z[37] + static_cast<T>(1)+ z[29];
    z[31]=z[4]*z[31];
    z[33]=z[1] + 1;
    z[31]= - n<T>(2,3)*z[33] + z[31];
    z[31]=z[31]*npow(z[4],3)*z[69];
    z[37]= - z[1]*z[75];
    z[28]=z[31] + 2*z[37] + z[28];
    z[28]=z[5]*z[28];
    z[31]= - z[1]*z[53];
    z[31]=z[31] - static_cast<T>(11)+ z[26];
    z[31]=z[3]*z[31];
    z[37]=3*z[1];
    z[41]= - z[58] + n<T>(101,2) + z[37];
    z[41]=z[4]*z[41];
    z[44]= - 93*z[1] - n<T>(373,3) + z[14];
    z[41]=n<T>(1,2)*z[44] + z[41];
    z[41]=z[4]*z[41];
    z[44]=z[65] + 18;
    z[44]=z[1]*z[44];
    z[44]= - static_cast<T>(11)+ z[44];
    z[44]=z[6]*z[44];
    z[28]=z[28] + z[41] + z[31] + z[44] + static_cast<T>(22)- z[1];
    z[28]=z[5]*z[28];
    z[31]=n<T>(1,2) - z[1];
    z[41]=8*z[1];
    z[44]= - z[6]*z[41];
    z[31]=5*z[31] + z[44];
    z[31]=z[31]*z[59];
    z[31]=z[31] + n<T>(175,12) - z[1];
    z[31]=z[6]*z[31];
    z[44]=static_cast<T>(17)+ n<T>(7,2)*z[14];
    z[47]=n<T>(7,8)*z[14];
    z[53]= - static_cast<T>(1)- z[47];
    z[53]=z[4]*z[53];
    z[44]=z[53] + n<T>(1,2)*z[44] + z[1];
    z[44]=z[4]*z[44];
    z[27]= - n<T>(51,2)*z[1] - n<T>(133,3) + z[27];
    z[46]=n<T>(15,2) - z[46];
    z[46]=z[3]*z[46];
    z[27]=z[28] + z[44] + z[46] + n<T>(1,4)*z[27] + z[31];
    z[27]=z[5]*z[27];
    z[28]= - static_cast<T>(8)+ z[72];
    z[18]=z[28]*z[18];
    z[18]=3*z[20] - z[67] + z[18];
    z[18]=z[18]*z[49];
    z[20]= - n<T>(9,4) + n<T>(8,3)*z[7];
    z[20]=z[20]*z[22];
    z[28]=2*z[1];
    z[31]=z[28] - 3;
    z[44]=z[31]*z[52];
    z[44]=z[44] + n<T>(65,2) - z[41];
    z[44]=z[3]*z[44];
    z[44]= - static_cast<T>(8)+ z[44];
    z[44]=z[3]*z[44];
    z[18]=z[18] + z[20] + z[44];
    z[18]=z[2]*z[18];
    z[20]=n<T>(1,2)*z[17];
    z[44]= - static_cast<T>(3)- z[20];
    z[44]=z[17]*z[44];
    z[44]= - z[51] + z[44] + static_cast<T>(3)+ n<T>(7,2)*z[10] + z[63];
    z[46]=z[72]*z[1];
    z[49]=4*z[1];
    z[51]= - z[46] - static_cast<T>(3)+ z[49];
    z[51]=z[3]*z[51];
    z[41]=8*z[51] + n<T>(73,2) - z[41];
    z[41]=z[3]*z[41];
    z[41]=z[41] - static_cast<T>(18)- z[43];
    z[41]=z[3]*z[41];
    z[51]= - n<T>(53,6) + z[15];
    z[53]= - n<T>(5,6) - z[54];
    z[53]=z[6]*z[53];
    z[51]=n<T>(1,2)*z[51] + z[53];
    z[51]=z[6]*z[51];
    z[34]=z[34] - 1;
    z[34]=z[6]*z[34];
    z[34]=z[34] + 1;
    z[53]=z[28] + 1;
    z[34]=z[6]*z[53]*z[34];
    z[54]=n<T>(9,4) - n<T>(4,3)*z[7];
    z[54]=z[7]*z[54];
    z[34]=z[54] + z[34] - n<T>(7,2) - z[1];
    z[34]=z[7]*z[34];
    z[54]= - z[10] + 5*z[9];
    z[54]=z[1]*z[54];
    z[18]=z[19] + z[24] + z[27] + z[25] + z[18] + z[41] + z[34] + z[51]
    + n<T>(1,2)*z[44] + z[54];
    z[18]=z[8]*z[18];
    z[19]=n<T>(5,8)*z[11];
    z[20]=z[19] - z[20] + n<T>(3,4) + z[13];
    z[20]=z[17]*z[20];
    z[24]= - n<T>(9,2) - z[17];
    z[24]=z[17]*z[24];
    z[24]= - static_cast<T>(3)+ z[24];
    z[24]=z[24]*z[35];
    z[25]=npow(z[11],2);
    z[27]=z[25]*z[2];
    z[34]=n<T>(1,2)*z[13];
    z[35]=z[34] + n<T>(2,3)*z[27];
    z[41]=n<T>(2,3)*z[11];
    z[44]= - n<T>(23,8) + z[41];
    z[44]=z[11]*z[44];
    z[44]=z[44] - z[35];
    z[44]=z[2]*z[44];
    z[51]= - z[10] + z[55] - z[11];
    z[51]=z[51]*z[9];
    z[54]= - z[11] - z[51];
    z[54]=z[4]*z[54];
    z[20]=z[54] + z[44] - z[30] + z[24] + n<T>(23,8)*z[10] - z[34] + n<T>(31,24)
    - 3*z[16] + z[20];
    z[20]=z[4]*z[20];
    z[24]= - z[79] - z[34] - n<T>(7,2) + z[16];
    z[24]=z[17]*z[24];
    z[30]=z[17] + 5;
    z[44]=n<T>(1,4)*z[17];
    z[54]=z[30]*z[44];
    z[54]=static_cast<T>(1)+ z[54];
    z[38]=z[54]*z[38];
    z[54]=n<T>(1,3)*z[2];
    z[27]=z[11] + z[27];
    z[27]=z[27]*z[54];
    z[55]= - n<T>(1,3) + z[16];
    z[30]=z[17]*z[30];
    z[30]=static_cast<T>(1)- n<T>(5,8)*z[30];
    z[30]=z[11]*z[30];
    z[24]=z[27] + z[51] + z[30] + z[38] + 2*z[55] + z[24];
    z[24]=z[4]*z[24];
    z[27]=n<T>(1,3)*z[11];
    z[30]=2*z[11];
    z[38]=n<T>(41,8) - z[30];
    z[38]=z[38]*z[27];
    z[35]=z[38] + z[35];
    z[35]=z[2]*z[35];
    z[35]=z[35] + z[41] - n<T>(8,3) + z[13];
    z[35]=z[2]*z[35];
    z[38]=3*z[17];
    z[51]= - n<T>(5,2) - z[17];
    z[51]=z[11]*z[51];
    z[24]=z[24] + z[35] - z[9] + n<T>(5,4)*z[51] - z[38] - n<T>(7,8)*z[10] + n<T>(11,8) + z[71];
    z[24]=z[4]*z[24];
    z[35]=n<T>(103,3) + 23*z[11];
    z[51]= - static_cast<T>(5)- z[30];
    z[51]=z[7]*z[51];
    z[35]=n<T>(1,8)*z[35] + z[51];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(9,8)*z[11] + z[35];
    z[35]=z[7]*z[35];
    z[51]=4*z[11];
    z[55]=static_cast<T>(5)+ z[51];
    z[55]=z[7]*z[55];
    z[55]= - n<T>(23,8)*z[11] + z[55];
    z[55]=z[55]*z[22];
    z[21]=z[21]*z[2];
    z[56]= - z[11]*z[21];
    z[55]=z[55] + z[56];
    z[55]=z[55]*z[54];
    z[56]=n<T>(1,3)*z[25];
    z[35]=z[55] + z[56] + z[35];
    z[35]=z[2]*z[35];
    z[55]= - static_cast<T>(37)- n<T>(23,2)*z[11];
    z[57]=n<T>(4,3)*z[11];
    z[58]=static_cast<T>(5)+ z[57];
    z[58]=z[7]*z[58];
    z[55]=n<T>(1,4)*z[55] + z[58];
    z[55]=z[7]*z[55];
    z[58]=static_cast<T>(17)+ 9*z[11];
    z[55]=n<T>(1,4)*z[58] + z[55];
    z[55]=z[7]*z[55];
    z[41]=n<T>(7,8) - z[41];
    z[41]=z[11]*z[41];
    z[35]=z[35] + z[55] + z[34] + z[41];
    z[35]=z[2]*z[35];
    z[41]= - static_cast<T>(5)- z[11];
    z[41]=z[41]*z[23];
    z[55]=static_cast<T>(45)+ n<T>(23,3)*z[11];
    z[41]=n<T>(1,8)*z[55] + z[41];
    z[41]=z[7]*z[41];
    z[55]= - static_cast<T>(11)- n<T>(9,4)*z[11];
    z[41]=n<T>(1,2)*z[55] + z[41];
    z[41]=z[7]*z[41];
    z[55]= - n<T>(27,8) + z[27];
    z[55]=z[11]*z[55];
    z[35]=z[35] + z[41] - n<T>(7,8) + z[55];
    z[35]=z[2]*z[35];
    z[41]=static_cast<T>(37)+ 25*z[11];
    z[55]=n<T>(5,4) - z[61];
    z[55]=z[7]*z[55];
    z[35]=z[35] + n<T>(1,8)*z[41] + z[55];
    z[35]=z[2]*z[35];
    z[41]=5*z[11];
    z[55]= - static_cast<T>(9)- z[41];
    z[24]=z[24] + n<T>(1,8)*z[55] + z[35];
    z[24]=z[12]*z[24];
    z[35]=z[11] + 1;
    z[55]=8*z[7];
    z[58]= - z[35]*z[55];
    z[61]=n<T>(19,4)*z[11];
    z[58]=z[61] + z[58];
    z[58]=z[58]*z[22];
    z[64]=z[30]*z[21];
    z[58]=z[58] + z[64];
    z[54]=z[58]*z[54];
    z[58]=z[11] + 2;
    z[50]=z[58]*z[50];
    z[64]=n<T>(19,2)*z[11];
    z[65]= - n<T>(35,3) - z[64];
    z[50]=n<T>(1,2)*z[65] + z[50];
    z[50]=z[7]*z[50];
    z[50]=n<T>(13,8)*z[11] + z[50];
    z[50]=z[7]*z[50];
    z[25]=z[54] + z[50] - n<T>(2,3)*z[25];
    z[25]=z[2]*z[25];
    z[50]= - n<T>(29,4) + z[51];
    z[50]=z[50]*z[27];
    z[51]=z[27] + 1;
    z[54]= - z[51]*z[55];
    z[54]=z[54] + n<T>(35,3) + z[61];
    z[54]=z[7]*z[54];
    z[55]=13*z[11];
    z[61]= - n<T>(41,2) - z[55];
    z[54]=n<T>(1,4)*z[61] + z[54];
    z[54]=z[7]*z[54];
    z[25]=z[25] + z[54] - z[13] + z[50];
    z[25]=z[2]*z[25];
    z[50]=static_cast<T>(4)+ z[11];
    z[50]=z[50]*z[77];
    z[54]= - static_cast<T>(35)- z[64];
    z[50]=n<T>(1,2)*z[54] + z[50];
    z[50]=z[50]*z[23];
    z[54]=n<T>(131,3) + z[55];
    z[50]=n<T>(1,8)*z[54] + z[50];
    z[50]=z[7]*z[50];
    z[54]=n<T>(59,4) - z[30];
    z[54]=z[11]*z[54];
    z[54]=static_cast<T>(7)+ z[54];
    z[25]=z[25] + n<T>(1,3)*z[54] + z[50];
    z[25]=z[2]*z[25];
    z[41]= - n<T>(29,3) - z[41];
    z[20]=z[24] + n<T>(1,4)*z[45] + z[20] + z[25] + n<T>(1,2)*z[41] - z[23];
    z[20]=z[12]*z[20];
    z[23]= - static_cast<T>(4)+ z[1];
    z[23]=z[23]*z[28];
    z[24]=static_cast<T>(1)- z[1];
    z[24]=z[24]*z[46];
    z[23]=z[24] + static_cast<T>(3)+ z[23];
    z[23]=z[3]*z[23];
    z[23]=z[23] - static_cast<T>(5)+ z[49];
    z[23]=z[3]*z[23];
    z[23]=static_cast<T>(2)+ z[23];
    z[23]=z[23]*z[52];
    z[24]=static_cast<T>(1)+ z[57];
    z[24]=z[7]*z[24];
    z[19]= - z[19] + z[24];
    z[19]=z[19]*z[22];
    z[21]= - z[27]*z[21];
    z[19]=z[19] + z[21];
    z[19]=z[2]*z[19];
    z[21]= - static_cast<T>(3)- z[30];
    z[21]=z[7]*z[21];
    z[21]=n<T>(15,8)*z[35] + z[21];
    z[21]=z[7]*z[21];
    z[22]=n<T>(1,2)*z[11];
    z[21]= - z[22] + z[21];
    z[21]=z[7]*z[21];
    z[19]=z[19] + z[23] + z[56] + z[21];
    z[19]=z[2]*z[19];
    z[21]= - static_cast<T>(1)- z[22];
    z[23]=static_cast<T>(3)+ z[57];
    z[23]=z[7]*z[23];
    z[21]=n<T>(15,4)*z[21] + z[23];
    z[21]=z[7]*z[21];
    z[21]=z[21] + z[36] + z[58];
    z[21]=z[7]*z[21];
    z[23]= - static_cast<T>(1)+ z[28];
    z[23]=z[23]*z[46];
    z[24]=static_cast<T>(11)- z[49];
    z[24]=z[1]*z[24];
    z[23]=z[23] - static_cast<T>(3)+ z[24];
    z[23]=z[23]*z[52];
    z[23]=z[23] + static_cast<T>(19)- 23*z[1];
    z[23]=z[3]*z[23];
    z[23]=z[23] - z[1] - static_cast<T>(5)- z[43];
    z[23]=z[3]*z[23];
    z[24]=n<T>(37,8) - z[30];
    z[24]=z[24]*z[27];
    z[19]=z[19] + z[23] + z[21] + z[24] + z[42] - static_cast<T>(2)+ z[34];
    z[19]=z[2]*z[19];
    z[21]= - n<T>(581,4) - 16*z[1];
    z[21]=z[21]*z[29];
    z[23]=n<T>(169,3) - z[14];
    z[24]= - n<T>(16,3) + z[78];
    z[24]=z[4]*z[24];
    z[23]=z[24] + n<T>(1,2)*z[23] + n<T>(32,3)*z[1];
    z[23]=z[4]*z[23];
    z[24]= - n<T>(137,3) + z[42];
    z[21]=z[23] + n<T>(1,2)*z[24] + z[21];
    z[21]=z[4]*z[21];
    z[23]=npow(z[1],2);
    z[24]=20*z[23];
    z[21]=z[24] + z[21];
    z[21]=z[4]*z[21];
    z[21]=z[24] + z[21];
    z[21]=z[4]*z[21];
    z[24]= - z[23]*z[75];
    z[25]= - z[62] + z[33];
    z[25]=z[4]*z[25];
    z[30]= - static_cast<T>(1)- z[36];
    z[30]=z[1]*z[30];
    z[25]=z[25] - n<T>(1,2) + z[30];
    z[25]=z[5]*z[25]*npow(z[4],4);
    z[21]=n<T>(13,3)*z[25] + z[21] + z[24];
    z[21]=z[5]*z[21];
    z[24]= - n<T>(1009,4) - 14*z[1];
    z[24]=z[24]*z[29];
    z[25]=n<T>(269,3) - z[14];
    z[29]= - n<T>(14,3) + z[78];
    z[29]=z[4]*z[29];
    z[25]=z[29] + n<T>(1,2)*z[25] + n<T>(28,3)*z[1];
    z[25]=z[4]*z[25];
    z[24]=z[25] + z[24] - static_cast<T>(31)+ z[78];
    z[24]=z[4]*z[24];
    z[25]=static_cast<T>(4)+ 39*z[1];
    z[25]=z[1]*z[25];
    z[24]=z[25] + z[24];
    z[24]=z[4]*z[24];
    z[25]=static_cast<T>(2)+ 13*z[1];
    z[25]=z[25]*z[28];
    z[29]=z[23]*z[80];
    z[30]= - static_cast<T>(3)- z[49];
    z[30]=z[1]*z[30];
    z[29]=z[30] + z[29];
    z[29]=z[6]*z[29];
    z[30]= - static_cast<T>(1)- z[26];
    z[30]=z[1]*z[30];
    z[33]=z[3]*z[23];
    z[30]=z[30] - n<T>(9,2)*z[33];
    z[30]=z[3]*z[30];
    z[21]=z[21] + z[24] + z[30] + z[25] + z[29];
    z[21]=z[5]*z[21];
    z[24]= - z[23]*z[40];
    z[25]= - n<T>(11,4) + 10*z[1];
    z[25]=z[1]*z[25];
    z[24]=z[25] + z[24];
    z[24]=z[24]*z[59];
    z[25]=n<T>(23,12) - z[49];
    z[25]=z[1]*z[25];
    z[24]=z[24] + n<T>(1,6) + z[25];
    z[24]=z[6]*z[24];
    z[25]=static_cast<T>(7)- 25*z[1];
    z[26]=n<T>(7,2) - z[26];
    z[26]=z[3]*z[1]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[26];
    z[25]=z[3]*z[25];
    z[26]=static_cast<T>(13)+ z[28];
    z[29]= - static_cast<T>(2)- z[47];
    z[29]=z[4]*z[29];
    z[26]=2*z[26] + z[29];
    z[26]=z[4]*z[26];
    z[29]= - n<T>(369,8) - z[28];
    z[29]=z[1]*z[29];
    z[26]=z[26] - n<T>(61,4) + z[29];
    z[26]=z[4]*z[26];
    z[29]=static_cast<T>(11)+ 21*z[1];
    z[29]=z[1]*z[29];
    z[21]=z[21] + z[26] + z[25] + z[24] - n<T>(11,3) + z[29];
    z[21]=z[5]*z[21];
    z[24]=z[53]*z[28];
    z[25]= - z[6]*z[24];
    z[26]=z[1]*z[53];
    z[25]=5*z[26] + z[25];
    z[25]=z[25]*z[59];
    z[24]= - z[24] + z[25];
    z[24]=z[6]*z[24];
    z[25]=static_cast<T>(3)+ z[11];
    z[26]= - z[7]*z[51];
    z[25]=n<T>(5,8)*z[25] + z[26];
    z[25]=z[7]*z[25];
    z[26]=n<T>(1,2) + z[28];
    z[26]=z[1]*z[26];
    z[22]=z[25] + z[24] + z[26] - static_cast<T>(2)- z[22];
    z[22]=z[7]*z[22];
    z[24]=z[2] - 1;
    z[24]=z[42]*z[24];
    z[25]=z[15] - z[32];
    z[25]=z[25]*z[60];
    z[26]= - static_cast<T>(1)- z[48];
    z[26]=z[26]*z[62];
    z[28]=static_cast<T>(3)- z[10];
    z[24]=z[26] + z[25] + z[1] + 4*z[9] + z[11] + 2*z[28] + z[24];
    z[24]=z[4]*z[24];
    z[25]=static_cast<T>(1)+ z[44];
    z[25]=z[25]*z[38];
    z[25]=n<T>(5,4) + z[25];
    z[25]=z[15]*z[25];
    z[26]= - n<T>(3,4)*z[17] - n<T>(5,2) - z[13];
    z[26]=z[17]*z[26];
    z[25]=z[25] - z[14] + z[26] - n<T>(37,4) + z[13];
    z[26]= - n<T>(37,8) + z[11];
    z[26]=z[26]*z[27];
    z[27]= - n<T>(57,4) - z[1];
    z[27]=z[27]*z[36];
    z[28]= - n<T>(19,3) + z[15];
    z[28]=z[28]*z[39];
    z[28]=n<T>(5,3) + z[28];
    z[28]=z[28]*z[70];
    z[23]= - z[23]*z[72];
    z[29]=z[1]*z[31];
    z[23]=z[29] + z[23];
    z[23]=z[23]*z[52];
    z[29]=static_cast<T>(7)- z[37];
    z[29]=z[1]*z[29];
    z[23]=z[23] + static_cast<T>(1)+ n<T>(3,2)*z[29];
    z[23]=z[3]*z[23];
    z[29]=static_cast<T>(1)+ z[63];
    z[23]=z[23] + n<T>(1,2)*z[29] - 7*z[1];
    z[23]=z[3]*z[23];

    r += z[18] + z[19] + z[20] + z[21] + z[22] + z[23] + z[24] + n<T>(1,2)*
      z[25] + z[26] + z[27] + z[28];
 
    return r;
}

template double qqb_2lha_r1245(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1245(const std::array<dd_real,30>&);
#endif
