#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r55(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=k[5];
    z[6]=k[12];
    z[7]=k[6];
    z[8]= - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[8]=z[8]*z[6];
    z[8]=z[8] + n<T>(3,4);
    z[9]=npow(z[2],2);
    z[10]=z[4]*z[9]*z[8];
    z[11]=z[9]*z[6];
    z[12]=z[2] - 3;
    z[13]= - z[2]*z[12];
    z[11]=z[13] + z[11];
    z[11]= - 5*z[10] + n<T>(5,4)*z[11];
    z[11]=z[3]*z[11];
    z[8]=z[2]*z[8];
    z[8]=z[10] + z[8];
    z[8]=z[5]*z[8];
    z[10]= - z[12] - z[6];
    z[10]=z[10]*z[6]*z[2];
    z[8]=z[8] + 3*z[9] + z[10] + z[11];
    z[10]= - static_cast<T>(1)- z[2];
    z[10]=z[7]*z[10];
    z[9]=z[1]*z[3]*z[9];
    z[8]= - n<T>(15,8)*z[9] + n<T>(1,2)*z[8] + z[10];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r55(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r55(const std::array<dd_real,30>&);
#endif
