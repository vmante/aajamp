#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r504(const std::array<T,30>& k) {
  T z[80];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[6];
    z[7]=k[8];
    z[8]=k[4];
    z[9]=k[27];
    z[10]=k[11];
    z[11]=k[10];
    z[12]=k[5];
    z[13]=k[12];
    z[14]=n<T>(1,2)*z[8];
    z[15]=9*z[8];
    z[16]=n<T>(461,3) + z[15];
    z[16]=z[16]*z[14];
    z[17]=n<T>(1,3)*z[8];
    z[18]=static_cast<T>(421)- n<T>(277,2)*z[8];
    z[18]=z[18]*z[17];
    z[18]= - n<T>(177,2) + z[18];
    z[19]=n<T>(1,2)*z[11];
    z[18]=z[18]*z[19];
    z[16]=z[18] - n<T>(355,3) + z[16];
    z[18]=n<T>(1,4)*z[11];
    z[16]=z[16]*z[18];
    z[20]=static_cast<T>(35)- n<T>(13,8)*z[8];
    z[20]=z[20]*z[14];
    z[16]=z[16] - static_cast<T>(27)+ z[20];
    z[20]= - n<T>(497,3) + 55*z[8];
    z[20]=z[20]*z[14];
    z[20]= - static_cast<T>(3)+ z[20];
    z[21]= - static_cast<T>(113)+ 151*z[8];
    z[21]=z[21]*z[14];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[18];
    z[22]= - static_cast<T>(721)+ 473*z[8];
    z[22]=z[8]*z[22];
    z[21]=z[21] - static_cast<T>(1)+ n<T>(1,24)*z[22];
    z[21]=z[11]*z[21];
    z[20]=n<T>(1,4)*z[20] + z[21];
    z[21]=npow(z[8],2);
    z[22]=z[19]*z[21];
    z[23]= - z[21] - z[22];
    z[23]=z[11]*z[23];
    z[24]=n<T>(1,2)*z[21];
    z[23]= - z[24] + z[23];
    z[25]=11*z[5];
    z[23]=z[23]*z[25];
    z[20]=n<T>(1,2)*z[20] + z[23];
    z[20]=z[5]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[16]=z[5]*z[16];
    z[20]=7*z[8];
    z[23]= - n<T>(19,2) - z[15];
    z[23]=z[8]*z[23];
    z[23]=n<T>(143,4) + z[23];
    z[23]=z[11]*z[23];
    z[23]=z[23] + n<T>(145,4) + z[20];
    z[23]=z[11]*z[23];
    z[26]=n<T>(919,2) - 19*z[8];
    z[23]=n<T>(1,6)*z[26] + z[23];
    z[16]=n<T>(1,8)*z[23] + z[16];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(5,48) + z[16];
    z[23]= - static_cast<T>(179)+ 205*z[8];
    z[23]=z[23]*z[18];
    z[23]=z[23] - static_cast<T>(133)+ n<T>(179,2)*z[8];
    z[26]=n<T>(1,3)*z[11];
    z[23]=z[23]*z[26];
    z[27]=n<T>(155,3) - n<T>(17,4)*z[8];
    z[27]=z[8]*z[27];
    z[23]=z[23] - static_cast<T>(51)+ z[27];
    z[27]= - static_cast<T>(1)+ z[17];
    z[27]=z[8]*z[27];
    z[28]=z[17]*z[11];
    z[29]= - z[8] - z[28];
    z[29]=z[11]*z[29];
    z[27]=z[27] + z[29];
    z[27]=z[27]*z[25];
    z[23]=n<T>(1,4)*z[23] + z[27];
    z[23]=z[5]*z[23];
    z[27]=n<T>(1,8)*z[11];
    z[29]=19*z[11] + n<T>(67,2) + z[15];
    z[29]=z[29]*z[27];
    z[23]=z[23] + z[29] + static_cast<T>(13)- n<T>(11,4)*z[8];
    z[23]=z[5]*z[23];
    z[29]=z[15] - 7;
    z[30]=n<T>(1,16)*z[11];
    z[29]=z[29]*z[30];
    z[29]=z[29] - 1;
    z[30]= - z[11]*z[29];
    z[23]=z[23] - n<T>(31,24) + z[30];
    z[30]=n<T>(1,2)*z[5];
    z[23]=z[23]*z[30];
    z[31]=z[11] + 3;
    z[32]=z[31]*z[11];
    z[33]=static_cast<T>(491)- 181*z[8];
    z[33]=n<T>(1,3)*z[33] + 9*z[32];
    z[34]=n<T>(1,8)*z[8];
    z[35]=static_cast<T>(1)- z[34];
    z[35]=z[35]*z[17];
    z[36]= - static_cast<T>(1)- z[18];
    z[36]=z[11]*z[36];
    z[35]=n<T>(1,6)*z[36] - n<T>(1,4) + z[35];
    z[35]=z[35]*z[25];
    z[33]=n<T>(1,32)*z[33] + z[35];
    z[33]=z[5]*z[33];
    z[35]=z[19] + 1;
    z[36]=z[11]*z[35];
    z[36]= - n<T>(143,3) + 9*z[36];
    z[33]=n<T>(1,32)*z[36] + z[33];
    z[36]=npow(z[5],2);
    z[33]=z[33]*z[36];
    z[37]=z[14] - 1;
    z[37]=z[37]*z[25];
    z[38]= - n<T>(65,8) - z[37];
    z[39]=npow(z[5],3);
    z[38]=z[38]*z[39];
    z[40]=z[4]*npow(z[5],4);
    z[38]=z[38] - n<T>(11,4)*z[40];
    z[38]=z[4]*z[38];
    z[33]=z[33] + n<T>(1,6)*z[38];
    z[33]=z[4]*z[33];
    z[23]=z[23] + z[33];
    z[23]=z[4]*z[23];
    z[16]=n<T>(1,2)*z[16] + z[23];
    z[16]=z[4]*z[16];
    z[23]= - static_cast<T>(137)+ n<T>(169,2)*z[8];
    z[23]=z[23]*z[17];
    z[23]= - static_cast<T>(1)+ z[23];
    z[33]=static_cast<T>(67)- n<T>(223,8)*z[8];
    z[33]=z[33]*z[17];
    z[33]= - n<T>(77,8) + z[33];
    z[38]=z[11]*z[8];
    z[33]=z[33]*z[38];
    z[23]=n<T>(1,4)*z[23] + z[33];
    z[23]=z[11]*z[23];
    z[33]=n<T>(1,4)*z[8];
    z[41]= - n<T>(103,3) + 11*z[8];
    z[41]=z[41]*z[33];
    z[41]= - static_cast<T>(1)+ z[41];
    z[23]=n<T>(1,2)*z[41] + z[23];
    z[41]= - static_cast<T>(101)+ 133*z[8];
    z[34]=z[41]*z[34];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[8]*z[34];
    z[34]= - n<T>(1,4) + z[34];
    z[34]=z[11]*z[34];
    z[41]=49*z[8];
    z[42]= - n<T>(139,2) + z[41];
    z[42]=z[8]*z[42];
    z[42]= - static_cast<T>(3)+ n<T>(1,6)*z[42];
    z[42]=z[8]*z[42];
    z[34]=z[34] - static_cast<T>(1)+ z[42];
    z[34]=z[11]*z[34];
    z[42]= - static_cast<T>(3)+ n<T>(7,12)*z[8];
    z[42]=z[8]*z[42];
    z[42]= - n<T>(3,2) + z[42];
    z[34]=n<T>(1,2)*z[42] + z[34];
    z[42]=npow(z[8],3);
    z[43]=z[42]*z[11];
    z[44]= - z[42] - z[43];
    z[44]=z[5]*z[11]*z[44];
    z[34]=n<T>(1,2)*z[34] + n<T>(11,3)*z[44];
    z[34]=z[5]*z[34];
    z[23]=n<T>(1,2)*z[23] + z[34];
    z[23]=z[5]*z[23];
    z[34]=5*z[8];
    z[44]=n<T>(197,4) + z[34];
    z[44]=z[44]*z[17];
    z[45]=n<T>(293,3) - z[20];
    z[45]=z[8]*z[45];
    z[45]= - n<T>(589,3) + z[45];
    z[46]= - n<T>(469,6) - z[15];
    z[46]=z[8]*z[46];
    z[46]=static_cast<T>(125)+ z[46];
    z[46]=z[8]*z[46];
    z[46]= - n<T>(145,6) + z[46];
    z[46]=z[11]*z[46];
    z[45]=n<T>(1,2)*z[45] + z[46];
    z[45]=z[45]*z[19];
    z[44]=z[45] - n<T>(225,4) + z[44];
    z[23]=n<T>(1,8)*z[44] + z[23];
    z[23]=z[5]*z[23];
    z[44]=n<T>(59,8) + n<T>(5,3)*z[8];
    z[45]=n<T>(3,2)*z[8];
    z[46]= - static_cast<T>(1)+ z[45];
    z[46]=z[11]*z[46];
    z[46]= - n<T>(1,2) + z[46];
    z[46]=z[11]*z[46];
    z[44]=n<T>(1,2)*z[44] + z[46];
    z[23]=n<T>(1,2)*z[44] + z[23];
    z[16]=n<T>(1,2)*z[23] + z[16];
    z[16]=z[4]*z[16];
    z[23]= - n<T>(9,2) - z[8];
    z[23]=z[23]*z[14];
    z[23]= - static_cast<T>(3)+ z[23];
    z[23]=z[8]*z[23];
    z[44]=z[8] + 5;
    z[46]= - z[8]*z[44];
    z[46]= - static_cast<T>(9)+ z[46];
    z[46]=z[8]*z[46];
    z[46]= - static_cast<T>(7)+ z[46];
    z[46]=z[46]*z[14];
    z[46]= - static_cast<T>(1)+ z[46];
    z[46]=z[46]*z[19];
    z[23]=z[46] - n<T>(5,4) + z[23];
    z[23]=z[11]*z[23];
    z[46]= - n<T>(1,2) - z[21];
    z[23]=n<T>(1,2)*z[46] + z[23];
    z[46]=z[33] + 1;
    z[46]=z[46]*z[8];
    z[47]=z[46] + n<T>(3,2);
    z[47]=z[47]*z[8];
    z[47]=z[47] + 1;
    z[47]=z[47]*z[8];
    z[47]=z[47] + n<T>(1,4);
    z[47]=z[47]*z[11];
    z[48]=z[8] + 3;
    z[48]=z[48]*z[8];
    z[49]=z[48] + 3;
    z[49]=z[49]*z[8];
    z[49]=z[49] + 1;
    z[50]=n<T>(3,4)*z[49] + z[47];
    z[50]=z[11]*z[50];
    z[47]=z[47] + z[49];
    z[47]=z[47]*z[11];
    z[51]=z[14] + 1;
    z[51]=z[51]*z[8];
    z[52]=z[51] + n<T>(1,2);
    z[53]=n<T>(3,2)*z[52];
    z[47]=z[47] + z[53];
    z[54]= - z[47]*z[30];
    z[55]=z[8] + n<T>(3,2);
    z[56]=z[8]*z[55];
    z[56]=n<T>(1,2) + z[56];
    z[50]=z[54] + n<T>(1,2)*z[56] + z[50];
    z[50]=z[5]*z[50];
    z[54]= - z[49]*z[19];
    z[54]=z[54] - z[52];
    z[56]=z[4]*z[11];
    z[57]=n<T>(1,8)*z[56];
    z[54]=z[54]*z[57];
    z[23]=z[54] + n<T>(1,4)*z[23] + z[50];
    z[23]=z[6]*z[23];
    z[50]=z[8] - 1;
    z[54]= - z[50]*z[14];
    z[58]=static_cast<T>(3)- z[21];
    z[58]=z[58]*z[14];
    z[58]=static_cast<T>(1)+ z[58];
    z[58]=z[58]*z[19];
    z[54]=z[58] + static_cast<T>(1)+ z[54];
    z[54]=z[11]*z[54];
    z[58]=z[8] + n<T>(1,2);
    z[54]=z[54] + z[58];
    z[59]=z[8] + n<T>(5,2);
    z[59]=z[59]*z[14];
    z[59]=z[59] + 1;
    z[60]= - z[8]*z[59];
    z[60]= - n<T>(1,4) + z[60];
    z[61]= - n<T>(7,2) - z[8];
    z[61]=z[8]*z[61];
    z[61]= - n<T>(9,2) + z[61];
    z[61]=z[8]*z[61];
    z[61]= - n<T>(5,2) + z[61];
    z[61]=z[8]*z[61];
    z[61]= - n<T>(1,2) + z[61];
    z[61]=z[61]*z[19];
    z[60]=3*z[60] + z[61];
    z[60]=z[11]*z[60];
    z[47]=z[5]*z[47];
    z[61]= - n<T>(5,4) - z[8];
    z[61]=z[8]*z[61];
    z[47]=z[47] + z[60] - n<T>(1,4) + z[61];
    z[47]=z[5]*z[47];
    z[60]=n<T>(5,2) + z[48];
    z[60]=z[60]*z[21];
    z[60]= - n<T>(1,2) + z[60];
    z[60]=z[60]*z[19];
    z[61]=static_cast<T>(1)+ 3*z[51];
    z[61]=z[8]*z[61];
    z[60]=z[60] - n<T>(1,2) + z[61];
    z[60]=z[11]*z[60];
    z[61]=z[8]*z[58];
    z[60]=z[60] - n<T>(1,4) + z[61];
    z[47]=n<T>(1,2)*z[60] + z[47];
    z[47]=z[5]*z[47];
    z[60]=z[8] + 1;
    z[61]=z[11]*z[52];
    z[61]=n<T>(1,2)*z[60] + z[61];
    z[57]=z[61]*z[57];
    z[23]=z[23] + z[57] + n<T>(1,4)*z[54] + z[47];
    z[47]=n<T>(1,4)*z[6];
    z[23]=z[23]*z[47];
    z[54]=3*z[11];
    z[57]= - n<T>(1,2) - z[11];
    z[57]=z[57]*z[54];
    z[61]= - static_cast<T>(21)- 5*z[11];
    z[61]=z[11]*z[61];
    z[61]= - static_cast<T>(27)+ z[61];
    z[61]=z[61]*z[30];
    z[62]=3*z[4];
    z[63]=z[62]*z[36];
    z[32]=z[32] + 3;
    z[32]=z[32]*z[5];
    z[64]=static_cast<T>(11)- 3*z[32];
    z[64]=z[5]*z[64];
    z[64]=z[64] + z[63];
    z[65]=n<T>(1,2)*z[4];
    z[64]=z[64]*z[65];
    z[57]=z[64] + z[57] + z[61];
    z[57]=z[57]*z[65];
    z[61]=3*z[8];
    z[64]= - static_cast<T>(1)- z[61];
    z[64]=z[8]*z[64];
    z[64]=n<T>(15,2) + z[64];
    z[64]=z[64]*z[19];
    z[64]=z[64] + static_cast<T>(7)- z[33];
    z[64]=z[11]*z[64];
    z[57]=z[57] + n<T>(5,4) + z[64];
    z[57]=z[4]*z[57];
    z[64]=static_cast<T>(17)+ z[8];
    z[64]=z[64]*z[14];
    z[66]=static_cast<T>(7)- z[14];
    z[66]=z[8]*z[66];
    z[66]=n<T>(15,2) + z[66];
    z[66]=z[66]*z[38];
    z[64]=z[64] + z[66];
    z[64]=z[11]*z[64];
    z[64]=z[8] + z[64];
    z[57]=n<T>(1,2)*z[64] + z[57];
    z[57]=z[4]*z[57];
    z[64]=z[21]*z[11];
    z[66]=z[52]*z[64];
    z[67]=z[60]*z[21];
    z[66]=z[66] + z[67];
    z[66]=z[66]*z[11];
    z[66]=z[66] + z[24];
    z[68]=z[11]*z[55];
    z[68]=n<T>(1,2)*z[44] + z[68];
    z[68]=z[11]*z[68];
    z[69]=z[4]*z[5];
    z[70]= - z[32] + z[69];
    z[70]=z[70]*z[65];
    z[68]=z[70] + n<T>(1,2) + z[68];
    z[68]=z[68]*z[65];
    z[46]=n<T>(3,4) + z[46];
    z[46]=z[46]*z[38];
    z[46]=z[51] + z[46];
    z[46]=z[11]*z[46];
    z[46]=z[68] + z[33] + z[46];
    z[46]=z[4]*z[46];
    z[46]=n<T>(1,2)*z[66] + z[46];
    z[46]=z[9]*z[46]*z[62];
    z[46]=z[46] + n<T>(5,2)*z[66] + z[57];
    z[46]=z[9]*z[46];
    z[57]=n<T>(1,4)*z[4];
    z[66]=z[57]*z[39];
    z[68]=static_cast<T>(1)- n<T>(1,4)*z[32];
    z[68]=z[68]*z[36];
    z[68]=z[68] + z[66];
    z[68]=z[68]*z[62];
    z[70]=3*z[5];
    z[71]= - static_cast<T>(2)- z[19];
    z[71]=z[11]*z[71];
    z[71]= - n<T>(5,2) + z[71];
    z[71]=z[71]*z[70];
    z[71]=n<T>(17,4) + z[71];
    z[71]=z[5]*z[71];
    z[68]=z[71] + z[68];
    z[68]=z[4]*z[68];
    z[71]= - static_cast<T>(1)+ z[33];
    z[71]=z[11]*z[71];
    z[71]= - n<T>(1,2) + z[71];
    z[54]=z[71]*z[54];
    z[71]= - static_cast<T>(17)- z[11];
    z[71]=z[71]*z[19];
    z[71]= - static_cast<T>(17)+ z[71];
    z[71]=z[71]*z[30];
    z[54]=z[68] + z[54] + z[71];
    z[54]=z[4]*z[54];
    z[68]=static_cast<T>(21)- z[34];
    z[71]=z[8] + 2;
    z[71]=z[71]*z[8];
    z[72]=n<T>(9,4) - 2*z[71];
    z[72]=z[11]*z[72];
    z[68]=n<T>(1,4)*z[68] + z[72];
    z[68]=z[11]*z[68];
    z[54]=z[54] + n<T>(3,4) + z[68];
    z[54]=z[4]*z[54];
    z[68]=n<T>(9,4)*z[8];
    z[72]= - z[50]*z[68];
    z[73]= - static_cast<T>(1)+ n<T>(7,4)*z[8];
    z[73]=z[73]*z[8];
    z[74]=n<T>(11,4) - z[73];
    z[74]=z[74]*z[38];
    z[72]=z[72] + z[74];
    z[72]=z[11]*z[72];
    z[46]=z[46] + z[54] - z[14] + z[72];
    z[46]=z[9]*z[46];
    z[54]=n<T>(33,2) - z[34];
    z[24]=z[54]*z[24];
    z[54]=z[60]*z[8];
    z[72]=n<T>(13,2) - z[54];
    z[72]=z[72]*z[64];
    z[24]=z[24] + z[72];
    z[24]=z[11]*z[24];
    z[24]=z[21] + z[24];
    z[72]=npow(z[8],4);
    z[74]=npow(z[11],2);
    z[75]=z[72]*z[74];
    z[76]=z[75]*z[30];
    z[77]=z[58]*z[43];
    z[77]=n<T>(7,4)*z[42] + z[77];
    z[77]=z[11]*z[77];
    z[77]=z[77] - z[76];
    z[77]=z[5]*z[77];
    z[24]=n<T>(1,2)*z[24] + z[77];
    z[24]=z[24]*z[30];
    z[77]=static_cast<T>(15)- 13*z[8];
    z[77]=z[77]*z[14];
    z[77]=n<T>(5,3) + z[77];
    z[78]=z[17] + 1;
    z[79]= - z[8]*z[78];
    z[79]=n<T>(5,24) + z[79];
    z[79]=z[79]*z[38];
    z[77]=n<T>(1,8)*z[77] + z[79];
    z[77]=z[11]*z[77];
    z[79]=static_cast<T>(5)+ z[15];
    z[24]=z[24] + n<T>(1,16)*z[79] + z[77];
    z[77]=n<T>(23,6) - z[8];
    z[77]=z[77]*z[43];
    z[77]=n<T>(13,12)*z[42] + z[77];
    z[77]=z[11]*z[77];
    z[76]=z[77] + z[76];
    z[76]=z[5]*z[76];
    z[77]=z[75]*z[5];
    z[75]=z[77] + z[54] - z[75];
    z[75]=z[3]*z[75];
    z[79]= - n<T>(23,2) - z[8];
    z[43]=z[79]*z[43];
    z[43]= - n<T>(13,4)*z[42] + z[43];
    z[26]=z[43]*z[26];
    z[26]=n<T>(5,6)*z[75] + z[76] - n<T>(3,4)*z[54] + z[26];
    z[26]=z[3]*z[26];
    z[43]=z[71] + 1;
    z[71]=z[43]*z[64];
    z[71]=z[71] + 2*z[67];
    z[71]=z[71]*z[11];
    z[71]=z[71] + z[21];
    z[75]=z[9]*z[71];
    z[24]=n<T>(1,8)*z[26] + n<T>(1,2)*z[24] + z[75];
    z[24]=z[3]*z[24];
    z[26]= - z[25] + n<T>(77,8);
    z[26]=z[8]*z[26];
    z[26]= - static_cast<T>(23)+ z[26];
    z[26]=z[5]*z[26];
    z[26]=n<T>(29,2) + z[26];
    z[26]=z[26]*z[36];
    z[37]=n<T>(233,16) + z[37];
    z[37]=z[37]*z[39];
    z[37]=z[37] + n<T>(11,2)*z[40];
    z[37]=z[4]*z[37];
    z[26]=z[26] + z[37];
    z[37]=n<T>(1,3)*z[4];
    z[26]=z[26]*z[37];
    z[75]= - static_cast<T>(47)+ n<T>(35,2)*z[8];
    z[76]= - static_cast<T>(1)- n<T>(139,12)*z[8];
    z[76]=z[5]*z[76];
    z[75]=n<T>(1,2)*z[75] + z[76];
    z[75]=z[5]*z[75];
    z[75]=n<T>(97,12) + z[75];
    z[76]=n<T>(1,4)*z[5];
    z[75]=z[75]*z[76];
    z[26]=z[75] + z[26];
    z[26]=z[4]*z[26];
    z[75]= - z[60]*z[30];
    z[58]=z[75] - z[58];
    z[58]=z[5]*z[58];
    z[75]= - n<T>(13,2) + z[8];
    z[58]=n<T>(19,24)*z[75] + z[58];
    z[58]=z[5]*z[58];
    z[58]=n<T>(5,12) + z[58];
    z[26]=n<T>(1,2)*z[58] + z[26];
    z[26]=z[4]*z[26];
    z[58]=z[60]*z[5];
    z[75]=z[58] - z[8];
    z[79]=z[5]*z[75];
    z[79]=z[14] + z[79];
    z[79]=z[5]*z[79];
    z[30]= - z[6]*z[75]*z[30];
    z[30]=z[79] + z[30];
    z[30]=z[30]*z[47];
    z[50]=n<T>(1,2)*z[50] - z[58];
    z[50]=z[5]*z[50];
    z[50]=n<T>(31,12)*z[8] + z[50];
    z[50]=z[5]*z[50];
    z[34]= - n<T>(107,8) - z[34];
    z[34]=n<T>(1,3)*z[34] + z[50];
    z[26]=z[30] + n<T>(1,4)*z[34] + z[26];
    z[30]=n<T>(1,2)*z[36];
    z[34]= - n<T>(151,12) - z[5];
    z[34]=z[34]*z[30];
    z[40]= - n<T>(103,4)*z[39] - 11*z[40];
    z[37]=z[40]*z[37];
    z[34]=z[34] + z[37];
    z[34]=z[34]*z[65];
    z[37]= - static_cast<T>(1)- z[5];
    z[37]=z[37]*z[76];
    z[37]= - n<T>(1,3) + z[37];
    z[37]=z[5]*z[37];
    z[34]=z[37] + z[34];
    z[34]=z[4]*z[34];
    z[37]= - n<T>(1,2) - z[5];
    z[37]=z[37]*z[36];
    z[37]=n<T>(17,12) + z[37];
    z[30]= - z[6]*z[30];
    z[30]=z[39] + z[30];
    z[30]=z[30]*z[47];
    z[30]=n<T>(5,12)*z[3] + z[30] + n<T>(1,4)*z[37] + z[34];
    z[30]=z[1]*z[30];
    z[34]= - z[36] - z[66];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(3,2)*z[5] + z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(3,4) + z[34];
    z[34]=z[34]*z[62];
    z[25]= - z[25] - z[63];
    z[25]=z[25]*z[57];
    z[25]= - static_cast<T>(2)+ z[25];
    z[25]=z[25]*npow(z[4],2);
    z[37]= - static_cast<T>(1)- z[69];
    z[37]=z[9]*z[37]*npow(z[4],3);
    z[25]=z[25] + n<T>(3,4)*z[37];
    z[25]=z[9]*z[25];
    z[25]=z[34] + z[25];
    z[25]=z[9]*z[25];
    z[34]=z[3]*z[78];
    z[25]=n<T>(1,4)*z[30] + n<T>(5,32)*z[34] + n<T>(1,2)*z[26] + z[25];
    z[25]=z[1]*z[25];
    z[26]=z[64] + z[8];
    z[30]=z[26]*z[19];
    z[34]=z[42]*z[19];
    z[34]=z[21] + z[34];
    z[34]=z[11]*z[34];
    z[34]=z[14] + z[34];
    z[34]=z[3]*z[34];
    z[30]=z[30] + z[34];
    z[30]=z[30]*npow(z[3],2);
    z[34]=3*z[10];
    z[37]= - static_cast<T>(1)- n<T>(9,16)*z[8];
    z[37]=z[8]*z[37];
    z[37]= - n<T>(3,16)*z[4] - n<T>(27,64) + z[37];
    z[34]=z[37]*z[34];
    z[37]= - static_cast<T>(5)- n<T>(27,4)*z[8];
    z[34]=n<T>(1,4)*z[37] + z[34];
    z[34]=z[10]*z[34];
    z[37]=static_cast<T>(1)+ n<T>(9,8)*z[8];
    z[37]=z[10]*z[37];
    z[37]=n<T>(9,16) + z[37];
    z[37]=z[10]*z[37];
    z[40]=z[2]*npow(z[10],2);
    z[37]=z[37] - n<T>(9,32)*z[40];
    z[37]=z[2]*z[37];
    z[30]=z[37] + n<T>(5,12)*z[30] - n<T>(9,32) + z[34];
    z[30]=z[2]*z[30];
    z[34]= - z[72]*z[19];
    z[34]= - z[67] + z[34];
    z[34]=z[11]*z[34];
    z[34]= - z[51] + z[34];
    z[34]=z[3]*z[34];
    z[37]=z[14] - z[64];
    z[40]=z[14] - n<T>(5,3);
    z[37]=z[11]*z[40]*z[37];
    z[40]=n<T>(3,4)*z[8];
    z[34]=n<T>(5,3)*z[34] + z[40] + z[37];
    z[34]=z[3]*z[34];
    z[28]=z[44]*z[28];
    z[37]= - n<T>(5,3) + z[45];
    z[28]=n<T>(1,2)*z[37] + z[28];
    z[28]=z[11]*z[28];
    z[28]=z[34] - n<T>(5,4) + z[28];
    z[28]=z[3]*z[28];
    z[34]= - z[8]*z[74];
    z[28]=z[28] - n<T>(5,6)*z[4] + n<T>(9,4)*z[60] + z[34];
    z[34]=static_cast<T>(5)+ n<T>(27,8)*z[8];
    z[34]=z[8]*z[34];
    z[34]=z[57] + n<T>(21,16) + z[34];
    z[37]=static_cast<T>(1)+ n<T>(3,8)*z[8];
    z[37]=z[8]*z[37];
    z[37]=n<T>(27,32) + z[37];
    z[37]=z[8]*z[37];
    z[37]=n<T>(13,64) + z[37];
    z[42]=27*z[8];
    z[44]=9*z[4] + static_cast<T>(23)+ z[42];
    z[44]=z[4]*z[44];
    z[37]=3*z[37] + n<T>(1,16)*z[44];
    z[37]=z[10]*z[37];
    z[34]=n<T>(1,2)*z[34] + z[37];
    z[34]=z[10]*z[34];
    z[37]= - n<T>(1,6)*z[1] - n<T>(1,4);
    z[37]=z[3]*z[37];
    z[37]=n<T>(1,3) + z[37];
    z[37]=z[1]*z[37];
    z[28]=z[30] + n<T>(5,4)*z[37] + z[34] + n<T>(1,4)*z[28];
    z[28]=z[2]*z[28];
    z[30]= - n<T>(125,4) + 31*z[8];
    z[30]=z[30]*z[17];
    z[30]= - static_cast<T>(3)+ z[30];
    z[30]=z[30]*z[14];
    z[30]= - static_cast<T>(1)+ z[30];
    z[30]=z[8]*z[30];
    z[30]= - n<T>(1,4) + z[30];
    z[30]=z[11]*z[30];
    z[34]= - static_cast<T>(3)+ n<T>(19,24)*z[8];
    z[34]=z[8]*z[34];
    z[34]= - static_cast<T>(3)+ z[34];
    z[34]=z[8]*z[34];
    z[30]=z[30] - static_cast<T>(1)+ z[34];
    z[30]=z[11]*z[30];
    z[30]= - n<T>(11,6)*z[77] - z[53] + z[30];
    z[30]=z[5]*z[30];
    z[34]=n<T>(265,2) - z[41];
    z[34]=z[34]*z[17];
    z[34]= - n<T>(19,4) + z[34];
    z[34]=z[8]*z[34];
    z[34]=static_cast<T>(3)+ z[34];
    z[34]=z[8]*z[34];
    z[34]=n<T>(1,2) + z[34];
    z[34]=z[34]*z[19];
    z[37]=static_cast<T>(11)- n<T>(13,2)*z[8];
    z[37]=z[8]*z[37];
    z[37]=static_cast<T>(3)+ n<T>(7,6)*z[37];
    z[37]=z[8]*z[37];
    z[34]=z[34] + n<T>(1,2) + z[37];
    z[34]=z[11]*z[34];
    z[37]=static_cast<T>(1)+ n<T>(11,24)*z[8];
    z[37]=z[8]*z[37];
    z[34]=z[34] - n<T>(1,4) + z[37];
    z[30]=n<T>(1,2)*z[34] + z[30];
    z[30]=z[5]*z[30];
    z[34]=n<T>(53,2) - 25*z[8];
    z[17]=z[34]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[34]=static_cast<T>(103)- n<T>(349,3)*z[8];
    z[34]=z[8]*z[34];
    z[34]=n<T>(17,3) + z[34];
    z[34]=z[34]*z[14];
    z[34]=static_cast<T>(1)+ z[34];
    z[27]=z[34]*z[27];
    z[34]=static_cast<T>(3)- n<T>(145,24)*z[8];
    z[34]=z[8]*z[34];
    z[27]=z[27] + n<T>(1,4) + z[34];
    z[27]=z[11]*z[27];
    z[17]=z[30] + n<T>(1,8)*z[17] + z[27];
    z[17]=z[5]*z[17];
    z[27]= - z[59]*z[61];
    z[30]=static_cast<T>(5)- n<T>(3,2)*z[48];
    z[30]=z[8]*z[30];
    z[30]= - static_cast<T>(13)+ z[30];
    z[30]=z[30]*z[14];
    z[30]=n<T>(5,3) + z[30];
    z[30]=z[11]*z[30];
    z[27]=z[30] + n<T>(127,24) + z[27];
    z[27]=z[11]*z[27];
    z[30]= - n<T>(61,6) - z[61];
    z[30]=z[8]*z[30];
    z[27]=z[27] + n<T>(45,4) + z[30];
    z[17]=n<T>(1,4)*z[27] + z[17];
    z[27]= - n<T>(7,2) - z[15];
    z[27]=z[8]*z[27];
    z[27]=n<T>(15,4) + z[27];
    z[27]=z[11]*z[27];
    z[27]=z[27] + n<T>(81,4) + z[20];
    z[27]=z[11]*z[27];
    z[27]=n<T>(7,2) + z[27];
    z[29]= - z[29]*z[56];
    z[27]=n<T>(1,8)*z[27] + z[29];
    z[27]=z[4]*z[27];
    z[29]=n<T>(39,4) - z[8];
    z[29]=z[8]*z[29];
    z[30]=static_cast<T>(7)+ n<T>(9,2)*z[8];
    z[30]=z[30]*z[8];
    z[30]=z[30] + n<T>(7,4);
    z[34]= - z[8]*z[30];
    z[34]=n<T>(7,8) + z[34];
    z[34]=z[11]*z[34];
    z[29]=z[34] + n<T>(81,8) + z[29];
    z[29]=z[29]*z[19];
    z[29]=z[29] + n<T>(21,8) - z[8];
    z[27]=n<T>(1,4)*z[29] + z[27];
    z[27]=z[4]*z[27];
    z[29]=static_cast<T>(1)- n<T>(1,4)*z[38];
    z[18]=z[29]*z[18];
    z[29]= - static_cast<T>(5)- z[68];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(21,8) + z[29];
    z[29]=z[8]*z[29];
    z[18]=z[18] + n<T>(9,16) + z[29];
    z[18]=n<T>(1,4)*z[18] + z[27];
    z[27]= - static_cast<T>(55)- z[42];
    z[27]=z[8]*z[27];
    z[29]= - z[11]*z[30];
    z[27]=z[29] - n<T>(49,2) + z[27];
    z[27]=z[11]*z[27];
    z[27]=z[27] - n<T>(55,2) - z[42];
    z[29]=z[35]*z[56];
    z[30]= - static_cast<T>(7)- z[15];
    z[30]=z[11]*z[30];
    z[30]=n<T>(1,32)*z[30] - static_cast<T>(1)- n<T>(27,32)*z[8];
    z[30]=z[11]*z[30];
    z[29]= - n<T>(9,32)*z[29] - n<T>(9,32) + z[30];
    z[29]=z[4]*z[29];
    z[27]=n<T>(1,32)*z[27] + z[29];
    z[27]=z[4]*z[27];
    z[15]= - static_cast<T>(23)- z[15];
    z[15]=z[8]*z[15];
    z[15]= - n<T>(35,2) + z[15];
    z[15]=z[15]*z[14];
    z[15]=n<T>(3,8)*z[11] - static_cast<T>(1)+ z[15];
    z[15]=z[11]*z[15];
    z[29]= - static_cast<T>(23)- n<T>(27,2)*z[8];
    z[29]=z[8]*z[29];
    z[15]=z[15] - n<T>(35,4) + z[29];
    z[15]=n<T>(1,16)*z[15] + z[27];
    z[15]=z[4]*z[15];
    z[27]= - static_cast<T>(1)- n<T>(9,32)*z[8];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(81,64) + z[27];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(39,64) + z[27];
    z[27]=z[8]*z[27];
    z[29]=static_cast<T>(1)- z[38];
    z[29]=z[11]*z[29];
    z[27]=n<T>(3,64)*z[29] - n<T>(1,64) + z[27];
    z[15]=n<T>(1,2)*z[27] + z[15];
    z[15]=z[10]*z[15];
    z[15]=n<T>(1,2)*z[18] + z[15];
    z[15]=z[10]*z[15];
    z[18]=3*z[21];
    z[27]= - z[52]*z[18];
    z[29]= - z[49]*z[22];
    z[27]=z[27] + z[29];
    z[27]=z[11]*z[27];
    z[29]=z[65] - z[55];
    z[21]=z[21]*z[29];
    z[21]=z[27] + z[21];
    z[27]=n<T>(1,2)*z[6];
    z[21]=z[21]*z[27];
    z[14]=z[60]*z[14];
    z[22]=z[52]*z[22];
    z[27]=static_cast<T>(1)+ z[40];
    z[27]=z[8]*z[27];
    z[27]=n<T>(1,4) + z[27];
    z[27]=z[8]*z[27];
    z[22]=z[27] + z[22];
    z[22]=z[11]*z[22];
    z[27]= - z[4]*z[33];
    z[14]=z[21] + z[27] + z[14] + z[22];
    z[14]=z[6]*z[14];
    z[21]= - z[61] + z[64];
    z[21]=z[11]*z[21];
    z[18]=z[10]*z[74]*z[18];
    z[18]=z[21] + z[18];
    z[18]=z[10]*z[18];
    z[20]=static_cast<T>(3)+ z[20];
    z[20]=z[20]*z[64];
    z[20]=z[73] + n<T>(1,4)*z[20];
    z[20]=z[20]*z[19];
    z[21]=z[2]*z[11]*z[26];
    z[14]= - n<T>(7,8)*z[21] + n<T>(1,8)*z[18] + z[20] + z[14];
    z[14]=z[12]*z[14];
    z[18]= - z[3]*z[71];
    z[20]= - z[43]*z[38];
    z[20]= - z[54] + z[20];
    z[20]=z[11]*z[20];
    z[21]= - z[4]*z[43]*z[74];
    z[18]=z[18] + 2*z[20] + z[21];
    z[18]=z[13]*z[18];
    z[20]=n<T>(9,2) + z[11];
    z[20]=z[20]*z[19];
    z[20]=static_cast<T>(3)+ z[20];
    z[20]=z[5]*z[20];
    z[20]= - n<T>(3,2) + z[20];
    z[20]=z[5]*z[20];
    z[21]= - static_cast<T>(5)+ n<T>(3,2)*z[32];
    z[21]=z[21]*z[36];
    z[22]=z[39]*z[4];
    z[22]=n<T>(3,2)*z[22];
    z[21]=z[21] - z[22];
    z[21]=z[21]*z[57];
    z[20]=z[20] + z[21];
    z[20]=z[4]*z[20];
    z[21]=static_cast<T>(11)+ z[11];
    z[19]=z[21]*z[19];
    z[19]=static_cast<T>(11)+ z[19];
    z[19]=z[5]*z[19];
    z[19]= - n<T>(5,2) + z[19];
    z[19]=n<T>(1,4)*z[19] + z[20];
    z[19]=z[4]*z[19];
    z[19]=n<T>(1,4)*z[31] + z[19];
    z[19]=z[4]*z[19];
    z[20]=5*z[36] + z[22];
    z[20]=z[20]*z[65];
    z[20]=z[70] + z[20];
    z[20]=z[4]*z[20];
    z[20]=n<T>(5,4) + z[20];
    z[20]=z[4]*z[20];
    z[20]=n<T>(1,4) + z[20];
    z[20]=z[1]*z[20];
    z[21]= - z[1] - static_cast<T>(1)+ z[4];
    z[21]=z[2]*z[21];
    z[19]=n<T>(1,8)*z[21] + n<T>(1,2)*z[20] + n<T>(1,8) + z[19];
    z[19]=z[7]*z[19];

    r += n<T>(1,16)*z[14] + z[15] + z[16] + n<T>(1,4)*z[17] + z[18] + z[19] + 
      z[23] + z[24] + z[25] + n<T>(1,2)*z[28] + z[46];
 
    return r;
}

template double qqb_2lha_r504(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r504(const std::array<dd_real,30>&);
#endif
