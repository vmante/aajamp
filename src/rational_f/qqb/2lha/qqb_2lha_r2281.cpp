#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2281(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[13];
    z[6]=k[17];
    z[7]=k[8];
    z[8]=k[3];
    z[9]=k[21];
    z[10]=k[4];
    z[11]=k[10];
    z[12]=k[18];
    z[13]=n<T>(1,2)*z[4];
    z[14]=z[13] - 1;
    z[15]=z[2]*z[4];
    z[16]= - z[14]*z[15];
    z[17]= - static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[17]=z[17]*z[15];
    z[18]=npow(z[4],2);
    z[19]=n<T>(1,2)*z[18];
    z[17]= - z[19] + z[17];
    z[20]=z[1]*z[2];
    z[17]=z[17]*z[20];
    z[21]=z[4] - 1;
    z[22]=z[13]*z[21];
    z[23]=npow(z[2],2);
    z[24]= - z[8]*z[23]*z[22];
    z[16]=z[24] + z[16] + z[17];
    z[17]=n<T>(3,2)*z[8];
    z[16]=z[16]*z[17];
    z[24]=npow(z[1],2);
    z[25]=n<T>(1,2)*z[3];
    z[25]= - z[24]*z[25];
    z[14]=z[14]*z[4];
    z[14]=z[14] + n<T>(1,2);
    z[26]=z[14]*z[8];
    z[27]= - z[1]*z[21];
    z[25]=z[25] + z[27] - z[26];
    z[25]=z[7]*z[25];
    z[27]=n<T>(1,2)*z[1];
    z[28]=z[27]*z[18];
    z[27]=z[27]*z[3];
    z[29]=static_cast<T>(2)- z[13];
    z[29]=z[4]*z[29];
    z[25]=z[25] + z[27] - z[28] - n<T>(1,2) + z[29];
    z[25]=z[7]*z[25];
    z[29]=z[21]*z[4];
    z[30]=z[18]*z[8];
    z[31]=z[29] - z[30];
    z[31]=z[9]*z[31];
    z[31]=7*z[18] + 3*z[31];
    z[31]=z[9]*z[31];
    z[32]=z[3]*z[9];
    z[33]= - z[29]*z[32];
    z[34]=npow(z[9],2);
    z[32]=n<T>(3,4)*z[34] - z[32];
    z[32]=z[10]*z[18]*z[32];
    z[31]=z[32] + n<T>(1,4)*z[31] + z[33];
    z[31]=z[10]*z[31];
    z[32]=z[23]*z[1];
    z[33]= - z[2] - z[32];
    z[24]=z[5]*z[24]*z[18]*z[33];
    z[15]=z[15]*z[21];
    z[21]=static_cast<T>(3)+ n<T>(7,3)*z[4];
    z[21]=z[4]*z[21];
    z[21]=z[24] + z[21] - z[15];
    z[24]=static_cast<T>(1)- z[2];
    z[24]=z[2]*z[24]*z[28];
    z[15]=z[24] - z[19] + z[15];
    z[15]=z[1]*z[15];
    z[15]= - z[22] + z[15];
    z[15]=z[6]*z[15];
    z[19]=static_cast<T>(1)- 3*z[4];
    z[13]=z[2]*z[19]*z[13];
    z[13]=z[18] + z[13];
    z[13]=z[13]*z[20];
    z[13]=z[13] + z[15];
    z[15]=static_cast<T>(1)- n<T>(5,4)*z[4];
    z[15]=z[4]*z[15];
    z[15]=n<T>(1,4) + z[15];
    z[15]=z[2]*z[15];
    z[15]=3*z[29] + z[15];
    z[15]=z[2]*z[15];
    z[13]= - n<T>(3,4)*z[18] + z[15] + n<T>(3,2)*z[13];
    z[13]=z[1]*z[13];
    z[14]=z[14]*z[23];
    z[15]=z[17]*z[14];
    z[19]= - n<T>(11,4) + z[4];
    z[19]=z[4]*z[19];
    z[19]=n<T>(7,4) + z[19];
    z[19]=z[2]*z[19];
    z[15]=z[19] + z[15];
    z[15]=z[8]*z[15];
    z[19]= - z[2]*z[26];
    z[19]=z[22] + z[19];
    z[17]=z[9]*z[19]*z[17];
    z[15]=z[17] - z[29] + z[15];
    z[15]=z[9]*z[15];
    z[17]= - z[29] - z[28];
    z[17]=z[17]*z[32];
    z[14]= - z[14] + z[17];
    z[14]=z[14]*z[27];
    z[17]=n<T>(5,6)*z[7] + n<T>(5,6);
    z[17]=z[30]*z[17];
    z[19]=z[10]*z[3];
    z[19]=z[19] + n<T>(11,6);
    z[18]=z[18]*z[19];
    z[19]=z[3]*z[29];
    z[18]=z[19] + z[18];
    z[18]=z[10]*z[18];
    z[19]= - static_cast<T>(1)+ n<T>(11,6)*z[4];
    z[19]=z[4]*z[19];
    z[17]=z[18] + z[19] + z[17];
    z[17]=z[11]*z[17];
    z[18]= - z[7] - 1;
    z[18]=z[30]*z[18];
    z[18]= - z[29] + z[18];
    z[18]=z[12]*z[18];

    r += z[13] + z[14] + z[15] + z[16] + z[17] + n<T>(1,2)*z[18] + n<T>(1,4)*
      z[21] + n<T>(1,3)*z[25] + z[31];
 
    return r;
}

template double qqb_2lha_r2281(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2281(const std::array<dd_real,30>&);
#endif
