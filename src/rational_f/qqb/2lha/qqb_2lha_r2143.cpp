#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2143(const std::array<T,30>& k) {
  T z[28];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[13];
    z[4]=k[17];
    z[5]=k[2];
    z[6]=k[7];
    z[7]=k[8];
    z[8]=k[11];
    z[9]=k[28];
    z[10]=k[4];
    z[11]=k[3];
    z[12]=k[10];
    z[13]=k[18];
    z[14]=n<T>(5,3)*z[7];
    z[15]=n<T>(1,3)*z[12];
    z[16]=z[15] - z[14] + n<T>(1,3) + z[13];
    z[16]=z[11]*z[16];
    z[16]=n<T>(1,3) + z[16];
    z[16]=z[12]*z[16];
    z[17]=z[11] + 1;
    z[17]=z[13]*z[17];
    z[18]=z[5]*z[7];
    z[14]= - n<T>(1,3)*z[18] + z[16] - z[14] + n<T>(5,3) + z[17];
    z[16]=n<T>(1,2)*z[9];
    z[17]=z[9]*z[5];
    z[17]=static_cast<T>(1)+ 9*z[17];
    z[17]=z[17]*z[16];
    z[19]=z[5]*z[4];
    z[19]=z[19] + z[6];
    z[17]=z[17] + z[15] + n<T>(3,2)*z[19];
    z[19]=npow(z[9],2);
    z[20]=z[19] + z[6];
    z[21]=3*z[4];
    z[22]=z[21] + z[2];
    z[23]=3*z[9];
    z[24]= - z[6] - z[23];
    z[24]=z[3]*z[24];
    z[20]=z[24] + static_cast<T>(3)+ z[22] + 3*z[20];
    z[24]=n<T>(1,2)*z[3];
    z[20]=z[20]*z[24];
    z[24]=z[22] + z[6];
    z[25]=z[3]*z[24];
    z[25]= - z[21] + z[25];
    z[26]=n<T>(1,2)*z[10];
    z[25]=z[3]*z[25]*z[26];
    z[27]= - z[4] - z[19];
    z[20]=z[25] + 3*z[27] + z[20];
    z[20]=z[20]*z[26];
    z[25]= - z[16] + static_cast<T>(1)- n<T>(1,2)*z[6];
    z[25]=z[3]*z[25];
    z[17]=z[20] + n<T>(1,2)*z[17] + z[25];
    z[17]=z[10]*z[17];
    z[20]= - z[6] + n<T>(3,4)*z[9];
    z[25]=z[5] - 1;
    z[20]=z[20]*z[25];
    z[20]=z[20] - n<T>(3,2);
    z[20]=z[20]*z[9];
    z[18]= - z[7] + z[18] + n<T>(1,4);
    z[18]=z[18]*z[6];
    z[18]=z[20] + z[18];
    z[20]= - z[5]*z[18];
    z[14]=z[17] + n<T>(1,2)*z[14] + z[20];
    z[14]=z[8]*z[14];
    z[17]=z[16] + z[24];
    z[20]= - static_cast<T>(1)+ z[22];
    z[20]=3*z[20] + z[6];
    z[20]=n<T>(1,2)*z[20] + z[23];
    z[20]=z[3]*z[20];
    z[23]= - n<T>(1,2)*z[4] - z[19];
    z[20]=3*z[23] + z[20];
    z[20]=z[20]*z[26];
    z[17]=z[20] + n<T>(1,2)*z[17] - 2*z[3];
    z[17]=z[3]*z[17];
    z[20]=n<T>(3,4)*z[4];
    z[23]=npow(z[12],2);
    z[23]=n<T>(1,3)*z[23];
    z[17]= - n<T>(9,4)*z[19] + z[23] - z[20] + z[17];
    z[17]=z[10]*z[17];
    z[24]= - z[13] + n<T>(1,3)*z[7];
    z[14]=z[14] + z[17] + n<T>(1,2)*z[24] - z[15] + z[18];
    z[14]=z[8]*z[14];
    z[17]=z[3]*z[22];
    z[16]=z[16] + z[17];
    z[16]=z[3]*z[16];
    z[17]= - z[3]*z[9];
    z[17]=z[19] + z[17];
    z[17]=z[3]*z[17];
    z[18]=z[6]*z[23];
    z[17]=z[18] + n<T>(3,2)*z[17];
    z[17]=z[10]*z[17];
    z[15]=z[6]*z[15];
    z[15]=z[17] + z[15] + z[16];
    z[15]=z[15]*z[26];
    z[16]=z[1]*z[21];
    z[16]=z[16] + z[2] - 1;
    z[17]=z[20] - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[17]=z[3]*z[1]*z[17];
    z[16]=n<T>(1,4)*z[16] + z[17];
    z[16]=z[3]*z[16];

    r += z[14] + z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r2143(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2143(const std::array<dd_real,30>&);
#endif
