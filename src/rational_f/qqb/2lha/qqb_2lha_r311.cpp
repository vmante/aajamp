#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r311(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=k[12];
    z[7]=z[2] - z[1];
    z[8]=static_cast<T>(13)- 5*z[7];
    z[8]=z[2]*z[8];
    z[9]=static_cast<T>(1)- 3*z[2];
    z[9]=z[4]*z[9];
    z[8]=z[8] + z[9];
    z[9]=n<T>(1,2)*z[5];
    z[10]= - z[4] + n<T>(3,2) - z[2];
    z[10]=z[4]*z[10];
    z[10]=n<T>(3,2)*z[2] + z[10];
    z[10]=z[10]*z[9];
    z[11]=n<T>(1,2)*z[4];
    z[12]=static_cast<T>(1)- z[11];
    z[12]=z[4]*z[12];
    z[10]=z[10] + n<T>(11,4)*z[2] + z[12];
    z[10]=z[5]*z[10];
    z[8]=n<T>(1,4)*z[8] + z[10];
    z[10]=npow(z[2],2);
    z[12]=n<T>(1,2)*z[10];
    z[13]=z[2] + z[11];
    z[13]=z[4]*z[13];
    z[13]=z[12] + z[13];
    z[13]=z[5]*z[13];
    z[14]=2*z[2] + z[11];
    z[14]=z[4]*z[14];
    z[10]=z[13] + n<T>(3,2)*z[10] + z[14];
    z[10]=z[5]*z[10];
    z[7]=static_cast<T>(3)- z[7];
    z[7]=z[7]*z[12];
    z[12]=n<T>(1,2)*z[2];
    z[13]=static_cast<T>(1)- z[12];
    z[13]=z[4]*z[2]*z[13];
    z[7]=z[10] + z[7] + z[13];
    z[7]=z[3]*z[7];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - n<T>(3,2) - z[4];
    z[8]=z[4]*z[8];
    z[8]= - static_cast<T>(1)+ z[8];
    z[8]=z[8]*z[9];
    z[9]= - static_cast<T>(1)- z[11];
    z[9]=z[4]*z[9];
    z[8]=z[8] - n<T>(7,12) + z[9];
    z[8]=z[5]*z[8];
    z[9]= - z[4] - z[12] + n<T>(1,3) + n<T>(1,2)*z[1];
    z[8]=n<T>(1,2)*z[9] + z[8];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]=static_cast<T>(1)+ z[5];
    z[8]=z[5]*z[6]*z[8];

    r += z[7] + n<T>(1,6)*z[8];
 
    return r;
}

template double qqb_2lha_r311(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r311(const std::array<dd_real,30>&);
#endif
