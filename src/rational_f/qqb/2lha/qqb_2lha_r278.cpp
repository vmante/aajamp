#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r278(const std::array<T,30>& k) {
  T z[52];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[17];
    z[6]=k[12];
    z[7]=k[2];
    z[8]=k[3];
    z[9]=k[4];
    z[10]=k[5];
    z[11]=k[9];
    z[12]=3*z[3];
    z[13]=n<T>(1,2)*z[3];
    z[14]=static_cast<T>(3)+ z[13];
    z[14]=z[14]*z[12];
    z[15]=n<T>(1,4)*z[8];
    z[16]= - static_cast<T>(27)+ n<T>(29,2)*z[3];
    z[16]=z[3]*z[16]*z[15];
    z[14]=z[14] + z[16];
    z[16]=z[8] - 1;
    z[17]=3*z[2];
    z[18]=z[17]*z[3]*z[16];
    z[14]=n<T>(1,2)*z[14] + z[18];
    z[14]=z[2]*z[14];
    z[18]=5*z[3];
    z[19]= - static_cast<T>(5)- n<T>(11,2)*z[3];
    z[19]=z[19]*z[18];
    z[20]=npow(z[3],2);
    z[21]=z[20]*z[8];
    z[19]=z[19] - n<T>(49,2)*z[21];
    z[14]=n<T>(1,8)*z[19] + z[14];
    z[14]=z[2]*z[14];
    z[19]=5*z[8];
    z[22]=z[19]*z[20];
    z[23]=n<T>(101,2)*z[20] + z[22];
    z[14]=n<T>(1,16)*z[23] + z[14];
    z[14]=z[2]*z[14];
    z[23]=z[13] - 1;
    z[24]=z[23]*z[13];
    z[24]=z[24] - z[21];
    z[25]=z[20]*z[2];
    z[26]= - n<T>(17,2) + z[17];
    z[26]=z[26]*z[25];
    z[26]=n<T>(27,4)*z[20] + z[26];
    z[27]=npow(z[2],2);
    z[26]=z[26]*z[27];
    z[26]=n<T>(5,8)*z[20] + z[26];
    z[28]=n<T>(1,4)*z[1];
    z[26]=z[26]*z[28];
    z[14]=z[26] + n<T>(5,8)*z[24] + z[14];
    z[14]=z[1]*z[14];
    z[24]=z[12] - 1;
    z[26]=n<T>(7,2)*z[3];
    z[29]=z[24]*z[26];
    z[30]=n<T>(1,2)*z[8];
    z[31]= - static_cast<T>(5)+ n<T>(39,2)*z[3];
    z[31]=z[3]*z[31];
    z[31]= - static_cast<T>(3)+ z[31];
    z[31]=z[31]*z[30];
    z[29]=z[31] - static_cast<T>(17)+ z[29];
    z[29]=z[29]*z[15];
    z[31]=n<T>(39,8) + z[3];
    z[31]=z[3]*z[31];
    z[29]=z[29] + static_cast<T>(7)+ z[31];
    z[31]=n<T>(3,2)*z[8];
    z[32]=n<T>(13,2)*z[3];
    z[33]= - static_cast<T>(1)+ z[32];
    z[33]=z[33]*z[31];
    z[34]=z[3] - n<T>(3,4);
    z[35]=z[3]*z[34];
    z[33]=z[33] + n<T>(33,2) + z[35];
    z[33]=z[8]*z[33];
    z[35]= - static_cast<T>(9)- z[3];
    z[35]=z[3]*z[35];
    z[33]=z[33] - static_cast<T>(15)+ z[35];
    z[35]=z[30] - 1;
    z[35]=z[35]*z[8];
    z[35]=z[35] + n<T>(1,2);
    z[36]=z[2]*z[35];
    z[33]=n<T>(1,4)*z[33] + 9*z[36];
    z[33]=z[2]*z[33];
    z[29]=n<T>(1,2)*z[29] + z[33];
    z[29]=z[2]*z[29];
    z[33]=9*z[3];
    z[36]= - n<T>(5,2) - z[33];
    z[36]=z[36]*z[26];
    z[36]= - n<T>(5,2)*z[21] - static_cast<T>(13)+ z[36];
    z[36]=z[36]*z[30];
    z[37]= - static_cast<T>(79)- 95*z[3];
    z[37]=z[3]*z[37];
    z[36]=z[36] + static_cast<T>(3)+ n<T>(1,8)*z[37];
    z[29]=n<T>(1,4)*z[36] + z[29];
    z[29]=z[2]*z[29];
    z[36]=z[23]*z[3];
    z[37]= - z[36] + z[21];
    z[37]=z[8]*z[37];
    z[38]= - static_cast<T>(5)- n<T>(233,4)*z[3];
    z[38]=z[3]*z[38];
    z[37]=15*z[37] - static_cast<T>(3)+ z[38];
    z[14]=z[14] + n<T>(1,16)*z[37] + z[29];
    z[14]=z[1]*z[14];
    z[29]=static_cast<T>(1)+ n<T>(25,4)*z[3];
    z[29]=z[29]*z[18];
    z[37]=n<T>(3,2)*z[36] - z[21];
    z[37]=z[37]*z[19];
    z[29]=z[37] + static_cast<T>(3)+ z[29];
    z[29]=z[8]*z[29];
    z[37]=static_cast<T>(47)+ 19*z[3];
    z[37]=z[3]*z[37];
    z[37]= - static_cast<T>(19)+ z[37];
    z[29]=n<T>(3,4)*z[37] + z[29];
    z[37]=n<T>(1,2) + z[12];
    z[37]=z[3]*z[37];
    z[38]= - z[8] + static_cast<T>(5)+ n<T>(9,2)*z[3];
    z[38]=z[8]*z[38];
    z[37]=z[38] - n<T>(13,4) + z[37];
    z[37]=z[8]*z[37];
    z[38]=z[12] + 5;
    z[39]= - z[3]*z[38];
    z[37]=z[37] - n<T>(3,4) + z[39];
    z[39]=z[3] + 3;
    z[39]=z[39]*z[3];
    z[40]= - n<T>(9,4)*z[8] + n<T>(27,2) + z[39];
    z[40]=z[40]*z[30];
    z[40]=z[40] - n<T>(81,8) - z[39];
    z[40]=z[8]*z[40];
    z[41]=static_cast<T>(9)+ z[39];
    z[40]=n<T>(1,2)*z[41] + z[40];
    z[40]=z[2]*z[40];
    z[37]=n<T>(1,2)*z[37] + z[40];
    z[40]=n<T>(1,2)*z[2];
    z[37]=z[37]*z[40];
    z[41]= - n<T>(9,16) - z[3];
    z[41]=z[41]*z[13];
    z[42]=static_cast<T>(5)+ n<T>(31,2)*z[3];
    z[42]=z[3]*z[42];
    z[42]=static_cast<T>(37)+ z[42];
    z[42]=z[8]*z[42];
    z[41]=n<T>(1,16)*z[42] - static_cast<T>(4)+ z[41];
    z[41]=z[8]*z[41];
    z[42]=n<T>(1,4) + z[12];
    z[42]=z[3]*z[42];
    z[42]=n<T>(1,8) + z[42];
    z[37]=z[37] + n<T>(1,4)*z[42] + z[41];
    z[37]=z[2]*z[37];
    z[14]=z[14] + n<T>(1,8)*z[29] + z[37];
    z[14]=z[1]*z[14];
    z[29]=static_cast<T>(163)+ 117*z[3];
    z[37]=n<T>(1,4)*z[3];
    z[29]=z[29]*z[37];
    z[41]=static_cast<T>(7)+ z[29];
    z[41]=z[2]*z[41];
    z[29]=z[41] - z[29] - 45*z[21];
    z[41]=z[40] - 1;
    z[25]=z[41]*z[25];
    z[41]=n<T>(1,2)*z[20];
    z[25]=z[25] + z[41];
    z[42]=z[1]*z[2];
    z[43]=z[25]*z[42];
    z[44]=static_cast<T>(21)+ n<T>(47,2)*z[3];
    z[44]=z[2]*z[44]*z[37];
    z[45]= - n<T>(7,2) - 10*z[3];
    z[45]=z[3]*z[45];
    z[44]=z[45] + z[44];
    z[44]=z[2]*z[44];
    z[44]=n<T>(7,2)*z[43] + n<T>(115,16)*z[20] + z[44];
    z[44]=z[1]*z[44];
    z[29]=n<T>(1,4)*z[29] + z[44];
    z[29]=z[1]*z[29];
    z[44]=7*z[3];
    z[45]=n<T>(171,4) - z[44];
    z[45]=z[3]*z[45];
    z[45]=z[45] + n<T>(281,8)*z[21];
    z[45]=z[45]*z[15];
    z[29]=z[29] + static_cast<T>(3)+ z[45];
    z[29]=z[1]*z[29];
    z[45]=n<T>(25,2) - z[3];
    z[45]=z[45]*z[26];
    z[45]= - static_cast<T>(15)+ z[45];
    z[46]= - n<T>(175,8) + 17*z[3];
    z[46]=z[3]*z[46];
    z[46]=z[46] - n<T>(61,4)*z[21];
    z[46]=z[8]*z[46];
    z[45]=n<T>(1,2)*z[45] + z[46];
    z[45]=z[8]*z[45];
    z[45]=n<T>(15,2) + z[45];
    z[46]=z[2]*z[3];
    z[47]= - z[46] + z[3];
    z[48]=n<T>(7,4)*z[3];
    z[49]=z[48] + 1;
    z[47]=z[49]*z[47];
    z[49]= - z[1]*z[25];
    z[47]=z[49] + n<T>(7,4)*z[21] + z[47];
    z[47]=z[1]*z[47];
    z[49]= - z[18] - n<T>(19,4)*z[21];
    z[49]=z[8]*z[49];
    z[49]= - static_cast<T>(1)+ z[49];
    z[47]=n<T>(1,2)*z[49] + z[47];
    z[47]=z[1]*z[47];
    z[32]=static_cast<T>(9)- z[32];
    z[32]=z[3]*z[32];
    z[32]=z[32] + n<T>(13,2)*z[21];
    z[32]=z[8]*z[32];
    z[24]= - 3*z[24] + z[32];
    z[24]=z[8]*z[24];
    z[24]= - static_cast<T>(3)+ z[24];
    z[24]=n<T>(1,4)*z[24] + z[47];
    z[32]=n<T>(1,2)*z[1];
    z[24]=z[24]*z[32];
    z[47]= - static_cast<T>(1)+ n<T>(5,4)*z[3];
    z[47]=z[3]*z[47];
    z[47]=z[47] - n<T>(5,8)*z[21];
    z[47]=z[47]*z[30];
    z[49]=static_cast<T>(1)- n<T>(5,16)*z[3];
    z[49]=z[3]*z[49];
    z[47]=z[47] - n<T>(3,16) + z[49];
    z[47]=z[8]*z[47];
    z[34]= - n<T>(1,2)*z[34] + z[47];
    z[34]=z[8]*z[34];
    z[24]=z[24] - n<T>(3,16) + z[34];
    z[24]=z[4]*z[24];
    z[24]=n<T>(7,2)*z[24] + n<T>(1,4)*z[45] + z[29];
    z[24]=z[4]*z[24];
    z[29]=z[30]*z[20];
    z[34]=z[3] - n<T>(3,2);
    z[45]=z[3]*z[34];
    z[45]=z[45] - z[29];
    z[45]=z[45]*z[30];
    z[47]= - static_cast<T>(1)+ z[21];
    z[47]=z[47]*z[28];
    z[45]=z[47] - static_cast<T>(1)+ z[45];
    z[45]=z[1]*z[45];
    z[47]= - static_cast<T>(5)+ z[3];
    z[47]=z[47]*z[13];
    z[47]=z[47] - z[21];
    z[47]=z[47]*z[30];
    z[47]= - static_cast<T>(1)+ z[47];
    z[45]=n<T>(3,2)*z[47] + z[45];
    z[45]=z[1]*z[45];
    z[47]=z[12] + z[29];
    z[47]=z[8]*z[47];
    z[47]=n<T>(13,4) + z[47];
    z[49]=z[3]*z[30];
    z[28]=z[28] + static_cast<T>(1)+ z[49];
    z[28]=z[1]*z[28];
    z[28]=n<T>(1,2)*z[47] + z[28];
    z[28]=z[1]*z[28];
    z[26]=z[26] + z[21];
    z[26]=z[8]*z[26];
    z[26]=n<T>(11,4) + z[26];
    z[26]=n<T>(1,2)*z[26] + z[28];
    z[26]=z[1]*z[26];
    z[28]=z[3] + z[29];
    z[28]=z[8]*z[28];
    z[28]=n<T>(1,2) + z[28];
    z[47]=n<T>(1,4)*z[10];
    z[28]=z[28]*z[47];
    z[49]=z[3] + n<T>(3,8)*z[21];
    z[49]=z[8]*z[49];
    z[26]=z[28] + z[26] + n<T>(5,8) + z[49];
    z[26]=z[6]*z[26];
    z[28]= - n<T>(27,2) + z[3];
    z[28]=z[28]*z[13];
    z[28]=z[28] - 3*z[21];
    z[28]=z[28]*z[15];
    z[49]= - z[33] - z[22];
    z[49]=z[49]*z[15];
    z[49]= - static_cast<T>(1)+ z[49];
    z[49]=z[49]*z[47];
    z[26]=n<T>(1,2)*z[26] + z[49] + z[45] - static_cast<T>(1)+ z[28];
    z[26]=z[6]*z[26];
    z[28]=z[12] + 7;
    z[37]=z[28]*z[37];
    z[45]=3*z[8];
    z[49]= - static_cast<T>(1)+ n<T>(3,8)*z[8];
    z[49]=z[49]*z[45];
    z[28]=z[28]*z[3];
    z[49]=z[49] + n<T>(53,8) + z[28];
    z[49]=z[49]*z[30];
    z[49]=z[49] - n<T>(35,8) - z[28];
    z[49]=z[49]*z[30];
    z[39]=z[39] + 3;
    z[50]=z[39]*z[8];
    z[51]=3*z[39];
    z[50]= - z[51] + z[50];
    z[50]=z[8]*z[50];
    z[50]=z[51] + z[50];
    z[50]=z[8]*z[50];
    z[50]=z[50] - z[39];
    z[50]=z[2]*z[50];
    z[37]=n<T>(1,4)*z[50] + z[49] + static_cast<T>(1)+ z[37];
    z[37]=z[2]*z[37];
    z[49]=static_cast<T>(9)+ n<T>(5,8)*z[3];
    z[50]= - static_cast<T>(1)- n<T>(5,32)*z[3];
    z[50]=z[8]*z[50];
    z[49]=n<T>(1,4)*z[49] + z[50];
    z[49]=z[8]*z[49];
    z[48]=z[48] + 3;
    z[48]=z[48]*z[3];
    z[50]= - n<T>(39,16) + z[48];
    z[49]=n<T>(1,2)*z[50] + z[49];
    z[49]=z[8]*z[49];
    z[48]= - n<T>(1,16) - z[48];
    z[37]=z[37] + n<T>(1,2)*z[48] + z[49];
    z[37]=z[2]*z[37];
    z[48]=static_cast<T>(69)- 19*z[8];
    z[48]=z[8]*z[48];
    z[48]= - static_cast<T>(93)+ z[48];
    z[48]=z[8]*z[48];
    z[48]=static_cast<T>(55)+ z[48];
    z[48]=z[48]*z[15];
    z[48]= - static_cast<T>(3)+ z[48];
    z[48]=z[2]*z[48];
    z[49]= - static_cast<T>(45)+ 17*z[8];
    z[49]=z[8]*z[49];
    z[49]=static_cast<T>(39)+ z[49];
    z[49]=z[8]*z[49];
    z[49]= - static_cast<T>(11)+ z[49];
    z[48]=n<T>(1,4)*z[49] + z[48];
    z[48]=z[2]*z[48];
    z[45]= - n<T>(29,4) + z[45];
    z[45]=z[8]*z[45];
    z[45]=n<T>(17,4) + z[45];
    z[45]=n<T>(1,2)*z[45] + z[48];
    z[15]=z[15] - 1;
    z[15]=z[15]*z[8];
    z[15]=z[15] + n<T>(3,2);
    z[48]=z[15]*z[8];
    z[48]=z[48] - 1;
    z[48]=z[48]*z[8];
    z[48]=z[48] + n<T>(1,4);
    z[48]=z[48]*z[2];
    z[49]=z[8] - 3;
    z[49]=z[49]*z[8];
    z[49]=z[49] + 3;
    z[50]=z[49]*z[8];
    z[50]=z[50] - 1;
    z[51]=n<T>(1,4)*z[50] + z[48];
    z[51]=z[2]*z[51];
    z[51]=n<T>(1,4)*z[35] + z[51];
    z[51]=z[11]*z[51];
    z[45]=z[51] + n<T>(1,4)*z[45];
    z[45]=z[2]*z[45];
    z[51]= - static_cast<T>(5)+ n<T>(7,2)*z[8];
    z[51]=z[8]*z[51];
    z[51]= - n<T>(1,2) + z[51];
    z[51]=z[51]*z[30];
    z[51]=static_cast<T>(1)+ z[51];
    z[17]=z[50]*z[17];
    z[17]=n<T>(1,2)*z[51] + z[17];
    z[17]=z[2]*z[17];
    z[51]=static_cast<T>(2)- n<T>(11,8)*z[8];
    z[51]=z[8]*z[51];
    z[17]=z[17] - n<T>(5,8) + z[51];
    z[17]=z[2]*z[17];
    z[31]=static_cast<T>(5)- z[31];
    z[17]=n<T>(1,4)*z[31] + z[17];
    z[17]=z[17]*z[42];
    z[31]= - n<T>(1,8)*z[50] + z[48];
    z[31]=z[2]*z[31];
    z[31]= - n<T>(1,8)*z[35] + z[31];
    z[27]=z[9]*z[31]*z[27];
    z[17]=3*z[27] + z[17] + z[45];
    z[17]=z[9]*z[17];
    z[27]=n<T>(3,4)*z[3];
    z[31]=static_cast<T>(5)+ z[44];
    z[31]=z[31]*z[27];
    z[42]= - n<T>(13,2) - 4*z[3];
    z[42]=z[42]*z[46];
    z[31]=z[31] + z[42];
    z[31]=z[2]*z[31];
    z[31]= - n<T>(5,2)*z[43] - n<T>(5,4)*z[20] + z[31];
    z[31]=z[1]*z[31];
    z[42]=n<T>(7,8) + z[3];
    z[42]=z[42]*z[12];
    z[33]= - n<T>(79,4) - z[33];
    z[33]=z[3]*z[33];
    z[33]= - static_cast<T>(13)+ z[33];
    z[33]=z[33]*z[40];
    z[31]=z[31] + z[33] - n<T>(5,4) + z[42];
    z[31]=z[1]*z[31];
    z[33]= - n<T>(29,8) - 2*z[3];
    z[33]=z[3]*z[33];
    z[31]=z[31] - n<T>(13,8) + z[33];
    z[31]=z[1]*z[31];
    z[27]=static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[46];
    z[25]=z[25]*z[32];
    z[32]=z[3] + n<T>(1,2);
    z[32]=z[32]*z[3];
    z[25]=z[25] - z[32] + z[27];
    z[25]=z[1]*z[25];
    z[27]=z[38]*z[13];
    z[27]=static_cast<T>(1)+ z[27];
    z[25]=n<T>(1,2)*z[27] + z[25];
    z[25]=z[5]*z[25]*npow(z[1],2);
    z[25]=z[31] + n<T>(1,2)*z[25];
    z[25]=z[5]*z[25];
    z[27]= - static_cast<T>(47)+ n<T>(27,2)*z[3];
    z[27]=z[27]*z[13];
    z[31]=n<T>(47,2) - 27*z[3];
    z[31]=z[3]*z[31];
    z[31]=z[31] + n<T>(27,2)*z[21];
    z[31]=z[31]*z[30];
    z[27]=z[31] + static_cast<T>(5)+ z[27];
    z[27]=z[27]*z[30];
    z[27]=z[27] - static_cast<T>(5)+ n<T>(47,8)*z[3];
    z[27]=z[8]*z[27];
    z[12]=z[23]*z[12];
    z[23]=static_cast<T>(1)- n<T>(3,2)*z[3];
    z[23]=z[3]*z[23];
    z[23]=z[23] + z[29];
    z[23]=z[8]*z[23];
    z[12]=z[23] + n<T>(1,2) + z[12];
    z[12]=z[8]*z[12];
    z[23]=static_cast<T>(3)- z[13];
    z[23]=z[3]*z[23];
    z[12]=z[12] - n<T>(3,2) + z[23];
    z[12]=z[8]*z[12];
    z[12]=z[12] - z[34];
    z[12]=z[8]*z[12];
    z[12]= - n<T>(1,2) + z[12];
    z[12]=z[4]*z[12];
    z[12]=n<T>(7,4)*z[12] + n<T>(5,2) + z[27];
    z[12]=z[4]*z[12];
    z[23]=n<T>(59,4) - z[18];
    z[23]=z[23]*z[13];
    z[22]=z[23] + z[22];
    z[22]=z[8]*z[22];
    z[23]=static_cast<T>(19)- 39*z[3];
    z[22]=n<T>(1,8)*z[23] + z[22];
    z[22]=z[8]*z[22];
    z[22]= - n<T>(19,8) + z[22];
    z[12]=n<T>(1,2)*z[22] + z[12];
    z[12]=z[12]*z[47];
    z[22]=z[3] + 1;
    z[23]=z[11]*z[22];
    z[23]=z[23] + n<T>(27,8) + z[3];
    z[23]=z[3]*z[23];
    z[20]=z[1]*z[20];
    z[20]= - n<T>(35,8)*z[20] + n<T>(23,4)*z[21] + z[23];
    z[13]=z[22]*z[13];
    z[23]=z[1]*z[41];
    z[23]= - z[32] + z[23];
    z[23]=z[1]*z[23];
    z[13]=z[13] + z[23];
    z[13]=z[5]*z[13];
    z[13]=z[13] + z[32];
    z[13]=z[1]*z[13];
    z[22]= - z[3]*z[22];
    z[13]=z[22] + z[13];
    z[13]=z[5]*z[13];
    z[21]= - z[3] - z[21];
    z[21]=z[10]*z[8]*z[21];
    z[13]=n<T>(19,16)*z[21] + n<T>(1,2)*z[20] + z[13];
    z[13]=z[7]*z[13];
    z[20]= - static_cast<T>(1)- n<T>(29,4)*z[3];
    z[18]=z[20]*z[18];
    z[20]= - z[36] + z[29];
    z[19]=z[20]*z[19];
    z[18]=z[19] - static_cast<T>(3)+ z[18];
    z[18]=z[8]*z[18];
    z[19]= - n<T>(181,2) + z[3];
    z[19]=z[3]*z[19];
    z[19]=static_cast<T>(21)+ z[19];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[8]*z[18];
    z[19]=n<T>(15,16) + z[3];
    z[19]=z[3]*z[19];
    z[18]=n<T>(1,8)*z[18] - n<T>(45,16) + z[19];
    z[19]=z[28] + 5;
    z[20]=z[8]*z[19]*z[49];
    z[19]=z[20] - z[19];
    z[15]=z[8]*z[39]*z[15];
    z[15]=z[15] - z[39];
    z[15]=z[8]*z[15];
    z[15]=n<T>(1,4)*z[39] + z[15];
    z[15]=z[2]*z[15];
    z[15]=n<T>(1,4)*z[19] + z[15];
    z[15]=z[2]*z[15];
    z[19]=z[44] + 13;
    z[19]=z[19]*z[3];
    z[19]=z[19] + 7;
    z[19]=z[19]*z[35];
    z[15]=n<T>(1,4)*z[19] + z[15];
    z[15]=z[2]*z[15];
    z[19]=z[3] + n<T>(3,2);
    z[19]=z[19]*z[3];
    z[19]=z[19] + n<T>(1,2);
    z[16]=z[19]*z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[11]*z[15];

    r += z[12] + n<T>(1,4)*z[13] + z[14] + z[15] + z[17] + n<T>(1,2)*z[18] + 
      z[24] + z[25] + n<T>(5,4)*z[26] + z[37];
 
    return r;
}

template double qqb_2lha_r278(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r278(const std::array<dd_real,30>&);
#endif
