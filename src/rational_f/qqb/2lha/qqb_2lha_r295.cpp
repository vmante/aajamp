#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r295(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[3];
    z[2]=k[10];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[12];
    z[7]=npow(z[2],2);
    z[8]=z[7]*z[4];
    z[9]=z[2] + 1;
    z[10]=z[9]*z[2];
    z[10]= - z[10] - z[8];
    z[10]=z[6]*z[10];
    z[8]=z[5]*z[8];
    z[8]=z[10] + z[8];
    z[9]=z[9]*z[3];
    z[10]= - z[2]*z[9];
    z[7]=z[7] + z[10];
    z[7]=z[4]*z[3]*z[7];
    z[10]= - static_cast<T>(2)- z[2];
    z[10]=z[2]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[1]*z[10]*npow(z[3],2);
    z[7]=z[7] + z[10];
    z[7]= - z[9] + 2*z[7] + n<T>(1,2)*z[8];

    r += n<T>(3,5)*z[7];
 
    return r;
}

template double qqb_2lha_r295(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r295(const std::array<dd_real,30>&);
#endif
