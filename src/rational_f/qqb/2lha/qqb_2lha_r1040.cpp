#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1040(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[3];
    z[2]=k[13];
    z[3]=k[4];
    z[4]=k[11];
    z[5]=k[9];
    z[6]=z[4]*z[2];
    z[7]=npow(z[2],2);
    z[8]= - z[7] + z[6];
    z[9]=n<T>(1,2)*z[4];
    z[8]=z[8]*z[9];
    z[10]=npow(z[2],3);
    z[11]= - z[1]*z[10];
    z[8]=z[11] - z[7] + z[8];
    z[8]=z[1]*z[8];
    z[6]= - n<T>(1,2)*z[6] + z[8];
    z[8]=z[3]*z[7];
    z[8]= - n<T>(1,2)*z[8] - n<T>(3,4)*z[2];
    z[11]=z[2] - 1;
    z[12]=npow(z[4],2);
    z[8]=z[11]*z[12]*z[8];
    z[7]=z[7]*z[9];
    z[7]= - z[10] + z[7];
    z[7]=z[1]*z[4]*z[7];
    z[7]=z[7] + z[8];
    z[7]=z[3]*z[7];
    z[8]= - z[1]*z[12];
    z[8]=z[4] + z[8];
    z[8]=z[5]*z[8];
    z[6]=n<T>(1,4)*z[8] + n<T>(1,2)*z[6] + z[7];

    r += n<T>(1,2)*z[6];
 
    return r;
}

template double qqb_2lha_r1040(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1040(const std::array<dd_real,30>&);
#endif
