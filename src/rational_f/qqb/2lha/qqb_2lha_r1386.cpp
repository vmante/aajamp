#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1386(const std::array<T,30>& k) {
  T z[62];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[13];
    z[8]=k[3];
    z[9]=k[6];
    z[10]=3*z[1];
    z[11]=z[10] - n<T>(1,3);
    z[12]=npow(z[1],2);
    z[13]=z[11]*z[12];
    z[14]=z[1] + n<T>(1,3);
    z[15]=npow(z[1],3);
    z[16]=z[15]*z[4];
    z[17]= - z[14]*z[16];
    z[18]=z[15]*z[9];
    z[13]=z[17] + z[13] - z[18];
    z[17]=n<T>(1,2)*z[4];
    z[13]=z[13]*z[17];
    z[19]=z[1] + 3;
    z[20]=z[19]*z[1];
    z[21]=z[20] + 3;
    z[21]=z[21]*z[1];
    z[22]=z[21] + 1;
    z[23]=z[22]*z[6];
    z[24]=n<T>(5,3) + z[10];
    z[24]=z[1]*z[24];
    z[24]=z[24] - 3*z[23];
    z[25]=n<T>(1,2)*z[6];
    z[26]=z[22]*z[25];
    z[27]=z[1] + n<T>(3,2);
    z[28]=z[27]*z[1];
    z[29]=z[28] + n<T>(1,2);
    z[26]=z[26] - z[29];
    z[30]=3*z[9];
    z[26]=z[26]*z[30];
    z[13]=z[13] + n<T>(1,2)*z[24] + z[26];
    z[24]=npow(z[1],4);
    z[26]=z[24]*z[4];
    z[31]=z[15] - n<T>(1,3)*z[26];
    z[31]=z[4]*z[31];
    z[31]= - z[12] + z[31];
    z[31]=z[3]*z[31];
    z[13]=n<T>(1,2)*z[13] + z[31];
    z[31]=n<T>(1,4)*z[3];
    z[13]=z[13]*z[31];
    z[32]=static_cast<T>(5)+ z[1];
    z[32]=z[1]*z[32];
    z[32]=static_cast<T>(7)+ z[32];
    z[32]=z[1]*z[32];
    z[32]= - n<T>(3,2)*z[23] + static_cast<T>(3)+ z[32];
    z[32]=z[32]*z[25];
    z[33]=z[1] + 1;
    z[34]=z[33]*z[10];
    z[34]= - z[23] + static_cast<T>(1)+ z[34];
    z[35]=n<T>(1,2)*z[9];
    z[34]=z[34]*z[35];
    z[36]=n<T>(1,4) - z[1];
    z[36]=z[1]*z[36];
    z[32]=z[34] + z[32] - n<T>(1,4) + z[36];
    z[34]=n<T>(1,4)*z[9];
    z[32]=z[32]*z[34];
    z[36]=z[1] - n<T>(1,2);
    z[37]=n<T>(1,3)*z[1];
    z[38]=z[36]*z[37];
    z[38]=n<T>(7,8) + z[38];
    z[38]=z[1]*z[38];
    z[38]=n<T>(3,8) + z[38];
    z[39]=n<T>(1,2)*z[1];
    z[40]=z[39] + 1;
    z[40]=z[40]*z[1];
    z[41]= - n<T>(5,4) - z[40];
    z[41]=z[41]*z[37];
    z[41]= - n<T>(1,2) + z[41];
    z[41]=z[1]*z[41];
    z[23]=n<T>(3,16)*z[23] - n<T>(1,4) + z[41];
    z[23]=z[6]*z[23];
    z[41]=n<T>(1,2)*z[12];
    z[42]=z[33]*z[1];
    z[43]= - n<T>(7,6) - z[42];
    z[43]=z[4]*z[43]*z[41];
    z[44]=z[1] - 1;
    z[45]=z[1]*z[44];
    z[45]=n<T>(7,12) + z[45];
    z[45]=z[1]*z[45];
    z[43]=z[45] + z[43];
    z[43]=z[4]*z[43];
    z[13]=z[13] + n<T>(1,4)*z[43] + z[32] + n<T>(1,2)*z[38] + z[23];
    z[13]=z[3]*z[13];
    z[23]=n<T>(1,4)*z[6];
    z[32]= - z[33]*z[15]*z[23];
    z[38]=z[1] + n<T>(3,4);
    z[43]=z[38]*z[12];
    z[32]=z[43] + z[32];
    z[32]=z[6]*z[32];
    z[43]=n<T>(1,2)*z[8];
    z[45]=z[43] - 1;
    z[46]= - z[8]*z[45];
    z[46]= - n<T>(1,2) + z[46];
    z[32]=n<T>(1,2)*z[46] + z[32];
    z[32]=z[5]*z[32];
    z[46]=z[43] - z[1];
    z[47]=z[46] - n<T>(5,4);
    z[47]=z[47]*z[8];
    z[48]=n<T>(3,2)*z[1];
    z[49]= - z[47] - static_cast<T>(1)- z[48];
    z[49]=z[8]*z[49];
    z[40]=z[40] + n<T>(1,2);
    z[50]=z[12]*z[6];
    z[51]=z[40]*z[50];
    z[52]=z[38]*z[1];
    z[53]=n<T>(1,2) - z[52];
    z[53]=z[1]*z[53];
    z[32]=z[32] + z[51] + z[49] + n<T>(1,4) + z[53];
    z[32]=z[5]*z[32];
    z[49]=n<T>(1,4)*z[8];
    z[51]=z[38] - z[49];
    z[51]=z[51]*z[8];
    z[51]=z[51] - n<T>(3,2)*z[29];
    z[51]=z[51]*z[8];
    z[51]=z[51] + n<T>(1,4);
    z[53]=z[1] + n<T>(9,4);
    z[53]=z[53]*z[1];
    z[54]=n<T>(3,2) + z[53];
    z[54]=z[1]*z[54];
    z[54]=z[54] + z[51];
    z[54]=z[8]*z[54];
    z[22]=z[1]*z[22];
    z[22]=z[32] - n<T>(1,4)*z[22] + z[54];
    z[32]=n<T>(1,2)*z[7];
    z[22]=z[22]*z[32];
    z[54]=z[48] - 1;
    z[55]=z[54]*z[50];
    z[56]=static_cast<T>(1)- z[6];
    z[18]=z[56]*z[18];
    z[18]=z[18] - z[15] + z[55];
    z[18]=z[6]*z[18];
    z[18]=z[12] + z[18];
    z[18]=z[18]*z[34];
    z[20]=n<T>(3,2) + z[20];
    z[20]=z[6]*z[20]*z[41];
    z[55]= - n<T>(11,4) - z[1];
    z[55]=z[1]*z[55];
    z[55]= - static_cast<T>(1)+ z[55];
    z[55]=z[1]*z[55];
    z[20]=z[55] + z[20];
    z[20]=z[6]*z[20];
    z[20]=z[20] - n<T>(1,2)*z[27] - z[47];
    z[47]=n<T>(1,2)*z[5];
    z[20]=z[20]*z[47];
    z[55]=z[39] - 1;
    z[50]= - z[55]*z[50];
    z[21]= - n<T>(3,2) - z[21];
    z[21]=z[1]*z[21];
    z[21]=z[21] + z[50];
    z[21]=z[21]*z[23];
    z[23]=n<T>(13,8) + z[53];
    z[23]=z[1]*z[23];
    z[18]=z[22] + z[20] + z[18] + z[21] + z[23] + z[51];
    z[18]=z[18]*z[32];
    z[20]= - static_cast<T>(11)- z[1];
    z[20]=z[1]*z[20];
    z[20]= - n<T>(67,4) + z[20];
    z[20]=z[20]*z[37];
    z[20]= - n<T>(5,2) + z[20];
    z[21]=n<T>(1,3)*z[8];
    z[22]= - z[21] - n<T>(1,3) + z[39];
    z[22]=z[8]*z[22];
    z[23]=n<T>(43,2) + 23*z[1];
    z[22]=n<T>(1,12)*z[23] + z[22];
    z[22]=z[8]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[8]*z[20];
    z[22]=n<T>(3,2) + z[28];
    z[22]=z[1]*z[22];
    z[22]=n<T>(1,2) + z[22];
    z[20]=n<T>(1,4)*z[22] + z[20];
    z[20]=z[6]*z[20];
    z[22]=n<T>(35,4) + z[1];
    z[22]=z[1]*z[22];
    z[22]=n<T>(49,4) + z[22];
    z[22]=z[22]*z[37];
    z[22]=n<T>(7,4) + z[22];
    z[23]=11*z[1];
    z[28]= - static_cast<T>(25)+ z[23];
    z[28]=z[1]*z[28];
    z[28]= - n<T>(89,2) + z[28];
    z[32]=static_cast<T>(7)- n<T>(19,2)*z[1];
    z[32]=n<T>(1,2)*z[32] + 2*z[8];
    z[32]=z[8]*z[32];
    z[28]=n<T>(1,4)*z[28] + z[32];
    z[28]=z[28]*z[21];
    z[32]=static_cast<T>(2)+ z[1];
    z[32]=z[1]*z[32];
    z[28]=z[28] + n<T>(9,8) + z[32];
    z[28]=z[8]*z[28];
    z[20]=z[20] + n<T>(1,2)*z[22] + z[28];
    z[20]=z[6]*z[20];
    z[19]=z[19]*z[39];
    z[19]=static_cast<T>(1)+ z[19];
    z[22]=z[49] - n<T>(2,3) - z[39];
    z[22]=z[8]*z[22];
    z[19]=n<T>(1,2)*z[19] + z[22];
    z[22]=npow(z[8],2);
    z[19]=z[19]*z[22];
    z[28]= - z[21] + z[33];
    z[28]=z[28]*z[43];
    z[28]=z[28] - z[40];
    z[28]=z[8]*z[28];
    z[32]=z[37] + 1;
    z[32]=z[32]*z[1];
    z[50]=z[32] + 1;
    z[51]=z[50]*z[1];
    z[51]=z[51] + n<T>(1,3);
    z[28]=n<T>(1,2)*z[51] + z[28];
    z[28]=z[6]*z[28]*z[43];
    z[19]=z[28] - n<T>(1,4)*z[51] + z[19];
    z[19]=z[6]*z[19];
    z[28]=n<T>(5,3) + z[1];
    z[28]=n<T>(1,2)*z[28] - z[21];
    z[28]=z[8]*z[28];
    z[28]= - n<T>(1,2)*z[33] + z[28];
    z[28]=z[8]*z[28];
    z[53]=z[42] + n<T>(1,3);
    z[53]=n<T>(1,2)*z[53];
    z[28]= - z[53] + z[28];
    z[28]=z[8]*z[28];
    z[28]=z[53] + z[28];
    z[19]=n<T>(1,2)*z[28] + z[19];
    z[19]=z[9]*z[19];
    z[28]=static_cast<T>(23)+ 17*z[1];
    z[53]=n<T>(13,3)*z[1];
    z[56]=z[53] - 3*z[8];
    z[56]=z[8]*z[56];
    z[28]=n<T>(1,3)*z[28] + z[56];
    z[28]=z[8]*z[28];
    z[23]= - n<T>(41,2) - z[23];
    z[23]=z[1]*z[23];
    z[23]= - n<T>(17,2) + z[23];
    z[23]=n<T>(1,3)*z[23] + z[28];
    z[23]=z[23]*z[49];
    z[28]= - n<T>(7,6) - z[1];
    z[28]=z[1]*z[28];
    z[19]=5*z[19] + z[20] + z[23] - n<T>(7,12) + z[28];
    z[19]=z[9]*z[19];
    z[20]=z[10] - z[8];
    z[23]=z[20] - n<T>(7,3);
    z[23]=z[23]*z[43];
    z[28]=z[48] - n<T>(7,3);
    z[28]=z[28]*z[1];
    z[23]=z[23] - z[28];
    z[28]=n<T>(5,4) - z[23];
    z[28]=z[8]*z[28];
    z[56]=z[1] - n<T>(7,4);
    z[57]=z[1]*z[56];
    z[28]=z[28] - n<T>(9,4) + z[57];
    z[28]=z[8]*z[28];
    z[42]= - n<T>(43,8) + z[42];
    z[42]=z[1]*z[42];
    z[42]= - n<T>(43,8) + z[42];
    z[28]=n<T>(1,3)*z[42] + z[28];
    z[42]=z[1] - n<T>(5,8);
    z[42]= - z[49] + n<T>(1,3)*z[42];
    z[42]=z[42]*z[8];
    z[32]= - n<T>(1,2) + z[32];
    z[32]=n<T>(1,4)*z[32] + z[42];
    z[32]=z[8]*z[32];
    z[57]=n<T>(5,4) - z[1];
    z[57]=z[1]*z[57];
    z[57]=n<T>(23,4) + z[57];
    z[57]=z[1]*z[57];
    z[57]=n<T>(31,8) + z[57];
    z[32]=n<T>(1,6)*z[57] + z[32];
    z[32]=z[8]*z[32];
    z[57]=z[1] + n<T>(1,2);
    z[58]=z[57]*z[1];
    z[59]=n<T>(1,4) - z[58];
    z[59]=z[1]*z[59];
    z[59]=n<T>(1,2) + z[59];
    z[32]=n<T>(1,4)*z[59] + z[32];
    z[32]=z[6]*z[32];
    z[28]=n<T>(1,2)*z[28] + z[32];
    z[28]=z[6]*z[28];
    z[32]=7*z[1];
    z[59]= - n<T>(11,2) - z[32];
    z[59]=z[59]*z[37];
    z[59]=n<T>(1,4) + z[59];
    z[48]= - n<T>(1,3) - z[48];
    z[48]=z[1]*z[48];
    z[48]=n<T>(29,24) + z[48];
    z[60]=z[37] + n<T>(1,8);
    z[61]=7*z[60] - n<T>(31,24)*z[8];
    z[61]=z[8]*z[61];
    z[48]=n<T>(1,2)*z[48] + z[61];
    z[48]=z[8]*z[48];
    z[19]=z[19] + z[28] + n<T>(1,4)*z[59] + z[48];
    z[19]=z[9]*z[19];
    z[28]=z[1] - n<T>(7,3);
    z[48]=z[28]*z[41];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[8]*z[23];
    z[23]=z[48] + z[23];
    z[23]=z[8]*z[23];
    z[23]=z[23] - z[29];
    z[48]=n<T>(19,4) + z[1];
    z[48]=z[48]*z[37];
    z[48]=n<T>(7,4) + z[48];
    z[48]=z[1]*z[48];
    z[48]=n<T>(1,2) + z[48];
    z[59]= - static_cast<T>(1)- n<T>(1,12)*z[1];
    z[59]=z[1]*z[59];
    z[42]= - z[42] - n<T>(5,8) + z[59];
    z[42]=z[8]*z[42];
    z[42]=n<T>(1,2)*z[48] + z[42];
    z[42]=z[6]*z[8]*z[42];
    z[23]=n<T>(1,2)*z[23] + z[42];
    z[23]=z[6]*z[23];
    z[38]= - z[21] + z[38];
    z[38]=z[8]*z[38];
    z[29]=z[38] - z[29];
    z[29]=z[8]*z[29];
    z[38]=z[37] + n<T>(3,4);
    z[38]=z[38]*z[1];
    z[42]=n<T>(1,2) + z[38];
    z[42]=z[1]*z[42];
    z[29]=z[29] - n<T>(1,2) + z[42];
    z[29]=z[8]*z[29];
    z[42]=n<T>(1,6)*z[8] - n<T>(1,2) - z[37];
    z[42]=z[8]*z[42];
    z[42]=n<T>(1,2)*z[50] + z[42];
    z[22]=z[6]*z[42]*z[22];
    z[22]=z[29] + z[22];
    z[22]=z[22]*z[25];
    z[29]=static_cast<T>(1)+ n<T>(1,6)*z[1];
    z[29]=z[29]*z[1];
    z[42]=n<T>(7,8) + z[29];
    z[42]=z[42]*z[41];
    z[33]=z[33]*z[39];
    z[48]=n<T>(1,12)*z[8] - z[60];
    z[48]=z[8]*z[48];
    z[33]=z[33] + z[48];
    z[33]=z[8]*z[33];
    z[38]= - n<T>(1,4) - z[38];
    z[38]=z[1]*z[38];
    z[33]=z[33] + n<T>(1,4) + z[38];
    z[33]=z[8]*z[33];
    z[22]=z[22] + z[42] + z[33];
    z[22]=z[4]*z[22];
    z[33]= - 3*z[36] + z[8];
    z[33]=z[33]*z[43];
    z[36]=z[1]*z[54];
    z[33]=z[33] - static_cast<T>(1)+ z[36];
    z[33]=z[8]*z[33];
    z[36]=n<T>(9,4) - z[58];
    z[36]=z[1]*z[36];
    z[36]=static_cast<T>(1)+ z[36];
    z[33]=n<T>(1,2)*z[36] + z[33];
    z[22]=z[22] + n<T>(1,2)*z[33] + z[23];
    z[22]=z[4]*z[22];
    z[23]= - z[40]*z[37];
    z[33]=z[6]*z[51]*z[39];
    z[23]=z[23] + z[33];
    z[23]=z[6]*z[23];
    z[33]= - static_cast<T>(1)- z[10];
    z[33]=z[33]*z[35];
    z[33]=z[33] + z[55];
    z[33]=z[33]*z[34];
    z[30]=z[57]*z[30];
    z[30]=z[30] - z[44];
    z[34]=z[3]*z[37];
    z[30]=n<T>(1,2)*z[30] + z[34];
    z[30]=z[30]*z[31];
    z[23]=z[30] + z[33] + n<T>(1,8)*z[44] + z[23];
    z[23]=z[3]*z[23];
    z[30]= - n<T>(21,2) - z[53];
    z[30]=n<T>(1,4)*z[30] + n<T>(4,3)*z[8];
    z[30]=z[8]*z[30];
    z[30]=z[30] + n<T>(9,8) + z[37];
    z[30]=z[8]*z[30];
    z[33]=z[45]*z[21];
    z[33]= - z[39] + z[33];
    z[33]=z[8]*z[33];
    z[33]=z[33] + z[14];
    z[33]=z[8]*z[33];
    z[14]= - n<T>(1,2)*z[14] + z[33];
    z[33]=n<T>(5,2)*z[9];
    z[14]=z[14]*z[33];
    z[34]=n<T>(7,3) + 9*z[1];
    z[14]=z[14] + n<T>(1,8)*z[34] + z[30];
    z[14]=z[9]*z[14];
    z[30]=n<T>(17,8) + z[1];
    z[27]=n<T>(11,12)*z[8] - z[27];
    z[27]=z[8]*z[27];
    z[27]=n<T>(1,3)*z[30] + z[27];
    z[14]=n<T>(1,2)*z[27] + z[14];
    z[14]=z[9]*z[14];
    z[21]=static_cast<T>(1)- z[21];
    z[21]=z[8]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[8]*z[21];
    z[21]=n<T>(1,3) + z[21];
    z[21]=z[21]*z[33];
    z[27]=n<T>(11,4) - z[8];
    z[27]=z[8]*z[27];
    z[27]= - n<T>(5,2) + z[27];
    z[21]=n<T>(1,3)*z[27] + z[21];
    z[21]=z[9]*z[21];
    z[21]=n<T>(1,4) + z[21];
    z[21]=z[9]*z[21];
    z[27]=npow(z[9],2);
    z[30]= - z[3]*z[9];
    z[27]=z[27] + z[30];
    z[27]=z[27]*z[31];
    z[21]=z[21] + z[27];
    z[21]=z[21]*z[47];
    z[27]= - z[43] + z[55];
    z[14]=z[21] + z[23] + n<T>(1,6)*z[27] + z[14];
    z[14]=z[5]*z[14];
    z[21]= - n<T>(5,3) + z[1];
    z[21]=z[21]*z[12];
    z[23]=n<T>(31,3) - z[1];
    z[23]=z[23]*z[16];
    z[26]=z[15] - z[26];
    z[26]=z[3]*z[26];
    z[21]=z[26] + z[21] + n<T>(1,4)*z[23];
    z[21]=z[3]*z[21];
    z[23]=z[12]*z[4];
    z[26]=z[56]*z[23];
    z[11]= - z[1]*z[11];
    z[11]=z[21] + z[11] + z[26];
    z[11]=z[3]*z[4]*z[11];
    z[21]= - n<T>(1,2) - z[10];
    z[21]=z[21]*z[39];
    z[26]=z[49] - z[1];
    z[27]=n<T>(5,12) - z[26];
    z[27]=z[8]*z[27];
    z[21]=z[21] + z[27];
    z[21]=z[4]*z[21];
    z[21]=z[21] + n<T>(1,3) + z[20];
    z[21]=z[4]*z[21];
    z[11]=z[21] + z[11];
    z[21]=npow(z[4],2);
    z[26]=z[26]*z[21];
    z[27]=z[24]*z[31];
    z[27]= - z[15] + z[27];
    z[27]=z[3]*z[27];
    z[27]=z[41] + n<T>(1,3)*z[27];
    z[21]=z[3]*z[21]*z[27];
    z[21]=n<T>(1,3)*z[26] + z[21];
    z[21]=z[2]*z[21];
    z[11]=n<T>(1,4)*z[11] + z[21];
    z[11]=z[2]*z[11];
    z[21]=n<T>(7,2) - z[10];
    z[21]=z[21]*z[12];
    z[16]=z[28]*z[16];
    z[16]=z[21] + z[16];
    z[16]=z[16]*z[17];
    z[21]=n<T>(1,6) + z[1];
    z[21]=z[1]*z[21];
    z[16]=z[21] + z[16];
    z[21]=z[24]*z[17];
    z[15]= - z[15] + z[21];
    z[15]=z[4]*z[15];
    z[15]=z[41] + z[15];
    z[15]=z[3]*z[15];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[3]*z[15];
    z[16]=z[44]*z[39];
    z[16]=n<T>(1,3) + z[16];
    z[16]=z[16]*z[23];
    z[21]=n<T>(35,12) - z[1];
    z[21]=z[1]*z[21];
    z[21]= - n<T>(1,6) + z[21];
    z[21]=z[1]*z[21];
    z[16]=z[21] + z[16];
    z[16]=z[4]*z[16];
    z[21]= - n<T>(5,6) + z[1];
    z[21]=z[1]*z[21];
    z[16]=z[16] + n<T>(1,2) + z[21];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[3]*z[15];
    z[15]=z[15] - n<T>(1,12) + z[46];
    z[16]= - n<T>(1,12) - z[20];
    z[16]=z[16]*z[49];
    z[20]=z[49] - z[57];
    z[20]=z[8]*z[20];
    z[10]=n<T>(5,2) + z[10];
    z[10]=z[1]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[10]=n<T>(1,2)*z[10] + z[20];
    z[10]=z[8]*z[10];
    z[20]= - n<T>(7,12) - z[52];
    z[20]=z[1]*z[20];
    z[10]=z[20] + z[10];
    z[10]=z[10]*z[17];
    z[17]= - n<T>(1,3) + n<T>(3,4)*z[1];
    z[17]=z[1]*z[17];
    z[10]=z[10] + z[16] + n<T>(5,48) + z[17];
    z[10]=z[4]*z[10];
    z[10]=z[11] + z[10] + n<T>(1,2)*z[15];
    z[10]=z[2]*z[10];
    z[11]=n<T>(11,8) + z[1];
    z[11]=z[11]*z[37];
    z[11]=n<T>(1,16) + z[11];
    z[11]=z[1]*z[11];
    z[15]= - n<T>(15,8) - z[29];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(25,24) + z[15];
    z[15]=z[1]*z[15];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[8]*z[12];
    z[12]=n<T>(3,4)*z[12] - n<T>(1,8) + z[15];
    z[12]=z[12]*z[25];
    z[15]=z[8]*z[44];
    z[11]=z[12] - n<T>(3,8)*z[15] - n<T>(3,16) + z[11];
    z[11]=z[6]*z[11];
    z[12]= - n<T>(5,2) - z[32];
    z[12]=z[12]*z[39];
    z[15]= - n<T>(13,6)*z[8] + n<T>(19,12) + 5*z[1];
    z[15]=z[8]*z[15];
    z[12]=z[15] + n<T>(1,3) + z[12];

    r += z[10] + z[11] + n<T>(1,4)*z[12] + z[13] + z[14] + z[18] + z[19] + 
      z[22];
 
    return r;
}

template double qqb_2lha_r1386(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1386(const std::array<dd_real,30>&);
#endif
