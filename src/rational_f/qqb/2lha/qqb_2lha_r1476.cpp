#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1476(const std::array<T,30>& k) {
  T z[51];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[29];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[2];
    z[9]=k[6];
    z[10]=k[7];
    z[11]=k[11];
    z[12]=k[10];
    z[13]=n<T>(1,2)*z[3];
    z[14]=5*z[8];
    z[15]=n<T>(7,2) - z[14];
    z[15]=z[15]*z[13];
    z[16]=n<T>(1,2)*z[9];
    z[17]=z[16] - 1;
    z[18]=n<T>(3,4)*z[7];
    z[19]=z[17]*z[18];
    z[20]=n<T>(1,4)*z[9];
    z[21]=static_cast<T>(11)+ z[16];
    z[21]=z[21]*z[20];
    z[21]=static_cast<T>(3)+ z[21];
    z[21]=z[6]*z[21];
    z[22]=3*z[6];
    z[23]= - n<T>(15,8) + z[6];
    z[23]=z[23]*z[22];
    z[23]=n<T>(5,4) + z[23];
    z[23]=z[4]*z[23];
    z[24]=n<T>(99,2)*z[6] - static_cast<T>(3)- z[13];
    z[23]=n<T>(1,4)*z[24] + z[23];
    z[23]=z[4]*z[23];
    z[24]=n<T>(1,8)*z[9];
    z[25]=n<T>(15,4) - z[8];
    z[15]=z[23] + z[21] + z[19] + z[15] + 3*z[25] + z[24];
    z[15]=z[1]*z[15];
    z[19]=n<T>(3,2)*z[3];
    z[21]=static_cast<T>(1)+ n<T>(5,2)*z[8];
    z[21]=z[21]*z[19];
    z[23]=3*z[8];
    z[25]=n<T>(3,4)*z[9];
    z[26]= - z[9]*z[18];
    z[21]=z[26] + z[21] + z[25] - n<T>(65,4) + z[23];
    z[26]= - static_cast<T>(47)+ n<T>(31,2)*z[3];
    z[27]= - n<T>(25,4)*z[6] + static_cast<T>(7)- n<T>(13,16)*z[3];
    z[27]=z[4]*z[27];
    z[28]=static_cast<T>(3)- n<T>(5,8)*z[3];
    z[28]=z[4]*z[28];
    z[28]=n<T>(31,8)*z[3] + z[28];
    z[28]=z[1]*z[28];
    z[26]=z[28] + n<T>(1,4)*z[26] + z[27];
    z[26]=z[4]*z[26];
    z[27]=n<T>(5,2)*z[3];
    z[28]= - n<T>(5,2) + z[3];
    z[28]=z[4]*z[28];
    z[28]= - z[27] + z[28];
    z[29]=5*z[4];
    z[28]=z[28]*z[29];
    z[30]=npow(z[4],2);
    z[31]=z[30]*z[3];
    z[32]=z[31]*z[2];
    z[31]=z[31]*z[1];
    z[28]= - n<T>(5,2)*z[32] + z[28] + 3*z[31];
    z[28]=z[2]*z[28];
    z[26]=n<T>(1,4)*z[28] - z[27] + z[26];
    z[26]=z[2]*z[26];
    z[28]=z[20] + 3;
    z[33]=z[6]*z[28]*z[16];
    z[33]=z[33] - static_cast<T>(3)- n<T>(5,8)*z[9];
    z[33]=z[6]*z[33];
    z[34]=5*z[6];
    z[35]=n<T>(5,4)*z[6];
    z[36]=static_cast<T>(3)- z[35];
    z[36]=z[36]*z[34];
    z[37]= - n<T>(83,4) + z[3];
    z[36]=n<T>(1,4)*z[37] + z[36];
    z[36]=z[4]*z[36];
    z[37]=static_cast<T>(121)- z[27];
    z[37]=n<T>(1,2)*z[37] - 65*z[6];
    z[36]=n<T>(1,4)*z[37] + z[36];
    z[36]=z[4]*z[36];
    z[37]=3*z[3];
    z[38]=static_cast<T>(31)- z[37];
    z[39]= - n<T>(27,2) + z[3];
    z[39]=n<T>(1,2)*z[39] + 9*z[6];
    z[39]=z[4]*z[39];
    z[38]=n<T>(3,4)*z[38] + z[39];
    z[38]=z[4]*z[38];
    z[38]=n<T>(25,4)*z[3] + z[38];
    z[39]=n<T>(1,2)*z[1];
    z[38]=z[38]*z[39];
    z[21]=z[26] + z[38] + z[36] + n<T>(1,2)*z[21] + z[33];
    z[21]=z[2]*z[21];
    z[26]=static_cast<T>(5)+ z[20];
    z[26]=z[26]*z[20];
    z[33]=n<T>(5,8)*z[6];
    z[36]= - static_cast<T>(7)- z[16];
    z[36]=z[9]*z[36]*z[33];
    z[26]=z[36] - static_cast<T>(3)+ z[26];
    z[26]=z[6]*z[26];
    z[36]=n<T>(59,2) - 13*z[6];
    z[36]=z[36]*z[22];
    z[38]=static_cast<T>(1)- n<T>(15,4)*z[3];
    z[40]=static_cast<T>(61)- n<T>(25,2)*z[6];
    z[40]=z[6]*z[40];
    z[40]= - n<T>(189,4) + z[40];
    z[40]=z[6]*z[40];
    z[40]=n<T>(13,2) + z[40];
    z[40]=z[4]*z[40];
    z[36]=z[40] + n<T>(1,2)*z[38] + z[36];
    z[38]=n<T>(1,4)*z[4];
    z[36]=z[36]*z[38];
    z[40]= - n<T>(17,2)*z[9] + n<T>(41,2) + 13*z[8];
    z[41]=n<T>(5,8) + z[8];
    z[41]=z[3]*z[41];
    z[42]= - n<T>(3,4) + z[9];
    z[42]=z[7]*z[42];
    z[15]=z[21] + z[15] + z[36] + z[26] + z[42] + n<T>(1,8)*z[40] + z[41];
    z[15]=z[2]*z[15];
    z[21]=7*z[8];
    z[26]=3*z[7];
    z[36]= - n<T>(9,2) - z[9];
    z[36]=z[9]*z[36];
    z[36]= - z[26] + z[36] - static_cast<T>(3)+ z[21];
    z[40]=n<T>(35,2) + z[9];
    z[40]=z[9]*z[40];
    z[40]= - n<T>(19,2) + z[40];
    z[40]=z[6]*z[40];
    z[36]=n<T>(1,2)*z[36] + z[40];
    z[40]=n<T>(1,2)*z[6];
    z[36]=z[36]*z[40];
    z[41]= - static_cast<T>(51)+ z[3];
    z[42]=static_cast<T>(111)- 17*z[6];
    z[42]=z[6]*z[42];
    z[42]= - n<T>(3,2) + z[42];
    z[42]=z[6]*z[42];
    z[41]=n<T>(1,2)*z[41] + z[42];
    z[42]=n<T>(5,2)*z[6];
    z[43]=static_cast<T>(29)- z[42];
    z[43]=z[6]*z[43];
    z[43]= - n<T>(191,4) + z[43];
    z[43]=z[43]*z[40];
    z[43]=static_cast<T>(7)+ z[43];
    z[43]=z[6]*z[43];
    z[43]= - n<T>(3,8) + z[43];
    z[43]=z[4]*z[43];
    z[41]=n<T>(1,4)*z[41] + z[43];
    z[41]=z[4]*z[41];
    z[43]=n<T>(1,4)*z[7];
    z[44]=static_cast<T>(7)- n<T>(13,2)*z[9];
    z[44]=z[44]*z[43];
    z[45]=n<T>(5,4) + z[8];
    z[45]=11*z[45] + n<T>(7,2)*z[9];
    z[46]=n<T>(3,16) + z[8];
    z[46]=z[3]*z[46];
    z[36]=z[41] + z[36] + z[44] + n<T>(1,2)*z[45] + z[46];
    z[41]=n<T>(1,4)*z[3];
    z[44]=static_cast<T>(3)+ z[14];
    z[44]=z[44]*z[41];
    z[45]=z[6]*z[7];
    z[44]= - n<T>(3,4)*z[45] - z[18] + z[44] - n<T>(31,4) + z[23];
    z[44]=z[44]*z[39];
    z[17]=z[9]*z[17];
    z[17]=13*z[3] + z[17] + n<T>(115,2) - 17*z[8];
    z[46]=static_cast<T>(1)- n<T>(5,4)*z[9];
    z[46]=z[7]*z[46];
    z[17]=n<T>(1,4)*z[17] + z[46];
    z[46]= - static_cast<T>(23)- z[9];
    z[46]=z[9]*z[46];
    z[46]= - z[43] + static_cast<T>(3)+ n<T>(1,16)*z[46];
    z[46]=z[46]*z[22];
    z[47]= - n<T>(25,2) + z[22];
    z[47]=z[6]*z[47];
    z[47]=static_cast<T>(7)+ z[47];
    z[47]=z[6]*z[47];
    z[47]= - n<T>(3,4) + z[47];
    z[47]=z[47]*z[38];
    z[48]=static_cast<T>(5)+ 47*z[6];
    z[48]=z[6]*z[48];
    z[47]=z[47] + n<T>(1,8)*z[48] - static_cast<T>(3)+ n<T>(1,16)*z[3];
    z[47]=z[4]*z[47];
    z[17]=z[44] + z[47] + n<T>(1,2)*z[17] + z[46];
    z[17]=z[1]*z[17];
    z[15]=z[15] + n<T>(1,2)*z[36] + z[17];
    z[15]=z[2]*z[15];
    z[17]=n<T>(3,2)*z[45];
    z[36]= - static_cast<T>(1)- z[14];
    z[36]=z[3]*z[36];
    z[36]= - z[17] - n<T>(3,2)*z[7] + static_cast<T>(3)+ z[36];
    z[44]= - n<T>(1,2) - z[3];
    z[44]=3*z[44] - z[40];
    z[44]=z[4]*z[44];
    z[46]=z[6] - 1;
    z[47]= - n<T>(29,2)*z[3] - z[46];
    z[44]=n<T>(1,2)*z[47] + z[44];
    z[44]=z[4]*z[44];
    z[47]= - static_cast<T>(1)- z[4];
    z[29]=z[29]*z[3]*z[47];
    z[29]=z[29] - z[31];
    z[29]=z[29]*z[39];
    z[47]= - static_cast<T>(1)- n<T>(21,4)*z[3];
    z[47]=z[4]*z[47];
    z[47]=z[47] - static_cast<T>(1)- n<T>(19,2)*z[3];
    z[47]=z[4]*z[47];
    z[29]=z[47] + z[29];
    z[29]=z[29]*z[39];
    z[29]=z[29] + n<T>(1,2)*z[36] + z[44];
    z[29]=z[29]*z[39];
    z[36]= - static_cast<T>(7)- n<T>(87,4)*z[3];
    z[44]=static_cast<T>(7)- z[40];
    z[44]=z[6]*z[44];
    z[36]=n<T>(1,4)*z[36] + z[44];
    z[44]=z[3] + n<T>(3,4);
    z[47]= - n<T>(3,2) - z[6];
    z[47]=z[6]*z[47];
    z[47]=n<T>(1,4)*z[47] - z[44];
    z[47]=z[4]*z[47];
    z[36]=n<T>(1,2)*z[36] + z[47];
    z[36]=z[4]*z[36];
    z[47]=static_cast<T>(1)- z[7];
    z[47]=z[47]*z[40];
    z[48]=n<T>(99,2) - z[14];
    z[23]=n<T>(7,8) - z[23];
    z[23]=z[3]*z[23];
    z[23]=z[29] + z[36] + z[47] - n<T>(13,16)*z[7] + n<T>(1,8)*z[48] + z[23];
    z[23]=z[1]*z[23];
    z[29]=static_cast<T>(7)+ z[20];
    z[29]=z[29]*z[20];
    z[36]=n<T>(1,8)*z[7];
    z[29]=z[22] + z[36] + z[29] + static_cast<T>(9)- n<T>(7,4)*z[8];
    z[29]=z[6]*z[29];
    z[46]= - z[6]*z[46];
    z[46]= - n<T>(5,4) + z[46];
    z[46]=z[6]*z[46];
    z[46]=z[46] - static_cast<T>(1)- z[19];
    z[46]=z[46]*z[38];
    z[47]= - static_cast<T>(1)- n<T>(3,4)*z[3];
    z[48]=static_cast<T>(2)+ z[35];
    z[48]=z[6]*z[48];
    z[48]= - static_cast<T>(5)+ z[48];
    z[48]=z[6]*z[48];
    z[46]=z[46] + n<T>(5,4)*z[47] + z[48];
    z[46]=z[4]*z[46];
    z[47]=z[9] - 5;
    z[47]= - z[47]*z[24];
    z[48]=11*z[8];
    z[47]=z[47] + n<T>(61,4) - z[48];
    z[49]=n<T>(3,2)*z[9];
    z[50]= - static_cast<T>(5)+ z[49];
    z[36]=z[50]*z[36];
    z[50]=n<T>(13,16) - z[8];
    z[50]=z[3]*z[50];
    z[23]=z[23] + z[46] + z[29] + z[36] + n<T>(1,2)*z[47] + z[50];
    z[23]=z[1]*z[23];
    z[29]= - n<T>(5,2) + z[9];
    z[24]=z[29]*z[24];
    z[29]= - static_cast<T>(13)- z[16];
    z[29]=z[29]*z[16];
    z[29]=static_cast<T>(7)+ z[29];
    z[29]=z[29]*z[40];
    z[36]=n<T>(1,2)*z[7];
    z[24]=z[29] + z[36] + static_cast<T>(11)+ z[24];
    z[24]=z[6]*z[24];
    z[29]=z[13] + 5;
    z[46]=z[9] - 1;
    z[47]=z[7]*z[46];
    z[47]=z[47] - z[9] + z[29];
    z[24]=n<T>(3,8)*z[47] + z[24];
    z[47]= - n<T>(1,2) + z[6];
    z[47]=z[6]*z[47];
    z[47]= - n<T>(57,32) + z[47];
    z[22]=z[47]*z[22];
    z[34]= - n<T>(41,2) + z[34];
    z[34]=z[6]*z[34];
    z[34]=n<T>(25,2) + z[34];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(3,2) + z[34];
    z[34]=z[6]*z[34];
    z[34]= - z[41] + z[34];
    z[34]=z[34]*z[38];
    z[47]= - static_cast<T>(5)- z[19];
    z[22]=z[34] + n<T>(1,16)*z[47] + z[22];
    z[22]=z[4]*z[22];
    z[15]=z[15] + z[23] + n<T>(1,2)*z[24] + z[22];
    z[15]=z[2]*z[15];
    z[22]=static_cast<T>(5)- 7*z[45];
    z[22]=z[22]*z[38];
    z[23]=static_cast<T>(11)- z[27];
    z[22]=z[22] - n<T>(7,2)*z[45] + n<T>(1,2)*z[23] + z[26];
    z[22]=z[4]*z[22];
    z[23]=z[45] - 1;
    z[23]=z[23]*z[4];
    z[24]= - z[3] - z[23];
    z[24]=z[24]*z[39];
    z[23]=z[23] - static_cast<T>(1)+ z[3];
    z[23]=z[24] - z[17] + z[7] - n<T>(3,2)*z[23];
    z[23]=z[4]*z[23];
    z[23]= - z[27] + z[23];
    z[23]=z[1]*z[23];
    z[14]= - n<T>(21,2) + z[14];
    z[14]=z[3]*z[14];
    z[14]=z[23] + z[22] - z[17] + z[14] + z[26];
    z[14]=z[1]*z[14];
    z[17]=n<T>(5,2)*z[7];
    z[22]=static_cast<T>(3)- z[17];
    z[22]=z[6]*z[22];
    z[18]= - static_cast<T>(1)- z[18];
    z[18]=z[6]*z[18];
    z[18]= - n<T>(3,4) + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[18] + z[22] + n<T>(13,4)*z[7] + z[29];
    z[18]=z[18]*z[38];
    z[22]= - static_cast<T>(1)- n<T>(17,8)*z[7];
    z[22]=z[22]*z[40];
    z[23]=n<T>(25,2) + 9*z[8];
    z[24]= - n<T>(25,8) + 2*z[8];
    z[24]=z[3]*z[24];
    z[14]=n<T>(1,4)*z[14] + z[18] + z[22] + n<T>(15,8)*z[7] + n<T>(1,8)*z[23] + 
    z[24];
    z[14]=z[1]*z[14];
    z[18]=n<T>(1,4)*z[10];
    z[22]= - z[18] + n<T>(15,4) + z[48];
    z[23]= - n<T>(29,16) + z[8];
    z[23]=z[3]*z[23];
    z[22]=z[26] + n<T>(1,2)*z[22] + z[23];
    z[23]=z[37] + 1;
    z[17]=z[17] + z[10] + z[23];
    z[24]=n<T>(1,2)*z[4];
    z[27]= - static_cast<T>(1)- n<T>(3,4)*z[6];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(9,8) + z[27];
    z[27]=z[27]*z[24];
    z[29]= - static_cast<T>(5)- z[7];
    z[29]=n<T>(1,8)*z[29] + z[6];
    z[29]=z[6]*z[29];
    z[17]=z[27] + n<T>(1,8)*z[17] + z[29];
    z[17]=z[4]*z[17];
    z[21]=static_cast<T>(19)+ z[21];
    z[21]= - z[33] + n<T>(1,8)*z[21] - z[7];
    z[21]=z[6]*z[21];
    z[14]=z[14] + z[17] + n<T>(1,2)*z[22] + z[21];
    z[14]=z[1]*z[14];
    z[17]= - static_cast<T>(1)+ z[8];
    z[17]=z[10]*z[17];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[41];
    z[17]=z[26] + z[17] - z[25] + static_cast<T>(9)- n<T>(5,4)*z[10];
    z[21]=n<T>(7,2)*z[6] - n<T>(5,8)*z[7] + static_cast<T>(11)- z[20];
    z[21]=z[6]*z[21];
    z[17]=n<T>(1,4)*z[17] + z[21];
    z[21]= - n<T>(5,2) + z[6];
    z[21]=z[21]*z[35];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[6]*z[21];
    z[22]= - static_cast<T>(1)- z[18];
    z[26]= - n<T>(1,4) - z[6];
    z[26]=z[6]*z[26];
    z[22]=n<T>(1,2)*z[22] + z[26];
    z[22]=z[6]*z[22];
    z[22]= - n<T>(7,8) + z[22];
    z[22]=z[22]*z[24];
    z[24]=z[10] - 1;
    z[26]=n<T>(3,4)*z[24] + z[3];
    z[21]=z[22] + n<T>(1,4)*z[26] + z[21];
    z[21]=z[4]*z[21];
    z[14]=z[14] + n<T>(1,2)*z[17] + z[21];
    z[14]=z[1]*z[14];
    z[17]=n<T>(1,2)*z[10];
    z[21]= - static_cast<T>(1)- n<T>(1,2)*z[8];
    z[21]=z[3]*z[21];
    z[21]= - z[17] + z[21];
    z[22]=z[6]*z[9];
    z[22]=z[25] + z[22];
    z[22]=z[6]*z[22];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[22]=7*z[6];
    z[25]= - static_cast<T>(3)- z[6];
    z[25]=z[25]*z[22];
    z[25]=z[25] - static_cast<T>(3)+ z[18];
    z[25]=z[6]*z[25];
    z[26]=z[3] + z[24];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[26]=z[33] - 1;
    z[26]=z[26]*z[6];
    z[27]= - n<T>(3,16) - z[26];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,16)*z[10] + z[27];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,8) + z[27];
    z[27]=z[4]*z[27];
    z[25]=n<T>(1,8)*z[25] + z[27];
    z[25]=z[4]*z[25];
    z[14]=z[15] + z[14] + n<T>(1,8)*z[21] + z[25];
    z[14]=z[5]*z[14];
    z[15]= - static_cast<T>(1)+ n<T>(1,2)*z[11];
    z[21]=npow(z[9],2);
    z[25]= - z[15]*z[21];
    z[25]=static_cast<T>(3)+ z[25];
    z[25]=z[9]*z[25];
    z[15]=z[9]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[15]*z[21];
    z[27]=npow(z[3],2);
    z[29]=n<T>(1,2)*z[27];
    z[15]=z[15] + z[29];
    z[15]=z[7]*z[15];
    z[15]=z[25] + z[15];
    z[15]=z[15]*z[43];
    z[25]= - z[9]*z[28];
    z[28]=z[3] + z[4];
    z[28]=z[4]*z[28];
    z[15]=n<T>(5,4)*z[32] + z[31] + n<T>(15,4)*z[28] + z[25] + z[15];
    z[15]=z[2]*z[15];
    z[20]= - z[46]*z[20];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[9]*z[20];
    z[25]=z[9] + n<T>(1,2);
    z[28]=z[25]*z[21];
    z[28]=z[28] - z[29];
    z[28]=z[28]*z[36];
    z[20]=z[28] + z[20] - z[41];
    z[20]=z[20]*z[36];
    z[28]=static_cast<T>(1)+ n<T>(3,8)*z[3];
    z[32]=z[4]*z[28];
    z[32]=n<T>(11,8)*z[3] + z[32];
    z[32]=z[4]*z[32];
    z[20]=n<T>(3,8)*z[31] + z[20] + z[32];
    z[20]=z[1]*z[20];
    z[31]=static_cast<T>(1)+ z[49];
    z[31]=z[31]*z[21];
    z[31]=z[31] - z[27];
    z[31]=z[7]*z[31];
    z[32]=n<T>(1,2) - 3*z[9];
    z[32]=z[9]*z[32];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=z[9]*z[32];
    z[31]=z[31] + z[32] - z[3];
    z[31]=z[7]*z[31];
    z[25]=z[9]*z[25];
    z[25]=static_cast<T>(11)+ z[25];
    z[25]=z[9]*z[25];
    z[25]=z[25] + z[31];
    z[31]=z[42] - static_cast<T>(1)+ z[41];
    z[31]=z[31]*z[38];
    z[28]=z[31] + z[28];
    z[28]=z[4]*z[28];
    z[15]=n<T>(1,2)*z[15] + z[20] + n<T>(1,8)*z[25] + 3*z[28];
    z[15]=z[2]*z[15];
    z[20]=z[10] - 3;
    z[25]=z[20]*z[17];
    z[28]= - static_cast<T>(1)- z[25];
    z[31]=z[20]*z[16];
    z[32]=static_cast<T>(1)- z[18];
    z[32]=z[10]*z[32];
    z[31]=z[31] - n<T>(5,4) + z[32];
    z[31]=z[9]*z[31];
    z[28]=n<T>(1,2)*z[28] + z[31];
    z[28]=z[9]*z[28];
    z[31]=z[12] + 1;
    z[32]= - z[31]*z[17];
    z[32]=z[12] + z[32];
    z[32]=z[32]*z[17];
    z[33]=static_cast<T>(3)+ z[12];
    z[33]=z[33]*z[13];
    z[33]=z[33] + static_cast<T>(1)+ n<T>(3,2)*z[12];
    z[33]=z[33]*z[13];
    z[28]=z[33] + z[32] + z[28];
    z[28]=z[7]*z[28];
    z[32]=z[24]*z[17];
    z[32]= - static_cast<T>(3)+ z[32];
    z[20]=z[20]*z[9];
    z[25]=z[25] - z[20];
    z[25]=z[9]*z[25];
    z[25]=n<T>(1,2)*z[32] + z[25];
    z[25]=z[9]*z[25];
    z[32]=npow(z[10],2);
    z[33]=n<T>(1,4)*z[32];
    z[34]=z[33]*z[12];
    z[19]=z[28] + z[19] + z[34] + z[25];
    z[19]=z[7]*z[19];
    z[25]=z[17] - 1;
    z[28]=z[25]*z[10];
    z[20]=z[20] + static_cast<T>(1)- z[28];
    z[16]=z[20]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[9]*z[16];
    z[20]= - z[6]*npow(z[7],2)*z[34];
    z[16]=z[20] + z[16] + z[19];
    z[19]=z[7]*z[3];
    z[20]=z[30]*z[39]*z[19];
    z[30]=n<T>(3,2)*z[19];
    z[34]=z[3] + z[30];
    z[34]=z[4]*z[34];
    z[30]=z[30] + z[34];
    z[30]=z[4]*z[30];
    z[20]=z[30] + z[20];
    z[20]=z[20]*z[39];
    z[30]= - static_cast<T>(1)- n<T>(1,2)*z[32];
    z[25]=z[9]*z[25];
    z[25]=n<T>(1,2)*z[30] + z[25];
    z[21]=z[25]*z[21];
    z[21]=z[21] + z[29];
    z[21]=z[7]*z[21];
    z[21]=z[37] + z[21];
    z[21]=z[21]*z[43];
    z[25]=n<T>(7,8)*z[19] + z[44];
    z[25]=z[4]*z[25];
    z[29]=z[3] + z[19];
    z[25]=n<T>(7,4)*z[29] + z[25];
    z[25]=z[4]*z[25];
    z[20]=z[20] + z[21] + z[25];
    z[20]=z[1]*z[20];
    z[21]=z[10] - n<T>(5,2);
    z[21]=z[21]*z[9];
    z[24]=z[10]*z[24];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=n<T>(1,2)*z[24] - z[21];
    z[24]=z[9]*z[24];
    z[24]= - n<T>(5,2) + z[24];
    z[24]=z[9]*z[24];
    z[21]=z[21] - static_cast<T>(1)- z[28];
    z[21]=z[9]*z[21];
    z[21]= - z[33] + z[21];
    z[21]=z[9]*z[21];
    z[21]=z[21] + z[27];
    z[21]=z[7]*z[21];
    z[21]=z[21] + z[24] + 5*z[3];
    z[21]=z[21]*z[43];
    z[18]=z[18] + z[3];
    z[18]=z[7]*z[18];
    z[18]=n<T>(1,2)*z[23] + z[18];
    z[18]=n<T>(1,2)*z[18] + z[6];
    z[18]=z[4]*z[18];
    z[19]=5*z[19] + static_cast<T>(9)+ 11*z[3];
    z[18]=n<T>(1,4)*z[19] + z[18];
    z[18]=z[4]*z[18];
    z[18]=z[20] + z[21] + z[18];
    z[18]=z[18]*z[39];
    z[17]=z[12]*z[17];
    z[17]=static_cast<T>(5)+ z[17];
    z[13]=z[17]*z[13];
    z[17]= - z[10]*z[31];
    z[19]=static_cast<T>(3)- z[12];
    z[19]=z[3]*z[19];
    z[17]=z[17] + z[19];
    z[17]=z[17]*z[43];
    z[13]=z[22] + z[17] + static_cast<T>(21)+ z[13];
    z[17]=z[10] + z[3];
    z[17]=z[17]*z[36];
    z[17]=z[17] + n<T>(3,2) + z[3];
    z[17]=n<T>(1,8)*z[17] + z[26];
    z[17]=z[4]*z[17];
    z[13]=n<T>(1,8)*z[13] + z[17];
    z[13]=z[4]*z[13];

    r += z[13] + z[14] + z[15] + n<T>(1,8)*z[16] + z[18];
 
    return r;
}

template double qqb_2lha_r1476(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1476(const std::array<dd_real,30>&);
#endif
