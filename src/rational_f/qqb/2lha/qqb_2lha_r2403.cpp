#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2403(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[4];
    z[4]=k[7];
    z[5]=k[13];
    z[6]=k[5];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=z[7] - 1;
    z[10]=n<T>(1,2)*z[5];
    z[11]= - z[9]*z[10];
    z[12]=z[6]*npow(z[4],2);
    z[12]=z[12] + z[8];
    z[13]=static_cast<T>(1)+ z[4];
    z[13]=z[4]*z[13];
    z[11]=z[13] + z[11] - static_cast<T>(1)+ n<T>(1,2)*z[12];
    z[11]=z[6]*z[11];
    z[12]=n<T>(1,4)*z[6];
    z[13]=z[8] - z[5];
    z[13]=z[3]*z[13]*z[12];
    z[14]=n<T>(1,2)*z[4];
    z[15]=static_cast<T>(1)+ z[14];
    z[15]=z[4]*z[15];
    z[10]=z[13] + z[11] + z[10] + z[15];
    z[10]=z[3]*z[10];
    z[11]=z[2] - 1;
    z[13]=z[11]*z[14];
    z[12]=z[12] + z[13] + n<T>(5,4) - z[2];
    z[12]=z[4]*z[12];
    z[13]= - z[7] - static_cast<T>(1)+ z[8];
    z[14]=static_cast<T>(1)- n<T>(1,2)*z[7];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(1,2) + z[14];
    z[14]=z[5]*z[14];
    z[13]=z[14] + n<T>(1,2)*z[13] + z[2];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[6]*z[12];
    z[13]=n<T>(1,2)*z[1];
    z[9]= - z[13] + z[9];
    z[9]=z[5]*z[9];
    z[9]=static_cast<T>(1)+ z[9];
    z[13]=z[13] + 1;
    z[11]=z[4]*z[13]*z[11];
    z[13]=static_cast<T>(3)- z[1];
    z[11]=z[11] + n<T>(1,4)*z[13] - z[2];
    z[11]=z[4]*z[11];
    z[9]=z[10] + z[12] + n<T>(1,2)*z[9] + z[11];

    r += n<T>(3,2)*z[9]*z[3];
 
    return r;
}

template double qqb_2lha_r2403(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2403(const std::array<dd_real,30>&);
#endif
