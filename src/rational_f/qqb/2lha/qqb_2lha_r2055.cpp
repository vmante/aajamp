#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2055(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[20];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[5];
    z[9]=3*z[5];
    z[10]=z[9] + z[4];
    z[11]=z[6]*npow(z[8],2);
    z[10]=z[10]*z[11];
    z[12]=n<T>(3,2)*z[5];
    z[13]=n<T>(1,4)*z[4];
    z[14]= - z[12] - static_cast<T>(1)- z[13];
    z[14]=z[8]*z[14];
    z[15]=n<T>(1,2)*z[4];
    z[16]= - z[15] - z[5];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[8]*z[14];
    z[14]=z[14] + n<T>(1,8)*z[10];
    z[14]=z[6]*z[14];
    z[16]= - static_cast<T>(1)+ z[15];
    z[12]=z[12] + n<T>(1,2)*z[16] + z[2];
    z[16]=n<T>(13,4)*z[5] + n<T>(1,2) + z[4];
    z[17]=n<T>(1,2)*z[2];
    z[18]=z[17] + 1;
    z[19]=z[8]*z[18];
    z[16]=n<T>(1,2)*z[16] + z[19];
    z[16]=z[8]*z[16];
    z[12]=z[14] + n<T>(1,2)*z[12] + z[16];
    z[12]=z[6]*z[12];
    z[14]=n<T>(5,2) - 3*z[1];
    z[14]=z[5]*z[14];
    z[16]=z[2] - 1;
    z[19]=z[8]*z[16];
    z[14]=z[14] + z[19];
    z[19]=n<T>(1,2)*z[1];
    z[18]=z[18]*z[19];
    z[20]= - n<T>(5,2) - z[4];
    z[21]=n<T>(7,4) - z[2];
    z[21]=z[2]*z[21];
    z[12]=z[12] + z[18] + n<T>(1,4)*z[20] + z[21] + n<T>(1,2)*z[14];
    z[12]=z[6]*z[12];
    z[14]= - n<T>(1,2) - z[2];
    z[14]=z[2]*z[14];
    z[14]= - z[1] + n<T>(11,2) + z[14];
    z[14]=z[14]*z[19];
    z[18]=z[5]*z[1];
    z[20]= - static_cast<T>(11)+ 7*z[1];
    z[20]=z[20]*z[18];
    z[21]=z[2] - 3;
    z[21]=z[21]*z[2];
    z[21]=z[21] + 3;
    z[22]=z[2]*z[21];
    z[14]=n<T>(1,4)*z[20] + z[14] - static_cast<T>(1)+ z[22];
    z[12]=n<T>(1,2)*z[14] + z[12];
    z[14]= - static_cast<T>(1)+ n<T>(3,4)*z[4];
    z[20]=n<T>(1,8)*z[4];
    z[22]=z[20] + n<T>(3,8)*z[5];
    z[23]=static_cast<T>(1)+ z[22];
    z[23]=z[8]*z[23];
    z[23]=z[23] + n<T>(5,4)*z[5] + z[14];
    z[23]=z[8]*z[23];
    z[23]=z[23] - n<T>(1,4)*z[10];
    z[23]=z[6]*z[23];
    z[24]=n<T>(9,8)*z[5];
    z[14]= - z[24] - n<T>(1,2)*z[14] + z[2];
    z[14]=z[8]*z[14];
    z[25]=n<T>(3,4)*z[5];
    z[26]=static_cast<T>(5)- 3*z[4];
    z[14]=z[23] + z[14] + z[25] + n<T>(1,4)*z[26] + z[2];
    z[14]=z[6]*z[14];
    z[23]=z[2] - n<T>(7,2);
    z[26]= - z[23]*z[17];
    z[15]=static_cast<T>(1)+ z[15];
    z[16]=z[2]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[15]*z[19];
    z[16]=n<T>(1,2) - z[1];
    z[16]=z[16]*z[24];
    z[14]=n<T>(1,2)*z[14] + z[16] + z[15] + z[26] - static_cast<T>(2)+ n<T>(3,16)*z[4];
    z[14]=z[6]*z[14];
    z[15]=static_cast<T>(5)- z[13];
    z[16]= - z[2]*z[23];
    z[16]= - n<T>(9,2) + z[16];
    z[16]=z[2]*z[16];
    z[23]=npow(z[2],2);
    z[23]= - static_cast<T>(3)+ n<T>(1,2)*z[23];
    z[23]=z[1]*z[23];
    z[15]=n<T>(1,4)*z[23] + n<T>(1,2)*z[15] + z[16];
    z[15]=z[1]*z[15];
    z[16]= - n<T>(3,8) + z[1];
    z[16]=z[16]*z[18];
    z[15]=z[15] + z[16];
    z[13]=z[13] + z[25];
    z[16]=static_cast<T>(1)- z[13];
    z[16]=z[8]*z[16];
    z[10]=z[16] + n<T>(1,16)*z[10];
    z[10]=z[6]*z[10];
    z[16]=z[17] - static_cast<T>(1)+ z[22];
    z[10]=3*z[16] + z[10];
    z[10]=z[6]*z[10];
    z[13]=z[21] - z[13];
    z[13]=z[1]*z[13];
    z[10]=z[10] + z[13];
    z[10]=z[6]*z[10];
    z[13]= - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[13]=z[2]*z[13];
    z[13]=n<T>(3,2) + z[13];
    z[13]=z[2]*z[13];
    z[13]=n<T>(3,16)*z[5] + z[13] - static_cast<T>(1)+ n<T>(1,16)*z[4];
    z[13]=z[13]*npow(z[1],2);
    z[10]=z[10] + z[13];
    z[10]=z[3]*z[10];
    z[10]=z[10] + n<T>(1,2)*z[15] + z[14];
    z[10]=z[3]*z[10];
    z[10]=n<T>(1,2)*z[12] + z[10];
    z[10]=z[3]*z[10];
    z[12]=n<T>(3,4) - z[7];
    z[12]=z[5]*z[12];
    z[13]=npow(z[7],2)*z[25];
    z[13]=z[13] - n<T>(3,4)*z[7] + z[2];
    z[13]=z[8]*z[13];
    z[12]=z[13] + n<T>(5,4) + z[12];
    z[12]=z[8]*z[12];
    z[13]= - n<T>(3,2) - z[2];
    z[14]= - z[1] + n<T>(11,4) + z[7];
    z[14]=z[5]*z[14];
    z[12]=z[12] + n<T>(1,2)*z[13] + z[14];
    z[13]= - static_cast<T>(1)+ n<T>(3,2)*z[7];
    z[9]=z[13]*z[9];
    z[9]= - n<T>(11,2) + z[9];
    z[9]=z[8]*z[9];
    z[9]=n<T>(1,4)*z[9] - z[20] - z[5];
    z[9]=z[8]*z[9];
    z[11]=n<T>(1,8)*z[11];
    z[13]=z[4] + 9*z[5];
    z[11]=z[13]*z[11];
    z[13]=n<T>(1,2) + z[5];
    z[9]=z[11] + n<T>(1,4)*z[13] + z[9];
    z[9]=z[6]*z[9];
    z[9]=n<T>(1,2)*z[12] + z[9];
    z[9]=z[6]*z[9];
    z[11]=static_cast<T>(1)+ z[2];
    z[11]=z[11]*z[17];
    z[12]=z[7] + n<T>(5,2);
    z[11]=z[11] - z[12];
    z[12]=z[7]*z[12];
    z[12]=n<T>(3,2) + z[12];
    z[13]=z[19] - static_cast<T>(3)- z[7];
    z[13]=z[1]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[5]*z[12];
    z[11]=z[12] + n<T>(1,2)*z[11] + z[1];
    z[9]=n<T>(1,2)*z[11] + z[9];

    r += n<T>(1,2)*z[9] + z[10];
 
    return r;
}

template double qqb_2lha_r2055(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2055(const std::array<dd_real,30>&);
#endif
