#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r899(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[2];
    z[2]=k[3];
    z[3]=k[5];
    z[4]=k[8];
    z[5]=k[4];
    z[6]=npow(z[5],2);
    z[7]=z[2] + 2*z[5];
    z[8]= - n<T>(3,4) - z[7];
    z[8]=z[8]*z[6];
    z[9]=z[2]*z[4];
    z[10]=static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[10]=z[1]*z[10];
    z[7]=n<T>(1,2)*z[10] - n<T>(1,4) - z[7];
    z[7]=z[1]*z[7];
    z[10]=n<T>(1,2) + z[2];
    z[10]=n<T>(1,2)*z[10] + z[5];
    z[10]=z[5]*z[10];
    z[7]=3*z[10] + z[7];
    z[7]=z[1]*z[7];
    z[7]=z[8] + z[7];
    z[7]=z[1]*z[7];
    z[8]=static_cast<T>(1)+ z[2];
    z[8]=n<T>(1,2)*z[8] + z[5];
    z[8]=z[8]*npow(z[5],3);
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - static_cast<T>(1)- n<T>(1,2)*z[2];
    z[8]=n<T>(1,2)*z[8] - z[5];
    z[6]=z[8]*z[6];
    z[8]=z[1] - 1;
    z[8]=z[4]*z[8];
    z[9]=z[9] + static_cast<T>(7)- z[8];
    z[10]=n<T>(1,4)*z[1];
    z[9]=z[9]*z[10];
    z[9]=z[9] - n<T>(15,4)*z[5] - static_cast<T>(1)- n<T>(3,4)*z[2];
    z[9]=z[1]*z[9];
    z[10]=13*z[5] + static_cast<T>(5)+ 3*z[2];
    z[10]=z[5]*z[10];
    z[9]=n<T>(1,4)*z[10] + z[9];
    z[9]=z[1]*z[9];
    z[6]=z[7] + z[6] + z[9];
    z[6]=z[3]*z[6];
    z[7]= - static_cast<T>(1)- n<T>(5,2)*z[5];
    z[8]=static_cast<T>(1)- n<T>(1,4)*z[8];
    z[8]=z[1]*z[8];
    z[7]=n<T>(1,2)*z[7] + z[8];
    z[7]=z[1]*z[7];
    z[8]=n<T>(1,2) + z[5];
    z[8]=z[5]*z[8];

    r += z[6] + z[7] + n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r899(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r899(const std::array<dd_real,30>&);
#endif
