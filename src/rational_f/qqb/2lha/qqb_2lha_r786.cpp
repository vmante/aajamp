#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r786(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[5];
    z[4]=k[7];
    z[5]=k[9];
    z[6]=k[8];
    z[7]=k[10];
    z[8]=z[7] + 1;
    z[9]=n<T>(3,2)*z[5];
    z[10]= - z[8]*z[9];
    z[10]=z[10] - n<T>(1,2) + 3*z[7];
    z[11]=n<T>(1,2)*z[6];
    z[12]=z[11] + z[7];
    z[8]=z[5]*z[8];
    z[8]=n<T>(3,8)*z[8] - n<T>(5,8) - z[7];
    z[8]=z[5]*z[8];
    z[8]=n<T>(1,4)*z[12] + z[8];
    z[8]=z[1]*z[8];
    z[12]=n<T>(1,2)*z[2];
    z[13]=static_cast<T>(1)- n<T>(1,2)*z[5];
    z[13]=z[1]*z[13];
    z[13]= - z[12] + z[13];
    z[13]=z[5]*z[13];
    z[9]= - z[7] + z[9];
    z[9]=n<T>(1,2)*z[9] + z[13];
    z[9]=z[9]*z[12];
    z[13]= - static_cast<T>(1)+ z[2];
    z[13]=z[4]*z[13];
    z[12]=z[13] - z[12] + n<T>(1,2) + z[1];
    z[12]=z[2]*z[12];
    z[13]=z[1] - 1;
    z[12]= - z[13] + z[12];
    z[14]=n<T>(1,2)*z[4];
    z[12]=z[12]*z[14];
    z[8]=z[12] + z[9] + n<T>(1,4)*z[10] + z[8];
    z[8]=z[3]*z[8];
    z[9]=n<T>(1,2) - z[2];
    z[9]=z[2]*z[9]*npow(z[5],2);
    z[9]=z[11] + z[9];
    z[10]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[10]=z[2]*z[10];
    z[10]= - n<T>(1,2)*z[13] + z[10];
    z[10]=z[4]*z[10];
    z[10]=n<T>(1,4) + z[10];
    z[10]=z[4]*z[10];
    z[8]=z[8] + n<T>(1,4)*z[9] + z[10];
    z[8]=z[3]*z[8];
    z[9]= - z[4]*z[13];
    z[9]=n<T>(1,2) + z[9];
    z[9]=z[9]*z[14];
    z[8]=z[9] + z[8];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r786(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r786(const std::array<dd_real,30>&);
#endif
