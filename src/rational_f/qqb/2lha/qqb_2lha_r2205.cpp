#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2205(const std::array<T,30>& k) {
  T z[40];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[11];
    z[9]=k[15];
    z[10]=z[6] - 1;
    z[11]=3*z[3];
    z[12]=z[10]*z[11];
    z[13]=z[6]*z[9];
    z[14]=3*z[13];
    z[15]=z[9] - 1;
    z[16]=z[15]*z[5];
    z[12]=z[12] - z[14] + n<T>(1,2)*z[15] - z[16];
    z[17]=z[4] + n<T>(1,2);
    z[18]=3*z[4];
    z[17]=z[17]*z[18];
    z[19]=z[17] + 1;
    z[19]=z[19]*z[4];
    z[20]=n<T>(1,2)*z[3];
    z[21]=z[20] - 1;
    z[22]=z[21]*z[3];
    z[19]=z[19] - z[22];
    z[23]=z[19]*z[2];
    z[24]=n<T>(7,2) - z[4];
    z[24]=z[24]*z[18];
    z[24]=static_cast<T>(5)+ z[24];
    z[24]=z[4]*z[24];
    z[25]=static_cast<T>(5)- n<T>(3,2)*z[3];
    z[25]=z[3]*z[25];
    z[24]= - n<T>(3,2)*z[23] + z[25] + z[24];
    z[25]=n<T>(1,2)*z[2];
    z[24]=z[24]*z[25];
    z[26]=n<T>(1,8)*z[3];
    z[27]= - static_cast<T>(19)+ z[11];
    z[27]=z[27]*z[26];
    z[28]=z[4] - n<T>(3,2);
    z[29]=z[28]*z[18];
    z[24]=z[24] + z[27] + z[29];
    z[24]=z[2]*z[24];
    z[27]=z[3] - n<T>(9,4);
    z[24]=z[24] - n<T>(1,2)*z[27] - 2*z[4];
    z[24]=z[2]*z[24];
    z[12]=n<T>(1,4)*z[12] + z[24];
    z[12]=z[8]*z[12];
    z[24]=n<T>(1,4)*z[4];
    z[29]= - n<T>(47,2) + 15*z[4];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(21,2) + z[29];
    z[29]=z[29]*z[24];
    z[30]= - n<T>(21,8) + z[3];
    z[30]=z[3]*z[30];
    z[29]=z[23] + z[30] + z[29];
    z[29]=z[2]*z[29];
    z[30]=n<T>(1,2)*z[4];
    z[31]=9*z[4];
    z[32]= - n<T>(49,2) + z[31];
    z[32]=z[4]*z[32];
    z[32]=n<T>(25,2) + z[32];
    z[32]=z[32]*z[30];
    z[27]= - z[3]*z[27];
    z[27]=z[27] + z[32];
    z[27]=n<T>(1,2)*z[27] + z[29];
    z[27]=z[2]*z[27];
    z[10]=z[10]*z[20];
    z[29]= - z[31] + n<T>(25,2) - 3*z[6];
    z[29]=z[29]*z[24];
    z[32]=z[5] + 5;
    z[32]=z[9]*z[32];
    z[32]= - static_cast<T>(7)+ z[32];
    z[33]= - static_cast<T>(1)+ n<T>(3,4)*z[9];
    z[33]=z[6]*z[33];
    z[10]=z[12] + z[27] + z[29] + z[10] + n<T>(1,8)*z[32] + z[33];
    z[10]=z[8]*z[10];
    z[12]=z[6] - n<T>(5,2);
    z[27]=z[6]*z[12];
    z[27]=n<T>(3,2) + z[27];
    z[27]=z[27]*z[11];
    z[29]=n<T>(1,2)*z[5];
    z[32]=z[29] + 1;
    z[32]=z[9]*z[32];
    z[33]=n<T>(5,2)*z[9] - z[14];
    z[33]=z[6]*z[33];
    z[34]= - z[18] + n<T>(17,2) - z[6];
    z[34]=z[4]*z[34];
    z[27]=z[34] + z[27] + z[33] - static_cast<T>(3)+ z[32];
    z[32]=z[11] - 13;
    z[26]=z[32]*z[26];
    z[33]= - n<T>(5,4) + z[4];
    z[33]=z[33]*z[18];
    z[33]= - n<T>(13,8) + z[33];
    z[33]=z[4]*z[33];
    z[26]=n<T>(3,4)*z[23] + z[26] + z[33];
    z[26]=z[2]*z[26];
    z[33]=n<T>(3,2)*z[4];
    z[34]= - n<T>(11,2) + z[4];
    z[34]=z[34]*z[33];
    z[34]=static_cast<T>(5)+ z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(7,4)*z[3] + z[34];
    z[26]=n<T>(1,2)*z[34] + z[26];
    z[26]=z[2]*z[26];
    z[26]=n<T>(1,4)*z[27] + z[26];
    z[26]=z[8]*z[26];
    z[19]=z[19]*z[25];
    z[27]=z[3] - 3;
    z[34]=n<T>(1,4)*z[3];
    z[35]= - z[27]*z[34];
    z[36]=static_cast<T>(2)- z[18];
    z[36]=z[4]*z[36];
    z[36]=n<T>(3,4) + z[36];
    z[36]=z[4]*z[36];
    z[35]= - z[19] + z[35] + z[36];
    z[35]=z[2]*z[35];
    z[14]=z[9] + z[14];
    z[14]=z[6]*z[14];
    z[14]= - z[9] + z[14];
    z[36]=n<T>(1,2)*z[6];
    z[37]=z[36] - 1;
    z[37]=z[37]*z[6];
    z[38]=n<T>(1,2) + z[37];
    z[38]=z[3]*z[38];
    z[14]=n<T>(1,2)*z[14] + z[38];
    z[38]=n<T>(1,2) - z[6];
    z[38]=z[6]*z[38];
    z[39]=n<T>(7,2) - z[18];
    z[39]=z[4]*z[39];
    z[38]=z[39] - n<T>(1,2) + z[38];
    z[38]=z[4]*z[38];
    z[14]=z[26] + z[35] + n<T>(1,2)*z[14] + z[38];
    z[14]=z[8]*z[14];
    z[26]=n<T>(1,2) - z[4];
    z[26]=z[26]*z[18];
    z[26]=n<T>(1,2) + z[26];
    z[26]=z[4]*z[26];
    z[26]= - z[19] + z[20] + z[26];
    z[26]=z[2]*z[26];
    z[35]=npow(z[6],2);
    z[13]=n<T>(3,2)*z[9] - z[13];
    z[13]=z[13]*z[35];
    z[13]= - n<T>(1,2)*z[9] + z[13];
    z[12]=z[12]*z[36];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[6]*z[12];
    z[12]= - n<T>(1,4) + z[12];
    z[12]=z[3]*z[12];
    z[28]= - z[28]*z[33];
    z[28]=z[28] - static_cast<T>(1)- z[37];
    z[28]=z[4]*z[28];
    z[12]=z[26] + z[28] + n<T>(1,2)*z[13] + z[12];
    z[12]=z[7]*z[12]*npow(z[8],2);
    z[12]=z[14] + n<T>(1,2)*z[12];
    z[12]=z[7]*z[12];
    z[13]=n<T>(5,2) - z[18];
    z[13]=z[4]*z[13];
    z[13]=static_cast<T>(1)+ z[13];
    z[13]=z[4]*z[13];
    z[13]= - z[19] - z[22] + z[13];
    z[13]=z[13]*z[25];
    z[14]=z[3] - n<T>(1,2);
    z[14]=z[14]*z[34];
    z[19]= - n<T>(7,2) + z[5];
    z[19]=z[5]*z[19];
    z[19]=n<T>(9,2) + z[19];
    z[19]=z[19]*z[36];
    z[22]= - n<T>(17,2) + z[9];
    z[19]=z[19] + n<T>(1,4)*z[22] + z[5];
    z[19]=z[6]*z[19];
    z[22]=n<T>(19,2) - z[31];
    z[22]=z[4]*z[22];
    z[22]= - 3*z[35] + z[22];
    z[22]=z[22]*z[24];
    z[10]=z[12] + z[10] + z[13] + z[22] + z[14] + n<T>(1,4) + z[19];
    z[10]=z[7]*z[10];
    z[12]=n<T>(5,2) - z[3];
    z[12]=z[12]*z[11];
    z[13]=n<T>(31,2) - z[18];
    z[13]=z[4]*z[13];
    z[13]=n<T>(15,2) + z[13];
    z[13]=z[4]*z[13];
    z[12]=z[12] + z[13];
    z[12]=n<T>(1,2)*z[12] - z[23];
    z[12]=z[12]*z[25];
    z[13]=z[18] - n<T>(13,2);
    z[19]=z[13]*z[30];
    z[22]= - static_cast<T>(2)+ n<T>(3,4)*z[3];
    z[22]=z[3]*z[22];
    z[12]=z[12] + z[22] + z[19];
    z[12]=z[2]*z[12];
    z[19]=n<T>(7,2) - z[3];
    z[19]=z[3]*z[19];
    z[19]=n<T>(5,2) + z[19];
    z[12]=z[12] + n<T>(1,4)*z[19] - z[4];
    z[12]=z[2]*z[12];
    z[19]=z[27]*z[20];
    z[17]=z[19] - z[17];
    z[17]=3*z[17] + z[23];
    z[17]=z[17]*z[25];
    z[19]= - z[32]*z[34];
    z[17]=z[17] + z[19] + 5*z[4];
    z[17]=z[2]*z[17];
    z[19]= - static_cast<T>(1)+ z[34];
    z[19]=z[3]*z[19];
    z[17]=z[17] - static_cast<T>(1)+ z[19];
    z[17]=z[2]*z[17];
    z[19]=static_cast<T>(1)- z[29];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[2]*z[17];
    z[15]=n<T>(1,2)*z[16] - z[15];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[8]*z[15];
    z[12]=n<T>(1,2)*z[15] - n<T>(1,8)*z[16] + z[12];
    z[12]=z[8]*z[12];
    z[15]= - n<T>(5,2) + z[5];
    z[15]=z[5]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];
    z[16]=z[5] - n<T>(3,2);
    z[16]=z[16]*z[5];
    z[17]=n<T>(1,2) - z[16];
    z[17]=z[1]*z[17];
    z[15]=z[15] + z[17];
    z[15]=z[6]*z[15];
    z[17]=3*z[5];
    z[19]= - n<T>(23,4) + z[17];
    z[19]=z[5]*z[19];
    z[22]=n<T>(3,4) - z[5];
    z[22]=z[1]*z[22];
    z[15]=z[15] + 3*z[22] + static_cast<T>(2)+ z[19];
    z[15]=z[6]*z[15];
    z[19]=z[1] + 3;
    z[22]= - static_cast<T>(1)- z[6];
    z[22]=z[6]*z[1]*z[22];
    z[26]=z[18]*z[1];
    z[27]= - z[26] - static_cast<T>(3)+ n<T>(5,2)*z[1];
    z[27]=z[4]*z[27];
    z[22]=z[27] + z[22] + z[19];
    z[22]=z[22]*z[30];
    z[11]=z[21]*z[11];
    z[13]=z[4]*z[13];
    z[13]= - static_cast<T>(3)+ z[13];
    z[13]=z[4]*z[13];
    z[11]=z[23] + z[11] + z[13];
    z[11]=z[11]*z[25];
    z[13]=n<T>(9,2) - z[1];
    z[21]= - n<T>(3,2) + z[1];
    z[21]=z[4]*z[21];
    z[13]=n<T>(1,2)*z[13] + z[21];
    z[13]=z[4]*z[13];
    z[19]=n<T>(1,2)*z[19] - z[3];
    z[19]=z[3]*z[19];
    z[11]=z[11] + z[13] - static_cast<T>(1)+ z[19];
    z[11]=z[11]*z[25];
    z[13]=2*z[1];
    z[19]=2*z[5];
    z[21]= - n<T>(19,2) + z[9];
    z[10]=z[10] + z[12] + z[11] + z[22] + z[14] + z[15] - z[13] + n<T>(1,4)*
    z[21] + z[19];
    z[10]=z[7]*z[10];
    z[11]=n<T>(1,2)*z[1];
    z[12]= - static_cast<T>(13)+ z[1];
    z[12]=z[12]*z[11];
    z[14]=static_cast<T>(1)- z[1];
    z[14]=z[14]*z[26];
    z[12]=z[14] + static_cast<T>(3)+ z[12];
    z[12]=z[4]*z[12];
    z[12]=z[12] - n<T>(7,2) + z[1];
    z[12]=z[4]*z[12];
    z[14]=z[20]*z[1];
    z[15]=z[1] + 1;
    z[21]=z[15]*z[14];
    z[22]=z[21] - n<T>(1,2) - z[1];
    z[22]=z[3]*z[22];
    z[12]=z[12] + static_cast<T>(1)+ z[22];
    z[12]=z[2]*z[12];
    z[22]= - n<T>(1,4) - z[1];
    z[22]=z[1]*z[22];
    z[21]= - z[21] + n<T>(1,4) + z[22];
    z[20]=z[21]*z[20];
    z[21]= - n<T>(1,2) + z[1];
    z[21]=z[21]*z[26];
    z[22]=n<T>(19,2) - z[1];
    z[22]=z[1]*z[22];
    z[22]= - static_cast<T>(3)+ z[22];
    z[21]=n<T>(1,2)*z[22] + z[21];
    z[21]=z[4]*z[21];
    z[22]= - n<T>(7,4) + z[1];
    z[22]=z[1]*z[22];
    z[21]=z[21] + n<T>(7,4) + z[22];
    z[21]=z[21]*z[30];
    z[12]=n<T>(1,4)*z[12] + z[21] + z[20] + z[13] + n<T>(1,4) - z[5];
    z[12]=z[2]*z[12];
    z[13]=z[5] - n<T>(1,2);
    z[20]=npow(z[5],2);
    z[21]= - z[13]*z[20];
    z[22]=n<T>(1,2) + z[5];
    z[22]=z[5]*z[22];
    z[22]=n<T>(1,2) + z[22];
    z[22]=z[22]*z[11];
    z[21]=z[21] + z[22];
    z[21]=z[1]*z[21];
    z[16]=n<T>(1,2) + z[16];
    z[16]=z[16]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[21];
    z[16]=z[6]*z[16];
    z[20]=n<T>(3,2) - 4*z[5];
    z[20]=z[5]*z[20];
    z[21]=static_cast<T>(1)+ z[19];
    z[21]=z[1]*z[21];
    z[20]=z[21] - n<T>(1,4) + z[20];
    z[20]=z[1]*z[20];
    z[19]= - n<T>(5,2) + z[19];
    z[19]=z[5]*z[19];
    z[19]=n<T>(3,4) + z[19];
    z[19]=z[5]*z[19];
    z[16]=z[16] + z[19] + z[20];
    z[16]=z[6]*z[16];
    z[19]=n<T>(1,4) - z[1];
    z[19]=z[1]*z[19];
    z[20]=npow(z[1],2);
    z[21]=z[6]*z[20];
    z[19]=z[19] - n<T>(1,4)*z[21];
    z[19]=z[6]*z[19];
    z[18]= - z[20]*z[18];
    z[11]= - static_cast<T>(3)+ z[11];
    z[11]=z[1]*z[11];
    z[11]=z[11] + z[18];
    z[11]=z[11]*z[24];
    z[18]=n<T>(7,2) - 5*z[1];
    z[18]=z[1]*z[18];
    z[11]=z[11] + n<T>(1,4)*z[18] + z[19];
    z[11]=z[4]*z[11];
    z[15]=z[15]*z[34];
    z[15]=z[15] + n<T>(3,4) + z[1];
    z[14]=z[15]*z[14];
    z[13]=z[5]*z[13];
    z[15]=n<T>(3,2)*z[1] - static_cast<T>(1)- z[17];
    z[15]=z[1]*z[15];

    r += z[10] + z[11] + z[12] + n<T>(3,2)*z[13] + z[14] + z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r2205(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2205(const std::array<dd_real,30>&);
#endif
