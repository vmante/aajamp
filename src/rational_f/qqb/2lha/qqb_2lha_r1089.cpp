#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1089(const std::array<T,30>& k) {
  T z[39];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[17];
    z[5]=k[2];
    z[6]=k[8];
    z[7]=k[11];
    z[8]=k[4];
    z[9]=k[7];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=k[5];
    z[13]=k[15];
    z[14]=k[12];
    z[15]=k[6];
    z[16]=static_cast<T>(1)+ n<T>(13,12)*z[4];
    z[17]=z[16]*z[4];
    z[18]=n<T>(13,12)*z[13];
    z[19]=z[18] + 1;
    z[19]=z[19]*z[13];
    z[20]=z[17] + z[19];
    z[21]=z[11] + 1;
    z[22]=n<T>(1,4)*z[11];
    z[23]=z[21]*z[22];
    z[24]=z[8]*npow(z[11],2);
    z[23]=n<T>(1,2)*z[24] + z[23] + z[20];
    z[23]=z[8]*z[23];
    z[24]=z[2]*z[10];
    z[25]=n<T>(1,2)*z[15];
    z[26]= - static_cast<T>(1)+ z[25];
    z[26]=z[10]*z[26];
    z[26]=z[24] - z[26];
    z[27]=n<T>(1,4)*z[6];
    z[28]=static_cast<T>(1)+ z[5];
    z[28]=z[28]*z[27];
    z[29]=n<T>(1,2)*z[10];
    z[30]=n<T>(5,3) - z[29];
    z[30]=z[11]*z[30];
    z[30]=z[30] - n<T>(5,2)*z[10] + static_cast<T>(5)+ n<T>(3,2)*z[6];
    z[22]=z[30]*z[22];
    z[30]= - z[14] + n<T>(1,4)*z[12];
    z[22]=z[23] + z[22] + z[28] + z[30] - n<T>(1,4)*z[26];
    z[23]=n<T>(1,2)*z[9];
    z[26]=n<T>(1,2)*z[12];
    z[28]=n<T>(13,3)*z[13];
    z[25]= - z[28] - z[26] + n<T>(9,2)*z[14] - z[23] - n<T>(13,3) - z[25];
    z[31]=n<T>(1,2)*z[4];
    z[32]=n<T>(13,3)*z[4];
    z[33]=static_cast<T>(9)- z[32];
    z[33]=z[33]*z[31];
    z[34]=n<T>(5,2) - z[28];
    z[34]=z[13]*z[34];
    z[33]=z[34] + z[33];
    z[20]=z[8]*z[20];
    z[20]=n<T>(1,2)*z[33] + z[20];
    z[20]=z[8]*z[20];
    z[20]=n<T>(1,2)*z[25] + z[20];
    z[20]=z[8]*z[20];
    z[25]=static_cast<T>(7)- n<T>(13,2)*z[13];
    z[25]=z[13]*z[25];
    z[33]=n<T>(13,2)*z[4];
    z[34]=static_cast<T>(7)- z[33];
    z[34]=z[4]*z[34];
    z[25]=z[25] + z[34];
    z[25]=z[8]*z[25];
    z[34]=z[12] + 5*z[14];
    z[25]=n<T>(1,6)*z[25] - n<T>(13,6)*z[4] + n<T>(1,4)*z[34] - z[28];
    z[25]=z[8]*z[25];
    z[25]=n<T>(1,6) + z[25];
    z[25]=z[8]*z[25];
    z[28]=n<T>(13,6)*z[3];
    z[35]= - z[13] - z[4];
    z[35]=z[35]*npow(z[8],3)*z[28];
    z[25]=z[25] + z[35];
    z[25]=z[3]*z[25];
    z[35]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[20]=z[25] - n<T>(1,3)*z[35] + z[20];
    z[25]=n<T>(1,2)*z[3];
    z[20]=z[20]*z[25];
    z[36]=z[15]*z[29];
    z[37]=13*z[13];
    z[38]=static_cast<T>(1)- z[37];
    z[38]=z[13]*z[38];
    z[36]=3*z[11] + n<T>(1,3)*z[38] + static_cast<T>(7)+ z[36];
    z[21]=z[2]*z[11]*z[21];
    z[21]=n<T>(1,2)*z[36] + z[21];
    z[36]= - z[5]*z[16];
    z[36]=n<T>(13,6) + z[36];
    z[36]=z[4]*z[36];
    z[21]=n<T>(1,2)*z[21] + z[36];
    z[36]=z[19] + n<T>(1,2)*z[11];
    z[17]=n<T>(1,2)*z[36] + z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,2)*z[21] + z[17];
    z[17]=z[8]*z[17];
    z[21]=static_cast<T>(1)- z[23];
    z[23]= - z[5] - n<T>(7,3) + n<T>(3,2)*z[9];
    z[23]=z[5]*z[23];
    z[21]=3*z[21] + z[23];
    z[21]=z[21]*z[27];
    z[23]=n<T>(1,3) - n<T>(5,4)*z[9];
    z[21]= - n<T>(1,8)*z[10] + z[21] + n<T>(1,2)*z[23] + z[14];
    z[23]=z[29] + static_cast<T>(1)+ n<T>(7,6)*z[6];
    z[23]=z[11]*z[23];
    z[23]=z[23] + n<T>(5,3)*z[6] + 3*z[10];
    z[23]=z[11]*z[23];
    z[23]=z[24] + z[23] + static_cast<T>(1)+ z[10];
    z[23]=z[2]*z[23];
    z[24]=static_cast<T>(1)- n<T>(1,8)*z[6];
    z[24]=n<T>(1,3)*z[24] - n<T>(1,16)*z[10];
    z[24]=z[11]*z[24];
    z[17]=z[20] + z[17] + n<T>(1,8)*z[23] + n<T>(1,2)*z[21] + z[24];
    z[17]=z[7]*z[17];
    z[20]=13*z[4];
    z[21]=z[20] - 1;
    z[23]=n<T>(1,12)*z[4];
    z[24]= - z[21]*z[23];
    z[27]=z[16]*z[31];
    z[19]=z[19] + z[27];
    z[19]=z[8]*z[19];
    z[18]=z[19] + z[24] + z[18] + z[30];
    z[18]=z[8]*z[18];
    z[19]= - z[4]*z[21];
    z[19]=z[19] + z[37] - z[34];
    z[19]=z[8]*z[19];
    z[21]=static_cast<T>(3)+ z[12];
    z[19]=n<T>(1,4)*z[19] + n<T>(1,2)*z[21] - z[32];
    z[19]=z[8]*z[19];
    z[21]= - z[3]*npow(z[8],2)*z[33];
    z[19]=z[21] + n<T>(1,6)*z[2] + z[19];
    z[19]=z[19]*z[25];
    z[21]=n<T>(1,8)*z[12];
    z[18]=z[19] + z[18] - z[21] - n<T>(1,3) - n<T>(1,8)*z[15];
    z[18]=z[3]*z[18];
    z[17]=z[17] + n<T>(1,2)*z[22] + z[18];
    z[17]=z[7]*z[17];
    z[18]=z[2] - z[1];
    z[19]= - n<T>(13,3) + z[26];
    z[19]=n<T>(1,2)*z[19] - z[18];
    z[19]=z[2]*z[19];
    z[22]= - z[20] - 25;
    z[22]=z[1]*z[22];
    z[22]= - static_cast<T>(13)+ z[22];
    z[22]=z[22]*z[23];
    z[24]=n<T>(13,2) + 19*z[1];
    z[19]=z[22] + n<T>(1,6)*z[24] + z[19];
    z[20]= - n<T>(11,2) - z[20];
    z[20]=z[20]*z[23];
    z[20]=z[20] - z[21] - z[35];
    z[20]=z[8]*z[20];
    z[21]= - z[4] + 1;
    z[21]=z[1]*z[21];
    z[22]= - z[2]*z[18];
    z[21]=z[22] + z[21];
    z[22]= - z[4] - z[35];
    z[22]=z[8]*z[22];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[21]=z[21]*z[28];
    z[19]=z[21] + n<T>(1,2)*z[19] + z[20];
    z[19]=z[3]*z[19];
    z[16]= - z[1]*z[16];
    z[16]= - n<T>(13,12) + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[16] + n<T>(3,4)*z[12] - z[18];
    z[16]=n<T>(1,2)*z[16] + z[19];
    z[16]=z[3]*z[16];

    r += z[16] + z[17];
 
    return r;
}

template double qqb_2lha_r1089(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1089(const std::array<dd_real,30>&);
#endif
