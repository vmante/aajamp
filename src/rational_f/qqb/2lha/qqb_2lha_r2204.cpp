#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2204(const std::array<T,30>& k) {
  T z[28];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[17];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[11];
    z[8]=n<T>(3,2)*z[7];
    z[9]=n<T>(7,2)*z[2];
    z[10]=static_cast<T>(3)+ z[9];
    z[10]=z[2]*z[10];
    z[10]=n<T>(1,2) + z[10];
    z[10]=z[10]*z[8];
    z[10]=z[10] - static_cast<T>(5)- z[9];
    z[10]=z[7]*z[10];
    z[11]=n<T>(1,2)*z[2];
    z[12]=z[11] + 1;
    z[13]= - static_cast<T>(1)- n<T>(3,4)*z[2];
    z[13]=z[2]*z[13];
    z[13]= - n<T>(1,4) + z[13];
    z[13]=z[7]*z[13];
    z[13]=z[13] + z[12];
    z[13]=z[2]*z[13];
    z[13]=static_cast<T>(1)+ z[13];
    z[14]=3*z[7];
    z[13]=z[3]*z[13]*z[14];
    z[10]=z[10] + z[13];
    z[10]=z[3]*z[10];
    z[13]=n<T>(1,2)*z[7];
    z[15]= - static_cast<T>(5)- 9*z[2];
    z[15]=z[15]*z[13];
    z[15]=static_cast<T>(2)+ z[15];
    z[15]=z[7]*z[15];
    z[16]= - static_cast<T>(1)+ z[13];
    z[16]=z[16]*z[13];
    z[17]=z[5]*z[7];
    z[16]=z[16] + z[17];
    z[16]=z[5]*z[16];
    z[10]=z[10] + z[15] + z[16];
    z[10]=z[3]*z[10];
    z[15]=static_cast<T>(1)- z[14];
    z[15]=z[15]*z[13];
    z[16]=2*z[17];
    z[17]= - static_cast<T>(2)+ z[13];
    z[17]=z[7]*z[17];
    z[17]=z[17] + z[16];
    z[17]=z[5]*z[17];
    z[15]=z[15] + z[17];
    z[15]=z[5]*z[15];
    z[17]=z[2] + 1;
    z[12]=z[12]*z[2];
    z[18]=n<T>(1,2) + z[12];
    z[18]=z[3]*z[18];
    z[18]= - z[17] + z[18];
    z[19]=n<T>(3,2)*z[3];
    z[18]=z[19]*z[18];
    z[18]=z[18] + 1;
    z[20]=npow(z[7],2);
    z[18]=z[20]*z[18];
    z[21]=n<T>(1,2)*z[5];
    z[22]=z[21] - 1;
    z[22]=z[20]*z[22];
    z[23]=z[22]*z[21];
    z[18]=z[23] + z[18];
    z[18]=z[3]*z[18];
    z[22]=z[5]*z[22];
    z[23]=n<T>(1,2)*z[20];
    z[22]=z[23] + z[22];
    z[22]=z[5]*z[22];
    z[18]=z[22] + z[18];
    z[18]=z[6]*z[18];
    z[10]=z[18] + z[10] + n<T>(5,2)*z[20] + z[15];
    z[10]=z[3]*z[10];
    z[15]=z[7] + 1;
    z[18]=z[7]*z[15];
    z[16]=z[18] - z[16];
    z[16]=z[5]*z[16];
    z[16]= - z[20] + z[16];
    z[16]=z[5]*z[16];
    z[10]=z[16] + z[10];
    z[10]=z[6]*z[10];
    z[16]=z[4] - 3;
    z[18]= - z[4]*z[16];
    z[18]= - static_cast<T>(3)+ z[18];
    z[18]=z[4]*z[18];
    z[18]= - static_cast<T>(3)+ z[18];
    z[18]=z[18]*z[21];
    z[20]=static_cast<T>(1)- n<T>(1,2)*z[4];
    z[20]=z[4]*z[20];
    z[20]=n<T>(1,2) + 3*z[20];
    z[22]=z[4] - n<T>(3,2);
    z[24]=z[7]*z[22];
    z[18]=z[18] + n<T>(1,2)*z[20] + z[24];
    z[18]=z[5]*z[18];
    z[16]= - z[16]*z[13];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[7]*z[16];
    z[16]=z[16] + z[18];
    z[16]=z[5]*z[16];
    z[18]=npow(z[2],3);
    z[20]=static_cast<T>(1)+ n<T>(3,2)*z[2];
    z[20]=z[20]*z[18]*z[13];
    z[24]= - n<T>(5,4) - z[2];
    z[24]=z[2]*z[24];
    z[24]= - n<T>(3,4) + z[24];
    z[24]=z[2]*z[24];
    z[20]=z[24] + z[20];
    z[20]=z[7]*z[20];
    z[12]=n<T>(3,2) + z[12];
    z[12]=n<T>(1,2)*z[12] + z[20];
    z[12]=z[3]*z[12];
    z[20]=npow(z[2],2);
    z[14]=z[14]*z[20];
    z[24]=2*z[2];
    z[25]= - static_cast<T>(1)- z[24];
    z[25]=z[25]*z[14];
    z[26]=static_cast<T>(29)+ 31*z[2];
    z[26]=z[2]*z[26];
    z[26]=static_cast<T>(9)+ z[26];
    z[25]=n<T>(1,4)*z[26] + z[25];
    z[25]=z[7]*z[25];
    z[12]=3*z[12] + z[25] - n<T>(7,2) - z[24];
    z[12]=z[3]*z[12];
    z[25]=static_cast<T>(1)+ 3*z[2];
    z[26]=z[7]*z[25]*z[24];
    z[26]=z[26] - n<T>(17,4) - 6*z[2];
    z[26]=z[7]*z[26];
    z[27]=z[7] + z[5];
    z[27]=z[5]*z[27];
    z[12]=z[12] + n<T>(3,4)*z[27] + static_cast<T>(1)+ z[26];
    z[12]=z[3]*z[12];
    z[9]= - static_cast<T>(1)- z[9];
    z[9]=z[7]*z[9];
    z[9]=static_cast<T>(2)+ z[9];
    z[9]=z[7]*z[9];
    z[8]=n<T>(3,2)*z[5] - static_cast<T>(1)+ z[8];
    z[8]=z[5]*z[8];
    z[8]= - n<T>(7,4)*z[7] + z[8];
    z[8]=z[5]*z[8];
    z[8]=z[12] + z[9] + z[8];
    z[8]=z[3]*z[8];
    z[8]=z[10] + z[8] - z[23] + z[16];
    z[8]=z[6]*z[8];
    z[9]=z[17]*z[20];
    z[10]=n<T>(1,2) + z[2];
    z[10]=z[10]*z[18];
    z[12]= - npow(z[2],5)*z[13];
    z[10]=z[10] + z[12];
    z[10]=z[7]*z[10];
    z[9]=z[10] + z[1] - n<T>(1,2)*z[9];
    z[9]=z[9]*z[19];
    z[10]=z[1] - n<T>(3,2);
    z[12]= - n<T>(1,2)*z[10] + z[24];
    z[12]=z[2]*z[12];
    z[16]= - static_cast<T>(3)- n<T>(17,2)*z[2];
    z[16]=z[16]*z[20];
    z[17]=z[7]*npow(z[2],4);
    z[16]=z[16] + n<T>(9,2)*z[17];
    z[13]=z[16]*z[13];
    z[16]=2*z[1];
    z[9]=z[9] + z[13] + z[12] + n<T>(3,2) - z[16];
    z[9]=z[3]*z[9];
    z[12]=static_cast<T>(1)+ 4*z[2];
    z[12]=z[2]*z[12];
    z[13]=z[7]*z[18];
    z[12]=z[12] - n<T>(5,2)*z[13];
    z[12]=z[7]*z[12];
    z[13]=static_cast<T>(1)+ z[5];
    z[13]=z[21]*z[1]*z[13];
    z[17]= - static_cast<T>(3)+ z[1];
    z[17]=z[2]*z[17];
    z[17]= - n<T>(9,2) + z[17];
    z[9]=z[9] + z[13] + n<T>(1,2)*z[17] + z[12];
    z[9]=z[3]*z[9];
    z[12]=z[14] - z[25];
    z[12]=z[7]*z[12];
    z[12]=z[12] + z[1] + z[2];
    z[13]=z[1] - n<T>(1,2);
    z[14]=z[5]*z[1];
    z[14]=n<T>(1,2) + z[14];
    z[14]=z[5]*z[14];
    z[14]= - n<T>(1,2)*z[13] + z[14];
    z[14]=z[5]*z[14];
    z[9]=z[9] + n<T>(1,2)*z[12] + z[14];
    z[9]=z[3]*z[9];
    z[12]=npow(z[4],3);
    z[14]=static_cast<T>(2)- z[4];
    z[14]=z[14]*z[12];
    z[17]=z[4] - 1;
    z[18]=z[17]*z[4];
    z[18]=z[18] - 1;
    z[20]=z[4]*z[18];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[1]*z[20];
    z[14]=z[14] + z[20];
    z[14]=z[5]*z[14];
    z[20]=n<T>(5,2)*z[4];
    z[23]=static_cast<T>(4)- z[20];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(1,2) + z[23];
    z[23]=z[4]*z[23];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[4]*z[20];
    z[20]= - n<T>(1,2) + z[20];
    z[20]=z[1]*z[20];
    z[14]=z[14] + z[20] - n<T>(1,2) + z[23];
    z[14]=z[5]*z[14];
    z[20]=z[22] - z[1];
    z[20]=z[7] - 3*z[20];
    z[20]=z[4]*z[20];
    z[20]= - n<T>(1,2) + z[20];
    z[14]=n<T>(1,2)*z[20] + z[14];
    z[14]=z[5]*z[14];
    z[8]=z[8] + z[9] + n<T>(1,2)*z[15] + z[14];
    z[8]=z[6]*z[8];
    z[9]=n<T>(7,2)*z[4];
    z[14]=static_cast<T>(3)- z[9];
    z[14]=z[14]*z[12];
    z[9]= - static_cast<T>(3)- z[9];
    z[9]=z[4]*z[9];
    z[9]= - n<T>(5,2) + z[9];
    z[9]=z[1]*z[9];
    z[9]=7*z[12] + z[9];
    z[9]=z[1]*z[9];
    z[9]=z[14] + z[9];
    z[12]=npow(z[4],4);
    z[14]=z[17]*z[12];
    z[15]=n<T>(1,2)*z[1];
    z[17]= - static_cast<T>(1)- z[4];
    z[17]=z[4]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[4]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[15];
    z[12]=z[12] + z[17];
    z[12]=z[1]*z[12];
    z[12]= - n<T>(1,2)*z[14] + z[12];
    z[12]=z[5]*z[12];
    z[9]=n<T>(1,2)*z[9] + z[12];
    z[9]=z[5]*z[9];
    z[12]= - n<T>(3,4) + 4*z[4];
    z[12]=z[4]*z[12];
    z[14]=2*z[4];
    z[17]= - static_cast<T>(1)- z[14];
    z[17]=z[1]*z[17];
    z[12]=z[17] - static_cast<T>(1)+ z[12];
    z[12]=z[1]*z[12];
    z[14]=n<T>(7,4) - z[14];
    z[14]=z[4]*z[14];
    z[14]=n<T>(1,4) + z[14];
    z[14]=z[4]*z[14];
    z[9]=z[9] + z[12] + n<T>(1,4) + z[14];
    z[9]=z[5]*z[9];
    z[10]= - z[10]*z[15];
    z[12]= - n<T>(11,4) + z[1];
    z[12]=z[1]*z[12];
    z[14]=z[15] - 2;
    z[14]=z[14]*z[1];
    z[17]= - n<T>(3,4) - z[14];
    z[17]=z[2]*z[17];
    z[12]=z[17] + n<T>(3,4) + z[12];
    z[12]=z[2]*z[12];
    z[17]=z[1] - 1;
    z[20]=z[2]*z[17]*z[15];
    z[13]= - z[1]*z[13];
    z[13]=z[13] + z[20];
    z[13]=z[2]*z[13];
    z[20]=npow(z[1],2);
    z[13]=n<T>(1,2)*z[20] + z[13];
    z[13]=z[13]*z[19];
    z[10]=z[13] + z[10] + z[12];
    z[10]=z[3]*z[10];
    z[12]=n<T>(5,4) - z[1];
    z[12]=z[2]*z[12];
    z[12]=z[12] - n<T>(5,4) - z[14];
    z[12]=z[2]*z[12];
    z[13]=z[20]*z[5];
    z[14]= - n<T>(1,4) + z[1];
    z[14]=z[1]*z[14];
    z[14]=z[14] + n<T>(1,4)*z[13];
    z[14]=z[5]*z[14];
    z[19]=z[1]*z[17];
    z[10]=z[10] + z[14] + n<T>(5,4)*z[19] + z[12];
    z[10]=z[3]*z[10];
    z[11]= - z[11] - z[17];
    z[11]=z[2]*z[11];
    z[12]=static_cast<T>(1)+ z[21];
    z[12]=z[12]*z[13];
    z[13]= - static_cast<T>(1)+ 5*z[1];
    z[12]=n<T>(1,4)*z[13] + z[12];
    z[12]=z[5]*z[12];
    z[10]=z[10] + z[12] + z[11] - n<T>(3,4) + z[16];
    z[10]=z[3]*z[10];
    z[11]= - z[15] - n<T>(1,2) + z[4];
    z[11]=z[1]*z[11];
    z[11]= - n<T>(1,2)*z[18] + z[11];
    z[11]=3*z[11] - z[2];

    r += z[8] + z[9] + z[10] + n<T>(1,2)*z[11];
 
    return r;
}

template double qqb_2lha_r2204(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2204(const std::array<dd_real,30>&);
#endif
