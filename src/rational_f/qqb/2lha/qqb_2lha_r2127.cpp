#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2127(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[11];
    z[6]=n<T>(1010663,4320)*z[4];
    z[7]=npow(z[5],2);
    z[6]=z[6]*z[7];
    z[8]= - n<T>(958823,2) - 1010663*z[5];
    z[8]=z[5]*z[8];
    z[8]=z[6] + static_cast<T>(3)+ n<T>(1,4320)*z[8];
    z[8]=z[4]*z[8];
    z[9]=z[2] - z[1];
    z[7]= - static_cast<T>(3)+ n<T>(1010663,4320)*z[7];
    z[7]=z[4]*z[7];
    z[7]=z[7] - n<T>(984743,2880) - 9*z[9];
    z[7]=z[2]*z[7];
    z[10]=static_cast<T>(12)- n<T>(1010663,2160)*z[5];
    z[10]=z[4]*z[10];
    z[11]=z[9] + z[4];
    z[11]= - n<T>(984743,4320) - 6*z[11];
    z[11]=z[2]*z[11];
    z[10]=z[10] + z[11];
    z[10]=z[2]*z[10];
    z[11]=static_cast<T>(6)- n<T>(1010663,4320)*z[5];
    z[11]=z[5]*z[11]*npow(z[4],2);
    z[10]=z[11] + z[10];
    z[10]=z[3]*z[10];
    z[7]=z[10] + z[8] + z[7];
    z[7]=z[3]*z[7];
    z[6]=z[7] + z[6] - n<T>(984743,8640) - 3*z[9];

    r += z[6]*z[3];
 
    return r;
}

template double qqb_2lha_r2127(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2127(const std::array<dd_real,30>&);
#endif
