#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r220(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[2];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=k[11];
    z[9]=k[10];
    z[10]=n<T>(1,4)*z[2];
    z[11]=n<T>(1,2) + z[2];
    z[11]=z[11]*z[10];
    z[12]=z[1]*z[2];
    z[13]= - static_cast<T>(1)- z[2];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,4) + z[13];
    z[13]=z[13]*z[12];
    z[14]=z[2] - n<T>(1,2);
    z[15]=z[14]*z[12];
    z[15]= - z[10] + z[15];
    z[16]=n<T>(1,2)*z[3];
    z[15]=z[15]*z[16];
    z[11]=z[15] + z[11] + z[13];
    z[11]=z[3]*z[11];
    z[13]=n<T>(1,2)*z[2];
    z[15]=3*z[2];
    z[17]=z[15] - 1;
    z[18]= - z[2]*z[17];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[13];
    z[19]=z[15] + 1;
    z[20]=npow(z[2],2);
    z[21]=z[19]*z[20];
    z[22]= - z[1]*z[21];
    z[23]= - z[6] + z[1];
    z[24]=npow(z[2],3);
    z[23]=z[24]*z[23];
    z[25]=3*z[24];
    z[23]=z[25] + z[23];
    z[26]=n<T>(1,2)*z[6];
    z[23]=z[23]*z[26];
    z[18]=z[23] + z[18] + z[22];
    z[18]=z[18]*z[26];
    z[22]=n<T>(1,2)*z[20];
    z[23]= - n<T>(5,2) - z[15];
    z[23]=z[23]*z[22];
    z[27]= - n<T>(1,4)*z[3] + static_cast<T>(1)+ z[13];
    z[27]=z[3]*z[20]*z[27];
    z[28]= - z[6]*z[24];
    z[21]=z[21] + z[28];
    z[21]=z[21]*z[26];
    z[21]=z[21] + z[23] + z[27];
    z[23]=n<T>(1,2)*z[7];
    z[21]=z[21]*z[23];
    z[27]=z[13] - 1;
    z[28]=z[27]*z[2];
    z[28]=z[28] + n<T>(1,2);
    z[29]=z[28]*z[13];
    z[30]=static_cast<T>(1)+ n<T>(9,4)*z[2];
    z[30]=z[2]*z[30];
    z[30]=n<T>(1,4) + z[30];
    z[12]=z[30]*z[12];
    z[11]=z[21] + z[18] + z[11] + z[29] + z[12];
    z[11]=z[11]*z[23];
    z[12]=n<T>(1,2)*z[5];
    z[18]=npow(z[3],2);
    z[21]=npow(z[4],2);
    z[30]=z[21]*z[12]*z[18];
    z[31]= - static_cast<T>(1)- z[4];
    z[31]=z[4]*z[31]*z[18];
    z[31]=z[31] + z[30];
    z[31]=z[5]*z[31];
    z[32]=z[4] + n<T>(1,2);
    z[33]=z[32]*z[4];
    z[34]=n<T>(1,2)*z[4];
    z[35]=z[34] + 1;
    z[36]=z[4]*z[35];
    z[36]=n<T>(1,2) + z[36];
    z[36]=z[3]*z[36];
    z[36]=z[33] + z[36];
    z[36]=z[3]*z[36];
    z[37]=n<T>(3,2)*z[21];
    z[31]=z[31] - z[37] + z[36];
    z[31]=z[31]*z[12];
    z[36]= - n<T>(3,2) - z[4];
    z[36]=z[4]*z[36];
    z[36]= - n<T>(1,2) + z[36];
    z[36]=z[36]*z[16];
    z[38]= - z[21]*z[26];
    z[39]=n<T>(5,4) + z[4];
    z[39]=z[4]*z[39];
    z[31]=z[31] + z[38] + z[39] + z[36];
    z[31]=z[5]*z[31];
    z[36]=z[6] - 1;
    z[36]=z[4]*z[36];
    z[36]= - static_cast<T>(1)+ z[36];
    z[31]=n<T>(1,2)*z[36] + z[31];
    z[31]=z[8]*z[31];
    z[36]=static_cast<T>(1)+ z[1];
    z[36]=z[33]*z[36];
    z[38]=n<T>(1,2)*z[1];
    z[39]=z[38]*z[21];
    z[40]= - n<T>(1,2) + z[4];
    z[40]=z[4]*z[40];
    z[40]=z[40] + z[39];
    z[40]=z[1]*z[40];
    z[41]= - static_cast<T>(1)+ z[34];
    z[41]=z[4]*z[41];
    z[40]=z[41] + z[40];
    z[40]=z[1]*z[40];
    z[40]= - z[34] + z[40];
    z[40]=z[3]*z[40];
    z[36]=z[40] + z[36];
    z[36]=z[36]*z[16];
    z[40]=z[33] + z[39];
    z[18]=z[40]*z[18];
    z[18]=z[18] - z[30];
    z[18]=z[5]*z[18];
    z[30]= - z[21] - z[39];
    z[30]=z[1]*z[30];
    z[30]= - n<T>(1,2)*z[21] + z[30];
    z[30]=z[3]*z[30];
    z[30]= - z[33] + z[30];
    z[30]=z[3]*z[30];
    z[18]=z[18] + z[37] + z[30];
    z[12]=z[18]*z[12];
    z[18]=z[26] - n<T>(3,4)*z[1];
    z[18]=z[21]*z[18];
    z[12]=z[31] + z[12] + z[36] - z[33] + z[18];
    z[12]=n<T>(1,4)*z[12];
    z[12]=z[5]*z[12];
    z[18]=n<T>(3,2)*z[2];
    z[30]=z[18] - 1;
    z[31]= - z[30]*z[20];
    z[33]=z[4]*z[2];
    z[36]=static_cast<T>(1)- z[22];
    z[36]=z[36]*z[33];
    z[31]=z[31] + z[36];
    z[31]=z[4]*z[31];
    z[19]=z[19]*z[22];
    z[19]=z[19] + z[31];
    z[19]=z[1]*z[19];
    z[22]=3*z[20];
    z[31]=z[14]*z[22];
    z[36]=z[2] - 1;
    z[37]=z[36]*z[2];
    z[40]= - static_cast<T>(1)+ z[37];
    z[40]=z[2]*z[40];
    z[40]=z[40] - z[34];
    z[40]=z[4]*z[40];
    z[19]=z[19] + z[31] + z[40];
    z[19]=z[1]*z[19];
    z[31]= - z[2]*z[28];
    z[19]=z[31] + z[19];
    z[31]= - static_cast<T>(1)- z[18];
    z[31]=z[31]*z[20];
    z[40]=z[24]*z[4];
    z[40]=z[40] + z[25];
    z[41]= - z[40]*z[34];
    z[31]=z[31] + z[41];
    z[31]=z[31]*z[38];
    z[40]=z[40]*z[4];
    z[25]=z[40] + z[25];
    z[40]=n<T>(1,4)*z[6];
    z[25]=z[25]*z[40];
    z[27]=z[27]*z[34];
    z[40]= - static_cast<T>(1)+ n<T>(3,4)*z[2];
    z[41]=z[40] + z[27];
    z[41]=z[20]*z[41];
    z[31]= - z[25] + z[31] + z[41];
    z[31]=z[6]*z[31];
    z[19]=n<T>(1,2)*z[19] + z[31];
    z[19]=z[19]*z[26];
    z[17]=z[17]*z[20];
    z[31]=static_cast<T>(1)- z[10];
    z[31]=z[2]*z[31];
    z[31]= - n<T>(3,4) + z[31];
    z[31]=z[31]*z[33];
    z[17]=n<T>(1,4)*z[17] + z[31];
    z[17]=z[1]*z[17];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[18]*z[13];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[2]*z[18];
    z[31]=n<T>(3,4) - z[2];
    z[31]=z[31]*z[34];
    z[17]=z[17] + z[18] + z[31];
    z[17]=z[4]*z[17];
    z[18]= - n<T>(1,2) - 9*z[2];
    z[18]=z[2]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[10];
    z[17]=z[18] + z[17];
    z[17]=z[17]*z[38];
    z[18]=n<T>(1,4)*z[4];
    z[31]= - n<T>(1,2) + z[15];
    z[31]=n<T>(1,2)*z[31] + z[4];
    z[31]=z[31]*z[18];
    z[33]=static_cast<T>(1)- n<T>(3,8)*z[2];
    z[33]=z[2]*z[33];
    z[33]= - n<T>(1,4) + z[33];
    z[33]=z[2]*z[33];
    z[17]=z[17] + z[33] + z[31];
    z[17]=z[1]*z[17];
    z[31]= - n<T>(3,2) + z[2];
    z[31]=z[2]*z[31];
    z[27]=z[27] + n<T>(1,2) + z[31];
    z[27]=z[4]*z[27];
    z[28]= - z[28]*z[39];
    z[27]=z[27] + z[28];
    z[27]=z[1]*z[27];
    z[28]= - z[36]*z[15];
    z[28]= - n<T>(1,2) + z[28];
    z[18]= - z[18] - z[40];
    z[18]=z[4]*z[18];
    z[18]=z[27] + n<T>(1,2)*z[28] + z[18];
    z[18]=z[1]*z[18];
    z[27]=z[4] + z[30];
    z[18]=n<T>(1,2)*z[27] + z[18];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(1,4) + z[18];
    z[18]=z[18]*z[16];
    z[27]=z[36]*z[4];
    z[28]= - z[10]*z[27];
    z[30]= - n<T>(1,4) - z[37];
    z[30]=z[2]*z[30];
    z[28]=z[30] + z[28];
    z[28]=z[4]*z[28];
    z[21]=z[1]*z[21]*z[29];
    z[21]=z[28] + z[21];
    z[21]=z[1]*z[21];
    z[14]=z[14]*z[15];
    z[14]=z[27] - n<T>(1,2) + z[14];
    z[14]=z[14]*z[34];
    z[15]=z[22] - n<T>(1,2);
    z[22]=z[2]*z[15];
    z[14]=z[22] + z[14];
    z[14]=n<T>(1,2)*z[14] + z[21];
    z[14]=z[1]*z[14];
    z[21]= - z[34] + n<T>(1,4) - z[2];
    z[21]=z[4]*z[21];
    z[15]= - n<T>(1,2)*z[15] + z[21];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[1]*z[14];
    z[15]=z[2] + z[32];
    z[14]=z[18] + n<T>(1,4)*z[15] + z[14];
    z[14]=z[14]*z[16];
    z[15]= - z[20]*z[35];
    z[15]= - z[25] + z[15];
    z[15]=z[6]*z[15];
    z[16]= - z[24]*z[26];
    z[16]= - z[20] + z[16];
    z[16]=z[6]*z[16];
    z[13]= - z[13] + z[16];
    z[13]=z[13]*z[23];
    z[10]=z[13] - z[10] + z[15];
    z[10]=z[9]*z[10]*npow(z[6],2);
    z[13]= - z[2] - z[4];

    r += n<T>(1,2)*z[10] + z[11] + z[12] + n<T>(1,8)*z[13] + z[14] + z[17] + 
      z[19];
 
    return r;
}

template double qqb_2lha_r220(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r220(const std::array<dd_real,30>&);
#endif
