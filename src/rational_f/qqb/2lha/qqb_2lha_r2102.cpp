#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2102(const std::array<T,30>& k) {
  T z[18];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[4];
    z[4]=k[7];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[13];
    z[8]=z[6]*z[7];
    z[9]=n<T>(1,2) - z[7];
    z[9]=z[9]*z[8];
    z[10]=npow(z[7],2);
    z[9]= - n<T>(1,2)*z[10] + z[9];
    z[9]=z[6]*z[9];
    z[9]=static_cast<T>(1)+ 3*z[9];
    z[11]=npow(z[8],2);
    z[12]=z[11]*z[3];
    z[9]=n<T>(1,2)*z[9] + z[12];
    z[9]=z[3]*z[9];
    z[13]=z[2] - n<T>(3,2);
    z[9]=z[9] + n<T>(1,4)*z[8] - z[13];
    z[9]=z[3]*z[9];
    z[14]=z[2] - n<T>(1,2);
    z[14]=z[14]*z[1];
    z[15]=z[2] - 3;
    z[15]=z[15]*z[2];
    z[16]=n<T>(5,2) + z[15];
    z[9]=z[9] + n<T>(1,2)*z[16] + z[14];
    z[9]=z[3]*z[9];
    z[16]=n<T>(1,4) - z[7];
    z[16]=z[16]*z[8];
    z[16]= - n<T>(1,4)*z[10] + z[16];
    z[16]=z[6]*z[16];
    z[17]=n<T>(1,4)*z[3];
    z[11]=z[11]*z[17];
    z[11]=z[16] + z[11];
    z[11]=z[3]*z[11];
    z[16]=n<T>(3,2) + z[8];
    z[11]=n<T>(1,2)*z[16] + z[11];
    z[11]=z[3]*z[11];
    z[16]=static_cast<T>(7)- 3*z[2];
    z[11]=z[11] + n<T>(1,4)*z[16] + z[14];
    z[11]=z[3]*z[11];
    z[14]=z[8] - z[12];
    z[14]=z[3]*z[14];
    z[14]=static_cast<T>(3)+ z[14];
    z[14]=z[3]*z[14];
    z[16]= - static_cast<T>(1)+ z[2];
    z[16]=z[1]*z[16];
    z[14]=5*z[16] + z[14];
    z[14]=z[14]*z[17];
    z[16]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[16]=z[16]*z[2];
    z[16]=z[16] + n<T>(1,2);
    z[16]=z[16]*npow(z[1],2);
    z[14]=z[16] + z[14];
    z[14]=z[4]*z[14];
    z[15]= - static_cast<T>(2)- z[15];
    z[15]=z[1]*z[15];
    z[11]=z[14] + z[15] + z[11];
    z[11]=z[3]*z[11];
    z[11]=z[16] + z[11];
    z[11]=z[4]*z[11];
    z[14]=n<T>(7,4) - z[2];
    z[14]=z[2]*z[14];
    z[14]= - n<T>(3,4) + z[14];
    z[14]=z[1]*z[14];
    z[9]=z[11] + z[14] + z[9];
    z[9]=z[4]*z[9];
    z[11]=n<T>(3,4) - z[7];
    z[11]=z[11]*z[8];
    z[11]= - n<T>(3,4)*z[10] + z[11];
    z[11]=z[6]*z[11];
    z[11]=n<T>(3,2)*z[12] + n<T>(1,2) + z[11];
    z[11]=z[3]*z[11];
    z[11]=z[11] + n<T>(3,4) - z[2];
    z[11]=z[3]*z[11];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,2) + z[13];
    z[9]=z[9] + n<T>(1,2)*z[13] + z[11];
    z[9]=z[4]*z[9];
    z[11]= - static_cast<T>(1)+ z[5];
    z[11]=z[7]*z[11];
    z[11]=static_cast<T>(1)+ z[11];
    z[8]=z[11]*z[8];
    z[8]= - z[10] + z[8];
    z[10]=n<T>(1,4)*z[6];
    z[8]=z[8]*z[10];
    z[8]=z[8] + z[12];
    z[8]=z[3]*z[8];

    r += z[8] + z[9];
 
    return r;
}

template double qqb_2lha_r2102(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2102(const std::array<dd_real,30>&);
#endif
