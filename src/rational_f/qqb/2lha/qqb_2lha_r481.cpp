#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r481(const std::array<T,30>& k) {
  T z[27];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=npow(z[3],2);
    z[9]=n<T>(1,3)*z[8];
    z[10]=z[3]*z[2];
    z[11]= - n<T>(5,4) - z[10];
    z[11]=z[11]*z[9];
    z[12]=n<T>(1,3)*z[3];
    z[13]=n<T>(1,2)*z[3];
    z[14]=npow(z[2],2)*z[13];
    z[14]= - static_cast<T>(2)+ z[14];
    z[14]=z[3]*z[14];
    z[14]= - n<T>(1,2) + z[14];
    z[14]=z[14]*z[12];
    z[14]= - n<T>(3,8) + z[14];
    z[14]=z[7]*z[14];
    z[11]=z[14] - n<T>(1,4) + z[11];
    z[11]=z[7]*z[11];
    z[14]=z[7] + 1;
    z[15]=n<T>(1,2)*z[5];
    z[14]=z[15]*z[14];
    z[16]=n<T>(1,2)*z[7];
    z[14]=z[14] - z[16];
    z[17]=static_cast<T>(1)+ 3*z[4];
    z[14]=z[17]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[15]*z[14];
    z[17]= - n<T>(3,8) + n<T>(2,3)*z[8];
    z[17]=z[7]*z[17];
    z[14]=z[14] + n<T>(5,12)*z[8] + z[17];
    z[14]=z[5]*z[7]*z[14];
    z[17]= - static_cast<T>(1)- z[2];
    z[17]=z[3]*z[17];
    z[17]= - static_cast<T>(1)+ z[17];
    z[9]=z[17]*z[9];
    z[9]= - n<T>(1,4) + z[9];
    z[17]=npow(z[7],2);
    z[9]=z[9]*z[17];
    z[18]= - n<T>(3,2) + z[5];
    z[18]=z[15]*z[18];
    z[19]=npow(z[3],3);
    z[18]=n<T>(1,3)*z[19] + z[18];
    z[18]=z[5]*z[17]*z[18];
    z[9]=z[9] + z[18];
    z[9]=z[6]*z[9];
    z[9]=n<T>(1,2)*z[9] + z[11] + z[14];
    z[9]=z[6]*z[9];
    z[11]=z[2] - 1;
    z[14]=z[12]*z[11];
    z[18]=n<T>(1,4) + z[14];
    z[10]=z[18]*z[10];
    z[10]= - n<T>(1,4) + z[10];
    z[10]=z[3]*z[10];
    z[18]=n<T>(1,2)*z[2];
    z[19]=static_cast<T>(1)+ z[18];
    z[19]=z[19]*z[12]*z[2];
    z[20]=n<T>(1,3)*z[2];
    z[21]= - static_cast<T>(1)+ z[20];
    z[19]=n<T>(1,2)*z[21] + z[19];
    z[19]=z[3]*z[19];
    z[19]= - n<T>(3,8) + z[19];
    z[19]=z[7]*z[19];
    z[10]=z[19] - n<T>(5,8) + z[10];
    z[10]=z[7]*z[10];
    z[19]= - static_cast<T>(1)- n<T>(3,4)*z[4];
    z[19]=z[4]*z[19];
    z[19]=n<T>(1,4) + z[19];
    z[17]=z[19]*z[17];
    z[19]=n<T>(1,2)*z[4];
    z[21]=z[7]*z[19];
    z[21]=z[4] + z[21];
    z[22]=z[4] + 1;
    z[21]=z[7]*z[22]*z[21];
    z[21]=z[19] + z[21];
    z[21]=z[5]*z[21];
    z[17]=3*z[21] - n<T>(1,4) + z[17];
    z[15]=z[17]*z[15];
    z[17]=n<T>(1,4)*z[4];
    z[21]= - z[17] - static_cast<T>(1)+ n<T>(5,2)*z[3];
    z[22]= - n<T>(3,8)*z[4] - n<T>(1,2) + z[3];
    z[22]=z[7]*z[22];
    z[21]=n<T>(1,2)*z[21] + z[22];
    z[21]=z[7]*z[21];
    z[22]=n<T>(1,3)*z[4];
    z[23]=z[22] - n<T>(5,6) + z[3];
    z[15]=z[15] + n<T>(1,2)*z[23] + z[21];
    z[15]=z[5]*z[15];
    z[21]= - n<T>(1,2) - z[14];
    z[8]=z[21]*z[8];
    z[8]= - n<T>(1,4) + z[8];
    z[8]=z[9] + z[15] + n<T>(1,2)*z[8] + z[10];
    z[8]=z[6]*z[8];
    z[9]=npow(z[4],2);
    z[10]=z[9]*z[7];
    z[15]=3*z[9] + z[10];
    z[21]=z[4] + 3;
    z[23]=n<T>(1,4)*z[7];
    z[15]=z[23]*z[21]*z[15];
    z[21]=z[17] - 1;
    z[24]=n<T>(1,4)*z[1];
    z[25]= - z[24] + z[21];
    z[25]=z[4]*z[25];
    z[26]=z[1] + 3;
    z[25]=n<T>(3,4)*z[26] + z[25];
    z[25]=z[25]*z[9];
    z[15]=z[25] + z[15];
    z[15]=z[5]*z[15];
    z[25]=z[4] - z[26];
    z[25]=z[25]*z[19];
    z[25]=z[25] + n<T>(5,2) + z[1];
    z[25]=z[25]*z[19];
    z[26]=static_cast<T>(1)- z[4];
    z[17]=z[7]*z[26]*z[17];
    z[26]=static_cast<T>(1)+ z[19];
    z[26]=z[4]*z[26];
    z[17]=z[26] + z[17];
    z[17]=z[7]*z[17];
    z[15]=z[15] + z[25] + z[17];
    z[15]=z[5]*z[15];
    z[17]=z[4] - n<T>(23,8) - z[1];
    z[17]=z[17]*z[22];
    z[25]=n<T>(13,12) - z[4];
    z[16]=z[25]*z[16];
    z[16]=z[16] + static_cast<T>(1)- z[19];
    z[16]=z[7]*z[16];
    z[25]=n<T>(7,2) + n<T>(5,3)*z[1];
    z[15]=z[15] + z[16] + n<T>(1,4)*z[25] + z[17];
    z[15]=z[5]*z[15];
    z[14]=z[2]*z[14];
    z[16]=static_cast<T>(3)+ z[2];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[14]=z[3]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[16]=z[2] + n<T>(1,2);
    z[17]=z[16]*z[12];
    z[17]= - n<T>(1,8) + z[17];
    z[17]=z[7]*z[17];
    z[14]=n<T>(1,2)*z[14] + z[17];
    z[14]=z[7]*z[14];
    z[17]= - static_cast<T>(1)+ z[18];
    z[17]=z[2]*z[17];
    z[17]=n<T>(1,2) + z[17];
    z[12]=z[17]*z[12];
    z[12]=n<T>(1,4)*z[11] + z[12];
    z[12]=z[3]*z[12];
    z[17]=n<T>(1,3)*z[1];
    z[18]=static_cast<T>(3)+ z[17];
    z[12]=n<T>(1,4)*z[18] + z[12];
    z[12]=z[3]*z[12];
    z[18]= - n<T>(11,4) - z[1];
    z[8]=z[8] + z[15] + z[14] + z[22] + n<T>(1,3)*z[18] + z[12];
    z[8]=z[6]*z[8];
    z[12]=static_cast<T>(1)+ z[24];
    z[12]=z[1]*z[12];
    z[14]= - n<T>(1,2)*z[1] + z[21];
    z[14]=z[4]*z[14];
    z[15]=static_cast<T>(1)+ z[23];
    z[15]=z[7]*z[15];
    z[12]=z[15] + z[14] + n<T>(3,2) + z[12];
    z[12]=z[5]*z[12]*npow(z[4],3);
    z[14]=static_cast<T>(2)+ n<T>(5,8)*z[1];
    z[14]=z[1]*z[14];
    z[15]=n<T>(5,8)*z[4] - static_cast<T>(2)- n<T>(5,4)*z[1];
    z[15]=z[4]*z[15];
    z[14]=z[15] + n<T>(9,4) + z[14];
    z[14]=z[14]*z[9];
    z[9]=z[9] + n<T>(1,8)*z[10];
    z[9]=z[7]*z[9];
    z[9]=z[12] + z[14] + z[9];
    z[9]=z[5]*z[9];
    z[10]=n<T>(37,8) + 2*z[1];
    z[10]=z[10]*z[17];
    z[12]=2*z[4] - n<T>(37,8) - 4*z[1];
    z[12]=z[12]*z[22];
    z[14]=static_cast<T>(7)+ z[7];
    z[14]=z[7]*z[14];
    z[10]=n<T>(1,24)*z[14] + z[12] + n<T>(9,8) + z[10];
    z[10]=z[4]*z[10];
    z[9]=z[9] + z[10];
    z[9]=z[5]*z[9];
    z[10]= - n<T>(1,2) - z[17];
    z[10]=z[1]*z[10];
    z[10]= - n<T>(1,2) + z[10];
    z[10]=z[13]*z[11]*z[10];
    z[12]=n<T>(7,8)*z[1] + n<T>(5,8) + z[2];
    z[12]=z[12]*z[17];
    z[13]=n<T>(7,12)*z[4] - n<T>(7,6)*z[1] - n<T>(3,4) - z[20];
    z[13]=z[13]*z[19];
    z[11]= - z[3]*z[11];
    z[11]=z[7] + n<T>(7,4) + z[11];
    z[11]=z[7]*z[11];

    r += z[8] + z[9] + z[10] + n<T>(1,6)*z[11] + z[12] + z[13] + n<T>(1,4)*
      z[16];
 
    return r;
}

template double qqb_2lha_r481(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r481(const std::array<dd_real,30>&);
#endif
