#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2013(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[2];
    z[5]=k[24];
    z[6]=k[5];
    z[7]=k[14];
    z[8]=k[3];
    z[9]=k[13];
    z[10]=k[27];
    z[11]=k[4];
    z[12]=z[2] - z[5];
    z[13]=n<T>(1,2)*z[2];
    z[14]=z[13] - n<T>(1,2)*z[9] - z[5] + z[10];
    z[14]=z[11]*z[14];
    z[14]=z[14] - z[12];
    z[14]=z[11]*z[14];
    z[15]=z[9] - z[10];
    z[16]=z[12] - z[15];
    z[17]=z[6]*z[16]*npow(z[11],2);
    z[14]=z[14] + n<T>(1,2)*z[17];
    z[14]=z[6]*z[14];
    z[17]=static_cast<T>(1)+ z[1];
    z[13]=z[17]*z[13];
    z[17]=n<T>(1,2)*z[11];
    z[18]=z[10]*z[17];
    z[12]=z[18] + n<T>(1,2) - z[12];
    z[12]=z[11]*z[12];
    z[18]= - static_cast<T>(1)+ n<T>(1,2)*z[4];
    z[19]= - z[4]*z[18];
    z[19]= - n<T>(1,2) + z[19];
    z[19]=z[5]*z[19];
    z[12]=z[14] + z[12] + z[13] + z[19] - n<T>(1,2)*z[1] + z[18];
    z[12]=z[3]*z[12];
    z[13]=z[2] - z[9];
    z[13]= - z[13]*z[17];
    z[13]=z[13] - z[15];
    z[14]=3*z[6];
    z[13]=z[13]*z[14];
    z[13]=z[13] + static_cast<T>(1)+ 3*z[16];
    z[13]=z[6]*z[11]*z[13];
    z[16]=static_cast<T>(1)+ z[11] - z[4];
    z[17]= - z[7]*z[16];
    z[12]=3*z[12] + z[13] - n<T>(3,2)*z[2] + n<T>(3,2) + z[17];
    z[12]=z[3]*z[12];
    z[13]=z[8] - 1;
    z[13]=z[13]*z[15];
    z[18]=z[5] + z[10];
    z[18]=z[9] - n<T>(1,2)*z[18];
    z[18]=z[11]*z[18];
    z[13]=n<T>(1,2)*z[13] + z[18];
    z[13]=z[13]*z[14];
    z[13]=z[13] + z[16] - n<T>(3,2)*z[15];
    z[13]=z[6]*z[13];
    z[12]=z[12] + z[13] + z[17];

    r += n<T>(1,4)*z[12];
 
    return r;
}

template double qqb_2lha_r2013(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2013(const std::array<dd_real,30>&);
#endif
