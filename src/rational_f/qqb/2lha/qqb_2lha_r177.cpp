#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r177(const std::array<T,30>& k) {
  T z[67];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[17];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[20];
    z[9]=k[6];
    z[10]=k[12];
    z[11]=k[11];
    z[12]=k[24];
    z[13]=k[4];
    z[14]=k[5];
    z[15]=k[9];
    z[16]=k[10];
    z[17]=k[14];
    z[18]=n<T>(1,4)*z[3];
    z[19]= - static_cast<T>(5)- 3*z[3];
    z[19]=z[19]*z[18];
    z[20]= - 31*z[8] - n<T>(107,6) - 17*z[7];
    z[19]=n<T>(1,3)*z[20] + z[19];
    z[20]=z[6] - 1;
    z[21]=n<T>(29,9)*z[9];
    z[22]=z[20]*z[21];
    z[23]=n<T>(13,36)*z[6] + n<T>(3,4)*z[3] + n<T>(253,18) - z[8];
    z[22]=n<T>(1,2)*z[23] + z[22];
    z[23]=n<T>(1,2)*z[9];
    z[22]=z[22]*z[23];
    z[24]=n<T>(1,9)*z[6];
    z[19]=z[22] + n<T>(1,8)*z[19] + z[24];
    z[19]=z[9]*z[19];
    z[22]=n<T>(1,4)*z[8];
    z[25]=3*z[8];
    z[26]=n<T>(13,6) - z[25];
    z[26]=z[26]*z[22];
    z[27]=z[18] + n<T>(1,3);
    z[28]=z[27]*z[18];
    z[29]= - n<T>(13,36) + z[5];
    z[30]=static_cast<T>(1)+ n<T>(17,48)*z[7];
    z[30]=z[7]*z[30];
    z[19]=z[19] + z[28] + z[26] + n<T>(1,2)*z[29] + n<T>(1,3)*z[30];
    z[19]=z[9]*z[19];
    z[26]=z[8] + n<T>(1,2);
    z[28]=z[26]*z[25];
    z[28]=z[28] + 1;
    z[28]=z[28]*z[8];
    z[29]= - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[29]=z[29]*z[7];
    z[28]=z[28] - z[29];
    z[30]=n<T>(3,16)*z[6];
    z[31]=z[28]*z[30];
    z[32]=z[5] + n<T>(1,2);
    z[33]=z[32]*z[5];
    z[34]=z[33] + n<T>(1,3);
    z[35]=z[2]*z[5];
    z[36]=z[34]*z[35];
    z[37]=n<T>(1,4)*z[10];
    z[38]= - static_cast<T>(1)- n<T>(35,6)*z[10];
    z[38]=z[38]*z[37];
    z[38]= - 5*z[33] + z[38];
    z[39]=n<T>(1,16)*z[8];
    z[40]=27*z[8];
    z[41]=n<T>(29,2) - z[40];
    z[41]=z[8]*z[41];
    z[41]=static_cast<T>(5)+ z[41];
    z[41]=z[41]*z[39];
    z[42]=n<T>(1,2)*z[3];
    z[43]=n<T>(1,2) - n<T>(1,3)*z[2];
    z[43]=z[43]*z[42];
    z[44]= - n<T>(13,12) + z[2];
    z[43]=n<T>(1,3)*z[44] + z[43];
    z[43]=z[3]*z[43];
    z[44]=npow(z[10],2);
    z[45]=z[44]*z[1];
    z[19]= - n<T>(1,36)*z[45] + z[19] - z[31] + z[43] + z[41] - n<T>(5,16)*z[29]
    + n<T>(1,6)*z[38] + z[36];
    z[19]=z[1]*z[19];
    z[38]=n<T>(3,8)*z[6];
    z[38]=z[38]*z[28];
    z[41]=9*z[8];
    z[43]= - n<T>(19,2) + z[41];
    z[43]=z[43]*z[22];
    z[43]= - static_cast<T>(1)+ z[43];
    z[43]=z[8]*z[43];
    z[29]=z[38] + z[43] - n<T>(1,8)*z[44] + z[29];
    z[43]=n<T>(1,2)*z[6];
    z[29]=z[29]*z[43];
    z[44]=n<T>(1,8)*z[10];
    z[46]=n<T>(17,3) - 3*z[10];
    z[46]=z[46]*z[44];
    z[47]= - n<T>(7,6) + z[5];
    z[46]=n<T>(1,3)*z[47] + z[46];
    z[47]=n<T>(1,2)*z[36];
    z[33]= - n<T>(1,3)*z[33] + z[47];
    z[33]=z[2]*z[33];
    z[48]=n<T>(1,48)*z[7];
    z[49]=n<T>(5,3)*z[7];
    z[50]=static_cast<T>(31)- z[49];
    z[50]=z[50]*z[48];
    z[51]=n<T>(1,8)*z[8];
    z[52]=13*z[8];
    z[53]=n<T>(25,3) - z[52];
    z[53]=z[53]*z[51];
    z[54]=z[2] - 1;
    z[55]=z[54]*z[2];
    z[56]=n<T>(1,2)*z[2];
    z[57]=z[56] - 1;
    z[58]= - z[2]*z[57];
    z[58]= - n<T>(1,8) + z[58];
    z[58]=z[3]*z[58];
    z[58]=z[58] + n<T>(1,12) + z[55];
    z[59]=n<T>(1,6)*z[3];
    z[58]=z[58]*z[59];
    z[60]=n<T>(1,24)*z[3];
    z[61]= - n<T>(17,2) + z[3];
    z[61]=z[61]*z[60];
    z[20]=z[9]*z[20];
    z[20]=n<T>(259,27)*z[20] + 5*z[6] + n<T>(11,6)*z[3] + n<T>(899,54) - z[8];
    z[20]=z[9]*z[20];
    z[20]=z[20] - n<T>(29,6)*z[7] + n<T>(3,2) - n<T>(1,3)*z[5];
    z[20]=n<T>(5,18)*z[6] + z[61] - z[8] + n<T>(1,8)*z[20];
    z[20]=z[9]*z[20];
    z[19]=z[19] + z[20] + z[29] + z[58] + z[53] + z[50] + n<T>(1,2)*z[46] + 
    z[33];
    z[19]=z[1]*z[19];
    z[20]=n<T>(1,9)*z[10];
    z[29]=static_cast<T>(1)- z[37];
    z[29]=z[29]*z[20];
    z[33]=n<T>(1,16)*z[7];
    z[46]=5*z[7];
    z[50]=n<T>(29,2) - z[46];
    z[50]=z[50]*z[33];
    z[53]=n<T>(61,2) - z[41];
    z[53]=z[8]*z[53];
    z[53]=n<T>(29,2) + z[53];
    z[53]=z[53]*z[39];
    z[29]= - z[38] + z[53] + z[29] + z[50];
    z[29]=z[6]*z[29];
    z[38]=n<T>(1,8)*z[7];
    z[50]= - n<T>(7,2) + z[7];
    z[50]=z[50]*z[38];
    z[53]= - n<T>(17,2) + z[41];
    z[53]=z[8]*z[53];
    z[53]= - n<T>(7,2) + z[53];
    z[53]=z[53]*z[51];
    z[34]=z[5]*z[34];
    z[34]=z[53] + z[50] - z[36] + z[34] - n<T>(1,18)*z[10];
    z[36]=z[27]*z[42];
    z[50]=n<T>(1,6)*z[7];
    z[53]=n<T>(49,2) + z[46];
    z[53]=z[53]*z[50];
    z[53]=z[36] + n<T>(3,2)*z[8] + z[53] + n<T>(23,36) + z[5];
    z[58]= - n<T>(253,9) - 31*z[7];
    z[58]=n<T>(1,4)*z[58] + z[8];
    z[58]=n<T>(1,3)*z[58] - z[18];
    z[58]=n<T>(1,2)*z[58] + n<T>(29,27)*z[9];
    z[58]=z[9]*z[58];
    z[53]=n<T>(1,2)*z[53] + z[58];
    z[53]=z[53]*z[23];
    z[58]=n<T>(1,4)*z[7];
    z[61]= - static_cast<T>(3)- n<T>(13,3)*z[7];
    z[61]=z[61]*z[58];
    z[61]=z[61] - n<T>(1,9) - z[5];
    z[62]=n<T>(1,8)*z[3];
    z[27]= - z[27]*z[62];
    z[63]= - n<T>(1,3) + z[22];
    z[63]=z[8]*z[63];
    z[27]=z[53] + z[27] + n<T>(1,4)*z[61] + z[63];
    z[27]=z[9]*z[27];
    z[53]=z[54]*z[18];
    z[53]=z[53] + n<T>(1,3) - z[56];
    z[54]=n<T>(1,3)*z[3];
    z[53]=z[53]*z[54];
    z[27]=z[27] + z[31] + n<T>(1,2)*z[34] + z[53];
    z[27]=z[1]*z[27];
    z[31]=static_cast<T>(67)+ n<T>(131,9)*z[7];
    z[31]=z[31]*z[58];
    z[34]=n<T>(1,2)*z[5];
    z[53]= - n<T>(1,3) + z[34];
    z[58]=static_cast<T>(1)- z[59];
    z[58]=z[3]*z[58];
    z[31]=z[58] + n<T>(19,6)*z[8] + n<T>(23,3)*z[53] + z[31];
    z[53]=n<T>(1,2)*z[8];
    z[58]=n<T>(73,9) - z[34];
    z[58]= - n<T>(7,18)*z[6] - n<T>(11,12)*z[3] + z[53] + n<T>(1,3)*z[58] - n<T>(47,4)*
    z[7];
    z[61]=n<T>(259,8) + 29*z[6];
    z[61]=z[9]*z[61];
    z[58]=n<T>(1,4)*z[58] + n<T>(1,27)*z[61];
    z[58]=z[58]*z[23];
    z[31]=z[58] + n<T>(1,8)*z[31] + z[24];
    z[31]=z[9]*z[31];
    z[58]=npow(z[5],2);
    z[58]=n<T>(1,12) + z[58];
    z[58]=z[5]*z[58];
    z[47]=z[58] - z[47];
    z[47]=z[2]*z[47];
    z[58]=n<T>(1,96)*z[7];
    z[61]= - static_cast<T>(83)- n<T>(131,3)*z[7];
    z[61]=z[61]*z[58];
    z[52]= - n<T>(45,2) + z[52];
    z[52]=z[52]*z[39];
    z[55]=n<T>(1,4) + z[55];
    z[55]=z[55]*z[42];
    z[63]=z[2] - n<T>(1,2);
    z[63]=z[63]*z[2];
    z[55]=z[55] - n<T>(1,3) - z[63];
    z[55]=z[55]*z[59];
    z[64]= - n<T>(11,18)*z[10] + static_cast<T>(1)- n<T>(5,6)*z[5];
    z[27]=z[27] + z[31] + z[29] + z[55] + z[52] + z[61] + n<T>(1,4)*z[64] + 
    z[47];
    z[27]=z[1]*z[27];
    z[29]= - n<T>(23,9) + 3*z[5];
    z[20]=n<T>(1,2)*z[29] - z[20];
    z[29]= - n<T>(1,2) + z[5];
    z[29]=z[5]*z[29];
    z[29]= - n<T>(1,6) + z[29];
    z[29]=z[29]*z[35];
    z[29]=n<T>(1,12)*z[5] + z[29];
    z[29]=z[2]*z[29];
    z[31]= - n<T>(1,8) - z[63];
    z[31]=z[31]*z[59];
    z[20]=z[31] - z[51] + n<T>(41,18)*z[7] + n<T>(1,4)*z[20] + z[29];
    z[28]=z[6]*z[28];
    z[29]= - n<T>(11,4) + z[7];
    z[29]=z[7]*z[29];
    z[26]=z[8]*z[26];
    z[26]=n<T>(3,4)*z[28] + z[29] - n<T>(11,2)*z[26];
    z[28]=n<T>(1,4)*z[6];
    z[26]=z[26]*z[28];
    z[29]=static_cast<T>(23)+ n<T>(41,12)*z[7];
    z[29]=z[29]*z[38];
    z[31]=static_cast<T>(7)- n<T>(19,3)*z[10];
    z[31]=z[10]*z[31];
    z[29]=n<T>(65,16)*z[8] + z[29] - static_cast<T>(1)+ n<T>(1,16)*z[31];
    z[26]=n<T>(1,3)*z[29] + z[26];
    z[26]=z[6]*z[26];
    z[29]=n<T>(5,8)*z[3];
    z[31]= - z[29] - n<T>(271,24)*z[7] + static_cast<T>(13)- z[34];
    z[47]=n<T>(1,2) + n<T>(85,27)*z[6];
    z[47]=z[9]*z[47];
    z[31]=z[47] + n<T>(1,3)*z[31] + z[6];
    z[31]=z[9]*z[31];
    z[20]=z[27] + n<T>(1,4)*z[31] + n<T>(1,2)*z[20] + z[26];
    z[20]=z[1]*z[20];
    z[26]=z[12] - 1;
    z[27]=n<T>(1,2)*z[10];
    z[31]=z[26]*z[27];
    z[47]=n<T>(13,6) - z[12];
    z[47]=z[12]*z[47];
    z[31]=z[31] + n<T>(1,2) + z[47];
    z[31]=z[31]*z[27];
    z[47]=n<T>(1,24)*z[7];
    z[46]= - z[14] - z[46];
    z[46]=z[46]*z[47];
    z[51]=z[27]*z[12];
    z[52]=z[12] + n<T>(1,2);
    z[55]=z[12]*z[52];
    z[55]=z[55] - z[51];
    z[55]=z[10]*z[55]*z[43];
    z[59]=npow(z[12],2);
    z[31]=z[55] + z[46] + z[31] + n<T>(5,9) - n<T>(1,2)*z[59];
    z[31]=z[31]*z[43];
    z[46]=n<T>(1,2)*z[14];
    z[55]= - static_cast<T>(5)+ n<T>(7,2)*z[11];
    z[55]=n<T>(1,3)*z[55] + z[46];
    z[27]=z[55]*z[27];
    z[55]=11*z[17];
    z[27]=z[27] - n<T>(13,2)*z[12] - z[14] - n<T>(47,3)*z[11] + n<T>(59,6) + z[55];
    z[27]=z[10]*z[27];
    z[55]= - n<T>(1,4)*z[14] + z[55] - n<T>(1,6);
    z[27]=z[27] + n<T>(89,6)*z[11] - z[55];
    z[61]=n<T>(5,2)*z[7];
    z[63]=z[61] - static_cast<T>(13)+ z[14];
    z[47]=z[63]*z[47];
    z[63]=z[59]*z[2];
    z[64]= - n<T>(13,24) - z[12];
    z[64]=z[12]*z[64];
    z[27]=z[31] + z[47] + n<T>(3,4)*z[63] + z[64] + n<T>(1,12)*z[27];
    z[27]=z[27]*z[43];
    z[31]=n<T>(89,4)*z[11] - z[5];
    z[47]=static_cast<T>(3)+ z[12];
    z[47]=z[12]*z[47];
    z[31]= - z[63] + n<T>(1,6)*z[31] + z[47];
    z[31]=z[2]*z[31];
    z[47]=n<T>(7,3)*z[12];
    z[63]=n<T>(1,6) - 5*z[11];
    z[64]= - z[47] + n<T>(9,4)*z[11];
    z[65]=z[2]*z[64];
    z[63]=z[65] + n<T>(1,2)*z[63] + z[47];
    z[63]=z[2]*z[63];
    z[63]=n<T>(1,6) + z[63];
    z[62]=z[63]*z[62];
    z[63]=static_cast<T>(13)- z[16];
    z[63]=n<T>(1,3)*z[63] - z[15];
    z[47]=n<T>(1,2)*z[63] + z[47];
    z[52]=z[52]*z[43];
    z[52]=z[52] + n<T>(5,6) + z[12];
    z[52]=z[12]*z[52];
    z[52]=n<T>(1,4) + z[52];
    z[52]=z[6]*z[52];
    z[47]=n<T>(1,2)*z[47] + z[52];
    z[47]=z[47]*z[43];
    z[52]=z[43] + 1;
    z[52]=z[12]*z[52];
    z[63]=n<T>(1,6)*z[15];
    z[65]= - z[63] + static_cast<T>(1)- n<T>(5,6)*z[16];
    z[52]=n<T>(1,2)*z[65] + z[52];
    z[52]=z[6]*z[52];
    z[65]=n<T>(1,3)*z[15];
    z[66]=static_cast<T>(1)+ z[65];
    z[52]=n<T>(1,2)*z[66] + z[52];
    z[52]=z[6]*z[52];
    z[66]=static_cast<T>(1)- z[15];
    z[52]=n<T>(1,12)*z[66] + z[52];
    z[52]=z[52]*z[23];
    z[66]=n<T>(1,8)*z[15];
    z[47]=z[52] + z[47] + n<T>(1,3) + z[66];
    z[47]=z[47]*z[23];
    z[52]= - static_cast<T>(1)+ n<T>(1,4)*z[11];
    z[52]=n<T>(1,3)*z[52] + n<T>(1,8)*z[14];
    z[52]=z[52]*z[37];
    z[52]=z[52] - n<T>(5,2)*z[12] - n<T>(1,16)*z[5] - z[66] + n<T>(1,96)*z[11] - 2
    - n<T>(11,8)*z[17];
    z[66]=static_cast<T>(7)- z[14];
    z[58]=z[66]*z[58];
    z[20]=z[20] + z[47] + z[27] + z[62] + z[58] + n<T>(1,3)*z[52] + n<T>(1,4)*
    z[31];
    z[20]=z[4]*z[20];
    z[27]= - z[34] - z[15] - n<T>(41,6)*z[11] - n<T>(1,2)*z[16] - z[55];
    z[31]=5*z[12];
    z[27]=n<T>(1,4)*z[27] - z[31];
    z[34]=static_cast<T>(1)+ n<T>(1,6)*z[14];
    z[34]=z[10]*z[34];
    z[34]=z[34] - z[46] - n<T>(13,3) + n<T>(1,2)*z[11];
    z[34]=z[34]*z[44];
    z[46]= - n<T>(1,6)*z[5] + z[59];
    z[46]=z[46]*z[56];
    z[47]=z[49] + z[14];
    z[49]= - n<T>(29,3) + z[47];
    z[49]=z[49]*z[48];
    z[52]= - n<T>(1,3) - z[64];
    z[52]=z[2]*z[52];
    z[52]= - n<T>(1,6) + z[52];
    z[52]=z[52]*z[18];
    z[27]=z[52] + z[49] + z[46] + n<T>(1,3)*z[27] + z[34];
    z[34]= - z[47]*z[48];
    z[26]=z[26]*z[10];
    z[34]=z[34] - n<T>(13,24)*z[26] + n<T>(5,18) - z[59];
    z[46]=n<T>(1,3) - n<T>(1,8)*z[12];
    z[46]=z[12]*z[46];
    z[26]=z[46] + n<T>(1,16)*z[26];
    z[26]=z[10]*z[26];
    z[46]=z[59] - z[51];
    z[44]=z[6]*z[46]*z[44];
    z[26]=z[26] + z[44];
    z[26]=z[6]*z[26];
    z[26]=n<T>(1,2)*z[34] + z[26];
    z[26]=z[6]*z[26];
    z[34]=n<T>(25,2) - z[16];
    z[34]=n<T>(1,2)*z[34] - z[15];
    z[31]=n<T>(1,2)*z[34] + z[31];
    z[34]=z[59]*z[6];
    z[44]=n<T>(1,3) + n<T>(1,2)*z[12];
    z[44]=z[12]*z[44];
    z[44]=n<T>(1,8)*z[34] - n<T>(5,36) + z[44];
    z[44]=z[6]*z[44];
    z[31]=n<T>(1,6)*z[31] + z[44];
    z[31]=z[6]*z[31];
    z[44]=z[28] + 1;
    z[44]=z[12]*z[44];
    z[46]= - z[65] + static_cast<T>(3)- n<T>(5,3)*z[16];
    z[44]=n<T>(1,8)*z[46] + z[44];
    z[44]=z[6]*z[44];
    z[46]=n<T>(3,2) + z[65];
    z[44]=n<T>(1,4)*z[46] + z[44];
    z[44]=z[6]*z[44];
    z[46]= - static_cast<T>(1)- z[63];
    z[44]=n<T>(1,4)*z[46] + z[44];
    z[23]=z[44]*z[23];
    z[29]=z[29] + n<T>(11,3) + z[15];
    z[23]=z[23] + n<T>(1,12)*z[29] + z[31];
    z[23]=z[9]*z[23];
    z[19]=z[20] + z[19] + z[23] + n<T>(1,2)*z[27] + z[26];
    z[19]=z[4]*z[19];
    z[20]=static_cast<T>(1)- z[10];
    z[20]=z[20]*z[37];
    z[23]=z[7] - 1;
    z[26]=z[23]*z[50];
    z[27]= - static_cast<T>(11)+ 5*z[2];
    z[27]=z[3]*z[27];
    z[20]=n<T>(1,16)*z[27] + z[26] + z[20] - z[57];
    z[20]=z[20]*z[54];
    z[26]= - n<T>(7,3) + z[61];
    z[27]= - n<T>(1,3) + z[25];
    z[27]=z[8]*z[27];
    z[26]= - z[36] + n<T>(5,18)*z[26] + z[27];
    z[27]= - static_cast<T>(1)+ z[43];
    z[27]=z[6]*z[27];
    z[27]=n<T>(1,2) + z[27];
    z[27]=z[27]*z[21];
    z[29]=n<T>(1121,4) + 73*z[6];
    z[29]=z[6]*z[29];
    z[27]=z[27] + n<T>(1,108)*z[29] - n<T>(3,16)*z[3] - n<T>(10,3) + z[22];
    z[27]=z[9]*z[27];
    z[29]= - n<T>(103,18) + 35*z[8];
    z[31]=static_cast<T>(1)+ z[42];
    z[31]=z[3]*z[31];
    z[29]=n<T>(173,27)*z[6] + n<T>(1,3)*z[29] + n<T>(3,2)*z[31];
    z[27]=n<T>(1,8)*z[29] + z[27];
    z[27]=z[9]*z[27];
    z[26]=n<T>(1,4)*z[26] + z[27];
    z[26]=z[9]*z[26];
    z[27]= - z[32]*z[35];
    z[27]=z[5] + z[27];
    z[29]=3*z[7];
    z[31]=n<T>(17,6) - z[29];
    z[31]=z[31]*z[33];
    z[32]= - n<T>(7,2) + z[40];
    z[32]=z[8]*z[32];
    z[32]= - n<T>(17,6) + z[32];
    z[32]=z[32]*z[39];
    z[25]=z[25] - n<T>(1,2);
    z[25]=z[25]*npow(z[8],2);
    z[33]=npow(z[7],2);
    z[25]=z[25] - n<T>(1,2)*z[33];
    z[30]= - z[25]*z[30];
    z[33]= - z[42]*z[45];
    z[35]=n<T>(1,2) - z[10];
    z[35]=z[3]*z[10]*z[35];
    z[33]=z[35] + z[33];
    z[33]=z[1]*z[33];
    z[20]=n<T>(1,6)*z[33] + z[26] + z[30] + z[20] + z[32] + n<T>(1,3)*z[27] + 
    z[31];
    z[20]=z[1]*z[20];
    z[26]=static_cast<T>(1)- n<T>(1,2)*z[13];
    z[26]=z[26]*z[29];
    z[26]= - n<T>(17,6) + z[26];
    z[26]=z[7]*z[26];
    z[27]= - static_cast<T>(1)+ z[13];
    z[27]=z[27]*z[41];
    z[29]=3*z[13];
    z[30]= - static_cast<T>(23)- z[29];
    z[27]=n<T>(1,2)*z[30] + z[27];
    z[27]=z[8]*z[27];
    z[27]=n<T>(17,6) + z[27];
    z[27]=z[8]*z[27];
    z[25]=z[6]*z[25];
    z[25]=3*z[25] + z[26] + z[27];
    z[25]=z[25]*z[28];
    z[26]= - n<T>(35,9) + z[29];
    z[26]=z[26]*z[38];
    z[27]=n<T>(23,8) - z[13];
    z[26]=n<T>(1,3)*z[27] + z[26];
    z[26]=z[7]*z[26];
    z[27]= - z[41] - n<T>(5,2);
    z[27]=z[13]*z[27];
    z[27]=static_cast<T>(13)+ z[27];
    z[22]=z[27]*z[22];
    z[27]=n<T>(35,8) + z[13];
    z[22]=n<T>(1,3)*z[27] + z[22];
    z[22]=z[8]*z[22];
    z[27]= - z[18] + n<T>(1,4);
    z[28]=z[2] - z[13];
    z[27]=z[28]*z[27];
    z[23]=z[7]*z[23];
    z[23]=n<T>(1,9)*z[23] + z[27];
    z[23]=z[3]*z[23];
    z[22]=z[25] + z[23] + z[26] + z[22];
    z[23]=z[8]*z[13];
    z[25]=n<T>(259,2) + 227*z[13];
    z[25]=n<T>(1,18)*z[25] - z[23];
    z[26]=z[13]*z[18];
    z[25]=n<T>(1,3)*z[25] + z[26];
    z[26]= - static_cast<T>(125)- n<T>(881,6)*z[13];
    z[27]=n<T>(241,4) + n<T>(109,3)*z[13];
    z[27]=n<T>(1,2)*z[27] - n<T>(29,3)*z[6];
    z[27]=z[6]*z[27];
    z[26]=n<T>(1,4)*z[26] + z[27];
    z[24]=z[26]*z[24];
    z[26]= - static_cast<T>(1)+ n<T>(1,3)*z[6];
    z[26]=z[6]*z[26];
    z[26]=static_cast<T>(1)+ z[26];
    z[26]=z[6]*z[13]*z[26];
    z[26]= - n<T>(1,3)*z[13] + z[26];
    z[21]=z[26]*z[21];
    z[21]=z[21] + n<T>(1,2)*z[25] + z[24];
    z[21]=z[9]*z[21];
    z[24]= - static_cast<T>(11)- 7*z[13];
    z[25]= - z[3]*z[13];
    z[24]=n<T>(1,3)*z[24] + z[25];
    z[18]=z[24]*z[18];
    z[24]= - n<T>(803,2) + 301*z[13];
    z[25]=n<T>(1,2) - n<T>(13,3)*z[13];
    z[25]=z[8]*z[25];
    z[18]=z[18] + n<T>(1,54)*z[24] + z[25];
    z[24]=static_cast<T>(197)- n<T>(29,2)*z[13];
    z[25]= - n<T>(91,27) + z[12];
    z[25]=z[6]*z[25];
    z[24]=n<T>(1,27)*z[24] + z[25];
    z[24]=z[6]*z[24];
    z[18]=n<T>(1,2)*z[18] + z[24];
    z[18]=n<T>(1,2)*z[18] + z[21];
    z[18]=z[9]*z[18];
    z[21]=n<T>(29,6) - z[13];
    z[21]=n<T>(1,2)*z[21] - z[23];
    z[21]=z[21]*z[53];
    z[23]=static_cast<T>(11)- z[3];
    z[23]=z[23]*z[60];
    z[24]=n<T>(35,9) + 13*z[12];
    z[24]=n<T>(1,6)*z[24] + z[34];
    z[24]=z[24]*z[43];
    z[25]= - n<T>(179,36) - z[13];
    z[18]=z[18] + z[24] + z[23] + n<T>(1,3)*z[25] + z[21];
    z[18]=z[9]*z[18];
    z[18]=n<T>(1,2)*z[22] + z[18];

    r += n<T>(1,2)*z[18] + z[19] + z[20];
 
    return r;
}

template double qqb_2lha_r177(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r177(const std::array<dd_real,30>&);
#endif
