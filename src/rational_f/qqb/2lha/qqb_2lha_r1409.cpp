#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1409(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[12];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=npow(z[1],2);
    z[7]=z[4]*z[5];
    z[8]=z[7] + 1;
    z[9]=z[8]*z[4];
    z[9]=z[9] + z[1];
    z[10]= - z[4]*z[9];
    z[6]= - z[6] + z[10];
    z[10]=z[3]*z[5];
    z[11]= - 3*z[7] - z[10] - 1;
    z[12]=n<T>(1,2)*z[3];
    z[11]=z[11]*z[12];
    z[12]= - static_cast<T>(1)- n<T>(3,2)*z[7];
    z[12]=z[4]*z[12];
    z[11]=z[11] - n<T>(1,2)*z[1] + z[12];
    z[11]=z[3]*z[11];
    z[6]=n<T>(1,2)*z[6] + z[11];
    z[6]=z[2]*z[6];
    z[6]=z[9] + z[6];
    z[7]=n<T>(1,2)*z[10] + n<T>(1,2) + z[7];
    z[7]=z[3]*z[7];
    z[6]=z[7] + n<T>(1,2)*z[6];
    z[6]=z[2]*z[6];
    z[7]= - z[10] - z[8];

    r += z[6] + n<T>(1,4)*z[7];
 
    return r;
}

template double qqb_2lha_r1409(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1409(const std::array<dd_real,30>&);
#endif
