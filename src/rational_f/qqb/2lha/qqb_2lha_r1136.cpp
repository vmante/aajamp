#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1136(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[8];
    z[11]=k[10];
    z[12]=k[12];
    z[13]=k[15];
    z[14]= - static_cast<T>(1)+ n<T>(11,4)*z[13];
    z[14]=z[14]*z[13];
    z[15]=n<T>(1,2)*z[7];
    z[14]=z[14] - z[15];
    z[16]=n<T>(1,4)*z[3];
    z[17]= - static_cast<T>(1)- n<T>(11,12)*z[3];
    z[17]=z[17]*z[16];
    z[18]=z[6]*z[5];
    z[19]=39*z[4] - n<T>(145,3) - 9*z[5];
    z[19]=n<T>(1,8)*z[19] + n<T>(29,3)*z[18];
    z[19]=z[6]*z[19];
    z[20]=z[4] - n<T>(1,2);
    z[21]=z[4]*z[20];
    z[19]=z[19] + n<T>(9,2)*z[21] - n<T>(37,48)*z[3] - n<T>(31,8)*z[13] + n<T>(3,4)*z[7]
    + static_cast<T>(1)- n<T>(1,4)*z[12];
    z[19]=z[6]*z[19];
    z[21]= - n<T>(13,2) + 9*z[4];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(5,8) + z[21];
    z[21]=z[4]*z[21];
    z[17]=z[19] + z[21] + z[17] + z[14];
    z[17]=z[6]*z[17];
    z[19]=z[2]*z[3];
    z[21]=z[3] - z[19];
    z[22]=n<T>(1,2)*z[3];
    z[23]=z[22] - 1;
    z[21]=z[23]*z[21];
    z[24]=3*z[4];
    z[25]=static_cast<T>(2)+ z[2];
    z[25]=z[25]*z[24];
    z[26]= - static_cast<T>(7)+ 3*z[2];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[25]=z[4]*z[25];
    z[26]=z[2] - 1;
    z[25]=z[25] + z[26];
    z[25]=z[4]*z[25];
    z[27]=z[12]*z[8]*npow(z[6],3);
    z[17]= - n<T>(1,2)*z[27] + z[17] + z[25] + z[21];
    z[17]=z[8]*z[17];
    z[21]=n<T>(5,2)*z[3];
    z[25]= - static_cast<T>(1)+ n<T>(1,12)*z[3];
    z[25]=z[25]*z[21];
    z[25]=z[25] + z[14];
    z[28]=n<T>(1,4)*z[6];
    z[29]=n<T>(193,9)*z[18] + 15*z[4] - n<T>(67,3) - z[5];
    z[29]=z[29]*z[28];
    z[30]=z[7] + n<T>(11,4) + z[12];
    z[31]= - n<T>(7,8) + z[24];
    z[31]=z[4]*z[31];
    z[29]=z[29] + z[31] + n<T>(1,24)*z[3] + n<T>(1,2)*z[30] - z[13];
    z[29]=z[6]*z[29];
    z[23]=z[23]*z[19];
    z[30]=z[3] - 2;
    z[31]= - z[3]*z[30];
    z[31]=z[31] + z[23];
    z[31]=z[2]*z[31];
    z[32]= - static_cast<T>(1)- z[2];
    z[32]=z[2]*z[32];
    z[32]=static_cast<T>(2)+ z[32];
    z[32]=z[32]*z[24];
    z[33]=static_cast<T>(5)- n<T>(3,2)*z[2];
    z[33]=z[2]*z[33];
    z[32]=z[32] - n<T>(1,2) + z[33];
    z[32]=z[4]*z[32];
    z[33]=z[2] - 2;
    z[34]= - z[2]*z[33];
    z[32]=z[32] - n<T>(21,8) + z[34];
    z[32]=z[4]*z[32];
    z[17]=z[17] + z[29] + z[32] + n<T>(1,2)*z[25] + z[31];
    z[17]=z[8]*z[17];
    z[25]= - static_cast<T>(1)+ n<T>(17,12)*z[3];
    z[25]=z[25]*z[22];
    z[25]=z[25] + z[14];
    z[29]=npow(z[13],2);
    z[31]=static_cast<T>(19)- n<T>(17,2)*z[3];
    z[31]=z[3]*z[31];
    z[29]=n<T>(1,6)*z[31] - n<T>(11,2)*z[29] + n<T>(9,2) + z[7];
    z[31]=n<T>(13,12)*z[3];
    z[32]= - n<T>(313,18) + z[12];
    z[32]=n<T>(13,4)*z[4] - z[31] + n<T>(1,2)*z[32] + z[13];
    z[34]=n<T>(29,9)*z[6];
    z[32]=n<T>(1,2)*z[32] + z[34];
    z[32]=z[6]*z[32];
    z[35]= - static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[35]=z[4]*z[35];
    z[29]=z[32] + n<T>(1,4)*z[29] + z[35];
    z[29]=z[6]*z[29];
    z[20]=z[20]*z[24];
    z[20]= - n<T>(5,8) + z[20];
    z[20]=z[4]*z[20];
    z[20]=z[29] + n<T>(1,2)*z[25] + z[20];
    z[20]=z[6]*z[20];
    z[25]=z[4]*z[2];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[25]=z[25]*z[24];
    z[25]=z[25] - n<T>(1,2) + z[2];
    z[25]=z[4]*z[25];
    z[20]=z[20] + z[25] - z[22] - z[23];
    z[20]=z[8]*z[20];
    z[25]=5*z[3];
    z[29]=n<T>(1,3)*z[3];
    z[32]= - n<T>(1,2) + z[29];
    z[32]=z[32]*z[25];
    z[14]=z[32] + z[14];
    z[25]=n<T>(101,8) - z[25];
    z[25]=z[25]*z[29];
    z[32]= - n<T>(3,4) + z[4];
    z[32]=z[32]*z[24];
    z[35]=11*z[13];
    z[36]=n<T>(19,2) - z[35];
    z[36]=z[13]*z[36];
    z[36]=z[36] + n<T>(21,2) + z[7];
    z[25]=z[32] + n<T>(1,2)*z[36] + z[25];
    z[32]=n<T>(1,9)*z[6];
    z[36]=n<T>(425,8) - 29*z[5];
    z[36]=z[36]*z[32];
    z[37]=n<T>(9,4)*z[5] - n<T>(503,36) + z[13];
    z[36]=z[36] + n<T>(15,8)*z[4] + n<T>(1,2)*z[37] - z[3];
    z[36]=z[6]*z[36];
    z[25]=n<T>(1,2)*z[25] + z[36];
    z[25]=z[6]*z[25];
    z[19]=z[30]*z[19];
    z[30]=n<T>(7,2) - z[3];
    z[30]=z[3]*z[30];
    z[19]=z[30] + z[19];
    z[19]=z[2]*z[19];
    z[30]=n<T>(5,2) - z[2];
    z[30]=z[2]*z[30];
    z[36]=2*z[2];
    z[37]=static_cast<T>(1)- z[36];
    z[37]=z[2]*z[37];
    z[37]=static_cast<T>(1)+ z[37];
    z[37]=z[4]*z[37];
    z[30]=z[37] - static_cast<T>(1)+ z[30];
    z[30]=z[30]*z[24];
    z[36]=n<T>(7,2) - z[36];
    z[36]=z[2]*z[36];
    z[30]=z[30] - static_cast<T>(3)+ z[36];
    z[30]=z[4]*z[30];
    z[14]=z[20] + z[25] + z[30] + n<T>(1,2)*z[14] + z[19];
    z[14]=z[8]*z[14];
    z[19]=static_cast<T>(43)- n<T>(101,2)*z[5];
    z[19]=z[19]*z[32];
    z[20]= - n<T>(67,9) - 5*z[12];
    z[19]=z[19] - z[31] + 3*z[5] + n<T>(1,4)*z[20] - z[13];
    z[19]=z[6]*z[19];
    z[20]=static_cast<T>(19)- z[35];
    z[20]=z[13]*z[20];
    z[19]=z[19] + static_cast<T>(3)+ n<T>(1,4)*z[20];
    z[20]=static_cast<T>(1)- n<T>(11,24)*z[3];
    z[20]=z[20]*z[21];
    z[21]= - static_cast<T>(3)+ z[3];
    z[21]=z[3]*z[21];
    z[21]=z[21] - z[23];
    z[21]=z[2]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[2]*z[20];
    z[21]=n<T>(1,2)*z[2];
    z[23]= - static_cast<T>(2)+ z[21];
    z[23]=z[2]*z[23];
    z[23]=n<T>(3,2) + z[23];
    z[23]=z[2]*z[23];
    z[25]=z[4]*z[26]*npow(z[2],2);
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[24];
    z[25]= - static_cast<T>(3)+ z[2];
    z[25]=z[2]*z[25];
    z[25]=n<T>(31,8) + z[25];
    z[25]=z[2]*z[25];
    z[23]=z[23] - n<T>(15,8) + z[25];
    z[23]=z[4]*z[23];
    z[14]=z[14] + z[23] + z[20] + z[3] + n<T>(1,2)*z[19];
    z[14]=z[8]*z[14];
    z[19]=3*z[10];
    z[20]=static_cast<T>(1)+ z[11];
    z[20]=z[20]*z[19];
    z[23]=5*z[11];
    z[25]=z[11]*z[19];
    z[25]=z[25] + static_cast<T>(1)+ z[23];
    z[25]=z[5]*z[25];
    z[23]= - z[23] + 3*z[12];
    z[20]=z[25] + z[20] + n<T>(37,3) - z[23];
    z[25]= - static_cast<T>(5)- z[10];
    z[25]=z[25]*z[29];
    z[26]= - static_cast<T>(15)- n<T>(37,3)*z[10];
    z[25]=n<T>(1,2)*z[26] + z[25];
    z[25]=z[25]*z[16];
    z[26]=n<T>(5,2) + z[10];
    z[26]=z[3]*z[26];
    z[26]=n<T>(19,4)*z[10] + z[26];
    z[26]=z[26]*z[29];
    z[29]=z[2]*npow(z[3],2);
    z[29]= - n<T>(1,2) + n<T>(1,6)*z[29];
    z[29]=z[29]*z[10];
    z[26]=z[26] - z[29];
    z[26]=z[26]*z[21];
    z[25]=z[26] + z[25] - static_cast<T>(1)+ n<T>(1,4)*z[10];
    z[25]=z[2]*z[25];
    z[19]=n<T>(23,3) + z[19];
    z[19]=z[19]*z[16];
    z[26]=n<T>(1,3) + z[5];
    z[26]=z[5]*z[26];
    z[26]= - n<T>(5,6) + z[26];
    z[26]=z[26]*z[28];
    z[19]=z[26] + z[25] + z[19] - z[13] + n<T>(1,4)*z[20];
    z[14]=n<T>(1,2)*z[19] + z[14];
    z[14]=z[9]*z[14];
    z[19]= - static_cast<T>(1)- 3*z[11];
    z[19]=z[10]*z[19];
    z[19]=z[19] + static_cast<T>(3)+ z[23];
    z[20]=static_cast<T>(55)+ 19*z[10];
    z[23]=n<T>(1,3)*z[10];
    z[25]= - static_cast<T>(1)+ z[23];
    z[25]=z[3]*z[25];
    z[20]=n<T>(1,6)*z[20] + z[25];
    z[16]=z[20]*z[16];
    z[20]=n<T>(1,2) - z[23];
    z[20]=z[3]*z[20];
    z[20]= - n<T>(19,12)*z[10] + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[20] + z[29];
    z[20]=z[20]*z[21];
    z[21]= - n<T>(7,3) - z[5];
    z[18]=z[21]*z[18];
    z[18]=n<T>(29,6) + z[18];
    z[18]=z[18]*z[28];
    z[16]=z[18] + z[20] + z[16] + n<T>(1,4)*z[19] + z[13];
    z[14]=z[14] + n<T>(1,2)*z[16] + z[17];
    z[14]=z[9]*z[14];
    z[16]=z[4] - 1;
    z[17]=n<T>(13,8)*z[1];
    z[16]=z[17]*z[16];
    z[18]=n<T>(1,9)*z[5];
    z[19]=z[5] - z[1];
    z[20]=static_cast<T>(49)- n<T>(157,4)*z[19];
    z[20]=z[20]*z[18];
    z[20]= - z[17] + z[20];
    z[20]=z[5]*z[20];
    z[21]=z[1] + 1;
    z[23]= - z[5] + z[21];
    z[23]=z[23]*npow(z[5],3)*z[34];
    z[16]=z[23] + z[20] + z[16];
    z[16]=z[6]*z[16];
    z[20]=z[24]*z[1];
    z[23]=static_cast<T>(7)- z[1];
    z[23]=n<T>(1,2)*z[23] + z[20];
    z[23]=z[4]*z[23];
    z[25]= - static_cast<T>(3)+ z[15];
    z[25]=z[1]*z[25];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[5]*z[19];
    z[19]=z[23] + n<T>(7,6)*z[19] - n<T>(7,2) + z[25];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[16]=z[6]*z[16];
    z[19]=n<T>(31,9)*z[5];
    z[15]=z[19] - n<T>(31,9)*z[1] - n<T>(1,9) + z[15];
    z[23]=z[20] + static_cast<T>(3)- n<T>(7,2)*z[1];
    z[23]=z[4]*z[23];
    z[17]=z[23] - static_cast<T>(3)+ z[17];
    z[17]=z[4]*z[17];
    z[15]=z[16] + n<T>(1,2)*z[15] + z[17];
    z[15]=z[6]*z[15];
    z[16]=static_cast<T>(49)- 41*z[5];
    z[16]=z[16]*z[18];
    z[16]=13*z[4] - static_cast<T>(13)+ z[16];
    z[17]=static_cast<T>(1)- n<T>(1,3)*z[5];
    z[17]=z[6]*z[17]*npow(z[5],2);
    z[16]=n<T>(1,4)*z[16] + n<T>(29,3)*z[17];
    z[16]=z[6]*z[16];
    z[17]=z[19] - n<T>(91,9) + z[7];
    z[18]= - n<T>(5,4) + z[24];
    z[18]=z[4]*z[18];
    z[16]=z[16] + n<T>(1,2)*z[17] + z[18];
    z[16]=z[6]*z[16];
    z[17]=n<T>(1,2)*z[12] - z[7];
    z[17]= - n<T>(7,6)*z[3] + n<T>(1,2)*z[17] + 3*z[13];
    z[18]= - static_cast<T>(5)+ 6*z[4];
    z[18]=z[18]*npow(z[4],2);
    z[16]=z[16] + n<T>(1,2)*z[17] + z[18];
    z[16]=z[6]*z[16];
    z[17]=z[12] - z[6];
    z[17]=z[17]*npow(z[6],2);
    z[17]=z[17] + z[27];
    z[17]=z[8]*z[17];
    z[18]= - static_cast<T>(1)+ z[3];
    z[18]=z[3]*z[18];
    z[19]= - static_cast<T>(1)+ z[24];
    z[19]=z[4]*z[19];
    z[19]= - n<T>(1,2) + 2*z[19];
    z[19]=z[4]*z[19];
    z[16]=n<T>(1,4)*z[17] + z[16] + n<T>(1,8)*z[18] + z[19];
    z[16]=z[8]*z[16];
    z[17]=z[21]*z[22];
    z[17]= - n<T>(5,3) + z[17];
    z[17]=z[3]*z[17];
    z[18]= - z[3]*z[21];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[2]*z[18]*z[22];
    z[17]=z[17] + z[18];
    z[18]= - z[33]*z[20];
    z[19]= - static_cast<T>(3)+ n<T>(1,2)*z[1];
    z[19]=z[2]*z[19];
    z[18]=z[18] + z[19] + static_cast<T>(6)- n<T>(5,2)*z[1];
    z[18]=z[4]*z[18];
    z[19]= - n<T>(9,2) + z[2];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[4]*z[18];

    r += z[14] + z[15] + z[16] + n<T>(1,4)*z[17] + z[18];
 
    return r;
}

template double qqb_2lha_r1136(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1136(const std::array<dd_real,30>&);
#endif
