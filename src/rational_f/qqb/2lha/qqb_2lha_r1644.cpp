#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1644(const std::array<T,30>& k) {
  T z[54];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[6];
    z[9]=k[5];
    z[10]=n<T>(1,2)*z[4];
    z[11]=z[4] - 3;
    z[12]=z[11]*z[4];
    z[13]=n<T>(7,2) + z[12];
    z[13]=z[13]*z[10];
    z[13]= - static_cast<T>(1)+ z[13];
    z[13]=z[4]*z[13];
    z[13]=n<T>(3,4) + z[13];
    z[13]=z[4]*z[13];
    z[14]=n<T>(1,4)*z[9];
    z[15]=npow(z[4],2);
    z[16]= - z[15]*z[14];
    z[13]=z[13] + z[16];
    z[13]=z[13]*z[14];
    z[16]=z[4] - n<T>(23,8);
    z[16]=z[16]*z[4];
    z[17]= - static_cast<T>(3)- z[16];
    z[17]=z[4]*z[17];
    z[18]=z[4] - n<T>(7,2);
    z[18]=z[18]*z[4];
    z[19]= - n<T>(5,2) - z[18];
    z[19]=z[6]*z[19];
    z[17]=z[19] + static_cast<T>(1)+ z[17];
    z[17]=z[4]*z[17];
    z[13]=z[13] + n<T>(3,16) + z[17];
    z[17]=n<T>(1,2)*z[9];
    z[13]=z[13]*z[17];
    z[19]=n<T>(1,2)*z[6];
    z[20]=3*z[4];
    z[21]=n<T>(5,2) - z[20];
    z[21]=z[21]*z[19];
    z[22]=n<T>(1,2)*z[1];
    z[23]=z[4] - 1;
    z[24]= - z[1] + n<T>(23,8)*z[23] + z[6];
    z[24]=z[24]*z[22];
    z[25]=2*z[4];
    z[26]=n<T>(27,8) - z[25];
    z[26]=z[4]*z[26];
    z[21]=z[24] + z[21] - n<T>(3,8) + z[26];
    z[21]=z[1]*z[21];
    z[24]=n<T>(1,4)*z[4];
    z[26]=z[24] - 1;
    z[26]=z[26]*z[4];
    z[27]=n<T>(3,4) + z[26];
    z[27]=z[9]*z[4]*z[27];
    z[28]=z[10] - 1;
    z[29]=3*z[28] - z[1];
    z[29]=z[1]*z[29];
    z[27]=z[29] + z[27] - n<T>(3,2) - z[12];
    z[27]=z[2]*z[27];
    z[29]=n<T>(1,8)*z[4];
    z[30]= - static_cast<T>(27)+ n<T>(23,2)*z[4];
    z[30]=z[30]*z[29];
    z[30]=static_cast<T>(2)+ z[30];
    z[30]=z[4]*z[30];
    z[31]= - n<T>(13,2) + z[20];
    z[31]=z[4]*z[31];
    z[31]=n<T>(1,2) + z[31];
    z[31]=z[31]*z[19];
    z[13]=n<T>(1,2)*z[27] + z[21] + z[13] + z[31] + n<T>(1,8) + z[30];
    z[13]=z[2]*z[13];
    z[21]=n<T>(1,4)*z[6];
    z[27]=z[6]*z[5];
    z[30]=static_cast<T>(21)- 19*z[27];
    z[30]=z[30]*z[21];
    z[31]=z[10] + z[6];
    z[31]=z[9]*z[31];
    z[31]=z[31] + z[4] + n<T>(9,2)*z[6];
    z[31]=z[31]*z[17];
    z[31]=z[31] + z[24] + 5*z[6];
    z[31]=z[9]*z[31];
    z[30]=z[30] + z[31];
    z[31]=npow(z[5],3);
    z[32]=z[31]*z[6];
    z[33]=npow(z[5],2);
    z[34]=3*z[33] - z[32];
    z[34]=z[34]*z[21];
    z[34]= - z[5] + z[34];
    z[35]=npow(z[6],2);
    z[34]=z[34]*z[35];
    z[36]=z[33] - z[32];
    z[36]=z[6]*z[36];
    z[36]= - z[5] + z[36];
    z[37]=n<T>(1,8)*z[35];
    z[38]=z[37]*z[1];
    z[36]=z[36]*z[38];
    z[34]=z[34] + z[36];
    z[34]=z[1]*z[34];
    z[32]=9*z[33] - z[32];
    z[32]=z[6]*z[32];
    z[32]= - n<T>(45,2)*z[5] + z[32];
    z[32]=z[32]*z[37];
    z[32]=z[32] + z[34];
    z[32]=z[1]*z[32];
    z[34]=z[6]*z[33];
    z[34]= - n<T>(25,4)*z[5] + z[34];
    z[34]=z[6]*z[34];
    z[34]=static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[19];
    z[32]=z[34] + z[32];
    z[32]=z[1]*z[32];
    z[34]=static_cast<T>(7)+ z[9];
    z[34]=z[17]*z[34];
    z[34]=static_cast<T>(5)+ z[34];
    z[34]=z[17]*z[35]*z[34];
    z[34]= - 3*z[35] + z[34];
    z[36]= - z[35] - z[38];
    z[36]=z[1]*z[36];
    z[36]= - n<T>(41,16)*z[35] + z[36];
    z[36]=z[1]*z[36];
    z[36]= - n<T>(43,16)*z[35] + z[36];
    z[36]=z[1]*z[36];
    z[34]=n<T>(1,4)*z[34] + z[36];
    z[34]=z[3]*z[34];
    z[30]=z[34] + n<T>(1,4)*z[30] + z[32];
    z[30]=z[7]*z[30];
    z[18]= - n<T>(9,2) - z[18];
    z[18]=z[4]*z[18];
    z[18]=n<T>(5,2) + z[18];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[18]*z[15];
    z[26]=z[26] + n<T>(3,2);
    z[26]=z[26]*z[4];
    z[26]=z[26] - 1;
    z[26]=z[26]*z[4];
    z[26]=z[26] + n<T>(1,4);
    z[32]=npow(z[4],3);
    z[34]=z[32]*z[5];
    z[36]= - z[26]*z[34];
    z[18]=z[18] + z[36];
    z[36]=15*z[5];
    z[18]=z[18]*z[36];
    z[37]=49*z[4];
    z[38]=static_cast<T>(155)- z[37];
    z[38]=z[4]*z[38];
    z[38]= - static_cast<T>(183)+ z[38];
    z[38]=z[4]*z[38];
    z[38]=static_cast<T>(97)+ z[38];
    z[38]=z[4]*z[38];
    z[38]= - static_cast<T>(21)+ z[38];
    z[38]=z[38]*z[10];
    z[18]=z[38] + z[18];
    z[38]=n<T>(1,2)*z[5];
    z[18]=z[18]*z[38];
    z[39]=53*z[4];
    z[40]=static_cast<T>(137)- z[39];
    z[40]=z[40]*z[24];
    z[40]= - static_cast<T>(31)+ z[40];
    z[40]=z[40]*z[10];
    z[40]=static_cast<T>(5)+ z[40];
    z[40]=z[4]*z[40];
    z[18]=z[30] + z[18] + n<T>(1,16) + z[40];
    z[30]=z[4] - n<T>(9,2);
    z[40]=5*z[4];
    z[41]= - z[30]*z[40];
    z[41]= - n<T>(81,2) + z[41];
    z[41]=z[4]*z[41];
    z[41]=n<T>(147,4) + z[41];
    z[41]=z[4]*z[41];
    z[41]= - n<T>(69,4) + z[41];
    z[41]=z[4]*z[41];
    z[41]=n<T>(15,4) + z[41];
    z[41]=z[4]*z[41];
    z[41]= - n<T>(1,4) + z[41];
    z[42]=n<T>(5,2)*z[4];
    z[43]=static_cast<T>(13)- z[42];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(111,4) + z[43];
    z[43]=z[4]*z[43];
    z[43]=static_cast<T>(31)+ z[43];
    z[43]=z[4]*z[43];
    z[43]= - static_cast<T>(19)+ z[43];
    z[43]=z[43]*z[10];
    z[43]=static_cast<T>(3)+ z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(3,8) + z[43];
    z[44]=z[5]*z[4];
    z[43]=z[43]*z[44];
    z[41]=n<T>(1,2)*z[41] + z[43];
    z[41]=z[5]*z[41];
    z[43]=static_cast<T>(19)- z[40];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(113,4) + z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(41,2) + z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(33,4) + z[43];
    z[43]=z[4]*z[43];
    z[43]=static_cast<T>(3)+ z[43];
    z[41]=n<T>(1,4)*z[43] + z[41];
    z[41]=z[41]*z[38];
    z[43]=static_cast<T>(11)- z[42];
    z[43]=z[4]*z[43];
    z[43]= - static_cast<T>(19)+ z[43];
    z[43]=z[43]*z[29];
    z[43]=static_cast<T>(2)+ z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(13,16) + z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(1,8) + z[43];
    z[43]=z[5]*z[43];
    z[12]=z[12] + 3;
    z[45]=z[12]*z[4];
    z[45]=z[45] - 1;
    z[43]=n<T>(1,16)*z[45] + z[43];
    z[43]=z[5]*z[43];
    z[26]= - z[5]*z[26];
    z[26]=n<T>(1,4)*z[45] + z[26];
    z[26]=z[5]*z[26];
    z[46]=z[28]*z[4];
    z[46]=z[46] + n<T>(1,2);
    z[47]=n<T>(1,2)*z[46];
    z[26]= - z[47] + z[26];
    z[26]=z[26]*z[21];
    z[26]=z[26] - n<T>(1,8)*z[46] + z[43];
    z[26]=z[26]*z[27];
    z[27]=static_cast<T>(21)- z[40];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(69,2) + z[27];
    z[27]=z[4]*z[27];
    z[27]=n<T>(55,2) + z[27];
    z[27]=z[4]*z[27];
    z[27]= - n<T>(21,2) + z[27];
    z[27]=z[4]*z[27];
    z[27]=n<T>(3,2) + z[27];
    z[43]=static_cast<T>(3)- n<T>(5,8)*z[4];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(93,16) + z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(23,4) + z[43];
    z[43]=z[4]*z[43];
    z[43]= - static_cast<T>(3)+ z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(3,4) + z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(1,16) + z[43];
    z[43]=z[5]*z[43];
    z[27]=n<T>(1,8)*z[27] + z[43];
    z[27]=z[5]*z[27];
    z[27]= - n<T>(1,16)*z[46] + z[27];
    z[27]=z[5]*z[27];
    z[26]=z[27] + z[26];
    z[26]=z[6]*z[26];
    z[26]=z[41] + z[26];
    z[26]=z[6]*z[26];
    z[27]=n<T>(1,16)*z[4];
    z[41]=15*z[4];
    z[43]=static_cast<T>(73)- z[41];
    z[43]=z[43]*z[27];
    z[43]= - static_cast<T>(9)+ z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(73,8) + z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(79,16) + z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(21,16) + z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(1,8) + z[43];
    z[43]=z[4]*z[43];
    z[48]=n<T>(5,4)*z[4];
    z[49]=static_cast<T>(7)- z[48];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(65,4) + z[49];
    z[49]=z[49]*z[24];
    z[49]=static_cast<T>(5)+ z[49];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(55,16) + z[49];
    z[49]=z[4]*z[49];
    z[49]=n<T>(5,4) + z[49];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(3,16) + z[49];
    z[50]=z[15]*z[5];
    z[49]=z[49]*z[50];
    z[43]=z[43] + z[49];
    z[43]=z[5]*z[43];
    z[49]=static_cast<T>(31)- n<T>(15,2)*z[4];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(203,4) + z[49];
    z[49]=z[4]*z[49];
    z[49]=n<T>(83,2) + z[49];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(39,2) + z[49];
    z[49]=z[4]*z[49];
    z[49]=n<T>(15,2) + z[49];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(9,4) + z[49];
    z[43]=n<T>(1,8)*z[49] + z[43];
    z[43]=z[5]*z[43];
    z[49]=z[40] - 17;
    z[49]=z[49]*z[4];
    z[51]= - n<T>(45,2) - z[49];
    z[51]=z[4]*z[51];
    z[51]=n<T>(29,2) + z[51];
    z[51]=z[4]*z[51];
    z[51]= - n<T>(15,2) + z[51];
    z[27]=z[51]*z[27];
    z[26]=z[26] + z[27] + z[43];
    z[26]=z[6]*z[26];
    z[27]=z[38]*z[6];
    z[12]=z[12]*z[27];
    z[43]= - n<T>(3,2) + z[4];
    z[43]=z[4]*z[43];
    z[51]=z[5]*z[45];
    z[12]=z[12] + z[43] + z[51];
    z[12]=z[6]*z[12];
    z[43]=z[28]*z[15];
    z[51]=z[10]*z[5];
    z[52]=z[45]*z[51];
    z[12]=z[12] + z[43] + z[52];
    z[12]=z[9]*z[12];
    z[43]=n<T>(21,4) - z[4];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(45,4) + z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(25,2) + z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(15,2) + z[43];
    z[43]=z[4]*z[43];
    z[43]=n<T>(9,4) + z[43];
    z[43]=z[4]*z[43];
    z[43]= - n<T>(1,4) + z[43];
    z[43]=z[43]*z[15];
    z[52]=static_cast<T>(3)- z[10];
    z[52]=z[4]*z[52];
    z[52]= - n<T>(15,2) + z[52];
    z[52]=z[52]*z[10];
    z[52]=static_cast<T>(5)+ z[52];
    z[52]=z[4]*z[52];
    z[52]= - n<T>(15,4) + z[52];
    z[52]=z[4]*z[52];
    z[52]=n<T>(3,2) + z[52];
    z[52]=z[4]*z[52];
    z[52]= - n<T>(1,4) + z[52];
    z[34]=z[52]*z[34];
    z[34]=z[43] + z[34];
    z[34]=z[5]*z[34];
    z[30]= - z[30]*z[20];
    z[30]= - n<T>(97,4) + z[30];
    z[30]=z[30]*z[10];
    z[30]=static_cast<T>(11)+ z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(23,4) + z[30];
    z[30]=z[4]*z[30];
    z[30]=n<T>(9,4) + z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(5,8) + z[30];
    z[30]=z[4]*z[30];
    z[30]=z[30] + z[34];
    z[30]=z[5]*z[30];
    z[34]=static_cast<T>(11)- z[20];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(31,2) + z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(21,2) + z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(11,2) + z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(3,2) + z[34];
    z[34]=z[34]*z[24];
    z[30]=z[34] + z[30];
    z[12]=n<T>(1,16)*z[12] + n<T>(1,4)*z[30] + z[26];
    z[12]=z[9]*z[12];
    z[26]= - n<T>(41,4) + z[20];
    z[26]=z[26]*z[20];
    z[26]=n<T>(155,4) + z[26];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(171,8) + z[26];
    z[26]=z[4]*z[26];
    z[26]=n<T>(9,2) + z[26];
    z[26]=z[4]*z[26];
    z[26]= - n<T>(1,8) + z[26];
    z[30]= - n<T>(25,2) + z[20];
    z[30]=z[30]*z[20];
    z[30]=static_cast<T>(61)+ z[30];
    z[30]=z[30]*z[24];
    z[30]= - static_cast<T>(12)+ z[30];
    z[30]=z[4]*z[30];
    z[30]=n<T>(9,2) + z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(5,8) + z[30];
    z[30]=z[30]*z[44];
    z[26]=n<T>(1,2)*z[26] + z[30];
    z[26]=z[5]*z[26];
    z[16]=n<T>(11,4) + z[16];
    z[16]=z[16]*z[20];
    z[16]= - n<T>(5,2) + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[26] + n<T>(3,32) + z[16];
    z[16]=z[5]*z[16];
    z[26]=n<T>(3,2)*z[4];
    z[30]= - n<T>(11,4) + z[4];
    z[30]=z[4]*z[30];
    z[30]=n<T>(5,2) + z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(3,4) + z[30];
    z[30]=z[30]*z[26];
    z[34]= - n<T>(15,4) + z[4];
    z[34]=z[34]*z[26];
    z[34]=static_cast<T>(8)+ z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(21,4) + z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(3,2) + z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(1,8) + z[34];
    z[34]=z[5]*z[34];
    z[30]=z[30] + z[34];
    z[30]=z[5]*z[30];
    z[34]= - n<T>(9,4) + z[4];
    z[34]=z[4]*z[34];
    z[34]=n<T>(7,8) + z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(3,8) + z[34];
    z[30]=n<T>(1,2)*z[34] + z[30];
    z[30]=z[5]*z[30];
    z[34]= - static_cast<T>(5)+ z[26];
    z[34]=z[34]*z[10];
    z[34]=static_cast<T>(3)+ z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(3,2) + z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(1,4) + z[34];
    z[34]=z[5]*z[34];
    z[34]=n<T>(3,4)*z[46] + z[34];
    z[34]=z[5]*z[34];
    z[43]=z[4] - n<T>(5,2);
    z[52]=z[43]*z[4];
    z[52]=z[52] + n<T>(3,2);
    z[34]=n<T>(1,4)*z[52] + z[34];
    z[34]=z[34]*z[27];
    z[30]=z[30] + z[34];
    z[30]=z[6]*z[30];
    z[11]=z[11]*z[10];
    z[11]=static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[26];
    z[11]=z[30] + z[11] + z[16];
    z[11]=z[6]*z[11];
    z[16]= - n<T>(47,4) + z[20];
    z[16]=z[16]*z[20];
    z[16]=static_cast<T>(53)+ z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(75,2) + z[16];
    z[16]=z[16]*z[10];
    z[16]=static_cast<T>(6)+ z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(5,8) + z[16];
    z[16]=z[4]*z[16];
    z[30]= - n<T>(55,4) + z[20];
    z[30]=z[4]*z[30];
    z[30]=static_cast<T>(25)+ z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(45,2) + z[30];
    z[30]=z[30]*z[10];
    z[30]=static_cast<T>(5)+ z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(7,8) + z[30];
    z[30]=z[30]*z[50];
    z[16]=z[16] + z[30];
    z[16]=z[5]*z[16];
    z[30]=n<T>(347,16) + z[49];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(99,8) + z[30];
    z[30]=z[4]*z[30];
    z[30]=n<T>(49,16) + z[30];
    z[30]=z[4]*z[30];
    z[16]=z[16] - n<T>(3,8) + z[30];
    z[16]=z[5]*z[16];
    z[30]= - n<T>(23,4) + z[25];
    z[30]=z[4]*z[30];
    z[30]=n<T>(91,16) + z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(57,32) + z[30];
    z[30]=z[4]*z[30];
    z[11]=z[11] + z[16] - n<T>(1,4) + z[30];
    z[11]=z[6]*z[11];
    z[16]=z[32]*z[38];
    z[30]= - static_cast<T>(5)+ z[4];
    z[30]=z[30]*z[10];
    z[30]=static_cast<T>(5)+ z[30];
    z[30]=z[4]*z[30];
    z[30]= - static_cast<T>(5)+ z[30];
    z[30]=z[4]*z[30];
    z[30]=n<T>(5,2) + z[30];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(1,2) + z[30];
    z[30]=z[30]*z[16];
    z[34]= - n<T>(35,8) + z[4];
    z[34]=z[4]*z[34];
    z[34]=n<T>(15,2) + z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(25,4) + z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(5,2) + z[34];
    z[34]=z[4]*z[34];
    z[34]= - n<T>(3,8) + z[34];
    z[34]=z[34]*z[15];
    z[30]=z[34] + z[30];
    z[34]=3*z[5];
    z[30]=z[30]*z[34];
    z[49]=19*z[4];
    z[53]= - n<T>(147,2) + z[49];
    z[53]=z[4]*z[53];
    z[53]=n<T>(223,2) + z[53];
    z[53]=z[4]*z[53];
    z[53]= - n<T>(167,2) + z[53];
    z[53]=z[4]*z[53];
    z[53]=static_cast<T>(33)+ z[53];
    z[53]=z[4]*z[53];
    z[53]= - n<T>(13,2) + z[53];
    z[53]=z[53]*z[24];
    z[30]=z[53] + z[30];
    z[30]=z[30]*z[38];
    z[48]= - static_cast<T>(4)+ z[48];
    z[48]=z[4]*z[48];
    z[48]=n<T>(39,8) + z[48];
    z[48]=z[4]*z[48];
    z[48]= - n<T>(11,4) + z[48];
    z[48]=z[4]*z[48];
    z[48]=n<T>(25,32) + z[48];
    z[48]=z[4]*z[48];
    z[11]=z[12] + z[11] + z[48] + z[30];
    z[11]=z[9]*z[11];
    z[12]= - n<T>(7,8) + z[4];
    z[12]=z[12]*z[15];
    z[30]=n<T>(1,4)*z[5];
    z[32]=z[30]*z[32];
    z[48]=z[23]*z[32];
    z[12]=z[12] + z[48];
    z[12]=z[12]*z[34];
    z[34]= - n<T>(31,2) + z[49];
    z[34]=z[34]*z[24];
    z[12]=z[34] + z[12];
    z[12]=z[5]*z[12];
    z[12]=z[12] - static_cast<T>(1)+ z[42];
    z[34]= - z[5]*z[24];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[34]*z[33];
    z[48]=z[31]*z[21];
    z[34]=z[34] + z[48];
    z[34]=z[34]*z[19];
    z[48]=z[40] + z[50];
    z[48]=z[5]*z[48];
    z[48]=static_cast<T>(1)+ n<T>(1,8)*z[48];
    z[48]=z[5]*z[48];
    z[34]=z[48] + z[34];
    z[34]=z[6]*z[34];
    z[31]=z[31]*z[19];
    z[48]= - static_cast<T>(1)- z[51];
    z[48]=z[48]*z[33];
    z[48]=z[48] + z[31];
    z[48]=z[6]*z[48];
    z[49]=z[20] + z[50];
    z[49]=z[5]*z[49];
    z[49]=static_cast<T>(3)+ z[49];
    z[49]=z[49]*z[38];
    z[48]=z[49] + z[48];
    z[19]=z[48]*z[19];
    z[48]= - z[15] - z[32];
    z[48]=z[5]*z[48];
    z[48]= - z[26] + z[48];
    z[48]=z[5]*z[48];
    z[19]=z[19] - n<T>(3,4) + z[48];
    z[48]=n<T>(1,4)*z[1];
    z[19]=z[19]*z[48];
    z[12]=z[19] + n<T>(1,2)*z[12] + z[34];
    z[12]=z[1]*z[12];
    z[19]=n<T>(3,8)*z[4];
    z[34]=static_cast<T>(11)- z[41];
    z[34]=z[34]*z[19];
    z[34]=static_cast<T>(1)+ z[34];
    z[34]=z[4]*z[34];
    z[49]=static_cast<T>(1)- n<T>(3,4)*z[4];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(1,4) + z[49];
    z[49]=z[49]*z[50];
    z[34]=z[34] + n<T>(5,2)*z[49];
    z[34]=z[5]*z[34];
    z[49]=static_cast<T>(7)- n<T>(53,2)*z[4];
    z[49]=z[4]*z[49];
    z[49]=n<T>(65,4) + z[49];
    z[34]=n<T>(1,4)*z[49] + z[34];
    z[34]=z[5]*z[34];
    z[49]= - static_cast<T>(5)- z[51];
    z[33]=z[49]*z[33];
    z[31]=z[33] + z[31];
    z[21]=z[31]*z[21];
    z[31]= - static_cast<T>(7)- 23*z[4];
    z[21]=z[21] + n<T>(1,8)*z[31] + z[34];
    z[21]=z[6]*z[21];
    z[31]=n<T>(7,4) - z[4];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(3,4) + z[31];
    z[31]=z[31]*z[15];
    z[16]= - z[46]*z[16];
    z[16]=z[31] + z[16];
    z[16]=z[16]*z[36];
    z[31]=n<T>(159,2) - z[37];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(131,4) + z[31];
    z[31]=z[31]*z[10];
    z[16]=z[31] + z[16];
    z[16]=z[5]*z[16];
    z[31]=static_cast<T>(61)- z[39];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(9,2) + z[31];
    z[16]=n<T>(1,4)*z[31] + z[16];
    z[16]=n<T>(1,2)*z[16] + z[21];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[1]*z[12];
    z[16]= - n<T>(21,8) + z[4];
    z[16]=z[4]*z[16];
    z[16]=n<T>(9,4) + z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(5,8) + z[16];
    z[16]=z[16]*z[15];
    z[21]=z[45]*z[32];
    z[16]=z[16] + z[21];
    z[16]=z[5]*z[16];
    z[21]= - n<T>(53,2) + 11*z[4];
    z[21]=z[21]*z[20];
    z[21]=n<T>(257,4) + z[21];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(71,4) + z[21];
    z[21]=z[4]*z[21];
    z[21]=n<T>(1,8) + z[21];
    z[16]=n<T>(1,4)*z[21] + 5*z[16];
    z[16]=z[5]*z[16];
    z[21]= - static_cast<T>(29)+ z[41];
    z[21]=z[4]*z[21];
    z[21]=static_cast<T>(15)+ z[21];
    z[21]=z[4]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[10];
    z[31]=z[43]*z[10];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(1,4) + z[31];
    z[32]=5*z[50];
    z[31]=z[31]*z[32];
    z[21]=z[21] + z[31];
    z[21]=z[5]*z[21];
    z[31]=9*z[4];
    z[33]= - static_cast<T>(14)+ z[31];
    z[33]=z[4]*z[33];
    z[33]=n<T>(55,16) + z[33];
    z[33]=z[4]*z[33];
    z[21]=z[21] + n<T>(15,8) + z[33];
    z[21]=z[5]*z[21];
    z[28]=z[28]*z[40];
    z[28]=static_cast<T>(3)+ z[28];
    z[28]=z[4]*z[28];
    z[28]= - n<T>(1,2) + z[28];
    z[28]=z[28]*z[44];
    z[33]= - n<T>(21,4) + z[40];
    z[33]=z[4]*z[33];
    z[33]=n<T>(3,4) + z[33];
    z[33]=z[4]*z[33];
    z[28]=z[28] - n<T>(3,8) + z[33];
    z[28]=z[28]*z[38];
    z[25]= - n<T>(7,8) + z[25];
    z[25]=z[4]*z[25];
    z[25]=z[28] + n<T>(1,16) + z[25];
    z[25]=z[5]*z[25];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=n<T>(1,4)*z[20] + z[25];
    z[20]=z[6]*z[20];
    z[25]= - n<T>(17,4) + 4*z[4];
    z[25]=z[4]*z[25];
    z[20]=z[20] + z[21] - n<T>(19,16) + z[25];
    z[20]=z[6]*z[20];
    z[21]= - static_cast<T>(17)+ z[31];
    z[21]=z[4]*z[21];
    z[21]=n<T>(71,8) + z[21];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(1,2) + z[21];
    z[12]=z[12] + z[20] + n<T>(1,2)*z[21] + z[16];
    z[12]=z[1]*z[12];
    z[16]=static_cast<T>(103)- 45*z[4];
    z[16]=z[16]*z[29];
    z[16]= - static_cast<T>(9)+ z[16];
    z[16]=z[4]*z[16];
    z[16]=n<T>(27,16) + z[16];
    z[16]=z[4]*z[16];
    z[20]=static_cast<T>(7)- n<T>(9,4)*z[4];
    z[20]=z[20]*z[40];
    z[20]= - static_cast<T>(39)+ z[20];
    z[20]=z[20]*z[10];
    z[20]=static_cast<T>(9)+ z[20];
    z[20]=z[4]*z[20];
    z[20]= - n<T>(11,8) + z[20];
    z[20]=z[20]*z[51];
    z[16]=z[20] + n<T>(1,16) + z[16];
    z[16]=z[5]*z[16];
    z[20]=n<T>(69,2)*z[4];
    z[21]=static_cast<T>(61)- z[20];
    z[21]=z[4]*z[21];
    z[21]= - static_cast<T>(21)+ z[21];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(3,2) + z[21];
    z[16]=n<T>(1,8)*z[21] + z[16];
    z[16]=z[5]*z[16];
    z[21]=static_cast<T>(3)- z[42];
    z[15]=z[21]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[19]=static_cast<T>(1)- z[19];
    z[19]=z[19]*z[40];
    z[19]= - n<T>(9,2) + z[19];
    z[19]=z[4]*z[19];
    z[19]=n<T>(3,2) + z[19];
    z[19]=z[4]*z[19];
    z[19]= - n<T>(1,8) + z[19];
    z[19]=z[5]*z[19];
    z[15]=n<T>(3,4)*z[15] + z[19];
    z[15]=z[5]*z[15];
    z[19]= - z[4]*z[23];
    z[15]=z[15] - n<T>(3,8) + z[19];
    z[15]=z[15]*z[27];
    z[19]=static_cast<T>(2)- z[26];
    z[19]=z[4]*z[19];
    z[15]=z[15] + z[16] + n<T>(1,2) + z[19];
    z[15]=z[6]*z[15];
    z[16]=static_cast<T>(199)- n<T>(135,2)*z[4];
    z[16]=z[16]*z[24];
    z[16]= - static_cast<T>(51)+ z[16];
    z[16]=z[4]*z[16];
    z[16]=n<T>(81,4) + z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(17,8) + z[16];
    z[10]=z[16]*z[10];
    z[16]=static_cast<T>(2)- n<T>(9,16)*z[4];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(21,8) + z[16];
    z[16]=z[4]*z[16];
    z[16]=n<T>(3,2) + z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(5,16) + z[16];
    z[16]=z[16]*z[32];
    z[10]=z[10] + z[16];
    z[10]=z[5]*z[10];
    z[16]=static_cast<T>(101)- n<T>(159,4)*z[4];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(333,4) + z[16];
    z[16]=z[4]*z[16];
    z[16]=n<T>(77,4) + z[16];
    z[16]=z[4]*z[16];
    z[16]=n<T>(7,4) + z[16];
    z[10]=n<T>(1,4)*z[16] + z[10];
    z[10]=z[5]*z[10];
    z[16]=static_cast<T>(71)- z[20];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(67,2) + z[16];
    z[16]=z[4]*z[16];
    z[16]= - n<T>(11,4) + z[16];
    z[10]=z[15] + n<T>(1,8)*z[16] + z[10];
    z[10]=z[6]*z[10];
    z[15]= - z[9]*z[30];
    z[15]= - z[5] + z[15];
    z[15]=z[9]*z[35]*z[15];
    z[16]=static_cast<T>(3)- n<T>(7,4)*z[5];
    z[16]=z[6]*z[16];
    z[16]= - n<T>(3,4) + z[16];
    z[16]=z[6]*z[16];
    z[15]=z[16] + z[15];
    z[14]=z[15]*z[14];
    z[15]=n<T>(1,8)*z[1];
    z[16]= - static_cast<T>(1)+ z[6];
    z[15]=z[16]*z[15];
    z[15]=z[15] - n<T>(5,8) + z[6];
    z[15]=z[1]*z[15];
    z[16]= - static_cast<T>(1)+ 7*z[6];
    z[15]=n<T>(5,16)*z[16] + z[15];
    z[15]=z[1]*z[15];
    z[16]=z[38] - 1;
    z[19]=z[16]*z[6];
    z[20]=n<T>(11,4) - z[19];
    z[20]=z[6]*z[20];
    z[20]=static_cast<T>(1)+ z[20];
    z[15]=n<T>(1,4)*z[20] + z[15];
    z[15]=z[1]*z[15];
    z[20]=static_cast<T>(1)- n<T>(3,8)*z[5];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(13,16) + z[20];
    z[20]=z[6]*z[20];
    z[14]=z[15] + z[14] + n<T>(1,16) + z[20];
    z[15]=static_cast<T>(3)+ z[1];
    z[15]=z[15]*z[48];
    z[15]=z[15] - n<T>(11,8) + z[6];
    z[15]=z[15]*z[48];
    z[15]=z[15] - n<T>(1,2) + z[6];
    z[15]=z[1]*z[15];
    z[20]=static_cast<T>(1)+ z[48];
    z[20]=z[1]*z[20];
    z[20]=n<T>(3,4) + z[20];
    z[20]=z[2]*z[20]*z[22];
    z[21]= - n<T>(1,8) + 3*z[6];
    z[15]=z[20] + n<T>(1,4)*z[21] + z[15];
    z[15]=z[2]*z[15];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[14]=z[3]*z[14];
    z[15]=z[6] + 1;
    z[15]=z[15]*z[6];
    z[20]=z[15] + n<T>(1,2);
    z[21]=z[35]*z[52]*z[20];
    z[15]= - z[46]*z[15];
    z[15]= - z[47] + z[15];
    z[15]=z[9]*z[15]*npow(z[6],3);
    z[15]=z[21] + z[15];
    z[15]=z[15]*z[17];
    z[17]=z[4] - n<T>(3,4);
    z[17]=z[6]*z[17]*z[20];
    z[19]= - z[19] - z[16];
    z[19]=z[6]*z[19];
    z[16]= - n<T>(1,2)*z[16] + z[19];
    z[16]=z[16]*z[22];
    z[15]=z[16] + z[17] + z[15];
    z[15]=z[8]*z[15];

    r += z[10] + z[11] + z[12] + z[13] + z[14] + n<T>(1,4)*z[15] + n<T>(1,2)*
      z[18];
 
    return r;
}

template double qqb_2lha_r1644(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1644(const std::array<dd_real,30>&);
#endif
