#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r165(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[7];
    z[6]=k[3];
    z[7]=k[2];
    z[8]=k[4];
    z[9]=k[5];
    z[10]=z[6] - 1;
    z[11]=z[10]*z[2];
    z[12]=3*z[6];
    z[13]=z[12]*z[11];
    z[14]=n<T>(3,2) + z[5];
    z[15]=n<T>(1,2) + z[12];
    z[15]=z[6]*z[15];
    z[13]=z[13] + n<T>(1,2)*z[14] + z[15];
    z[14]=n<T>(1,2)*z[2];
    z[13]=z[13]*z[14];
    z[15]=3*z[4];
    z[16]= - static_cast<T>(3)+ z[7];
    z[16]=z[5]*z[16];
    z[16]= - z[15] - static_cast<T>(9)+ z[16];
    z[17]=n<T>(1,2)*z[4];
    z[18]=static_cast<T>(11)- z[17];
    z[18]=z[18]*z[17];
    z[19]=npow(z[4],2);
    z[20]=z[19]*z[6];
    z[18]=n<T>(1,4)*z[20] + static_cast<T>(1)+ z[18];
    z[18]=z[6]*z[18];
    z[21]=static_cast<T>(27)- z[4];
    z[21]=z[4]*z[21];
    z[21]=z[21] + z[20];
    z[21]=z[6]*z[21];
    z[21]= - z[15] + z[21];
    z[22]=n<T>(1,2)*z[6];
    z[21]=z[21]*z[22];
    z[23]=n<T>(1,2)*z[7];
    z[21]=z[21] + static_cast<T>(1)+ z[23];
    z[24]=n<T>(3,4) + z[6];
    z[24]=z[6]*z[24];
    z[24]= - n<T>(3,8) + z[24];
    z[24]=z[2]*z[24];
    z[21]=n<T>(1,4)*z[21] + z[24];
    z[21]=z[3]*z[21];
    z[13]=z[21] + z[13] + n<T>(1,8)*z[16] + z[18];
    z[13]=z[3]*z[13];
    z[16]=2*z[4];
    z[18]=n<T>(3,4)*z[2] - n<T>(3,2);
    z[18]=z[6]*z[18];
    z[21]=n<T>(1,4)*z[5];
    z[24]= - static_cast<T>(9)- z[21];
    z[18]=n<T>(1,4)*z[24] + z[16] + z[18];
    z[18]=z[2]*z[18];
    z[24]=n<T>(1,8)*z[5];
    z[25]=static_cast<T>(1)- z[23];
    z[25]=z[25]*z[24];
    z[26]= - 15*z[4] - z[20];
    z[26]=z[6]*z[26];
    z[16]=z[18] + n<T>(3,16)*z[26] + z[25] - z[16];
    z[16]=z[3]*z[16];
    z[18]=static_cast<T>(7)+ n<T>(3,2)*z[5];
    z[18]=n<T>(3,2)*z[11] + n<T>(1,4)*z[18] - z[12];
    z[18]=z[2]*z[18];
    z[25]=n<T>(1,2)*z[5];
    z[26]= - static_cast<T>(5)- z[25];
    z[26]= - z[12] + n<T>(3,2)*z[26] + 11*z[4];
    z[18]=n<T>(1,2)*z[26] + z[18];
    z[18]=z[2]*z[18];
    z[26]= - z[20] - 13*z[4] - static_cast<T>(1)+ z[21];
    z[18]=n<T>(1,2)*z[26] + z[18];
    z[16]=n<T>(1,2)*z[18] + z[16];
    z[16]=z[3]*z[16];
    z[18]=z[25] - 1;
    z[26]= - static_cast<T>(5)- z[17];
    z[26]=z[4]*z[26];
    z[21]=z[21] + 1;
    z[27]=z[21] - z[2];
    z[27]=n<T>(17,4)*z[4] + 3*z[27];
    z[27]=z[2]*z[27];
    z[26]=z[27] + z[26] - z[18];
    z[26]=z[2]*z[26];
    z[26]=n<T>(1,2)*z[19] + z[26];
    z[24]=z[24] + 3;
    z[27]=z[14] + n<T>(19,8)*z[4] - z[24];
    z[27]=z[2]*z[27];
    z[28]= - static_cast<T>(49)- z[15];
    z[28]=z[4]*z[28];
    z[24]=z[27] + n<T>(1,8)*z[28] + z[24];
    z[24]=z[2]*z[24];
    z[27]=static_cast<T>(11)+ z[4];
    z[27]=z[4]*z[27];
    z[20]=z[27] + z[20];
    z[20]=n<T>(3,8)*z[20] + z[24];
    z[20]=z[3]*z[20];
    z[20]=n<T>(1,2)*z[26] + z[20];
    z[20]=z[3]*z[20];
    z[24]=n<T>(9,2) + z[4];
    z[24]=z[4]*z[24];
    z[24]=z[25] + z[24];
    z[26]= - static_cast<T>(13)- z[4];
    z[26]=z[4]*z[26];
    z[26]= - z[5] + z[26];
    z[27]=z[2]*z[4];
    z[26]=n<T>(1,4)*z[26] + z[27];
    z[26]=z[2]*z[26];
    z[24]=n<T>(1,2)*z[24] + z[26];
    z[24]=z[2]*z[24];
    z[26]=n<T>(1,4)*z[19];
    z[24]= - z[26] + z[24];
    z[27]=n<T>(1,2)*z[1];
    z[24]=z[24]*npow(z[3],2)*z[27];
    z[20]=z[20] + z[24];
    z[20]=z[20]*z[27];
    z[24]=n<T>(3,4)*z[5];
    z[11]= - 3*z[11] - static_cast<T>(7)- z[24];
    z[11]=z[2]*z[11];
    z[27]=n<T>(11,2)*z[4] - static_cast<T>(5)+ z[25];
    z[11]=n<T>(1,2)*z[27] + z[11];
    z[11]=z[2]*z[11];
    z[11]= - z[26] + z[11];
    z[11]=z[20] + n<T>(1,4)*z[11] + z[16];
    z[11]=z[1]*z[11];
    z[16]=static_cast<T>(17)- z[4];
    z[16]=z[16]*z[17];
    z[17]=z[19]*z[22];
    z[16]=z[17] + z[16] - z[18];
    z[17]= - z[22] + 1;
    z[17]=z[6]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[18]=z[8] - 3;
    z[14]=z[14]*z[18]*z[17];
    z[17]=z[5]*z[18];
    z[17]=z[17] - static_cast<T>(13)+ 9*z[8];
    z[18]=static_cast<T>(1)- n<T>(5,8)*z[8];
    z[18]=z[6]*z[18];
    z[14]=z[14] + n<T>(1,16)*z[17] + z[18];
    z[14]=z[2]*z[14];
    z[17]=z[12] - static_cast<T>(1)+ z[24];
    z[14]=n<T>(1,4)*z[17] + z[14];
    z[14]=z[2]*z[14];
    z[11]=z[11] + n<T>(1,2)*z[13] + n<T>(1,8)*z[16] + z[14];
    z[11]=z[1]*z[11];
    z[13]= - z[15]*z[10];
    z[14]= - z[9]*z[23];
    z[13]=z[14] + z[13];
    z[13]=z[13]*z[22];
    z[13]=z[13] - static_cast<T>(1)+ n<T>(1,4)*z[7];
    z[13]=z[3]*z[13];
    z[14]= - z[7] - 1;
    z[14]=z[9]*z[14];
    z[14]=static_cast<T>(7)+ z[14];
    z[16]=z[15] + 1;
    z[17]=n<T>(1,4)*z[9] - z[16];
    z[17]=z[6]*z[17];
    z[13]=z[13] + z[17] + n<T>(1,4)*z[14] + z[15];
    z[13]=z[6]*z[13];
    z[14]=n<T>(5,2) - z[6];
    z[14]=z[6]*z[14];
    z[14]= - n<T>(9,4) + z[14];
    z[14]=z[6]*z[14];
    z[14]=n<T>(3,4) + z[14];
    z[14]=z[2]*z[14];
    z[13]=z[14] - n<T>(3,2) + z[13];
    z[13]=z[3]*z[13];
    z[14]= - static_cast<T>(1)- z[8];
    z[14]=z[5]*z[14];
    z[14]=z[14] + static_cast<T>(1)- z[8];
    z[15]=z[8] - z[10];
    z[15]=z[6]*z[15];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[12]= - n<T>(11,2) + z[12];
    z[12]=z[22]*z[12];
    z[12]=z[12] + z[21];
    z[12]=z[8]*z[12];
    z[15]= - static_cast<T>(3)+ z[6];
    z[15]=z[6]*z[15];
    z[15]=static_cast<T>(3)+ z[15];
    z[15]=z[6]*z[8]*z[15];
    z[15]= - z[8] + z[15];
    z[15]=z[2]*z[15];
    z[12]=z[15] + z[12];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,2)*z[14] + z[12];
    z[12]=z[2]*z[12];
    z[14]=z[16] - n<T>(1,2)*z[9];
    z[10]= - z[14]*z[10];
    z[10]=z[25] + z[10];
    z[10]=z[13] + n<T>(1,2)*z[10] + z[12];

    r += n<T>(1,4)*z[10] + z[11];
 
    return r;
}

template double qqb_2lha_r165(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r165(const std::array<dd_real,30>&);
#endif
