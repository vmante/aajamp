#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r445(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[4];
    z[4]=k[13];
    z[5]=k[6];
    z[6]=k[10];
    z[7]=k[7];
    z[8]= - static_cast<T>(1)+ z[4];
    z[9]=n<T>(1,4)*z[3];
    z[10]=z[5] - z[7];
    z[8]=z[9]*z[10]*z[8]*npow(z[6],2);
    z[11]=n<T>(3,2)*z[5];
    z[12]=z[11] - 1;
    z[13]=n<T>(1,2)*z[7];
    z[14]=z[13] - z[12];
    z[14]=z[6]*z[14];
    z[10]=z[14] - static_cast<T>(1)- z[10];
    z[14]=n<T>(1,2)*z[6];
    z[10]=z[10]*z[14];
    z[15]= - z[6] - 1;
    z[16]= - n<T>(3,4)*z[5] + static_cast<T>(1)+ n<T>(1,4)*z[7];
    z[15]=z[4]*z[6]*z[16]*z[15];
    z[8]=z[8] + z[10] + z[15];
    z[8]=z[3]*z[8];
    z[10]=z[13] - 1;
    z[13]=n<T>(1,2)*z[5];
    z[15]= - z[13] + z[10];
    z[12]= - z[12]*z[14];
    z[10]=z[12] + n<T>(1,2)*z[10] - z[5];
    z[10]=z[6]*z[10];
    z[12]=z[14] + 1;
    z[11]= - z[2] + z[11] - n<T>(5,2);
    z[11]=z[6]*z[11]*z[12];
    z[11]=z[11] - static_cast<T>(1)+ z[13];
    z[11]=z[4]*z[11];
    z[8]=z[8] + z[11] + n<T>(1,2)*z[15] + z[10];
    z[8]=z[3]*z[4]*z[8];
    z[10]=z[5] - 1;
    z[11]=static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[10]=z[11]*z[10];
    z[15]=static_cast<T>(3)+ z[6];
    z[13]= - z[2] + z[13] - n<T>(1,2);
    z[13]=z[6]*z[13]*z[15];
    z[15]=z[2] - n<T>(5,2) - z[1];
    z[15]=z[2]*z[15];
    z[10]=z[13] + z[15] + z[10];
    z[10]=z[4]*z[10];
    z[10]=z[10] + z[2];
    z[12]= - z[5]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[12]*z[14];
    z[10]=z[12] - z[11] + n<T>(1,2)*z[10];
    z[10]=z[4]*z[10];
    z[8]=z[10] + z[8];

    r += z[9]*z[8];
 
    return r;
}

template double qqb_2lha_r445(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r445(const std::array<dd_real,30>&);
#endif
