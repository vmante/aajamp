#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1023(const std::array<T,30>& k) {
  T z[44];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[29];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[5];
    z[9]=k[9];
    z[10]=k[8];
    z[11]=k[27];
    z[12]=k[12];
    z[13]=z[5]*z[4];
    z[14]=z[13] + z[4];
    z[14]=z[14]*z[5];
    z[15]=n<T>(5,2)*z[4];
    z[16]= - z[15] + z[14];
    z[16]=z[5]*z[16];
    z[17]=5*z[4];
    z[16]= - z[17] + z[16];
    z[16]=z[5]*z[16];
    z[18]=n<T>(7,2)*z[4];
    z[16]= - z[18] + z[16];
    z[16]=z[5]*z[16];
    z[16]= - z[4] + z[16];
    z[19]=z[7]*z[8];
    z[20]=n<T>(1,2)*z[5];
    z[16]=z[16]*z[20]*z[19];
    z[21]=3*z[4];
    z[22]=z[21] + z[13];
    z[22]=z[22]*z[5];
    z[22]=z[22] + z[18];
    z[23]= - z[5]*z[22];
    z[24]=n<T>(3,4)*z[4];
    z[23]= - z[24] + z[23];
    z[23]=z[5]*z[23];
    z[25]=z[4] + n<T>(1,2)*z[13];
    z[26]=z[25]*z[5];
    z[27]=z[15] + 3*z[26];
    z[27]=z[5]*z[27];
    z[28]=n<T>(1,4)*z[4];
    z[27]= - z[28] + z[27];
    z[27]=z[5]*z[27];
    z[29]=n<T>(1,2)*z[4];
    z[27]= - z[29] + z[27];
    z[30]=z[8]*z[5];
    z[27]=z[27]*z[30];
    z[16]=z[16] + z[27] - z[29] + z[23];
    z[16]=z[7]*z[16];
    z[23]=z[26] + z[4];
    z[23]=z[23]*z[5];
    z[27]=n<T>(1,8)*z[4];
    z[31]=z[23] - z[27];
    z[32]=z[15] + z[13];
    z[33]=z[32]*z[5];
    z[34]=z[15] + z[33];
    z[34]=z[5]*z[34];
    z[34]=z[4] + n<T>(3,2)*z[34];
    z[34]=z[34]*z[30];
    z[16]=z[16] + z[34] - z[31];
    z[22]=z[22]*z[20];
    z[22]=z[22] + z[4];
    z[22]=z[22]*z[5];
    z[22]=z[22] + z[28];
    z[32]=z[32]*z[20];
    z[32]=z[4] + z[32];
    z[32]=z[5]*z[32];
    z[32]=z[27] + z[32];
    z[32]=z[5]*z[32];
    z[32]= - z[28] + z[32];
    z[32]=z[5]*z[32];
    z[32]= - z[27] + z[32];
    z[32]=z[8]*z[32];
    z[32]= - n<T>(1,2)*z[22] + z[32];
    z[32]=z[7]*z[32];
    z[34]=n<T>(9,4)*z[4];
    z[35]=z[34] + z[13];
    z[35]=z[5]*z[35];
    z[34]=z[34] + z[35];
    z[34]=z[5]*z[34];
    z[35]=n<T>(5,8)*z[4];
    z[34]=z[35] + z[34];
    z[34]=z[34]*z[30];
    z[31]=z[32] - n<T>(1,2)*z[31] + z[34];
    z[31]=z[7]*z[31];
    z[32]=z[23] + z[28];
    z[34]=z[8]*z[32];
    z[36]=z[22]*z[19];
    z[34]=z[34] + z[36];
    z[36]=n<T>(1,2)*z[7];
    z[34]=z[6]*z[34]*z[36];
    z[37]=n<T>(1,2)*z[8];
    z[32]=z[32]*z[37];
    z[31]=z[34] + z[32] + z[31];
    z[31]=z[6]*z[31];
    z[16]=n<T>(1,2)*z[16] + z[31];
    z[16]=z[6]*z[16];
    z[31]= - z[28] + z[13];
    z[31]=z[31]*z[20];
    z[32]=n<T>(5,4)*z[4];
    z[26]= - z[32] - z[26];
    z[26]=z[5]*z[26];
    z[26]= - z[29] + z[26];
    z[34]=npow(z[5],2);
    z[38]=z[34]*z[8];
    z[26]=z[26]*z[38];
    z[22]=z[22]*z[34];
    z[39]= - z[22] - n<T>(3,8)*z[8];
    z[19]=z[39]*z[19];
    z[19]=z[19] + z[31] + z[26];
    z[19]=z[19]*z[36];
    z[26]=n<T>(7,4)*z[2] - n<T>(39,8);
    z[26]=z[4]*z[26];
    z[26]= - z[13] + z[26];
    z[26]=z[2]*z[26];
    z[31]=9*z[4] + n<T>(17,4)*z[13];
    z[26]=n<T>(1,2)*z[31] + z[26];
    z[26]=z[2]*z[26];
    z[31]=z[2]*z[29];
    z[25]=z[31] - z[25];
    z[25]=z[2]*z[25];
    z[31]=z[4] + n<T>(3,2)*z[13];
    z[25]=n<T>(1,2)*z[31] + z[25];
    z[25]=z[2]*z[25];
    z[31]=n<T>(1,4)*z[13];
    z[25]= - z[31] + z[25];
    z[39]=n<T>(1,2)*z[3];
    z[25]=z[25]*z[39];
    z[40]=5*z[13];
    z[41]= - n<T>(17,4)*z[4] - z[40];
    z[25]=z[25] + n<T>(1,4)*z[41] + z[26];
    z[25]=z[2]*z[25];
    z[25]= - n<T>(5,16)*z[4] + z[25];
    z[25]=z[3]*z[25];
    z[26]= - z[6]*z[7];
    z[26]=z[26] - 1;
    z[41]=n<T>(3,2)*z[4];
    z[26]=z[41]*z[26];
    z[42]= - z[7]*z[13];
    z[26]=z[42] + z[26];
    z[25]=n<T>(1,8)*z[26] + z[25];
    z[25]=z[1]*z[25];
    z[26]=n<T>(9,8)*z[4];
    z[31]= - z[4] - z[31];
    z[31]=z[5]*z[31];
    z[31]= - z[26] + z[31];
    z[31]=z[5]*z[31];
    z[31]= - z[28] + z[31];
    z[31]=z[5]*z[31];
    z[42]=z[41] + z[13];
    z[42]=z[42]*z[34];
    z[42]= - z[29] + z[42];
    z[42]=z[42]*z[30];
    z[16]=z[25] + z[16] + z[19] + n<T>(1,4)*z[42] + n<T>(1,16)*z[4] + z[31];
    z[19]=z[34]*z[29];
    z[25]=z[2]*z[13];
    z[14]=z[25] - z[14] - z[8];
    z[14]=z[2]*z[14];
    z[14]=z[14] + z[19] + z[30];
    z[14]=z[2]*z[14];
    z[19]=npow(z[30],2);
    z[25]=z[19]*z[36];
    z[31]= - z[2]*z[30];
    z[19]=z[31] - z[19] + z[25];
    z[19]=z[19]*z[39];
    z[31]=n<T>(1,4)*z[5];
    z[39]= - static_cast<T>(1)- z[31];
    z[39]=z[39]*z[30];
    z[31]= - z[31] + z[39];
    z[31]=z[8]*z[31];
    z[39]=z[5] + 3*z[30];
    z[39]=z[8]*z[39];
    z[25]=z[39] - z[25];
    z[39]=n<T>(1,4)*z[7];
    z[25]=z[25]*z[39];
    z[14]=z[19] + n<T>(1,2)*z[14] + z[31] + z[25];
    z[14]=z[3]*z[14];
    z[19]=static_cast<T>(1)+ z[8];
    z[19]=z[19]*z[37];
    z[25]=z[34] + z[38];
    z[25]=z[3]*z[25]*z[37];
    z[31]=z[5] + z[30];
    z[31]=z[8]*z[31];
    z[25]=z[31] + z[25];
    z[25]=z[3]*z[25];
    z[19]=z[19] + z[25];
    z[19]=z[12]*z[19];
    z[25]= - static_cast<T>(1)+ z[4];
    z[25]=z[25]*z[20];
    z[31]=z[5] + n<T>(3,2);
    z[34]= - z[8]*z[31];
    z[25]=z[25] + z[34];
    z[25]=z[8]*z[25];
    z[34]=npow(z[8],2);
    z[38]=z[34]*z[7];
    z[42]= - z[20]*z[38];
    z[43]=n<T>(1,2) + z[8];
    z[43]=z[8]*z[43];
    z[42]=z[43] + z[42];
    z[42]=z[7]*z[42];
    z[43]= - z[4] + n<T>(5,2)*z[13];
    z[19]=z[19] + n<T>(3,2)*z[42] + n<T>(1,2)*z[43] + z[25];
    z[21]= - z[21] - n<T>(5,4)*z[13];
    z[21]=z[5]*z[21];
    z[25]= - n<T>(7,4) - z[13];
    z[25]=z[25]*z[37];
    z[42]=n<T>(3,2)*z[8] + 9;
    z[42]=z[13]*z[42];
    z[43]= - z[2] + 1;
    z[43]=z[4]*z[43];
    z[42]=z[42] + z[43];
    z[43]=n<T>(1,4)*z[2];
    z[42]=z[42]*z[43];
    z[21]=z[42] + z[25] + n<T>(3,16)*z[4] + z[21];
    z[21]=z[2]*z[21];
    z[25]=3*z[13];
    z[15]=z[15] + z[25];
    z[15]=z[5]*z[15];
    z[15]= - z[29] + z[15];
    z[42]=static_cast<T>(1)+ z[27];
    z[42]=z[5]*z[42];
    z[42]=n<T>(11,8) + z[42];
    z[42]=z[8]*z[42];
    z[15]=n<T>(1,2)*z[15] + z[42];
    z[15]=n<T>(1,2)*z[15] + z[21];
    z[15]=z[2]*z[15];
    z[14]=n<T>(1,4)*z[14] + z[15] + n<T>(1,8)*z[19];
    z[14]=z[3]*z[14];
    z[15]=n<T>(11,2)*z[8] + 11;
    z[15]=z[13]*z[15];
    z[19]=n<T>(11,4)*z[4];
    z[21]= - z[19] - 9*z[13];
    z[21]=z[6]*z[8]*z[21];
    z[18]=z[6]*z[18];
    z[18]=z[18] + z[29];
    z[18]=z[8]*z[18];
    z[18]= - 7*z[4] + z[18];
    z[18]=z[2]*z[18];
    z[15]=z[18] + z[21] + n<T>(25,2)*z[4] + z[15];
    z[15]=z[15]*z[43];
    z[18]=n<T>(23,2)*z[4];
    z[21]= - z[18] - 19*z[13];
    z[21]=z[5]*z[21];
    z[21]= - z[41] + z[21];
    z[21]=z[8]*z[21];
    z[18]=z[18] + 15*z[13];
    z[29]=z[30]*z[6];
    z[18]=z[18]*z[29];
    z[18]=z[21] + z[18];
    z[21]= - z[24] - z[13];
    z[21]=z[5]*z[21];
    z[15]=z[15] - n<T>(11,8)*z[4] + z[21] + n<T>(1,8)*z[18];
    z[15]=z[2]*z[15];
    z[17]= - z[17] + z[13];
    z[17]=z[5]*z[17];
    z[17]= - z[4] + n<T>(1,8)*z[17];
    z[17]=z[5]*z[17];
    z[18]=n<T>(11,2)*z[4] + z[25];
    z[18]=z[5]*z[18];
    z[18]=z[32] + z[18];
    z[18]=z[5]*z[18];
    z[18]=z[28] + z[18];
    z[21]=n<T>(1,4)*z[8];
    z[18]=z[18]*z[21];
    z[24]= - n<T>(17,2)*z[4] - z[40];
    z[24]=z[24]*z[20];
    z[24]= - z[4] + z[24];
    z[24]=z[24]*z[29];
    z[25]=n<T>(5,32)*z[4];
    z[15]=z[15] + n<T>(1,4)*z[24] + z[18] - z[25] + z[17];
    z[15]=z[2]*z[15];
    z[17]=static_cast<T>(3)+ z[5];
    z[17]=z[5]*z[17];
    z[17]=n<T>(7,2) + z[17];
    z[17]=z[17]*z[20];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[5]*z[17];
    z[17]=n<T>(1,4) + z[17];
    z[17]=z[8]*z[17];
    z[17]=z[22] + z[17];
    z[17]=z[17]*z[39];
    z[13]=z[4] + n<T>(3,8)*z[13];
    z[13]=z[5]*z[13];
    z[13]=z[26] + z[13];
    z[13]=z[5]*z[13];
    z[13]=n<T>(21,32)*z[4] + z[13];
    z[13]=z[5]*z[13];
    z[13]=z[25] + z[13];
    z[13]=z[5]*z[13];
    z[18]=z[20] + 1;
    z[18]=z[18]*z[5];
    z[20]=static_cast<T>(1)+ z[18];
    z[20]=z[5]*z[20];
    z[20]=n<T>(5,8) + z[20];
    z[20]=z[20]*z[21];
    z[13]=z[17] + z[13] + z[20];
    z[13]=z[7]*z[13];
    z[17]=z[19] + z[33];
    z[17]=z[5]*z[17];
    z[17]=n<T>(13,8)*z[4] + z[17];
    z[17]=z[5]*z[17];
    z[19]=n<T>(1,2)*z[34];
    z[19]= - z[31]*z[19];
    z[17]=z[19] + n<T>(3,8)*z[4] + z[17];
    z[19]= - z[35] - z[23];
    z[19]=z[5]*z[19];
    z[19]= - z[27] + z[19];
    z[19]=z[5]*z[19];
    z[20]= - z[7]*z[22];
    z[19]=z[19] + z[20];
    z[19]=z[6]*z[19]*z[39];
    z[20]=z[34]*z[9];
    z[18]=n<T>(3,4) + z[18];
    z[18]=z[18]*z[20];
    z[13]=n<T>(1,8)*z[18] + z[19] + n<T>(1,4)*z[17] + z[13];
    z[13]=z[9]*z[13];
    z[17]=z[34]*z[6];
    z[18]=z[17]*z[11];
    z[19]=z[9]*z[8];
    z[19]= - z[18] + z[19];
    z[19]=z[36]*z[19];
    z[17]=npow(z[7],2)*z[17];
    z[17]=z[38] - n<T>(1,2)*z[17] + z[19];
    z[17]=z[11]*z[17];
    z[19]=z[9]*z[37];
    z[18]= - n<T>(1,2)*z[18] + z[34] + z[19];
    z[18]=z[11]*z[18];
    z[19]= - 3*z[34] + z[20];
    z[19]=z[9]*z[19];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[10]*z[18];
    z[17]=z[17] + z[18];

    r += z[13] + z[14] + z[15] + n<T>(1,2)*z[16] + n<T>(1,16)*z[17];
 
    return r;
}

template double qqb_2lha_r1023(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1023(const std::array<dd_real,30>&);
#endif
