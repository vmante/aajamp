#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2155(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[7];
    z[6]=k[11];
    z[7]=k[15];
    z[8]=npow(z[6],2);
    z[9]=z[8]*z[4];
    z[10]=n<T>(3,2)*z[7];
    z[11]=z[10] + z[5];
    z[11]=z[11]*z[9];
    z[11]=z[11] - z[7];
    z[12]=5*z[7] + z[5];
    z[13]= - n<T>(1,4)*z[5] + static_cast<T>(1)- n<T>(3,4)*z[7];
    z[13]=z[6]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[12]=z[6]*z[12];
    z[11]=z[12] + n<T>(1,2)*z[11];
    z[11]=z[4]*z[11];
    z[12]= - z[1] + z[2] - 1;
    z[11]=n<T>(1,4)*z[12] + z[11];
    z[13]=n<T>(1,8)*z[4];
    z[8]=z[13]*z[8];
    z[13]=z[5] + 3*z[7];
    z[14]=z[13]*z[8];
    z[10]= - n<T>(1,2)*z[5] + static_cast<T>(1)- z[10];
    z[10]=z[6]*z[10];
    z[10]=n<T>(1,2)*z[10] + z[7] + n<T>(1,8)*z[5];
    z[10]=z[6]*z[10];
    z[10]=z[10] + z[14];
    z[10]=z[4]*z[10];
    z[14]=static_cast<T>(1)+ z[2];
    z[10]=z[10] + n<T>(1,8)*z[14] - z[6];
    z[10]=z[4]*z[10];
    z[9]=z[13]*z[9];
    z[9]= - 3*z[6] - n<T>(1,4)*z[9];
    z[9]=z[4]*z[9];
    z[13]= - static_cast<T>(3)+ z[2];
    z[13]=z[2]*z[13];
    z[9]=z[13] + z[9];
    z[9]=z[4]*z[9];
    z[13]=z[12]*npow(z[2],2);
    z[9]=z[13] + z[9];
    z[9]=z[3]*z[9];
    z[12]=z[2]*z[12];
    z[9]=n<T>(1,2)*z[9] + n<T>(5,8)*z[12] + z[10];
    z[9]=z[3]*z[9];
    z[9]=n<T>(1,2)*z[11] + z[9];
    z[9]=z[3]*z[9];
    z[8]=z[5]*z[8];
    z[8]=z[8] + z[9];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r2155(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2155(const std::array<dd_real,30>&);
#endif
