#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r236(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[6];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=n<T>(1,2)*z[4];
    z[9]=npow(z[6],2);
    z[9]=static_cast<T>(1)- z[9];
    z[9]=z[9]*z[8];
    z[10]=n<T>(1,2)*z[6];
    z[11]=z[10] - 1;
    z[11]=z[11]*z[6];
    z[12]= - n<T>(1,2) - z[11];
    z[13]=z[7] + 1;
    z[12]=z[5]*z[13]*z[12];
    z[14]=z[6]*z[7];
    z[14]= - static_cast<T>(3)- z[14];
    z[14]=z[6]*z[14];
    z[9]=3*z[12] + z[9] + z[14] + static_cast<T>(3)+ z[7];
    z[12]=n<T>(1,4)*z[5];
    z[9]=z[9]*z[12];
    z[14]=n<T>(1,2)*z[7];
    z[15]=n<T>(1,3)*z[3];
    z[16]= - n<T>(5,8) + z[15];
    z[16]=z[3]*z[7]*z[16];
    z[16]=z[14] + z[16];
    z[16]=z[3]*z[16];
    z[17]=n<T>(1,4)*z[7];
    z[18]=z[17] + 1;
    z[19]=z[10] - z[18];
    z[19]=z[19]*z[10];
    z[20]=z[6] - n<T>(1,2);
    z[21]= - z[6]*z[20];
    z[22]=n<T>(1,4) - z[6];
    z[22]=z[4]*z[22];
    z[21]=z[22] - n<T>(3,2) + z[21];
    z[22]=n<T>(1,4)*z[4];
    z[21]=z[21]*z[22];
    z[23]=n<T>(5,3)*z[7];
    z[24]= - n<T>(3,2) - z[23];
    z[9]=z[9] + z[21] + z[19] + n<T>(1,8)*z[24] + z[16];
    z[9]=z[5]*z[9];
    z[16]=z[4] + 1;
    z[19]=z[6] - 1;
    z[21]= - z[5]*z[19]*z[16];
    z[24]= - n<T>(5,2) - z[6];
    z[25]= - n<T>(1,4) - z[6];
    z[25]=z[4]*z[25];
    z[25]= - static_cast<T>(3)+ z[25];
    z[25]=z[4]*z[25];
    z[21]=n<T>(3,2)*z[21] + n<T>(1,2)*z[24] + z[25];
    z[24]=n<T>(1,2)*z[5];
    z[21]=z[21]*z[24];
    z[25]=static_cast<T>(1)+ z[6];
    z[25]=n<T>(1,2)*z[25] + z[4];
    z[25]=z[25]*z[8];
    z[26]=n<T>(1,4)*z[6];
    z[27]=n<T>(5,4) - z[3];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[27]=z[3]*z[27];
    z[21]=z[21] + z[25] + z[27] - z[26];
    z[21]=z[21]*z[24];
    z[25]=npow(z[3],2);
    z[27]=n<T>(1,3)*z[2];
    z[28]= - static_cast<T>(1)+ z[27];
    z[28]=z[3]*z[28];
    z[28]=n<T>(1,12) + z[28];
    z[28]=z[28]*z[25];
    z[29]=z[2] - 1;
    z[30]=n<T>(1,2)*z[2];
    z[31]=z[30] - 1;
    z[32]= - z[2]*z[31];
    z[32]= - n<T>(1,2) + z[32];
    z[32]=z[3]*z[32];
    z[32]=z[32] + z[29];
    z[25]=n<T>(1,3)*z[25];
    z[32]=z[32]*z[25];
    z[32]=n<T>(1,4) + z[32];
    z[32]=z[4]*z[32];
    z[28]=z[28] + z[32];
    z[28]=z[28]*z[8];
    z[32]= - n<T>(5,8) + z[3];
    z[25]=z[32]*z[25];
    z[16]= - z[16]*z[24];
    z[16]=z[16] + n<T>(1,2) + z[4];
    z[12]=z[12]*z[16];
    z[12]=z[25] + z[12];
    z[12]=z[5]*z[4]*z[12];
    z[16]= - z[31] - z[24];
    z[16]=z[1]*z[16]*npow(z[4],2)*npow(z[3],3);
    z[12]=n<T>(1,6)*z[16] + z[28] + z[12];
    z[12]=z[1]*z[12];
    z[16]= - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[16]=z[16]*z[27];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[3]*z[16];
    z[25]=static_cast<T>(1)- 5*z[2];
    z[16]=n<T>(1,24)*z[25] + z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(1,24) + z[16];
    z[16]=z[3]*z[16];
    z[25]=z[2]*z[29]*z[3];
    z[25]=z[25] - z[30];
    z[25]=z[25]*z[15];
    z[28]= - n<T>(3,4)*z[6] + n<T>(3,8) + z[25];
    z[28]=z[28]*z[8];
    z[16]=z[28] - n<T>(1,8) + z[16];
    z[16]=z[4]*z[16];
    z[28]= - n<T>(1,2) - z[2];
    z[29]=static_cast<T>(1)- n<T>(1,6)*z[2];
    z[29]=z[3]*z[29];
    z[29]= - n<T>(1,6) + z[29];
    z[29]=z[3]*z[29];
    z[28]=n<T>(1,6)*z[28] + z[29];
    z[28]=z[3]*z[28];
    z[28]= - n<T>(1,8) + z[28];
    z[12]=z[12] + z[21] + n<T>(1,2)*z[28] + z[16];
    z[12]=z[1]*z[12];
    z[16]=z[14] + static_cast<T>(5)+ z[30];
    z[21]=n<T>(1,3)*z[7];
    z[28]= - z[21] - n<T>(1,2) + z[27];
    z[28]=z[3]*z[28];
    z[16]=n<T>(1,6)*z[16] + z[28];
    z[16]=z[3]*z[16];
    z[28]=n<T>(1,8)*z[2];
    z[16]=n<T>(1,2)*z[16] + n<T>(1,24)*z[7] - n<T>(1,3) - z[28];
    z[16]=z[3]*z[16];
    z[29]= - npow(z[2],2)*z[15];
    z[30]=z[6]*z[19];
    z[29]=n<T>(3,2)*z[30] + n<T>(1,4) + z[29];
    z[29]=z[4]*z[29];
    z[20]=z[29] - z[25] + z[20];
    z[20]=z[20]*z[22];
    z[22]=n<T>(1,8)*z[6];
    z[25]= - z[7] - n<T>(1,8) + z[2];
    z[9]=z[12] + z[9] + z[20] + z[22] + n<T>(1,6)*z[25] + z[16];
    z[9]=z[1]*z[9];
    z[12]= - n<T>(3,4) - z[21];
    z[12]=z[12]*z[14];
    z[16]= - z[26] + z[18];
    z[16]=z[6]*z[16];
    z[12]=z[16] - static_cast<T>(1)+ z[12];
    z[12]=z[6]*z[12];
    z[16]= - static_cast<T>(3)- z[17];
    z[16]=z[7]*z[16];
    z[17]=n<T>(1,2) + z[7];
    z[17]=z[6]*z[17];
    z[16]=z[17] - n<T>(5,4) + z[16];
    z[16]=z[6]*z[16];
    z[14]=static_cast<T>(3)+ z[14];
    z[14]=z[7]*z[14];
    z[14]=z[16] + static_cast<T>(1)+ z[14];
    z[14]=z[6]*z[14];
    z[16]=static_cast<T>(3)- z[6];
    z[16]=z[6]*z[16];
    z[16]= - static_cast<T>(3)+ z[16];
    z[13]=z[13]*z[7];
    z[16]=z[6]*z[13]*z[16];
    z[13]=z[13] + z[16];
    z[13]=z[13]*z[24];
    z[16]= - z[7]*z[18];
    z[13]=z[13] + z[14] - n<T>(1,4) + z[16];
    z[13]=z[13]*z[24];
    z[14]=n<T>(5,2) - z[3];
    z[14]=z[15]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[3]*z[14]*npow(z[7],2);
    z[16]=n<T>(1,2) + z[23];
    z[16]=z[7]*z[16];
    z[16]=static_cast<T>(1)+ z[16];
    z[14]=n<T>(1,2)*z[16] + z[14];
    z[11]=n<T>(3,4) + z[11];
    z[11]=z[6]*z[11];
    z[11]= - n<T>(1,4) + z[11];
    z[8]=z[11]*z[8];
    z[8]=z[13] + z[8] + n<T>(1,2)*z[14] + z[12];
    z[8]=z[8]*z[24];
    z[11]=n<T>(1,4) - z[27];
    z[11]= - z[22] + n<T>(1,2)*z[11] + z[21];
    z[10]=z[11]*z[10];
    z[11]=z[3] - n<T>(7,2);
    z[11]=z[7]*z[11];
    z[11]=z[2] + z[11];
    z[11]=z[11]*z[15];
    z[12]= - z[2] + 3*z[7];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[11]=z[3]*z[11];
    z[12]=n<T>(3,2) - z[6];
    z[12]=z[6]*z[12];
    z[12]= - n<T>(1,2) + z[12];
    z[12]=z[4]*z[12];
    z[12]= - z[19] + z[12];
    z[12]=z[4]*z[6]*z[12];

    r += z[8] + z[9] + z[10] + n<T>(1,4)*z[11] + n<T>(1,8)*z[12] - z[21] + 
      z[28];
 
    return r;
}

template double qqb_2lha_r236(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r236(const std::array<dd_real,30>&);
#endif
