#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1041(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[3];
    z[2]=k[13];
    z[3]=k[4];
    z[4]=k[11];
    z[5]=k[5];
    z[6]=k[9];
    z[7]=k[17];
    z[8]=z[3]*z[4];
    z[9]=z[8] + 1;
    z[10]=z[4] - 1;
    z[9]=z[7]*z[9]*z[10];
    z[11]=n<T>(1,2)*z[4];
    z[12]=static_cast<T>(1)- z[11];
    z[8]=z[12]*z[8];
    z[8]=3*z[8] - n<T>(3,2)*z[10];
    z[8]=z[2]*z[8];
    z[8]=z[9] + static_cast<T>(1)+ z[11] + z[8];
    z[10]=n<T>(1,2)*z[5];
    z[8]=z[10]*z[8];
    z[10]=z[3]*npow(z[2],2);
    z[12]=static_cast<T>(1)- z[2];
    z[13]=npow(z[4],2);
    z[12]=z[10]*z[13]*z[12];
    z[14]=z[2]*z[4];
    z[15]= - static_cast<T>(1)- n<T>(1,4)*z[4];
    z[15]=z[15]*z[14];
    z[15]=n<T>(1,4)*z[13] + z[15];
    z[15]=z[2]*z[15];
    z[12]=z[15] + n<T>(1,2)*z[12];
    z[12]=z[3]*z[12];
    z[15]=n<T>(1,2)*z[13];
    z[16]= - static_cast<T>(3)+ z[11];
    z[16]=z[2]*z[16];
    z[16]= - z[15] + z[16];
    z[16]=n<T>(3,4)*z[5] + n<T>(1,2)*z[16];
    z[16]=z[2]*z[16];
    z[14]=z[15] - z[14];
    z[10]=z[14]*z[10];
    z[14]=n<T>(1,2)*z[1];
    z[15]= - npow(z[2],3)*z[14];
    z[10]=z[15] + z[10] + z[16];
    z[10]=z[1]*z[10];
    z[14]=z[14] + z[3];
    z[14]=z[13]*z[14];
    z[9]= - z[9] + z[11] + z[14];
    z[9]=z[6]*z[9];
    z[11]= - static_cast<T>(5)- z[4];
    z[11]=z[2]*z[11];
    z[11]=z[13] + z[11];
    z[8]=n<T>(1,2)*z[9] + z[10] + n<T>(1,4)*z[11] + z[12] + z[8];

    r += n<T>(1,2)*z[8];
 
    return r;
}

template double qqb_2lha_r1041(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1041(const std::array<dd_real,30>&);
#endif
