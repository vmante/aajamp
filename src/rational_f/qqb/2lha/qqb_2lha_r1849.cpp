#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1849(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[13];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[10];
    z[7]=z[4] + 2;
    z[8]=3*z[4];
    z[9]= - z[7]*z[8];
    z[10]=z[5] + 3;
    z[11]=z[10]*z[5];
    z[11]=z[11] + 3;
    z[11]=z[11]*z[5];
    z[11]=z[11] + 1;
    z[12]=z[11]*z[2];
    z[13]=static_cast<T>(12)+ z[5];
    z[13]=z[5]*z[13];
    z[13]=static_cast<T>(21)+ z[13];
    z[13]=z[5]*z[13];
    z[13]= - 2*z[12] + static_cast<T>(10)+ z[13];
    z[13]=z[2]*z[13];
    z[14]= - 5*z[5] - static_cast<T>(15)- 7*z[4];
    z[14]=z[5]*z[14];
    z[9]=z[13] + z[14] - static_cast<T>(8)+ z[9];
    z[13]=2*z[5];
    z[14]= - 6*z[5] - static_cast<T>(7)- 5*z[4];
    z[14]=z[14]*z[13];
    z[15]=z[5] + 4;
    z[15]=z[15]*z[5];
    z[15]=z[15] + 6;
    z[15]=z[15]*z[5];
    z[15]=z[15] + 4;
    z[15]=z[15]*z[5];
    z[15]=z[15] + 1;
    z[15]=z[15]*z[2];
    z[16]=6*z[11] - z[15];
    z[16]=z[2]*z[16];
    z[17]= - static_cast<T>(4)- z[8];
    z[17]=z[4]*z[17];
    z[14]=z[16] + z[14] - static_cast<T>(5)+ z[17];
    z[14]=z[6]*z[14];
    z[9]=2*z[9] + z[14];
    z[9]=z[6]*z[9];
    z[14]=z[1] + 3;
    z[16]=z[1]*z[14];
    z[17]=z[5] + static_cast<T>(7)+ z[1];
    z[17]=z[5]*z[17];
    z[16]=z[17] + static_cast<T>(6)+ z[16];
    z[17]=3*z[5];
    z[14]= - 2*z[14] - z[17];
    z[14]=z[5]*z[14];
    z[18]=z[1] + 2;
    z[19]= - z[1]*z[18];
    z[14]=z[14] - static_cast<T>(3)+ z[19];
    z[14]=z[2]*z[14];
    z[14]=2*z[16] + z[14];
    z[14]=z[2]*z[14];
    z[16]= - static_cast<T>(3)- 2*z[4];
    z[7]= - 2*z[7] - z[1];
    z[7]=z[1]*z[7];
    z[7]=z[9] + z[14] - 8*z[5] + 3*z[16] + z[7];
    z[7]=z[3]*z[7];
    z[9]= - 5*z[11] + z[15];
    z[9]=z[2]*z[9];
    z[11]=7*z[5] + static_cast<T>(1)+ z[8];
    z[11]=z[5]*z[11];
    z[8]=z[9] - z[8] + z[11];
    z[8]=z[6]*z[8];
    z[9]= - static_cast<T>(2)- z[5];
    z[9]=z[5]*z[9];
    z[9]= - static_cast<T>(1)+ z[9];
    z[9]=4*z[9] + z[12];
    z[9]=z[2]*z[9];
    z[8]=z[8] + 3*z[9] - 8*z[4] - z[10];
    z[8]=z[6]*z[8];
    z[9]=z[13] + z[18];
    z[9]=z[5]*z[9];
    z[9]= - z[1] + z[9];
    z[9]=z[2]*z[9];
    z[10]=z[1] - z[13];
    z[9]=2*z[10] + z[9];
    z[9]=z[2]*z[9];
    z[10]= - static_cast<T>(3)- z[4];
    z[7]=z[7] + z[8] + z[9] + 2*z[10] - 3*z[1];
    z[7]=z[3]*z[7];
    z[8]=z[5]*npow(z[2],2);
    z[9]=z[6]*z[17];
    z[9]= - static_cast<T>(2)+ z[9];
    z[9]=z[6]*z[9];
    z[7]=z[7] + z[9] - static_cast<T>(2)+ z[8];

    r += 2*z[7]*z[3];
 
    return r;
}

template double qqb_2lha_r1849(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1849(const std::array<dd_real,30>&);
#endif
