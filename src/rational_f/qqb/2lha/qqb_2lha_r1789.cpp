#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1789(const std::array<T,30>& k) {
  T z[6];
  T r = 0;

    z[1]=k[4];
    z[2]=k[7];
    z[3]=k[10];
    z[4]=npow(z[3],2);
    z[5]=z[1]*npow(z[3],3);
    z[4]= - z[4] - n<T>(2,3)*z[5];
    z[4]=z[1]*z[4];
    z[4]= - z[3] + z[4];
    z[4]=z[1]*z[4];
    z[4]= - n<T>(2,3) + z[4];

    r += n<T>(1,9)*z[4]*z[2];
 
    return r;
}

template double qqb_2lha_r1789(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1789(const std::array<dd_real,30>&);
#endif
