#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2326(const std::array<T,30>& k) {
  T z[57];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[20];
    z[6]=k[6];
    z[7]=k[17];
    z[8]=k[3];
    z[9]=n<T>(3,2)*z[1];
    z[10]=z[9] + 1;
    z[11]=n<T>(1,2)*z[3];
    z[12]=npow(z[1],2);
    z[13]= - z[10]*z[12]*z[11];
    z[14]=z[1] + n<T>(1,2);
    z[15]=z[14]*z[1];
    z[16]= - n<T>(3,4) + z[15];
    z[16]=z[1]*z[16];
    z[16]= - n<T>(1,4) + z[16];
    z[16]=z[1]*z[16];
    z[13]=z[16] + z[13];
    z[13]=z[13]*z[11];
    z[16]=z[12] + n<T>(1,2);
    z[17]=z[12]*z[3];
    z[18]= - z[16]*z[17];
    z[19]= - static_cast<T>(1)+ 5*z[12];
    z[19]=z[1]*z[19];
    z[18]=z[19] + z[18];
    z[18]=z[18]*z[11];
    z[19]=z[1] - n<T>(1,2);
    z[20]=z[19]*z[12];
    z[21]= - z[11]*z[20];
    z[22]=z[1] - n<T>(1,4);
    z[22]=z[22]*z[1];
    z[21]=z[22] + z[21];
    z[21]=z[2]*z[3]*z[21];
    z[18]=z[21] + z[18] + static_cast<T>(1)- z[22];
    z[21]=n<T>(1,2)*z[2];
    z[18]=z[18]*z[21];
    z[23]=z[1] + n<T>(3,8);
    z[23]=z[23]*z[1];
    z[24]= - n<T>(3,4) - z[23];
    z[24]=z[1]*z[24];
    z[13]=z[18] + z[13] - n<T>(3,8) + z[24];
    z[13]=z[2]*z[13];
    z[18]=n<T>(5,4)*z[1];
    z[24]= - static_cast<T>(1)- z[18];
    z[24]=z[24]*z[12];
    z[25]=npow(z[1],3);
    z[26]=z[25]*z[3];
    z[27]=z[1] + 1;
    z[28]=z[27]*z[26];
    z[24]=z[24] + n<T>(1,4)*z[28];
    z[24]=z[3]*z[24];
    z[28]=z[25]*z[11];
    z[29]= - z[12] + z[28];
    z[30]=z[11]*z[2];
    z[29]=z[29]*z[30];
    z[16]=z[29] + n<T>(1,2)*z[16] + z[24];
    z[16]=z[2]*z[16];
    z[24]=n<T>(1,2)*z[1];
    z[29]=z[24] + 1;
    z[31]=z[29]*z[1];
    z[32]=z[31] + n<T>(1,2);
    z[33]= - z[32]*z[17];
    z[34]=z[27]*z[1];
    z[35]= - n<T>(1,2) + z[34];
    z[35]=z[1]*z[35];
    z[16]=z[16] + z[33] - n<T>(1,4) + z[35];
    z[16]=z[2]*z[16];
    z[33]=n<T>(1,4)*z[1];
    z[35]=z[1] + 3;
    z[36]=z[35]*z[1];
    z[37]=static_cast<T>(5)+ z[36];
    z[37]=z[1]*z[37];
    z[37]=static_cast<T>(3)+ z[37];
    z[37]=z[37]*z[33];
    z[16]=z[37] + z[16];
    z[37]=z[1] + n<T>(5,2);
    z[38]=z[21] - z[37];
    z[38]=z[2]*z[38];
    z[39]=n<T>(1,2)*z[8];
    z[40]=z[39] + z[2];
    z[41]= - n<T>(3,2)*z[27] + z[40];
    z[41]=z[8]*z[41];
    z[38]=z[38] + z[41];
    z[38]=n<T>(3,8) + z[31] + n<T>(1,4)*z[38];
    z[38]=z[8]*z[38];
    z[41]=z[21] - 1;
    z[42]=z[24] - 1;
    z[43]=z[42]*z[1];
    z[44]= - z[43] - z[41];
    z[44]=z[44]*z[21];
    z[45]= - static_cast<T>(1)- n<T>(3,8)*z[1];
    z[45]=z[1]*z[45];
    z[45]= - static_cast<T>(1)+ z[45];
    z[45]=z[1]*z[45];
    z[38]=z[38] + z[44] - n<T>(1,8) + z[45];
    z[38]=z[8]*z[38];
    z[16]=n<T>(1,2)*z[16] + z[38];
    z[16]=z[4]*z[16];
    z[38]= - z[42]*z[12];
    z[42]=z[1] - 3;
    z[28]= - z[42]*z[28];
    z[28]=z[38] + z[28];
    z[28]=z[3]*z[28];
    z[38]=z[37]*z[1];
    z[44]=n<T>(3,2) - z[38];
    z[44]=z[1]*z[44];
    z[44]=n<T>(3,2) + z[44];
    z[44]=z[1]*z[44];
    z[28]=z[44] + z[28];
    z[44]=3*z[1];
    z[45]= - n<T>(1,2) - z[44];
    z[45]=z[45]*z[24];
    z[46]=z[1] - 1;
    z[47]=z[46]*z[39];
    z[45]=z[47] - n<T>(3,2)*z[2] + static_cast<T>(1)+ z[45];
    z[45]=z[45]*z[39];
    z[47]=z[1] + n<T>(5,4);
    z[47]=z[47]*z[1];
    z[48]= - z[2] + n<T>(9,4) + z[47];
    z[48]=z[48]*z[21];
    z[49]=static_cast<T>(1)+ n<T>(3,4)*z[1];
    z[49]=z[1]*z[49];
    z[49]= - n<T>(5,8) + z[49];
    z[49]=z[1]*z[49];
    z[45]=z[45] + z[48] - n<T>(1,4) + z[49];
    z[45]=z[8]*z[45];
    z[48]= - n<T>(5,4) + z[1];
    z[48]=z[48]*z[26];
    z[48]=z[20] + z[48];
    z[48]=z[3]*z[48];
    z[49]=n<T>(5,2)*z[1];
    z[50]=static_cast<T>(1)- z[49];
    z[50]=z[50]*z[26];
    z[50]= - z[25] + z[50];
    z[50]=z[3]*z[50];
    z[51]=z[6]*npow(z[3],2)*npow(z[1],4);
    z[50]=z[50] + z[51];
    z[52]=n<T>(1,2)*z[6];
    z[50]=z[50]*z[52];
    z[48]=z[48] + z[50];
    z[48]=z[48]*z[52];
    z[13]=z[16] + z[48] + z[45] + n<T>(1,4)*z[28] + z[13];
    z[13]=z[4]*z[13];
    z[16]=2*z[1];
    z[28]= - static_cast<T>(3)- z[16];
    z[28]=z[28]*z[44];
    z[45]=z[27]*z[44];
    z[48]= - static_cast<T>(1)- z[33];
    z[48]=z[1]*z[48];
    z[48]= - n<T>(3,2) + z[48];
    z[48]=z[1]*z[48];
    z[48]= - static_cast<T>(1)+ z[48];
    z[48]=z[1]*z[48];
    z[48]= - n<T>(1,4) + z[48];
    z[48]=z[5]*z[48];
    z[45]=z[48] + static_cast<T>(1)+ z[45];
    z[45]=z[3]*z[45];
    z[48]=static_cast<T>(3)+ z[36];
    z[48]=z[1]*z[48];
    z[48]=static_cast<T>(1)+ z[48];
    z[48]=z[5]*z[48];
    z[28]=z[45] + z[48] - static_cast<T>(4)+ z[28];
    z[28]=z[3]*z[28];
    z[45]=z[5]*z[14];
    z[48]=n<T>(1,4)*z[5];
    z[50]=static_cast<T>(1)- z[48];
    z[50]=z[2]*z[50];
    z[45]=z[50] + z[45] - static_cast<T>(2)- z[44];
    z[45]=z[2]*z[45];
    z[50]= - static_cast<T>(2)+ z[11];
    z[50]=z[3]*z[50];
    z[41]=z[2]*z[41];
    z[41]=z[41] + n<T>(3,2) + z[50];
    z[41]=z[8]*z[41];
    z[50]= - n<T>(5,4) - z[16];
    z[50]=z[3]*z[50];
    z[50]=z[50] + static_cast<T>(5)+ 6*z[1];
    z[50]=z[3]*z[50];
    z[16]= - n<T>(5,4)*z[2] + n<T>(5,2) + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[41] + z[16] + z[50] - n<T>(15,4) - 4*z[1];
    z[16]=z[8]*z[16];
    z[41]=static_cast<T>(2)+ z[1];
    z[41]=z[1]*z[41];
    z[41]=static_cast<T>(1)+ z[41];
    z[50]= - static_cast<T>(2)- z[9];
    z[50]=z[1]*z[50];
    z[50]= - n<T>(3,4) + z[50];
    z[50]=z[5]*z[50];
    z[16]=z[16] + z[45] + z[28] + 3*z[41] + z[50];
    z[16]=z[6]*z[16];
    z[28]=z[1] - n<T>(3,2);
    z[41]= - z[28]*z[24];
    z[41]=static_cast<T>(3)+ z[41];
    z[41]=z[1]*z[41];
    z[41]=n<T>(7,4) + z[41];
    z[41]=z[5]*z[41];
    z[45]=n<T>(9,2)*z[1];
    z[50]= - static_cast<T>(5)- z[45];
    z[50]=z[1]*z[50];
    z[52]=n<T>(3,2) + z[38];
    z[52]=z[1]*z[52];
    z[52]= - n<T>(1,2) + z[52];
    z[52]=z[1]*z[52];
    z[52]= - n<T>(1,2) + z[52];
    z[52]=z[5]*z[52];
    z[50]=z[52] - n<T>(5,4) + z[50];
    z[50]=z[50]*z[11];
    z[18]=z[18] + 2;
    z[18]=z[18]*z[1];
    z[18]=z[18] + n<T>(1,4);
    z[41]=z[50] + z[41] + z[18];
    z[41]=z[3]*z[41];
    z[50]=n<T>(1,2)*z[35];
    z[52]=z[3]*z[27];
    z[52]= - z[50] + z[52];
    z[52]=z[3]*z[52];
    z[52]= - n<T>(9,2)*z[27] + z[52];
    z[53]= - z[2] + n<T>(11,4) + z[1];
    z[53]=z[2]*z[53];
    z[54]= - static_cast<T>(1)+ z[11];
    z[54]=z[3]*z[54];
    z[54]=n<T>(9,2) + z[54];
    z[54]=n<T>(1,4)*z[54] - z[2];
    z[54]=z[8]*z[54];
    z[52]=z[54] + n<T>(1,2)*z[52] + z[53];
    z[52]=z[8]*z[52];
    z[53]= - z[37]*z[9];
    z[53]= - static_cast<T>(2)+ z[53];
    z[53]=z[5]*z[53];
    z[54]=n<T>(1,2)*z[5];
    z[55]=n<T>(7,2) + 5*z[1];
    z[55]=z[55]*z[54];
    z[56]=n<T>(7,4) - z[5];
    z[56]=z[2]*z[56];
    z[55]=z[56] + z[55] - static_cast<T>(3)- n<T>(13,4)*z[1];
    z[55]=z[2]*z[55];
    z[56]=static_cast<T>(7)+ n<T>(9,4)*z[1];
    z[56]=z[1]*z[56];
    z[56]=n<T>(19,4) + z[56];
    z[16]=z[16] + z[52] + z[55] + z[41] + n<T>(1,2)*z[56] + z[53];
    z[16]=z[6]*z[16];
    z[41]=z[46]*z[24];
    z[52]=static_cast<T>(1)- z[41];
    z[52]=z[1]*z[52];
    z[52]= - static_cast<T>(3)+ z[52];
    z[52]=z[52]*z[24];
    z[50]= - z[12]*z[50];
    z[50]=static_cast<T>(1)+ z[50];
    z[50]=z[5]*z[1]*z[50];
    z[50]=z[52] + z[50];
    z[52]=3*z[5];
    z[50]=z[50]*z[52];
    z[53]= - n<T>(3,2) - z[12];
    z[53]=z[53]*z[24];
    z[53]=static_cast<T>(1)+ z[53];
    z[53]=z[1]*z[53];
    z[50]=z[53] + z[50];
    z[50]=z[50]*z[11];
    z[18]=z[1]*z[18];
    z[18]=n<T>(1,4) + z[18];
    z[18]=z[18]*z[52];
    z[53]= - static_cast<T>(1)+ n<T>(15,8)*z[1];
    z[53]=z[1]*z[53];
    z[53]= - n<T>(5,8) + z[53];
    z[53]=z[1]*z[53];
    z[18]=z[18] - n<T>(9,8) + z[53];
    z[18]=z[5]*z[18];
    z[53]=static_cast<T>(1)+ 5*z[25];
    z[18]=z[50] + n<T>(1,4)*z[53] + z[18];
    z[18]=z[3]*z[18];
    z[50]=z[27]*z[5];
    z[46]= - n<T>(1,2)*z[46] - z[50];
    z[46]=z[52]*z[12]*z[46];
    z[20]= - z[20] + z[46];
    z[20]=z[20]*z[11];
    z[46]=z[19]*z[24];
    z[53]=z[5]*z[15];
    z[46]=z[46] + z[53];
    z[46]=z[46]*z[52];
    z[20]=z[20] + z[22] + z[46];
    z[20]=z[30]*z[20];
    z[18]=z[20] + z[18];
    z[18]=z[5]*z[18];
    z[20]= - z[19]*z[44];
    z[20]=n<T>(1,2) + z[20];
    z[30]= - n<T>(1,2) - z[15];
    z[30]=z[30]*z[52];
    z[20]=n<T>(1,2)*z[20] + z[30];
    z[20]=z[5]*z[20];
    z[20]=z[20] - n<T>(5,2) - z[22];
    z[20]=z[20]*z[54];
    z[18]=z[20] + z[18];
    z[18]=z[2]*z[18];
    z[20]= - n<T>(1,4) - z[44];
    z[20]=z[20]*z[24];
    z[20]= - static_cast<T>(2)+ z[20];
    z[20]=z[1]*z[20];
    z[22]=static_cast<T>(1)- z[47];
    z[22]=z[1]*z[22];
    z[22]=n<T>(1,4) + z[22];
    z[22]=z[22]*z[52];
    z[20]=z[22] + n<T>(5,8) + z[20];
    z[20]=z[5]*z[20];
    z[22]=n<T>(9,4) - z[23];
    z[22]=z[1]*z[22];
    z[20]=z[20] + n<T>(19,8) + z[22];
    z[20]=z[5]*z[20];
    z[22]=static_cast<T>(3)- z[41];
    z[22]=z[1]*z[22];
    z[23]= - static_cast<T>(7)- z[1];
    z[23]=z[1]*z[23];
    z[23]= - n<T>(13,2) + z[23];
    z[23]=z[1]*z[23];
    z[23]=n<T>(5,2) + z[23];
    z[30]=n<T>(3,2) + z[36];
    z[30]=z[1]*z[30];
    z[30]= - n<T>(1,2) + z[30];
    z[30]=z[5]*z[30];
    z[23]=n<T>(1,2)*z[23] + z[30];
    z[23]=z[5]*z[23];
    z[22]=n<T>(3,2)*z[23] - n<T>(11,8) + z[22];
    z[22]=z[5]*z[22];
    z[23]=n<T>(1,2) + z[43];
    z[22]=n<T>(1,4)*z[23] + z[22];
    z[22]=z[3]*z[22];
    z[23]=n<T>(1,2) + z[36];
    z[23]=z[1]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[1]*z[23];
    z[23]=n<T>(7,2) + z[23];
    z[23]=z[23]*z[52];
    z[30]= - static_cast<T>(1)+ z[44];
    z[30]=z[1]*z[30];
    z[30]= - n<T>(3,2) + z[30];
    z[30]=z[1]*z[30];
    z[30]=n<T>(1,2) + z[30];
    z[30]=z[1]*z[30];
    z[30]= - n<T>(85,2) + z[30];
    z[23]=n<T>(1,2)*z[30] + z[23];
    z[23]=z[5]*z[23];
    z[15]=n<T>(3,4) + z[15];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(9,4) + z[15];
    z[15]=z[1]*z[15];
    z[15]=z[23] + n<T>(47,4) + z[15];
    z[15]=z[5]*z[15];
    z[15]= - n<T>(1,4)*z[35] + z[15];
    z[15]=n<T>(1,2)*z[15] + z[22];
    z[15]=z[3]*z[15];
    z[15]=z[18] + z[20] + z[15];
    z[15]=z[2]*z[15];
    z[18]=static_cast<T>(11)+ z[45];
    z[18]=z[18]*z[24];
    z[18]= - static_cast<T>(3)+ z[18];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(35,4) + z[18];
    z[10]=z[10]*z[24];
    z[10]=static_cast<T>(2)+ z[10];
    z[10]=z[1]*z[10];
    z[10]=n<T>(7,4) + z[10];
    z[10]=z[10]*z[52];
    z[10]=n<T>(1,2)*z[18] + z[10];
    z[10]=z[5]*z[10];
    z[18]=z[1]*z[19];
    z[18]=n<T>(1,2) + z[18];
    z[19]=z[1]*z[28];
    z[19]= - n<T>(5,2) + z[19];
    z[19]=z[5]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[18]*z[52];
    z[19]= - n<T>(7,4) + z[1];
    z[19]=z[1]*z[19];
    z[20]=z[52] - n<T>(1,2);
    z[22]=z[5]*z[20];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=z[2]*z[22];
    z[18]=z[22] + z[18] - n<T>(17,4) + z[19];
    z[18]=z[21]*z[18];
    z[19]=static_cast<T>(1)+ z[31];
    z[19]=z[19]*z[9];
    z[10]=z[18] + z[10] + static_cast<T>(2)+ z[19];
    z[10]=z[5]*z[10];
    z[18]=z[37]*z[52];
    z[14]= - n<T>(1,2)*z[14] + z[18];
    z[14]=z[5]*z[14];
    z[14]=n<T>(3,2) + z[14];
    z[14]=z[5]*z[14];
    z[18]=z[20]*npow(z[5],2);
    z[19]= - z[21]*z[18];
    z[14]=z[14] + z[19];
    z[14]=z[14]*z[21];
    z[18]= - z[18]*z[40];
    z[19]=n<T>(1,2) + 9*z[5];
    z[19]=z[19]*z[50];
    z[19]=z[19] + z[27];
    z[19]=z[19]*z[54];
    z[18]=z[19] + z[18];
    z[18]=z[18]*z[39];
    z[19]= - n<T>(5,4) - z[47];
    z[19]=z[19]*z[52];
    z[20]= - n<T>(13,8) - z[1];
    z[20]=z[1]*z[20];
    z[19]=z[19] + n<T>(5,8) + z[20];
    z[19]=z[5]*z[19];
    z[20]= - n<T>(3,2) - z[1];
    z[20]=z[1]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[19]=n<T>(3,4)*z[20] + z[19];
    z[19]=z[5]*z[19];
    z[14]=z[18] + z[14] - n<T>(1,4) + z[19];
    z[14]=z[8]*z[14];
    z[18]=n<T>(1,4) + z[1];
    z[19]=z[3]*z[1];
    z[20]=n<T>(1,2) + z[19];
    z[20]=z[3]*z[20];
    z[10]=z[14] + n<T>(1,4)*z[20] + n<T>(1,2)*z[18] + z[10];
    z[10]=z[8]*z[10];
    z[14]= - z[27]*z[24];
    z[18]=z[32]*z[19];
    z[14]=z[14] + z[18];
    z[14]=z[2]*z[14];
    z[18]=static_cast<T>(3)- z[34];
    z[18]=z[1]*z[18];
    z[18]=static_cast<T>(3)+ z[18];
    z[18]=z[3]*z[18]*z[24];
    z[19]= - static_cast<T>(1)+ z[31];
    z[19]=z[1]*z[19];
    z[14]=z[14] + z[19] + z[18];
    z[14]=z[3]*z[14];
    z[18]= - z[42]*z[12];
    z[19]= - static_cast<T>(3)- z[49];
    z[17]=z[19]*z[17];
    z[17]=z[18] + z[17];
    z[17]=z[3]*z[17];
    z[18]=z[29]*z[26];
    z[18]= - z[25] + z[18];
    z[18]=z[3]*z[18];
    z[18]=3*z[18] - z[51];
    z[18]=z[6]*z[18];
    z[17]=z[18] + z[12] + z[17];
    z[17]=z[6]*z[17];
    z[14]=z[17] + z[41] + z[14];
    z[14]=z[7]*z[14];
    z[14]=z[12] - z[14];
    z[17]= - static_cast<T>(3)- z[38];
    z[17]=z[17]*z[24];
    z[17]= - static_cast<T>(7)+ z[17];
    z[17]=z[17]*z[24];
    z[18]= - static_cast<T>(3)- z[12];
    z[18]=z[1]*z[18];
    z[18]= - static_cast<T>(11)+ z[18];
    z[18]=z[1]*z[18];
    z[18]= - static_cast<T>(9)+ z[18];
    z[18]=z[18]*z[52];
    z[9]= - static_cast<T>(5)- z[9];
    z[9]=z[1]*z[9];
    z[9]=n<T>(7,2) + z[9];
    z[9]=z[1]*z[9];
    z[9]=n<T>(63,2) + z[9];
    z[9]=z[1]*z[9];
    z[9]=z[18] + n<T>(89,2) + z[9];
    z[9]=z[9]*z[48];
    z[9]=z[9] - static_cast<T>(7)+ z[17];
    z[9]=z[5]*z[9];
    z[17]=static_cast<T>(13)+ z[44];
    z[17]=z[17]*z[33];
    z[18]= - z[35]*z[24];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[5]*z[18];
    z[17]=z[18] + static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[52];
    z[18]=static_cast<T>(1)- z[34];
    z[18]=z[18]*z[24];
    z[18]= - static_cast<T>(5)+ z[18];
    z[18]=z[1]*z[18];
    z[17]=z[17] - n<T>(37,4) + z[18];
    z[17]=z[5]*z[17];
    z[18]= - z[42]*z[24];
    z[18]=static_cast<T>(7)+ z[18];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[11]=z[17]*z[11];
    z[17]=n<T>(7,2) + z[1];
    z[17]=z[1]*z[17];
    z[17]=n<T>(11,4) + z[17];
    z[17]=z[17]*z[52];
    z[18]= - n<T>(75,2) - 7*z[1];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(167,4) + z[18];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[5]*z[17];
    z[12]=static_cast<T>(35)+ z[12];
    z[12]=z[1]*z[12];
    z[12]=static_cast<T>(73)+ z[12];
    z[12]=n<T>(1,4)*z[12] + z[17];
    z[12]=z[5]*z[12];
    z[17]= - static_cast<T>(37)- z[34];
    z[11]=z[11] + n<T>(1,8)*z[17] + z[12];
    z[11]=z[3]*z[11];

    r += z[9] + z[10] + z[11] + z[13] - n<T>(1,4)*z[14] + z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r2326(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2326(const std::array<dd_real,30>&);
#endif
