#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1666(const std::array<T,30>& k) {
  T z[7];
  T r = 0;

    z[1]=k[2];
    z[2]=k[8];
    z[3]=k[11];
    z[4]=k[3];
    z[5]=z[1] - 1;
    z[6]= - z[4] - z[5];
    z[6]=z[3]*z[1]*z[6];
    z[5]=z[6] + z[5];
    z[5]=z[2]*z[5];
    z[6]=z[1] + z[4];
    z[6]=z[3]*z[6];
    z[5]=z[5] - n<T>(3,2) + z[6];

    r += n<T>(3,8)*z[5]*z[3]*z[2];
 
    return r;
}

template double qqb_2lha_r1666(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1666(const std::array<dd_real,30>&);
#endif
