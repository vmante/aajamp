#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2381(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[7];
    z[6]=k[2];
    z[7]=k[4];
    z[8]=k[9];
    z[9]=n<T>(1,2)*z[7];
    z[10]=3*z[7];
    z[11]= - static_cast<T>(1)- z[10];
    z[11]=z[11]*z[9];
    z[12]=n<T>(3,2)*z[7];
    z[13]=3*z[1];
    z[14]=z[13] + static_cast<T>(1)- z[12];
    z[14]=z[1]*z[14];
    z[11]=z[11] + z[14];
    z[14]=z[7] - 1;
    z[15]=n<T>(1,4)*z[2];
    z[16]=z[15] + n<T>(1,4)*z[14] - z[1];
    z[16]=z[2]*z[16];
    z[11]=n<T>(1,2)*z[11] + z[16];
    z[11]=z[2]*z[11];
    z[16]=npow(z[7],2);
    z[17]=n<T>(1,4)*z[16];
    z[18]=static_cast<T>(3)- 5*z[7];
    z[18]=z[18]*z[17];
    z[19]=n<T>(3,4)*z[7];
    z[20]=z[19] - z[1];
    z[20]=z[1]*z[20];
    z[20]=n<T>(3,2)*z[16] + z[20];
    z[20]=z[1]*z[20];
    z[11]=z[11] + z[18] + z[20];
    z[11]=z[2]*z[11];
    z[18]= - z[14]*z[10];
    z[18]=static_cast<T>(1)+ z[18];
    z[20]=z[9] + 1;
    z[21]=n<T>(1,2)*z[1];
    z[22]=z[21] - z[20];
    z[22]=z[1]*z[22];
    z[18]=n<T>(1,2)*z[18] + z[22];
    z[18]=z[1]*z[18];
    z[22]= - static_cast<T>(3)+ n<T>(5,2)*z[7];
    z[22]=z[22]*z[16];
    z[18]=z[22] + z[18];
    z[18]=z[1]*z[18];
    z[22]=npow(z[7],3);
    z[23]=n<T>(5,2) - z[7];
    z[23]=z[23]*z[22];
    z[18]=z[23] + z[18];
    z[11]=n<T>(1,2)*z[18] + z[11];
    z[11]=z[2]*z[11];
    z[18]=z[7] + 1;
    z[23]=z[18] - z[21];
    z[23]=z[23]*z[1];
    z[24]=z[20]*z[7];
    z[25]=z[24] + n<T>(1,2);
    z[26]=z[23] - z[25];
    z[27]=n<T>(1,2)*z[4];
    z[28]=npow(z[1],2);
    z[26]=z[26]*z[28]*z[27];
    z[29]=z[12] + 1;
    z[30]=z[7]*z[29];
    z[30]=z[30] - z[23] + n<T>(1,2);
    z[30]=z[30]*z[21];
    z[22]= - z[22] + z[30];
    z[22]=z[1]*z[22];
    z[30]=npow(z[7],4);
    z[11]=z[26] + z[11] + n<T>(1,2)*z[30] + z[22];
    z[22]=n<T>(1,2)*z[3];
    z[11]=z[11]*z[22];
    z[26]=n<T>(1,2)*z[16];
    z[14]=z[14]*z[26];
    z[30]= - n<T>(1,4) - z[7];
    z[30]=z[30]*z[10];
    z[31]=n<T>(3,2) + 7*z[7] - 5*z[1];
    z[31]=z[1]*z[31];
    z[30]=z[31] - n<T>(1,2) + z[30];
    z[30]=z[1]*z[30];
    z[14]=z[14] + z[30];
    z[30]=15*z[1] + static_cast<T>(1)- 13*z[7];
    z[30]=z[30]*z[21];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[7]*z[19];
    z[19]=z[19] + z[30];
    z[30]=n<T>(5,2)*z[1];
    z[31]=n<T>(5,8)*z[2] - z[30] - n<T>(1,4) + z[7];
    z[31]=z[2]*z[31];
    z[19]=n<T>(1,2)*z[19] + z[31];
    z[19]=z[2]*z[19];
    z[14]=n<T>(1,2)*z[14] + z[19];
    z[14]=z[2]*z[14];
    z[19]=n<T>(11,8)*z[7];
    z[31]=z[21] - 1;
    z[32]=z[19] - z[31];
    z[32]=z[1]*z[32];
    z[19]= - static_cast<T>(2)- z[19];
    z[19]=z[7]*z[19];
    z[19]=z[32] - n<T>(3,4) + z[19];
    z[19]=z[1]*z[19];
    z[32]=z[18]*z[7];
    z[33]=static_cast<T>(1)+ n<T>(7,2)*z[32];
    z[19]=n<T>(1,4)*z[33] + z[19];
    z[19]=z[1]*z[19];
    z[28]=z[4]*z[16]*z[28];
    z[19]=z[19] - n<T>(1,8)*z[28];
    z[19]=z[4]*z[19];
    z[28]=z[32] + n<T>(1,2);
    z[33]=z[28]*z[26];
    z[34]= - 5*z[23] + n<T>(7,2) + 9*z[24];
    z[34]=z[1]*z[34];
    z[35]= - n<T>(7,8) - z[7];
    z[35]=z[7]*z[35];
    z[35]= - n<T>(7,8) + z[35];
    z[35]=z[7]*z[35];
    z[34]=n<T>(1,4)*z[34] - n<T>(1,4) + z[35];
    z[34]=z[1]*z[34];
    z[11]=z[11] + z[19] + z[14] + z[33] + z[34];
    z[11]=z[3]*z[11];
    z[14]= - static_cast<T>(7)+ z[13];
    z[14]=z[14]*z[21];
    z[14]=z[14] + n<T>(11,2) + z[7];
    z[14]=z[14]*z[21];
    z[19]=n<T>(19,2) + z[10];
    z[33]= - z[8]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[33];
    z[19]=z[8]*z[19];
    z[14]=z[19] + z[14] - n<T>(15,4) - z[7];
    z[14]=z[5]*z[14];
    z[19]=z[7] + 3;
    z[30]= - z[30] + z[19];
    z[30]=z[1]*z[30];
    z[29]= - n<T>(3,4)*z[2] + z[13] - z[29];
    z[33]=n<T>(1,2)*z[2];
    z[29]=z[29]*z[33];
    z[34]=n<T>(1,4)*z[8];
    z[35]=z[7] + n<T>(7,2);
    z[36]=z[35]*z[34];
    z[37]=n<T>(1,4)*z[7];
    z[36]=z[36] - static_cast<T>(3)+ z[37];
    z[36]=z[8]*z[36];
    z[31]=z[1]*z[31];
    z[31]=n<T>(3,2) + z[31];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[8]*z[34];
    z[31]=n<T>(1,2)*z[31] + z[34];
    z[31]=z[5]*z[31];
    z[34]=z[33] + static_cast<T>(1)- z[1];
    z[31]=n<T>(1,2)*z[34] + z[31];
    z[31]=z[6]*z[31];
    z[14]=z[31] + z[14] + z[36] + z[29] + n<T>(3,4)*z[30] - n<T>(5,8) - z[7];
    z[14]=z[6]*z[14];
    z[29]=z[7] - n<T>(1,2);
    z[29]=z[29]*z[16];
    z[29]=z[29] - 3;
    z[29]=z[29]*z[15];
    z[30]=n<T>(3,2) + z[32];
    z[30]=z[7]*z[30];
    z[30]=n<T>(23,2) + z[30];
    z[30]=z[30]*z[37];
    z[30]=z[29] + static_cast<T>(4)+ z[30];
    z[30]=z[2]*z[30];
    z[31]=n<T>(11,4) + z[7];
    z[31]=z[7]*z[31];
    z[31]=n<T>(7,4) + z[31];
    z[31]=z[31]*z[26];
    z[32]= - static_cast<T>(5)- z[7];
    z[32]=z[32]*z[37];
    z[34]=z[18]*z[2];
    z[32]=n<T>(3,8)*z[34] - static_cast<T>(1)+ z[32];
    z[32]=z[2]*z[16]*z[32];
    z[31]=z[31] + z[32];
    z[31]=z[2]*z[31];
    z[32]= - z[25]*z[26];
    z[31]=z[32] + z[31];
    z[31]=z[4]*z[31];
    z[32]= - n<T>(9,2) - z[24];
    z[32]=z[7]*z[32];
    z[32]= - n<T>(63,4) + z[32];
    z[32]=z[7]*z[32];
    z[32]= - n<T>(25,2) + z[32];
    z[30]=z[31] + n<T>(1,2)*z[32] + z[30];
    z[30]=z[4]*z[30];
    z[31]=z[2]*z[25];
    z[24]=z[31] - static_cast<T>(1)- z[24];
    z[24]=z[22]*z[16]*z[24];
    z[31]=z[35]*z[7];
    z[31]=z[31] + n<T>(5,2);
    z[32]=n<T>(3,4)*z[34] - z[31];
    z[32]=z[32]*z[33];
    z[35]=n<T>(19,8) + z[7];
    z[35]=z[7]*z[35];
    z[32]=z[32] + n<T>(11,8) + z[35];
    z[32]=z[4]*z[32];
    z[35]= - n<T>(17,2) - z[7];
    z[35]=z[7]*z[35];
    z[35]= - static_cast<T>(9)+ z[35];
    z[36]=static_cast<T>(1)+ n<T>(5,8)*z[7];
    z[36]=z[2]*z[36];
    z[32]=z[32] + n<T>(1,4)*z[35] + z[36];
    z[32]=z[8]*z[32];
    z[35]=n<T>(3,2) - z[7];
    z[35]=z[7]*z[35];
    z[35]= - static_cast<T>(11)+ z[35];
    z[15]=z[35]*z[15];
    z[15]=z[32] + z[24] + z[30] + z[15] + static_cast<T>(9)+ n<T>(35,8)*z[7];
    z[15]=z[8]*z[15];
    z[24]=static_cast<T>(3)+ z[9];
    z[24]=z[7]*z[24];
    z[24]=n<T>(5,2) + z[24];
    z[30]= - z[4]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[30];
    z[24]=z[8]*z[24];
    z[30]= - static_cast<T>(6)- z[9];
    z[30]=z[7]*z[30];
    z[32]=2*z[7];
    z[35]=static_cast<T>(5)+ z[32];
    z[35]=z[7]*z[35];
    z[35]=static_cast<T>(3)+ z[35];
    z[35]=z[4]*z[35];
    z[17]=z[3]*z[17];
    z[17]=z[24] + z[17] + z[35] - n<T>(27,4) + z[30];
    z[17]=z[8]*z[17];
    z[24]= - n<T>(7,2) + z[1];
    z[21]=z[24]*z[21];
    z[20]=3*z[20] + z[21];
    z[20]=z[1]*z[20];
    z[20]=z[20] - n<T>(17,4) - z[10];
    z[20]=z[1]*z[20];
    z[21]= - static_cast<T>(2)- z[9];
    z[21]=z[7]*z[21];
    z[21]=z[23] - n<T>(3,2) + z[21];
    z[21]=z[1]*z[21];
    z[23]=z[7]*z[19];
    z[21]=z[21] + static_cast<T>(2)+ z[23];
    z[21]=z[1]*z[21];
    z[12]= - static_cast<T>(4)- z[12];
    z[12]=z[7]*z[12];
    z[12]=z[21] - n<T>(5,2) + z[12];
    z[12]=z[4]*z[12];
    z[21]=z[16]*z[1];
    z[23]=z[26] - z[21];
    z[23]=z[1]*z[23];
    z[23]= - z[26] + z[23];
    z[22]=z[23]*z[22];
    z[23]=static_cast<T>(9)+ z[9];
    z[23]=z[7]*z[23];
    z[23]=static_cast<T>(11)+ z[23];
    z[12]=z[17] + z[22] + z[12] + n<T>(1,2)*z[23] + z[20];
    z[12]=z[5]*z[12];
    z[17]=2*z[1];
    z[20]=static_cast<T>(1)+ n<T>(7,2)*z[7];
    z[20]=z[33] + n<T>(1,4)*z[20] - z[17];
    z[20]=z[2]*z[20];
    z[22]=z[7] + n<T>(1,2);
    z[10]=z[22]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[23]=static_cast<T>(3)+ n<T>(23,4)*z[7];
    z[23]=n<T>(1,2)*z[23];
    z[13]= - z[23] + z[13];
    z[13]=z[1]*z[13];
    z[10]=z[20] + n<T>(1,4)*z[10] + z[13];
    z[10]=z[2]*z[10];
    z[13]=z[1]*z[18];
    z[13]=z[34] + z[13];
    z[13]= - z[31] + n<T>(3,2)*z[13];
    z[13]=z[33]*z[13];
    z[13]=z[13] + z[25];
    z[13]=z[16]*z[13];
    z[18]= - z[22]*z[16];
    z[18]=z[18] + n<T>(3,4)*z[21];
    z[18]=z[1]*z[18];
    z[13]=z[18] + z[13];
    z[13]=z[13]*z[27];
    z[18]= - n<T>(1,8) - z[7];
    z[16]=z[18]*z[16];
    z[16]=n<T>(9,8)*z[21] - n<T>(3,4) + z[16];
    z[16]=z[1]*z[16];
    z[18]=z[7]*z[28];
    z[18]=static_cast<T>(5)+ z[18];
    z[18]=z[7]*z[18];
    z[18]=n<T>(23,4) + z[18];
    z[13]=z[13] + z[29] + n<T>(1,2)*z[18] + z[16];
    z[13]=z[4]*z[13];
    z[9]=z[19]*z[9];
    z[9]= - static_cast<T>(5)+ z[9];
    z[16]= - z[17] + static_cast<T>(3)+ z[32];
    z[16]=z[1]*z[16];
    z[16]= - z[23] + z[16];
    z[16]=z[1]*z[16];

    r += n<T>(1,2)*z[9] + z[10] + z[11] + z[12] + z[13] + z[14] + z[15] + 
      z[16];
 
    return r;
}

template double qqb_2lha_r2381(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2381(const std::array<dd_real,30>&);
#endif
