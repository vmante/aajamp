#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1833(const std::array<T,30>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[20];
    z[5]=k[17];
    z[6]=k[4];
    z[7]=k[2];
    z[8]=k[10];
    z[9]=k[15];
    z[10]=z[5] - 1;
    z[11]=z[1] + 3;
    z[12]= - z[11]*z[10];
    z[13]=z[2] - z[11];
    z[13]=z[2]*z[13];
    z[11]=z[13] + z[11];
    z[11]=z[2]*z[11];
    z[13]=z[10] - z[2];
    z[13]=z[13]*z[8];
    z[14]= - static_cast<T>(3)- z[8];
    z[14]=z[14]*z[13];
    z[11]=2*z[14] + z[11] + z[12];
    z[12]=2*z[10];
    z[14]= - z[8]*z[10];
    z[14]= - z[12] + z[14];
    z[14]=z[8]*z[14];
    z[15]=z[5] + z[9];
    z[16]=z[6]*z[15]*npow(z[8],2);
    z[17]= - static_cast<T>(1)- z[8];
    z[17]=z[8]*z[5]*z[17];
    z[17]=4*z[17] - z[16];
    z[17]=z[6]*z[17];
    z[18]=3*z[10];
    z[19]= - static_cast<T>(3)+ z[2];
    z[19]=z[2]*z[19];
    z[14]=z[17] + 6*z[14] - z[18] + z[19];
    z[14]=z[6]*z[14];
    z[11]=2*z[11] + z[14];
    z[11]=z[6]*z[11];
    z[14]=z[1] + 2;
    z[14]=z[14]*z[1];
    z[14]=z[14] + 3;
    z[17]= - z[14]*z[10];
    z[19]=static_cast<T>(3)+ 2*z[1];
    z[20]=z[2] - z[19];
    z[20]=z[2]*z[20];
    z[20]=z[20] + z[14];
    z[20]=z[2]*z[20];
    z[14]=z[20] + z[14];
    z[14]=z[2]*z[14];
    z[20]=z[2] + 1;
    z[20]=z[20]*z[2];
    z[20]=z[20] - z[10];
    z[20]=z[20]*z[8];
    z[21]=static_cast<T>(4)+ z[8];
    z[21]=z[21]*z[20];
    z[11]=z[11] + z[21] + z[14] + z[17];
    z[11]=z[3]*z[11];
    z[14]=2*z[2];
    z[17]= - z[18] + z[14];
    z[18]=z[14] - z[10];
    z[18]=z[8]*z[18];
    z[17]=2*z[17] + z[18];
    z[17]=z[8]*z[17];
    z[18]=n<T>(1,2)*z[2];
    z[21]= - static_cast<T>(1)+ z[18];
    z[21]=z[21]*z[18];
    z[22]=3*z[9];
    z[23]=z[22] + 5*z[5];
    z[23]=z[8]*z[23];
    z[23]=4*z[15] + z[23];
    z[23]=z[8]*z[23];
    z[23]=z[23] + 2*z[16];
    z[23]=z[6]*z[23];
    z[24]=3*z[8] + 3;
    z[24]=z[5]*z[24];
    z[24]=static_cast<T>(2)+ z[24];
    z[24]=z[8]*z[24];
    z[21]=z[23] + z[24] + z[21] + n<T>(5,4) + 2*z[5];
    z[21]=z[6]*z[21];
    z[23]=n<T>(1,2)*z[1];
    z[24]=z[18] + static_cast<T>(1)- z[23];
    z[24]=z[2]*z[24];
    z[25]= - static_cast<T>(5)- 3*z[1];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[2]*z[24];
    z[17]=z[21] + z[17] - z[12] + z[24];
    z[17]=z[6]*z[17];
    z[14]=static_cast<T>(5)+ z[14];
    z[14]=z[2]*z[14];
    z[14]=z[20] - 5*z[10] + z[14];
    z[14]=z[8]*z[14];
    z[19]=z[19]*z[1];
    z[19]=z[19] + 4;
    z[20]= - z[19]*z[10];
    z[21]= - static_cast<T>(7)+ z[23];
    z[21]=z[1]*z[21];
    z[23]=z[18] + static_cast<T>(3)- z[1];
    z[23]=z[2]*z[23];
    z[21]=z[23] - n<T>(15,2) + z[21];
    z[21]=z[21]*z[18];
    z[19]=z[21] + z[19];
    z[19]=z[2]*z[19];
    z[11]=z[11] + z[17] + z[14] + z[19] + z[20];
    z[11]=z[3]*z[11];
    z[14]=z[7] + 3;
    z[14]=z[14]*z[10];
    z[17]=2*z[9];
    z[19]= - static_cast<T>(1)- z[17];
    z[19]=z[2]*z[19];
    z[14]=z[19] - z[22] + z[14];
    z[14]=z[8]*z[14];
    z[19]= - z[2] - 5;
    z[19]=z[9]*z[19];
    z[20]=static_cast<T>(1)+ 2*z[7];
    z[20]=z[5]*z[20];
    z[14]=z[14] + z[20] - static_cast<T>(1)+ z[19];
    z[14]=z[8]*z[14];
    z[19]=z[18]*z[4];
    z[20]= - z[4] + z[19];
    z[20]=z[20]*z[18];
    z[21]= - z[2] - 4;
    z[21]=z[9]*z[21];
    z[22]=z[5]*z[7];
    z[21]=z[22] + z[21];
    z[21]=z[8]*z[21];
    z[15]= - 2*z[15] + z[21];
    z[15]=z[8]*z[15];
    z[15]=z[15] - z[16];
    z[15]=z[6]*z[15];
    z[16]=n<T>(1,4)*z[4];
    z[14]=z[15] + z[14] + z[20] - z[5] - z[17] + z[16];
    z[14]=z[6]*z[14];
    z[15]=z[7] + 2;
    z[10]=z[15]*z[10];
    z[15]= - static_cast<T>(2)- z[9];
    z[15]=z[2]*z[15];
    z[12]=z[12] + z[15];
    z[12]=z[8]*z[12];
    z[15]= - static_cast<T>(1)- z[9];
    z[15]=z[2]*z[15];
    z[10]=z[12] + z[15] + z[10];
    z[10]=z[8]*z[10];
    z[12]=z[2] - 1;
    z[12]=z[4]*z[12];
    z[15]=z[1]*z[4];
    z[12]= - z[15] + z[12];
    z[12]=z[2]*z[12];
    z[17]=n<T>(1,2)*z[4];
    z[12]= - z[17] + z[12];
    z[12]=z[12]*z[18];
    z[20]=static_cast<T>(1)+ z[7];
    z[20]=z[5]*z[20];
    z[10]=z[14] + z[10] + z[12] + z[20] - static_cast<T>(1)+ z[16];
    z[10]=z[6]*z[10];
    z[12]= - z[15] + z[19];
    z[12]=z[2]*z[12];
    z[14]=static_cast<T>(3)- z[4];
    z[16]=z[1]*z[17];
    z[16]= - z[4] + z[16];
    z[16]=z[1]*z[16];
    z[12]=z[12] + n<T>(1,2)*z[14] + z[16];
    z[12]=z[2]*z[12];
    z[14]=z[15] - static_cast<T>(3)+ z[17];
    z[14]=z[1]*z[14];
    z[12]=z[12] - n<T>(3,2) + z[14];
    z[12]=z[12]*z[18];
    z[14]=z[1] + 1;
    z[14]=z[14]*z[1];
    z[15]=z[4] + 3;
    z[15]=z[15]*z[14];
    z[14]= - static_cast<T>(1)- z[14];
    z[14]=z[5]*z[14];

    r += static_cast<T>(1)+ z[10] + z[11] + z[12] - z[13] + z[14] + n<T>(1,4)*z[15];
 
    return r;
}

template double qqb_2lha_r1833(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1833(const std::array<dd_real,30>&);
#endif
