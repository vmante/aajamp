#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1560(const std::array<T,30>& k) {
  T z[61];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[13];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[11];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=z[4] + n<T>(1,2);
    z[13]=npow(z[4],2);
    z[14]=z[12]*z[13];
    z[15]=n<T>(1,2)*z[10];
    z[16]=z[4] + 1;
    z[17]=z[15]*z[16];
    z[18]=npow(z[4],3);
    z[19]= - z[18]*z[17];
    z[19]=z[14] + z[19];
    z[19]=z[10]*z[19];
    z[20]=n<T>(1,2)*z[4];
    z[21]=z[20] + 1;
    z[22]=z[21]*z[4];
    z[23]=z[22] + n<T>(1,2);
    z[24]=z[23]*z[13];
    z[25]=z[10]*z[24];
    z[26]=z[16]*z[13];
    z[27]=z[26] - z[25];
    z[27]=z[10]*z[27];
    z[28]=n<T>(1,2)*z[13];
    z[27]= - z[28] + z[27];
    z[27]=z[8]*z[27];
    z[19]=z[27] - z[28] + z[19];
    z[27]=n<T>(1,2)*z[8];
    z[19]=z[19]*z[27];
    z[29]=z[21]*z[3];
    z[30]=n<T>(1,2)*z[3];
    z[31]=z[30]*z[1];
    z[32]=z[29] - z[31];
    z[32]=z[1]*z[32];
    z[33]=z[4] - 1;
    z[34]=n<T>(1,2)*z[33];
    z[35]=z[20] - 1;
    z[36]=z[35]*z[4];
    z[37]= - n<T>(7,4) + z[36];
    z[37]=z[3]*z[37];
    z[32]=z[32] - z[34] + z[37];
    z[37]=z[16]*z[4];
    z[38]=z[37] + n<T>(1,2);
    z[39]= - z[38]*z[20];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[4]*z[39];
    z[40]=z[30] - 1;
    z[40]=z[40]*z[17];
    z[41]= - n<T>(5,4) - z[4];
    z[41]=z[3]*z[41];
    z[39]=z[40] + z[41] + n<T>(7,4) + z[39];
    z[39]=z[39]*z[15];
    z[40]=n<T>(1,4)*z[4];
    z[41]=z[40] + 1;
    z[41]=z[41]*z[4];
    z[42]=z[41] + n<T>(1,2);
    z[42]=z[42]*z[13];
    z[43]=n<T>(7,2) - z[37];
    z[43]=z[4]*z[43];
    z[43]=n<T>(11,2) + z[43];
    z[43]=z[3]*z[43];
    z[39]=z[39] + n<T>(1,4)*z[43] - n<T>(5,4) + z[42];
    z[39]=z[10]*z[39];
    z[19]=z[19] + n<T>(1,2)*z[32] + z[39];
    z[19]=z[8]*z[19];
    z[21]=z[21]*z[18];
    z[32]=npow(z[4],4);
    z[39]=z[32]*z[30];
    z[21]=z[21] + z[39];
    z[21]=z[3]*z[21];
    z[21]=z[24] + z[21];
    z[21]=z[3]*z[21];
    z[39]= - z[23]*z[20];
    z[21]=z[39] + z[21];
    z[21]=z[3]*z[21];
    z[26]=n<T>(1,4)*z[26];
    z[39]=z[4] + n<T>(3,2);
    z[43]=n<T>(1,2)*z[39];
    z[44]=z[18]*z[43];
    z[45]=z[32]*z[3];
    z[44]=z[44] + z[45];
    z[44]=z[3]*z[44];
    z[44]= - z[26] + z[44];
    z[44]=z[3]*z[44];
    z[46]=z[16]*z[40];
    z[44]=z[46] + z[44];
    z[44]=z[3]*z[44];
    z[45]=z[45] - n<T>(1,2)*z[18];
    z[45]=z[3]*z[45];
    z[45]=z[28] + z[45];
    z[45]=z[3]*z[45];
    z[45]= - z[20] + z[45];
    z[45]=z[45]*z[31];
    z[44]=z[44] + z[45];
    z[44]=z[1]*z[44];
    z[21]=z[21] + z[44];
    z[21]=z[1]*z[21];
    z[44]=z[4] + n<T>(5,2);
    z[45]=z[44]*z[20];
    z[45]=z[45] + 1;
    z[45]=z[45]*z[4];
    z[45]=z[45] + n<T>(1,4);
    z[46]=z[3]*z[4];
    z[45]=z[45]*z[46];
    z[47]=z[23]*z[28];
    z[47]=z[47] - z[45];
    z[47]=z[10]*z[47];
    z[48]=static_cast<T>(3)+ z[4];
    z[48]=z[4]*z[48];
    z[48]=static_cast<T>(3)+ z[48];
    z[48]=z[4]*z[48];
    z[48]=static_cast<T>(1)+ z[48];
    z[48]=z[48]*z[20];
    z[49]= - z[1]*z[4];
    z[49]=z[37] + z[49];
    z[50]=n<T>(1,2)*z[1];
    z[49]=z[49]*z[50];
    z[51]= - z[4]*z[23];
    z[49]=z[51] + z[49];
    z[49]=z[1]*z[49];
    z[48]=z[48] + z[49];
    z[41]= - n<T>(3,2) - z[41];
    z[41]=z[4]*z[41];
    z[41]= - static_cast<T>(1)+ z[41];
    z[41]=z[4]*z[41];
    z[41]= - n<T>(1,4) + z[41];
    z[41]=z[10]*z[4]*z[41];
    z[41]=n<T>(1,2)*z[48] + z[41];
    z[41]=z[6]*z[41];
    z[21]=z[41] + z[47] + z[45] + z[21];
    z[21]=z[5]*z[21];
    z[41]=z[4] - n<T>(1,2);
    z[45]= - z[41]*z[13];
    z[45]=n<T>(7,2) + z[45];
    z[47]=n<T>(1,4)*z[16];
    z[48]=z[23]*z[3];
    z[49]=z[47] - z[48];
    z[51]=npow(z[3],2);
    z[49]=z[49]*z[51];
    z[45]=n<T>(1,2)*z[45] + z[49];
    z[45]=z[3]*z[45];
    z[49]= - z[4]*z[12];
    z[49]=n<T>(1,2) + z[49];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(3,2) + z[49];
    z[52]=z[33]*z[4];
    z[52]=z[52] + 1;
    z[53]=z[52]*z[4];
    z[54]= - n<T>(1,2) - z[53];
    z[54]=z[3]*z[54];
    z[49]=n<T>(1,2)*z[49] + z[54];
    z[49]=z[3]*z[49];
    z[54]= - static_cast<T>(1)+ z[28];
    z[49]=n<T>(1,2)*z[54] + z[49];
    z[49]=z[3]*z[49];
    z[49]= - z[40] + z[49];
    z[49]=z[3]*z[49];
    z[53]=static_cast<T>(1)- z[53];
    z[53]=z[3]*z[53];
    z[52]=n<T>(1,2)*z[52] + z[53];
    z[52]=z[3]*z[52];
    z[34]= - z[34] + z[52];
    z[34]=z[3]*z[34];
    z[34]=n<T>(1,2) + z[34];
    z[31]=z[34]*z[31];
    z[31]=z[31] - n<T>(1,2) + z[49];
    z[31]=z[1]*z[31];
    z[34]= - z[33]*z[20];
    z[34]=static_cast<T>(1)+ z[34];
    z[34]=z[4]*z[34];
    z[34]=n<T>(1,2) + z[34];
    z[34]=z[3]*z[34];
    z[49]=n<T>(3,2) - z[37];
    z[49]=z[4]*z[49];
    z[49]=n<T>(1,2) + z[49];
    z[34]=n<T>(1,2)*z[49] + z[34];
    z[34]=z[3]*z[34];
    z[49]=n<T>(1,2) - z[37];
    z[49]=z[4]*z[49];
    z[49]=n<T>(1,2) + z[49];
    z[34]=n<T>(1,2)*z[49] + z[34];
    z[34]=z[3]*z[34];
    z[37]= - static_cast<T>(3)+ z[37];
    z[34]=n<T>(1,4)*z[37] + z[34];
    z[34]=z[3]*z[34];
    z[37]=n<T>(1,2)*z[35];
    z[31]=z[31] + z[37] + z[34];
    z[31]=z[1]*z[31];
    z[34]=7*z[4];
    z[49]=static_cast<T>(5)- z[34];
    z[49]=z[4]*z[49];
    z[49]=n<T>(3,2) + z[49];
    z[19]=z[21] + z[19] + z[31] + n<T>(1,4)*z[49] + z[45];
    z[21]= - static_cast<T>(3)- z[20];
    z[21]=z[21]*z[20];
    z[31]=z[4] + 5;
    z[45]= - z[4]*z[31];
    z[45]= - n<T>(5,2) + z[45];
    z[22]= - static_cast<T>(1)- z[22];
    z[22]=z[3]*z[22];
    z[22]=n<T>(1,2)*z[45] + z[22];
    z[22]=z[22]*z[30];
    z[21]=z[22] - static_cast<T>(1)+ z[21];
    z[21]=z[3]*z[21];
    z[22]=3*z[4];
    z[45]=z[22] + 1;
    z[21]= - n<T>(1,4)*z[45] + z[21];
    z[21]=z[3]*z[21];
    z[49]= - n<T>(3,2) - z[36];
    z[49]=z[3]*z[49];
    z[37]=z[37] + z[49];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(1,4) + z[37];
    z[37]=z[37]*z[51]*z[50];
    z[49]= - z[4]*z[41];
    z[49]=n<T>(13,2) + z[49];
    z[52]=n<T>(1,2) - z[36];
    z[52]=z[3]*z[52];
    z[49]=n<T>(1,4)*z[49] + z[52];
    z[49]=z[3]*z[49];
    z[31]=n<T>(1,8)*z[31] + z[49];
    z[31]=z[3]*z[31];
    z[31]=n<T>(1,4) + z[31];
    z[31]=z[3]*z[31];
    z[31]=z[31] + z[37];
    z[31]=z[1]*z[31];
    z[21]=z[31] - n<T>(1,8) + z[21];
    z[21]=z[1]*z[21];
    z[31]=n<T>(1,2)*z[12];
    z[37]=z[16]*z[3];
    z[49]=z[31] + z[37];
    z[49]=z[3]*z[49];
    z[31]=z[31] + z[49];
    z[31]=z[3]*z[31];
    z[49]=static_cast<T>(9)+ 11*z[4];
    z[49]=z[4]*z[49];
    z[49]= - n<T>(5,2) + z[49];
    z[31]=n<T>(1,4)*z[49] + z[31];
    z[31]=z[3]*z[31];
    z[31]=z[31] - n<T>(1,4) + z[4];
    z[21]=n<T>(1,2)*z[31] + z[21];
    z[31]=n<T>(1,2)*z[2];
    z[49]=npow(z[3],4);
    z[52]=z[31]*z[49];
    z[49]=z[49]*z[50];
    z[53]=npow(z[3],3);
    z[54]=n<T>(7,4) - z[37];
    z[54]=z[54]*z[53];
    z[54]=z[52] + z[54] - z[49];
    z[54]=z[2]*z[54];
    z[29]= - n<T>(7,4) + z[29];
    z[29]=z[29]*z[53];
    z[29]=z[29] + z[49];
    z[55]=n<T>(1,4)*z[1];
    z[29]=z[29]*z[55];
    z[56]=n<T>(1,4)*z[48] - n<T>(11,16) - z[4];
    z[56]=z[3]*z[56];
    z[56]=n<T>(9,16) + z[56];
    z[56]=z[56]*z[51];
    z[29]=n<T>(1,4)*z[54] + z[56] + z[29];
    z[29]=z[2]*z[29];
    z[54]=n<T>(7,4) - z[3];
    z[54]=z[54]*z[53];
    z[49]=z[54] - z[49];
    z[49]=z[1]*z[49];
    z[54]=9*z[4];
    z[56]=static_cast<T>(11)+ z[54];
    z[56]=n<T>(1,2)*z[56] - z[3];
    z[56]=z[3]*z[56];
    z[56]= - n<T>(9,2) + z[56];
    z[56]=z[56]*z[51];
    z[49]=n<T>(1,2)*z[56] + z[49];
    z[49]=z[1]*z[49];
    z[54]=z[54] + 13;
    z[54]=z[54]*z[40];
    z[54]=z[54] + 1;
    z[56]=z[3]*z[54];
    z[57]= - static_cast<T>(5)- n<T>(23,2)*z[4];
    z[56]=n<T>(1,2)*z[57] + z[56];
    z[56]=z[3]*z[56];
    z[56]=n<T>(5,4) + z[56];
    z[56]=z[3]*z[56];
    z[49]=z[56] + z[49];
    z[29]=n<T>(1,4)*z[49] + z[29];
    z[29]=z[2]*z[29];
    z[49]=z[4] - 3;
    z[56]= - z[3]*z[49];
    z[56]=n<T>(1,2) + z[56];
    z[56]=z[56]*z[53]*z[50];
    z[57]= - n<T>(17,2) - z[4];
    z[57]=n<T>(1,2)*z[57] - z[46];
    z[57]=z[3]*z[57];
    z[57]= - n<T>(3,4) + z[57];
    z[57]=z[57]*z[51];
    z[56]=z[57] + z[56];
    z[55]=z[56]*z[55];
    z[56]=n<T>(1,8)*z[3];
    z[33]= - z[3]*z[33];
    z[33]= - z[4] + z[33];
    z[33]=z[33]*z[56];
    z[33]=z[33] + static_cast<T>(1)+ n<T>(7,8)*z[4];
    z[33]=z[3]*z[33];
    z[33]=n<T>(3,16) + z[33];
    z[33]=z[3]*z[33];
    z[33]=z[33] + z[55];
    z[33]=z[1]*z[33];
    z[55]= - static_cast<T>(1)- z[3];
    z[55]=z[55]*z[56];
    z[56]=z[4] + n<T>(7,8);
    z[56]=z[56]*z[4];
    z[55]=z[56] + z[55];
    z[55]=z[3]*z[55];
    z[57]= - n<T>(5,2) - z[34];
    z[55]=n<T>(1,8)*z[57] + z[55];
    z[55]=z[3]*z[55];
    z[29]=z[29] + z[33] - n<T>(1,16) + z[55];
    z[29]=z[2]*z[29];
    z[21]=n<T>(1,2)*z[21] + z[29];
    z[21]=z[2]*z[21];
    z[28]=z[28]*z[16];
    z[29]= - z[28] + z[25];
    z[29]=z[8]*z[29];
    z[33]= - z[45]*z[13];
    z[45]=z[16]*z[10];
    z[55]=z[18]*z[45];
    z[29]=3*z[29] + z[33] + z[55];
    z[29]=z[27]*z[29];
    z[33]=z[38]*z[13];
    z[17]=z[17] - static_cast<T>(1)+ z[33];
    z[55]=n<T>(1,4)*z[10];
    z[17]=z[17]*z[55];
    z[57]=z[4] + n<T>(3,4);
    z[57]=z[57]*z[4];
    z[58]=n<T>(5,8) - z[57];
    z[58]=z[4]*z[58];
    z[17]=z[29] + z[17] - n<T>(5,8) + z[58];
    z[17]=z[8]*z[10]*z[17];
    z[29]= - z[16]*z[18]*z[55];
    z[58]=z[4] + n<T>(1,4);
    z[58]=z[58]*z[13];
    z[29]=z[58] + z[29];
    z[29]=z[10]*z[29];
    z[59]=z[8]*npow(z[10],2)*z[24];
    z[29]=z[29] - n<T>(3,2)*z[59];
    z[59]=npow(z[8],2);
    z[29]=z[29]*z[59];
    z[25]=z[28] + z[25];
    z[28]=z[7]*npow(z[8],3);
    z[15]=z[25]*z[15]*z[28];
    z[16]=z[16] + z[1];
    z[25]=n<T>(1,4)*z[6];
    z[60]=z[16]*z[25];
    z[15]=z[15] + z[29] + z[60];
    z[15]=z[7]*z[15];
    z[29]=n<T>(1,2)*z[49] - z[1];
    z[29]=z[29]*z[50];
    z[33]=z[33]*z[55];
    z[57]= - n<T>(1,4) - z[57];
    z[57]=z[4]*z[57];
    z[57]=z[57] + z[33];
    z[57]=z[10]*z[57];
    z[60]=static_cast<T>(1)+ n<T>(3,4)*z[4];
    z[60]=z[60]*z[4];
    z[29]=z[57] + z[29] + n<T>(1,8) + z[60];
    z[29]=z[6]*z[29];
    z[15]=z[15] + z[29] + n<T>(1,4)*z[16] + z[17];
    z[16]=n<T>(1,4)*z[7];
    z[15]=z[15]*z[16];
    z[16]= - static_cast<T>(1)- n<T>(13,8)*z[4];
    z[16]=z[16]*z[22];
    z[17]=z[20]*z[3];
    z[29]=z[54]*z[17];
    z[16]=z[29] - n<T>(1,4) + z[16];
    z[16]=z[3]*z[16];
    z[29]=static_cast<T>(3)+ 19*z[4];
    z[16]=n<T>(1,8)*z[29] + z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(1,8) + z[16];
    z[29]=static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[29]=z[3]*z[29];
    z[29]= - n<T>(7,4) + z[29];
    z[29]=z[29]*z[53];
    z[29]=z[29] - z[52];
    z[29]=z[29]*z[31];
    z[54]=static_cast<T>(11)+ 23*z[4];
    z[57]= - n<T>(1,4) - z[60];
    z[57]=z[3]*z[57];
    z[54]=n<T>(1,8)*z[54] + z[57];
    z[54]=z[3]*z[54];
    z[54]= - n<T>(9,8) + z[54];
    z[51]=z[54]*z[51];
    z[29]=z[51] + z[29];
    z[29]=z[29]*z[31];
    z[23]=z[23]*z[17];
    z[31]= - static_cast<T>(3)- n<T>(25,8)*z[4];
    z[31]=z[4]*z[31];
    z[31]=z[23] - n<T>(1,2) + z[31];
    z[31]=z[31]*z[30];
    z[31]=z[31] + n<T>(5,8) + 2*z[4];
    z[31]=z[3]*z[31];
    z[31]= - n<T>(5,16) + z[31];
    z[31]=z[3]*z[31];
    z[29]=z[31] + z[29];
    z[29]=z[2]*z[29];
    z[16]=n<T>(1,2)*z[16] + z[29];
    z[16]=z[2]*z[16];
    z[29]= - static_cast<T>(1)- n<T>(13,4)*z[4];
    z[29]=z[29]*z[20];
    z[31]=n<T>(1,8) + z[56];
    z[31]=z[31]*z[46];
    z[29]=z[29] + z[31];
    z[29]=z[3]*z[29];
    z[16]=z[16] + z[40] + z[29];
    z[16]=z[2]*z[16];
    z[29]=static_cast<T>(3)+ z[34];
    z[29]=z[3]*z[29];
    z[29]= - static_cast<T>(3)+ z[29];
    z[29]=z[13]*z[29];
    z[16]=n<T>(1,8)*z[29] + z[16];
    z[16]=z[2]*z[16];
    z[29]= - z[4]*z[39];
    z[29]= - n<T>(1,2) + z[29];
    z[13]=z[3]*z[29]*z[13];
    z[13]=z[24] + z[13];
    z[13]=z[10]*z[13];
    z[14]=z[30]*z[14];
    z[14]=z[18] + z[14];
    z[18]= - static_cast<T>(1)+ z[45];
    z[18]=z[8]*z[32]*z[18];
    z[13]=n<T>(1,16)*z[18] + n<T>(1,8)*z[13] + n<T>(1,4)*z[14] + z[16];
    z[13]=z[9]*z[13];
    z[14]=z[43] - z[37];
    z[14]=z[14]*z[53];
    z[14]=z[14] + z[52];
    z[14]=z[2]*z[14];
    z[16]= - z[4]*z[44];
    z[16]= - n<T>(3,2) + z[16];
    z[16]=n<T>(1,2)*z[16] + z[48];
    z[16]=z[3]*z[16];
    z[16]=z[47] + z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(11,4) + z[16];
    z[16]=z[3]*z[16];
    z[18]=static_cast<T>(1)- z[3];
    z[18]=z[10]*z[18];
    z[18]=z[18] - n<T>(3,2) + 5*z[3];
    z[18]=z[18]*z[55];
    z[14]=z[18] + z[14] - static_cast<T>(1)+ z[16];
    z[14]=z[14]*z[55];
    z[16]= - z[33] + z[42] + z[23];
    z[16]=z[10]*z[16];
    z[18]=z[50] - z[35];
    z[18]=z[1]*z[18];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[4]*z[20];
    z[17]=z[18] + z[17] - n<T>(1,2) + z[20];
    z[17]=z[1]*z[17];
    z[18]= - z[12]*z[46];
    z[20]= - z[4]*z[49];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[4]*z[20];
    z[20]=n<T>(1,2) + z[20];
    z[17]=z[17] + n<T>(1,2)*z[20] + z[18];
    z[12]= - z[50] + z[12];
    z[12]=z[1]*z[12];
    z[12]= - n<T>(1,2)*z[38] + z[12];
    z[12]=z[12]*z[27];
    z[12]=z[12] + n<T>(1,2)*z[17] + z[16];
    z[12]=z[12]*z[25];
    z[16]= - z[59]*z[58];
    z[17]= - z[28]*z[26];
    z[16]=z[16] + z[17];
    z[16]=z[7]*z[16];
    z[17]= - z[41]*z[22];
    z[17]= - n<T>(7,4) + z[17];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(7,4)*z[10] + n<T>(7,4) + z[17];
    z[17]=z[17]*z[27];
    z[16]=z[17] + z[16];
    z[16]=z[7]*z[16];
    z[17]= - n<T>(7,4) - z[36];
    z[17]=z[4]*z[17];
    z[17]=n<T>(11,4) + z[17];
    z[17]=z[2]*z[17]*z[30];
    z[18]=n<T>(5,4) - z[4];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(7,4) + z[18];
    z[18]=z[4]*z[18];
    z[16]=z[16] + z[17] + n<T>(9,4) + z[18];
    z[17]= - z[55] + n<T>(3,2);
    z[18]=z[2]*z[3];
    z[17]=z[18]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[55];
    z[17]=z[17] - n<T>(11,8) - z[18];
    z[17]=z[10]*z[17];
    z[16]=z[17] + n<T>(1,2)*z[16];
    z[16]=z[11]*z[16];

    r += z[12] + z[13] + z[14] + z[15] + n<T>(1,2)*z[16] + n<T>(1,4)*z[19] + 
      z[21];
 
    return r;
}

template double qqb_2lha_r1560(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1560(const std::array<dd_real,30>&);
#endif
