#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r519(const std::array<T,30>& k) {
  T z[59];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[6];
    z[8]=k[11];
    z[9]=k[10];
    z[10]=k[12];
    z[11]=k[15];
    z[12]=3*z[6];
    z[13]=n<T>(41,6) + z[12];
    z[14]=z[6] + 3;
    z[15]= - z[3]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[15];
    z[13]=z[3]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[15]=n<T>(1,2)*z[3];
    z[13]=z[13]*z[15];
    z[16]=n<T>(1,3)*z[3];
    z[17]=n<T>(7,2) + z[6];
    z[17]=z[17]*z[16];
    z[17]= - n<T>(3,4) + z[17];
    z[18]=npow(z[3],2);
    z[17]=z[17]*z[18];
    z[19]=npow(z[3],3);
    z[20]=z[19]*z[2];
    z[21]=n<T>(1,3)*z[20];
    z[17]=z[17] - z[21];
    z[17]=z[2]*z[17];
    z[13]=z[13] + z[17];
    z[13]=z[2]*z[13];
    z[17]=n<T>(5,3)*z[3] - n<T>(7,3) - n<T>(3,2)*z[6];
    z[17]=z[3]*z[17];
    z[17]=z[17] - n<T>(1,4) + z[6];
    z[17]=z[17]*z[15];
    z[13]=z[17] + z[13];
    z[13]=z[2]*z[13];
    z[17]= - static_cast<T>(1)+ z[6];
    z[17]=z[3]*z[17];
    z[17]=n<T>(5,4) + z[17];
    z[17]=z[17]*z[16];
    z[22]=n<T>(13,3)*z[6];
    z[23]=n<T>(5,2) - z[22];
    z[17]=n<T>(1,2)*z[23] + z[17];
    z[17]=z[3]*z[17];
    z[17]=n<T>(7,4) + z[17];
    z[23]= - n<T>(5,6) + z[5];
    z[24]=n<T>(1,2)*z[5];
    z[23]=z[24]*z[6]*z[23];
    z[13]=z[23] + n<T>(1,2)*z[17] + z[13];
    z[17]= - n<T>(1,2) + z[3];
    z[17]=z[17]*z[18];
    z[23]=n<T>(1,4) - z[3];
    z[23]=z[23]*z[18];
    z[21]=z[23] + z[21];
    z[21]=z[2]*z[21];
    z[17]=z[17] + z[21];
    z[17]=z[2]*z[17];
    z[21]=n<T>(1,4) - z[16];
    z[21]=z[21]*z[18];
    z[23]=z[4]*npow(z[5],3);
    z[25]=npow(z[5],2);
    z[26]=n<T>(11,4)*z[25] + z[23];
    z[26]=z[4]*z[26];
    z[26]=n<T>(5,2)*z[5] + z[26];
    z[26]=z[4]*z[26];
    z[17]=z[26] + z[17] + n<T>(3,4) + z[21];
    z[17]=z[1]*z[17];
    z[21]=n<T>(5,2) - z[6];
    z[26]=n<T>(1,3)*z[6];
    z[27]= - z[5]*z[26];
    z[21]=n<T>(3,4)*z[21] + z[27];
    z[21]=z[5]*z[21];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[5]*z[21];
    z[27]=n<T>(1,2)*z[6];
    z[28]=z[27] - 1;
    z[28]=z[28]*z[5];
    z[29]= - n<T>(5,4) - z[28];
    z[29]=z[29]*z[25];
    z[29]=z[29] - n<T>(1,2)*z[23];
    z[29]=z[4]*z[29];
    z[21]=z[21] + z[29];
    z[21]=z[4]*z[21];
    z[29]= - z[5]*z[12];
    z[29]=z[29] + n<T>(21,4) - z[6];
    z[29]=z[5]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[21]=n<T>(1,4)*z[29] + z[21];
    z[21]=z[4]*z[21];
    z[13]=n<T>(1,4)*z[17] + n<T>(1,2)*z[13] + z[21];
    z[13]=z[1]*z[13];
    z[17]=z[6] + n<T>(1,4);
    z[21]=npow(z[6],2);
    z[29]=z[17]*z[21];
    z[30]=npow(z[6],3);
    z[31]=z[30]*z[3];
    z[32]=z[6] + 1;
    z[33]=z[32]*z[31];
    z[29]=3*z[29] - n<T>(1,3)*z[33];
    z[33]=n<T>(1,4)*z[3];
    z[29]=z[29]*z[33];
    z[34]=17*z[6];
    z[35]=static_cast<T>(7)+ z[34];
    z[35]=z[35]*z[27];
    z[35]= - static_cast<T>(5)+ z[35];
    z[36]=n<T>(1,2)*z[21];
    z[35]=z[35]*z[36];
    z[37]=z[27] + 1;
    z[37]=z[37]*z[6];
    z[38]=z[37] + n<T>(1,2);
    z[39]= - z[38]*z[31];
    z[35]=z[35] + z[39];
    z[35]=z[35]*z[16];
    z[39]=n<T>(67,12) + z[6];
    z[39]=z[6]*z[39];
    z[39]=n<T>(79,12) + z[39];
    z[39]=z[6]*z[39];
    z[39]=n<T>(5,12) + z[39];
    z[39]=z[6]*z[39];
    z[35]=z[35] - n<T>(19,12) + z[39];
    z[39]=n<T>(1,4)*z[9];
    z[35]=z[35]*z[39];
    z[40]=n<T>(91,24) + z[6];
    z[40]=z[40]*z[27];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[6]*z[40];
    z[29]=z[35] + z[29] - n<T>(19,48) + z[40];
    z[29]=z[9]*z[29];
    z[35]=static_cast<T>(25)- z[6];
    z[35]=z[35]*z[27];
    z[35]=static_cast<T>(13)+ z[35];
    z[40]=n<T>(1,3)*z[9];
    z[35]=z[35]*z[40];
    z[41]=z[6] + n<T>(5,2);
    z[35]=3*z[41] + z[35];
    z[35]=z[9]*z[35];
    z[42]=z[38]*z[9];
    z[43]= - z[42] - z[32];
    z[43]=z[9]*z[43];
    z[43]= - n<T>(1,2) + z[43];
    z[43]=z[8]*z[43];
    z[35]=n<T>(7,3)*z[43] + n<T>(19,6) + z[35];
    z[35]=z[8]*z[35];
    z[43]=n<T>(19,2)*z[21] - z[31];
    z[43]=z[3]*z[43];
    z[37]=z[37] + n<T>(1,12)*z[43];
    z[43]=z[14]*z[27];
    z[43]=static_cast<T>(1)+ z[43];
    z[44]=n<T>(1,4)*z[6];
    z[45]=z[44] + 1;
    z[46]=z[6]*z[45];
    z[46]=n<T>(5,4) + z[46];
    z[46]=z[6]*z[46];
    z[46]=n<T>(1,2) + z[46];
    z[46]=z[9]*z[46];
    z[43]=n<T>(1,2)*z[43] + z[46];
    z[46]=z[4]*z[9];
    z[43]=z[43]*z[46];
    z[29]=z[43] + n<T>(1,8)*z[35] + n<T>(1,2)*z[37] + z[29];
    z[29]=z[10]*z[29];
    z[35]= - z[38]*z[12];
    z[14]=z[14]*z[6];
    z[14]=z[14] + 3;
    z[37]=z[14]*z[6];
    z[37]=z[37] + 1;
    z[43]=z[27]*z[9];
    z[47]= - z[37]*z[43];
    z[35]=z[35] + z[47];
    z[35]=z[9]*z[35];
    z[47]= - z[6]*z[32];
    z[35]=z[47] + z[35];
    z[35]=z[35]*z[24];
    z[47]=z[44]*z[9];
    z[48]=z[37]*z[47];
    z[49]=z[38]*z[6];
    z[48]=z[49] + z[48];
    z[48]=z[9]*z[48];
    z[35]=z[35] + n<T>(1,4)*z[21] + z[48];
    z[35]=z[5]*z[35];
    z[48]= - z[1]*z[25]*z[44];
    z[35]=z[35] + z[48];
    z[35]=z[7]*z[35];
    z[13]=z[35] + z[13] + z[29];
    z[29]=n<T>(109,4) - z[34];
    z[29]=z[29]*z[26];
    z[34]= - n<T>(41,3)*z[21] + z[31];
    z[34]=z[34]*z[33];
    z[29]=z[29] + z[34];
    z[34]=static_cast<T>(29)+ z[6];
    z[34]=z[6]*z[34];
    z[34]=static_cast<T>(5)+ z[34];
    z[34]=z[34]*z[21];
    z[35]=13*z[6];
    z[48]=static_cast<T>(1)+ z[35];
    z[48]=z[48]*z[31];
    z[34]=z[34] + z[48];
    z[34]=z[3]*z[34];
    z[48]=static_cast<T>(1)- n<T>(41,4)*z[6];
    z[48]=z[48]*z[27];
    z[48]=static_cast<T>(5)+ z[48];
    z[48]=z[6]*z[48];
    z[34]=z[48] + n<T>(1,8)*z[34];
    z[34]=z[34]*z[40];
    z[29]=n<T>(1,4)*z[29] + z[34];
    z[29]=z[9]*z[29];
    z[34]= - static_cast<T>(9)+ z[26];
    z[34]=z[34]*z[30];
    z[40]=npow(z[6],4);
    z[48]=z[40]*z[3];
    z[34]=z[34] - n<T>(13,6)*z[48];
    z[34]=z[34]*z[15];
    z[49]= - n<T>(9,2) + n<T>(55,3)*z[6];
    z[49]=z[6]*z[49];
    z[49]=static_cast<T>(1)+ z[49];
    z[49]=z[6]*z[49];
    z[34]=z[49] + z[34];
    z[34]=z[9]*z[34];
    z[49]=29*z[6];
    z[50]= - static_cast<T>(23)+ z[49];
    z[50]=z[6]*z[50];
    z[50]=static_cast<T>(3)+ n<T>(1,6)*z[50];
    z[50]=z[6]*z[50];
    z[34]=z[34] + z[50] - n<T>(5,3)*z[31];
    z[50]=n<T>(1,2)*z[9];
    z[34]=z[34]*z[50];
    z[51]=z[26] + 1;
    z[51]=z[51]*z[6];
    z[34]=z[51] + z[34];
    z[52]=3*z[30];
    z[53]= - z[52] - n<T>(1,12)*z[48];
    z[53]=z[9]*z[53];
    z[52]= - z[52] + z[53];
    z[52]=z[5]*z[9]*z[52];
    z[34]=n<T>(1,2)*z[34] + z[52];
    z[34]=z[5]*z[34];
    z[52]=n<T>(31,4) - z[6];
    z[52]=z[52]*z[26];
    z[53]=z[3]*z[21];
    z[52]=z[52] - n<T>(7,2)*z[53];
    z[53]=z[18]*z[8];
    z[54]=z[30]*z[53];
    z[29]=z[34] + n<T>(1,16)*z[54] + n<T>(1,4)*z[52] + z[29];
    z[29]=z[29]*z[24];
    z[34]=npow(z[9],2);
    z[52]=z[34]*z[32];
    z[54]=z[6] - n<T>(1,2);
    z[55]= - z[54]*z[50];
    z[56]=z[12] + 1;
    z[55]=z[55] - z[56];
    z[55]=z[9]*z[55];
    z[55]= - n<T>(3,2) + z[55];
    z[55]=z[8]*z[55];
    z[55]= - n<T>(1,2)*z[52] + z[55];
    z[55]=z[8]*z[55];
    z[55]=n<T>(1,4) + z[55];
    z[57]=static_cast<T>(3)+ z[27];
    z[57]=z[57]*z[27];
    z[58]=static_cast<T>(5)+ 9*z[6];
    z[17]=z[9]*z[17];
    z[17]=n<T>(1,2)*z[58] + z[17];
    z[17]=z[9]*z[17];
    z[17]=z[17] + static_cast<T>(7)+ z[57];
    z[57]= - static_cast<T>(1)+ z[26];
    z[57]=z[6]*z[57];
    z[58]= - z[9]*z[26];
    z[58]= - z[6] + z[58];
    z[58]=z[9]*z[58];
    z[57]=z[57] + z[58];
    z[57]=z[5]*z[57];
    z[17]=n<T>(1,4)*z[17] + z[57];
    z[17]=z[5]*z[17];
    z[52]= - z[52] - n<T>(15,2) + z[6];
    z[17]=n<T>(1,8)*z[52] + z[17];
    z[17]=z[5]*z[17];
    z[17]=n<T>(1,4)*z[55] + z[17];
    z[52]=z[9] + 3;
    z[55]=z[52]*z[39];
    z[55]=z[55] - static_cast<T>(3)+ n<T>(5,4)*z[6];
    z[57]=static_cast<T>(1)+ z[39];
    z[57]=z[57]*z[50];
    z[58]= - n<T>(1,3) + n<T>(1,8)*z[6];
    z[58]=z[6]*z[58];
    z[57]=z[57] + n<T>(3,4) + z[58];
    z[57]=z[5]*z[57];
    z[55]=n<T>(1,4)*z[55] + z[57];
    z[55]=z[5]*z[55];
    z[55]=n<T>(3,16) + z[55];
    z[55]=z[5]*z[55];
    z[57]=npow(z[8],2);
    z[52]=z[9]*z[52]*z[57];
    z[28]=n<T>(9,16) + z[28];
    z[25]=z[28]*z[25];
    z[23]=z[25] + n<T>(1,4)*z[23];
    z[23]=z[4]*z[23];
    z[23]=n<T>(1,2)*z[23] - n<T>(1,16)*z[52] + z[55];
    z[23]=z[4]*z[23];
    z[17]=n<T>(1,2)*z[17] + z[23];
    z[17]=z[4]*z[17];
    z[23]=z[32]*z[27];
    z[25]=static_cast<T>(1)- z[23];
    z[25]=z[9]*z[25];
    z[25]=z[25] + n<T>(9,4) - z[6];
    z[25]=z[9]*z[25];
    z[28]= - z[6]*z[56];
    z[28]= - n<T>(7,6)*z[9] - n<T>(7,3) + z[28];
    z[28]=z[28]*z[50];
    z[28]=z[28] - n<T>(1,2) - z[12];
    z[28]=z[8]*z[28];
    z[25]=z[28] - n<T>(3,2) + z[25];
    z[25]=z[8]*z[25];
    z[28]=z[2]*z[57];
    z[25]=n<T>(3,2)*z[28] - n<T>(7,4) + z[25];
    z[28]= - static_cast<T>(65)+ 7*z[6];
    z[28]=z[28]*z[44];
    z[49]= - n<T>(11,2) + z[49];
    z[43]=z[49]*z[43];
    z[49]= - static_cast<T>(13)+ n<T>(47,4)*z[6];
    z[49]=z[6]*z[49];
    z[43]=z[49] + z[43];
    z[43]=z[9]*z[43];
    z[28]=z[28] + z[43];
    z[43]=z[21]*z[9];
    z[49]= - static_cast<T>(1)- z[50];
    z[49]=z[49]*z[43];
    z[36]= - z[36] + z[49];
    z[36]=z[5]*z[36];
    z[28]=n<T>(1,6)*z[28] + 5*z[36];
    z[28]=z[5]*z[28];
    z[36]=5*z[6];
    z[49]= - static_cast<T>(7)- z[36];
    z[47]=z[49]*z[47];
    z[47]=z[47] + n<T>(5,8) - z[6];
    z[47]=z[47]*z[50];
    z[28]=z[28] + z[47] + static_cast<T>(1)- n<T>(11,48)*z[6];
    z[28]=z[5]*z[28];
    z[25]=n<T>(1,4)*z[25] + z[28];
    z[17]=n<T>(1,2)*z[25] + z[17];
    z[17]=z[4]*z[17];
    z[24]=z[40]*z[34]*z[24];
    z[25]=z[30]*z[9];
    z[28]=n<T>(3,2) + z[6];
    z[28]=z[9]*z[28];
    z[28]=static_cast<T>(2)+ z[28];
    z[28]=z[28]*z[25];
    z[28]=z[28] - z[24];
    z[28]=z[5]*z[28];
    z[34]= - static_cast<T>(2)- z[27];
    z[34]=z[6]*z[34];
    z[34]= - n<T>(3,2) + z[34];
    z[34]=z[9]*z[34];
    z[34]= - z[41] + z[34];
    z[34]=z[34]*z[43];
    z[40]= - z[6]*z[42];
    z[23]= - z[23] + z[40];
    z[23]=z[23]*z[46];
    z[23]=z[23] + z[28] - z[21] + z[34];
    z[23]=z[11]*z[23];
    z[28]= - n<T>(1,8) + z[6];
    z[28]=z[9]*z[28];
    z[28]=n<T>(3,4) + z[28];
    z[25]=z[28]*z[25];
    z[24]=z[25] - z[24];
    z[24]=z[5]*z[24];
    z[25]= - z[6]*z[54];
    z[25]=n<T>(15,4) + z[25];
    z[25]=z[25]*z[50];
    z[28]=z[44] - 1;
    z[25]= - 3*z[28] + z[25];
    z[25]=z[25]*z[43];
    z[24]=z[24] + n<T>(3,4)*z[21] + z[25];
    z[24]=z[5]*z[24];
    z[25]= - static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(7,4) + z[25];
    z[25]=z[9]*z[25];
    z[25]= - n<T>(15,4) + z[25];
    z[25]=z[50]*z[25];
    z[25]=z[28] + z[25];
    z[25]=z[6]*z[25];
    z[28]= - n<T>(1,2)*z[32] - z[42];
    z[28]=z[4]*z[28]*z[39];
    z[23]=z[23] + z[28] + z[24] + z[25];
    z[23]=z[11]*z[23];
    z[24]= - z[30]*z[15];
    z[25]= - n<T>(1,2) - z[6];
    z[25]=z[6]*z[25];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[6]*z[25];
    z[24]=z[24] - static_cast<T>(1)+ z[25];
    z[24]=z[24]*z[15];
    z[25]=n<T>(1,6)*z[9];
    z[28]= - static_cast<T>(19)+ z[6];
    z[28]=z[28]*z[25];
    z[34]=z[6] - n<T>(3,2);
    z[39]= - z[6]*z[34];
    z[28]=z[28] - n<T>(19,6) + z[39];
    z[28]=z[28]*z[50];
    z[39]= - static_cast<T>(1)+ z[35];
    z[39]=z[6]*z[39];
    z[39]= - static_cast<T>(11)+ z[39];
    z[39]=z[6]*z[39];
    z[39]= - static_cast<T>(7)+ z[39];
    z[39]=z[39]*z[27];
    z[40]=z[9]*z[32];
    z[40]=static_cast<T>(1)+ z[40];
    z[40]=z[9]*z[40];
    z[39]=n<T>(7,2)*z[40] - static_cast<T>(5)+ z[39];
    z[39]=z[8]*z[39];
    z[40]=n<T>(11,2)*z[6];
    z[41]= - static_cast<T>(7)+ z[40];
    z[41]=z[6]*z[41];
    z[41]= - n<T>(23,4) + z[41];
    z[41]=z[6]*z[41];
    z[41]=n<T>(7,2) + z[41];
    z[24]=n<T>(1,6)*z[39] + z[28] + n<T>(1,3)*z[41] + z[24];
    z[24]=z[8]*z[24];
    z[28]= - 23*z[30] - 11*z[48];
    z[25]=z[28]*z[25];
    z[21]=z[25] + z[21] + n<T>(1,6)*z[31];
    z[21]=z[50]*z[21];
    z[25]=z[12] - n<T>(7,3);
    z[28]=z[25]*z[27];
    z[28]= - n<T>(1,3) + z[28];
    z[30]=z[3]*z[26];
    z[28]=n<T>(1,2)*z[28] + z[30];
    z[28]=z[3]*z[28];
    z[30]= - n<T>(17,6) - z[6];
    z[30]=z[6]*z[30];
    z[30]=n<T>(7,3) + z[30];
    z[21]=z[21] + n<T>(1,2)*z[30] + z[28];
    z[21]=z[3]*z[21];
    z[25]=z[25]*z[44];
    z[21]=z[24] + static_cast<T>(1)+ z[25] + z[21];
    z[24]=n<T>(5,4) + z[6];
    z[24]=z[3]*z[24];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[24]*z[18];
    z[25]= - n<T>(43,12) - z[36];
    z[28]=z[3]*z[38];
    z[25]=n<T>(1,4)*z[25] + z[28];
    z[25]=z[3]*z[25];
    z[25]=n<T>(1,2) + z[25];
    z[25]=z[8]*z[3]*z[25];
    z[24]=z[24] + z[25];
    z[24]=z[8]*z[24];
    z[25]= - z[32]*z[16];
    z[25]=n<T>(5,16) + z[25];
    z[25]=z[25]*z[53];
    z[25]= - n<T>(1,3)*z[19] + z[25];
    z[25]=z[8]*z[25];
    z[20]=z[57]*z[20];
    z[20]=z[25] + n<T>(1,12)*z[20];
    z[20]=z[2]*z[20];
    z[19]=z[20] + n<T>(1,4)*z[19] + z[24];
    z[19]=z[2]*z[19];
    z[20]= - n<T>(11,4) - z[6];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(7,4) + z[20];
    z[20]=z[3]*z[20];
    z[20]=z[20] + n<T>(127,48) + z[12];
    z[20]=z[3]*z[20];
    z[20]= - n<T>(5,4) + z[20];
    z[20]=z[20]*z[15];
    z[24]=n<T>(43,2) + 15*z[6];
    z[24]=z[6]*z[24];
    z[24]=n<T>(13,2) + z[24];
    z[25]= - static_cast<T>(1)- z[51];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(1,3) + z[25];
    z[25]=z[3]*z[25];
    z[24]=n<T>(1,8)*z[24] + z[25];
    z[24]=z[24]*z[15];
    z[24]=z[24] - n<T>(11,24) - z[6];
    z[24]=z[3]*z[24];
    z[24]=n<T>(13,96) + z[24];
    z[24]=z[8]*z[24];
    z[20]=z[20] + z[24];
    z[20]=z[8]*z[20];
    z[24]=z[6] + n<T>(11,6);
    z[25]= - z[3]*z[24];
    z[25]=n<T>(11,8) + z[25];
    z[18]=z[25]*z[18];
    z[18]=n<T>(1,2)*z[19] + n<T>(1,4)*z[18] + z[20];
    z[18]=z[2]*z[18];
    z[19]= - n<T>(17,3) - z[12];
    z[19]=z[6]*z[19];
    z[20]=n<T>(7,4) + z[26];
    z[20]=z[6]*z[20];
    z[20]=n<T>(5,2) + z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(13,12) + z[20];
    z[20]=z[3]*z[20];
    z[19]=z[20] - n<T>(53,24) + z[19];
    z[19]=z[3]*z[19];
    z[20]=n<T>(9,4) + z[36];
    z[19]=n<T>(3,4)*z[20] + z[19];
    z[19]=z[3]*z[19];
    z[20]=z[45]*z[26];
    z[20]=n<T>(1,2) + z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(1,3) + z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(1,12) + z[20];
    z[20]=z[3]*z[20];
    z[25]= - n<T>(43,4) - z[36];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(13,2) + z[25];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(3,4) + z[25];
    z[20]=n<T>(1,4)*z[25] + z[20];
    z[20]=z[3]*z[20];
    z[25]=n<T>(11,4) + z[12];
    z[25]=z[6]*z[25];
    z[20]=z[20] + n<T>(11,24) + z[25];
    z[20]=z[3]*z[20];
    z[25]=n<T>(1,4) - z[35];
    z[20]=n<T>(1,12)*z[25] + z[20];
    z[20]=z[8]*z[20];
    z[19]=z[20] - n<T>(11,24) + z[19];
    z[19]=z[8]*z[19];
    z[20]= - static_cast<T>(7)- z[40];
    z[25]=n<T>(7,3) + z[27];
    z[25]=z[6]*z[25];
    z[25]=n<T>(5,2) + z[25];
    z[25]=z[3]*z[25];
    z[20]=n<T>(1,2)*z[20] + z[25];
    z[20]=z[3]*z[20];
    z[20]=n<T>(3,2) + z[20];
    z[20]=z[20]*z[15];
    z[19]=z[20] + z[19];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[2]*z[18];
    z[19]= - z[37]*z[33];
    z[20]=n<T>(163,48) + z[6];
    z[20]=z[6]*z[20];
    z[20]=n<T>(45,16) + z[20];
    z[20]=z[6]*z[20];
    z[19]=z[19] + n<T>(23,48) + z[20];
    z[19]=z[3]*z[19];
    z[20]= - static_cast<T>(7)- n<T>(15,2)*z[6];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(3,8) + z[20];
    z[19]=n<T>(1,2)*z[20] + z[19];
    z[19]=z[3]*z[19];
    z[20]= - n<T>(7,3) + z[40];
    z[19]=n<T>(1,4)*z[20] + z[19];
    z[20]=n<T>(43,3) + z[36];
    z[20]=z[6]*z[20];
    z[20]=static_cast<T>(13)+ z[20];
    z[20]=z[6]*z[20];
    z[20]=static_cast<T>(3)+ z[20];
    z[20]=z[20]*z[27];
    z[20]= - n<T>(1,3) + z[20];
    z[20]=z[3]*z[20];
    z[25]= - n<T>(11,8) - z[6];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(11,24) + z[25];
    z[25]=z[6]*z[25];
    z[20]=n<T>(1,16)*z[20] - n<T>(1,12) + z[25];
    z[20]=z[3]*z[20];
    z[25]= - n<T>(1,2) + z[35];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(11,6) + z[25];
    z[20]=n<T>(1,16)*z[25] + z[20];
    z[20]=z[8]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[8]*z[19];
    z[20]=static_cast<T>(17)+ z[40];
    z[20]=z[6]*z[20];
    z[20]=n<T>(67,6) + z[20];
    z[14]= - z[3]*z[14];
    z[14]=n<T>(1,2)*z[20] + z[14];
    z[14]=z[14]*z[15];
    z[14]=z[14] - n<T>(37,24) - z[12];
    z[14]=z[3]*z[14];
    z[14]=n<T>(3,8) + z[14];
    z[14]=z[18] + n<T>(1,4)*z[14] + z[19];
    z[14]=z[2]*z[14];
    z[18]= - n<T>(13,3) - z[12];
    z[18]=z[6]*z[18];
    z[18]= - n<T>(7,6) + z[18];
    z[18]=z[18]*z[27];
    z[18]=n<T>(1,3) + z[18];
    z[18]=z[3]*z[18];
    z[19]=n<T>(31,4) + z[36];
    z[19]=z[6]*z[19];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[6]*z[19];
    z[18]=z[18] - n<T>(1,2) + z[19];
    z[15]=z[18]*z[15];
    z[18]=n<T>(1,4) - z[22];
    z[18]=z[6]*z[18];
    z[18]=n<T>(11,6) + z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(7,12) + z[18];
    z[19]=z[6]*z[24];
    z[19]=n<T>(11,12) + z[19];
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,3) + z[19];
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,4) + z[19];
    z[19]=z[3]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[8]*z[18];
    z[19]=n<T>(7,3) - n<T>(11,4)*z[6];
    z[19]=z[6]*z[19];
    z[15]=z[18] + z[15] + n<T>(17,24) + z[19];
    z[15]=z[8]*z[15];
    z[18]= - n<T>(29,6) - z[12];
    z[18]=z[6]*z[18];
    z[18]= - n<T>(7,3) + z[18];
    z[16]=n<T>(1,4)*z[18] + z[16];
    z[16]=z[3]*z[16];
    z[12]=n<T>(43,12) + z[12];
    z[12]=z[6]*z[12];
    z[12]= - n<T>(13,12) + z[12];
    z[12]=n<T>(1,2)*z[12] + z[16];
    z[12]=z[3]*z[12];
    z[12]=z[15] - n<T>(3,4)*z[34] + z[12];
    z[12]=n<T>(1,4)*z[12] + z[14];
    z[12]=z[2]*z[12];

    r += z[12] + n<T>(1,2)*z[13] + z[17] + n<T>(1,8)*z[21] + z[23] + z[29];
 
    return r;
}

template double qqb_2lha_r519(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r519(const std::array<dd_real,30>&);
#endif
