#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1665(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=2*z[3];
    z[7]=2*z[4];
    z[8]= - static_cast<T>(1)+ z[7];
    z[8]=z[8]*z[6];
    z[8]= - 3*z[4] + z[8];
    z[8]=z[3]*z[8];
    z[9]=2*z[2];
    z[6]=static_cast<T>(1)- z[6];
    z[6]=z[6]*z[9];
    z[6]=z[8] + z[6];
    z[6]=z[2]*z[6];
    z[8]=z[3]*z[4];
    z[10]= - static_cast<T>(7)+ 4*z[4];
    z[10]=z[10]*z[8];
    z[11]=npow(z[4],2);
    z[10]=3*z[11] + z[10];
    z[10]=z[3]*z[10];
    z[6]=z[10] + z[6];
    z[6]=z[2]*z[6];
    z[10]= - static_cast<T>(4)+ 5*z[4];
    z[10]=z[10]*z[8];
    z[7]= - z[7] + z[8];
    z[7]=z[7]*z[9];
    z[7]=z[7] + 2*z[11] + z[10];
    z[7]=z[2]*z[3]*z[7];
    z[10]=npow(z[8],2);
    z[7]= - 5*z[10] + z[7];
    z[7]=z[2]*z[7];
    z[12]= - static_cast<T>(1)+ z[2];
    z[12]=z[1]*npow(z[2],2)*z[10]*z[12];
    z[7]=z[7] + 2*z[12];
    z[7]=z[1]*z[7];
    z[6]=z[7] - 4*z[10] + z[6];
    z[6]=z[1]*z[6];
    z[7]=z[4]*z[5];
    z[7]= - static_cast<T>(2)+ z[7];
    z[7]=z[7]*z[8];
    z[7]=z[11] + z[7];
    z[7]=z[3]*z[7];
    z[8]=z[5] - 1;
    z[9]= - z[8]*z[9];
    z[9]= - 3*z[3] + z[9];
    z[9]=z[2]*z[9];
    z[10]= - static_cast<T>(1)+ z[4];
    z[10]=z[10]*npow(z[3],2);
    z[9]=2*z[10] + z[9];
    z[9]=z[2]*z[9];
    z[6]=z[6] + z[7] + z[9];
    z[6]=z[1]*z[6];
    z[7]= - z[2]*z[8];
    z[7]=z[7] - static_cast<T>(1)- z[3];
    z[7]=z[2]*z[7];

    r += z[6] + z[7];
 
    return r;
}

template double qqb_2lha_r1665(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1665(const std::array<dd_real,30>&);
#endif
