#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1671(const std::array<T,30>& k) {
  T z[8];
  T r = 0;

    z[1]=k[1];
    z[2]=k[7];
    z[3]=k[8];
    z[4]=k[3];
    z[5]=npow(z[4],2);
    z[6]=z[1]*z[2];
    z[7]= - static_cast<T>(1)+ n<T>(1,3)*z[6];
    z[7]=z[1]*z[7];
    z[7]=z[4] + z[7];
    z[7]=z[1]*z[7];
    z[5]= - n<T>(1,3)*z[5] + z[7];
    z[5]=z[3]*z[5];
    z[7]=static_cast<T>(2)- z[6];
    z[7]=z[1]*z[7];
    z[5]=2*z[5] - z[4] + z[7];
    z[5]=z[3]*z[5];
    z[5]=z[5] - static_cast<T>(1)+ z[6];

    r += n<T>(1,3)*z[5]*z[3];
 
    return r;
}

template double qqb_2lha_r1671(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1671(const std::array<dd_real,30>&);
#endif
