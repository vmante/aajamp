#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r106(const std::array<T,30>& k) {
  T z[39];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[17];
    z[6]=k[20];
    z[7]=k[7];
    z[8]=k[12];
    z[9]=k[3];
    z[10]=k[2];
    z[11]=k[11];
    z[12]=k[24];
    z[13]=k[4];
    z[14]=k[14];
    z[15]=npow(z[12],2);
    z[15]=3*z[15];
    z[16]= - static_cast<T>(1)+ n<T>(1,2)*z[10];
    z[16]=z[16]*z[15];
    z[17]=n<T>(5,3)*z[11];
    z[18]=z[17] - z[14];
    z[19]=n<T>(1,8)*z[4];
    z[20]=n<T>(1,4)*z[9];
    z[21]=n<T>(7,6) - z[18];
    z[21]=z[8]*z[21];
    z[16]=z[20] + z[16] + z[21] + z[19] + n<T>(1,4) + z[18];
    z[18]=n<T>(1,2)*z[9];
    z[16]=z[16]*z[18];
    z[21]=z[6] + n<T>(1,3);
    z[22]=n<T>(1,2)*z[5];
    z[23]= - z[22] - z[21];
    z[23]=z[1]*z[23];
    z[24]=z[18] + z[6];
    z[25]=n<T>(1,4)*z[7];
    z[23]=z[23] + z[25] + z[5] - n<T>(11,3) - n<T>(5,4)*z[4] + z[24];
    z[23]=z[1]*z[23];
    z[26]=n<T>(1,2)*z[7];
    z[27]= - n<T>(7,3) + z[26];
    z[28]=static_cast<T>(5)- z[18];
    z[28]=z[9]*z[28];
    z[23]=z[23] + n<T>(1,2)*z[27] + z[28];
    z[23]=z[1]*z[23];
    z[27]=n<T>(1,4)*z[6];
    z[28]=z[27] + n<T>(1,8)*z[5] - n<T>(1,3) - z[19];
    z[28]=z[1]*z[28];
    z[29]=static_cast<T>(3)+ n<T>(5,3)*z[9];
    z[28]=n<T>(1,4)*z[29] + z[28];
    z[28]=z[1]*z[28];
    z[29]=n<T>(1,3)*z[9];
    z[30]= - static_cast<T>(1)- z[29];
    z[30]=z[9]*z[30];
    z[30]=n<T>(13,3) + z[30];
    z[28]=n<T>(1,4)*z[30] + z[28];
    z[28]=z[1]*z[28];
    z[30]=z[9]*z[12];
    z[30]=static_cast<T>(1)+ z[30];
    z[30]=z[9]*z[30];
    z[31]=z[9] - 1;
    z[32]=z[1] - z[31];
    z[32]=z[2]*z[32]*npow(z[1],2);
    z[28]=n<T>(5,12)*z[32] + n<T>(3,4)*z[30] + z[28];
    z[28]=z[2]*z[28];
    z[30]=z[29] - 1;
    z[32]=z[15] + z[30];
    z[32]=z[32]*z[20];
    z[32]=z[32] + n<T>(1,4) + z[12];
    z[32]=z[9]*z[32];
    z[23]=z[28] + n<T>(1,4)*z[23] + n<T>(11,12) + z[32];
    z[23]=z[2]*z[23];
    z[28]=z[5] - 1;
    z[20]=z[20] - n<T>(13,12);
    z[20]=z[8]*z[20];
    z[32]=n<T>(1,4)*z[8];
    z[33]= - z[1]*z[32];
    z[20]=z[33] - n<T>(1,4)*z[28] - z[6] + z[20];
    z[20]=z[1]*z[20];
    z[33]= - static_cast<T>(1)+ z[6];
    z[34]=n<T>(1,3)*z[8];
    z[35]=static_cast<T>(1)- z[34];
    z[35]=z[8]*z[35];
    z[33]=n<T>(1,2)*z[33] + z[35];
    z[33]=z[9]*z[33];
    z[22]=z[22] - static_cast<T>(1)- z[4];
    z[20]=z[20] + z[33] + n<T>(1,2)*z[22] - z[8];
    z[22]=n<T>(1,2)*z[1];
    z[20]=z[20]*z[22];
    z[33]=z[10]*z[11];
    z[19]=n<T>(11,3)*z[33] - z[19] - n<T>(23,12) - z[14];
    z[35]=z[7]*z[10];
    z[33]= - z[11] + z[33];
    z[33]=z[33]*z[35];
    z[36]=z[12]*z[10];
    z[36]=z[35] + n<T>(3,4)*z[36];
    z[37]=z[10] - 1;
    z[37]= - z[37]*z[36];
    z[38]= - static_cast<T>(11)+ 7*z[10];
    z[37]=n<T>(1,4)*z[38] + z[37];
    z[37]=z[12]*z[37];
    z[16]=z[23] + z[20] + z[16] + z[37] - z[32] + n<T>(1,2)*z[19] + z[33];
    z[16]=z[3]*z[16];
    z[19]=z[5] + z[21];
    z[19]=z[19]*z[22];
    z[20]=static_cast<T>(9)- z[25];
    z[19]=z[19] + n<T>(1,2)*z[20] - z[24];
    z[19]=z[1]*z[19];
    z[20]=static_cast<T>(33)- z[7];
    z[21]=n<T>(1,2) + z[15];
    z[21]=z[9]*z[21];
    z[21]=z[21] - static_cast<T>(1)+ n<T>(11,2)*z[12];
    z[21]=z[9]*z[21];
    z[19]=z[19] + n<T>(1,8)*z[20] + z[21];
    z[20]=3*z[6];
    z[21]=z[29] + n<T>(13,3) - z[20];
    z[21]=z[21]*z[22];
    z[23]=static_cast<T>(11)- 5*z[9];
    z[23]=z[9]*z[23];
    z[21]=z[21] - static_cast<T>(1)+ n<T>(1,6)*z[23];
    z[21]=z[21]*z[22];
    z[23]=z[31]*z[22];
    z[24]=z[18] - 1;
    z[24]=z[24]*z[9];
    z[24]=z[24] + n<T>(1,2);
    z[23]=z[23] - z[24];
    z[23]=z[2]*z[1]*z[23];
    z[25]=n<T>(3,2)*z[12] + z[30];
    z[25]=z[9]*z[25];
    z[25]=n<T>(5,2) + z[25];
    z[25]=z[9]*z[25];
    z[21]=n<T>(5,3)*z[23] + z[21] - n<T>(13,12) + z[25];
    z[21]=z[2]*z[21];
    z[19]=n<T>(1,2)*z[19] + z[21];
    z[19]=z[2]*z[19];
    z[21]= - n<T>(11,4) + z[36];
    z[21]=z[12]*z[21];
    z[23]=n<T>(1,4) - z[15];
    z[23]=z[23]*z[18];
    z[25]=n<T>(1,3)*z[1];
    z[29]=n<T>(1,4) - z[8];
    z[29]=z[29]*z[25];
    z[29]=z[29] + n<T>(3,4) - z[34];
    z[29]=z[8]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[6];
    z[28]=n<T>(1,4)*z[28] + z[29];
    z[28]=z[1]*z[28];
    z[17]= - z[17] + static_cast<T>(1)- z[14];
    z[29]= - z[11]*z[35];
    z[16]=z[16] + z[19] + z[28] + z[23] + z[21] + n<T>(1,2)*z[17] + z[29];
    z[16]=z[3]*z[16];
    z[17]= - static_cast<T>(1)+ 3*z[12];
    z[17]=z[17]*z[18];
    z[17]=z[17] + static_cast<T>(1)- n<T>(1,2)*z[13];
    z[17]=z[9]*z[17];
    z[19]=z[24]*z[25];
    z[21]= - z[9]*z[30];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[9]*z[21];
    z[21]=n<T>(1,3) + z[21];
    z[19]=n<T>(1,2)*z[21] + z[19];
    z[19]=z[2]*z[19];
    z[21]=n<T>(1,2)*z[6];
    z[23]= - z[21] + 1;
    z[23]=z[13]*z[23];
    z[20]= - static_cast<T>(5)+ z[20];
    z[20]=n<T>(1,2)*z[20] + z[9];
    z[20]=z[1]*z[20];
    z[17]=5*z[19] + z[20] + z[17] - n<T>(1,2) + z[23];
    z[17]=z[2]*z[17];
    z[19]=static_cast<T>(1)- z[13];
    z[19]=z[19]*z[21];
    z[15]=z[9]*z[15];
    z[15]=z[15] + static_cast<T>(1)+ 7*z[12];
    z[15]=z[15]*z[18];
    z[18]= - n<T>(1,3) + z[6];
    z[18]=z[18]*z[22];
    z[15]=z[17] + z[18] + z[15] - static_cast<T>(1)+ z[19];
    z[15]=z[2]*z[15];
    z[17]=z[8]*z[7];
    z[18]=z[26] - z[17];
    z[18]=z[8]*z[18];
    z[19]= - z[7]*npow(z[8],2)*z[22];
    z[18]=z[18] + z[19];
    z[18]=z[1]*z[18];
    z[17]=z[7] - z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[17]*z[25];

    r += n<T>(1,2)*z[15] + z[16] + z[17] + z[27];
 
    return r;
}

template double qqb_2lha_r106(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r106(const std::array<dd_real,30>&);
#endif
