#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1843(const std::array<T,30>& k) {
  T z[40];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[27];
    z[7]=k[4];
    z[8]=k[2];
    z[9]=k[10];
    z[10]=k[12];
    z[11]=k[7];
    z[12]=9*z[5];
    z[13]=3*z[4];
    z[14]=n<T>(9,2)*z[2] - z[12] + n<T>(37,4) - z[13];
    z[14]=z[3]*z[14];
    z[15]=n<T>(9,4)*z[5];
    z[16]= - n<T>(15,2) + z[11];
    z[14]=z[14] + z[15] + n<T>(1,2)*z[16] + z[13];
    z[14]=z[3]*z[14];
    z[16]=n<T>(1,3)*z[11];
    z[17]=static_cast<T>(2)+ z[11];
    z[17]=z[17]*z[16];
    z[18]=3*z[5];
    z[19]=z[18] + z[4];
    z[20]=n<T>(1,2)*z[11];
    z[21]=static_cast<T>(1)- z[20];
    z[21]=n<T>(3,2)*z[21] - z[19];
    z[21]=z[3]*z[21];
    z[22]=n<T>(3,4)*z[4];
    z[17]=z[21] + z[18] + z[17] + z[22];
    z[17]=z[3]*z[17];
    z[21]=n<T>(1,3)*z[10];
    z[23]=n<T>(1,2)*z[6];
    z[24]=z[21] - z[23];
    z[25]=z[24] - z[16];
    z[25]=z[25]*z[11];
    z[26]=n<T>(3,2)*z[5];
    z[25]=z[25] - z[26];
    z[17]=z[17] + z[25];
    z[17]=z[7]*z[17];
    z[27]= - n<T>(1,4) + z[10];
    z[27]=n<T>(1,3)*z[27] - z[23];
    z[27]=z[11]*z[27];
    z[28]=n<T>(1,2) + z[8];
    z[28]=z[28]*z[26];
    z[29]=n<T>(1,2)*z[4];
    z[30]= - z[6] + n<T>(3,2) + z[10];
    z[14]=z[17] + z[14] + z[28] - z[29] + n<T>(1,2)*z[30] + z[27];
    z[14]=z[7]*z[14];
    z[17]=static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[17]=z[17]*z[26];
    z[27]=n<T>(1,2)*z[2];
    z[28]=z[24]*z[27];
    z[30]=9*z[2];
    z[31]=n<T>(35,2) + z[30];
    z[31]=z[31]*z[27];
    z[32]=z[19] - 4;
    z[31]= - 3*z[32] + z[31];
    z[31]=z[3]*z[31];
    z[31]=z[31] + n<T>(19,12)*z[2] - n<T>(9,2)*z[5] + n<T>(11,3) + n<T>(15,4)*z[4];
    z[31]=z[3]*z[31];
    z[33]= - n<T>(17,3) - 3*z[8];
    z[33]=n<T>(1,2)*z[33] + z[10];
    z[14]=z[14] + z[31] + z[28] + z[17] - z[4] + n<T>(1,2)*z[33] - z[6];
    z[14]=z[7]*z[14];
    z[17]=z[29] + z[26];
    z[28]= - n<T>(1,2) - z[16];
    z[28]=z[11]*z[28];
    z[28]=z[28] - z[17];
    z[31]=n<T>(1,2)*z[3];
    z[28]=z[28]*z[31];
    z[33]=n<T>(1,4)*z[4];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[11]*z[16];
    z[16]=z[28] + z[26] + z[16] + z[33];
    z[16]=z[3]*z[16];
    z[25]=n<T>(1,2)*z[25];
    z[16]=z[25] + z[16];
    z[16]=z[7]*z[16];
    z[28]= - n<T>(1,2) + z[10];
    z[28]=n<T>(1,3)*z[28] - z[23];
    z[28]=z[11]*z[28];
    z[34]=z[19] - 3;
    z[35]=n<T>(5,6)*z[11];
    z[36]= - z[35] - z[34];
    z[36]=z[3]*z[36];
    z[37]=n<T>(15,2)*z[5];
    z[38]=z[37] + n<T>(5,2)*z[4] - static_cast<T>(9)+ n<T>(13,6)*z[11];
    z[36]=n<T>(1,2)*z[38] + z[36];
    z[36]=z[3]*z[36];
    z[38]=n<T>(3,4)*z[5];
    z[39]=z[8]*z[38];
    z[16]=z[16] + z[36] + z[39] - z[33] + z[28] - n<T>(1,4)*z[6] + n<T>(3,4) + 
    z[21];
    z[16]=z[7]*z[16];
    z[28]= - static_cast<T>(25)+ z[11];
    z[12]=z[12] + n<T>(1,3)*z[28] + 9*z[4];
    z[28]=3*z[2];
    z[34]=z[28] - z[34];
    z[34]=z[3]*z[34];
    z[12]=3*z[34] + n<T>(1,2)*z[12] - z[30];
    z[12]=z[12]*z[31];
    z[30]=z[38] - n<T>(3,4);
    z[31]=z[8] + 3;
    z[30]=z[31]*z[30];
    z[20]=z[24]*z[20];
    z[31]=n<T>(1,6)*z[2];
    z[34]=z[31] + n<T>(2,3);
    z[34]=z[10]*z[34];
    z[12]=z[16] + z[12] - z[22] + z[20] - z[6] + z[34] + z[30];
    z[12]=z[7]*z[12];
    z[16]= - z[18] + n<T>(5,3) + 7*z[4];
    z[20]=n<T>(11,6) - z[28];
    z[20]=z[2]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[20]=n<T>(17,6) + z[28];
    z[20]=z[2]*z[20];
    z[20]=z[20] - z[32];
    z[20]=z[3]*z[20];
    z[16]=n<T>(1,2)*z[16] + z[20];
    z[16]=z[3]*z[16];
    z[20]= - n<T>(3,2) + z[24];
    z[20]=z[2]*z[20];
    z[30]=z[26] - n<T>(3,2);
    z[12]=z[12] + z[16] + z[20] - z[22] - n<T>(3,4)*z[6] + z[21] + z[30];
    z[12]=z[7]*z[12];
    z[16]=n<T>(2,3) + n<T>(3,4)*z[2];
    z[16]=z[2]*z[16];
    z[16]=static_cast<T>(1)+ z[16];
    z[16]=z[2]*z[16];
    z[16]=z[16] - z[38] + static_cast<T>(1)- z[33];
    z[16]=z[3]*z[16];
    z[20]=z[6] + 3;
    z[33]=n<T>(1,4)*z[2];
    z[34]=z[20]*z[33];
    z[34]=n<T>(4,3) + z[34];
    z[34]=z[2]*z[34];
    z[36]= - z[26] + static_cast<T>(1)+ z[4];
    z[16]=z[16] + n<T>(1,2)*z[36] + z[34];
    z[16]=z[3]*z[16];
    z[34]=n<T>(3,2)*z[6];
    z[36]=z[21] - z[34];
    z[36]=z[2]*z[36];
    z[29]= - z[29] + z[36];
    z[12]=z[12] + n<T>(1,2)*z[29] + z[16];
    z[12]=z[9]*z[12];
    z[16]=z[20]*z[28];
    z[16]=static_cast<T>(19)+ z[16];
    z[16]=z[16]*z[27];
    z[13]=z[16] - z[37] + n<T>(13,2) + z[13];
    z[16]=n<T>(11,2) + z[28];
    z[16]=z[16]*z[27];
    z[16]=static_cast<T>(4)+ z[16];
    z[16]=z[2]*z[16];
    z[16]=z[16] - z[32];
    z[16]=z[3]*z[16];
    z[13]=n<T>(1,2)*z[13] + z[16];
    z[13]=z[3]*z[13];
    z[16]= - n<T>(5,2)*z[6] + n<T>(3,2) + z[21];
    z[16]=z[2]*z[16];
    z[16]=z[16] - z[4] - z[30];
    z[12]=z[12] + z[14] + n<T>(1,2)*z[16] + z[13];
    z[12]=z[9]*z[12];
    z[13]=static_cast<T>(2)- z[17];
    z[14]=z[1] + 3;
    z[13]=z[14]*z[13];
    z[14]=z[2] + static_cast<T>(3)- n<T>(5,6)*z[1];
    z[14]=z[14]*z[27];
    z[13]=z[14] + z[13];
    z[13]=z[3]*z[13];
    z[14]=n<T>(1,2)*z[1];
    z[16]=static_cast<T>(1)- z[14];
    z[20]=n<T>(1,4)*z[1];
    z[21]=static_cast<T>(1)+ z[20];
    z[21]=z[4]*z[21];
    z[13]=z[13] + z[33] - z[26] + n<T>(1,3)*z[16] + z[21];
    z[13]=z[3]*z[13];
    z[16]=static_cast<T>(1)+ z[8];
    z[16]=z[16]*z[26];
    z[16]=z[16] + n<T>(1,6)*z[11] + z[24];
    z[15]=z[31] - z[15] + static_cast<T>(1)- z[22];
    z[15]=z[3]*z[15];
    z[19]=z[35] + z[19];
    z[15]=n<T>(1,2)*z[19] + z[15];
    z[15]=z[3]*z[15];
    z[15]=z[25] + z[15];
    z[15]=z[7]*z[15];
    z[13]=z[15] + n<T>(1,2)*z[16] + z[13];
    z[13]=z[7]*z[13];
    z[15]=z[14] + 1;
    z[15]=z[15]*z[1];
    z[15]=z[15] + n<T>(3,2);
    z[15]= - z[15]*z[17];
    z[16]=z[1] + 2;
    z[16]=z[16]*z[1];
    z[16]=z[16] + 3;
    z[17]=static_cast<T>(1)- n<T>(7,6)*z[1];
    z[17]=n<T>(1,2)*z[17] + n<T>(1,3)*z[2];
    z[17]=z[2]*z[17];
    z[17]=z[17] + z[16];
    z[17]=z[2]*z[17];
    z[15]=z[17] + z[16] + z[15];
    z[15]=z[3]*z[15];
    z[16]= - n<T>(3,2) - z[1];
    z[14]=z[16]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[14]*z[18];
    z[16]= - z[2] + z[1];
    z[16]=z[23]*z[16];
    z[16]=n<T>(1,3) + z[34] + z[16];
    z[16]=z[16]*z[27];
    z[17]=static_cast<T>(1)- n<T>(1,3)*z[1];
    z[16]=2*z[17] + z[16];
    z[16]=z[2]*z[16];
    z[17]=static_cast<T>(11)+ 7*z[1];
    z[17]=z[1]*z[17];
    z[17]=static_cast<T>(13)+ z[17];
    z[17]=n<T>(1,2)*z[17] + z[4];
    z[14]=z[15] + z[16] + n<T>(1,2)*z[17] + z[14];
    z[14]=z[3]*z[14];
    z[15]= - static_cast<T>(1)- z[1];
    z[15]=z[1]*z[15];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[38];
    z[16]=z[1] - 1;
    z[16]=z[6]*z[16];
    z[16]=n<T>(1,3) + z[16];
    z[16]=z[16]*z[33];
    z[17]=static_cast<T>(2)- z[20];

    r += z[12] + z[13] + z[14] + z[15] + z[16] + n<T>(1,3)*z[17];
 
    return r;
}

template double qqb_2lha_r1843(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1843(const std::array<dd_real,30>&);
#endif
