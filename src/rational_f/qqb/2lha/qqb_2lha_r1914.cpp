#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1914(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[9];
    z[5]=k[4];
    z[6]=z[5] + 4;
    z[6]=z[6]*z[5];
    z[7]=z[5] + 2;
    z[8]=z[7]*z[5];
    z[8]=z[8] + 1;
    z[9]=z[4]*z[8];
    z[9]=z[9] - static_cast<T>(3)- z[6];
    z[9]=z[4]*z[9];
    z[10]=2*z[5];
    z[11]=z[1] - 2;
    z[9]=z[9] + z[10] - z[11];
    z[12]=z[5] + 1;
    z[13]= - z[4]*z[12];
    z[13]=z[13] + static_cast<T>(3)+ z[10];
    z[13]=z[4]*z[13];
    z[13]=z[13] + z[1] - z[7];
    z[14]= - static_cast<T>(3)+ z[4];
    z[14]=z[4]*z[14];
    z[14]=z[14] - z[11];
    z[14]=z[2]*z[14];
    z[13]=3*z[13] + z[14];
    z[13]=z[2]*z[13];
    z[9]=3*z[9] + z[13];
    z[9]=z[2]*z[9];
    z[13]=z[5] + 3;
    z[13]=z[13]*z[5];
    z[14]= - static_cast<T>(3)- z[13];
    z[14]=z[5]*z[14];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[14]=3*z[8] + z[14];
    z[14]=z[4]*z[14];
    z[9]=z[9] + z[14] - 3*z[5] + z[11];
    z[9]=z[3]*z[9];
    z[11]=22*z[4];
    z[7]=z[7]*z[11];
    z[7]= - static_cast<T>(129)+ z[7];
    z[7]=z[4]*z[7];
    z[14]=z[2] - z[5];
    z[15]=41*z[1];
    z[7]=z[7] + static_cast<T>(19)- z[15] + 22*z[14];
    z[7]=z[2]*z[7];
    z[13]= - static_cast<T>(2)- z[13];
    z[13]=z[13]*z[11];
    z[13]=z[13] + static_cast<T>(129)+ 107*z[5];
    z[13]=z[4]*z[13];
    z[13]=z[13] - 41*z[5] - static_cast<T>(52)+ z[15];
    z[7]=2*z[13] + z[7];
    z[7]=z[2]*z[7];
    z[6]=static_cast<T>(5)+ z[6];
    z[6]=z[5]*z[6];
    z[6]=static_cast<T>(2)+ z[6];
    z[6]=z[6]*z[11];
    z[13]= - static_cast<T>(214)- 85*z[5];
    z[13]=z[5]*z[13];
    z[6]=z[6] - static_cast<T>(129)+ z[13];
    z[6]=z[4]*z[6];
    z[6]=22*z[9] + z[7] + z[6] + 104*z[5] + static_cast<T>(63)- z[15];
    z[6]=z[3]*z[6];
    z[7]= - z[8]*z[11];
    z[7]=63*z[12] + z[7];
    z[8]=2*z[4];
    z[7]=z[7]*z[8];
    z[9]=z[12]*z[11];
    z[9]= - static_cast<T>(63)+ z[9];
    z[8]=z[9]*z[8];
    z[9]= - z[10] - static_cast<T>(2)- z[1];
    z[8]=82*z[2] + 41*z[9] + z[8];
    z[8]=z[2]*z[8];
    z[9]=z[5] - z[1];
    z[6]=2*z[6] + z[8] - 41*z[9] + z[7];
    z[6]=z[3]*z[6];
    z[7]=z[2] - z[12];
    z[6]=41*z[7] + z[6];

    r += z[6]*z[3];
 
    return r;
}

template double qqb_2lha_r1914(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1914(const std::array<dd_real,30>&);
#endif
