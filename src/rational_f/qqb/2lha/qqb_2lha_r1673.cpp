#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1673(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[12];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=k[24];
    z[8]=k[3];
    z[9]=n<T>(1,3)*z[6];
    z[10]=n<T>(3,8)*z[7];
    z[11]= - z[10] + n<T>(3,8) + z[9];
    z[11]=z[4]*z[11];
    z[12]=z[2] + z[4];
    z[12]=z[8]*z[7]*z[12];
    z[13]=n<T>(3,8)*z[2];
    z[9]=n<T>(3,8)*z[12] + z[11] - z[10] - z[9] + z[13];
    z[9]=z[8]*z[9];
    z[11]= - z[6]*z[5];
    z[11]= - n<T>(1,8) + z[11];
    z[13]=z[1]*z[13];
    z[10]=z[5]*z[10];
    z[9]=z[9] + z[10] + n<T>(1,3)*z[11] + z[13];
    z[9]=z[3]*z[9];
    z[10]=static_cast<T>(1)- z[7];
    z[10]=z[4]*z[10];
    z[10]=z[12] + z[2] + z[10];
    z[10]=z[8]*z[10];
    z[10]=z[10] - z[2];
    z[11]= - n<T>(1,3) + 3*z[1];
    z[11]=z[4]*z[11];
    z[10]=z[11] + 3*z[10];
    z[9]=n<T>(1,8)*z[10] + z[9];

    r += z[9]*z[3];
 
    return r;
}

template double qqb_2lha_r1673(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1673(const std::array<dd_real,30>&);
#endif
