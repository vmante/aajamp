#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2075(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[5];
    z[9]=k[18];
    z[10]=npow(z[4],2);
    z[11]=npow(z[8],2);
    z[12]=z[10]*z[11];
    z[13]=npow(z[5],2);
    z[14]=z[11]*z[13];
    z[15]=z[12] - z[14];
    z[16]=n<T>(1,4)*z[6];
    z[15]=z[15]*z[16];
    z[17]=n<T>(1,2)*z[5];
    z[18]=z[17] + 1;
    z[19]=static_cast<T>(1)- n<T>(3,2)*z[5];
    z[19]=z[8]*z[19];
    z[19]=z[19] + z[18];
    z[19]=z[8]*z[19];
    z[20]=z[8] + 1;
    z[21]=n<T>(1,2)*z[8];
    z[22]=z[20]*z[21];
    z[23]=z[4]*z[8];
    z[22]=z[22] - z[23];
    z[22]=z[4]*z[22];
    z[19]=z[15] + z[22] + z[9] + z[19];
    z[22]=n<T>(1,2)*z[6];
    z[19]=z[19]*z[22];
    z[24]=n<T>(3,2)*z[8];
    z[25]=z[4] - z[20];
    z[25]=z[4]*z[25];
    z[19]=z[19] + n<T>(3,4)*z[25] + z[24] - n<T>(5,4) + z[2];
    z[19]=z[19]*z[22];
    z[25]=z[1] + 1;
    z[26]=z[4]*z[1];
    z[27]=n<T>(3,2)*z[25] - z[26];
    z[28]=n<T>(1,4)*z[4];
    z[27]=z[27]*z[28];
    z[29]= - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[29]=z[29]*z[2];
    z[30]=z[2] - n<T>(1,2);
    z[31]=z[1]*z[30];
    z[19]=z[19] + z[27] - n<T>(1,4)*z[31] - n<T>(9,8) - z[29];
    z[19]=z[6]*z[19];
    z[27]=n<T>(1,2)*z[2];
    z[31]= - n<T>(5,2) + z[2];
    z[31]=z[31]*z[27];
    z[31]=n<T>(1,8)*z[1] + static_cast<T>(1)+ z[31];
    z[31]=z[1]*z[31];
    z[32]=npow(z[1],2);
    z[33]=n<T>(1,2)*z[4];
    z[34]=z[32]*z[33];
    z[25]= - z[1]*z[25];
    z[25]=z[25] + z[34];
    z[25]=z[25]*z[28];
    z[25]=z[31] + z[25];
    z[31]= - z[5] + z[4];
    z[11]=z[16]*z[11]*z[31];
    z[11]=z[11] + z[24] - z[23];
    z[11]=z[6]*z[11];
    z[27]=z[27] - 1;
    z[31]=z[33] + z[27];
    z[11]=3*z[31] + z[11];
    z[11]=z[11]*z[22];
    z[31]=z[33]*z[1];
    z[29]=n<T>(5,4) + z[29];
    z[29]=z[1]*z[29];
    z[11]=z[11] + z[29] - z[31];
    z[11]=z[6]*z[11];
    z[29]=static_cast<T>(3)- z[2];
    z[29]=z[2]*z[29];
    z[29]=z[4] - static_cast<T>(3)+ z[29];
    z[29]=z[32]*z[29];
    z[11]=n<T>(1,8)*z[29] + z[11];
    z[11]=z[3]*z[11];
    z[11]=z[11] + n<T>(1,2)*z[25] + z[19];
    z[11]=z[3]*z[11];
    z[19]= - static_cast<T>(1)- z[24];
    z[19]=z[13]*z[19];
    z[24]=static_cast<T>(1)+ z[21];
    z[24]=z[24]*z[10];
    z[19]=z[24] + z[19];
    z[19]=z[21]*z[19];
    z[15]= - z[15] + z[9] + z[19];
    z[15]=z[6]*z[15];
    z[19]=z[5] - 1;
    z[19]=z[19]*z[8];
    z[19]=z[19] - z[18];
    z[24]=3*z[8];
    z[25]= - z[19]*z[24];
    z[23]= - 3*z[23] - static_cast<T>(1)+ z[24];
    z[23]=z[23]*z[33];
    z[23]=z[23] + z[25] - static_cast<T>(1)+ z[17];
    z[15]=n<T>(1,2)*z[23] + z[15];
    z[15]=z[6]*z[15];
    z[23]=z[1] - 3;
    z[24]=n<T>(3,2) - z[1];
    z[24]=z[4]*z[24];
    z[24]=z[24] + z[23];
    z[24]=z[24]*z[33];
    z[15]=z[15] + z[24] + z[21] + z[30];
    z[15]=z[6]*z[15];
    z[24]=z[1] - 1;
    z[25]=z[24]*z[26];
    z[23]= - z[1]*z[23];
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[28];
    z[25]= - z[2]*z[27];
    z[15]=z[15] + z[23] - n<T>(1,2)*z[1] - n<T>(1,2) + z[25];
    z[11]=n<T>(1,4)*z[15] + z[11];
    z[11]=z[3]*z[11];
    z[14]=3*z[14];
    z[12]=z[14] - z[12];
    z[12]=z[12]*z[16];
    z[10]=z[20]*z[10];
    z[10]=z[13] - z[10];
    z[15]=z[13]*z[8];
    z[16]= - static_cast<T>(1)- n<T>(3,4)*z[8];
    z[16]=z[16]*z[15];
    z[10]=z[12] + z[16] - n<T>(1,4)*z[10];
    z[10]=z[6]*z[10];
    z[12]= - z[8]*z[19];
    z[16]=z[1] - n<T>(1,2);
    z[19]= - z[4]*z[16];
    z[19]=z[19] - static_cast<T>(1)+ z[21];
    z[19]=z[4]*z[19];
    z[12]=z[12] + z[19];
    z[10]=n<T>(1,2)*z[12] + z[10];
    z[10]=z[6]*z[10];
    z[12]=z[24]*z[31];
    z[12]=z[12] + z[16];
    z[12]=z[4]*z[12];
    z[12]=n<T>(1,2) + z[12];
    z[10]=n<T>(1,2)*z[12] + z[10];
    z[10]=n<T>(1,4)*z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]=z[4]*z[5];
    z[11]= - z[13] + z[11];
    z[11]=z[4]*z[11];
    z[11]=z[14] + z[11];
    z[11]=z[11]*z[22];
    z[12]= - static_cast<T>(1)+ z[7];
    z[12]=z[12]*z[21];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[12]*z[15];
    z[13]=z[1]*z[5];
    z[14]=z[13] - z[17];
    z[15]= - z[4]*z[14];
    z[16]= - z[5]*z[18];
    z[15]=z[16] + z[15];
    z[15]=z[4]*z[15];
    z[11]=z[11] + z[12] + z[15];
    z[11]=z[6]*z[11];
    z[12]= - z[5] + z[13];
    z[12]=z[12]*z[31];
    z[12]=z[12] + z[14];
    z[12]=z[4]*z[12];
    z[11]=z[11] + z[17] + z[12];

    r += z[10] + n<T>(1,8)*z[11];
 
    return r;
}

template double qqb_2lha_r2075(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2075(const std::array<dd_real,30>&);
#endif
