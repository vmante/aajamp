#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1487(const std::array<T,30>& k) {
  T z[67];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[6];
    z[6]=k[17];
    z[7]=k[8];
    z[8]=k[3];
    z[9]=k[7];
    z[10]=k[2];
    z[11]=k[11];
    z[12]=k[5];
    z[13]=k[9];
    z[14]=n<T>(1,2)*z[3];
    z[15]=n<T>(5,2) - z[3];
    z[15]=z[15]*z[14];
    z[16]=n<T>(1,4)*z[8];
    z[17]=z[16] - 1;
    z[15]=z[15] + z[17];
    z[18]=npow(z[3],2);
    z[19]=z[18]*z[2];
    z[20]=n<T>(23,2) - 17*z[3];
    z[20]=z[3]*z[20];
    z[20]=z[20] - 3*z[19];
    z[20]=z[2]*z[20];
    z[21]=static_cast<T>(39)- 25*z[3];
    z[21]=z[3]*z[21];
    z[20]=z[20] - static_cast<T>(3)+ z[21];
    z[21]=n<T>(1,2)*z[2];
    z[20]=z[20]*z[21];
    z[15]=11*z[15] + z[20];
    z[15]=z[2]*z[15];
    z[20]=z[8] - 3;
    z[22]=z[3] - 3;
    z[23]= - z[3]*z[22];
    z[23]=z[23] + z[20];
    z[24]=z[3] - 1;
    z[25]=z[24]*z[3];
    z[26]= - 3*z[25] - z[19];
    z[26]=z[26]*z[21];
    z[27]=z[14] - 1;
    z[28]=z[27]*z[3];
    z[26]=z[26] - static_cast<T>(1)- 3*z[28];
    z[26]=z[2]*z[26];
    z[23]=n<T>(1,2)*z[23] + z[26];
    z[23]=z[4]*z[23]*npow(z[2],2);
    z[15]=z[15] + n<T>(11,2)*z[23];
    z[15]=z[4]*z[15];
    z[23]=static_cast<T>(19)- n<T>(11,2)*z[3];
    z[23]=z[23]*z[14];
    z[15]=z[15] - n<T>(23,3) + z[23];
    z[23]=n<T>(1,8)*z[3];
    z[26]=static_cast<T>(3)- z[14];
    z[26]=z[26]*z[23];
    z[19]=n<T>(5,16)*z[19];
    z[29]=z[18]*z[8];
    z[26]=z[19] + z[26] - z[29];
    z[26]=z[26]*z[21];
    z[30]=static_cast<T>(11)- n<T>(17,4)*z[3];
    z[30]=z[30]*z[23];
    z[31]=n<T>(11,16)*z[18];
    z[32]=z[3] - z[29];
    z[32]=z[8]*z[32];
    z[32]= - z[31] + z[32];
    z[32]=z[8]*z[32];
    z[33]=n<T>(59,96) - z[3];
    z[33]=z[3]*z[33];
    z[32]=z[33] + n<T>(1,6)*z[32];
    z[32]=z[8]*z[32];
    z[26]=z[26] + z[30] + z[32];
    z[26]=z[2]*z[26];
    z[30]= - n<T>(5,24) - z[3];
    z[30]=z[30]*z[14];
    z[32]=n<T>(1,2)*z[8];
    z[33]=z[32]*z[18];
    z[34]=z[33] - z[3];
    z[35]=n<T>(1,3)*z[8];
    z[36]= - z[34]*z[35];
    z[37]= - n<T>(1,3) + n<T>(23,16)*z[18];
    z[36]=n<T>(1,2)*z[37] + z[36];
    z[36]=z[8]*z[36];
    z[30]=z[30] + z[36];
    z[30]=z[8]*z[30];
    z[36]= - 3*z[3] + n<T>(5,3)*z[29];
    z[36]=z[36]*z[16];
    z[34]=z[8]*z[34];
    z[34]=n<T>(1,2) + z[34];
    z[34]=z[7]*z[34];
    z[34]=n<T>(1,6)*z[34] + n<T>(1,3) + z[36];
    z[34]=z[7]*z[34];
    z[15]=n<T>(5,2)*z[34] + z[26] + z[30] + n<T>(1,8)*z[15];
    z[15]=z[13]*z[15];
    z[26]= - static_cast<T>(101)+ 41*z[3];
    z[30]=z[26]*z[14];
    z[30]=static_cast<T>(19)+ z[30];
    z[34]=n<T>(1,16)*z[3];
    z[36]= - z[26]*z[34];
    z[36]= - n<T>(11,16)*z[8] - static_cast<T>(1)+ z[36];
    z[36]=z[8]*z[36];
    z[30]=n<T>(3,8)*z[30] + z[36];
    z[30]=z[8]*z[30];
    z[26]=z[26]*z[3];
    z[36]= - static_cast<T>(17)- n<T>(3,8)*z[26];
    z[30]=n<T>(1,2)*z[36] + z[30];
    z[30]=z[8]*z[30];
    z[22]=z[22]*z[14];
    z[22]=z[22] + 1;
    z[36]=z[17]*z[8];
    z[36]=z[36] + n<T>(3,2);
    z[37]= - z[8]*z[22]*z[36];
    z[37]=z[37] + z[22];
    z[37]=z[8]*z[37];
    z[37]= - n<T>(1,4)*z[22] + z[37];
    z[37]=z[5]*z[37];
    z[26]=static_cast<T>(49)+ z[26];
    z[26]=n<T>(11,2)*z[37] + n<T>(1,16)*z[26] + z[30];
    z[26]=z[5]*z[26];
    z[30]= - static_cast<T>(43)+ n<T>(67,3)*z[3];
    z[37]=5*z[3];
    z[38]=z[30]*z[37];
    z[38]=n<T>(139,3) + z[38];
    z[30]=z[30]*z[3];
    z[39]= - n<T>(19,16)*z[8] + n<T>(1,3) - n<T>(5,32)*z[30];
    z[39]=z[8]*z[39];
    z[38]=n<T>(1,16)*z[38] + z[39];
    z[38]=z[8]*z[38];
    z[30]= - n<T>(49,3) - n<T>(5,4)*z[30];
    z[26]=z[26] + n<T>(1,8)*z[30] + z[38];
    z[26]=z[5]*z[26];
    z[30]= - static_cast<T>(49)+ n<T>(155,4)*z[3];
    z[30]=z[3]*z[30];
    z[38]=n<T>(139,2) - 49*z[3];
    z[38]=z[3]*z[38];
    z[38]=z[38] - n<T>(41,4)*z[8];
    z[38]=z[8]*z[38];
    z[30]=z[30] + z[38];
    z[20]=z[20]*z[8];
    z[20]=z[20] + 3;
    z[20]=z[20]*z[8];
    z[20]=z[20] - 1;
    z[36]=z[8]*z[36];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[8]*z[36];
    z[36]=n<T>(1,4) + z[36];
    z[36]=z[5]*z[36];
    z[36]=n<T>(19,4)*z[20] + 11*z[36];
    z[36]=z[5]*z[36];
    z[38]=z[32] - 1;
    z[39]=z[38]*z[8];
    z[39]=z[39] + n<T>(1,2);
    z[36]=n<T>(41,12)*z[39] + z[36];
    z[36]=z[5]*z[36];
    z[36]= - n<T>(41,24)*z[25] + z[36];
    z[40]=n<T>(1,4)*z[2];
    z[36]=z[36]*z[40];
    z[26]=z[36] + n<T>(1,24)*z[30] + z[26];
    z[26]=z[12]*z[26];
    z[30]= - z[33] + n<T>(13,2) + z[25];
    z[30]=z[8]*z[30];
    z[33]=static_cast<T>(121)- n<T>(293,2)*z[3];
    z[33]=z[3]*z[33];
    z[33]= - static_cast<T>(1)+ z[33];
    z[30]=n<T>(1,16)*z[33] + z[30];
    z[30]=z[30]*z[35];
    z[33]= - n<T>(159,32) + n<T>(17,3)*z[3];
    z[33]=z[3]*z[33];
    z[15]=z[15] + z[26] + z[30] - n<T>(11,8) + z[33];
    z[26]= - static_cast<T>(683)+ n<T>(2465,6)*z[3];
    z[26]=z[26]*z[37];
    z[26]=n<T>(3949,3) + z[26];
    z[30]=n<T>(7,3)*z[8];
    z[33]= - static_cast<T>(1)- n<T>(47,6)*z[3];
    z[33]=z[3]*z[33];
    z[33]= - n<T>(1,3) + z[33];
    z[33]=z[33]*z[30];
    z[36]= - n<T>(3509,36) + 45*z[3];
    z[36]=z[3]*z[36];
    z[36]=n<T>(401,6) + z[36];
    z[33]=n<T>(1,2)*z[36] + z[33];
    z[33]=z[8]*z[33];
    z[26]=n<T>(1,24)*z[26] + z[33];
    z[33]=n<T>(5885,9) - 741*z[3];
    z[33]=z[33]*z[14];
    z[33]=n<T>(473,9) + z[33];
    z[36]= - n<T>(85,3) + 29*z[3];
    z[36]=z[36]*z[14];
    z[36]= - n<T>(1,3) + z[36];
    z[36]=z[36]*z[30];
    z[33]=n<T>(1,8)*z[33] + z[36];
    z[33]=z[8]*z[33];
    z[36]= - n<T>(25,4) + n<T>(31,3)*z[3];
    z[36]=z[36]*z[37];
    z[36]= - n<T>(139,6) + z[36];
    z[33]=n<T>(1,4)*z[36] + z[33];
    z[36]=z[28] + n<T>(1,2);
    z[41]=z[36]*z[8];
    z[42]= - static_cast<T>(2)+ z[3];
    z[42]=z[3]*z[42];
    z[42]= - z[41] + static_cast<T>(1)+ z[42];
    z[42]=z[8]*z[42];
    z[42]=z[42] - z[36];
    z[43]=n<T>(35,3)*z[5];
    z[42]=z[42]*z[43];
    z[33]=n<T>(1,4)*z[33] + z[42];
    z[33]=z[5]*z[33];
    z[26]=n<T>(1,8)*z[26] + z[33];
    z[26]=z[5]*z[26];
    z[33]= - static_cast<T>(47)+ n<T>(169,4)*z[3];
    z[33]=z[33]*z[34];
    z[33]= - n<T>(7,4)*z[29] + static_cast<T>(5)+ z[33];
    z[33]=z[8]*z[33];
    z[42]=static_cast<T>(691)- n<T>(5203,6)*z[3];
    z[42]=z[3]*z[42];
    z[42]=static_cast<T>(233)+ z[42];
    z[33]=n<T>(1,32)*z[42] + z[33];
    z[26]=n<T>(1,3)*z[33] + z[26];
    z[26]=z[5]*z[26];
    z[33]=z[35]*z[18];
    z[42]= - z[27]*z[14];
    z[42]=z[42] - z[33];
    z[44]=7*z[8];
    z[45]=n<T>(19,9) - n<T>(9,2)*z[3];
    z[45]=z[45]*z[44];
    z[46]=static_cast<T>(43)+ n<T>(673,6)*z[3];
    z[45]=n<T>(1,24)*z[46] + z[45];
    z[45]=z[3]*z[45];
    z[46]=z[8] - 1;
    z[47]=z[5]*z[25]*z[46];
    z[45]=n<T>(1,4)*z[45] + n<T>(35,9)*z[47];
    z[45]=z[5]*z[45];
    z[47]=n<T>(1,4)*z[3];
    z[48]=static_cast<T>(365)+ 1349*z[3];
    z[48]=z[48]*z[47];
    z[48]=z[48] + 287*z[29];
    z[45]=n<T>(1,72)*z[48] + z[45];
    z[45]=z[5]*z[45];
    z[48]= - n<T>(25,8)*z[18] + z[33];
    z[45]=n<T>(7,8)*z[48] + z[45];
    z[45]=z[5]*z[45];
    z[48]=z[18]*z[5];
    z[49]=5*z[5];
    z[50]=static_cast<T>(13)- z[49];
    z[50]=z[50]*z[48];
    z[50]= - n<T>(19,2)*z[18] + z[50];
    z[51]=npow(z[5],2);
    z[50]=z[50]*z[51];
    z[50]=n<T>(7,3)*z[50] - n<T>(1,4)*z[18];
    z[50]=z[1]*z[50];
    z[42]=n<T>(1,12)*z[50] + n<T>(1,4)*z[42] + z[45];
    z[42]=z[1]*z[42];
    z[45]=n<T>(1,4) - z[3];
    z[45]=z[3]*z[45];
    z[45]=z[45] + n<T>(1,8)*z[29];
    z[45]=z[45]*z[35];
    z[50]=static_cast<T>(1)+ n<T>(1339,288)*z[3];
    z[50]=z[3]*z[50];
    z[50]=n<T>(7,3) + z[50];
    z[26]=z[42] + z[26] + n<T>(1,4)*z[50] + z[45];
    z[26]=z[1]*z[26];
    z[42]=static_cast<T>(163)- n<T>(1745,24)*z[3];
    z[42]=z[42]*z[14];
    z[42]= - static_cast<T>(25)+ z[42];
    z[45]=n<T>(1,3) + z[47];
    z[45]=z[45]*z[44];
    z[50]=n<T>(3361,2) - 695*z[3];
    z[50]=z[3]*z[50];
    z[50]= - n<T>(3907,2) + z[50];
    z[45]=n<T>(1,96)*z[50] + z[45];
    z[45]=z[8]*z[45];
    z[42]=n<T>(1,2)*z[42] + z[45];
    z[42]=z[42]*z[35];
    z[45]= - n<T>(4405,9) + n<T>(597,2)*z[3];
    z[45]=z[3]*z[45];
    z[45]= - n<T>(3283,18) + z[45];
    z[50]=static_cast<T>(1)- n<T>(5,8)*z[3];
    z[50]=z[3]*z[50];
    z[50]=n<T>(19,8) + 7*z[50];
    z[50]=z[8]*z[50];
    z[45]=n<T>(1,32)*z[45] + n<T>(7,9)*z[50];
    z[45]=z[8]*z[45];
    z[50]=n<T>(529,3) - n<T>(853,8)*z[3];
    z[50]=z[3]*z[50];
    z[50]=n<T>(1687,24) + z[50];
    z[45]=n<T>(1,12)*z[50] + z[45];
    z[45]=z[8]*z[45];
    z[50]=z[35] - 1;
    z[50]=z[50]*z[8];
    z[50]=z[50] + 1;
    z[52]=n<T>(1,3)*z[3];
    z[53]=z[52] - 1;
    z[54]=z[53]*z[3];
    z[55]=z[54] + n<T>(2,3);
    z[56]=z[8]*z[55]*z[50];
    z[55]= - n<T>(1,3)*z[55] + z[56];
    z[56]=35*z[5];
    z[55]=z[55]*z[56];
    z[57]= - n<T>(303,2) + n<T>(821,9)*z[3];
    z[57]=z[3]*z[57];
    z[57]= - n<T>(385,6) + z[57];
    z[45]=z[55] + n<T>(1,32)*z[57] + z[45];
    z[45]=z[5]*z[45];
    z[55]=n<T>(1,9)*z[3];
    z[57]= - static_cast<T>(2851)+ n<T>(4889,4)*z[3];
    z[57]=z[57]*z[55];
    z[57]=n<T>(651,4) + z[57];
    z[42]=z[45] + n<T>(1,16)*z[57] + z[42];
    z[42]=z[5]*z[42];
    z[45]=static_cast<T>(1939)- n<T>(11129,8)*z[3];
    z[45]=z[45]*z[55];
    z[45]= - n<T>(41,2) + z[45];
    z[55]=static_cast<T>(1)- n<T>(23,32)*z[3];
    z[55]=z[55]*z[37];
    z[55]=n<T>(7,8)*z[29] - n<T>(59,8) + z[55];
    z[55]=z[8]*z[55];
    z[57]= - static_cast<T>(275)+ n<T>(2623,6)*z[3];
    z[57]=z[3]*z[57];
    z[57]= - n<T>(545,2) + z[57];
    z[55]=n<T>(1,32)*z[57] + z[55];
    z[55]=z[55]*z[35];
    z[42]=z[42] + n<T>(1,16)*z[45] + z[55];
    z[42]=z[5]*z[42];
    z[45]=n<T>(1,6)*z[3];
    z[55]= - static_cast<T>(1709)+ 2599*z[3];
    z[55]=z[55]*z[45];
    z[55]= - static_cast<T>(47)+ z[55];
    z[57]=z[3] - n<T>(1,3);
    z[58]=z[8]*z[57]*z[23];
    z[59]=static_cast<T>(1)- n<T>(67,32)*z[3];
    z[59]=z[3]*z[59];
    z[59]= - n<T>(13,4) + z[59];
    z[58]=n<T>(1,3)*z[59] + z[58];
    z[58]=z[8]*z[58];
    z[26]=z[26] + z[42] + n<T>(1,192)*z[55] + z[58];
    z[26]=z[1]*z[26];
    z[42]=n<T>(169,8) + 19*z[8];
    z[42]=z[42]*z[35];
    z[42]= - n<T>(107,8) + z[42];
    z[55]=z[50]*z[8];
    z[55]=z[55] - n<T>(1,3);
    z[49]= - z[55]*z[49];
    z[58]=n<T>(173,8) - z[44];
    z[58]=z[8]*z[58];
    z[58]= - n<T>(89,4) + z[58];
    z[58]=z[8]*z[58];
    z[58]=n<T>(61,8) + z[58];
    z[49]=n<T>(1,12)*z[58] + z[49];
    z[49]=z[5]*z[49];
    z[42]=n<T>(1,4)*z[42] + n<T>(7,3)*z[49];
    z[42]=z[5]*z[42];
    z[42]=z[42] + n<T>(1,3) + z[8];
    z[42]=z[5]*z[42];
    z[49]=npow(z[8],2);
    z[58]=n<T>(1,4)*z[49];
    z[59]= - z[18]*z[58];
    z[60]=static_cast<T>(1)- n<T>(145,64)*z[3];
    z[60]=z[3]*z[60];
    z[59]=z[60] + z[59];
    z[60]=z[18]*z[1];
    z[61]=n<T>(1,3)*z[60];
    z[62]= - z[3]*z[57];
    z[62]=z[62] - z[61];
    z[63]=n<T>(1,4)*z[1];
    z[62]=z[62]*z[63];
    z[42]=z[62] + n<T>(1,6)*z[59] + z[42];
    z[42]=z[1]*z[42];
    z[59]=n<T>(43,2) - n<T>(109,3)*z[3];
    z[59]=z[59]*z[23];
    z[62]= - z[25] - z[29];
    z[62]=z[8]*z[62];
    z[31]= - z[31] + z[62];
    z[31]=z[31]*z[35];
    z[31]=z[59] + z[31];
    z[59]= - n<T>(71,8) + z[30];
    z[59]=z[8]*z[59];
    z[59]=n<T>(101,8) + z[59];
    z[59]=z[8]*z[59];
    z[59]= - n<T>(191,24) + z[59];
    z[59]=z[59]*z[35];
    z[59]=n<T>(5,8) + z[59];
    z[62]= - static_cast<T>(2)+ z[32];
    z[62]=z[62]*z[35];
    z[62]=static_cast<T>(1)+ z[62];
    z[62]=z[8]*z[62];
    z[62]= - n<T>(2,3) + z[62];
    z[62]=z[8]*z[62];
    z[62]=n<T>(1,6) + z[62];
    z[62]=z[62]*z[43];
    z[59]=n<T>(17,8)*z[59] + z[62];
    z[59]=z[5]*z[59];
    z[62]=n<T>(121,8) - 31*z[8];
    z[62]=z[8]*z[62];
    z[62]=n<T>(251,4) + z[62];
    z[62]=z[62]*z[35];
    z[62]= - n<T>(125,8) + z[62];
    z[59]=n<T>(1,8)*z[62] + z[59];
    z[59]=z[5]*z[59];
    z[62]=n<T>(193,192) - z[8];
    z[62]=z[8]*z[62];
    z[59]=z[59] - n<T>(1,192) + z[62];
    z[59]=z[5]*z[59];
    z[17]= - z[17]*z[35];
    z[17]= - n<T>(1,2) + z[17];
    z[17]=z[8]*z[17];
    z[17]=n<T>(1,3) + z[17];
    z[17]=z[8]*z[17];
    z[17]= - n<T>(1,12) + z[17];
    z[17]=z[17]*z[43];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] + z[39];
    z[17]=z[17]*z[51];
    z[17]= - n<T>(1,8)*z[18] + z[17];
    z[17]=z[2]*z[17];
    z[17]=z[17] + z[42] + n<T>(1,8)*z[31] + z[59];
    z[17]=z[2]*z[17];
    z[20]=n<T>(1,2)*z[5];
    z[31]=z[20] - 1;
    z[31]=z[31]*z[48];
    z[42]=n<T>(1,2)*z[18];
    z[31]=z[31] + z[42];
    z[43]=z[31]*z[1];
    z[37]=n<T>(499,32) - z[37];
    z[37]=z[5]*z[37]*z[52];
    z[51]= - n<T>(113,32) + n<T>(5,3)*z[3];
    z[51]=z[3]*z[51];
    z[37]=n<T>(113,32)*z[43] + z[51] + z[37];
    z[51]=n<T>(1,3)*z[1];
    z[37]=z[37]*z[51];
    z[59]=n<T>(2,9)*z[3];
    z[37]=z[37] + n<T>(193,128) - z[59];
    z[37]=z[1]*z[37];
    z[62]= - n<T>(995,128)*z[8] + n<T>(995,128) - z[3];
    z[37]=n<T>(1,9)*z[62] + z[37];
    z[37]=z[1]*z[37];
    z[62]=n<T>(1,2)*z[60];
    z[64]=z[2]*z[42];
    z[25]=z[64] + z[25] - z[62];
    z[25]=z[2]*z[25];
    z[25]=z[36] + z[25];
    z[64]=11*z[3];
    z[65]=n<T>(355,9) - z[64];
    z[65]=z[3]*z[65];
    z[65]=z[65] - n<T>(29,9)*z[60];
    z[66]=n<T>(1,2)*z[1];
    z[65]=z[65]*z[66];
    z[25]=z[65] + 11*z[25];
    z[25]=z[2]*z[25];
    z[59]=n<T>(21,64) + z[59];
    z[59]=z[3]*z[59];
    z[59]=z[59] - n<T>(1,9)*z[60];
    z[59]=z[1]*z[59];
    z[65]= - static_cast<T>(5)+ z[3];
    z[59]=n<T>(1,9)*z[65] + z[59];
    z[59]=z[1]*z[59];
    z[25]=n<T>(1,64)*z[25] + n<T>(1,3)*z[38] + z[59];
    z[25]=z[2]*z[25];
    z[38]=4*z[3] - z[60];
    z[38]=z[1]*z[38];
    z[38]=n<T>(2,3)*z[38] + z[24];
    z[38]=z[1]*z[38];
    z[59]=z[8] - 2;
    z[38]= - n<T>(2,3)*z[59] + z[38];
    z[38]=z[1]*z[38];
    z[61]=z[3] - z[61];
    z[61]=z[2]*z[61]*npow(z[1],2);
    z[38]=z[61] + z[38] + z[50];
    z[38]=z[2]*z[38];
    z[38]=z[55] + z[38];
    z[50]=static_cast<T>(1)+ z[3];
    z[50]=z[3]*z[50];
    z[48]=z[50] - z[48];
    z[50]=2*z[1];
    z[48]=z[48]*z[50];
    z[48]= - static_cast<T>(7)+ z[48];
    z[55]=n<T>(1,9)*z[1];
    z[48]=z[48]*z[55];
    z[48]=z[48] + z[46];
    z[48]=z[1]*z[48];
    z[59]= - z[8]*z[59];
    z[59]= - static_cast<T>(1)+ z[59];
    z[48]=n<T>(5,9)*z[59] + z[48];
    z[48]=z[1]*z[48];
    z[38]=z[48] + n<T>(1,3)*z[38];
    z[38]=z[4]*z[38];
    z[25]=z[38] + z[25] + n<T>(1,3)*z[39] + z[37];
    z[25]=z[4]*z[25];
    z[37]= - static_cast<T>(3)+ n<T>(17,2)*z[3];
    z[37]=z[37]*z[23];
    z[19]= - z[19] + z[37] + z[60];
    z[19]=z[19]*z[40];
    z[37]= - n<T>(3,2) + z[3];
    z[37]=z[37]*z[64];
    z[37]=n<T>(97,6) + z[37];
    z[38]= - static_cast<T>(1)+ n<T>(29,32)*z[3];
    z[38]=z[3]*z[38];
    z[38]=z[38] - n<T>(1,4)*z[60];
    z[38]=z[38]*z[55];
    z[19]=z[19] + n<T>(1,32)*z[37] + z[38];
    z[19]=z[19]*z[21];
    z[37]= - static_cast<T>(427)+ n<T>(235,2)*z[3];
    z[37]=z[5]*z[37]*z[14];
    z[38]=static_cast<T>(157)- 61*z[3];
    z[38]=z[3]*z[38];
    z[37]=z[38] + z[37];
    z[37]= - n<T>(161,3)*z[43] + n<T>(1,3)*z[37];
    z[37]=z[5]*z[37];
    z[18]=n<T>(3,4)*z[18] + z[37];
    z[18]=z[18]*z[66];
    z[37]=n<T>(5,8) + n<T>(17,9)*z[3];
    z[37]=z[3]*z[37];
    z[38]=n<T>(137,8) - z[64];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(217,4) + z[38];
    z[38]=z[5]*z[38];
    z[18]=z[18] + z[37] + n<T>(1,3)*z[38];
    z[18]=z[1]*z[18];
    z[37]=z[52] - n<T>(1,2);
    z[37]=z[37]*z[3];
    z[38]= - n<T>(1075,384) - z[37];
    z[18]=n<T>(1,3)*z[38] + n<T>(1,8)*z[18];
    z[18]=z[1]*z[18];
    z[18]=z[25] + z[19] + n<T>(1,6)*z[46] + z[18];
    z[18]=z[4]*z[18];
    z[19]= - z[27]*z[52];
    z[25]=z[53]*z[14];
    z[38]=z[5]*z[25];
    z[39]= - z[31]*z[51];
    z[19]=z[39] + z[19] + z[38];
    z[19]=z[1]*z[5]*z[19];
    z[38]=z[24]*z[14];
    z[39]= - n<T>(1,3) + z[38];
    z[39]=z[5]*z[39];
    z[39]= - z[42] + z[39];
    z[19]=n<T>(1,2)*z[39] + z[19];
    z[19]=z[1]*z[19];
    z[19]= - n<T>(1,3)*z[36] + z[19];
    z[19]=z[1]*z[19];
    z[39]= - z[57]*z[47];
    z[42]=n<T>(1,6)*z[1];
    z[31]= - z[31]*z[42];
    z[43]= - n<T>(1,3) + z[47];
    z[43]=z[5]*z[3]*z[43];
    z[31]=z[31] + z[39] + z[43];
    z[31]=z[1]*z[31];
    z[39]=n<T>(1,6)*z[36];
    z[31]= - z[39] + z[31];
    z[31]=z[1]*z[31];
    z[31]=z[39] + z[31];
    z[31]=z[1]*z[31];
    z[43]=z[3] - n<T>(1,2);
    z[46]=z[43]*z[3];
    z[46]=z[46] + z[62];
    z[46]=z[46]*z[1];
    z[38]=z[46] + z[38];
    z[38]=z[38]*z[2];
    z[46]=z[42]*z[38];
    z[31]=z[31] + z[46];
    z[31]=z[6]*z[31];
    z[19]=z[31] + n<T>(1,6)*z[38] + z[39] + z[19];
    z[19]=z[6]*z[19];
    z[31]=static_cast<T>(727)- n<T>(661,3)*z[3];
    z[31]=z[31]*z[34];
    z[31]=n<T>(185,3) + z[31];
    z[34]= - n<T>(7,9) + z[25];
    z[34]=z[34]*z[44];
    z[31]=n<T>(1,3)*z[31] + z[34];
    z[31]=z[8]*z[31];
    z[34]= - static_cast<T>(475)+ n<T>(451,3)*z[3];
    z[34]=z[34]*z[14];
    z[34]= - static_cast<T>(211)+ z[34];
    z[31]=n<T>(1,8)*z[34] + z[31];
    z[31]=z[8]*z[31];
    z[34]=static_cast<T>(391)- 127*z[3];
    z[23]=z[34]*z[23];
    z[23]=n<T>(263,9) + z[23];
    z[23]=n<T>(1,2)*z[23] + z[31];
    z[23]=z[8]*z[23];
    z[31]= - n<T>(349,2) + n<T>(173,3)*z[3];
    z[31]=z[3]*z[31];
    z[31]= - n<T>(419,6) + z[31];
    z[23]=n<T>(1,24)*z[31] + z[23];
    z[31]=z[3] - 4;
    z[31]=z[31]*z[52];
    z[31]=z[31] + 1;
    z[34]=z[47] - 1;
    z[34]=z[34]*z[52];
    z[34]=z[34] + n<T>(1,4);
    z[38]= - z[8]*z[34];
    z[38]=z[38] + z[31];
    z[38]=z[38]*z[35];
    z[39]=static_cast<T>(2)- z[14];
    z[39]=z[39]*z[52];
    z[38]=z[38] - n<T>(1,2) + z[39];
    z[38]=z[8]*z[38];
    z[31]=n<T>(1,3)*z[31] + z[38];
    z[31]=z[8]*z[31];
    z[31]= - n<T>(1,3)*z[34] + z[31];
    z[31]=z[31]*z[56];
    z[23]=n<T>(1,4)*z[23] + z[31];
    z[23]=z[5]*z[23];
    z[28]= - n<T>(11,6) + z[28];
    z[28]=z[28]*z[30];
    z[30]= - n<T>(97,6) - z[3];
    z[30]=z[3]*z[30];
    z[30]=n<T>(1999,36) + z[30];
    z[28]=n<T>(1,2)*z[30] + z[28];
    z[28]=z[28]*z[32];
    z[30]= - static_cast<T>(27)+ n<T>(1213,96)*z[3];
    z[30]=z[3]*z[30];
    z[28]=z[28] + n<T>(185,96) + z[30];
    z[28]=z[8]*z[28];
    z[30]=static_cast<T>(1645)- 649*z[3];
    z[30]=z[3]*z[30];
    z[30]= - n<T>(4969,6) + z[30];
    z[28]=n<T>(1,24)*z[30] + z[28];
    z[28]=z[8]*z[28];
    z[30]= - static_cast<T>(89)+ n<T>(269,8)*z[3];
    z[30]=z[3]*z[30];
    z[30]=n<T>(1247,24) + z[30];
    z[28]=n<T>(5,12)*z[30] + z[28];
    z[23]=n<T>(1,4)*z[28] + z[23];
    z[23]=z[5]*z[23];
    z[28]= - n<T>(3377,3) + 701*z[3];
    z[28]=z[28]*z[14];
    z[28]= - static_cast<T>(29)+ z[28];
    z[30]=static_cast<T>(11)- n<T>(199,8)*z[3];
    z[30]=z[3]*z[30];
    z[30]=n<T>(133,4) + z[30];
    z[31]= - static_cast<T>(5)+ n<T>(13,4)*z[3];
    z[31]=z[3]*z[31];
    z[31]=n<T>(13,2) + z[31];
    z[31]=z[8]*z[31];
    z[30]=n<T>(1,4)*z[30] + z[31];
    z[30]=z[30]*z[35];
    z[28]=n<T>(1,32)*z[28] + z[30];
    z[28]=z[8]*z[28];
    z[30]=n<T>(3539,4) - 479*z[3];
    z[30]=z[30]*z[52];
    z[30]= - n<T>(249,4) + z[30];
    z[28]=n<T>(1,16)*z[30] + z[28];
    z[23]=n<T>(1,2)*z[28] + z[23];
    z[23]=z[5]*z[23];
    z[28]= - static_cast<T>(1)- z[52];
    z[28]=z[28]*z[47];
    z[30]= - n<T>(1,6)*z[29] + z[36];
    z[30]=z[8]*z[30];
    z[28]=z[30] + n<T>(1,3) + z[28];
    z[28]=z[8]*z[28];
    z[30]= - static_cast<T>(1)- z[54];
    z[30]=n<T>(1,2)*z[30] + z[33];
    z[30]=z[8]*z[30];
    z[29]=static_cast<T>(1)- z[29];
    z[29]=z[29]*z[42];
    z[29]=z[29] - n<T>(1,6) + z[30];
    z[29]=z[1]*z[29];
    z[28]=z[29] + n<T>(1,6) + z[28];
    z[28]=z[1]*z[28];
    z[29]=static_cast<T>(1)- z[45];
    z[29]=z[3]*z[29];
    z[29]= - static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[49];
    z[29]= - n<T>(1,12) + z[29];
    z[27]= - z[8]*z[27];
    z[27]=z[27] - z[63];
    z[27]=z[27]*z[51];
    z[27]=n<T>(1,2)*z[29] + z[27];
    z[27]=z[1]*z[27];
    z[25]=z[25] + n<T>(1,3);
    z[29]=z[25]*z[49];
    z[29]= - n<T>(1,12)*z[24] + z[29];
    z[29]=z[8]*z[29];
    z[27]=z[27] + n<T>(1,24) + z[29];
    z[27]=z[1]*z[27];
    z[29]= - n<T>(1,2) - z[49];
    z[29]=z[29]*z[41];
    z[29]=n<T>(1,2)*z[43] + z[29];
    z[29]=z[8]*z[29];
    z[29]= - n<T>(1,4) + z[29];
    z[27]=n<T>(1,6)*z[29] + z[27];
    z[27]=z[7]*z[27];
    z[29]= - n<T>(1,6) - z[37];
    z[29]=z[8]*z[29];
    z[25]= - n<T>(1,2)*z[25] + z[29];
    z[25]=z[8]*z[25];
    z[29]= - n<T>(1,3) + n<T>(3,4)*z[3];
    z[25]=n<T>(1,2)*z[29] + z[25];
    z[25]=z[8]*z[25];
    z[25]=z[27] + z[28] - n<T>(1,6) + z[25];
    z[25]=z[7]*z[25];
    z[27]=static_cast<T>(11)+ z[3];
    z[27]=z[27]*z[47];
    z[27]= - n<T>(7,4)*z[8] - static_cast<T>(3)+ z[27];
    z[28]=npow(z[8],3);
    z[27]=z[27]*z[28];
    z[29]=z[5]*npow(z[8],4);
    z[22]=z[22]*z[29];
    z[22]=z[27] - n<T>(7,2)*z[22];
    z[22]=z[5]*z[22];
    z[27]= - static_cast<T>(7)+ z[3];
    z[14]=z[27]*z[14];
    z[14]= - 13*z[8] + static_cast<T>(3)+ z[14];
    z[14]=z[14]*z[58];
    z[14]=z[14] + z[22];
    z[14]=z[5]*z[14];
    z[22]= - n<T>(9,2)*z[8] - z[24];
    z[16]=z[22]*z[16];
    z[22]=13*z[28] + 7*z[29];
    z[22]=z[5]*z[22];
    z[22]=n<T>(9,2)*z[49] + z[22];
    z[20]=z[22]*z[20];
    z[20]= - z[8] + z[20];
    z[20]=z[20]*z[21];
    z[14]=z[20] + z[16] + z[14];
    z[14]=z[11]*z[14];
    z[16]=z[1] + n<T>(1,2);
    z[20]=z[50] + 1;
    z[21]=z[4]*z[20];
    z[21]= - n<T>(1,3)*z[21] + z[16];
    z[21]=z[2]*z[21];
    z[20]=z[20]*z[51];
    z[20]=z[20] + z[21];
    z[20]=z[4]*z[20];
    z[21]= - z[1] - z[2];
    z[21]=z[16]*z[21];
    z[20]=z[20] + z[21];
    z[20]=z[4]*z[20];
    z[16]=z[1]*z[16];
    z[16]=z[16] + z[20];
    z[16]=z[9]*z[16];
    z[20]=n<T>(1,4)*z[10] + z[8] - z[40];
    z[20]=z[11]*z[20];
    z[20]= - n<T>(1,4) + z[20];
    z[20]=z[10]*z[20];

    r += n<T>(1,16)*z[14] + n<T>(1,4)*z[15] + n<T>(1,3)*z[16] + z[17] + z[18] + 5*
      z[19] + n<T>(1,32)*z[20] + z[23] + n<T>(5,4)*z[25] + z[26];
 
    return r;
}

template double qqb_2lha_r1487(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1487(const std::array<dd_real,30>&);
#endif
