#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r347(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[2];
    z[2]=k[11];
    z[3]=k[3];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=k[27];
    z[7]=k[13];
    z[8]=k[7];
    z[9]=k[15];
    z[10]=k[5];
    z[11]=k[6];
    z[12]=k[12];
    z[13]=k[14];
    z[14]=n<T>(1,2)*z[5];
    z[15]=z[14] + 1;
    z[16]=5*z[5];
    z[17]= - z[15]*z[16];
    z[17]= - static_cast<T>(1)+ z[17];
    z[18]=npow(z[5],2);
    z[19]=z[18]*z[3];
    z[20]= - static_cast<T>(11)- z[14];
    z[20]=z[5]*z[20];
    z[20]=z[20] - n<T>(13,2)*z[19];
    z[21]=n<T>(1,3)*z[3];
    z[20]=z[20]*z[21];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[20]=n<T>(1,4)*z[5];
    z[22]= - static_cast<T>(1)+ z[14];
    z[22]=z[22]*z[20];
    z[23]= - z[18]*z[21];
    z[24]=z[18]*z[4];
    z[22]= - n<T>(1,24)*z[24] + z[22] + z[23];
    z[22]=z[4]*z[22];
    z[17]=n<T>(1,4)*z[17] + z[22];
    z[17]=z[4]*z[17];
    z[22]=static_cast<T>(11)+ z[5];
    z[22]=z[22]*z[14];
    z[22]=n<T>(25,3) + z[22];
    z[23]=z[3]*z[5];
    z[25]=z[5] + 1;
    z[26]=z[23]*z[25];
    z[27]=static_cast<T>(3)+ n<T>(7,4)*z[5];
    z[27]=z[5]*z[27];
    z[27]=n<T>(1,6)*z[26] + n<T>(1,6) + z[27];
    z[27]=z[3]*z[27];
    z[22]=n<T>(1,4)*z[22] + z[27];
    z[22]=z[3]*z[22];
    z[27]=n<T>(1,3)*z[5];
    z[28]=n<T>(1,2) + z[27];
    z[28]=z[28]*z[23];
    z[29]=n<T>(41,12) + z[5];
    z[29]=z[5]*z[29];
    z[28]=z[28] + n<T>(1,2) + z[29];
    z[28]=z[3]*z[28];
    z[29]=static_cast<T>(1)+ z[27];
    z[29]=z[29]*z[23];
    z[30]=z[4]*z[27];
    z[29]=z[30] + z[29] + static_cast<T>(1)+ n<T>(7,3)*z[5];
    z[30]=n<T>(1,2)*z[4];
    z[29]=z[29]*z[30];
    z[28]=z[29] + z[28] + n<T>(7,3) + n<T>(3,8)*z[5];
    z[28]=z[4]*z[28];
    z[29]= - static_cast<T>(3)- z[14];
    z[29]=z[5]*z[29];
    z[29]=n<T>(3,2) + z[29];
    z[22]=z[28] + n<T>(1,4)*z[29] + z[22];
    z[22]=z[2]*z[22];
    z[28]=n<T>(13,8)*z[5];
    z[29]=z[28] + 2;
    z[31]=z[29]*z[27];
    z[31]=z[31] + n<T>(1,4)*z[19];
    z[31]=z[31]*z[3];
    z[32]= - n<T>(61,8) - 4*z[5];
    z[32]=z[5]*z[32];
    z[32]= - static_cast<T>(1)+ z[32];
    z[32]=n<T>(1,3)*z[32] - z[31];
    z[32]=z[3]*z[32];
    z[33]=n<T>(1,4) - n<T>(2,3)*z[5];
    z[33]=z[5]*z[33];
    z[17]=n<T>(1,2)*z[22] + z[17] + z[32] - n<T>(1,6) + z[33];
    z[17]=z[2]*z[17];
    z[22]= - static_cast<T>(8)- n<T>(23,2)*z[5];
    z[22]=z[22]*z[27];
    z[16]= - n<T>(11,2) - z[16];
    z[16]=z[16]*z[23];
    z[16]=n<T>(1,6)*z[16] - n<T>(1,2) + z[22];
    z[16]=z[3]*z[16];
    z[22]=z[30]*z[18];
    z[27]= - n<T>(7,4) - 2*z[5];
    z[27]=z[27]*z[23];
    z[32]= - static_cast<T>(1)- n<T>(23,4)*z[5];
    z[32]=z[5]*z[32];
    z[27]= - z[22] + z[32] + z[27];
    z[27]=z[4]*z[27];
    z[16]=z[16] + n<T>(1,3)*z[27];
    z[16]=z[4]*z[16];
    z[27]=npow(z[3],2);
    z[20]= - n<T>(1,3) - z[20];
    z[20]=z[5]*z[20];
    z[20]= - n<T>(1,12) + z[20];
    z[20]=z[20]*z[27];
    z[32]= - n<T>(1,3) - z[14];
    z[23]=z[32]*z[23];
    z[23]=z[23] - n<T>(1,6)*z[24];
    z[23]=z[23]*z[30];
    z[20]=z[20] + z[23];
    z[20]=z[4]*z[20];
    z[23]=npow(z[3],3);
    z[15]= - z[5]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[15]*z[23];
    z[15]=n<T>(1,6)*z[15] + z[20];
    z[15]=z[7]*z[15];
    z[20]= - static_cast<T>(37)- 35*z[5];
    z[20]=z[5]*z[20];
    z[20]= - static_cast<T>(11)+ z[20];
    z[20]=n<T>(1,4)*z[20] - z[26];
    z[20]=z[20]*z[27];
    z[15]=5*z[15] + n<T>(1,3)*z[20] + z[16];
    z[15]=z[7]*z[15];
    z[16]= - static_cast<T>(61)- 53*z[5];
    z[16]=z[16]*z[14];
    z[16]= - static_cast<T>(7)+ z[16];
    z[16]=n<T>(1,12)*z[16] - z[31];
    z[16]=z[3]*z[16];
    z[20]=static_cast<T>(1)- z[21];
    z[20]=z[18]*z[20];
    z[20]=n<T>(5,2)*z[20] + n<T>(1,3)*z[24];
    z[20]=z[4]*z[20];
    z[31]=n<T>(1,2) - n<T>(13,3)*z[5];
    z[31]=z[5]*z[31];
    z[20]=z[20] - n<T>(1,3) + z[31];
    z[31]= - n<T>(11,4) + z[5];
    z[31]=z[5]*z[31];
    z[31]=z[31] - n<T>(13,8)*z[19];
    z[21]=z[31]*z[21];
    z[20]=z[21] + n<T>(1,4)*z[20];
    z[20]=z[4]*z[20];
    z[21]=z[25]*z[5];
    z[15]=z[15] + z[20] - n<T>(1,16)*z[21] + z[16];
    z[15]=z[7]*z[15];
    z[16]=z[25]*z[28];
    z[20]=n<T>(3,8)*z[19];
    z[28]= - z[5]*z[29];
    z[28]=z[28] - z[20];
    z[28]=z[3]*z[28];
    z[16]=z[16] + z[28];
    z[16]=z[3]*z[16];
    z[26]=z[26] - z[21];
    z[26]=z[26]*z[2];
    z[28]=z[27]*z[26];
    z[16]=z[16] + n<T>(3,8)*z[28];
    z[16]=z[2]*z[16];
    z[23]=z[7]*z[21]*z[23];
    z[28]= - static_cast<T>(2)- n<T>(5,4)*z[5];
    z[28]=z[5]*z[28];
    z[20]=z[28] - z[20];
    z[20]=z[20]*z[27];
    z[20]=z[20] - n<T>(3,4)*z[23];
    z[20]=z[7]*z[20];
    z[26]= - z[26] + z[18];
    z[26]=z[27]*z[26];
    z[27]=z[3]*z[24];
    z[23]= - z[23] + z[27] + z[26];
    z[23]=z[6]*z[23];
    z[19]=n<T>(7,2)*z[19] + 5*z[24];
    z[16]=n<T>(3,4)*z[23] + z[20] + n<T>(1,4)*z[19] + z[16];
    z[16]=z[6]*z[16];
    z[19]=n<T>(1,4)*z[2] - 1;
    z[19]=z[24]*z[19];
    z[19]= - n<T>(1,4)*z[21] + z[19];
    z[19]=z[2]*z[19];
    z[19]=z[22] + z[19];
    z[19]=z[10]*z[19];
    z[20]=z[21] + z[22];
    z[23]=npow(z[4],2);
    z[20]=z[20]*z[23];
    z[26]=z[7]*npow(z[4],3);
    z[27]=z[18]*z[26];
    z[20]=z[20] - n<T>(1,2)*z[27];
    z[20]=z[7]*z[20];
    z[21]=z[21] + z[24];
    z[27]= - z[21]*z[30];
    z[20]=z[27] + z[20];
    z[20]=z[9]*z[20];
    z[27]=z[1] - static_cast<T>(7)- z[3];
    z[27]= - z[4] + n<T>(1,3)*z[27];
    z[27]=z[1]*z[27]*npow(z[2],2);
    z[19]=z[27] + z[19] + z[20];
    z[20]=z[5] + n<T>(1,2);
    z[20]=z[20]*z[5];
    z[20]=z[20] + z[22];
    z[20]=z[20]*z[4];
    z[14]=z[14]*z[25];
    z[14]=z[20] + z[14];
    z[20]=z[7] + z[10];
    z[14]=z[11]*z[14]*z[20];
    z[20]= - n<T>(3,2)*z[23] - z[26];
    z[20]=z[7]*z[18]*z[20];
    z[20]= - z[22] + z[20];
    z[20]=z[8]*z[20];
    z[14]=z[14] + z[20];
    z[20]=z[21]*z[2];
    z[20]=z[20] - z[24];
    z[18]= - z[18] + z[20];
    z[18]=z[12]*z[18];
    z[20]=z[13]*z[20];

    r +=  - n<T>(1,12) + n<T>(1,8)*z[14] + z[15] + z[16] + z[17] + n<T>(1,6)*z[18]
       + n<T>(1,4)*z[19] + n<T>(1,2)*z[20];
 
    return r;
}

template double qqb_2lha_r347(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r347(const std::array<dd_real,30>&);
#endif
