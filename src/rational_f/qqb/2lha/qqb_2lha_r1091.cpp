#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1091(const std::array<T,30>& k) {
  T z[34];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[2];
    z[7]=k[7];
    z[8]=k[11];
    z[9]=k[4];
    z[10]=k[8];
    z[11]=k[10];
    z[12]=k[5];
    z[13]=k[18];
    z[14]=k[12];
    z[15]=n<T>(1,4)*z[11];
    z[16]=n<T>(1,3)*z[7];
    z[17]=z[15] - z[16];
    z[18]=n<T>(1,2)*z[9];
    z[19]=z[18] + 1;
    z[17]=z[17]*z[19];
    z[19]=3*z[5];
    z[20]=z[19] - n<T>(1,4);
    z[21]=z[20]*z[5];
    z[22]=z[6]*z[7];
    z[23]=n<T>(1,2)*z[22];
    z[24]=n<T>(1,8)*z[2];
    z[25]=z[11]*z[24];
    z[17]=z[25] + z[21] + z[23] + z[17];
    z[17]=z[17]*z[18];
    z[25]=n<T>(1,2)*z[11];
    z[26]= - n<T>(1,3) + z[25];
    z[26]=n<T>(5,2)*z[26] - z[16];
    z[22]= - n<T>(1,4)*z[22] + n<T>(1,16) + z[16];
    z[22]=z[6]*z[22];
    z[27]=static_cast<T>(11)+ n<T>(1,2)*z[6];
    z[28]= - z[6]*z[19];
    z[27]=n<T>(1,2)*z[27] + z[28];
    z[28]=n<T>(1,4)*z[5];
    z[27]=z[27]*z[28];
    z[17]=z[17] - n<T>(1,16)*z[2] + z[27] + n<T>(1,4)*z[26] + z[22];
    z[17]=z[9]*z[17];
    z[22]=z[2] - z[6];
    z[26]=n<T>(3,4)*z[13];
    z[27]=z[26] - z[10];
    z[29]=n<T>(5,8) - z[27];
    z[29]=z[11]*z[29];
    z[22]= - z[26] + z[29] - n<T>(1,12)*z[22];
    z[22]=z[2]*z[22];
    z[26]=n<T>(1,3)*z[6];
    z[23]=z[23] - n<T>(1,4) - z[7];
    z[23]=z[23]*z[26];
    z[23]=z[23] + n<T>(1,6)*z[7] + n<T>(11,24) - z[10];
    z[23]=z[6]*z[23];
    z[22]=z[22] + z[23] + z[25] + static_cast<T>(1)- z[27];
    z[23]=n<T>(1,2)*z[5];
    z[26]=n<T>(17,2) - z[19];
    z[26]=z[26]*z[23];
    z[27]= - n<T>(1,6) + z[21];
    z[27]=z[27]*z[18];
    z[29]=n<T>(1,4)*z[4];
    z[24]=z[27] - z[24] + z[26] - n<T>(1,3) + z[29];
    z[24]=z[9]*z[24];
    z[26]=z[4] + z[12];
    z[27]=npow(z[2],2);
    z[30]= - z[27] - static_cast<T>(21)- z[26];
    z[24]=n<T>(1,8)*z[30] + z[24];
    z[24]=z[9]*z[24];
    z[30]=z[4] + n<T>(11,2)*z[5];
    z[31]= - n<T>(71,3) + z[12];
    z[31]= - n<T>(5,6)*z[2] + n<T>(1,4)*z[31] - z[30];
    z[23]=static_cast<T>(1)- z[23];
    z[23]=z[23]*z[19];
    z[32]= - n<T>(1,3) + n<T>(1,2)*z[4];
    z[23]=z[23] + z[32];
    z[23]=z[9]*z[23];
    z[23]=n<T>(1,2)*z[31] + z[23];
    z[23]=z[9]*z[23];
    z[31]=n<T>(1,4)*z[2];
    z[33]=static_cast<T>(1)- n<T>(1,3)*z[2];
    z[33]=z[33]*z[31];
    z[23]=z[33] + z[23];
    z[23]=z[9]*z[23];
    z[33]=n<T>(5,3)*z[2];
    z[32]= - 5*z[32] - n<T>(23,2)*z[5];
    z[32]=z[9]*z[32];
    z[32]=z[33] + z[32];
    z[32]=z[3]*z[32]*npow(z[9],2);
    z[23]=z[23] + n<T>(1,4)*z[32];
    z[23]=z[3]*z[23];
    z[32]=static_cast<T>(1)- z[2];
    z[32]=z[32]*z[27];
    z[23]=z[23] + n<T>(1,12)*z[32] + z[24];
    z[24]=n<T>(1,2)*z[3];
    z[23]=z[23]*z[24];
    z[17]=z[23] + n<T>(1,2)*z[22] + z[17];
    z[17]=z[8]*z[17];
    z[22]=n<T>(1,4)*z[12];
    z[23]= - n<T>(1,2) + z[13];
    z[23]= - z[16] + z[25] + z[22] + n<T>(3,2)*z[23] + z[14];
    z[25]=z[16]*z[6];
    z[32]= - n<T>(1,6) + z[7];
    z[32]=n<T>(1,2)*z[32] - z[25];
    z[32]=z[6]*z[32];
    z[23]=n<T>(5,12)*z[2] + n<T>(1,2)*z[23] + z[32];
    z[20]=z[20]*z[28];
    z[32]= - n<T>(1,8)*z[11] - z[16];
    z[32]=z[32]*z[18];
    z[15]= - z[7] + n<T>(1,3) + z[15];
    z[15]=z[32] + z[20] + n<T>(1,4)*z[15] + z[25];
    z[15]=z[9]*z[15];
    z[20]=static_cast<T>(3)+ z[33];
    z[20]=z[20]*z[31];
    z[25]=n<T>(1,2)*z[12];
    z[31]=n<T>(7,2)*z[4] - z[25] + n<T>(1,3) - n<T>(1,2)*z[14];
    z[32]=static_cast<T>(1)- z[5];
    z[32]=z[5]*z[32];
    z[31]=n<T>(1,6)*z[2] + n<T>(1,2)*z[31] + 9*z[32];
    z[31]=z[31]*z[18];
    z[20]=z[31] + z[20] + static_cast<T>(7)+ z[22] - z[30];
    z[20]=z[9]*z[20];
    z[30]= - n<T>(1,2) + z[2];
    z[30]=z[30]*z[27];
    z[31]= - n<T>(69,2)*z[5] + n<T>(131,3) - n<T>(15,2)*z[4];
    z[31]=n<T>(1,2)*z[31] + z[33];
    z[31]=z[9]*z[31];
    z[27]=n<T>(5,3)*z[27] + z[31];
    z[27]=z[3]*z[27]*z[18];
    z[20]=z[27] + n<T>(1,3)*z[30] + z[20];
    z[20]=z[3]*z[20];
    z[25]=z[29] + z[25] - static_cast<T>(1)+ n<T>(3,4)*z[14];
    z[21]=n<T>(1,6) + z[21];
    z[18]=z[21]*z[18];
    z[21]=n<T>(23,8) - z[19];
    z[21]=z[5]*z[21];
    z[18]=z[18] + n<T>(7,12)*z[2] + n<T>(1,2)*z[25] + z[21];
    z[18]=z[9]*z[18];
    z[21]= - static_cast<T>(1)+ 3*z[2];
    z[21]=z[2]*z[21];
    z[21]= - n<T>(1,2)*z[26] + z[21];
    z[18]=z[20] + n<T>(1,4)*z[21] + z[18];
    z[18]=z[18]*z[24];
    z[15]=z[17] + z[18] + n<T>(1,2)*z[23] + z[15];
    z[15]=z[8]*z[15];
    z[17]=n<T>(1,2) + z[1];
    z[18]= - static_cast<T>(1)- n<T>(1,4)*z[1];
    z[18]=z[4]*z[18];
    z[20]=z[19]*z[1];
    z[21]= - z[20] - n<T>(11,4) - 3*z[1];
    z[21]=z[5]*z[21];
    z[23]= - n<T>(31,3)*z[2] + n<T>(149,12)*z[1] + n<T>(13,3) + z[22];
    z[23]=z[2]*z[23];
    z[17]=z[23] + z[21] + n<T>(15,2)*z[17] + z[18];
    z[18]=n<T>(1,2) - z[5];
    z[18]=z[18]*z[19];
    z[19]=n<T>(3,4)*z[4] + n<T>(13,3) - z[22];
    z[21]=z[9]*z[14];
    z[18]=n<T>(1,8)*z[21] - n<T>(43,24)*z[2] + n<T>(1,2)*z[19] + z[18];
    z[18]=z[9]*z[18];
    z[19]=7*z[2];
    z[21]=static_cast<T>(5)+ 47*z[1];
    z[21]=n<T>(1,6)*z[21] - z[19];
    z[21]=z[2]*z[21];
    z[21]=7*z[1] + z[21];
    z[21]=z[2]*z[21];
    z[22]= - static_cast<T>(7)+ n<T>(5,4)*z[4] + n<T>(23,4)*z[5];
    z[23]= - z[1]*z[22];
    z[21]=z[21] + z[23];
    z[19]=n<T>(47,3) - z[19];
    z[19]=z[2]*z[19];
    z[19]=n<T>(1,2)*z[19] - z[22];
    z[19]=z[9]*z[19];
    z[19]=n<T>(1,2)*z[21] + z[19];
    z[19]=z[3]*z[19];
    z[17]=z[19] + n<T>(1,2)*z[17] + z[18];
    z[17]=z[17]*z[24];
    z[18]= - static_cast<T>(11)- z[1];
    z[18]=n<T>(1,4)*z[18] - z[20];
    z[18]=z[18]*z[28];
    z[19]=n<T>(1,3) - n<T>(5,4)*z[14];
    z[19]=z[9]*z[19];
    z[17]=z[17] + n<T>(1,4)*z[19] - n<T>(3,4)*z[2] + z[18] - n<T>(1,16)*z[4] + n<T>(55,48)*z[1] + static_cast<T>(1)+ n<T>(3,16)*z[12];
    z[17]=z[3]*z[17];
    z[18]= - z[9] + z[6] - 1;
    z[16]=z[16]*z[18];
    z[16]=n<T>(1,3) - n<T>(7,4)*z[14] + z[16];

    r += z[15] + n<T>(1,4)*z[16] + z[17];
 
    return r;
}

template double qqb_2lha_r1091(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1091(const std::array<dd_real,30>&);
#endif
