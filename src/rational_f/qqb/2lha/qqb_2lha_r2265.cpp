#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2265(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[3];
    z[7]=z[6] - 1;
    z[8]=z[5] - 5;
    z[8]= - z[8]*z[7];
    z[9]=z[7]*z[3];
    z[10]=4*z[3];
    z[11]= - static_cast<T>(9)+ z[10];
    z[11]=z[11]*z[9];
    z[12]=3*z[4];
    z[13]=z[3]*z[4];
    z[14]=z[12] - 4*z[13];
    z[14]=z[3]*z[14];
    z[15]=z[1]*npow(z[13],2);
    z[14]=z[14] - z[15];
    z[14]=z[1]*z[14];
    z[16]=static_cast<T>(3)- 2*z[3];
    z[16]=z[3]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[14]=3*z[16] + z[14];
    z[14]=z[1]*z[14];
    z[8]=z[14] + z[11] + z[8];
    z[8]=z[1]*z[8];
    z[11]=z[3] - 3;
    z[14]=z[11]*z[3];
    z[14]=z[14] - z[5] + 2;
    z[16]=z[6] - 2;
    z[16]=z[16]*z[6];
    z[16]=z[16] + 1;
    z[14]=z[16]*z[14];
    z[8]=z[8] - z[14];
    z[8]=z[2]*z[8];
    z[16]= - static_cast<T>(2)+ z[3];
    z[16]=z[16]*z[9];
    z[17]=static_cast<T>(3)- z[6];
    z[17]=z[6]*z[17];
    z[8]=z[8] + z[16] - static_cast<T>(2)+ z[17];
    z[16]=2*z[4];
    z[17]= - static_cast<T>(1)- z[16];
    z[17]=z[17]*z[16];
    z[18]=static_cast<T>(4)- z[4];
    z[18]=z[18]*z[13];
    z[17]=z[17] + 3*z[18];
    z[17]=z[3]*z[17];
    z[17]=z[17] + 4*z[15];
    z[17]=z[1]*z[17];
    z[18]=z[4] - 2;
    z[19]=static_cast<T>(6)- 5*z[4];
    z[19]=z[3]*z[19];
    z[19]=z[19] + z[18];
    z[19]=z[3]*z[19];
    z[19]=z[19] - static_cast<T>(1)+ z[4];
    z[17]=2*z[19] + z[17];
    z[17]=z[1]*z[17];
    z[10]= - z[10] + 2;
    z[10]=z[6]*z[10];
    z[10]= - static_cast<T>(1)+ z[10];
    z[10]=z[3]*z[10];
    z[10]=z[17] + z[10] - static_cast<T>(1)+ 4*z[6];
    z[10]=z[1]*z[10];
    z[8]=z[10] + 2*z[8];
    z[8]=z[1]*z[8];
    z[8]=z[8] - z[14];
    z[8]=z[2]*z[8];
    z[10]=z[12] - 2;
    z[14]= - z[10]*z[16];
    z[17]=static_cast<T>(14)- z[4];
    z[17]=z[4]*z[17];
    z[17]= - static_cast<T>(2)+ z[17];
    z[17]=z[3]*z[17];
    z[14]=z[14] + z[17];
    z[14]=z[3]*z[14];
    z[10]=z[10]*z[13];
    z[17]=npow(z[4],2);
    z[19]=2*z[17];
    z[10]=z[19] + z[10];
    z[10]=z[3]*z[10];
    z[10]=z[10] - z[15];
    z[10]=z[1]*z[10];
    z[10]=2*z[10] - z[19] + z[14];
    z[10]=z[1]*z[10];
    z[14]=z[3]*z[18];
    z[12]= - 3*z[14] + static_cast<T>(1)- z[12];
    z[12]=z[3]*z[12];
    z[10]=z[12] + z[10];
    z[10]=z[1]*z[10];
    z[12]=static_cast<T>(3)- 2*z[6];
    z[12]=z[3]*z[12];
    z[12]=z[12] - static_cast<T>(5)+ z[6];
    z[12]=z[3]*z[12];
    z[10]=z[10] + z[12] + static_cast<T>(2)+ z[6];
    z[10]=z[1]*z[10];
    z[9]= - z[11]*z[9];
    z[11]=z[5] - 1;
    z[12]=z[5] - z[6];
    z[12]=z[6]*z[12];
    z[8]=z[8] + z[10] + z[9] + z[12] - z[11];
    z[8]=z[2]*z[8];
    z[9]=z[4]*z[5];
    z[10]=static_cast<T>(3)+ z[9];
    z[10]=z[10]*z[16];
    z[12]=static_cast<T>(3)- z[5];
    z[12]=z[4]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[3]*z[12];
    z[10]=z[10] + z[12];
    z[10]=z[3]*z[10];
    z[11]= - z[4]*z[11];
    z[11]= - static_cast<T>(4)+ z[11];
    z[11]=z[11]*z[13];
    z[11]=7*z[17] + z[11];
    z[11]=z[3]*z[11];
    z[11]=z[11] - 3*z[15];
    z[11]=z[1]*z[11];
    z[10]=z[11] - 5*z[17] + z[10];
    z[10]=z[1]*z[10];
    z[7]=z[4]*z[7];
    z[7]= - static_cast<T>(1)+ z[7];
    z[7]=z[4]*z[7];
    z[9]=static_cast<T>(2)+ z[9];
    z[9]=z[3]*z[9];
    z[7]=z[10] + z[9] + static_cast<T>(1)+ z[7];
    z[7]=z[1]*z[7];

    r += z[3] - z[6] + z[7] + z[8];
 
    return r;
}

template double qqb_2lha_r2265(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2265(const std::array<dd_real,30>&);
#endif
