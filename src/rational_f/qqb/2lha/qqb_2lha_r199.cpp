#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r199(const std::array<T,30>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[6];
    z[6]=k[8];
    z[7]=k[20];
    z[8]=k[13];
    z[9]=k[2];
    z[10]=k[3];
    z[11]=k[11];
    z[12]=n<T>(1,4)*z[4];
    z[13]=5*z[2];
    z[14]=n<T>(1,4) + z[13];
    z[14]=n<T>(1,3)*z[14] - z[12];
    z[14]=z[3]*z[14];
    z[15]=n<T>(1,2)*z[4];
    z[13]=n<T>(1,4) - z[13];
    z[13]=z[14] + n<T>(1,3)*z[13] + z[15];
    z[14]=n<T>(1,4)*z[3];
    z[13]=z[13]*z[14];
    z[16]=n<T>(1,2)*z[3];
    z[17]= - n<T>(53,6) - z[4];
    z[17]=z[17]*z[16];
    z[17]=z[17] + n<T>(5,3) + z[15];
    z[17]=z[17]*z[14];
    z[18]=n<T>(1,16)*z[4];
    z[19]=npow(z[3],2);
    z[20]= - z[1]*z[19]*z[18];
    z[21]=z[7] - n<T>(1,2);
    z[21]=z[21]*z[7];
    z[17]=z[20] + z[17] - n<T>(3,16) + z[21];
    z[17]=z[1]*z[17];
    z[20]= - n<T>(5,2) + z[2];
    z[22]=static_cast<T>(1)- z[2];
    z[22]=z[7]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[7]*z[20];
    z[22]= - n<T>(17,3) + z[10];
    z[13]=z[17] + z[13] - z[18] + n<T>(1,16)*z[22] + z[20];
    z[17]=n<T>(1,2)*z[7];
    z[20]=n<T>(1,24)*z[3];
    z[22]= - z[20] - z[17];
    z[22]=z[2]*z[22];
    z[23]=static_cast<T>(4)- n<T>(7,8)*z[2];
    z[24]= - n<T>(2,3) - n<T>(1,8)*z[2];
    z[24]=z[10]*z[24];
    z[23]=n<T>(1,3)*z[23] + z[24];
    z[23]=z[10]*z[23];
    z[24]=3*z[7];
    z[25]=z[24] + z[14];
    z[26]= - static_cast<T>(23)+ n<T>(7,2)*z[10];
    z[26]=n<T>(1,6)*z[26] + z[25];
    z[27]=n<T>(1,2)*z[1];
    z[26]=z[26]*z[27];
    z[28]= - static_cast<T>(2)+ n<T>(23,8)*z[2];
    z[22]=z[26] + n<T>(1,3)*z[28] + z[23] + z[22];
    z[22]=z[5]*z[22];
    z[23]=n<T>(1,2) + z[2];
    z[26]= - z[7]*z[2];
    z[23]=n<T>(1,2)*z[23] + z[26];
    z[23]=z[7]*z[23];
    z[26]=n<T>(1,8)*z[3];
    z[28]= - n<T>(13,6)*z[3] + n<T>(3,2);
    z[28]=z[2]*z[28];
    z[28]=n<T>(1,3) + z[28];
    z[28]=z[28]*z[26];
    z[29]= - n<T>(13,3) - n<T>(1,2)*z[2];
    z[29]=n<T>(1,2)*z[29] + z[10];
    z[29]=z[10]*z[29];
    z[29]= - n<T>(17,12)*z[2] + z[29];
    z[23]=z[28] + n<T>(1,4)*z[29] + z[23];
    z[28]= - n<T>(1,3) + n<T>(13,16)*z[3];
    z[28]=z[28]*z[16];
    z[29]=n<T>(29,6) - 3*z[10];
    z[30]= - static_cast<T>(1)+ n<T>(3,2)*z[7];
    z[30]=z[7]*z[30];
    z[28]=z[28] + n<T>(1,16)*z[29] + z[30];
    z[28]=z[1]*z[28];
    z[22]=z[22] + n<T>(1,2)*z[23] + z[28];
    z[22]=z[5]*z[22];
    z[13]=n<T>(1,2)*z[13] + z[22];
    z[13]=z[5]*z[13];
    z[22]=z[11]*npow(z[9],2);
    z[23]=z[9] - z[22];
    z[28]=z[9] - 1;
    z[23]=z[16]*z[28]*z[23];
    z[29]= - static_cast<T>(1)- n<T>(25,4)*z[9];
    z[29]=z[11]*z[29];
    z[29]=z[29] + n<T>(25,4);
    z[29]=z[9]*z[29];
    z[23]=z[23] + static_cast<T>(1)+ z[29];
    z[29]=n<T>(1,6)*z[3];
    z[23]=z[23]*z[29];
    z[30]=n<T>(1,2)*z[8];
    z[31]= - n<T>(1,2)*z[10] + n<T>(5,8) - z[9];
    z[31]=z[11]*z[31];
    z[31]=static_cast<T>(1)- z[30] + z[31];
    z[31]=z[10]*z[31];
    z[32]=n<T>(1,8)*z[4];
    z[33]= - static_cast<T>(1)+ n<T>(7,3)*z[11];
    z[33]=z[33]*z[32];
    z[33]=z[33] - n<T>(1,8) + n<T>(1,3)*z[11];
    z[33]=z[10]*z[33];
    z[34]= - static_cast<T>(1)+ z[11];
    z[33]=n<T>(5,6)*z[34] + z[33];
    z[33]=z[4]*z[33];
    z[34]=n<T>(1,3) + n<T>(1,2)*z[9];
    z[35]= - z[9]*z[34];
    z[35]= - n<T>(19,12) + z[35];
    z[35]=z[11]*z[35];
    z[23]=z[23] + z[33] + z[31] + z[35] + z[34];
    z[31]=z[4]*z[10];
    z[31]=n<T>(3,2) + z[31];
    z[31]=z[31]*z[32];
    z[28]= - z[3]*z[28];
    z[28]= - n<T>(25,2) + z[28];
    z[28]=z[28]*z[20];
    z[33]=3*z[8];
    z[34]= - static_cast<T>(1)+ z[33];
    z[35]=z[7]*z[10];
    z[35]= - n<T>(1,4) + z[35];
    z[35]=z[7]*z[35];
    z[28]=z[28] + z[31] + n<T>(1,4)*z[34] + z[35];
    z[31]=npow(z[7],2);
    z[34]= - z[31] + n<T>(1,48)*z[19];
    z[34]=z[1]*z[34];
    z[28]=n<T>(1,2)*z[28] + z[34];
    z[28]=z[1]*z[28];
    z[34]=n<T>(35,2) - z[3];
    z[34]=z[34]*z[20];
    z[35]= - z[31] - n<T>(13,48)*z[19];
    z[35]=z[1]*z[35];
    z[33]=static_cast<T>(1)+ z[33];
    z[36]= - n<T>(1,4) + z[7];
    z[36]=z[7]*z[36];
    z[33]=z[35] + z[34] + n<T>(1,4)*z[33] + z[36];
    z[33]=z[1]*z[33];
    z[34]= - z[29] - z[10] + n<T>(31,12) - z[8];
    z[33]=n<T>(1,2)*z[34] + z[33];
    z[33]=z[1]*z[33];
    z[34]=z[10] - n<T>(7,2);
    z[34]=z[34]*z[10];
    z[35]=n<T>(3,2) + z[34];
    z[33]=n<T>(1,4)*z[35] + z[33];
    z[35]=n<T>(1,12)*z[3];
    z[17]=z[35] + z[17] + n<T>(13,3) - z[30];
    z[30]=static_cast<T>(1)+ n<T>(13,2)*z[3];
    z[30]=z[30]*z[20];
    z[36]= - static_cast<T>(1)+ z[7];
    z[36]=z[7]*z[36];
    z[30]=z[30] + n<T>(1,4)*z[8] + z[36];
    z[30]=z[1]*z[30];
    z[17]=n<T>(1,2)*z[17] + z[30];
    z[17]=z[1]*z[17];
    z[30]= - n<T>(5,2) - n<T>(13,3)*z[10];
    z[17]=n<T>(1,4)*z[30] + z[17];
    z[17]=z[17]*z[27];
    z[30]=z[7] + z[35];
    z[30]=z[30]*z[27];
    z[30]= - n<T>(2,3) + z[30];
    z[30]=z[5]*z[30]*npow(z[1],2);
    z[17]=z[17] + z[30];
    z[17]=z[5]*z[17];
    z[17]=n<T>(1,2)*z[33] + z[17];
    z[17]=z[5]*z[17];
    z[17]=z[17] + n<T>(1,4)*z[23] + z[28];
    z[17]=z[6]*z[17];
    z[23]= - static_cast<T>(11)+ 9*z[11];
    z[23]=n<T>(1,2)*z[23] + n<T>(5,3)*z[4];
    z[23]=z[23]*z[15];
    z[28]=z[11]*z[9];
    z[22]= - static_cast<T>(1)+ z[22];
    z[22]=z[3]*z[22];
    z[22]=z[22] + static_cast<T>(1)+ n<T>(23,2)*z[28];
    z[22]=z[22]*z[29];
    z[28]=z[10] - n<T>(1,4) + z[9];
    z[28]=z[11]*z[28];
    z[22]=z[22] + z[23] - z[8] + z[28];
    z[23]=n<T>(1,4)*z[1] + n<T>(1,16);
    z[28]=npow(z[4],2);
    z[23]=z[28]*z[23];
    z[23]= - n<T>(1,24)*z[19] + z[31] + z[23];
    z[23]=z[1]*z[23];
    z[22]=n<T>(1,4)*z[22] + z[23];
    z[23]=n<T>(1,3)*z[3];
    z[30]= - n<T>(7,2) + z[23];
    z[26]=z[30]*z[26];
    z[19]=z[31] + n<T>(49,48)*z[19];
    z[19]=z[19]*z[27];
    z[30]= - n<T>(1,2) - z[8];
    z[31]=n<T>(3,4) - z[7];
    z[31]=z[7]*z[31];
    z[19]=z[19] + z[26] + n<T>(1,4)*z[30] + z[31];
    z[19]=z[1]*z[19];
    z[26]= - static_cast<T>(1)- z[8];
    z[24]=n<T>(5,2) - z[24];
    z[24]=z[7]*z[24];
    z[30]=n<T>(5,3) - 13*z[3];
    z[30]=z[3]*z[30];
    z[24]=n<T>(1,16)*z[30] + n<T>(1,4)*z[26] + z[24];
    z[24]=z[1]*z[24];
    z[26]= - n<T>(23,3) - z[10];
    z[26]= - z[29] + n<T>(1,2)*z[26] - z[7];
    z[24]=n<T>(1,2)*z[26] + z[24];
    z[24]=z[1]*z[24];
    z[26]=n<T>(5,2) + z[34];
    z[24]=n<T>(1,4)*z[26] + z[24];
    z[25]=n<T>(23,12) - z[25];
    z[25]=z[25]*z[27];
    z[26]=static_cast<T>(1)- z[10];
    z[25]=n<T>(4,3)*z[26] + z[25];
    z[25]=z[5]*z[1]*z[25];
    z[24]=n<T>(1,2)*z[24] + z[25];
    z[24]=z[5]*z[24];
    z[19]=z[24] + z[19] + z[20] - n<T>(2,3) + n<T>(1,8)*z[10];
    z[19]=z[5]*z[19];
    z[17]=z[17] + n<T>(1,2)*z[22] + z[19];
    z[17]=z[6]*z[17];
    z[19]=n<T>(1,3)*z[2];
    z[20]=z[19] - n<T>(1,8);
    z[22]=z[4]*z[2];
    z[24]=n<T>(1,6)*z[22] + z[20];
    z[24]=z[4]*z[24];
    z[24]= - n<T>(1,3) + z[24];
    z[25]=npow(z[2],2);
    z[12]=z[25]*z[12];
    z[12]= - z[19] + z[12];
    z[12]=z[4]*z[12];
    z[12]=n<T>(1,12) + z[12];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,2)*z[24] + z[12];
    z[12]=z[3]*z[12];
    z[24]=static_cast<T>(1)- z[22];
    z[24]=z[24]*z[32];
    z[25]=z[25]*z[15];
    z[26]= - z[2] + z[25];
    z[26]=z[4]*z[26];
    z[26]=n<T>(1,2) + z[26];
    z[26]=z[26]*z[23];
    z[24]=z[24] + z[26];
    z[24]=z[3]*z[24];
    z[24]=n<T>(1,4)*z[28] + z[24];
    z[24]=z[24]*z[27];
    z[26]= - static_cast<T>(3)+ z[4];
    z[18]=z[26]*z[18];
    z[12]=z[24] + z[18] + z[12];
    z[12]=z[1]*z[3]*z[12];
    z[18]= - z[19] + z[25];
    z[18]=z[3]*z[18];
    z[18]=z[18] + n<T>(17,24)*z[22] + z[20];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(1,6) + z[18];
    z[18]=z[3]*z[18];
    z[19]= - static_cast<T>(1)+ z[15];
    z[19]=z[4]*z[19];
    z[19]=n<T>(1,3) + n<T>(3,2)*z[19];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[16]=z[18]*z[16];
    z[12]=z[16] + z[12];
    z[12]=z[1]*z[12];
    z[16]=n<T>(1,2) + n<T>(5,3)*z[22];
    z[15]=z[16]*z[15];
    z[16]=npow(z[22],2)*z[23];
    z[15]=z[15] + z[16];
    z[15]=z[3]*z[15];
    z[16]= - n<T>(7,4) + z[4];
    z[16]=z[4]*z[16];
    z[15]=z[16] + z[15];
    z[14]=z[15]*z[14];
    z[12]=z[12] + z[21] + z[14];

    r += n<T>(1,2)*z[12] + z[13] + z[17];
 
    return r;
}

template double qqb_2lha_r199(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r199(const std::array<dd_real,30>&);
#endif
