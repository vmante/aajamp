#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1892(const std::array<T,30>& k) {
  T z[47];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[8];
    z[6]=k[13];
    z[7]=k[27];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[10];
    z[11]=k[14];
    z[12]=k[12];
    z[13]=z[8]*z[10];
    z[14]=z[10] + 1;
    z[15]=n<T>(1,2)*z[10];
    z[16]=z[15] + 1;
    z[16]=z[16]*z[10];
    z[17]= - z[4]*z[16];
    z[17]=z[17] - z[13] - z[14];
    z[17]=z[4]*z[17];
    z[18]=n<T>(1,2)*z[8];
    z[19]= - static_cast<T>(1)- z[18];
    z[19]=z[8]*z[19];
    z[17]=z[17] - n<T>(1,2) + z[19];
    z[17]=z[9]*z[17];
    z[19]=3*z[10];
    z[20]=static_cast<T>(7)+ z[19];
    z[20]=z[20]*z[15];
    z[21]=npow(z[10],2);
    z[22]=z[21]*z[8];
    z[20]=z[20] - z[22];
    z[20]=z[4]*z[20];
    z[23]=z[14]*z[10];
    z[24]=static_cast<T>(1)- n<T>(3,2)*z[21];
    z[24]=z[8]*z[24];
    z[17]=z[17] + z[20] + z[24] + static_cast<T>(1)- n<T>(3,2)*z[23];
    z[17]=z[9]*z[17];
    z[20]= - z[10] - z[22];
    z[20]=z[11]*z[20]*z[18];
    z[24]=static_cast<T>(1)+ z[8];
    z[24]=z[9]*z[24];
    z[24]= - static_cast<T>(1)+ z[24];
    z[24]=z[9]*z[24];
    z[25]=n<T>(1,2)*z[1];
    z[26]=z[3]*z[25];
    z[27]=z[2]*npow(z[9],2);
    z[20]= - n<T>(1,2)*z[27] + z[20] + z[26] - n<T>(1,2) + z[24];
    z[20]=z[2]*z[20];
    z[24]=static_cast<T>(1)+ z[19];
    z[24]=z[10]*z[24];
    z[24]=z[24] + z[22];
    z[24]=z[24]*z[18];
    z[24]=z[23] + z[24];
    z[24]=z[8]*z[24];
    z[26]=z[18]*z[21];
    z[23]=z[23] + z[26];
    z[23]=z[23]*z[8];
    z[23]=z[23] + z[16] + n<T>(1,2);
    z[27]= - z[9]*z[23];
    z[24]=z[27] - n<T>(1,2)*z[14] + z[24];
    z[24]=z[11]*z[24];
    z[17]=z[20] + z[17] + z[24];
    z[20]= - static_cast<T>(17)- 15*z[10];
    z[20]=z[20]*z[15];
    z[24]=7*z[10];
    z[27]= - n<T>(1,2) - z[24];
    z[27]=z[10]*z[27];
    z[27]=z[27] + z[26];
    z[27]=z[8]*z[27];
    z[20]=z[27] - static_cast<T>(1)+ z[20];
    z[20]=z[20]*z[18];
    z[27]=3*z[22];
    z[28]=z[10] + n<T>(1,2);
    z[28]=z[28]*z[10];
    z[29]=z[28] + z[27];
    z[29]=z[29]*z[18];
    z[30]=n<T>(3,2)*z[4];
    z[31]=z[28]*z[30];
    z[32]= - static_cast<T>(7)- n<T>(15,4)*z[10];
    z[32]=z[10]*z[32];
    z[29]=z[31] + z[29] - n<T>(5,4) + z[32];
    z[29]=z[4]*z[29];
    z[20]=z[20] + z[29];
    z[20]=z[4]*z[20];
    z[29]=z[10] + 3;
    z[29]=z[29]*z[10];
    z[29]=z[29] - z[4] + 3;
    z[31]=z[6]*npow(z[4],4);
    z[32]=3*z[31];
    z[33]=z[29]*z[32];
    z[34]=npow(z[4],3);
    z[35]=5*z[10];
    z[36]=static_cast<T>(21)+ z[35];
    z[36]=z[10]*z[36];
    z[36]= - 11*z[4] + static_cast<T>(27)+ z[36];
    z[36]=z[36]*z[34];
    z[36]=z[36] + z[33];
    z[37]=n<T>(1,4)*z[6];
    z[36]=z[36]*z[37];
    z[38]= - z[28] - n<T>(1,4)*z[22];
    z[38]=z[8]*z[38];
    z[39]= - static_cast<T>(1)- n<T>(3,4)*z[10];
    z[39]=z[10]*z[39];
    z[38]=z[38] - n<T>(1,4) + z[39];
    z[38]=z[8]*z[38];
    z[39]= - static_cast<T>(5)- z[19];
    z[39]=z[10]*z[39];
    z[39]= - static_cast<T>(1)+ z[39];
    z[40]= - z[8]*z[28];
    z[39]=n<T>(1,2)*z[39] + z[40];
    z[40]=n<T>(1,2)*z[4];
    z[39]=z[39]*z[40];
    z[38]=z[38] + z[39];
    z[38]=z[4]*z[38];
    z[39]=npow(z[8],2);
    z[41]=n<T>(1,2)*z[39];
    z[42]= - z[23]*z[41];
    z[38]=z[42] + z[38];
    z[38]=z[4]*z[38];
    z[29]=z[29]*z[31];
    z[38]=z[38] + n<T>(1,4)*z[29];
    z[38]=z[7]*z[38];
    z[42]=z[23]*z[39];
    z[20]=3*z[38] + z[36] - n<T>(5,2)*z[42] + z[20];
    z[20]=z[7]*z[20];
    z[36]=n<T>(7,4) + z[35];
    z[36]=z[10]*z[36];
    z[36]=z[36] + n<T>(9,4)*z[22];
    z[36]=z[8]*z[36];
    z[38]=7*z[28] - n<T>(3,2)*z[22];
    z[38]=z[38]*z[40];
    z[43]= - n<T>(17,2) - z[19];
    z[43]=z[10]*z[43];
    z[43]= - static_cast<T>(1)+ z[43];
    z[36]=z[38] + n<T>(1,2)*z[43] + z[36];
    z[36]=z[36]*z[40];
    z[38]=static_cast<T>(27)+ z[24];
    z[38]=z[10]*z[38];
    z[38]= - 13*z[4] + static_cast<T>(33)+ z[38];
    z[38]=z[38]*z[34];
    z[33]=z[38] + z[33];
    z[33]=z[33]*z[37];
    z[38]=npow(z[4],2);
    z[43]=static_cast<T>(3)- z[15];
    z[43]=z[10]*z[43];
    z[43]= - n<T>(17,4)*z[4] + n<T>(33,4) + z[43];
    z[43]=z[43]*z[38];
    z[33]=z[43] + z[33];
    z[43]=n<T>(1,2)*z[6];
    z[33]=z[33]*z[43];
    z[44]=n<T>(11,2) - z[10];
    z[44]=z[10]*z[44];
    z[44]=n<T>(1,4)*z[44] + z[22];
    z[44]=z[8]*z[44];
    z[45]= - n<T>(7,2) - z[35];
    z[45]=z[10]*z[45];
    z[45]=n<T>(3,2) + z[45];
    z[44]=n<T>(1,4)*z[45] + z[44];
    z[44]=z[8]*z[44];
    z[20]=n<T>(1,2)*z[20] + z[33] + z[44] + z[36];
    z[20]=z[7]*z[20];
    z[33]=z[3]*z[39];
    z[33]=z[9] + z[33];
    z[23]=z[23]*z[33];
    z[14]= - z[14]*z[15];
    z[33]=static_cast<T>(3)+ z[35];
    z[33]=z[10]*z[33];
    z[33]=z[33] + z[27];
    z[33]=z[33]*z[18];
    z[16]=z[16] + z[33];
    z[16]=z[8]*z[16];
    z[33]=2*z[21] + z[22];
    z[33]=z[8]*z[33];
    z[33]=z[21] + z[33];
    z[33]=z[4]*z[33];
    z[14]=z[33] + z[14] + z[16] + z[23];
    z[14]=z[12]*z[14];
    z[16]=static_cast<T>(85)+ 233*z[10];
    z[16]=z[10]*z[16];
    z[16]= - static_cast<T>(241)+ z[16];
    z[23]=n<T>(47,2) + 55*z[10];
    z[23]=z[10]*z[23];
    z[23]=static_cast<T>(5)+ n<T>(1,12)*z[23];
    z[23]=z[8]*z[23];
    z[16]=n<T>(1,24)*z[16] + z[23];
    z[16]=z[16]*z[18];
    z[23]=n<T>(9,2) + n<T>(7,3)*z[10];
    z[23]=z[23]*z[15];
    z[23]=static_cast<T>(1)+ z[23];
    z[33]=n<T>(35,16) + n<T>(1,3)*z[28];
    z[33]=z[8]*z[33];
    z[23]= - n<T>(19,12)*z[4] + 5*z[23] + z[33];
    z[23]=z[4]*z[23];
    z[16]=z[16] + z[23];
    z[16]=z[4]*z[16];
    z[23]=n<T>(1,4)*z[8];
    z[33]= - static_cast<T>(55)- 21*z[10];
    z[33]=z[10]*z[33];
    z[33]= - static_cast<T>(47)+ z[33];
    z[33]=n<T>(1,2)*z[33] + 13*z[8];
    z[33]=z[33]*z[23];
    z[36]=n<T>(1,3)*z[4];
    z[44]=n<T>(1,3)*z[10];
    z[45]= - static_cast<T>(1)- z[44];
    z[45]=z[10]*z[45];
    z[45]=z[36] + n<T>(47,24)*z[8] - static_cast<T>(1)+ z[45];
    z[45]=z[4]*z[45];
    z[33]=z[33] + z[45];
    z[33]=z[4]*z[33];
    z[19]= - n<T>(25,4) - z[19];
    z[19]=z[19]*z[35];
    z[19]=n<T>(19,4)*z[8] - n<T>(39,2) + z[19];
    z[19]=z[19]*z[41];
    z[19]=z[19] + z[33];
    z[19]=z[4]*z[19];
    z[33]= - n<T>(27,4) - n<T>(13,3)*z[10];
    z[33]=z[33]*z[35];
    z[33]=n<T>(3,2)*z[8] - n<T>(57,4) + z[33];
    z[41]=npow(z[8],3);
    z[33]=z[33]*z[41];
    z[19]=n<T>(1,2)*z[33] + z[19];
    z[19]=z[19]*z[43];
    z[33]=n<T>(1,8)*z[8];
    z[45]=static_cast<T>(13)+ n<T>(125,3)*z[10];
    z[45]=z[10]*z[45];
    z[45]=n<T>(15,2) + z[45];
    z[45]=z[45]*z[33];
    z[46]= - static_cast<T>(287)- 29*z[10];
    z[46]=z[10]*z[46];
    z[45]=z[45] - static_cast<T>(5)+ n<T>(1,48)*z[46];
    z[45]=z[45]*z[39];
    z[16]=z[19] + z[45] + z[16];
    z[16]=z[16]*z[37];
    z[19]=n<T>(11,2) - 49*z[10];
    z[19]=z[19]*z[44];
    z[19]= - n<T>(47,6)*z[22] + static_cast<T>(9)+ z[19];
    z[19]=z[19]*z[33];
    z[33]= - n<T>(49,24) + z[10];
    z[33]=z[10]*z[33];
    z[19]=z[19] - n<T>(143,48) + z[33];
    z[19]=z[19]*z[23];
    z[33]= - n<T>(1,4) - z[10];
    z[33]=z[10]*z[33];
    z[33]= - n<T>(1,6)*z[22] + n<T>(31,16) + n<T>(23,3)*z[33];
    z[33]=z[8]*z[33];
    z[37]=n<T>(19,4) + z[10];
    z[37]=z[10]*z[37];
    z[33]=z[33] + n<T>(13,4) + z[37];
    z[28]= - n<T>(5,8) - z[28];
    z[28]=z[4]*z[28];
    z[28]=n<T>(1,4)*z[33] + z[28];
    z[28]=z[4]*z[28];
    z[16]=z[16] + z[19] + z[28];
    z[16]=z[6]*z[16];
    z[19]=25*z[10];
    z[28]=static_cast<T>(11)- z[19];
    z[28]=z[28]*z[15];
    z[19]=static_cast<T>(3)+ z[19];
    z[19]=z[19]*z[13];
    z[19]=z[19] + static_cast<T>(3)+ z[28];
    z[28]=npow(z[8],4);
    z[19]=z[19]*z[28];
    z[33]=npow(z[8],5);
    z[37]= - static_cast<T>(9)- n<T>(31,2)*z[10];
    z[37]=z[6]*z[10]*z[37]*z[33];
    z[19]=z[19] + z[37];
    z[19]=z[19]*z[43];
    z[37]= - n<T>(23,2) + 19*z[10];
    z[37]=z[10]*z[37];
    z[37]=z[37] - n<T>(19,2)*z[22];
    z[18]=z[37]*z[18];
    z[18]=z[18] - static_cast<T>(1)+ n<T>(17,4)*z[10];
    z[18]=z[18]*z[41];
    z[18]=z[18] + z[19];
    z[18]=z[6]*z[18];
    z[19]=static_cast<T>(1)- z[15];
    z[19]=z[10]*z[19];
    z[19]=z[19] + z[22];
    z[19]=z[19]*z[33];
    z[21]= - z[21]*npow(z[8],6)*z[43];
    z[19]=z[19] + z[21];
    z[19]=z[6]*z[19];
    z[21]= - static_cast<T>(1)+ z[10];
    z[21]=z[10]*z[21];
    z[21]=z[21] - z[26];
    z[21]=z[8]*z[21];
    z[21]=z[21] - n<T>(1,2) + z[10];
    z[21]=z[21]*z[28];
    z[19]=z[21] + z[19];
    z[19]=z[6]*z[19];
    z[21]= - z[10] - z[26];
    z[21]=z[8]*z[21];
    z[21]= - n<T>(1,2) + z[21];
    z[21]=z[21]*z[41];
    z[19]=z[21] + z[19];
    z[19]=z[3]*z[19];
    z[21]= - 17*z[10] - 13*z[22];
    z[21]=z[21]*z[23];
    z[21]= - static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[39];
    z[18]=3*z[19] + z[21] + z[18];
    z[18]=z[3]*z[18];
    z[19]=45*z[10];
    z[21]=n<T>(19,2) + z[19];
    z[13]=z[21]*z[13];
    z[19]= - static_cast<T>(41)- z[19];
    z[19]=z[10]*z[19];
    z[19]= - static_cast<T>(23)+ z[19];
    z[13]=n<T>(1,2)*z[19] + z[13];
    z[13]=z[13]*z[41];
    z[19]= - n<T>(15,2) - z[24];
    z[19]=z[19]*z[35];
    z[19]= - static_cast<T>(9)+ z[19];
    z[19]=z[6]*z[19]*z[28];
    z[13]=z[13] + z[19];
    z[13]=z[13]*z[43];
    z[19]= - static_cast<T>(3)+ 13*z[10];
    z[19]=z[10]*z[19];
    z[19]=z[19] - n<T>(13,2)*z[22];
    z[19]=z[8]*z[19];
    z[15]= - static_cast<T>(11)- z[15];
    z[15]=n<T>(1,2)*z[15] + z[19];
    z[15]=z[15]*z[39];
    z[13]=z[15] + z[13];
    z[13]=z[13]*z[43];
    z[15]= - z[10] - n<T>(13,8)*z[22];
    z[15]=z[8]*z[15];
    z[15]= - n<T>(5,8) + z[15];
    z[15]=z[8]*z[15];
    z[13]=z[15] + z[13];
    z[15]= - z[7]*z[42];
    z[13]=n<T>(1,16)*z[18] + n<T>(1,8)*z[13] + z[15];
    z[13]=z[3]*z[13];
    z[15]= - n<T>(31,4)*z[8] - n<T>(5,3)*z[4];
    z[15]=z[4]*z[15];
    z[15]= - n<T>(11,2)*z[39] + z[15];
    z[18]= - n<T>(13,8)*z[8] - z[36];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(13,8)*z[39] + z[18];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(3,4)*z[41] + z[18];
    z[18]=z[6]*z[18];
    z[15]=n<T>(1,2)*z[15] + z[18];
    z[15]=z[6]*z[15];
    z[15]=z[15] - n<T>(27,8)*z[8] - z[4];
    z[15]=z[6]*z[15];
    z[18]= - z[6]*z[41];
    z[18]= - n<T>(3,2)*z[39] + z[18];
    z[18]=z[6]*z[18];
    z[18]=n<T>(3,4)*z[18] - static_cast<T>(1)- n<T>(9,8)*z[8];
    z[18]=z[3]*z[18];
    z[15]=z[18] - n<T>(1,2) + z[15];
    z[18]=11*z[34] + z[32];
    z[18]=z[6]*z[18];
    z[19]=z[34] + z[31];
    z[19]=z[7]*z[19];
    z[18]=n<T>(3,8)*z[19] + z[38] + n<T>(1,8)*z[18];
    z[18]=z[7]*z[18];
    z[19]=13*z[34] + z[32];
    z[19]=z[6]*z[19];
    z[19]=19*z[38] + z[19];
    z[19]=z[6]*z[19];
    z[19]=9*z[4] + z[19];
    z[18]=n<T>(1,8)*z[19] + z[18];
    z[18]=z[7]*z[18];
    z[15]=z[18] + n<T>(1,8)*z[15];
    z[15]=z[1]*z[15];
    z[18]= - static_cast<T>(5)- z[10];
    z[18]=z[10]*z[18];
    z[18]=3*z[4] - static_cast<T>(7)+ z[18];
    z[18]=z[18]*z[34];
    z[18]=z[18] - z[29];
    z[18]=z[18]*z[43];
    z[19]=z[30] - n<T>(5,2) - z[10];
    z[19]=z[19]*z[38];
    z[18]=z[19] + z[18];
    z[18]=z[6]*z[18];
    z[19]= - static_cast<T>(1)+ z[4];
    z[19]=z[19]*z[40];
    z[21]= - 3*z[34] - z[31];
    z[21]=z[6]*z[21];
    z[21]= - 3*z[38] + z[21];
    z[21]=z[6]*z[21];
    z[21]= - z[4] + z[21];
    z[21]=z[21]*z[25];
    z[18]=z[21] + z[19] + z[18];
    z[18]=z[5]*z[18];
    z[19]= - n<T>(7,12) - z[10];
    z[19]=z[10]*z[19];
    z[19]=n<T>(17,16) + z[19];
    z[19]=n<T>(1,2)*z[19] - n<T>(7,3)*z[22];
    z[19]=z[8]*z[19];
    z[21]=n<T>(1,2) - z[27];
    z[21]=z[4]*z[21];
    z[21]=z[21] - n<T>(17,6) + z[10];
    z[19]=z[19] + n<T>(1,4)*z[21];

    r += z[13] + z[14] + z[15] + z[16] + n<T>(1,4)*z[17] + n<T>(3,4)*z[18] + n<T>(1,2)*z[19] + z[20];
 
    return r;
}

template double qqb_2lha_r1892(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1892(const std::array<dd_real,30>&);
#endif
