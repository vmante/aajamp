#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1054(const std::array<T,30>& k) {
  T z[15];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[11];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[10];
    z[7]=k[13];
    z[8]=k[9];
    z[9]= - static_cast<T>(1)+ z[6];
    z[9]=z[6]*z[9];
    z[10]= - z[2]*npow(z[6],2);
    z[9]=z[9] + z[10];
    z[10]=n<T>(1,4)*z[2];
    z[9]=z[9]*z[10];
    z[10]=z[5]*npow(z[7],2);
    z[10]=z[10] - z[7];
    z[11]=static_cast<T>(1)+ n<T>(7,4)*z[6];
    z[11]=z[6]*z[11];
    z[9]=z[9] + z[11] - n<T>(1,4) + z[10];
    z[9]=z[2]*z[9];
    z[11]=n<T>(1,4)*z[6];
    z[12]=static_cast<T>(7)+ 5*z[6];
    z[12]=z[12]*z[11];
    z[13]= - z[8] + n<T>(1,2)*z[4];
    z[9]=z[9] + z[12] + n<T>(1,2)*z[13] + z[10];
    z[10]=z[6]*z[5];
    z[12]= - z[2] - z[5] - z[10];
    z[12]=z[11]*z[12];
    z[13]= - n<T>(1,2) + z[7];
    z[13]=z[7]*z[13];
    z[12]= - n<T>(1,2) + z[13] + z[12];
    z[12]=z[2]*z[12];
    z[11]=z[11] + n<T>(1,2);
    z[11]=z[5]*z[11];
    z[11]=static_cast<T>(2)+ z[11];
    z[11]=z[6]*z[11];
    z[11]=z[11] + z[12];
    z[12]=n<T>(1,3)*z[5];
    z[13]= - z[12] + n<T>(1,2) + n<T>(1,3)*z[7];
    z[13]=z[7]*z[13];
    z[14]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[11]=n<T>(1,6)*z[14] + z[13] + n<T>(1,3)*z[11];
    z[11]=z[2]*z[11];
    z[13]=n<T>(1,2)*z[8] - z[7];
    z[12]=z[13]*z[12];
    z[13]=static_cast<T>(7)+ 5*z[4];
    z[10]=z[13]*z[10];
    z[13]=n<T>(13,12) + z[4];
    z[13]=z[5]*z[13];
    z[10]=n<T>(1,12)*z[10] + z[13] + static_cast<T>(1)+ n<T>(5,12)*z[4];
    z[10]=z[6]*z[10];
    z[13]=static_cast<T>(1)- n<T>(1,12)*z[1];
    z[13]=z[4]*z[13];
    z[10]=z[11] + z[10] + z[12] + n<T>(2,3)*z[7] + static_cast<T>(1)+ z[13];
    z[10]=z[3]*z[10];
    z[9]=n<T>(1,3)*z[9] + z[10];

    r += z[9]*z[3];
 
    return r;
}

template double qqb_2lha_r1054(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1054(const std::array<dd_real,30>&);
#endif
