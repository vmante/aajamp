#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2309(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[20];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=z[3] + 3;
    z[9]=n<T>(1,2)*z[3];
    z[10]= - z[8]*z[9];
    z[11]=z[5] - 3;
    z[11]=z[11]*z[5];
    z[12]=z[7] - 3;
    z[10]=z[11] - 3*z[12] + z[10];
    z[13]=3*z[4];
    z[10]=z[10]*z[13];
    z[14]=z[3] + n<T>(5,2);
    z[15]=3*z[3];
    z[16]=z[14]*z[15];
    z[17]=8*z[7];
    z[18]=z[5] - 2;
    z[19]=z[18]*z[5];
    z[10]=z[10] - 6*z[19] + z[16] - n<T>(77,2) + z[17];
    z[10]=z[4]*z[10];
    z[16]= - n<T>(7,2) - z[3];
    z[16]=z[3]*z[16];
    z[20]=2*z[5];
    z[21]= - static_cast<T>(7)+ z[20];
    z[21]=z[5]*z[21];
    z[16]=z[21] + static_cast<T>(9)+ z[16];
    z[16]=z[4]*z[16];
    z[21]=z[20] - 5;
    z[21]=z[21]*z[5];
    z[8]=z[3]*z[8];
    z[8]=z[16] - z[21] - static_cast<T>(4)+ z[8];
    z[8]=z[4]*z[8];
    z[16]=n<T>(1,2)*z[5];
    z[22]=z[16] - 2;
    z[22]=z[22]*z[5];
    z[23]= - static_cast<T>(1)- n<T>(1,4)*z[3];
    z[23]=z[3]*z[23];
    z[23]=z[23] + z[22];
    z[24]=z[1]*npow(z[4],2);
    z[23]=z[23]*z[24];
    z[8]=z[8] + z[23];
    z[8]=z[1]*z[8];
    z[23]=z[9] + 1;
    z[25]= - z[23]*z[15];
    z[26]=z[5] - 1;
    z[27]=z[26]*z[5];
    z[28]=3*z[27];
    z[29]=4*z[7];
    z[8]=z[8] + z[10] + z[28] + z[25] + n<T>(21,2) + z[29];
    z[8]=z[1]*z[8];
    z[10]= - n<T>(13,2) + z[7];
    z[10]=z[7]*z[10];
    z[10]=static_cast<T>(9)+ z[10];
    z[14]= - z[3]*z[14];
    z[10]=z[21] + 3*z[10] + z[14];
    z[10]=z[4]*z[10];
    z[14]=static_cast<T>(2)+ z[3];
    z[14]=z[14]*z[15];
    z[21]=3*z[5];
    z[25]=static_cast<T>(3)- z[20];
    z[25]=z[25]*z[21];
    z[30]=static_cast<T>(45)- z[29];
    z[30]=z[7]*z[30];
    z[10]=z[10] + z[25] + z[14] - static_cast<T>(65)+ z[30];
    z[10]=z[4]*z[10];
    z[14]=z[6] - 2;
    z[25]= - z[14]*z[20];
    z[25]= - z[6] + z[25];
    z[25]=z[5]*z[25];
    z[17]= - n<T>(3,2) - z[17];
    z[17]=z[7]*z[17];
    z[30]=5*z[7];
    z[31]=z[30] + 1;
    z[32]=z[7]*z[31];
    z[32]= - static_cast<T>(9)+ z[32];
    z[32]=z[6]*z[32];
    z[33]=z[3]*z[14];
    z[33]=z[33] - n<T>(3,2) + z[6];
    z[33]=z[3]*z[33];
    z[8]=z[8] + z[10] + z[25] + z[33] + z[32] + static_cast<T>(17)+ z[17];
    z[8]=z[1]*z[8];
    z[10]=z[20] + z[3];
    z[17]= - static_cast<T>(2)+ z[7];
    z[17]=3*z[17] + z[10];
    z[17]=z[4]*z[17];
    z[25]=4*z[5] + 2*z[3];
    z[32]=7*z[7];
    z[17]=z[17] + static_cast<T>(13)- z[32] - z[25];
    z[17]=z[4]*z[17];
    z[17]=z[17] - static_cast<T>(7)+ z[29] + z[10];
    z[33]=2*z[4];
    z[34]= - static_cast<T>(3)+ z[10];
    z[34]=z[34]*z[33];
    z[34]=z[34] + static_cast<T>(7)- z[25];
    z[34]=z[4]*z[34];
    z[35]=z[9] + z[5];
    z[36]=z[35]*z[24];
    z[34]=z[34] + z[36];
    z[34]=z[1]*z[34];
    z[17]=3*z[17] + z[34];
    z[17]=z[1]*z[17];
    z[34]= - static_cast<T>(20)+ z[32];
    z[34]=z[7]*z[34];
    z[34]=static_cast<T>(19)+ z[34] - z[25];
    z[12]=z[12]*z[7];
    z[36]= - static_cast<T>(3)- z[12];
    z[10]=3*z[36] + z[10];
    z[10]=z[10]*z[33];
    z[10]=3*z[34] + z[10];
    z[10]=z[4]*z[10];
    z[33]= - z[14]*z[25];
    z[34]=static_cast<T>(14)- z[30];
    z[34]=z[7]*z[34];
    z[36]=3*z[7];
    z[37]= - static_cast<T>(8)+ z[36];
    z[37]=z[7]*z[37];
    z[37]=static_cast<T>(7)+ z[37];
    z[37]=z[6]*z[37];
    z[34]=z[37] - static_cast<T>(13)+ z[34];
    z[10]=z[17] + z[10] + 3*z[34] + z[33];
    z[10]=z[1]*z[10];
    z[17]=n<T>(1,2)*z[7];
    z[33]= - static_cast<T>(2)+ z[17];
    z[33]=z[7]*z[33];
    z[33]=static_cast<T>(3)+ z[33];
    z[33]=z[7]*z[33];
    z[33]= - static_cast<T>(2)+ z[33];
    z[33]=3*z[33] + z[35];
    z[33]=z[4]*z[33];
    z[32]=static_cast<T>(27)- z[32];
    z[32]=z[7]*z[32];
    z[32]= - static_cast<T>(39)+ z[32];
    z[32]=z[7]*z[32];
    z[25]=z[33] + static_cast<T>(25)+ z[32] - z[25];
    z[25]=z[4]*z[25];
    z[32]=static_cast<T>(15)- z[29];
    z[32]=z[7]*z[32];
    z[32]= - static_cast<T>(21)+ z[32];
    z[32]=z[7]*z[32];
    z[33]=n<T>(5,2)*z[7];
    z[34]= - static_cast<T>(9)+ z[33];
    z[34]=z[7]*z[34];
    z[34]=static_cast<T>(12)+ z[34];
    z[34]=z[7]*z[34];
    z[34]= - static_cast<T>(7)+ z[34];
    z[34]=z[6]*z[34];
    z[32]=z[34] + static_cast<T>(13)+ z[32];
    z[32]=z[6]*z[32];
    z[34]=n<T>(1,2)*z[6];
    z[35]=z[34] - 1;
    z[37]=z[35]*z[6];
    z[37]=z[37] + n<T>(3,2);
    z[38]=z[37]*z[3];
    z[39]= - static_cast<T>(21)+ n<T>(11,2)*z[7];
    z[39]=z[7]*z[39];
    z[39]=static_cast<T>(30)+ z[39];
    z[39]=z[7]*z[39];
    z[14]=z[14]*z[6];
    z[40]=static_cast<T>(3)+ z[14];
    z[40]=z[5]*z[40];
    z[10]=z[10] + z[25] + z[40] + z[38] + z[32] - static_cast<T>(19)+ z[39];
    z[10]=z[2]*z[10];
    z[25]= - static_cast<T>(7)+ z[33];
    z[17]=z[25]*z[17];
    z[17]=static_cast<T>(3)+ z[17];
    z[23]= - z[23]*z[9];
    z[25]=z[16] - 1;
    z[25]=z[25]*z[5];
    z[17]=z[25] + 3*z[17] + z[23];
    z[17]=z[4]*z[17];
    z[20]=z[26]*z[20];
    z[23]=29*z[7];
    z[26]=static_cast<T>(77)- z[23];
    z[26]=z[7]*z[26];
    z[26]= - static_cast<T>(61)+ z[26];
    z[32]=n<T>(3,2) + z[3];
    z[32]=z[3]*z[32];
    z[17]=z[17] - z[20] + n<T>(1,2)*z[26] + z[32];
    z[17]=z[4]*z[17];
    z[26]=static_cast<T>(11)- z[30];
    z[26]=z[7]*z[26];
    z[26]= - n<T>(13,2) + z[26];
    z[26]=z[7]*z[26];
    z[32]=n<T>(9,4)*z[7];
    z[39]= - static_cast<T>(5)+ z[32];
    z[39]=z[7]*z[39];
    z[39]=n<T>(5,2) + z[39];
    z[39]=z[6]*z[39];
    z[26]=z[39] + n<T>(3,2) + z[26];
    z[26]=z[6]*z[26];
    z[38]= - z[6] - z[38];
    z[38]=z[38]*z[9];
    z[37]=z[5]*z[37];
    z[14]=z[37] + static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[14];
    z[29]= - n<T>(17,4) + z[29];
    z[29]=z[7]*z[29];
    z[29]= - static_cast<T>(7)+ z[29];
    z[29]=z[7]*z[29];
    z[8]=z[10] + z[8] + z[17] + z[14] + z[38] + z[26] + n<T>(17,2) + z[29];
    z[8]=z[2]*z[8];
    z[10]=n<T>(3,2)*z[3];
    z[14]=z[3] + 1;
    z[17]= - z[14]*z[10];
    z[10]=static_cast<T>(1)+ z[10];
    z[10]=z[10]*z[9];
    z[10]= - static_cast<T>(1)+ z[10];
    z[26]=static_cast<T>(3)- n<T>(5,2)*z[5];
    z[26]=z[5]*z[26];
    z[10]=3*z[10] + z[26];
    z[10]=z[4]*z[10];
    z[10]=z[10] + z[17] + z[20];
    z[10]=z[4]*z[10];
    z[17]=z[14]*z[9];
    z[17]=z[17] - z[27];
    z[17]=z[17]*z[24];
    z[10]=z[10] + z[17];
    z[10]=z[1]*z[10];
    z[17]= - static_cast<T>(11)- 21*z[3];
    z[17]=z[17]*z[9];
    z[20]=static_cast<T>(1)+ 5*z[3];
    z[20]=z[20]*z[9];
    z[20]= - z[27] + z[20] - static_cast<T>(7)+ z[7];
    z[13]=z[20]*z[13];
    z[13]=z[13] + z[28] + static_cast<T>(19)+ z[17];
    z[17]=n<T>(1,2)*z[4];
    z[13]=z[13]*z[17];
    z[14]=z[14]*z[15];
    z[14]=z[14] + z[31];
    z[10]=z[10] + z[13] + n<T>(1,2)*z[14] + z[5];
    z[10]=z[1]*z[10];
    z[13]= - n<T>(13,2) + z[36];
    z[14]= - static_cast<T>(1)+ n<T>(11,4)*z[3];
    z[14]=z[3]*z[14];
    z[13]=z[22] + n<T>(3,2)*z[13] + z[14];
    z[13]=z[4]*z[13];
    z[14]= - z[18]*z[21];
    z[18]=n<T>(1,2) - 6*z[3];
    z[18]=z[3]*z[18];
    z[13]=z[13] + z[14] + z[18] + n<T>(37,2) - 6*z[7];
    z[13]=z[4]*z[13];
    z[14]=9*z[7];
    z[18]= - static_cast<T>(1)+ z[14];
    z[18]=z[18]*z[34];
    z[20]=z[6] - n<T>(9,2);
    z[20]=z[20]*z[3];
    z[21]=z[6] + 1;
    z[22]= - z[20] - z[21];
    z[22]=z[22]*z[9];
    z[26]=n<T>(3,2) - z[30];
    z[26]=z[7]*z[26];
    z[27]=n<T>(9,2) - 2*z[6];
    z[27]=z[5]*z[27];
    z[27]= - static_cast<T>(3)+ z[27];
    z[27]=z[5]*z[27];
    z[10]=z[10] + z[13] + z[27] + z[22] + z[18] + n<T>(1,4) + z[26];
    z[10]=z[1]*z[10];
    z[13]=static_cast<T>(7)- 9*z[3];
    z[13]=z[3]*z[13];
    z[13]=z[13] + static_cast<T>(27)- z[23];
    z[18]=z[3] - 1;
    z[18]=z[18]*z[3];
    z[22]=z[7] - 1;
    z[23]=3*z[22] + z[18];
    z[11]=n<T>(3,2)*z[23] + z[11];
    z[11]=z[4]*z[11];
    z[23]=static_cast<T>(11)- 5*z[5];
    z[23]=z[5]*z[23];
    z[11]=z[11] + n<T>(1,2)*z[13] + z[23];
    z[11]=z[11]*z[17];
    z[13]= - static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[13]=z[13]*z[18];
    z[14]=n<T>(29,2) - z[14];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(11,2) + z[14];
    z[14]=z[14]*z[34];
    z[18]=z[34] - 3;
    z[23]= - n<T>(5,2) + z[6];
    z[23]=z[6]*z[23];
    z[23]=static_cast<T>(3)+ z[23];
    z[23]=z[5]*z[23];
    z[23]=z[23] + z[18];
    z[23]=z[5]*z[23];
    z[26]= - static_cast<T>(2)+ z[33];
    z[26]=z[7]*z[26];
    z[26]= - n<T>(1,2) + z[26];
    z[26]=z[7]*z[26];
    z[8]=z[8] + z[10] + z[11] + z[23] - z[13] + z[14] + static_cast<T>(1)+ z[26];
    z[8]=z[2]*z[8];
    z[10]= - z[20] + z[35];
    z[10]=z[3]*z[10];
    z[10]= - n<T>(5,2) + z[10];
    z[11]=static_cast<T>(1)- z[6];
    z[11]=z[11]*z[16];
    z[11]=z[11] + z[35];
    z[11]=z[5]*z[11];
    z[14]= - static_cast<T>(7)+ 3*z[6];
    z[14]=z[14]*z[9];
    z[16]=z[6] - 3;
    z[14]=z[14] - z[16];
    z[14]=z[14]*z[9];
    z[14]=z[14] - z[19];
    z[14]=z[4]*z[14];
    z[10]=z[14] + n<T>(1,2)*z[10] + z[11];
    z[10]=z[4]*z[10];
    z[11]=z[15] - 1;
    z[11]=z[3]*z[16]*z[11];
    z[11]=static_cast<T>(3)+ z[11];
    z[14]=n<T>(1,2)*z[16];
    z[15]=z[5]*z[35];
    z[15]= - z[14] + z[15];
    z[15]=z[5]*z[15];
    z[11]=n<T>(1,4)*z[11] + z[15];
    z[11]=z[4]*z[11];
    z[15]=npow(z[5],2);
    z[16]=z[21]*z[15];
    z[18]= - z[3]*z[18];
    z[18]=static_cast<T>(1)+ z[18];
    z[9]=z[18]*z[9];
    z[9]=z[11] + z[9] - z[16];
    z[9]=z[4]*z[9];
    z[11]=npow(z[3],2);
    z[11]=n<T>(1,2)*z[11];
    z[18]= - static_cast<T>(5)+ z[6];
    z[18]=z[18]*z[11];
    z[16]=z[18] + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[16] + z[11] - 3*z[15];
    z[16]=z[4]*z[16];
    z[11]= - z[11] + z[15];
    z[11]=z[11]*z[24];
    z[11]=z[16] + z[11];
    z[11]=z[1]*z[11];
    z[9]=n<T>(1,2)*z[11] + z[9] + n<T>(9,4) + 2*z[15];
    z[9]=z[1]*z[9];
    z[11]=n<T>(1,2) - z[7];
    z[15]=static_cast<T>(5)- z[36];
    z[15]=n<T>(1,2)*z[15] - z[6];
    z[15]=z[5]*z[15];
    z[15]=n<T>(1,2) + z[15];
    z[15]=z[5]*z[15];
    z[9]=z[9] + z[10] + n<T>(9,2)*z[11] + z[15];
    z[9]=z[1]*z[9];
    z[10]=z[3]*z[35];
    z[10]= - z[14] + z[10];
    z[10]=z[3]*z[10];
    z[10]= - n<T>(1,2) + z[10];
    z[10]=z[10]*z[17];
    z[10]=z[10] - z[13] - z[25];
    z[10]=z[4]*z[10];
    z[11]=z[22]*z[32];
    z[12]=static_cast<T>(5)+ z[12];
    z[13]=z[34] - n<T>(5,2) + z[7];
    z[13]=z[6]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[5]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[5]*z[12];

    r += z[8] + z[9] + z[10] + z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r2309(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2309(const std::array<dd_real,30>&);
#endif
