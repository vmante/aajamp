#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r615(const std::array<T,30>& k) {
  T z[44];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[2];
    z[5]=k[8];
    z[6]=k[9];
    z[7]=k[10];
    z[8]=k[5];
    z[9]=k[4];
    z[10]=k[3];
    z[11]=k[15];
    z[12]=k[20];
    z[13]=k[11];
    z[14]=k[12];
    z[15]=k[21];
    z[16]=k[28];
    z[17]=k[13];
    z[18]=n<T>(1,3)*z[6];
    z[19]= - n<T>(5,2) + z[6];
    z[19]=z[19]*z[18];
    z[20]=n<T>(1,2)*z[3];
    z[21]=z[6] - 1;
    z[22]=z[21]*z[20];
    z[19]=z[19] + z[22];
    z[19]=z[3]*z[19];
    z[22]=npow(z[3],2);
    z[23]=z[22]*z[9];
    z[24]=n<T>(1,2)*z[6];
    z[25]=z[23]*z[24];
    z[26]=z[18] - n<T>(1,2);
    z[27]=z[6]*z[26];
    z[28]=z[3]*z[21];
    z[27]=z[27] + z[28];
    z[27]=z[3]*z[27];
    z[27]=z[27] + z[25];
    z[27]=z[9]*z[27];
    z[19]=z[19] + z[27];
    z[27]=z[3]*z[6];
    z[28]= - z[21]*z[27];
    z[29]=n<T>(1,2)*z[9];
    z[30]=npow(z[6],2);
    z[31]=z[29]*z[30];
    z[32]= - z[3]*z[31];
    z[28]=z[28] + z[32];
    z[28]=z[9]*z[28];
    z[32]=z[24] - 1;
    z[33]=z[32]*z[6];
    z[34]=z[33] + n<T>(1,2);
    z[35]=z[34]*z[3];
    z[28]= - z[35] + z[28];
    z[28]=z[2]*z[28];
    z[36]=z[21]*z[6];
    z[31]=z[31] + z[36];
    z[31]=z[31]*z[9];
    z[31]=z[31] + z[34];
    z[31]=z[31]*z[9];
    z[37]=z[15]*z[31];
    z[38]=z[30]*z[9];
    z[39]=z[38] + z[36];
    z[29]=z[39]*z[29];
    z[29]=z[37] + z[29];
    z[31]= - z[2]*z[31];
    z[31]=z[31] + z[29];
    z[37]=n<T>(1,3)*z[15];
    z[31]=z[37]*z[31];
    z[19]=z[31] + n<T>(1,2)*z[19] + n<T>(1,3)*z[28];
    z[19]=z[2]*z[19];
    z[28]=n<T>(1,4)*z[6];
    z[31]=n<T>(7,3) - n<T>(9,2)*z[6];
    z[31]=z[31]*z[28];
    z[40]=static_cast<T>(1)- n<T>(3,2)*z[6];
    z[40]=z[3]*z[40]*z[24];
    z[41]= - static_cast<T>(1)+ n<T>(43,24)*z[6];
    z[41]=z[6]*z[41];
    z[40]=z[41] + z[40];
    z[40]=z[3]*z[40];
    z[41]=z[30]*z[3];
    z[42]=n<T>(7,3)*z[30] - z[41];
    z[42]=z[3]*z[42];
    z[42]= - n<T>(5,3)*z[30] + z[42];
    z[42]=z[9]*z[42];
    z[31]=n<T>(1,4)*z[42] + z[31] + z[40];
    z[31]=z[9]*z[31];
    z[40]= - n<T>(49,3) + 13*z[6];
    z[40]=z[6]*z[40];
    z[42]=static_cast<T>(1)- n<T>(3,4)*z[6];
    z[42]=z[6]*z[42];
    z[42]= - n<T>(1,4) + z[42];
    z[42]=z[3]*z[42];
    z[40]=z[42] + n<T>(2,3) + n<T>(1,8)*z[40];
    z[40]=z[3]*z[40];
    z[42]=n<T>(1,4) - 2*z[36];
    z[31]=z[31] + n<T>(1,3)*z[42] + z[40];
    z[31]=z[3]*z[31];
    z[40]=npow(z[9],2);
    z[41]= - z[40]*z[41];
    z[40]=z[40]*z[16];
    z[42]=z[39]*z[40];
    z[41]=z[41] + z[42];
    z[41]=z[17]*z[41];
    z[40]=z[40] - z[9];
    z[40]=z[16]*z[40]*z[39];
    z[41]= - z[40] + z[41];
    z[26]= - z[26]*z[27];
    z[25]=z[26] + z[25];
    z[25]=z[9]*z[25];
    z[25]=z[25] + n<T>(1,3)*z[41];
    z[25]=z[17]*z[25];
    z[26]=n<T>(1,3)*z[8];
    z[41]=z[26] + 1;
    z[42]=n<T>(1,2)*z[8];
    z[41]=z[41]*z[42];
    z[41]=z[41] + n<T>(1,3);
    z[39]= - z[14]*z[39]*z[41];
    z[25]=z[39] + z[25];
    z[39]=static_cast<T>(65)- 67*z[6];
    z[39]=z[39]*z[24];
    z[39]= - static_cast<T>(5)+ z[39];
    z[42]= - static_cast<T>(9)+ 7*z[6];
    z[42]=z[42]*z[24];
    z[42]=static_cast<T>(1)+ z[42];
    z[42]=z[3]*z[42];
    z[39]=n<T>(1,6)*z[39] + z[42];
    z[39]=z[39]*z[20];
    z[42]=n<T>(5,2)*z[6];
    z[43]= - static_cast<T>(1)+ z[42];
    z[43]=z[43]*z[18];
    z[42]=static_cast<T>(3)- z[42];
    z[42]=z[6]*z[42];
    z[42]= - n<T>(1,2) + z[42];
    z[20]=z[42]*z[20];
    z[20]=z[43] + z[20];
    z[20]=z[3]*z[20];
    z[20]=n<T>(1,6)*z[30] + z[20];
    z[20]=z[4]*z[20];
    z[42]= - static_cast<T>(1)+ 2*z[6];
    z[42]=z[42]*z[18];
    z[20]=z[20] + z[42] + z[39];
    z[20]=z[3]*z[20];
    z[39]=static_cast<T>(1)- n<T>(17,4)*z[6];
    z[18]=z[39]*z[18];
    z[39]= - n<T>(1,2) + z[6];
    z[27]=z[39]*z[27];
    z[18]=z[18] + z[27];
    z[18]=z[18]*z[23];
    z[18]=z[18] + z[20];
    z[18]=z[4]*z[18];
    z[20]=n<T>(13,3) - 3*z[6];
    z[20]=z[20]*z[28];
    z[23]= - static_cast<T>(7)+ 5*z[6];
    z[23]=z[23]*z[24];
    z[23]=z[23] + 1;
    z[23]=n<T>(1,3)*z[23];
    z[27]=z[23] - z[35];
    z[27]=z[3]*z[27];
    z[39]= - static_cast<T>(1)+ n<T>(1,6)*z[6];
    z[39]=z[39]*z[6];
    z[39]=z[39] + n<T>(5,6);
    z[27]=n<T>(1,2)*z[39] + z[27];
    z[27]=z[3]*z[27];
    z[20]=z[27] - n<T>(1,3) + z[20];
    z[27]= - z[23] + n<T>(3,2)*z[35];
    z[27]=z[3]*z[27];
    z[27]= - n<T>(1,4)*z[39] + z[27];
    z[27]=z[3]*z[27];
    z[23]=z[23] - 3*z[35];
    z[23]=z[23]*z[22];
    z[34]=z[34]*npow(z[3],3);
    z[35]=z[4]*z[34];
    z[23]=z[23] + z[35];
    z[35]=n<T>(1,2)*z[4];
    z[23]=z[23]*z[35];
    z[23]=z[27] + z[23];
    z[23]=z[4]*z[23];
    z[20]=n<T>(1,2)*z[20] + z[23];
    z[20]=z[5]*z[20];
    z[23]=z[9]*z[6];
    z[27]=z[32]*z[23];
    z[21]= - z[4]*z[21]*z[24];
    z[21]=z[21] + n<T>(1,4)*z[36] + z[27];
    z[21]=z[4]*z[21];
    z[23]=z[24] + z[23];
    z[23]=z[9]*z[23];
    z[23]=z[36] + z[23];
    z[21]=n<T>(1,2)*z[23] + z[21];
    z[21]=z[21]*z[26];
    z[23]= - n<T>(1,3) + z[28];
    z[23]=z[6]*z[23];
    z[23]=z[23] + n<T>(1,6)*z[38];
    z[23]=z[9]*z[23];
    z[26]= - z[30]*z[35];
    z[26]= - z[33] + z[26];
    z[26]=z[4]*z[26];
    z[21]=z[21] + n<T>(1,3)*z[26] + n<T>(5,12)*z[36] + z[23];
    z[21]=z[8]*z[21];
    z[23]=z[5] - z[14];
    z[23]=z[11]*z[23];
    z[26]= - z[13] + z[7];
    z[26]=z[12]*z[26];
    z[23]=z[26] + z[23];
    z[23]=z[36]*z[23];
    z[26]=z[14] + z[13];
    z[26]=z[30]*z[26];
    z[23]=z[26] + z[23];
    z[26]=n<T>(1,6)*z[10];
    z[23]=z[26]*z[23];
    z[26]= - z[29]*z[37];
    z[27]=z[38]*z[41];
    z[26]=n<T>(1,2)*z[27] + z[26];
    z[26]=z[13]*z[26];
    z[27]=n<T>(11,4) - z[6];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(7,4) + z[27];
    z[27]=z[27]*z[22];
    z[28]= - z[35]*z[34];
    z[27]=n<T>(1,3)*z[27] + z[28];
    z[27]=z[4]*z[27];
    z[24]=n<T>(1,3) + z[24];
    z[24]=z[6]*z[24];
    z[24]= - n<T>(5,6) + z[24];
    z[24]=z[3]*z[24];
    z[24]=n<T>(1,4)*z[24] + z[27];
    z[24]=z[4]*z[24];
    z[27]=n<T>(5,4) - z[6];
    z[27]=z[6]*z[27];
    z[27]= - n<T>(1,4) + z[27];
    z[24]=n<T>(1,6)*z[27] + z[24];
    z[24]=z[7]*z[24];
    z[27]=z[14]*z[40];
    z[28]= - static_cast<T>(1)+ z[2];
    z[22]=z[1]*z[22]*z[28];

    r += z[18] + z[19] + z[20] + z[21] + n<T>(1,4)*z[22] + z[23] + z[24] + 
      n<T>(1,2)*z[25] + z[26] + n<T>(1,6)*z[27] - n<T>(1,3)*z[30] + z[31];
 
    return r;
}

template double qqb_2lha_r615(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r615(const std::array<dd_real,30>&);
#endif
