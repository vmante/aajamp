#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1957(const std::array<T,30>& k) {
  T z[32];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[28];
    z[6]=k[4];
    z[7]=k[21];
    z[8]=k[9];
    z[9]=k[13];
    z[10]=n<T>(1,2)*z[1];
    z[11]=z[10] - 1;
    z[12]=3*z[5];
    z[13]=z[11]*z[12];
    z[14]=3*z[1];
    z[15]=z[5]*z[11];
    z[15]=z[15] - n<T>(1,2) + z[7];
    z[15]=z[2]*z[15];
    z[15]=z[15] - z[13] + n<T>(13,2) - z[14];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(13)- z[1];
    z[16]=z[16]*z[10];
    z[13]=z[15] + z[13] - static_cast<T>(11)+ z[16];
    z[15]=n<T>(1,2)*z[2];
    z[13]=z[13]*z[15];
    z[16]=z[4] + z[9];
    z[17]=n<T>(1,2)*z[6];
    z[18]=z[16]*z[17];
    z[19]=2*z[4];
    z[18]=z[19] + z[18];
    z[18]=z[6]*z[18];
    z[20]=3*z[4];
    z[18]=z[18] - static_cast<T>(1)+ z[20];
    z[18]=z[6]*z[18];
    z[21]=n<T>(3,2)*z[2];
    z[18]=z[18] - z[21] - n<T>(3,4) + z[19];
    z[18]=z[6]*z[18];
    z[19]=z[5] - z[7];
    z[22]=z[19]*z[2];
    z[23]= - z[22] + static_cast<T>(7)+ z[12];
    z[23]=z[2]*z[23];
    z[24]= - static_cast<T>(3)- z[5];
    z[23]=3*z[24] + z[23];
    z[23]=z[23]*z[15];
    z[24]=n<T>(1,2)*z[5];
    z[23]=z[23] + z[24] + n<T>(1,2) + z[4];
    z[18]=n<T>(1,2)*z[23] + z[18];
    z[18]=z[8]*z[18];
    z[19]=z[19]*z[15];
    z[23]= - static_cast<T>(1)- z[24];
    z[23]=3*z[23] + z[19];
    z[25]=3*z[2];
    z[23]=z[23]*z[25];
    z[25]=9*z[5];
    z[26]=static_cast<T>(25)+ z[25];
    z[23]=n<T>(1,2)*z[26] + z[23];
    z[23]=z[23]*z[15];
    z[26]=3*z[9];
    z[27]= - z[26] - 5*z[4];
    z[27]=z[27]*z[17];
    z[27]=z[27] - static_cast<T>(1)- n<T>(15,2)*z[4];
    z[27]=z[27]*z[17];
    z[27]=z[27] - n<T>(15,4)*z[4] + 4*z[2];
    z[27]=z[6]*z[27];
    z[18]=z[18] + z[27] + z[23] - n<T>(3,4)*z[5] - static_cast<T>(1)- n<T>(5,4)*z[4];
    z[18]=z[8]*z[18];
    z[23]=z[4] - 1;
    z[27]=z[6]*z[4];
    z[27]=3*z[23] + z[27];
    z[27]=z[6]*z[27];
    z[28]=z[4] + z[2] - 2;
    z[27]=3*z[28] + z[27];
    z[27]=z[6]*z[27];
    z[29]=n<T>(1,4)*z[6];
    z[16]=z[16]*z[29];
    z[16]=z[16] + z[4];
    z[16]=z[16]*z[6];
    z[23]=n<T>(3,2)*z[23];
    z[30]= - z[23] - z[16];
    z[30]=z[6]*z[30];
    z[30]=z[30] - z[28];
    z[30]=z[6]*z[30];
    z[31]=z[2] - 3;
    z[31]=z[31]*z[2];
    z[31]=z[31] - z[4] + 3;
    z[30]=n<T>(1,4)*z[31] + z[30];
    z[30]=z[8]*z[30];
    z[27]=z[30] + z[27] - z[31];
    z[27]=z[8]*z[27];
    z[30]=z[11]*z[1];
    z[30]=z[30] + n<T>(3,2);
    z[30]=z[30]*z[31];
    z[31]=z[1] - 2;
    z[28]=z[31]*z[28];
    z[23]= - z[6]*z[23];
    z[23]=z[23] + z[28];
    z[23]=z[6]*z[23];
    z[23]=z[27] + n<T>(1,2)*z[30] + z[23];
    z[23]=z[3]*z[23];
    z[27]=z[1] - n<T>(7,2);
    z[28]=z[7]*npow(z[2],2);
    z[28]= - n<T>(1,4)*z[28] + z[27];
    z[28]=z[2]*z[28];
    z[30]=static_cast<T>(5)+ z[20];
    z[30]=z[30]*z[29];
    z[31]=static_cast<T>(3)+ z[1];
    z[31]=z[4]*z[31];
    z[31]=z[31] + static_cast<T>(11)- 5*z[1];
    z[28]=z[30] + n<T>(1,4)*z[31] + z[28];
    z[28]=z[6]*z[28];
    z[30]=n<T>(1,4)*z[4];
    z[31]=static_cast<T>(1)- z[1];
    z[31]=z[1]*z[31];
    z[31]=static_cast<T>(1)+ z[31];
    z[31]=z[31]*z[30];
    z[11]= - z[11]*z[24];
    z[10]= - static_cast<T>(2)+ z[10];
    z[10]=z[1]*z[10];
    z[10]=z[23] + z[18] + z[28] + z[13] + z[11] + z[31] + n<T>(9,4) + z[10];
    z[10]=z[3]*z[10];
    z[11]= - static_cast<T>(13)- z[4];
    z[11]=n<T>(1,2)*z[11] - z[12];
    z[13]= - n<T>(3,2)*z[5] - static_cast<T>(1)- n<T>(3,4)*z[7];
    z[13]=z[2]*z[13];
    z[13]=z[13] + static_cast<T>(4)+ z[12];
    z[13]=z[2]*z[13];
    z[18]= - n<T>(5,2) - z[20];
    z[16]=n<T>(1,2)*z[18] - z[16];
    z[16]=z[6]*z[16];
    z[16]=z[16] + n<T>(5,2)*z[2] - n<T>(13,4) - z[4];
    z[16]=z[6]*z[16];
    z[11]=z[16] + n<T>(1,2)*z[11] + z[13];
    z[11]=z[8]*z[11];
    z[13]=7*z[5];
    z[16]=static_cast<T>(17)+ z[4];
    z[16]=n<T>(3,4)*z[16] + z[13];
    z[13]=z[13] + static_cast<T>(1)+ n<T>(5,2)*z[7];
    z[13]=z[13]*z[15];
    z[18]= - static_cast<T>(1)- z[5];
    z[13]=7*z[18] + z[13];
    z[13]=z[2]*z[13];
    z[18]=z[26] + z[4];
    z[18]=z[18]*z[17];
    z[18]=z[18] + n<T>(7,4)*z[4] + static_cast<T>(3)+ n<T>(1,4)*z[9];
    z[18]=z[18]*z[17];
    z[18]=z[18] - z[21] + n<T>(11,2) + z[4];
    z[18]=z[6]*z[18];
    z[11]=z[11] + z[18] + n<T>(1,2)*z[16] + z[13];
    z[11]=z[8]*z[11];
    z[13]= - static_cast<T>(1)- z[1];
    z[13]=z[13]*z[30];
    z[16]=n<T>(1,2)*z[4];
    z[18]=z[16] + n<T>(3,2) - z[9];
    z[18]=z[18]*z[17];
    z[21]=n<T>(1,4)*z[1];
    z[23]=n<T>(1,4)*z[2];
    z[24]= - z[7]*z[23];
    z[24]= - static_cast<T>(2)+ z[24];
    z[24]=z[2]*z[24];
    z[13]=z[18] + z[24] + z[13] - static_cast<T>(1)- z[21];
    z[13]=z[6]*z[13];
    z[18]= - static_cast<T>(5)- z[1];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[14]=3*z[18] + z[14];
    z[18]=z[5]*z[27];
    z[14]=n<T>(1,4)*z[14] + z[18];
    z[18]= - n<T>(5,2) + z[1];
    z[18]=z[5]*z[18];
    z[18]= - z[19] + z[18] + static_cast<T>(3)- z[7];
    z[18]=z[18]*z[15];
    z[19]=n<T>(13,4) - z[1];
    z[19]=z[5]*z[19];
    z[18]=z[18] + n<T>(1,2) + z[19];
    z[18]=z[2]*z[18];
    z[10]=z[10] + z[11] + z[13] + n<T>(1,2)*z[14] + z[18];
    z[10]=z[3]*z[10];
    z[11]=z[12]*z[2];
    z[13]=z[9] + z[20];
    z[14]= - z[6]*z[5];
    z[12]=z[14] + z[11] + n<T>(1,2)*z[13] + z[12];
    z[12]=z[12]*z[29];
    z[13]= - static_cast<T>(1)- z[4];
    z[13]=n<T>(1,2)*z[13] - z[25];
    z[14]= - 5*z[5] - z[16] - n<T>(1,2)*z[9] + z[7];
    z[14]=z[14]*z[29];
    z[18]=z[2]*z[5];
    z[13]=z[14] + n<T>(1,2)*z[13] + z[18];
    z[13]=z[6]*z[13];
    z[14]= - z[22] - z[7] + z[25];
    z[18]=n<T>(1,2)*z[7];
    z[19]= - z[18] + z[5];
    z[19]=z[6]*z[19];
    z[14]=n<T>(1,2)*z[14] + z[19];
    z[14]=z[14]*z[17];
    z[17]=4*z[5];
    z[18]= - z[18] - 2*z[5];
    z[18]=z[2]*z[18];
    z[14]=z[14] + z[17] + z[18];
    z[14]=z[6]*z[14];
    z[18]=z[5] + z[7];
    z[15]=z[18]*z[15];
    z[15]= - z[17] + z[15];
    z[15]=z[2]*z[15];
    z[14]=z[14] + n<T>(7,2)*z[5] + z[15];
    z[14]=z[8]*z[14];
    z[15]= - z[2]*z[18];
    z[15]=25*z[5] + z[15];
    z[15]=z[15]*z[23];
    z[16]= - static_cast<T>(1)- z[16];
    z[13]=z[14] + z[13] + z[15] + n<T>(1,4)*z[16] - 6*z[5];
    z[13]=z[8]*z[13];
    z[14]= - static_cast<T>(5)+ z[1];
    z[14]=z[5]*z[14];
    z[11]=z[14] - z[11];
    z[11]=z[11]*z[23];
    z[14]=static_cast<T>(2)- z[21];
    z[14]=z[5]*z[14];

    r += n<T>(3,8) + z[10] + z[11] + z[12] + z[13] + z[14];
 
    return r;
}

template double qqb_2lha_r1957(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1957(const std::array<dd_real,30>&);
#endif
