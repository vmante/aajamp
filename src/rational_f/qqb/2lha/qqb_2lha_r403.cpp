#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r403(const std::array<T,30>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=k[8];
    z[9]=k[9];
    z[10]=k[11];
    z[11]=k[6];
    z[12]=k[15];
    z[13]=n<T>(5,2)*z[9];
    z[14]=z[13] - static_cast<T>(1)- n<T>(11,2)*z[8];
    z[14]=z[4]*z[14];
    z[14]=z[14] - n<T>(131,12) - 5*z[9];
    z[14]=z[4]*z[14];
    z[13]=z[14] + z[13] - n<T>(3,2) - z[11];
    z[14]=n<T>(1,24)*z[3];
    z[15]= - static_cast<T>(1)- n<T>(13,2)*z[3];
    z[15]=z[15]*z[14];
    z[16]=static_cast<T>(1)- z[12];
    z[16]=z[12]*z[16];
    z[15]=z[15] - n<T>(1,4)*z[11] + z[16];
    z[15]=z[6]*z[15];
    z[16]= - n<T>(37,24)*z[3] - n<T>(5,2)*z[12] + n<T>(13,3) - n<T>(3,2)*z[11];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[6]*z[15];
    z[16]=n<T>(17,6)*z[4] - n<T>(155,12) - 3*z[11];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[15]=z[6]*z[15];
    z[13]=n<T>(1,4)*z[13] + z[15];
    z[13]=z[7]*z[13];
    z[15]=n<T>(1,2)*z[4];
    z[16]=n<T>(7,2)*z[9] - static_cast<T>(1)- n<T>(15,2)*z[8];
    z[16]=z[4]*z[16];
    z[16]=z[16] - n<T>(155,12) - 7*z[9];
    z[16]=z[16]*z[15];
    z[17]=z[11] - n<T>(1,2);
    z[16]=z[16] + n<T>(7,4)*z[9] - z[17];
    z[18]=n<T>(1,2)*z[6];
    z[17]= - n<T>(11,24)*z[3] + z[12] - z[17];
    z[17]=z[17]*z[18];
    z[17]=z[17] - n<T>(35,24) - z[11];
    z[17]=z[6]*z[17];
    z[13]=z[13] + n<T>(1,2)*z[16] + z[17];
    z[13]=z[7]*z[13];
    z[16]=z[9] - z[8];
    z[16]=z[16]*z[4];
    z[17]=3*z[9];
    z[16]=z[16] - z[17];
    z[19]= - n<T>(23,6) + z[16];
    z[19]=z[4]*z[19];
    z[20]=z[9] - n<T>(1,2);
    z[19]=3*z[20] + z[19];
    z[19]=z[4]*z[19];
    z[21]= - z[4] - z[18];
    z[21]=z[6]*z[21];
    z[19]=n<T>(23,3)*z[21] + z[19] - z[20];
    z[16]= - n<T>(13,3) + z[16];
    z[16]=z[4]*z[16];
    z[20]=z[9] - 1;
    z[16]=3*z[20] + z[16];
    z[16]=z[4]*z[16];
    z[16]=z[16] - z[20];
    z[21]= - static_cast<T>(1)- 13*z[4];
    z[22]= - z[12] - n<T>(1,12)*z[3];
    z[22]=z[6]*z[22];
    z[22]= - n<T>(13,4) + z[22];
    z[22]=z[6]*z[22];
    z[21]=n<T>(1,4)*z[21] + z[22];
    z[21]=z[6]*z[21];
    z[16]=n<T>(1,4)*z[16] + z[21];
    z[16]=z[7]*z[16];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[16]=z[7]*z[16];
    z[19]= - z[8] + z[20];
    z[19]=z[4]*z[19];
    z[19]=z[19] - z[17] - n<T>(7,3) + z[1];
    z[19]=z[4]*z[19];
    z[20]= - n<T>(7,3) - z[4];
    z[20]=z[6]*z[20];
    z[17]=z[20] + z[17] + z[19];
    z[17]=z[4]*z[17];
    z[17]= - z[9] + z[17];
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[5]*z[16];
    z[17]= - static_cast<T>(9)+ 7*z[1];
    z[19]=n<T>(1,2)*z[9];
    z[20]=z[19] - z[8];
    z[21]= - n<T>(7,8) + z[20];
    z[21]=z[4]*z[21];
    z[17]=z[21] + n<T>(1,8)*z[17] - z[9];
    z[17]=z[4]*z[17];
    z[21]= - n<T>(1,4)*z[4] - n<T>(5,12) - z[11];
    z[21]=z[21]*z[18];
    z[22]=z[9] + static_cast<T>(1)- z[11];
    z[17]=z[21] + n<T>(1,2)*z[22] + z[17];
    z[13]=z[16] + n<T>(1,2)*z[17] + z[13];
    z[13]=z[5]*z[13];
    z[16]=npow(z[3],2);
    z[17]=npow(z[12],2);
    z[21]=z[17] + n<T>(13,48)*z[16];
    z[21]=z[21]*z[18];
    z[22]=n<T>(5,8) + z[12];
    z[22]=z[12]*z[22];
    z[23]=static_cast<T>(1)- n<T>(13,24)*z[3];
    z[23]=z[3]*z[23];
    z[21]=z[21] + n<T>(1,4)*z[23] + n<T>(1,8) + z[22];
    z[21]=z[6]*z[21];
    z[15]=z[15] - n<T>(13,12)*z[3] + n<T>(143,24) + 7*z[12];
    z[15]=n<T>(1,4)*z[15] + z[21];
    z[15]=z[6]*z[15];
    z[21]=static_cast<T>(5)- z[8];
    z[21]=n<T>(1,2)*z[21] + z[9];
    z[21]=z[4]*z[21];
    z[21]=z[21] + n<T>(71,24) - z[9];
    z[15]=n<T>(1,4)*z[21] + z[15];
    z[15]=z[7]*z[15];
    z[16]=z[17] - n<T>(7,96)*z[16];
    z[16]=z[6]*z[16];
    z[16]=z[16] - n<T>(19,48)*z[3] + n<T>(3,16) + 2*z[12];
    z[16]=z[6]*z[16];
    z[21]= - n<T>(3,2)*z[8] + z[9];
    z[21]=z[4]*z[21];
    z[21]=z[21] + n<T>(91,24) - z[9];
    z[15]=z[15] + n<T>(1,4)*z[21] + z[16];
    z[15]=z[7]*z[15];
    z[16]= - n<T>(1,3) + 3*z[1];
    z[21]= - n<T>(1,2) - z[8];
    z[21]=3*z[21] + z[9];
    z[21]=z[4]*z[21];
    z[16]=z[21] + n<T>(1,2)*z[16] - z[9];
    z[21]=z[12] - n<T>(5,6)*z[3];
    z[21]=z[6]*z[21];
    z[16]=n<T>(1,4)*z[16] + z[21];
    z[13]=n<T>(1,2)*z[13] + n<T>(1,4)*z[16] + z[15];
    z[13]=z[5]*z[13];
    z[15]=5*z[12];
    z[16]=static_cast<T>(1)+ z[10];
    z[21]=z[4]*z[10];
    z[16]=z[21] + z[19] + n<T>(1,2)*z[16] - z[15];
    z[19]=n<T>(1,2)*z[3];
    z[21]=static_cast<T>(13)- 11*z[2];
    z[19]=z[21]*z[19];
    z[19]= - static_cast<T>(13)+ z[19];
    z[14]=z[19]*z[14];
    z[19]=z[6]*z[3];
    z[21]= - z[3]*npow(z[2],2);
    z[21]= - n<T>(11,4)*z[2] + z[21];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(13,4) + z[21];
    z[21]=z[21]*z[19];
    z[14]=n<T>(1,12)*z[21] - z[17] + z[14];
    z[14]=z[6]*z[14];
    z[21]= - n<T>(5,4) - z[12];
    z[21]=z[12]*z[21];
    z[14]=z[14] + n<T>(1,4)*z[10] + z[21];
    z[14]=z[6]*z[14];
    z[14]=n<T>(1,4)*z[16] + z[14];
    z[14]=z[7]*z[14];
    z[15]= - n<T>(13,6)*z[3] - z[15] + z[10] + z[20];
    z[16]=z[3]*z[2];
    z[20]=n<T>(1,2)*z[2];
    z[21]=n<T>(1,3) - z[20];
    z[21]=z[21]*z[16];
    z[22]=static_cast<T>(7)- 25*z[2];
    z[21]=n<T>(1,24)*z[22] + z[21];
    z[21]=z[3]*z[21];
    z[21]= - n<T>(11,12) + z[21];
    z[18]=z[21]*z[18];
    z[21]= - static_cast<T>(1)- n<T>(1,2)*z[16];
    z[18]=z[18] + n<T>(7,12)*z[21];
    z[18]=z[3]*z[18];
    z[17]= - z[17] + z[18];
    z[17]=z[6]*z[17];
    z[14]=z[14] + n<T>(1,4)*z[15] + z[17];
    z[14]=z[7]*z[14];
    z[15]=n<T>(7,3) + z[1];
    z[17]= - n<T>(11,3) - z[1];
    z[17]=n<T>(1,4)*z[17] + n<T>(1,3)*z[2];
    z[17]=z[2]*z[17];
    z[15]=n<T>(1,4)*z[15] + z[17];
    z[17]=static_cast<T>(1)- z[20];
    z[17]=z[2]*z[17];
    z[17]= - n<T>(1,2) + z[17];
    z[18]=n<T>(1,3)*z[3];
    z[17]=z[18]*z[1]*z[17];
    z[15]=n<T>(1,2)*z[15] + z[17];
    z[15]=z[3]*z[15];
    z[17]= - n<T>(29,3) + z[2];
    z[15]=n<T>(1,8)*z[17] + z[15];
    z[15]=z[3]*z[15];
    z[17]=static_cast<T>(1)- z[2];
    z[16]=z[17]*z[16];
    z[17]=n<T>(1,2) - z[2];
    z[16]=n<T>(7,4)*z[17] + z[16];
    z[16]=z[16]*z[18];
    z[16]= - n<T>(3,8) + z[16];
    z[16]=z[16]*z[19];
    z[15]=z[16] - n<T>(1,4)*z[8] + z[15];
    z[14]=n<T>(1,2)*z[15] + z[14];

    r += z[13] + n<T>(1,2)*z[14];
 
    return r;
}

template double qqb_2lha_r403(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r403(const std::array<dd_real,30>&);
#endif
