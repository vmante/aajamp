#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1267(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[9];
    z[7]=npow(z[4],2);
    z[8]=n<T>(1,4)*z[7];
    z[9]=n<T>(1,4)*z[4] - z[2];
    z[9]=z[2]*z[9];
    z[9]= - z[8] + z[9];
    z[10]=z[1]*z[2];
    z[9]=z[9]*z[10];
    z[11]=z[5] - 1;
    z[12]=z[11]*z[2];
    z[13]=n<T>(1,2) + z[12];
    z[13]=z[13]*npow(z[2],2);
    z[9]=z[9] - z[8] + z[13];
    z[9]=z[1]*z[9];
    z[13]=n<T>(1,2)*z[4];
    z[14]=z[13] + 1;
    z[15]= - n<T>(1,2)*z[14] + z[2];
    z[15]=z[2]*z[15];
    z[15]= - z[8] + z[15];
    z[15]=z[2]*z[15];
    z[14]= - z[14] + z[2];
    z[14]=z[2]*z[4]*z[14];
    z[7]=n<T>(1,2)*z[7] + z[14];
    z[7]=z[7]*z[10];
    z[7]=n<T>(1,2)*z[7] + z[8] + z[15];
    z[7]=z[1]*z[7];
    z[8]= - static_cast<T>(1)- z[12];
    z[8]=z[2]*z[8];
    z[8]= - z[4] + z[8];
    z[8]=z[2]*z[8];
    z[8]=z[13] + z[8];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[1]*z[7];
    z[8]= - z[5]*z[6];
    z[8]= - static_cast<T>(1)+ z[8];
    z[7]=n<T>(1,4)*z[8] + z[7];
    z[7]=z[3]*z[7];
    z[8]=n<T>(1,2)*z[6] - z[2];
    z[7]=z[7] + n<T>(1,2)*z[8] + z[9];
    z[7]=z[3]*z[7];
    z[8]= - z[1]*z[11]*npow(z[2],3);
    z[8]=z[2] + z[8];
    z[7]=n<T>(1,2)*z[8] + z[7];

    r += n<T>(1,4)*z[7];
 
    return r;
}

template double qqb_2lha_r1267(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1267(const std::array<dd_real,30>&);
#endif
