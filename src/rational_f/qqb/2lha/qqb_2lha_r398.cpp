#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r398(const std::array<T,30>& k) {
  T z[42];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[9];
    z[9]=k[10];
    z[10]=k[5];
    z[11]=k[17];
    z[12]=npow(z[3],2);
    z[13]=3*z[3];
    z[14]=n<T>(11,2) - z[13];
    z[14]=z[14]*z[12];
    z[15]=npow(z[3],4);
    z[16]=z[15]*z[2];
    z[17]=npow(z[3],3);
    z[18]=n<T>(31,4)*z[17] + 5*z[16];
    z[18]=z[2]*z[18];
    z[14]=n<T>(1,4)*z[14] + n<T>(1,3)*z[18];
    z[14]=z[2]*z[14];
    z[18]=z[3] + 1;
    z[19]= - z[18]*z[13];
    z[19]=n<T>(11,6) + z[19];
    z[19]=z[3]*z[19];
    z[20]=n<T>(1,2)*z[4];
    z[21]= - z[20] + n<T>(1,2) + z[3];
    z[21]=z[4]*z[12]*z[21];
    z[14]=n<T>(3,4)*z[21] + n<T>(1,4)*z[19] + z[14];
    z[14]=z[1]*z[14];
    z[19]=3*z[9];
    z[21]=z[19] + 5;
    z[22]=n<T>(1,2)*z[9];
    z[21]=z[21]*z[22];
    z[21]=z[21] + 1;
    z[23]=z[22] + 1;
    z[24]=z[23]*z[9];
    z[25]=z[24] + n<T>(1,2);
    z[26]=z[25]*z[13];
    z[26]=z[26] - z[21];
    z[26]=z[26]*z[12];
    z[17]= - z[2]*z[25]*z[17];
    z[17]=z[26] + z[17];
    z[26]=n<T>(1,2)*z[2];
    z[17]=z[17]*z[26];
    z[27]=z[25]*z[3];
    z[28]= - n<T>(3,2)*z[27] + z[21];
    z[28]=z[3]*z[28];
    z[29]=n<T>(3,4)*z[9];
    z[30]=z[29] + 1;
    z[30]=z[30]*z[9];
    z[30]=z[30] + n<T>(1,4);
    z[28]=z[28] - z[30];
    z[28]=z[3]*z[28];
    z[17]=z[28] + z[17];
    z[17]=z[2]*z[17];
    z[21]=z[27] - z[21];
    z[28]=n<T>(1,2)*z[3];
    z[21]=z[21]*z[28];
    z[21]=z[21] + z[30];
    z[21]=z[3]*z[21];
    z[30]=z[9] + 1;
    z[31]=z[30]*z[9];
    z[17]=z[17] - n<T>(1,4)*z[31] + z[21];
    z[17]=z[8]*z[17];
    z[21]= - z[12]*z[31];
    z[32]=npow(z[9],2);
    z[33]=z[32]*z[3];
    z[34]=z[33] - z[32];
    z[35]=n<T>(1,2)*z[7];
    z[36]=z[35]*z[3];
    z[37]= - z[34]*z[36];
    z[21]=z[37] + n<T>(1,2)*z[32] + z[21];
    z[21]=z[7]*z[21];
    z[18]= - z[18]*z[27];
    z[18]=z[18] + z[21];
    z[18]=z[11]*z[18];
    z[21]=n<T>(1,4) + z[9];
    z[21]=z[9]*z[21];
    z[27]=z[9] + n<T>(5,2);
    z[27]=z[27]*z[9];
    z[37]= - n<T>(3,2) - z[27];
    z[37]=z[3]*z[37];
    z[21]=z[37] - n<T>(7,8) + z[21];
    z[21]=z[3]*z[21];
    z[37]= - n<T>(11,8) - z[9];
    z[37]=z[9]*z[37];
    z[21]=z[21] - n<T>(5,12) + z[37];
    z[21]=z[3]*z[21];
    z[14]=z[17] + n<T>(1,4)*z[32] + z[21] + z[18] + z[14];
    z[17]=n<T>(1,8)*z[9];
    z[18]= - n<T>(1,2) - z[19];
    z[18]=z[18]*z[17];
    z[21]=5*z[9];
    z[37]=static_cast<T>(47)+ z[21];
    z[37]=z[37]*z[22];
    z[37]=static_cast<T>(41)+ z[37];
    z[37]=z[3]*z[37];
    z[37]=n<T>(1,6)*z[37] - n<T>(13,8) - z[24];
    z[37]=z[3]*z[37];
    z[18]=z[18] + z[37];
    z[18]=z[18]*z[28];
    z[37]=5*z[3];
    z[38]=static_cast<T>(1)+ n<T>(1,3)*z[9];
    z[38]=z[9]*z[38];
    z[38]=static_cast<T>(1)+ z[38];
    z[38]=z[38]*z[37];
    z[39]= - n<T>(65,6) - z[24];
    z[38]=n<T>(1,4)*z[39] + z[38];
    z[38]=z[3]*z[38];
    z[38]= - n<T>(1,16)*z[31] + z[38];
    z[38]=z[38]*z[12];
    z[15]=n<T>(5,3)*z[15];
    z[15]= - z[7]*z[15];
    z[15]= - n<T>(5,3)*z[16] + z[38] + z[15];
    z[15]=z[15]*z[26];
    z[16]=z[25]*z[37];
    z[26]= - static_cast<T>(3)- n<T>(61,12)*z[9];
    z[26]=z[9]*z[26];
    z[26]= - n<T>(37,12) + z[26];
    z[16]=n<T>(1,4)*z[26] + z[16];
    z[16]=z[3]*z[16];
    z[16]= - n<T>(1,16)*z[24] + z[16];
    z[16]=z[3]*z[16];
    z[16]=n<T>(1,32)*z[32] + z[16];
    z[24]=z[7]*z[3];
    z[16]=z[16]*z[24];
    z[15]=z[15] + z[18] + z[16];
    z[15]=z[2]*z[15];
    z[16]=n<T>(7,2)*z[9];
    z[18]= - static_cast<T>(11)- z[16];
    z[18]=z[18]*z[22];
    z[18]= - n<T>(7,3) + z[18];
    z[26]=z[30]*z[3];
    z[37]= - n<T>(7,3) - n<T>(9,4)*z[9];
    z[37]=z[9]*z[37];
    z[26]= - n<T>(3,2)*z[26] + n<T>(37,24) + z[37];
    z[26]=z[3]*z[26];
    z[18]=n<T>(1,4)*z[18] + z[26];
    z[18]=z[18]*z[28];
    z[26]=z[9] - 1;
    z[28]=n<T>(5,16)*z[9];
    z[37]=z[26]*z[28];
    z[38]=n<T>(1,4)*z[9];
    z[39]= - static_cast<T>(5)- n<T>(113,6)*z[9];
    z[39]=z[39]*z[38];
    z[40]=z[31]*z[3];
    z[39]=z[39] + 5*z[40];
    z[39]=z[3]*z[39];
    z[37]=z[37] + z[39];
    z[37]=z[3]*z[37];
    z[39]=n<T>(1,8)*z[32];
    z[37]=z[39] + z[37];
    z[36]=z[37]*z[36];
    z[37]= - static_cast<T>(1)- n<T>(4,3)*z[9];
    z[37]=z[9]*z[37];
    z[41]=static_cast<T>(13)+ z[21];
    z[41]=z[9]*z[41];
    z[41]=static_cast<T>(11)+ z[41];
    z[41]=z[3]*z[41];
    z[37]=n<T>(1,12)*z[41] - n<T>(13,24) + z[37];
    z[37]=z[37]*z[12];
    z[36]=z[36] - z[39] + z[37];
    z[36]=z[7]*z[36];
    z[15]=z[15] + z[18] + z[36];
    z[15]=z[2]*z[15];
    z[16]= - static_cast<T>(3)- z[16];
    z[16]=z[16]*z[38];
    z[18]=z[23]*z[19];
    z[36]=z[18] + 1;
    z[37]=n<T>(3,2)*z[3];
    z[37]=z[36]*z[37];
    z[37]=z[37] - n<T>(3,8) - z[31];
    z[37]=z[3]*z[37];
    z[16]=z[16] + z[37];
    z[16]=z[3]*z[16];
    z[37]= - static_cast<T>(9)- 13*z[9];
    z[37]=z[37]*z[22];
    z[37]=z[37] + 9*z[40];
    z[37]=z[3]*z[37];
    z[37]= - z[32] + z[37];
    z[37]=z[3]*z[37];
    z[12]=z[7]*z[34]*z[12];
    z[12]=z[37] + 3*z[12];
    z[12]=z[7]*z[12];
    z[12]=n<T>(1,4)*z[12] + n<T>(5,16)*z[32] + z[16];
    z[12]=z[7]*z[12];
    z[16]=z[9] + 3;
    z[37]=z[16]*z[22];
    z[37]=z[37] + 1;
    z[13]=z[37]*z[13];
    z[26]= - z[9]*z[26];
    z[26]=static_cast<T>(5)+ z[26];
    z[26]=n<T>(1,4)*z[26] + z[13];
    z[26]=z[3]*z[26];
    z[37]= - static_cast<T>(9)- z[21];
    z[37]=z[9]*z[37];
    z[37]= - static_cast<T>(1)+ z[37];
    z[26]=n<T>(1,4)*z[37] + z[26];
    z[26]=z[3]*z[26];
    z[26]=n<T>(5,8)*z[31] + z[26];
    z[12]=n<T>(1,2)*z[26] + z[12];
    z[21]=z[23]*z[21];
    z[13]= - z[13] + static_cast<T>(1)+ z[21];
    z[13]=z[3]*z[13];
    z[13]= - n<T>(5,4)*z[31] + z[13];
    z[21]=static_cast<T>(3)+ n<T>(11,2)*z[9];
    z[21]=z[9]*z[21];
    z[21]=z[21] - n<T>(9,2)*z[40];
    z[21]=z[3]*z[21];
    z[23]=z[34]*z[24];
    z[21]= - n<T>(3,2)*z[23] - n<T>(5,4)*z[32] + z[21];
    z[21]=z[7]*z[21];
    z[23]=z[3]*z[36];
    z[26]=static_cast<T>(1)+ n<T>(13,16)*z[9];
    z[26]=z[9]*z[26];
    z[23]= - n<T>(3,8)*z[23] + n<T>(3,16) + z[26];
    z[23]=z[3]*z[23];
    z[26]=z[9] + n<T>(1,2);
    z[26]=z[26]*z[9];
    z[21]=n<T>(1,8)*z[21] - n<T>(5,16)*z[26] + z[23];
    z[21]=z[7]*z[21];
    z[13]=n<T>(1,8)*z[13] + z[21];
    z[13]=z[4]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[4]*z[12];
    z[13]= - static_cast<T>(1)+ z[22];
    z[13]=z[13]*z[28];
    z[21]=n<T>(11,12) - z[27];
    z[23]= - n<T>(9,4) - z[9];
    z[23]=z[9]*z[23];
    z[23]= - n<T>(3,4) + z[23];
    z[23]=z[3]*z[23];
    z[21]=n<T>(1,4)*z[21] + z[23];
    z[21]=z[3]*z[21];
    z[13]=z[13] + z[21];
    z[13]=z[3]*z[13];
    z[21]= - static_cast<T>(9)- n<T>(7,3)*z[9];
    z[21]=z[21]*z[3]*z[9];
    z[21]= - n<T>(17,3)*z[32] + z[21];
    z[21]=z[3]*z[21];
    z[21]=n<T>(5,2)*z[26] + z[21];
    z[21]=z[3]*z[21];
    z[21]= - z[32] + z[21];
    z[23]= - n<T>(11,8)*z[32] + z[33];
    z[23]=z[3]*z[23];
    z[28]=n<T>(1,16)*z[32];
    z[23]=z[28] + n<T>(1,3)*z[23];
    z[23]=z[3]*z[23];
    z[23]=z[28] + z[23];
    z[23]=z[23]*z[24];
    z[21]=n<T>(1,4)*z[21] + 5*z[23];
    z[21]=z[21]*z[35];
    z[13]=z[21] - z[17] + z[13];
    z[13]=z[7]*z[13];
    z[21]=z[7]*z[9];
    z[23]=n<T>(3,2) + z[9];
    z[23]=z[23]*z[21];
    z[22]=z[2]*z[30]*z[22];
    z[22]=z[22] + z[23] + n<T>(1,2) + z[27];
    z[22]=z[2]*z[22];
    z[19]=static_cast<T>(11)+ z[19];
    z[17]=z[19]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[19]=z[16]*z[21];
    z[23]=z[38] + 1;
    z[23]=z[23]*z[9];
    z[19]=n<T>(1,8)*z[19] + n<T>(3,8) + z[23];
    z[19]=z[7]*z[19];
    z[17]=n<T>(1,4)*z[22] + n<T>(1,2)*z[17] + z[19];
    z[17]=z[2]*z[17];
    z[19]=3*z[30] + z[21];
    z[19]=z[19]*z[35];
    z[19]=z[19] + static_cast<T>(3)+ n<T>(7,4)*z[9];
    z[19]=z[7]*z[19];
    z[19]=z[19] + n<T>(7,4) + z[23];
    z[17]=n<T>(1,4)*z[19] + z[17];
    z[17]=z[6]*z[17];
    z[19]=z[35]*z[32];
    z[21]= - z[26] + z[19];
    z[21]=z[2]*z[21];
    z[22]=z[32]*npow(z[7],2);
    z[18]=z[21] - z[18] + z[22];
    z[18]=z[2]*z[18];
    z[21]=z[26] + z[19];
    z[21]=z[7]*z[21];
    z[22]= - static_cast<T>(1)+ z[29];
    z[22]=z[9]*z[22];
    z[21]=z[21] + static_cast<T>(1)+ z[22];
    z[21]=z[7]*z[21];
    z[16]=z[9]*z[16];
    z[16]= - static_cast<T>(1)- n<T>(3,2)*z[16];
    z[16]=z[18] + n<T>(1,2)*z[16] + z[21];
    z[16]=n<T>(1,4)*z[16] + z[17];
    z[17]=n<T>(5,4)*z[6];
    z[16]=z[16]*z[17];
    z[18]=z[32]*z[7];
    z[21]= - z[31] - z[18];
    z[21]=z[7]*z[21];
    z[22]=z[31] + z[19];
    z[22]=z[7]*z[22];
    z[22]=z[22] + z[25];
    z[22]=z[4]*z[22]*z[35];
    z[21]=z[21] + z[22];
    z[20]=z[21]*z[20];
    z[21]=static_cast<T>(1)- n<T>(1,4)*z[6];
    z[17]=z[17]*z[18]*z[21];
    z[18]=z[9] + z[19];
    z[18]=z[7]*z[18];
    z[19]=static_cast<T>(1)+ n<T>(19,8)*z[32];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[7]*z[18];
    z[17]=z[17] + z[18] + z[20];
    z[17]=z[10]*z[17];
    z[18]= - static_cast<T>(1)- z[7];
    z[18]=3*z[18] - z[2];
    z[18]=z[6]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[6]*z[18];
    z[19]=z[5]*npow(z[6],2);
    z[18]=z[18] + z[19];
    z[18]=z[5]*z[18];

    r += z[12] + z[13] + n<T>(1,2)*z[14] + z[15] + z[16] + n<T>(1,4)*z[17] + n<T>(5,32)*z[18];
 
    return r;
}

template double qqb_2lha_r398(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r398(const std::array<dd_real,30>&);
#endif
