#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r164(const std::array<T,30>& k) {
  T z[29];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=k[5];
    z[8]=3*z[3];
    z[9]=n<T>(1,2)*z[3];
    z[10]=static_cast<T>(1)- z[9];
    z[10]=z[10]*z[8];
    z[11]=n<T>(1,2)*z[5];
    z[12]=npow(z[3],2);
    z[13]=z[11]*z[12];
    z[14]=n<T>(3,2)*z[3];
    z[15]= - static_cast<T>(1)+ z[14];
    z[15]=z[3]*z[15];
    z[15]=z[15] - z[13];
    z[15]=z[5]*z[15];
    z[10]=z[15] - n<T>(1,2) + z[10];
    z[10]=z[5]*z[10];
    z[15]= - static_cast<T>(3)+ z[9];
    z[15]=z[3]*z[15];
    z[10]=z[10] + n<T>(3,2) + z[15];
    z[10]=z[5]*z[10];
    z[10]=z[10] - n<T>(3,2) + z[3];
    z[10]=z[5]*z[10];
    z[10]=n<T>(1,2) + z[10];
    z[15]=n<T>(1,2)*z[4];
    z[10]=z[10]*z[15];
    z[16]=z[12]*z[5];
    z[17]=n<T>(3,2)*z[16];
    z[18]= - n<T>(5,2) + z[8];
    z[18]=z[3]*z[18];
    z[18]=z[18] - z[17];
    z[18]=z[5]*z[18];
    z[19]=static_cast<T>(5)- z[14];
    z[19]=z[3]*z[19];
    z[18]=z[18] - static_cast<T>(1)+ z[19];
    z[18]=z[18]*z[11];
    z[19]= - static_cast<T>(1)+ n<T>(5,4)*z[3];
    z[18]=z[18] - z[19];
    z[18]=z[5]*z[18];
    z[10]=z[10] - n<T>(1,2) + z[18];
    z[10]=z[4]*z[10];
    z[18]=z[3] - 1;
    z[20]=z[18]*z[3];
    z[21]=z[20] - z[16];
    z[21]=z[5]*z[21];
    z[21]=z[3] + z[21];
    z[21]=z[21]*z[11];
    z[10]=z[21] + z[10];
    z[10]=z[7]*z[10];
    z[21]=z[5] - 1;
    z[22]=n<T>(1,2)*z[1];
    z[23]=z[21]*z[22];
    z[24]=z[5] - 3;
    z[25]= - z[5]*z[24];
    z[25]= - static_cast<T>(3)+ z[25];
    z[25]=z[5]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[26]=z[11] - 1;
    z[26]=z[26]*z[5];
    z[26]=z[26] + n<T>(1,2);
    z[27]=z[1]*z[26];
    z[25]=n<T>(1,2)*z[25] + z[27];
    z[25]=z[2]*z[25];
    z[23]=z[25] + z[23] - z[26];
    z[23]=z[6]*z[23]*npow(z[2],2);
    z[17]= - z[20] + z[17];
    z[17]=z[5]*z[17];
    z[25]=3*z[5];
    z[27]=z[1] - z[25];
    z[27]=z[12]*z[27];
    z[27]=z[20] + z[27];
    z[27]=z[27]*z[22];
    z[10]=z[23] + z[10] + z[27] - z[9] + z[17];
    z[14]=z[14] + 1;
    z[17]=z[14]*z[3];
    z[23]=n<T>(7,2) - z[17];
    z[20]=n<T>(1,2) - z[20];
    z[20]=z[5]*z[20];
    z[20]=n<T>(1,2)*z[23] + z[20];
    z[23]=z[3] - n<T>(1,4);
    z[27]=z[1]*z[3];
    z[28]=z[23]*z[27];
    z[20]=n<T>(1,2)*z[20] + z[28];
    z[20]=z[1]*z[20];
    z[28]= - static_cast<T>(1)- z[9];
    z[28]=z[5]*z[28];
    z[28]= - z[3] + z[28];
    z[28]=z[5]*z[28];
    z[14]=z[28] + z[14];
    z[14]=n<T>(1,2)*z[14] + z[20];
    z[14]=z[1]*z[14];
    z[18]= - z[18]*z[21];
    z[20]=static_cast<T>(3)- z[3];
    z[20]=z[20]*z[27];
    z[18]=3*z[18] + z[20];
    z[18]=z[18]*z[22];
    z[18]= - 3*z[26] + z[18];
    z[18]=z[1]*z[18];
    z[20]=z[12]*npow(z[1],4);
    z[21]= - z[15]*z[20];
    z[18]=z[18] + z[21];
    z[21]=n<T>(1,2)*z[2];
    z[18]=z[18]*z[21];
    z[26]=z[22]*z[12];
    z[27]= - n<T>(3,4) - z[3];
    z[27]=z[3]*z[27];
    z[27]=z[27] + z[26];
    z[28]=npow(z[1],3);
    z[27]=z[27]*z[28];
    z[20]=z[4]*z[20];
    z[20]=z[27] + n<T>(1,8)*z[20];
    z[20]=z[4]*z[20];
    z[27]= - n<T>(7,2) + z[5];
    z[27]=z[5]*z[27];
    z[27]=static_cast<T>(1)+ n<T>(1,4)*z[27];
    z[27]=z[5]*z[27];
    z[14]=z[18] + z[20] + z[14] - n<T>(3,8) + z[27];
    z[14]=z[14]*z[21];
    z[18]=n<T>(5,2)*z[3];
    z[20]=z[18] + 1;
    z[20]=z[20]*z[3];
    z[21]= - z[1]*z[12];
    z[21]=z[20] + z[21];
    z[15]=z[21]*z[28]*z[15];
    z[21]=static_cast<T>(1)+ n<T>(7,2)*z[3];
    z[21]=z[3]*z[21];
    z[21]=z[21] - z[26];
    z[21]=z[1]*z[21];
    z[8]=z[8] + n<T>(17,4);
    z[26]= - z[3]*z[8];
    z[21]=z[21] - n<T>(1,2) + z[26];
    z[21]=z[21]*npow(z[1],2);
    z[15]=z[21] + z[15];
    z[15]=z[4]*z[15];
    z[21]= - z[9] + z[24];
    z[21]=z[21]*z[11];
    z[15]=z[15] + z[21] + static_cast<T>(1)+ n<T>(1,4)*z[3];
    z[13]= - z[13] - static_cast<T>(1)- z[17];
    z[13]=z[13]*z[11];
    z[17]= - static_cast<T>(1)- n<T>(1,8)*z[3];
    z[17]=z[3]*z[17];
    z[13]=z[13] + n<T>(1,2) + z[17];
    z[17]=z[1] - z[5];
    z[17]= - n<T>(3,8)*z[17];
    z[17]=z[12]*z[17];
    z[21]=n<T>(1,2) + z[3];
    z[21]=z[3]*z[21];
    z[17]=n<T>(1,8) + z[21] + z[17];
    z[17]=z[1]*z[17];
    z[13]=n<T>(1,2)*z[13] + z[17];
    z[13]=z[1]*z[13];
    z[13]=z[14] + z[13] + n<T>(1,4)*z[15];
    z[13]=z[2]*z[13];
    z[14]= - n<T>(11,8)*z[1] + z[25];
    z[14]=z[12]*z[14];
    z[8]=z[8]*z[9];
    z[8]=z[8] + z[14];
    z[8]=z[1]*z[8];
    z[9]= - n<T>(27,8) + z[3];
    z[9]=z[3]*z[9];
    z[9]=z[9] - n<T>(13,4)*z[16];
    z[9]=z[5]*z[9];
    z[8]=z[8] - n<T>(3,4) + z[9];
    z[8]=z[8]*z[22];
    z[9]= - static_cast<T>(3)+ z[18];
    z[9]=z[3]*z[9];
    z[9]=z[9] - n<T>(5,2)*z[16];
    z[9]=z[5]*z[9];
    z[9]=3*z[23] + z[9];
    z[9]=z[5]*z[9];
    z[9]=n<T>(3,4) + z[9];
    z[14]=z[22] - n<T>(5,2)*z[5];
    z[12]=z[12]*z[14];
    z[12]= - z[20] + z[12];
    z[12]=z[1]*z[12];
    z[14]=z[3] + n<T>(5,4)*z[16];
    z[14]=z[5]*z[14];
    z[12]=n<T>(1,4)*z[12] + n<T>(1,8) + z[14];
    z[12]=z[1]*z[12];
    z[9]=n<T>(1,2)*z[9] + z[12];
    z[9]=z[9]*z[22];
    z[12]= - z[3]*z[19];
    z[12]=z[12] + n<T>(5,8)*z[16];
    z[11]=z[12]*z[11];
    z[12]= - static_cast<T>(1)+ n<T>(5,16)*z[3];
    z[12]=z[3]*z[12];
    z[11]=z[11] + n<T>(3,16) + z[12];
    z[11]=z[5]*z[11];
    z[12]= - n<T>(3,4) + z[3];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[11]=z[5]*z[11];
    z[9]=z[9] + n<T>(3,16) + z[11];
    z[9]=z[4]*z[9];
    z[11]=n<T>(19,16) - z[3];
    z[11]=z[3]*z[11];
    z[11]=z[11] + n<T>(7,8)*z[16];
    z[11]=z[5]*z[11];
    z[12]= - n<T>(19,2) + z[3];
    z[12]=z[3]*z[12];
    z[12]=static_cast<T>(3)+ z[12];
    z[11]=n<T>(1,8)*z[12] + z[11];
    z[11]=z[5]*z[11];
    z[8]=z[9] + z[8] - n<T>(3,8) + z[11];
    z[8]=z[4]*z[8];
    z[8]=z[13] + z[8] + n<T>(1,4)*z[10];

    r += n<T>(3,4)*z[8];
 
    return r;
}

template double qqb_2lha_r164(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r164(const std::array<dd_real,30>&);
#endif
