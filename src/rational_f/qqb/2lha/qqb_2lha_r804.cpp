#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r804(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=n<T>(1,2)*z[4];
    z[6]=static_cast<T>(3)+ z[1];
    z[6]=z[6]*z[5];
    z[7]=npow(z[1],2);
    z[7]=z[7] - 1;
    z[8]=z[1] - z[5];
    z[8]=z[4]*z[8];
    z[8]= - n<T>(1,2)*z[7] + z[8];
    z[8]=z[2]*z[8];
    z[6]=z[8] + z[6] - z[7];
    z[6]=z[4]*z[6];
    z[7]=z[1] - 3;
    z[7]=z[7]*z[1];
    z[7]=z[7] + 3;
    z[7]=z[7]*z[1];
    z[7]=z[7] - 1;
    z[7]=n<T>(1,2)*z[7];
    z[6]=z[7] + z[6];
    z[8]=n<T>(1,2)*z[2];
    z[6]=z[6]*z[8];
    z[9]=z[1] - 1;
    z[10]=z[9]*z[5];
    z[11]= - static_cast<T>(1)+ n<T>(1,2)*z[1];
    z[11]=z[11]*z[1];
    z[11]=z[11] + n<T>(1,2);
    z[10]=z[10] - z[11];
    z[6]=z[6] + z[10];
    z[12]=z[4]*z[11];
    z[12]= - z[7] + z[12];
    z[10]=z[4]*z[10];
    z[13]=z[4] - z[9];
    z[13]=z[2]*z[13]*npow(z[4],2);
    z[10]=z[10] + n<T>(1,4)*z[13];
    z[10]=z[2]*z[10];
    z[10]=n<T>(1,2)*z[12] + z[10];
    z[10]=z[3]*z[10];
    z[6]=n<T>(1,2)*z[6] + z[10];
    z[6]=z[3]*z[6];
    z[10]=z[5] - z[9];
    z[10]=z[4]*z[10];
    z[9]=3*z[9] - z[4];
    z[5]=z[9]*z[5];
    z[5]= - 3*z[11] + z[5];
    z[5]=z[4]*z[5];
    z[5]=z[7] + z[5];
    z[5]=z[5]*z[8];
    z[5]=z[5] + z[10] + z[11];
    z[5]=z[5]*z[8];
    z[5]=z[5] + z[6];

    r += z[5]*z[3];
 
    return r;
}

template double qqb_2lha_r804(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r804(const std::array<dd_real,30>&);
#endif
