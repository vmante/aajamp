#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2202(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[17];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[4];
    z[8]=k[11];
    z[9]=k[15];
    z[10]=n<T>(3,2)*z[3];
    z[11]= - static_cast<T>(11)+ z[10];
    z[12]=static_cast<T>(13)- n<T>(33,8)*z[9];
    z[12]=z[9]*z[12];
    z[13]=n<T>(3,8)*z[3];
    z[14]=z[13] + n<T>(57,8)*z[9];
    z[15]=static_cast<T>(5)- z[14];
    z[15]=z[6]*z[15];
    z[11]=z[15] + n<T>(1,2)*z[11] + z[12];
    z[11]=z[6]*z[11];
    z[12]=npow(z[2],2);
    z[15]=z[12] - 1;
    z[13]=z[13]*z[15];
    z[15]=n<T>(1,2)*z[9];
    z[16]=11*z[9];
    z[17]= - static_cast<T>(15)+ z[16];
    z[17]=z[17]*z[15];
    z[18]= - static_cast<T>(1)+ n<T>(11,2)*z[9];
    z[19]=z[18]*z[9];
    z[20]=n<T>(1,4)*z[5];
    z[21]=z[20]*z[19];
    z[22]=n<T>(3,2)*z[4];
    z[23]=static_cast<T>(1)+ n<T>(3,4)*z[2];
    z[23]=z[2]*z[23];
    z[23]=n<T>(1,4) + z[23];
    z[23]=z[23]*z[22];
    z[24]= - static_cast<T>(1)+ n<T>(3,8)*z[2];
    z[24]=z[2]*z[24];
    z[23]=z[23] - n<T>(5,8) + z[24];
    z[23]=z[4]*z[23];
    z[11]=z[11] + z[23] + z[21] + z[17] + static_cast<T>(1)+ z[13];
    z[11]=z[8]*z[11];
    z[13]=n<T>(1,2)*z[2];
    z[17]=z[13]*z[3];
    z[19]=z[19] + z[17];
    z[21]=z[13] + 1;
    z[23]=3*z[4];
    z[21]=z[21]*z[23];
    z[24]= - z[21] + static_cast<T>(1)- z[13];
    z[24]=z[4]*z[24];
    z[24]=z[24] - z[19];
    z[25]=z[9] - 1;
    z[26]=z[25]*z[16];
    z[27]=n<T>(1,2)*z[3];
    z[28]= - static_cast<T>(1)+ z[23];
    z[28]=z[4]*z[28];
    z[26]=z[28] - z[27] + z[26];
    z[28]=13*z[9] + static_cast<T>(11)+ z[27];
    z[28]=n<T>(1,2)*z[28] - 10*z[6];
    z[28]=z[6]*z[28];
    z[26]=n<T>(1,2)*z[26] + z[28];
    z[26]=z[6]*z[26];
    z[11]=z[11] + n<T>(1,2)*z[24] + z[26];
    z[11]=z[8]*z[11];
    z[24]=z[2] - 1;
    z[26]= - n<T>(1,2)*z[24] - z[21];
    z[26]=z[4]*z[26];
    z[19]=z[26] - z[27] - z[19];
    z[26]=n<T>(1,4)*z[3];
    z[28]=n<T>(11,8)*z[9];
    z[29]=static_cast<T>(5)- z[28];
    z[29]=z[9]*z[29];
    z[30]= - z[3] - 19*z[9];
    z[30]=z[6]*z[30];
    z[29]=n<T>(1,8)*z[30] + z[26] + z[29];
    z[29]=z[6]*z[29];
    z[30]=n<T>(1,2)*z[4];
    z[31]=static_cast<T>(1)+ z[23];
    z[31]=z[31]*z[30];
    z[32]= - n<T>(23,2) + z[16];
    z[32]=z[9]*z[32];
    z[31]=z[32] + z[31];
    z[29]=n<T>(1,4)*z[31] + z[29];
    z[29]=z[6]*z[29];
    z[19]=n<T>(1,4)*z[19] + z[29];
    z[19]=z[7]*z[19]*npow(z[8],2);
    z[11]=z[11] + z[19];
    z[11]=z[7]*z[11];
    z[19]=static_cast<T>(13)- 3*z[2];
    z[19]=z[19]*z[13];
    z[19]=static_cast<T>(3)+ z[19];
    z[19]=z[2]*z[19];
    z[12]=z[12]*z[23];
    z[29]=n<T>(3,2)*z[2];
    z[31]= - static_cast<T>(1)- z[29];
    z[31]=z[31]*z[12];
    z[19]=z[19] + z[31];
    z[19]=z[19]*z[30];
    z[31]=z[16] - 13;
    z[15]=z[31]*z[15];
    z[15]=z[15] + 1;
    z[15]=z[15]*z[5];
    z[31]=static_cast<T>(5)- n<T>(3,2)*z[9];
    z[31]=z[31]*z[16];
    z[32]= - static_cast<T>(97)+ 3*z[3];
    z[31]=n<T>(1,2)*z[32] + z[31];
    z[24]=z[3]*z[24];
    z[29]= - z[24]*z[29];
    z[10]=z[29] - static_cast<T>(5)+ z[10];
    z[10]=z[10]*z[13];
    z[10]=z[19] + z[10] + n<T>(1,2)*z[31] - z[15];
    z[19]=n<T>(5,2)*z[5];
    z[14]= - z[19] + static_cast<T>(10)- z[14];
    z[14]=z[6]*z[14];
    z[10]=n<T>(1,2)*z[10] + z[14];
    z[10]=z[8]*z[10];
    z[14]=n<T>(1,2)*z[5];
    z[18]=z[18]*z[14];
    z[29]= - static_cast<T>(71)+ 55*z[9];
    z[18]=z[18] + n<T>(1,4)*z[29];
    z[18]=z[9]*z[18];
    z[29]=n<T>(5,4) + z[2];
    z[29]=z[2]*z[29];
    z[29]=n<T>(3,4) + z[29];
    z[29]=z[29]*z[23];
    z[31]= - n<T>(7,2) + z[2];
    z[31]=z[2]*z[31];
    z[29]=z[29] - n<T>(11,4) + z[31];
    z[29]=z[4]*z[29];
    z[24]=z[2]*z[24];
    z[18]=z[29] + z[24] + n<T>(5,2) - z[3] + z[18];
    z[24]=5*z[6];
    z[29]= - n<T>(5,2) + z[5];
    z[29]=z[29]*z[24];
    z[29]=z[29] + 10*z[9] + static_cast<T>(3)+ z[27];
    z[29]=z[6]*z[29];
    z[10]=z[10] + n<T>(1,2)*z[18] + z[29];
    z[10]=z[8]*z[10];
    z[18]=static_cast<T>(3)- z[2];
    z[18]=n<T>(1,2)*z[18] - z[21];
    z[18]=z[4]*z[18];
    z[16]=static_cast<T>(3)- z[16];
    z[16]=z[9]*z[16];
    z[16]=z[18] - z[17] + z[27] + z[16];
    z[18]=static_cast<T>(7)- 3*z[5];
    z[18]=z[18]*z[14];
    z[21]=static_cast<T>(2)- z[14];
    z[21]=z[5]*z[21];
    z[21]= - static_cast<T>(3)+ z[21];
    z[21]=z[5]*z[21]*z[24];
    z[18]=z[21] + static_cast<T>(4)+ z[18];
    z[18]=z[6]*z[18];
    z[21]= - static_cast<T>(5)+ 9*z[4];
    z[21]=z[4]*z[21];
    z[18]=z[18] + n<T>(1,8)*z[21] + z[20] - static_cast<T>(1)- n<T>(5,4)*z[9];
    z[18]=z[6]*z[18];
    z[10]=z[11] + z[10] + n<T>(1,4)*z[16] + z[18];
    z[10]=z[7]*z[10];
    z[11]= - static_cast<T>(19)+ 39*z[9];
    z[11]=n<T>(1,2)*z[11] - z[15];
    z[15]= - n<T>(1,2) - z[2];
    z[12]=z[15]*z[12];
    z[15]=static_cast<T>(5)- z[2];
    z[15]=z[2]*z[15];
    z[15]=n<T>(3,2) + z[15];
    z[15]=z[2]*z[15];
    z[12]=z[15] + z[12];
    z[12]=z[12]*z[30];
    z[15]= - static_cast<T>(1)+ z[5];
    z[15]=z[15]*z[24];
    z[16]=z[17] - z[3];
    z[17]= - z[2]*z[16];
    z[17]= - n<T>(3,2) + z[17];
    z[17]=z[2]*z[17];
    z[11]=z[15] + z[12] + n<T>(1,2)*z[11] + z[17];
    z[12]=z[16]*z[13];
    z[15]=static_cast<T>(1)+ z[12];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(15)+ z[5];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[15]*z[13];
    z[16]= - static_cast<T>(3)+ z[13];
    z[16]=z[16]*npow(z[2],3);
    z[17]=npow(z[2],4)*z[22];
    z[16]=z[16] + z[17];
    z[17]=n<T>(1,4)*z[4];
    z[16]=z[16]*z[17];
    z[18]= - static_cast<T>(3)+ z[28];
    z[18]=z[9]*z[18];
    z[18]=n<T>(13,8) + z[18];
    z[18]=z[5]*z[18];
    z[15]=z[16] + z[15] - n<T>(15,4)*z[25] + z[18];
    z[15]=z[8]*z[15];
    z[11]=n<T>(1,2)*z[11] + z[15];
    z[11]=z[8]*z[11];
    z[15]=static_cast<T>(1)+ z[2];
    z[15]=z[4]*z[15];
    z[15]=n<T>(3,4)*z[15] - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[15]=z[2]*z[15];
    z[16]=n<T>(1,2)*z[1];
    z[15]=z[16] + z[15];
    z[15]=z[4]*z[15];
    z[18]=z[5] - z[1];
    z[20]= - n<T>(9,2)*z[9] - static_cast<T>(1)+ z[27];
    z[12]=z[15] + z[12] + n<T>(1,2)*z[20] + z[18];
    z[15]=z[4] - 1;
    z[15]=z[1]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[22];
    z[20]=z[16] + 1;
    z[21]=n<T>(3,2) - z[18];
    z[21]=z[5]*z[21];
    z[15]=z[15] + 5*z[21] + z[20];
    z[21]=z[1] + 1;
    z[25]=static_cast<T>(3)- z[18];
    z[25]=z[5]*z[25];
    z[25]= - 2*z[21] + z[25];
    z[28]=npow(z[5],2);
    z[25]=z[25]*z[28]*z[24];
    z[29]= - static_cast<T>(43)- 33*z[1];
    z[18]=n<T>(75,4) - 8*z[18];
    z[18]=z[5]*z[18];
    z[18]=n<T>(1,4)*z[29] + z[18];
    z[18]=z[5]*z[18];
    z[18]=z[18] + z[25];
    z[18]=z[6]*z[18];
    z[15]=n<T>(1,2)*z[15] + z[18];
    z[15]=z[6]*z[15];
    z[10]=z[10] + z[11] + n<T>(1,2)*z[12] + z[15];
    z[10]=z[7]*z[10];
    z[11]=5*z[1];
    z[12]=static_cast<T>(13)- z[11];
    z[12]=z[1]*z[12];
    z[15]= - static_cast<T>(7)+ z[1];
    z[15]=z[1]*z[15];
    z[15]=static_cast<T>(3)+ z[15];
    z[15]=z[2]*z[15];
    z[12]=z[15] - static_cast<T>(3)+ z[12];
    z[15]=static_cast<T>(1)- z[1];
    z[13]=z[15]*z[13];
    z[13]=z[13] - n<T>(1,2) + z[1];
    z[13]=z[23]*z[1]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];
    z[12]=z[12]*z[30];
    z[13]= - static_cast<T>(5)+ z[27];
    z[13]=z[13]*z[16];
    z[13]=z[13] - static_cast<T>(5)+ z[26];
    z[13]=z[1]*z[13];
    z[11]= - z[19] + n<T>(7,2) + z[11];
    z[11]=z[5]*z[11];
    z[15]= - z[26]*z[21];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[1]*z[15];
    z[15]= - z[14] - n<T>(1,2) + z[15];
    z[15]=z[2]*z[15];
    z[11]=z[12] + z[15] + z[11] + n<T>(3,4) + z[13];
    z[12]=13*z[1];
    z[13]= - static_cast<T>(21)- z[12];
    z[13]=z[13]*z[16];
    z[12]= - n<T>(13,2)*z[5] + n<T>(21,2) + z[12];
    z[12]=z[5]*z[12];
    z[12]=z[12] - static_cast<T>(4)+ z[13];
    z[12]=z[12]*z[28];
    z[13]= - z[14] + z[21];
    z[13]=z[5]*z[13];
    z[14]= - z[1]*z[20];
    z[13]=z[13] - n<T>(1,2) + z[14];
    z[13]=z[13]*npow(z[5],3)*z[24];
    z[12]=z[12] + z[13];
    z[12]=z[6]*z[12];
    z[13]= - n<T>(3,2) + z[1];
    z[13]=z[13]*z[16];
    z[14]=n<T>(7,2)*z[1];
    z[15]= - static_cast<T>(5)- z[14];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(11,4) + 3*z[15];
    z[16]= - n<T>(3,4)*z[5] + static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[16]=z[5]*z[16];
    z[15]=n<T>(1,2)*z[15] + 7*z[16];
    z[15]=z[5]*z[15];
    z[14]=static_cast<T>(3)- z[14];
    z[14]=z[1]*z[14];
    z[16]=npow(z[1],2)*z[22];
    z[14]=z[14] + z[16];
    z[14]=z[14]*z[17];
    z[12]=z[12] + z[14] + z[13] + z[15];
    z[12]=z[6]*z[12];

    r += z[10] + n<T>(1,2)*z[11] + z[12];
 
    return r;
}

template double qqb_2lha_r2202(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2202(const std::array<dd_real,30>&);
#endif
