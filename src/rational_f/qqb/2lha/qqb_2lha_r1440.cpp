#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1440(const std::array<T,30>& k) {
  T z[54];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[12];
    z[7]=k[20];
    z[8]=k[6];
    z[9]=k[17];
    z[10]=k[7];
    z[11]=k[9];
    z[12]=k[10];
    z[13]=n<T>(1,2)*z[3];
    z[14]=z[13] + 1;
    z[15]=n<T>(1,2)*z[2];
    z[16]= - z[3]*z[14]*z[15];
    z[17]=n<T>(1,2)*z[4];
    z[18]=n<T>(9,2)*z[5];
    z[19]=n<T>(1,6)*z[4] - static_cast<T>(7)- z[18];
    z[19]=z[5]*z[19];
    z[19]=n<T>(161,12) + z[19];
    z[19]=z[19]*z[17];
    z[20]=9*z[7];
    z[21]=static_cast<T>(1)+ z[5];
    z[21]=z[5]*z[21];
    z[21]= - static_cast<T>(3)+ z[21];
    z[21]=z[21]*z[20];
    z[22]=9*z[5];
    z[23]= - static_cast<T>(19)- z[22];
    z[23]=z[5]*z[23];
    z[23]=static_cast<T>(63)+ z[23];
    z[21]=n<T>(1,2)*z[23] + z[21];
    z[23]=3*z[7];
    z[21]=z[21]*z[23];
    z[24]=z[18] + 1;
    z[24]=z[24]*z[5];
    z[21]=z[21] - n<T>(27,2) - z[24];
    z[25]=n<T>(1,2)*z[7];
    z[21]=z[21]*z[25];
    z[26]=n<T>(1,8)*z[3];
    z[27]=z[26] - 1;
    z[28]= - static_cast<T>(1)- n<T>(3,2)*z[9];
    z[28]=z[9]*z[28];
    z[16]=z[21] + z[19] + z[16] + z[28] - z[27];
    z[19]=static_cast<T>(1)+ n<T>(3,2)*z[5];
    z[21]=3*z[5];
    z[28]=z[19]*z[21];
    z[29]=n<T>(1,4)*z[4];
    z[30]= - n<T>(31,2) - z[22];
    z[30]=z[5]*z[30];
    z[30]=n<T>(85,6) + z[30];
    z[30]=z[30]*z[29];
    z[30]=z[30] + n<T>(43,8) + z[28];
    z[30]=z[4]*z[30];
    z[31]=n<T>(1,2)*z[9];
    z[32]=3*z[9];
    z[33]=z[32] + 1;
    z[34]=z[31]*z[33];
    z[35]=z[5] + 3;
    z[36]=z[5]*z[35];
    z[36]= - static_cast<T>(1)+ z[36];
    z[36]=z[36]*z[20];
    z[37]= - static_cast<T>(1)+ z[22];
    z[37]=z[5]*z[37];
    z[37]=static_cast<T>(25)+ z[37];
    z[36]=n<T>(1,2)*z[37] + z[36];
    z[36]=z[36]*z[25];
    z[37]=z[19]*z[5];
    z[36]=z[36] + n<T>(3,4) + z[37];
    z[36]=z[36]*z[23];
    z[30]=z[36] - z[34] + z[30];
    z[36]= - static_cast<T>(3)- n<T>(19,4)*z[5];
    z[36]=z[4]*z[36];
    z[36]=z[36] + n<T>(13,2) + z[22];
    z[36]=z[36]*z[29];
    z[38]=n<T>(9,4)*z[7];
    z[39]=z[38] + n<T>(9,8);
    z[39]=z[5]*z[39];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[7]*z[39];
    z[19]=n<T>(1,2)*z[19] + z[39];
    z[19]=z[19]*z[23];
    z[19]=z[36] + z[19];
    z[36]=static_cast<T>(19)- z[32];
    z[31]=z[36]*z[31];
    z[31]=z[10] + z[31];
    z[36]=n<T>(1,3)*z[4];
    z[39]= - static_cast<T>(29)- n<T>(41,8)*z[4];
    z[39]=z[39]*z[36];
    z[40]=5*z[9];
    z[41]= - z[40] + n<T>(37,6)*z[4];
    z[41]=z[8]*z[41];
    z[31]=z[41] + n<T>(1,2)*z[31] + z[39];
    z[39]=2*z[7];
    z[31]= - z[39] + n<T>(1,2)*z[31];
    z[31]=z[8]*z[31];
    z[33]=z[33]*z[9];
    z[41]=static_cast<T>(9)+ n<T>(59,6)*z[4];
    z[41]=z[4]*z[41];
    z[41]=z[33] + z[41];
    z[42]=n<T>(5,2) - z[23];
    z[42]=z[7]*z[42];
    z[41]=n<T>(1,4)*z[41] + z[42];
    z[31]=n<T>(1,2)*z[41] + z[31];
    z[31]=z[8]*z[31];
    z[19]=n<T>(1,2)*z[19] + z[31];
    z[19]=z[1]*z[19];
    z[31]= - static_cast<T>(13)- n<T>(85,8)*z[4];
    z[31]=z[31]*z[36];
    z[36]=n<T>(3,2)*z[10];
    z[41]= - static_cast<T>(7)+ z[32];
    z[41]=z[9]*z[41];
    z[31]=z[31] - z[36] + z[41];
    z[41]=n<T>(19,2) - z[20];
    z[41]=z[7]*z[41];
    z[31]=n<T>(1,2)*z[31] + z[41];
    z[41]=n<T>(7,4)*z[8];
    z[42]=6*z[7];
    z[43]=z[41] - z[42];
    z[44]=3*z[10];
    z[45]= - n<T>(1,12)*z[4] + n<T>(33,2)*z[9] - n<T>(77,12) + z[44];
    z[45]=n<T>(1,4)*z[45] + z[43];
    z[45]=z[8]*z[45];
    z[31]=n<T>(1,2)*z[31] + z[45];
    z[31]=z[8]*z[31];
    z[19]=z[19] + n<T>(1,4)*z[30] + z[31];
    z[19]=z[1]*z[19];
    z[30]=n<T>(1,4)*z[2];
    z[31]=n<T>(23,2) - z[20];
    z[31]=z[7]*z[31];
    z[31]=z[31] - n<T>(33,8)*z[4] + z[30] - z[32] - n<T>(19,3) - z[36];
    z[32]=z[2] - 1;
    z[41]=z[41]*z[32];
    z[41]=z[41] + z[42];
    z[45]=n<T>(13,3)*z[2];
    z[46]=z[45] + n<T>(109,4) + z[44];
    z[46]=n<T>(1,4)*z[46] - z[41];
    z[46]=z[8]*z[46];
    z[31]=n<T>(1,2)*z[31] + z[46];
    z[31]=z[8]*z[31];
    z[16]=z[19] + n<T>(1,4)*z[16] + z[31];
    z[16]=z[1]*z[16];
    z[19]=z[11] - 1;
    z[31]=z[19]*z[3];
    z[14]=z[2]*z[14];
    z[14]=z[14] - n<T>(1,4)*z[31] + n<T>(3,2) - z[11];
    z[14]=z[3]*z[14];
    z[14]= - n<T>(3,4)*z[11] + z[14];
    z[14]=z[14]*z[30];
    z[46]=n<T>(1,3)*z[12];
    z[47]=n<T>(1,16)*z[11];
    z[48]= - z[46] + z[47];
    z[48]=z[5]*z[48];
    z[49]=n<T>(1,2)*z[5];
    z[50]=z[49] + 1;
    z[51]= - z[11]*z[50];
    z[51]=n<T>(1,2) + z[51];
    z[51]=z[5]*z[51];
    z[19]= - n<T>(1,2)*z[19] + z[51];
    z[19]=z[4]*z[19];
    z[51]=z[5]*z[11];
    z[19]=z[19] - n<T>(1,4)*z[51] + n<T>(13,12) - z[11];
    z[19]=z[5]*z[19];
    z[52]=n<T>(1,3) - 3*z[11];
    z[19]=n<T>(1,4)*z[52] + z[19];
    z[19]=z[19]*z[29];
    z[52]=static_cast<T>(11)- z[11];
    z[52]=z[3]*z[52];
    z[14]=z[19] + z[48] + z[14] + n<T>(1,16)*z[52] - n<T>(1,4)*z[9] + n<T>(29,16) - 
    z[46];
    z[19]=n<T>(1,2) - z[2];
    z[19]=z[2]*z[19];
    z[19]=z[19] - n<T>(139,4) - z[44];
    z[46]=n<T>(3,2) - z[7];
    z[46]=z[46]*z[23];
    z[19]=n<T>(1,4)*z[19] + z[46];
    z[46]=3*z[2];
    z[48]= - n<T>(85,3) - z[46];
    z[48]=z[48]*z[30];
    z[48]=z[48] + n<T>(89,6) + z[10];
    z[52]=z[15] - 1;
    z[52]=z[52]*z[2];
    z[52]=z[52] + n<T>(1,2);
    z[53]=z[52]*z[8];
    z[48]=n<T>(7,6)*z[53] + n<T>(1,4)*z[48] - z[39];
    z[48]=z[8]*z[48];
    z[19]=n<T>(1,2)*z[19] + z[48];
    z[19]=z[8]*z[19];
    z[38]= - z[50]*z[38];
    z[38]=z[38] + static_cast<T>(4)+ n<T>(27,16)*z[5];
    z[38]=z[38]*z[23];
    z[48]= - n<T>(73,2) - z[22];
    z[38]=n<T>(1,8)*z[48] + z[38];
    z[38]=z[7]*z[38];
    z[14]=z[16] + z[19] + n<T>(1,2)*z[14] + z[38];
    z[14]=z[6]*z[14];
    z[16]= - z[5]*z[50];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=z[16]*z[20];
    z[19]= - static_cast<T>(45)- z[24];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[16]=z[16]*z[23];
    z[16]=z[16] - n<T>(1,2) - z[28];
    z[16]=z[7]*z[16];
    z[19]=static_cast<T>(25)+ z[22];
    z[19]=z[5]*z[19];
    z[19]=static_cast<T>(9)+ z[19];
    z[19]=z[19]*z[29];
    z[24]= - n<T>(9,4) - z[37];
    z[19]=3*z[24] + z[19];
    z[19]=z[4]*z[19];
    z[24]=z[26] + 1;
    z[24]=z[24]*z[3];
    z[28]=z[3] + 1;
    z[28]=z[28]*z[3];
    z[37]=z[2]*z[28];
    z[16]=z[16] + z[19] + z[37] + z[34] + z[24];
    z[19]= - static_cast<T>(5)+ z[20];
    z[19]=z[19]*z[25];
    z[34]=n<T>(119,24)*z[4] - n<T>(45,4)*z[9] + static_cast<T>(1)- z[36];
    z[34]=n<T>(1,2)*z[34] - z[43];
    z[34]=z[8]*z[34];
    z[33]=static_cast<T>(1)- z[33];
    z[36]= - static_cast<T>(2)+ n<T>(5,96)*z[4];
    z[36]=z[4]*z[36];
    z[19]=z[34] + z[19] + n<T>(1,4)*z[33] + z[36];
    z[19]=z[8]*z[19];
    z[33]=static_cast<T>(31)+ n<T>(57,2)*z[5];
    z[17]=z[33]*z[17];
    z[33]= - n<T>(23,4) - z[22];
    z[17]=3*z[33] + z[17];
    z[17]=z[4]*z[17];
    z[33]= - static_cast<T>(1)- z[21];
    z[33]=z[33]*z[20];
    z[34]= - static_cast<T>(19)- 27*z[5];
    z[33]=n<T>(1,2)*z[34] + z[33];
    z[33]=z[7]*z[33];
    z[33]=z[33] - n<T>(11,2) - z[22];
    z[33]=z[33]*z[23];
    z[17]=z[33] - z[28] + z[17];
    z[33]=z[7] + n<T>(1,2);
    z[33]=z[33]*z[23];
    z[33]=z[33] + 1;
    z[33]=z[33]*z[20];
    z[34]= - static_cast<T>(9)+ n<T>(19,4)*z[4];
    z[34]=z[4]*z[34];
    z[34]=z[34] - z[33];
    z[36]=n<T>(1,8)*z[1];
    z[34]=z[34]*z[36];
    z[17]=z[34] + n<T>(1,8)*z[17] + z[19];
    z[17]=z[1]*z[17];
    z[19]=n<T>(5,4) + z[44];
    z[19]=n<T>(55,8)*z[4] - z[15] + n<T>(1,2)*z[19] + z[40];
    z[34]=n<T>(7,2)*z[8];
    z[37]=z[32]*z[34];
    z[38]= - n<T>(7,4)*z[2] - n<T>(167,6) - z[44];
    z[37]=z[37] + n<T>(1,2)*z[38] + 12*z[7];
    z[37]=z[8]*z[37];
    z[38]= - static_cast<T>(7)+ z[20];
    z[38]=z[7]*z[38];
    z[19]=z[37] + n<T>(1,2)*z[19] + z[38];
    z[19]=z[8]*z[19];
    z[16]=z[17] + n<T>(1,4)*z[16] + z[19];
    z[16]=z[1]*z[16];
    z[17]=npow(z[2],2);
    z[17]=n<T>(1,2)*z[17] + n<T>(289,8) + z[44];
    z[19]= - static_cast<T>(1)+ z[7];
    z[19]=z[19]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[19]=n<T>(271,6) + z[46];
    z[19]=z[19]*z[15];
    z[19]=z[19] - n<T>(541,12) - z[44];
    z[34]= - z[52]*z[34];
    z[19]=z[34] + n<T>(1,4)*z[19] + z[42];
    z[19]=z[8]*z[19];
    z[17]=n<T>(1,2)*z[17] + z[19];
    z[17]=z[8]*z[17];
    z[19]= - static_cast<T>(3)+ z[11];
    z[19]=5*z[19] + z[31];
    z[19]=z[3]*z[19];
    z[28]= - z[15]*z[28];
    z[24]= - z[24] + z[28];
    z[24]=z[24]*z[30];
    z[28]=z[51] - n<T>(7,6) + z[11];
    z[28]=z[28]*z[49];
    z[28]= - n<T>(1,3) + z[28];
    z[28]=z[4]*z[28];
    z[31]=n<T>(3,4)*z[51] + static_cast<T>(3)+ n<T>(5,4)*z[11];
    z[28]=n<T>(1,2)*z[31] + z[28];
    z[28]=z[28]*z[29];
    z[31]=static_cast<T>(7)+ z[21];
    z[31]=z[31]*z[23];
    z[31]= - n<T>(19,2)*z[35] + z[31];
    z[31]=z[31]*z[23];
    z[21]=z[31] + n<T>(43,2) + z[21];
    z[21]=z[7]*z[21];
    z[14]=z[14] + z[16] + z[17] + n<T>(3,8)*z[21] + z[28] + z[24] + n<T>(1,32)*
    z[19] + n<T>(1,8)*z[9] + z[47] - static_cast<T>(1)+ n<T>(1,6)*z[12];
    z[14]=z[6]*z[14];
    z[16]=z[13] - 1;
    z[16]=z[16]*z[3];
    z[17]=npow(z[3],2);
    z[19]=z[17]*z[2];
    z[21]= - n<T>(3,8)*z[19] + static_cast<T>(3)+ n<T>(1,4)*z[16];
    z[24]=z[3] - n<T>(3,2);
    z[24]=z[24]*z[3];
    z[28]=z[24] - n<T>(3,2)*z[19];
    z[28]=z[2]*z[28];
    z[31]= - static_cast<T>(39)+ z[3];
    z[28]=n<T>(1,2)*z[31] + z[28];
    z[28]=z[28]*z[29];
    z[29]=z[4]*z[19];
    z[29]=3*z[17] + z[29];
    z[29]=z[29]*z[36];
    z[21]=z[29] + 3*z[21] + z[28];
    z[21]=z[4]*z[21];
    z[26]= - z[8]*npow(z[4],2)*z[26];
    z[21]=z[26] + z[33] + z[21];
    z[21]=z[1]*z[21];
    z[26]= - static_cast<T>(3)+ z[5];
    z[26]=z[7]*z[26];
    z[26]=n<T>(3,2)*z[26] + n<T>(3,4)*z[5] + n<T>(9,4) - z[2];
    z[26]=z[26]*z[20];
    z[22]=z[22] + n<T>(47,2) - 9*z[2];
    z[22]=n<T>(1,2)*z[22] + z[26];
    z[22]=z[7]*z[22];
    z[21]=z[22] + z[21];
    z[19]=n<T>(3,4)*z[19];
    z[22]=z[19] - static_cast<T>(3)- z[16];
    z[22]=z[2]*z[22];
    z[19]= - z[24] + z[19];
    z[19]=z[2]*z[19];
    z[26]= - static_cast<T>(3)+ z[13];
    z[26]=z[3]*z[26];
    z[26]=static_cast<T>(3)+ z[26];
    z[19]=n<T>(1,2)*z[26] + z[19];
    z[19]=z[2]*z[19];
    z[18]= - z[18] - n<T>(9,2) + z[19];
    z[18]=z[4]*z[18];
    z[18]=n<T>(1,8)*z[18] + n<T>(9,8)*z[5] + n<T>(3,8)*z[22] - z[27];
    z[18]=z[4]*z[18];
    z[19]=n<T>(5,2) - z[20];
    z[19]=z[19]*z[25];
    z[22]= - n<T>(5,6)*z[2] + n<T>(341,12) + z[44];
    z[22]=n<T>(1,4)*z[22] - z[41];
    z[22]=z[8]*z[22];
    z[19]=z[22] + z[19] - n<T>(11,8)*z[4] + n<T>(107,48) - z[9];
    z[19]=z[8]*z[19];
    z[18]=z[19] + z[18] + n<T>(1,4)*z[21];
    z[18]=z[1]*z[18];
    z[19]=static_cast<T>(5)- z[3];
    z[13]=z[19]*z[13];
    z[15]=z[17]*z[15];
    z[17]=z[24] - z[15];
    z[17]=z[2]*z[17];
    z[13]=z[17] + static_cast<T>(7)+ z[13];
    z[13]=z[2]*z[13];
    z[13]=z[13] - static_cast<T>(15)- z[3];
    z[13]=z[13]*z[30];
    z[13]=n<T>(7,3) + z[13];
    z[17]= - n<T>(1,8)*z[11] + z[32];
    z[17]=z[5]*z[17];
    z[13]=n<T>(1,2)*z[13] + z[17];
    z[13]=z[4]*z[13];
    z[15]=z[16] - z[15];
    z[15]=z[2]*z[15];
    z[15]=n<T>(3,2)*z[15] + n<T>(15,2) + z[3];
    z[15]=z[2]*z[15];
    z[16]= - static_cast<T>(23)- z[11];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[13]=z[13] + n<T>(1,4)*z[15] + z[5];
    z[13]=z[4]*z[13];
    z[15]= - static_cast<T>(7)+ z[46];
    z[15]=z[2]*z[15];
    z[16]=static_cast<T>(5)+ z[46];
    z[16]=z[5]*z[16];
    z[15]=z[16] + static_cast<T>(45)+ z[15];
    z[16]=static_cast<T>(3)- z[2];
    z[16]=z[2]*z[16];
    z[17]= - z[5]*z[32];
    z[16]=z[17] - static_cast<T>(5)+ z[16];
    z[16]=z[16]*z[20];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[7]*z[15];
    z[16]=static_cast<T>(19)- 17*z[2];
    z[15]=n<T>(3,4)*z[15] + n<T>(1,8)*z[16] - z[5];
    z[15]=z[7]*z[15];
    z[13]=z[13] + z[15];
    z[15]=n<T>(101,12)*z[2] - n<T>(185,12) - z[10];
    z[15]=z[15]*z[49];
    z[16]= - static_cast<T>(121)+ n<T>(19,2)*z[2];
    z[16]=z[2]*z[16];
    z[15]=z[15] + n<T>(1,12)*z[16] + n<T>(391,24) + z[10];
    z[16]= - static_cast<T>(2)+ z[5];
    z[16]=z[16]*z[39];
    z[17]=z[49] - 1;
    z[19]= - z[17]*z[53];
    z[15]=n<T>(7,3)*z[19] + n<T>(1,2)*z[15] + z[16];
    z[15]=z[8]*z[15];
    z[16]=z[17]*z[23];
    z[16]=n<T>(1,4) + z[16];
    z[16]=z[7]*z[16];
    z[17]= - z[45] - n<T>(5,3) - n<T>(1,4)*z[10];
    z[15]=z[15] + z[16] + n<T>(1,2)*z[17] + z[5];
    z[15]=z[8]*z[15];

    r += n<T>(1,2)*z[13] + z[14] + z[15] + z[18];
 
    return r;
}

template double qqb_2lha_r1440(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1440(const std::array<dd_real,30>&);
#endif
