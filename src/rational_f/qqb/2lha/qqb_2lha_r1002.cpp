#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1002(const std::array<T,30>& k) {
  T z[76];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[20];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[15];
    z[9]=k[5];
    z[10]=k[9];
    z[11]=k[10];
    z[12]=k[13];
    z[13]=k[24];
    z[14]=z[6] + 1;
    z[15]=z[14]*z[6];
    z[16]=z[15] - 1;
    z[16]=z[16]*z[6];
    z[16]=z[16] - 1;
    z[17]=z[6] + 3;
    z[17]=z[17]*z[6];
    z[17]=z[17] + 3;
    z[17]=z[17]*z[6];
    z[17]=z[17] + 1;
    z[18]=z[17]*z[4];
    z[18]= - z[16] - z[18];
    z[19]=z[4]*z[6];
    z[18]=z[18]*z[19];
    z[20]=n<T>(1,2)*z[6];
    z[21]=z[17]*z[20];
    z[22]=z[7]*npow(z[4],2)*z[21];
    z[18]=z[18] + z[22];
    z[18]=z[7]*z[18];
    z[22]=3*z[6];
    z[23]=z[22] + 1;
    z[24]=z[9]*z[6];
    z[25]= - z[23]*z[24];
    z[26]=npow(z[9],2);
    z[27]=z[26]*z[2];
    z[28]=z[6] - n<T>(1,2);
    z[29]= - z[28]*z[27];
    z[25]=z[25] + z[29];
    z[25]=z[2]*z[25];
    z[29]=npow(z[6],2);
    z[30]=3*z[29];
    z[31]= - z[14]*z[30];
    z[21]=z[4]*z[21];
    z[16]=z[6]*z[16];
    z[16]=z[16] + z[21];
    z[16]=z[4]*z[16];
    z[16]=z[25] + z[18] + z[31] + z[16];
    z[18]=n<T>(1,4)*z[10];
    z[16]=z[16]*z[18];
    z[21]=n<T>(1,4)*z[9];
    z[25]=5*z[6];
    z[31]=z[25] + 3;
    z[32]= - z[6]*z[31];
    z[32]= - n<T>(7,2) + z[32];
    z[32]=z[32]*z[21];
    z[32]=z[32] - n<T>(3,4) + z[15];
    z[32]=z[9]*z[32];
    z[33]=n<T>(3,2)*z[6];
    z[34]=z[33] - 1;
    z[35]=z[34]*z[27];
    z[32]=n<T>(1,2)*z[35] + z[32] + n<T>(1,8)*z[3];
    z[32]=z[2]*z[32];
    z[35]=static_cast<T>(3)- z[29];
    z[35]=z[35]*z[20];
    z[35]=static_cast<T>(1)+ z[35];
    z[36]=n<T>(1,2)*z[4];
    z[35]=z[35]*z[36];
    z[37]=z[6] + n<T>(5,4);
    z[37]=z[37]*z[6];
    z[38]= - n<T>(5,4) - z[37];
    z[38]=z[6]*z[38];
    z[35]=z[35] - static_cast<T>(1)+ z[38];
    z[35]=z[35]*z[36];
    z[38]=z[6] + n<T>(3,2);
    z[39]=z[38]*z[29];
    z[39]= - n<T>(1,2) + z[39];
    z[39]=z[39]*z[36];
    z[37]=n<T>(1,2) + z[37];
    z[37]=z[6]*z[37];
    z[37]=z[39] + n<T>(1,4) + z[37];
    z[39]=z[36]*z[7];
    z[37]=z[37]*z[39];
    z[17]= - z[17]*z[36];
    z[40]=z[20] + 1;
    z[41]=z[40]*z[6];
    z[42]=z[41] + n<T>(1,2);
    z[17]=3*z[42] + z[17];
    z[17]=z[4]*z[17];
    z[17]= - n<T>(3,2)*z[14] + z[17];
    z[43]=n<T>(1,4)*z[3];
    z[17]=z[17]*z[43];
    z[44]=static_cast<T>(3)+ z[20];
    z[44]=z[6]*z[44];
    z[44]=n<T>(1,4) + z[44];
    z[44]=z[6]*z[44];
    z[44]= - static_cast<T>(1)+ z[44];
    z[45]= - n<T>(7,4) - z[6];
    z[45]=z[6]*z[45];
    z[45]= - n<T>(13,8) + z[45];
    z[45]=z[6]*z[45];
    z[45]= - n<T>(3,4) + z[45];
    z[45]=z[9]*z[45];
    z[16]=z[16] + z[32] + z[17] + z[37] + z[35] + n<T>(1,2)*z[44] + z[45];
    z[16]=z[16]*z[18];
    z[17]=13*z[6];
    z[18]= - static_cast<T>(3)+ z[17];
    z[32]=7*z[6];
    z[35]=n<T>(1,2) - z[32];
    z[35]=z[4]*z[35];
    z[18]=n<T>(1,2)*z[18] + z[35];
    z[18]=z[4]*z[18];
    z[35]= - n<T>(163,2) - 35*z[6];
    z[18]=n<T>(1,4)*z[35] + z[18];
    z[19]= - z[33] + z[19];
    z[19]=z[4]*z[19];
    z[19]= - 26*z[6] + z[19];
    z[19]=z[3]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[3]*z[18];
    z[19]=17*z[6];
    z[35]= - n<T>(7,2) + 37*z[6];
    z[35]=z[4]*z[35];
    z[35]=z[35] + static_cast<T>(5)- z[19];
    z[35]=z[4]*z[35];
    z[37]= - n<T>(11,2) - z[22];
    z[35]=11*z[37] + z[35];
    z[18]=n<T>(1,8)*z[35] + z[18];
    z[18]=z[3]*z[18];
    z[35]=z[3]*z[6];
    z[37]=n<T>(313,4) + z[17];
    z[37]=n<T>(1,4)*z[37] + 27*z[35];
    z[37]=z[3]*z[37];
    z[44]=2*z[6];
    z[37]=z[37] - n<T>(11,16) - z[44];
    z[37]=z[3]*z[37];
    z[45]= - n<T>(245,2) - 33*z[6];
    z[46]=9*z[6];
    z[47]= - z[3]*z[46];
    z[45]=n<T>(1,8)*z[45] + z[47];
    z[45]=z[3]*z[45];
    z[45]=n<T>(33,16) + z[45];
    z[45]=z[3]*z[45];
    z[47]=npow(z[3],2);
    z[48]=z[2]*z[47];
    z[45]=z[45] + n<T>(69,16)*z[48];
    z[45]=z[2]*z[45];
    z[37]=z[45] + static_cast<T>(2)+ z[37];
    z[37]=z[2]*z[37];
    z[45]=n<T>(5,2) - 53*z[3];
    z[45]=z[45]*z[43];
    z[45]= - static_cast<T>(1)+ z[45];
    z[45]=z[3]*z[45];
    z[48]= - n<T>(17,16) + 9*z[3];
    z[47]=z[48]*z[47];
    z[48]=z[2]*npow(z[3],3);
    z[47]=z[47] - n<T>(9,4)*z[48];
    z[47]=z[2]*z[47];
    z[45]=z[45] + z[47];
    z[45]=z[2]*z[45];
    z[47]= - n<T>(3,2) + z[4];
    z[47]=z[4]*z[47];
    z[48]=n<T>(5,2) - z[4];
    z[48]=z[4]*z[48];
    z[48]=static_cast<T>(33)+ z[48];
    z[48]=z[3]*z[48];
    z[47]=z[48] + static_cast<T>(19)+ 3*z[47];
    z[47]=z[47]*z[43];
    z[48]=static_cast<T>(1)- z[4];
    z[48]=z[4]*z[48];
    z[47]=z[47] + n<T>(23,8) + z[48];
    z[47]=z[3]*z[47];
    z[48]= - static_cast<T>(1)+ z[36];
    z[48]=z[4]*z[48];
    z[48]=n<T>(1,2) + z[48];
    z[45]=z[45] + n<T>(9,4)*z[48] + z[47];
    z[45]=z[1]*z[45];
    z[47]=n<T>(5,4) - z[46];
    z[47]=z[47]*z[36];
    z[47]=z[47] - n<T>(3,4) + z[25];
    z[47]=z[4]*z[47];
    z[48]=z[6] - 1;
    z[49]=z[4]*z[48];
    z[49]=static_cast<T>(3)+ z[49];
    z[49]=z[49]*z[36];
    z[49]= - static_cast<T>(1)+ z[49];
    z[50]=n<T>(9,4)*z[7];
    z[49]=z[49]*z[50];
    z[18]=z[45] + z[37] + z[18] + z[49] + z[47] - n<T>(45,8) - z[44];
    z[18]=z[1]*z[18];
    z[37]=z[14]*z[20];
    z[41]=n<T>(3,2) - z[41];
    z[41]=z[41]*z[24];
    z[41]=z[37] + z[41];
    z[41]=z[41]*z[50];
    z[45]=n<T>(1,4)*z[6];
    z[47]= - n<T>(11,2) + z[46];
    z[47]=z[6]*z[47];
    z[47]=static_cast<T>(1)+ z[47];
    z[47]=z[47]*z[45];
    z[49]=static_cast<T>(1)- n<T>(9,8)*z[6];
    z[49]=z[6]*z[49];
    z[49]=n<T>(29,8) + z[49];
    z[49]=z[6]*z[49];
    z[49]=static_cast<T>(1)+ z[49];
    z[49]=z[49]*z[24];
    z[41]=z[41] + z[47] + z[49];
    z[41]=z[9]*z[41];
    z[41]= - n<T>(3,4)*z[15] + z[41];
    z[47]=n<T>(1,2)*z[7];
    z[41]=z[41]*z[47];
    z[49]=n<T>(1,2)*z[9];
    z[51]=z[49]*z[29];
    z[52]= - n<T>(23,2) + z[17];
    z[52]=z[52]*z[45];
    z[52]=static_cast<T>(3)+ z[52];
    z[52]=z[52]*z[51];
    z[53]=4*z[6];
    z[54]=n<T>(41,8) - z[53];
    z[54]=z[54]*z[29];
    z[52]=z[54] + z[52];
    z[52]=z[9]*z[52];
    z[41]=z[41] + n<T>(11,2)*z[29] + z[52];
    z[41]=z[7]*z[41];
    z[52]=z[6] - n<T>(5,2);
    z[54]=n<T>(1,2)*z[29];
    z[52]=z[52]*z[54];
    z[55]=z[29]*z[9];
    z[56]=z[45] - 1;
    z[57]= - z[6]*z[56];
    z[57]= - n<T>(3,4) + z[57];
    z[57]=z[57]*z[55];
    z[57]=z[52] + z[57];
    z[57]=z[9]*z[57];
    z[48]=z[48]*z[20];
    z[58]=z[20] - 1;
    z[59]= - z[6]*z[58];
    z[59]= - n<T>(1,2) + z[59];
    z[59]=z[59]*z[24];
    z[59]=z[48] + z[59];
    z[59]=z[7]*z[59]*z[49];
    z[54]=z[59] - z[54] + z[57];
    z[54]=z[7]*z[54];
    z[57]=npow(z[6],3);
    z[59]=z[49]*z[57];
    z[60]=z[6] - n<T>(3,2);
    z[61]=z[60]*z[59];
    z[61]= - z[57] + z[61];
    z[61]=z[9]*z[61];
    z[54]=z[61] + z[54];
    z[54]=z[7]*z[54];
    z[61]=npow(z[6],4);
    z[62]=z[61]*z[26];
    z[54]= - n<T>(1,4)*z[62] + z[54];
    z[54]=z[8]*z[54];
    z[63]=static_cast<T>(3)- n<T>(25,8)*z[6];
    z[63]=z[63]*z[59];
    z[63]=5*z[57] + z[63];
    z[63]=z[9]*z[63];
    z[64]=n<T>(1,2)*z[3];
    z[65]=z[62]*z[64];
    z[41]=n<T>(9,2)*z[54] + z[65] + z[63] + z[41];
    z[41]=z[8]*z[41];
    z[54]=z[22] - n<T>(13,4);
    z[54]=z[54]*z[22];
    z[63]= - n<T>(11,4) + z[54];
    z[63]=z[63]*z[20];
    z[63]=static_cast<T>(1)+ z[63];
    z[15]= - static_cast<T>(1)- z[15];
    z[15]=z[15]*z[24];
    z[65]=z[6] + n<T>(1,2);
    z[66]=z[65]*z[6];
    z[15]=z[66] + z[15];
    z[15]=z[7]*z[15];
    z[67]=static_cast<T>(5)- z[22];
    z[67]=z[6]*z[67];
    z[67]=static_cast<T>(2)+ n<T>(3,8)*z[67];
    z[67]=z[6]*z[67];
    z[67]= - n<T>(13,4) + z[67];
    z[67]=z[6]*z[67];
    z[67]=n<T>(1,2) + z[67];
    z[67]=z[9]*z[67];
    z[15]=n<T>(9,8)*z[15] + n<T>(1,2)*z[63] + z[67];
    z[15]=z[9]*z[15];
    z[60]=z[60]*z[6];
    z[15]= - n<T>(9,8)*z[60] + z[15];
    z[15]=z[7]*z[15];
    z[63]=static_cast<T>(9)+ 23*z[6];
    z[63]=z[63]*z[45];
    z[67]=z[6] + n<T>(1,4);
    z[68]=z[67]*z[22];
    z[68]= - n<T>(25,4) + z[68];
    z[68]=z[6]*z[68];
    z[68]=n<T>(7,4) + z[68];
    z[68]=z[68]*z[24];
    z[69]= - n<T>(1,2) - 57*z[6];
    z[69]=z[6]*z[69];
    z[69]=static_cast<T>(4)+ n<T>(1,8)*z[69];
    z[69]=z[6]*z[69];
    z[68]=z[69] + z[68];
    z[68]=z[9]*z[68];
    z[15]=z[15] + z[63] + z[68];
    z[15]=z[7]*z[15];
    z[63]=29*z[6];
    z[68]=static_cast<T>(5)+ z[63];
    z[68]=z[68]*z[29];
    z[69]= - static_cast<T>(11)- n<T>(19,2)*z[6];
    z[69]=z[6]*z[69];
    z[69]=static_cast<T>(9)+ z[69];
    z[69]=z[69]*z[55];
    z[68]=z[68] + z[69];
    z[68]=z[9]*z[68];
    z[68]= - n<T>(89,2)*z[29] + z[68];
    z[69]=z[6] + 5;
    z[70]=z[57]*z[9];
    z[71]=z[69]*z[70];
    z[71]= - 9*z[57] + z[71];
    z[71]=z[9]*z[71];
    z[72]=z[3]*z[62];
    z[71]=z[71] + z[72];
    z[71]=z[71]*z[43];
    z[50]= - z[1]*z[6]*z[50];
    z[15]=3*z[41] + z[50] + z[71] + n<T>(1,4)*z[68] + z[15];
    z[15]=z[8]*z[15];
    z[41]= - z[56]*z[70];
    z[41]=z[52] + z[41];
    z[41]=z[9]*z[41];
    z[34]= - z[34]*z[51];
    z[50]=z[6] - n<T>(1,4);
    z[52]=z[6]*z[50];
    z[34]=z[52] + z[34];
    z[34]=z[9]*z[34];
    z[52]=z[26]*z[7];
    z[56]= - z[20]*z[52];
    z[68]=z[6] - n<T>(3,2)*z[55];
    z[68]=z[9]*z[68];
    z[56]=z[56] - n<T>(1,2) + z[68];
    z[56]=z[56]*z[47];
    z[34]=z[56] - n<T>(1,4)*z[23] + z[34];
    z[34]=z[7]*z[34];
    z[56]=z[6] - n<T>(3,4);
    z[68]= - z[56]*z[29];
    z[71]=z[28]*z[59];
    z[68]=z[68] + z[71];
    z[68]=z[9]*z[68];
    z[71]=z[61]*z[21];
    z[71]=z[71] - z[57];
    z[71]=z[71]*z[9];
    z[72]=n<T>(3,2)*z[29];
    z[71]=z[71] + z[72];
    z[73]=z[71]*z[3];
    z[74]=n<T>(3,4)*z[6];
    z[68]= - z[73] - z[74] + z[68];
    z[68]=z[3]*z[68];
    z[75]= - static_cast<T>(1)- z[64];
    z[75]=z[3]*z[75];
    z[75]= - n<T>(1,2) + z[75];
    z[75]=z[1]*z[75];
    z[38]=z[75] + z[7] + z[38];
    z[75]=z[35] + z[67];
    z[75]=z[3]*z[75];
    z[38]=z[75] + n<T>(1,2)*z[38];
    z[38]=z[1]*z[38];
    z[34]=z[38] + z[68] + z[34] - z[37] + z[41];
    z[34]=z[5]*z[34];
    z[37]= - static_cast<T>(33)+ 73*z[6];
    z[37]=z[37]*z[45];
    z[38]=25*z[6];
    z[41]=n<T>(33,2) - z[38];
    z[41]=z[41]*z[55];
    z[37]=z[37] + z[41];
    z[37]=z[9]*z[37];
    z[41]=15*z[6];
    z[68]= - n<T>(127,2) - z[41];
    z[68]=z[6]*z[68];
    z[68]= - n<T>(33,2) + z[68];
    z[34]=z[34] + n<T>(1,2)*z[68] + z[37];
    z[37]=19*z[6];
    z[68]= - n<T>(139,2) + z[37];
    z[68]=z[68]*z[29];
    z[19]=static_cast<T>(67)- z[19];
    z[19]=z[19]*z[70];
    z[19]=z[68] + z[19];
    z[19]=z[19]*z[49];
    z[68]= - static_cast<T>(31)- n<T>(33,2)*z[6];
    z[68]=z[6]*z[68];
    z[19]=z[68] + z[19];
    z[68]=n<T>(17,2) - z[46];
    z[68]=z[68]*z[30];
    z[75]=z[28]*z[70];
    z[68]=z[68] + 17*z[75];
    z[68]=z[9]*z[68];
    z[75]= - n<T>(17,2) - z[32];
    z[75]=z[75]*z[22];
    z[68]=z[75] + z[68];
    z[68]=n<T>(1,2)*z[68] - 17*z[73];
    z[68]=z[3]*z[68];
    z[19]=n<T>(1,2)*z[19] + z[68];
    z[19]=z[19]*z[64];
    z[68]=n<T>(5,2)*z[6] - 7*z[55];
    z[68]=z[9]*z[68];
    z[68]=n<T>(7,2)*z[68] - n<T>(31,2) - z[41];
    z[73]= - z[26]*z[44];
    z[73]= - n<T>(15,8) + z[73];
    z[73]=z[7]*z[73];
    z[68]=n<T>(1,4)*z[68] + z[73];
    z[68]=z[7]*z[68];
    z[73]=n<T>(17,2) + 55*z[6];
    z[73]=n<T>(1,2)*z[73] + 17*z[35];
    z[73]=z[3]*z[73];
    z[75]=n<T>(129,2) + 77*z[6];
    z[73]=n<T>(1,4)*z[75] + z[73];
    z[73]=z[3]*z[73];
    z[75]=15*z[7] + n<T>(77,2) + z[41];
    z[73]=n<T>(1,2)*z[75] + z[73];
    z[75]= - static_cast<T>(6)- n<T>(17,8)*z[3];
    z[75]=z[3]*z[75];
    z[75]= - n<T>(23,4) + z[75];
    z[75]=z[3]*z[75];
    z[75]= - n<T>(15,8) + z[75];
    z[75]=z[1]*z[75];
    z[73]=n<T>(1,2)*z[73] + z[75];
    z[73]=z[1]*z[73];
    z[19]=z[73] + z[19] + z[68] + n<T>(1,4)*z[34];
    z[19]=z[5]*z[19];
    z[23]=z[23]*z[20];
    z[23]=z[23] - 1;
    z[34]=n<T>(1,4)*z[55];
    z[23]=z[23]*z[34];
    z[34]=z[66] - n<T>(1,2);
    z[34]=z[34]*z[6];
    z[23]=z[23] - z[34];
    z[23]=z[23]*z[9];
    z[31]=z[31]*z[20];
    z[31]=z[31] - 1;
    z[23]=z[23] + n<T>(1,4)*z[31];
    z[31]=z[30] - z[70];
    z[34]= - z[49]*z[31];
    z[34]=z[34] + z[6];
    z[34]=z[42]*z[34];
    z[68]= - z[42]*z[47];
    z[68]=z[68] + z[34];
    z[68]=z[13]*z[68];
    z[73]=n<T>(1,4)*z[7];
    z[75]=z[14]*z[73];
    z[68]=z[68] + z[75] - z[23];
    z[68]=z[13]*z[68];
    z[75]= - static_cast<T>(1)+ z[32];
    z[75]=z[6]*z[75];
    z[75]=static_cast<T>(1)+ z[75];
    z[75]=z[75]*z[21];
    z[75]= - z[6] + z[75];
    z[75]=z[9]*z[75];
    z[74]=static_cast<T>(1)- z[74];
    z[74]=z[74]*z[27];
    z[74]=z[75] + z[74];
    z[74]=z[2]*z[74];
    z[40]=z[40]*z[55];
    z[75]=static_cast<T>(1)+ z[45];
    z[75]=z[6]*z[75];
    z[40]=z[75] - n<T>(3,4)*z[40];
    z[40]=z[9]*z[40];
    z[27]= - z[26] - 5*z[27];
    z[27]=z[2]*z[27];
    z[75]=z[10]*z[26]*npow(z[2],2);
    z[27]=z[27] + z[75];
    z[27]=z[10]*z[27];
    z[27]=z[68] + n<T>(1,8)*z[27] + n<T>(1,2)*z[74] + n<T>(1,4)*z[58] + z[40];
    z[27]=z[11]*z[27];
    z[34]=z[34]*z[4];
    z[23]=z[34] - z[23];
    z[23]=z[4]*z[23];
    z[40]= - z[42]*z[39];
    z[34]=z[34] + z[40];
    z[34]=z[13]*z[34];
    z[40]= - z[4]*z[42];
    z[40]=n<T>(1,2)*z[14] + z[40];
    z[39]=z[40]*z[39];
    z[23]=z[34] + z[23] + z[39];
    z[23]=z[13]*z[23];
    z[34]=static_cast<T>(39)- n<T>(179,2)*z[6];
    z[34]=z[34]*z[29];
    z[39]= - static_cast<T>(33)+ 53*z[6];
    z[39]=z[39]*z[59];
    z[34]=z[34] + z[39];
    z[34]=z[34]*z[21];
    z[39]= - n<T>(35,4) + z[63];
    z[39]=z[6]*z[39];
    z[34]=z[34] - n<T>(3,8) + z[39];
    z[34]=z[4]*z[34];
    z[39]= - n<T>(73,2) - 41*z[6];
    z[39]=z[6]*z[39];
    z[39]=n<T>(51,2) + z[39];
    z[39]=z[39]*z[51];
    z[40]=static_cast<T>(45)+ 61*z[6];
    z[40]=z[6]*z[40];
    z[40]= - n<T>(21,2) + z[40];
    z[40]=z[6]*z[40];
    z[39]=z[40] + z[39];
    z[39]=z[39]*z[49];
    z[40]= - n<T>(39,4) - 31*z[6];
    z[40]=z[6]*z[40];
    z[34]=z[34] + z[39] + n<T>(1,4) + z[40];
    z[34]=z[4]*z[34];
    z[17]=n<T>(147,2) + z[17];
    z[17]=z[17]*z[45];
    z[17]= - static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[24];
    z[39]= - static_cast<T>(13)+ z[20];
    z[39]=z[6]*z[39];
    z[39]= - n<T>(153,4) + z[39];
    z[39]=z[6]*z[39];
    z[39]=n<T>(3,2) + z[39];
    z[17]=n<T>(1,2)*z[39] + z[17];
    z[17]=z[9]*z[17];
    z[39]=n<T>(17,4) - z[22];
    z[39]=z[6]*z[39];
    z[17]=z[27] + z[23] + z[34] + z[17] + n<T>(53,4) + z[39];
    z[14]= - z[14]*z[55];
    z[14]=z[66] + z[14];
    z[14]=z[9]*z[14];
    z[23]= - z[29] + z[70];
    z[23]=z[4]*z[23]*z[49];
    z[14]=z[14] + z[23];
    z[14]=z[7]*z[14];
    z[14]=z[14] - static_cast<T>(1)- z[66];
    z[23]= - n<T>(19,4) + z[54];
    z[23]=z[6]*z[23];
    z[27]=static_cast<T>(7)- n<T>(9,2)*z[6];
    z[27]=z[6]*z[27];
    z[27]=n<T>(57,8) + z[27];
    z[27]=z[27]*z[55];
    z[23]=z[23] + z[27];
    z[23]=z[9]*z[23];
    z[27]=static_cast<T>(19)- z[46];
    z[27]=z[27]*z[29];
    z[34]= - static_cast<T>(39)+ z[37];
    z[34]=z[34]*z[70];
    z[27]=z[27] + n<T>(1,4)*z[34];
    z[27]=z[9]*z[27];
    z[34]=z[28]*z[46];
    z[27]=z[34] + z[27];
    z[27]=z[27]*z[36];
    z[14]=z[27] + z[23] + n<T>(9,2)*z[14];
    z[14]=z[4]*z[14];
    z[23]=n<T>(31,2) + z[22];
    z[23]=z[23]*z[20];
    z[23]= - static_cast<T>(5)+ z[23];
    z[23]=z[23]*z[26];
    z[23]=static_cast<T>(9)+ z[23];
    z[14]=n<T>(1,2)*z[23] + z[14];
    z[14]=z[14]*z[73];
    z[23]= - z[69]*z[22];
    z[23]= - n<T>(7,4) + z[23];
    z[27]=n<T>(33,4) + z[6];
    z[27]=z[6]*z[27];
    z[27]=n<T>(15,8) + z[27];
    z[27]=z[27]*z[24];
    z[23]=n<T>(1,2)*z[23] + z[27];
    z[21]=z[23]*z[21];
    z[23]=59*z[6];
    z[27]= - n<T>(95,2) + z[23];
    z[27]=z[27]*z[29];
    z[34]=n<T>(5,2) - z[22];
    z[34]=z[34]*z[70];
    z[27]=z[27] + 7*z[34];
    z[27]=z[9]*z[27];
    z[34]=static_cast<T>(3)- n<T>(7,2)*z[6];
    z[34]=z[6]*z[34];
    z[27]=z[34] + n<T>(1,16)*z[27];
    z[27]=z[4]*z[27];
    z[34]= - static_cast<T>(11)+ 47*z[6];
    z[34]=z[6]*z[34];
    z[34]= - static_cast<T>(27)+ z[34];
    z[34]=z[34]*z[55];
    z[36]=n<T>(67,32) - z[32];
    z[36]=z[6]*z[36];
    z[36]=n<T>(9,4) + z[36];
    z[36]=z[6]*z[36];
    z[34]=z[36] + n<T>(1,16)*z[34];
    z[34]=z[9]*z[34];
    z[36]= - static_cast<T>(3)+ n<T>(19,4)*z[6];
    z[36]=z[6]*z[36];
    z[27]=z[27] + z[34] + n<T>(1,8) + z[36];
    z[27]=z[4]*z[27];
    z[14]=z[14] + z[27] + z[21] + n<T>(11,4) + z[44];
    z[14]=z[7]*z[14];
    z[21]= - n<T>(167,4) - z[46];
    z[21]=z[21]*z[20];
    z[21]=static_cast<T>(1)+ z[21];
    z[21]=z[21]*z[20];
    z[27]=n<T>(13,2) + z[6];
    z[27]=z[6]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[27]*z[55];
    z[21]=z[21] + z[27];
    z[21]=z[9]*z[21];
    z[27]=n<T>(1,4)*z[29];
    z[34]= - static_cast<T>(71)+ 205*z[6];
    z[34]=z[34]*z[27];
    z[36]=n<T>(29,4) - z[41];
    z[36]=z[36]*z[70];
    z[34]=z[34] + z[36];
    z[34]=z[9]*z[34];
    z[36]=n<T>(29,2) - 65*z[6];
    z[36]=z[6]*z[36];
    z[34]=z[34] + n<T>(1,4) + z[36];
    z[34]=z[4]*z[34];
    z[36]=static_cast<T>(5)+ z[32];
    z[36]=z[6]*z[36];
    z[36]=n<T>(5,8) + z[36];
    z[21]=n<T>(1,4)*z[34] + n<T>(1,2)*z[36] + z[21];
    z[21]=z[4]*z[21];
    z[34]= - static_cast<T>(79)+ 45*z[6];
    z[34]=z[34]*z[55];
    z[36]= - n<T>(31,8) - z[6];
    z[36]=z[6]*z[36];
    z[36]=n<T>(11,2) + z[36];
    z[36]=z[6]*z[36];
    z[34]=z[36] + n<T>(1,8)*z[34];
    z[34]=z[9]*z[34];
    z[36]=n<T>(53,4) + z[32];
    z[36]=z[6]*z[36];
    z[36]=n<T>(57,8) + z[36];
    z[21]=z[21] + n<T>(1,2)*z[36] + z[34];
    z[34]=static_cast<T>(5)- z[6];
    z[34]=z[34]*z[27];
    z[36]= - static_cast<T>(1)+ n<T>(5,8)*z[6];
    z[36]=z[36]*z[70];
    z[34]=z[34] + z[36];
    z[34]=z[9]*z[34];
    z[36]=n<T>(1,2) - z[22];
    z[30]=z[36]*z[30];
    z[36]=z[25] - 1;
    z[37]=z[36]*z[59];
    z[30]=z[30] + z[37];
    z[30]=z[30]*z[49];
    z[37]= - n<T>(1,4) + z[44];
    z[37]=z[37]*z[22];
    z[30]=z[37] + z[30];
    z[30]=z[4]*z[30];
    z[36]= - z[36]*z[20];
    z[30]=z[30] + z[36] + z[34];
    z[30]=z[4]*z[30];
    z[34]=z[9]*z[61];
    z[34]=z[57] - n<T>(3,4)*z[34];
    z[34]=z[9]*z[34];
    z[34]=z[72] + z[34];
    z[36]= - z[4]*z[71];
    z[34]=n<T>(1,2)*z[34] + z[36];
    z[34]=z[4]*z[34];
    z[31]=9*z[31] + z[34];
    z[31]=z[3]*z[31];
    z[34]= - n<T>(41,2) + z[38];
    z[34]=z[34]*z[29];
    z[34]=z[34] - n<T>(71,2)*z[70];
    z[36]=n<T>(1,8)*z[9];
    z[34]=z[34]*z[36];
    z[37]=n<T>(287,16) - z[53];
    z[37]=z[6]*z[37];
    z[30]=z[31] + z[30] + z[37] + z[34];
    z[30]=z[3]*z[30];
    z[21]=n<T>(1,2)*z[21] + z[30];
    z[21]=z[3]*z[21];
    z[20]=z[20]*z[9];
    z[30]= - z[20] + static_cast<T>(1)+ z[33];
    z[30]=z[9]*z[30];
    z[30]= - n<T>(29,4) + z[30];
    z[31]=n<T>(1,8)*z[52];
    z[34]=z[9]*z[65];
    z[34]= - n<T>(1,2) + z[34];
    z[34]=z[9]*z[34];
    z[34]=z[34] + z[31];
    z[34]=z[7]*z[34];
    z[23]=67*z[35] + static_cast<T>(97)+ z[23];
    z[23]=z[23]*z[43];
    z[35]=n<T>(1,4)*z[2];
    z[37]= - 31*z[3] - z[9] - z[52];
    z[37]=z[37]*z[35];
    z[23]=z[37] + z[23] + n<T>(1,2)*z[30] + z[34];
    z[23]=z[23]*z[35];
    z[30]= - n<T>(13,2) - z[22];
    z[30]=z[30]*z[24];
    z[30]=z[30] + n<T>(13,4) + z[22];
    z[30]=z[30]*z[36];
    z[31]= - z[67]*z[31];
    z[30]=z[31] - static_cast<T>(1)+ z[30];
    z[30]=z[7]*z[30];
    z[31]= - static_cast<T>(31)- n<T>(37,4)*z[6];
    z[31]=z[6]*z[31];
    z[34]=z[3]*z[29];
    z[31]= - 27*z[34] + z[31] - n<T>(73,8)*z[55];
    z[31]=z[31]*z[64];
    z[34]= - n<T>(11,2) + z[6];
    z[34]=z[6]*z[34];
    z[34]=z[34] - n<T>(33,8)*z[55];
    z[34]=z[34]*z[49];
    z[35]= - static_cast<T>(8)- n<T>(13,8)*z[6];
    z[35]=z[6]*z[35];
    z[31]=z[31] + z[34] - n<T>(195,32) + z[35];
    z[31]=z[3]*z[31];
    z[25]= - n<T>(67,2) - z[25];
    z[25]=z[6]*z[25];
    z[25]=static_cast<T>(3)+ z[25];
    z[25]=z[25]*z[49];
    z[33]=static_cast<T>(5)- z[33];
    z[33]=z[6]*z[33];
    z[25]=z[25] - n<T>(1,2) + z[33];
    z[25]=z[25]*z[49];
    z[25]=z[25] - n<T>(5,4) + z[32];
    z[23]=z[23] + z[31] + n<T>(1,4)*z[25] + z[30];
    z[23]=z[2]*z[23];
    z[25]= - static_cast<T>(1)+ n<T>(5,4)*z[6];
    z[25]=z[25]*z[29];
    z[29]=n<T>(25,8) - z[6];
    z[29]=z[6]*z[29];
    z[29]= - n<T>(3,4) + z[29];
    z[29]=z[29]*z[55];
    z[25]=z[25] + z[29];
    z[25]=z[9]*z[25];
    z[29]=z[56]*z[70];
    z[29]= - n<T>(1,2)*z[57] + z[29];
    z[29]=z[9]*z[29];
    z[30]= - z[43]*z[62];
    z[29]=z[29] + z[30];
    z[29]=z[3]*z[29];
    z[25]=z[29] - z[27] + z[25];
    z[25]=z[3]*z[25];
    z[27]= - n<T>(25,2) + z[6];
    z[27]=z[6]*z[27];
    z[27]=n<T>(27,2) + z[27];
    z[27]=z[6]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[20]=z[27]*z[20];
    z[27]=n<T>(21,4) - z[6];
    z[27]=z[6]*z[27];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[6]*z[27];
    z[20]=z[27] + z[20];
    z[20]=z[9]*z[20];
    z[20]=z[48] + z[20];
    z[26]=z[28]*z[26]*z[47];
    z[27]= - n<T>(11,2) + z[22];
    z[27]=z[6]*z[27];
    z[27]=n<T>(1,2) + z[27];
    z[27]=z[27]*z[49];
    z[27]=z[27] - z[50];
    z[27]=z[9]*z[27];
    z[26]=z[27] + z[26];
    z[26]=z[26]*z[47];
    z[27]= - n<T>(9,2) + z[6];
    z[22]=z[27]*z[22];
    z[22]=n<T>(11,2) + z[22];
    z[22]=z[22]*z[24];
    z[22]= - z[60] + n<T>(1,4)*z[22];
    z[22]=z[9]*z[22];
    z[22]=z[26] + z[45] + z[22];
    z[22]=z[7]*z[22];
    z[20]=z[25] + n<T>(1,2)*z[20] + z[22];
    z[20]=z[12]*z[20];

    r += z[14] + z[15] + z[16] + n<T>(1,4)*z[17] + z[18] + z[19] + z[20] + 
      z[21] + z[23];
 
    return r;
}

template double qqb_2lha_r1002(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1002(const std::array<dd_real,30>&);
#endif
