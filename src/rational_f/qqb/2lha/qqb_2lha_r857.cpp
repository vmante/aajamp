#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r857(const std::array<T,30>& k) {
  T z[26];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[13];
    z[6]=k[5];
    z[7]=k[4];
    z[8]=k[9];
    z[9]=k[24];
    z[10]=k[3];
    z[11]=k[27];
    z[12]=z[10] - 1;
    z[13]=z[10]*z[12];
    z[13]= - static_cast<T>(5)+ z[13];
    z[13]=z[5]*z[13];
    z[13]=z[10] + z[13] - z[8];
    z[14]=n<T>(1,2)*z[2];
    z[15]=3*z[11];
    z[16]= - static_cast<T>(1)+ z[2];
    z[16]=z[3]*z[2]*z[16];
    z[13]=z[16] - z[14] + z[15] + n<T>(1,2)*z[13];
    z[16]=n<T>(5,2)*z[5];
    z[17]= - z[14] + z[11] + z[9];
    z[18]=n<T>(1,2) - z[2];
    z[18]=z[3]*z[18];
    z[17]=z[18] + 3*z[17] - z[16];
    z[17]=z[3]*z[17];
    z[18]=n<T>(3,2)*z[9];
    z[19]= - static_cast<T>(1)- z[18];
    z[19]=z[8]*z[19];
    z[20]= - static_cast<T>(1)+ 3*z[10];
    z[20]=z[5]*z[20];
    z[17]=z[17] + n<T>(1,4)*z[20] + n<T>(3,4) + z[19];
    z[19]=z[7]*z[5];
    z[20]=n<T>(1,2)*z[4] - z[8];
    z[20]=z[20]*z[19];
    z[21]=3*z[9];
    z[22]=z[21] + 1;
    z[23]= - z[8]*z[22];
    z[24]=static_cast<T>(3)+ z[4];
    z[24]=n<T>(1,2)*z[24] - z[8];
    z[24]=z[5]*z[24];
    z[20]=z[20] + z[23] + z[24];
    z[18]=static_cast<T>(1)- z[18];
    z[23]= - n<T>(5,8)*z[5] + n<T>(15,8)*z[4] - static_cast<T>(1)+ n<T>(3,2)*z[11];
    z[23]=z[3]*z[23];
    z[18]=n<T>(1,2)*z[18] + z[23];
    z[18]=z[3]*z[18];
    z[18]=z[18] + n<T>(1,4)*z[20];
    z[18]=z[7]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[7]*z[17];
    z[16]=z[16] - z[15];
    z[18]= - static_cast<T>(1)- z[16];
    z[18]=z[3]*z[18];
    z[20]=z[21] + 5;
    z[23]=n<T>(15,2)*z[5];
    z[18]=z[18] + z[23] - z[20];
    z[18]=z[3]*z[18];
    z[24]=npow(z[3],2);
    z[25]= - z[4] + z[5];
    z[25]=z[7]*z[25]*z[24];
    z[20]= - z[8]*z[20];
    z[18]=n<T>(5,2)*z[25] + z[20] + z[18];
    z[18]=z[7]*z[18];
    z[20]= - z[11] + z[9];
    z[25]= - z[2] - n<T>(11,2) - z[21];
    z[25]=z[8]*z[25];
    z[18]=z[18] + z[23] + 3*z[20] + z[25];
    z[20]= - z[14] - n<T>(1,2) - z[16];
    z[20]=z[3]*z[20];
    z[18]=z[20] + n<T>(1,2)*z[18];
    z[18]=z[7]*z[18];
    z[12]=z[12]*z[16];
    z[14]= - z[8]*z[14];
    z[12]=z[14] - n<T>(1,2) + z[12];
    z[12]=n<T>(1,2)*z[12] + z[18];
    z[12]=z[6]*z[12];
    z[12]=n<T>(1,2)*z[12] + n<T>(1,4)*z[13] + z[17];
    z[12]=z[6]*z[12];
    z[13]=5*z[4] - 3;
    z[13]=z[1]*z[13];
    z[13]= - static_cast<T>(1)+ z[13];
    z[14]= - z[2] + n<T>(3,2) - z[1];
    z[14]=z[2]*z[14];
    z[13]=n<T>(1,2)*z[13] + z[14];
    z[13]=z[3]*z[13];
    z[14]=n<T>(1,2) + z[9];
    z[14]=z[2]*z[14];
    z[13]=z[13] + 3*z[14] + n<T>(1,2)*z[1] - z[22];
    z[13]=z[3]*z[13];
    z[14]= - z[19] - static_cast<T>(1)- z[5];
    z[16]=z[8]*z[4];
    z[14]=z[16]*z[14];
    z[15]=z[24]*z[15];
    z[14]=z[15] + n<T>(1,2)*z[14];
    z[14]=z[7]*z[14];
    z[15]=static_cast<T>(1)- 3*z[4];
    z[15]=n<T>(5,2)*z[15] + z[2];
    z[15]=z[3]*z[15];
    z[15]=z[15] - n<T>(5,2) + z[21];
    z[15]=z[3]*z[15];
    z[16]= - z[16] - 3*z[5];
    z[14]=z[14] + n<T>(1,2)*z[16] + z[15];
    z[14]=z[7]*z[14];
    z[15]=static_cast<T>(1)+ z[1];
    z[15]=n<T>(1,2)*z[15] - z[10];
    z[15]=z[5]*z[15];
    z[13]=z[14] + z[13] - static_cast<T>(1)+ z[15];
    z[12]=n<T>(1,4)*z[13] + z[12];

    r += n<T>(1,2)*z[12];
 
    return r;
}

template double qqb_2lha_r857(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r857(const std::array<dd_real,30>&);
#endif
