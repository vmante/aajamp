#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r36(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[11];
    z[6]=static_cast<T>(2)- z[4];
    z[7]=npow(z[4],2);
    z[8]=npow(z[5],2);
    z[6]=z[7]*z[8]*z[6];
    z[9]=z[8]*z[4];
    z[10]=static_cast<T>(1)+ z[5];
    z[10]=2*z[10] - z[9];
    z[10]=z[4]*z[10];
    z[11]= - z[2] + z[1] + 1;
    z[12]= - z[4] + 2*z[11];
    z[12]=z[2]*z[12];
    z[10]=z[10] + z[12];
    z[10]=z[2]*z[10];
    z[6]=z[6] + z[10];
    z[7]=z[5]*z[7];
    z[10]= - z[4] + z[11];
    z[10]=z[2]*z[10];
    z[10]=z[4] + n<T>(1,3)*z[10];
    z[10]=z[2]*z[10];
    z[7]=z[7] + z[10];
    z[7]=z[2]*z[7];
    z[10]=z[8]*npow(z[4],3);
    z[7]=n<T>(1,3)*z[10] + z[7];
    z[7]=z[3]*z[7];
    z[6]=n<T>(1,3)*z[6] + z[7];
    z[6]=z[3]*z[6];
    z[7]= - 2*z[4] + 1;
    z[7]=z[8]*z[7];
    z[7]=n<T>(1,2) + z[7];
    z[10]=n<T>(1,3)*z[4];
    z[7]=z[7]*z[10];
    z[8]= - n<T>(1,2) - z[8];
    z[8]=z[8]*z[10];
    z[10]=n<T>(1,2)*z[11];
    z[8]=z[8] + z[10];
    z[8]=z[2]*z[8];
    z[6]=z[6] + z[7] + z[8];
    z[6]=z[3]*z[6];
    z[7]= - z[9] + z[10];
    z[6]=n<T>(1,3)*z[7] + z[6];

    r += z[6]*z[3];
 
    return r;
}

template double qqb_2lha_r36(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r36(const std::array<dd_real,30>&);
#endif
