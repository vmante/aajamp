#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r748(const std::array<T,30>& k) {
  T z[55];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[20];
    z[5]=k[8];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[6];
    z[9]=k[10];
    z[10]=k[9];
    z[11]=k[5];
    z[12]=k[11];
    z[13]=k[21];
    z[14]=n<T>(1,2)*z[2];
    z[15]=11*z[2];
    z[16]= - n<T>(103,4) + z[15];
    z[16]=z[16]*z[14];
    z[17]=n<T>(1,4)*z[6];
    z[18]=13*z[2];
    z[19]=3*z[6] + n<T>(29,2) - z[18];
    z[19]=z[19]*z[17];
    z[16]=z[19] + static_cast<T>(7)+ z[16];
    z[16]=z[6]*z[16];
    z[19]=3*z[2];
    z[20]=n<T>(23,2) - z[19];
    z[20]=z[2]*z[20];
    z[20]= - n<T>(51,4) + z[20];
    z[20]=z[2]*z[20];
    z[20]=n<T>(9,2) + z[20];
    z[16]=n<T>(3,2)*z[20] + z[16];
    z[16]=z[6]*z[16];
    z[20]=5*z[5];
    z[21]=z[20] + 3*z[9];
    z[22]= - z[21]*z[14];
    z[22]=z[22] + static_cast<T>(7)+ n<T>(25,2)*z[5];
    z[22]=z[2]*z[22];
    z[23]=25*z[5];
    z[22]=z[22] - static_cast<T>(43)- z[23];
    z[24]=n<T>(1,4)*z[2];
    z[22]=z[22]*z[24];
    z[22]=z[22] + static_cast<T>(18)+ n<T>(25,4)*z[5];
    z[22]=z[2]*z[22];
    z[23]= - static_cast<T>(101)- z[23];
    z[22]=n<T>(1,8)*z[23] + z[22];
    z[22]=z[2]*z[22];
    z[23]=static_cast<T>(13)+ n<T>(5,2)*z[5];
    z[16]=z[16] + n<T>(1,4)*z[23] + z[22];
    z[16]=z[10]*z[16];
    z[22]=n<T>(3,8)*z[2];
    z[21]=z[21]*z[22];
    z[23]=n<T>(15,8)*z[5];
    z[25]= - static_cast<T>(1)- z[23];
    z[21]=5*z[25] + z[21];
    z[21]=z[2]*z[21];
    z[25]=static_cast<T>(11)+ n<T>(15,2)*z[5];
    z[21]=n<T>(5,2)*z[25] + z[21];
    z[21]=z[2]*z[21];
    z[25]= - static_cast<T>(3)- n<T>(5,4)*z[5];
    z[21]=15*z[25] + z[21];
    z[21]=z[2]*z[21];
    z[25]=static_cast<T>(5)+ n<T>(3,2)*z[5];
    z[21]=n<T>(25,4)*z[25] + z[21];
    z[21]=z[2]*z[21];
    z[15]= - n<T>(19,2) + z[15];
    z[15]=z[15]*z[17];
    z[26]=static_cast<T>(2)- z[2];
    z[26]=z[2]*z[26];
    z[26]= - static_cast<T>(1)+ z[26];
    z[15]=9*z[26] + z[15];
    z[15]=z[6]*z[15];
    z[26]= - n<T>(47,2) + 7*z[2];
    z[26]=z[2]*z[26];
    z[26]=n<T>(99,4) + z[26];
    z[26]=z[2]*z[26];
    z[26]= - n<T>(17,2) + z[26];
    z[15]=n<T>(3,2)*z[26] + z[15];
    z[15]=z[6]*z[15];
    z[15]=z[16] + z[15] + z[21] - static_cast<T>(8)- z[23];
    z[15]=z[10]*z[15];
    z[16]=n<T>(1,2)*z[1];
    z[21]=z[16] - 1;
    z[23]=3*z[1];
    z[26]=z[21]*z[23];
    z[27]=z[21]*z[5];
    z[28]=n<T>(1,2) - z[9];
    z[28]=3*z[28] + 5*z[27];
    z[28]=z[28]*z[14];
    z[29]=n<T>(25,2)*z[27];
    z[28]=z[28] - z[29] + n<T>(13,2) + z[26];
    z[28]=z[28]*z[14];
    z[30]=z[1] - 2;
    z[31]=z[23]*z[30];
    z[28]=z[28] + z[29] - n<T>(67,4) - z[31];
    z[28]=z[2]*z[28];
    z[32]=z[21]*z[1];
    z[33]=static_cast<T>(3)+ z[32];
    z[28]=z[28] + 9*z[33] - z[29];
    z[28]=z[2]*z[28];
    z[28]=z[28] + n<T>(25,4)*z[27] - n<T>(149,8) - z[31];
    z[28]=z[2]*z[28];
    z[29]=z[9]*npow(z[2],2);
    z[29]=n<T>(1,8)*z[29] + z[30];
    z[29]=z[2]*z[29];
    z[31]=3*z[30];
    z[29]= - z[31] + z[29];
    z[29]=z[2]*z[29];
    z[29]=z[31] + z[29];
    z[29]=z[2]*z[29];
    z[29]=z[29] - z[30];
    z[17]=z[17]*z[11];
    z[33]= - n<T>(5,2) + z[2];
    z[33]=z[33]*z[17];
    z[34]=z[14] - 1;
    z[35]=z[2]*z[34];
    z[35]=n<T>(1,2) + z[35];
    z[33]=9*z[35] + z[33];
    z[33]=z[6]*z[33];
    z[29]=3*z[29] + z[33];
    z[29]=z[6]*z[29];
    z[26]= - n<T>(5,2)*z[27] + n<T>(19,2) + z[26];
    z[15]=z[15] + z[29] + n<T>(1,2)*z[26] + z[28];
    z[15]=z[3]*z[15];
    z[26]=n<T>(1,4)*z[5];
    z[27]=23*z[1];
    z[28]=static_cast<T>(41)- z[27];
    z[28]=z[28]*z[26];
    z[29]=7*z[1];
    z[33]=n<T>(55,2) - 13*z[9];
    z[33]=n<T>(1,2)*z[33] - z[29];
    z[35]=n<T>(5,8)*z[2];
    z[36]= - z[35] - static_cast<T>(1)+ n<T>(23,16)*z[1];
    z[36]=z[5]*z[36];
    z[33]=n<T>(1,4)*z[33] + z[36];
    z[33]=z[2]*z[33];
    z[36]=static_cast<T>(2)+ z[1];
    z[36]=z[1]*z[36];
    z[28]=z[33] + z[28] + static_cast<T>(3)+ z[36];
    z[28]=z[2]*z[28];
    z[33]=n<T>(1,2)*z[5];
    z[36]= - static_cast<T>(37)+ n<T>(69,4)*z[1];
    z[36]=z[36]*z[33];
    z[37]=4*z[1];
    z[38]=n<T>(13,2) - z[37];
    z[38]=z[1]*z[38];
    z[28]=z[28] + z[36] - n<T>(261,8) + z[38];
    z[28]=z[2]*z[28];
    z[36]=z[23] - 7;
    z[38]=2*z[1];
    z[39]=z[36]*z[38];
    z[27]=n<T>(107,2) - z[27];
    z[27]=z[27]*z[26];
    z[27]=z[28] + z[27] + n<T>(91,2) + z[39];
    z[27]=z[2]*z[27];
    z[28]=n<T>(1,2)*z[11];
    z[39]=static_cast<T>(11)- z[28];
    z[40]=n<T>(15,2) + z[11];
    z[40]=z[2]*z[40];
    z[39]=3*z[39] + z[40];
    z[39]=z[39]*z[24];
    z[40]=static_cast<T>(3)- z[2];
    z[17]=z[40]*z[17];
    z[40]=6*z[4];
    z[41]=5*z[11];
    z[42]= - static_cast<T>(129)- z[41];
    z[17]=z[17] + z[39] + n<T>(1,8)*z[42] + z[40];
    z[17]=z[6]*z[17];
    z[39]=4*z[4];
    z[42]= - z[30]*z[39];
    z[43]=n<T>(17,8)*z[1];
    z[44]=z[2]*z[9];
    z[44]=n<T>(19,16)*z[44] - static_cast<T>(4)+ n<T>(13,8)*z[1];
    z[44]=z[2]*z[44];
    z[44]=z[44] - static_cast<T>(8)+ z[43];
    z[44]=z[2]*z[44];
    z[45]=static_cast<T>(12)- n<T>(35,8)*z[1];
    z[44]=3*z[45] + z[44];
    z[44]=z[2]*z[44];
    z[17]=z[17] + z[44] + z[42] - static_cast<T>(32)+ n<T>(107,8)*z[1];
    z[17]=z[6]*z[17];
    z[42]=n<T>(1,8)*z[2];
    z[44]=static_cast<T>(313)+ z[2];
    z[44]=z[2]*z[44];
    z[44]= - static_cast<T>(763)+ z[44];
    z[44]=z[44]*z[42];
    z[45]=5*z[2];
    z[46]= - n<T>(237,2) + z[45];
    z[46]=z[46]*z[24];
    z[47]= - n<T>(7,8)*z[2] + n<T>(59,8) - z[39];
    z[47]=z[6]*z[47];
    z[46]=z[47] + z[46] + n<T>(329,8) - 12*z[4];
    z[46]=z[6]*z[46];
    z[47]=n<T>(357,16) - z[39];
    z[44]=z[46] + 3*z[47] + z[44];
    z[44]=z[6]*z[44];
    z[18]=static_cast<T>(79)- z[18];
    z[18]=z[18]*z[22];
    z[46]= - n<T>(11,8) + z[4];
    z[46]=z[6]*z[46];
    z[46]=z[46] + n<T>(35,8)*z[2] - n<T>(49,4) + z[39];
    z[46]=z[6]*z[46];
    z[18]=z[46] + z[18] - n<T>(499,16) + z[40];
    z[18]=z[6]*z[18];
    z[40]= - static_cast<T>(115)+ n<T>(17,2)*z[2];
    z[40]=z[2]*z[40];
    z[40]=n<T>(455,2) + z[40];
    z[40]=z[40]*z[24];
    z[18]=z[18] + z[40] - n<T>(541,16) + z[39];
    z[18]=z[6]*z[18];
    z[40]=43*z[5];
    z[46]=n<T>(7,4)*z[9];
    z[47]= - n<T>(43,4)*z[5] - static_cast<T>(1)- z[46];
    z[47]=z[2]*z[47];
    z[47]=z[47] + n<T>(101,2) + z[40];
    z[47]=z[47]*z[24];
    z[47]=z[47] - static_cast<T>(40)- n<T>(129,8)*z[5];
    z[47]=z[2]*z[47];
    z[48]=n<T>(339,2) + z[40];
    z[47]=n<T>(1,4)*z[48] + z[47];
    z[47]=z[2]*z[47];
    z[40]= - static_cast<T>(259)- z[40];
    z[18]=z[18] + z[47] + n<T>(1,16)*z[40] + z[4];
    z[18]=z[10]*z[18];
    z[40]=109*z[5];
    z[47]= - n<T>(173,2) - z[40];
    z[48]=n<T>(109,8)*z[5] - static_cast<T>(1)+ n<T>(33,8)*z[9];
    z[48]=z[2]*z[48];
    z[47]=n<T>(1,2)*z[47] + z[48];
    z[47]=z[2]*z[47];
    z[48]=static_cast<T>(653)+ 327*z[5];
    z[47]=n<T>(1,4)*z[48] + z[47];
    z[47]=z[2]*z[47];
    z[40]= - n<T>(751,2) - z[40];
    z[40]=n<T>(1,2)*z[40] + z[47];
    z[40]=z[40]*z[14];
    z[18]=z[18] + z[44] + z[40] - z[39] + static_cast<T>(40)+ n<T>(109,16)*z[5];
    z[18]=z[10]*z[18];
    z[39]= - static_cast<T>(7)+ n<T>(23,8)*z[1];
    z[39]=z[39]*z[33];
    z[37]=n<T>(37,4) - z[37];
    z[37]=z[1]*z[37];
    z[40]=z[1]*z[30];
    z[40]=static_cast<T>(3)+ z[40];
    z[40]=z[4]*z[40];
    z[15]=z[15] + z[18] + z[17] + z[27] + z[40] + z[39] - n<T>(357,16) + 
    z[37];
    z[15]=z[3]*z[15];
    z[17]=3*z[5];
    z[18]= - n<T>(7,4) - z[17];
    z[18]=3*z[18] + z[24];
    z[18]=z[2]*z[18];
    z[18]=z[18] + n<T>(197,4) + 27*z[5];
    z[18]=z[2]*z[18];
    z[27]= - n<T>(13,4) - z[5];
    z[18]=27*z[27] + z[18];
    z[18]=z[2]*z[18];
    z[27]=z[4] - n<T>(3,2);
    z[27]=z[27]*z[4];
    z[37]=n<T>(1,2)*z[8];
    z[17]=n<T>(31,2) + z[17];
    z[17]=z[18] + z[27] + 3*z[17] - z[37];
    z[18]=static_cast<T>(47)- z[8];
    z[39]=2*z[4];
    z[40]= - n<T>(27,4) + z[39];
    z[40]=z[4]*z[40];
    z[44]=n<T>(1,2)*z[4];
    z[47]=z[44] - 2;
    z[47]=z[47]*z[4];
    z[48]=n<T>(9,16) + z[47];
    z[48]=z[6]*z[48];
    z[18]=z[48] - n<T>(9,8)*z[2] + n<T>(1,4)*z[18] + z[40];
    z[18]=z[6]*z[18];
    z[40]=3*z[4];
    z[48]= - n<T>(11,4) + z[4];
    z[48]=z[48]*z[40];
    z[49]= - static_cast<T>(75)+ n<T>(11,4)*z[2];
    z[49]=z[49]*z[24];
    z[50]=3*z[8];
    z[51]=n<T>(577,4) - z[50];
    z[18]=z[18] + z[49] + n<T>(1,4)*z[51] + z[48];
    z[18]=z[6]*z[18];
    z[48]=n<T>(93,2) - z[2];
    z[48]=z[48]*z[24];
    z[48]= - static_cast<T>(48)+ z[48];
    z[48]=z[2]*z[48];
    z[49]=n<T>(349,2) - z[50];
    z[51]= - n<T>(17,4) + z[39];
    z[51]=z[4]*z[51];
    z[18]=z[18] + z[48] + n<T>(1,4)*z[49] + z[51];
    z[18]=z[6]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[10]*z[17];
    z[18]=n<T>(15,4) - z[39];
    z[18]=z[18]*z[40];
    z[48]=static_cast<T>(13)- z[14];
    z[48]=z[48]*z[14];
    z[49]= - n<T>(253,2) + z[50];
    z[51]=static_cast<T>(5)- z[39];
    z[51]=z[4]*z[51];
    z[51]= - n<T>(23,8) + z[51];
    z[51]=z[6]*z[51];
    z[18]=z[51] + z[48] + n<T>(1,4)*z[49] + z[18];
    z[18]=z[6]*z[18];
    z[48]= - n<T>(169,4) + z[8];
    z[49]=n<T>(5,2) - z[39];
    z[49]=z[4]*z[49];
    z[48]=n<T>(1,2)*z[48] + z[49];
    z[49]= - n<T>(33,4) + z[2];
    z[49]=z[2]*z[49];
    z[49]=n<T>(409,4) + z[49];
    z[49]=z[49]*z[14];
    z[18]=z[18] + 3*z[48] + z[49];
    z[18]=z[6]*z[18];
    z[34]=n<T>(75,4)*z[5] + z[46] - z[34];
    z[34]=z[2]*z[34];
    z[46]= - static_cast<T>(13)- n<T>(45,4)*z[5];
    z[34]=5*z[46] + z[34];
    z[34]=z[2]*z[34];
    z[20]=static_cast<T>(13)+ z[20];
    z[20]=n<T>(45,4)*z[20] + z[34];
    z[20]=z[20]*z[14];
    z[34]=n<T>(3,4)*z[8];
    z[46]=n<T>(5,4) - z[39];
    z[46]=z[4]*z[46];
    z[17]=z[17] + z[18] + z[20] + z[46] + z[34] - static_cast<T>(44)- n<T>(75,8)*z[5];
    z[17]=z[10]*z[17];
    z[18]=z[4] - 1;
    z[20]=z[18]*z[40];
    z[28]=n<T>(15,4)*z[2] + static_cast<T>(9)- z[28];
    z[28]=z[28]*z[24];
    z[46]=n<T>(65,4) + 3*z[11];
    z[48]=n<T>(1,8)*z[6];
    z[49]= - z[11]*z[48];
    z[20]=z[49] + z[28] + n<T>(1,4)*z[46] + z[20];
    z[20]=z[6]*z[20];
    z[28]= - static_cast<T>(9)- z[41];
    z[28]=n<T>(1,2)*z[28] - z[29];
    z[41]= - n<T>(17,2) + 5*z[9];
    z[41]=z[41]*z[24];
    z[41]=z[41] + z[43] - static_cast<T>(4)+ n<T>(1,4)*z[11];
    z[41]=z[2]*z[41];
    z[28]=n<T>(1,4)*z[28] + z[41];
    z[28]=z[2]*z[28];
    z[41]= - z[30]*z[39];
    z[41]=z[41] + n<T>(3,4) - z[1];
    z[41]=z[4]*z[41];
    z[43]= - z[50] + static_cast<T>(45)- n<T>(11,2)*z[1];
    z[20]=z[20] + z[28] + n<T>(1,4)*z[43] + z[41];
    z[20]=z[6]*z[20];
    z[28]=static_cast<T>(9)- n<T>(23,2)*z[5];
    z[28]=z[28]*z[42];
    z[41]=static_cast<T>(27)- 7*z[9];
    z[41]=n<T>(1,2)*z[41] - 9*z[1];
    z[43]=n<T>(7,8) + z[1];
    z[43]=z[5]*z[43];
    z[28]=z[28] + n<T>(1,4)*z[41] + z[43];
    z[28]=z[2]*z[28];
    z[41]=static_cast<T>(5)+ n<T>(1,4)*z[1];
    z[41]=z[1]*z[41];
    z[31]= - z[5]*z[31];
    z[28]=z[28] + z[31] - n<T>(5,4) + z[41];
    z[28]=z[2]*z[28];
    z[31]= - n<T>(13,2) - z[1];
    z[31]=z[1]*z[31];
    z[31]= - n<T>(113,4) + z[31];
    z[41]= - n<T>(71,8) + z[23];
    z[41]=z[5]*z[41];
    z[28]=z[28] + n<T>(1,2)*z[31] + z[41];
    z[28]=z[2]*z[28];
    z[31]=z[21]*z[37];
    z[41]= - n<T>(9,4) + z[1];
    z[41]=z[1]*z[41];
    z[32]=n<T>(3,2) + z[32];
    z[32]=z[4]*z[32];
    z[32]=z[32] + n<T>(3,2) + z[41];
    z[32]=z[4]*z[32];
    z[41]=static_cast<T>(3)- z[16];
    z[41]=z[1]*z[41];
    z[41]=n<T>(91,4) + z[41];
    z[43]=n<T>(55,16) - z[1];
    z[43]=z[5]*z[43];
    z[15]=z[15] + z[17] + z[20] + z[28] + z[32] + z[31] + n<T>(1,2)*z[41] + 
    z[43];
    z[15]=z[3]*z[15];
    z[17]=z[18]*z[6];
    z[20]=z[39]*z[17];
    z[28]= - static_cast<T>(5)- z[50];
    z[28]=z[8]*z[28];
    z[28]=n<T>(113,2) + z[28];
    z[31]=n<T>(3,2)*z[4];
    z[32]= - static_cast<T>(1)+ z[31];
    z[32]=z[4]*z[32];
    z[20]=z[20] - n<T>(11,16)*z[2] + n<T>(1,8)*z[28] + z[32];
    z[20]=z[6]*z[20];
    z[28]= - n<T>(61,2) + z[45];
    z[28]=z[28]*z[24];
    z[32]= - static_cast<T>(1)- z[50];
    z[32]=z[8]*z[32];
    z[32]=n<T>(283,4) + z[32];
    z[39]=n<T>(11,2) - z[40];
    z[39]=z[4]*z[39];
    z[20]=z[20] + z[28] + n<T>(1,4)*z[32] + z[39];
    z[20]=z[6]*z[20];
    z[28]=5*z[4];
    z[18]= - z[18]*z[28];
    z[32]=n<T>(1,4)*z[8];
    z[39]=static_cast<T>(1)+ z[32];
    z[39]=z[8]*z[39];
    z[18]=z[14] + z[18] - n<T>(55,8) + z[39];
    z[17]= - z[4]*z[17];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[6]*z[17];
    z[18]=n<T>(1,2) - z[4];
    z[18]=z[18]*z[31];
    z[31]=n<T>(63,8) - z[2];
    z[31]=z[31]*z[14];
    z[39]=static_cast<T>(1)+ n<T>(3,8)*z[8];
    z[39]=z[8]*z[39];
    z[17]=z[17] + z[31] + z[18] - static_cast<T>(14)+ z[39];
    z[17]=z[6]*z[17];
    z[18]= - n<T>(15,2) + z[2];
    z[18]=z[2]*z[18];
    z[18]=static_cast<T>(57)+ z[18];
    z[18]=z[18]*z[24];
    z[31]=static_cast<T>(1)+ z[34];
    z[31]=z[8]*z[31];
    z[31]= - n<T>(321,8) + z[31];
    z[17]=z[17] + z[18] + n<T>(1,2)*z[31] + z[47];
    z[17]=z[6]*z[17];
    z[18]=npow(z[8],2);
    z[31]=29*z[5];
    z[34]=z[18] - static_cast<T>(105)- z[31];
    z[39]=z[19] - z[31] - n<T>(77,2) - z[9];
    z[39]=z[39]*z[14];
    z[31]=z[39] + n<T>(277,4) + z[31];
    z[31]=z[31]*z[14];
    z[39]= - n<T>(5,2) + z[4];
    z[39]=z[4]*z[39];
    z[31]=z[31] + n<T>(1,4)*z[34] + z[39];
    z[17]=n<T>(1,2)*z[31] + z[17];
    z[17]=z[10]*z[17];
    z[31]=2*z[5];
    z[34]=static_cast<T>(1)- z[8];
    z[34]=z[8]*z[34];
    z[34]=n<T>(1,8)*z[34] + n<T>(41,8) + z[31];
    z[28]=static_cast<T>(9)- z[28];
    z[28]=z[28]*z[44];
    z[39]=static_cast<T>(7)+ z[9];
    z[31]=n<T>(1,8)*z[39] + z[31];
    z[31]=3*z[31] - z[35];
    z[31]=z[2]*z[31];
    z[31]=z[31] - n<T>(143,8) - 12*z[5];
    z[31]=z[2]*z[31];
    z[17]=z[17] + z[20] + z[31] + 3*z[34] + z[28];
    z[17]=z[10]*z[17];
    z[20]= - static_cast<T>(23)+ n<T>(9,2)*z[9];
    z[20]=z[20]*z[24];
    z[28]=static_cast<T>(9)+ z[11];
    z[20]=z[20] + n<T>(1,4)*z[28] + z[1];
    z[20]=z[20]*z[14];
    z[28]= - static_cast<T>(1)+ n<T>(3,2)*z[8];
    z[28]=z[8]*z[28];
    z[28]= - n<T>(11,2) + z[28];
    z[31]=n<T>(9,2) - z[38];
    z[31]=z[4]*z[31];
    z[31]= - n<T>(5,2) + z[31];
    z[31]=z[4]*z[31];
    z[34]=n<T>(5,4)*z[2] - n<T>(1,8)*z[11] + z[4];
    z[34]=z[6]*z[34];
    z[20]=z[34] + z[20] + n<T>(1,4)*z[28] + z[31];
    z[20]=z[6]*z[20];
    z[28]=n<T>(3,2)*z[1];
    z[31]= - static_cast<T>(11)+ z[28];
    z[31]=z[5]*z[31];
    z[31]=z[31] - n<T>(19,2) - z[23];
    z[23]= - static_cast<T>(5)+ z[23];
    z[21]= - z[8]*z[21];
    z[21]=n<T>(1,2)*z[23] + z[21];
    z[21]=z[8]*z[21];
    z[21]=n<T>(1,2)*z[31] + z[21];
    z[23]=n<T>(11,2) + z[29];
    z[29]=static_cast<T>(7)- z[28];
    z[29]=z[5]*z[29];
    z[23]=n<T>(1,2)*z[23] + z[29];
    z[28]=static_cast<T>(5)+ z[28];
    z[28]=z[28]*z[26];
    z[29]= - n<T>(1,2) - z[9];
    z[28]=z[28] + n<T>(1,2)*z[29] - z[1];
    z[29]=n<T>(3,2) - z[5];
    z[29]=z[2]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[28]=z[2]*z[28];
    z[23]=n<T>(1,4)*z[23] + z[28];
    z[23]=z[2]*z[23];
    z[28]= - n<T>(5,2) + z[1];
    z[28]=z[1]*z[28];
    z[28]=static_cast<T>(3)+ z[28];
    z[28]=z[4]*z[28];
    z[28]= - static_cast<T>(2)+ z[28];
    z[28]=z[4]*z[28];
    z[15]=z[15] + z[17] + z[20] + z[23] + n<T>(1,4)*z[21] + z[28];
    z[15]=z[3]*z[15];
    z[17]=5*z[12];
    z[20]=z[13]*z[12];
    z[21]= - z[17] - n<T>(3,2)*z[20];
    z[23]=n<T>(1,2)*z[13];
    z[21]=z[21]*z[23];
    z[28]=n<T>(1,2)*z[7];
    z[29]=z[28] - 3;
    z[31]=n<T>(3,2)*z[13];
    z[34]= - z[31] + z[29];
    z[34]=z[8]*z[34];
    z[35]=z[31] + 5;
    z[38]=z[13]*z[35];
    z[34]=z[38] + z[34];
    z[34]=z[34]*z[37];
    z[21]=z[34] + z[21] - z[28] + n<T>(7,4) - z[12];
    z[34]=3*z[20];
    z[38]= - z[17] - z[34];
    z[38]=z[38]*z[23];
    z[39]=3*z[13];
    z[40]=z[39] + 1;
    z[40]=z[40]*z[8];
    z[41]=static_cast<T>(5)+ z[39];
    z[41]=z[13]*z[41];
    z[41]=z[41] - z[40];
    z[41]=z[41]*z[37];
    z[43]=z[7]*z[12];
    z[38]=z[41] + z[38] - n<T>(1,2) + z[43];
    z[41]=z[8]*z[13];
    z[43]=npow(z[13],2);
    z[45]=z[41] - z[43];
    z[45]=z[45]*z[8];
    z[43]=z[43]*z[12];
    z[43]=z[45] + z[43];
    z[45]= - z[43]*z[48];
    z[46]=npow(z[4],2);
    z[38]=z[45] + n<T>(1,4)*z[38] + z[46];
    z[38]=z[6]*z[38];
    z[45]=z[4]*z[7];
    z[45]=n<T>(1,2) + z[45];
    z[45]=z[4]*z[45];
    z[21]=z[38] - z[14] + n<T>(1,2)*z[21] + z[45];
    z[21]=z[6]*z[21];
    z[38]=n<T>(1,4)*z[13];
    z[45]=static_cast<T>(5)+ z[13];
    z[45]=z[45]*z[38];
    z[47]=n<T>(1,4)*z[7];
    z[49]=z[47] - 1;
    z[50]=3*z[49] - z[38];
    z[50]=z[8]*z[50];
    z[45]=z[50] + z[45] - z[49];
    z[45]=z[45]*z[32];
    z[17]= - z[17] - z[20];
    z[17]=z[13]*z[17];
    z[50]=z[7] - 7;
    z[51]=z[7] - 3;
    z[52]=z[4]*z[51];
    z[52]= - n<T>(1,2)*z[50] + z[52];
    z[44]=z[52]*z[44];
    z[52]= - static_cast<T>(1)+ n<T>(1,8)*z[7];
    z[53]=z[42] + z[52];
    z[53]=z[2]*z[53];
    z[54]=n<T>(3,8)*z[7];
    z[17]=n<T>(1,2)*z[21] + z[53] + z[44] + z[45] + n<T>(1,16)*z[17] - z[54] + 2
    - n<T>(1,4)*z[12];
    z[17]=z[6]*z[17];
    z[21]=n<T>(3,2)*z[7];
    z[44]= - static_cast<T>(5)+ z[21];
    z[44]=z[44]*z[32];
    z[44]=z[44] - z[49];
    z[44]=z[8]*z[44];
    z[44]= - 7*z[52] + z[44];
    z[45]=z[7] - 9;
    z[52]=n<T>(1,2)*z[45] + z[2];
    z[52]=z[52]*z[14];
    z[17]=z[17] + z[52] + n<T>(1,2)*z[44] - z[27];
    z[17]=z[6]*z[17];
    z[44]=static_cast<T>(11)- z[21];
    z[26]=z[44]*z[26];
    z[44]=z[51]*z[32];
    z[44]=z[44] - z[49];
    z[44]=z[44]*z[37];
    z[19]=z[19] - 11*z[5] - static_cast<T>(21)+ z[7];
    z[19]=z[19]*z[24];
    z[49]= - static_cast<T>(7)+ n<T>(3,4)*z[7];
    z[19]=z[19] + z[44] + z[26] - z[49];
    z[17]=n<T>(1,2)*z[19] + z[17];
    z[17]=z[10]*z[17];
    z[19]=z[49]*z[33];
    z[26]= - z[8]*z[29];
    z[26]=z[26] + z[28] - 5;
    z[26]=z[26]*z[32];
    z[29]=7*z[5] - z[28] + n<T>(29,4) + z[9];
    z[29]=n<T>(1,2)*z[29] - z[2];
    z[29]=z[2]*z[29];
    z[19]=z[29] - z[27] + z[26] + z[19] - static_cast<T>(5)+ z[54];
    z[20]=z[12] + n<T>(1,4)*z[20];
    z[26]=z[20]*z[31];
    z[27]= - z[28] + z[35];
    z[27]=z[27]*z[37];
    z[29]= - static_cast<T>(1)- z[38];
    z[29]=z[13]*z[29];
    z[29]= - n<T>(1,4) + z[29];
    z[27]=3*z[29] + z[27];
    z[27]=z[27]*z[37];
    z[26]=z[27] + z[26] + z[47] - n<T>(5,4) + z[12];
    z[27]=n<T>(11,2)*z[12] + z[34];
    z[27]=z[27]*z[23];
    z[29]= - n<T>(11,2) - z[39];
    z[29]=z[13]*z[29];
    z[29]=z[29] + z[40];
    z[29]=z[29]*z[37];
    z[31]= - z[21] + n<T>(1,2);
    z[31]=z[12]*z[31];
    z[27]=z[29] + z[27] + static_cast<T>(1)+ z[31];
    z[29]=z[12] + n<T>(3,2)*z[43];
    z[29]=z[29]*z[48];
    z[27]=z[29] + n<T>(1,4)*z[27] - z[46];
    z[27]=z[6]*z[27];
    z[29]=n<T>(1,2) - z[7];
    z[29]=z[4]*z[29];
    z[29]= - n<T>(3,4) + z[29];
    z[29]=z[4]*z[29];
    z[22]=z[27] + z[22] + n<T>(1,2)*z[26] + z[29];
    z[22]=z[6]*z[22];
    z[26]= - z[13] - static_cast<T>(13)+ z[7];
    z[27]= - z[8]*z[50];
    z[26]=n<T>(1,2)*z[26] + z[27];
    z[26]=z[26]*z[37];
    z[20]=z[26] + z[7] - n<T>(31,4) + z[20];
    z[26]= - static_cast<T>(5)+ z[7];
    z[27]=static_cast<T>(1)- z[28];
    z[27]=z[4]*z[27];
    z[26]=n<T>(1,4)*z[26] + z[27];
    z[26]=z[4]*z[26];
    z[27]= - z[2] - z[45];
    z[27]=z[27]*z[42];
    z[20]=z[22] + z[27] + n<T>(1,4)*z[20] + z[26];
    z[20]=z[6]*z[20];
    z[17]=z[17] + n<T>(1,2)*z[19] + z[20];
    z[17]=z[10]*z[17];
    z[19]=z[28]*z[12];
    z[20]= - z[12] - z[19];
    z[20]=z[13]*z[20];
    z[20]= - n<T>(5,2)*z[12] + z[20];
    z[20]=z[20]*z[23];
    z[22]=z[28] + 1;
    z[26]= - z[22]*z[41];
    z[22]=z[13]*z[22];
    z[22]=n<T>(5,2) + z[22];
    z[22]=z[13]*z[22];
    z[22]=z[22] + z[26];
    z[22]=z[22]*z[37];
    z[20]=z[22] + z[20] + static_cast<T>(1)+ z[19];
    z[22]= - z[12] - z[43];
    z[22]=z[22]*z[48];
    z[26]= - z[12]*z[42];
    z[20]=z[22] + z[26] + n<T>(1,4)*z[20] + z[46];
    z[20]=z[6]*z[20];
    z[22]=z[7] + 1;
    z[22]=z[22]*z[23];
    z[23]= - z[22] - n<T>(3,2);
    z[23]=z[12]*z[23];
    z[26]=z[7]*z[9];
    z[23]=z[26] + static_cast<T>(5)+ z[23];
    z[22]=static_cast<T>(1)+ z[22];
    z[22]=n<T>(1,2)*z[22] - z[8];
    z[22]=z[8]*z[22];
    z[22]=n<T>(1,2)*z[23] + z[22];
    z[21]=z[21] - z[30];
    z[21]=z[4]*z[21];
    z[21]= - n<T>(1,4) + z[21];
    z[21]=z[4]*z[21];
    z[14]= - z[14] + n<T>(1,2);
    z[14]=z[12]*z[14];
    z[14]= - z[19] - static_cast<T>(3)+ z[9] + z[14];
    z[14]=z[14]*z[24];
    z[14]=z[20] + z[14] + n<T>(1,4)*z[22] + z[21];
    z[14]=z[6]*z[14];
    z[18]= - n<T>(1,2)*z[18] + z[25];
    z[19]=static_cast<T>(3)+ z[7];
    z[19]=z[7]*z[19];
    z[19]=static_cast<T>(5)+ z[19];
    z[16]=z[16] - n<T>(5,2) - z[7];
    z[16]=z[1]*z[16];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[16]=z[4]*z[16];
    z[19]= - 3*z[7] + z[36];
    z[16]=n<T>(1,4)*z[19] + z[16];
    z[16]=z[4]*z[16];
    z[19]=static_cast<T>(1)- z[33];
    z[19]=z[2]*z[19];
    z[19]=n<T>(3,2)*z[19] - n<T>(5,4) - z[9];
    z[19]=z[19]*z[24];

    r += z[14] + z[15] + z[16] + z[17] + n<T>(1,8)*z[18] + z[19];
 
    return r;
}

template double qqb_2lha_r748(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r748(const std::array<dd_real,30>&);
#endif
