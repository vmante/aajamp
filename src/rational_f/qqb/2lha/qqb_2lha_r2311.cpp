#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2311(const std::array<T,30>& k) {
  T z[35];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[17];
    z[5]=k[20];
    z[6]=k[3];
    z[7]=k[4];
    z[8]=3*z[6];
    z[9]=z[5] + z[4];
    z[10]=n<T>(1,2)*z[4];
    z[11]=z[10] + 1;
    z[12]=z[7]*z[11];
    z[12]=z[8] + z[12] - n<T>(9,2) + z[9];
    z[13]=n<T>(1,2)*z[3];
    z[12]=z[12]*z[13];
    z[14]=n<T>(1,4)*z[7];
    z[15]=z[5] - z[4];
    z[16]= - static_cast<T>(1)+ z[15];
    z[16]=z[16]*z[14];
    z[17]=n<T>(1,2)*z[6];
    z[18]=static_cast<T>(3)- 7*z[4];
    z[12]=z[12] - z[17] + z[16] + n<T>(1,4)*z[18] - z[5];
    z[12]=z[12]*z[13];
    z[16]= - static_cast<T>(9)+ z[9];
    z[18]=n<T>(1,2)*z[5];
    z[19]=z[18] - z[4];
    z[20]=n<T>(1,2) - z[19];
    z[20]=z[7]*z[20];
    z[16]=z[6] + n<T>(1,2)*z[16] + z[20];
    z[16]=z[3]*z[16];
    z[20]=3*z[4];
    z[21]=static_cast<T>(1)- z[20];
    z[22]= - z[10] + z[5];
    z[22]=z[7]*z[22];
    z[16]=z[16] + n<T>(1,2)*z[21] + z[22];
    z[16]=z[3]*z[16];
    z[16]=z[16] - static_cast<T>(1)- n<T>(5,2)*z[4];
    z[21]=n<T>(1,2)*z[7];
    z[15]= - z[15]*z[21];
    z[15]=z[15] - static_cast<T>(1)- z[9];
    z[15]=z[3]*z[15];
    z[22]=3*z[5];
    z[23]=z[4] + z[22];
    z[15]=n<T>(1,2)*z[23] + z[15];
    z[15]=z[3]*z[15];
    z[23]=z[1]*npow(z[3],2);
    z[24]=n<T>(1,2)*z[23];
    z[9]= - z[9]*z[24];
    z[9]=z[15] + z[9];
    z[15]=n<T>(1,4)*z[1];
    z[9]=z[9]*z[15];
    z[9]=z[9] - z[5] + n<T>(1,4)*z[16];
    z[9]=z[1]*z[9];
    z[15]=static_cast<T>(1)+ n<T>(11,4)*z[5];
    z[15]=z[15]*z[17];
    z[16]=2*z[5];
    z[25]=z[7]*z[5];
    z[26]= - static_cast<T>(3)+ n<T>(5,4)*z[4];
    z[9]=z[9] + z[12] + z[15] + n<T>(5,4)*z[25] + n<T>(1,2)*z[26] - z[16];
    z[9]=z[1]*z[9];
    z[12]=11*z[6];
    z[15]=z[12] - n<T>(73,2) + z[5];
    z[26]=static_cast<T>(47)- z[5];
    z[26]=n<T>(1,2)*z[26] - 16*z[6];
    z[26]=z[3]*z[26];
    z[15]=n<T>(1,2)*z[15] + z[26];
    z[15]=z[3]*z[15];
    z[26]=static_cast<T>(21)- z[10] - z[18];
    z[27]=n<T>(1,4)*z[23];
    z[26]=z[26]*z[27];
    z[15]=z[15] + z[26];
    z[15]=z[1]*z[15];
    z[26]= - static_cast<T>(37)- n<T>(19,2)*z[6];
    z[26]=z[26]*z[17];
    z[28]=static_cast<T>(53)- z[5];
    z[12]= - static_cast<T>(37)+ z[12];
    z[12]=z[6]*z[12];
    z[12]=n<T>(1,2)*z[28] + z[12];
    z[12]=z[3]*z[12];
    z[12]=z[12] - n<T>(85,2) + z[5];
    z[28]=n<T>(295,2) - 23*z[6];
    z[28]=z[6]*z[28];
    z[12]=z[28] + 3*z[12];
    z[12]=z[12]*z[13];
    z[28]=n<T>(1,4)*z[5];
    z[29]=static_cast<T>(8)- z[28];
    z[12]=z[15] + z[12] + 3*z[29] + z[26];
    z[12]=z[1]*z[12];
    z[15]= - n<T>(97,2) + z[5];
    z[26]=13*z[6];
    z[29]= - n<T>(331,2) + z[26];
    z[29]=z[6]*z[29];
    z[29]=static_cast<T>(295)+ z[29];
    z[29]=z[6]*z[29];
    z[15]=3*z[15] + z[29];
    z[29]=n<T>(27,2) - 2*z[6];
    z[8]=z[29]*z[8];
    z[8]= - n<T>(127,2) + z[8];
    z[8]=z[6]*z[8];
    z[29]=static_cast<T>(59)- z[5];
    z[8]=n<T>(1,2)*z[29] + z[8];
    z[8]=z[3]*z[8];
    z[8]=n<T>(1,2)*z[15] + z[8];
    z[8]=z[3]*z[8];
    z[15]= - n<T>(55,2) + z[5];
    z[15]=z[15]*z[21];
    z[29]=static_cast<T>(49)- 11*z[7];
    z[30]=5*z[7];
    z[31]=n<T>(19,2) - z[30];
    z[31]=z[6]*z[31];
    z[29]=n<T>(1,4)*z[29] + z[31];
    z[29]=z[6]*z[29];
    z[31]= - static_cast<T>(18)+ 7*z[7];
    z[29]=3*z[31] + z[29];
    z[29]=z[6]*z[29];
    z[8]=z[12] + z[8] + z[29] + z[15] + n<T>(133,4) - z[5];
    z[8]=z[1]*z[8];
    z[12]=z[6] - 3;
    z[12]=z[12]*z[6];
    z[12]=z[12] + 3;
    z[15]=z[7] - 2;
    z[29]= - z[6]*z[15]*z[12];
    z[31]= - static_cast<T>(3)+ z[3];
    z[12]=z[12]*z[6];
    z[12]=z[12] - 1;
    z[12]=z[3]*z[12]*z[31];
    z[12]=z[12] + z[29] + z[15];
    z[29]=static_cast<T>(2)- z[3];
    z[29]=z[3]*z[29];
    z[29]=z[29] - 1;
    z[31]=z[6] - 2;
    z[31]=z[31]*z[6];
    z[31]=z[31] + 1;
    z[29]=z[31]*z[29];
    z[31]=z[3] - 1;
    z[32]=z[6] - 1;
    z[31]=z[3]*z[32]*z[31];
    z[24]=2*z[31] - z[24];
    z[24]=z[1]*z[24];
    z[24]=3*z[29] + z[24];
    z[24]=z[1]*z[24];
    z[12]=2*z[12] + z[24];
    z[12]=z[1]*z[12];
    z[15]=z[15]*z[7];
    z[15]=z[15] + 3;
    z[24]=2*z[15];
    z[29]=z[21] - 1;
    z[31]=z[29]*z[7];
    z[31]=z[31] + n<T>(3,2);
    z[32]= - z[6]*z[31];
    z[32]=z[24] + z[32];
    z[32]=z[6]*z[32];
    z[15]= - 3*z[15] + z[32];
    z[15]=z[6]*z[15];
    z[15]=z[24] + z[15];
    z[15]=z[6]*z[15];
    z[24]= - static_cast<T>(4)+ z[6];
    z[24]=z[6]*z[24];
    z[24]=static_cast<T>(6)+ z[24];
    z[24]=z[6]*z[24];
    z[24]= - static_cast<T>(4)+ z[24];
    z[24]=z[6]*z[24];
    z[24]=static_cast<T>(1)+ z[24];
    z[32]=static_cast<T>(2)- z[17];
    z[32]=z[6]*z[32];
    z[32]= - static_cast<T>(3)+ z[32];
    z[32]=z[6]*z[32];
    z[32]=static_cast<T>(2)+ z[32];
    z[32]=z[6]*z[32];
    z[32]= - n<T>(1,2) + z[32];
    z[32]=z[3]*z[32];
    z[24]=2*z[24] + z[32];
    z[24]=z[3]*z[24];
    z[12]=z[12] + z[24] + z[15] - z[31];
    z[12]=z[2]*z[12];
    z[15]=n<T>(1,8)*z[7];
    z[24]=static_cast<T>(23)- z[5];
    z[24]=z[24]*z[15];
    z[24]=z[24] - static_cast<T>(6)+ z[28];
    z[24]=z[7]*z[24];
    z[31]= - n<T>(39,2) - z[30];
    z[31]=z[7]*z[31];
    z[31]=n<T>(5,2) + z[31];
    z[32]= - n<T>(19,4) + z[30];
    z[32]=z[6]*z[32];
    z[31]=n<T>(1,2)*z[31] + z[32];
    z[31]=z[6]*z[31];
    z[32]= - static_cast<T>(1)+ n<T>(61,8)*z[7];
    z[32]=z[7]*z[32];
    z[31]=z[31] + n<T>(201,8) + z[32];
    z[31]=z[6]*z[31];
    z[32]=static_cast<T>(23)- n<T>(63,4)*z[7];
    z[32]=z[7]*z[32];
    z[32]= - n<T>(283,4) + z[32];
    z[31]=n<T>(1,2)*z[32] + z[31];
    z[31]=z[6]*z[31];
    z[32]=static_cast<T>(65)- z[5];
    z[33]= - static_cast<T>(17)+ z[17];
    z[33]=z[6]*z[33];
    z[33]=n<T>(193,4) + z[33];
    z[33]=z[6]*z[33];
    z[33]= - n<T>(191,4) + z[33];
    z[33]=z[6]*z[33];
    z[32]=n<T>(1,4)*z[32] + z[33];
    z[32]=z[3]*z[32];
    z[33]=n<T>(109,2) - z[6];
    z[33]=z[6]*z[33];
    z[33]= - n<T>(317,2) + z[33];
    z[33]=z[6]*z[33];
    z[33]=n<T>(317,2) + z[33];
    z[33]=z[6]*z[33];
    z[32]=z[32] + z[33] - n<T>(109,2) + z[5];
    z[32]=z[32]*z[13];
    z[33]=static_cast<T>(113)- z[22];
    z[8]=5*z[12] + z[8] + z[32] + z[31] + n<T>(1,8)*z[33] + z[24];
    z[8]=z[2]*z[8];
    z[12]= - static_cast<T>(23)+ z[10];
    z[12]=3*z[12] + z[18];
    z[12]=n<T>(1,2)*z[12] + z[26];
    z[12]=z[12]*z[13];
    z[12]=z[12] + n<T>(35,4) + z[19];
    z[12]=z[3]*z[12];
    z[19]=n<T>(1,8)*z[5];
    z[24]=z[19] - 3;
    z[26]=n<T>(1,8)*z[4];
    z[31]=z[26] + z[24];
    z[23]=z[31]*z[23];
    z[12]=z[12] + z[23];
    z[12]=z[1]*z[12];
    z[23]= - static_cast<T>(23)- 9*z[5];
    z[31]=5*z[6];
    z[32]= - n<T>(9,2) - z[31];
    z[32]=z[6]*z[32];
    z[23]=n<T>(1,2)*z[23] + z[32];
    z[32]= - static_cast<T>(11)- z[19];
    z[33]=n<T>(121,4) - 4*z[6];
    z[33]=z[6]*z[33];
    z[32]=3*z[32] + z[33];
    z[32]=z[3]*z[32];
    z[33]=static_cast<T>(23)+ n<T>(7,4)*z[5];
    z[33]=3*z[33] - n<T>(85,2)*z[6];
    z[32]=n<T>(1,2)*z[33] + z[32];
    z[32]=z[3]*z[32];
    z[12]=z[12] + n<T>(1,2)*z[23] + z[32];
    z[12]=z[1]*z[12];
    z[23]= - static_cast<T>(43)- z[5];
    z[32]= - n<T>(55,2) + z[6];
    z[32]=z[6]*z[32];
    z[32]=n<T>(161,2) + z[32];
    z[32]=z[6]*z[32];
    z[23]=n<T>(5,4)*z[23] + z[32];
    z[23]=z[23]*z[13];
    z[28]=static_cast<T>(3)+ z[28];
    z[32]= - static_cast<T>(61)+ n<T>(55,4)*z[6];
    z[32]=z[6]*z[32];
    z[23]=z[23] + 15*z[28] + z[32];
    z[23]=z[3]*z[23];
    z[28]=29*z[5];
    z[32]= - static_cast<T>(35)- z[28];
    z[33]= - n<T>(1,2) + 5*z[5];
    z[33]=z[7]*z[33];
    z[32]=n<T>(1,4)*z[32] + z[33];
    z[33]=z[31] + static_cast<T>(2)- z[30];
    z[33]=z[6]*z[33];
    z[33]=n<T>(13,4)*z[7] + z[33];
    z[33]=z[6]*z[33];
    z[12]=z[12] + z[23] + n<T>(1,2)*z[32] + z[33];
    z[12]=z[1]*z[12];
    z[23]= - static_cast<T>(45)- z[7];
    z[14]=z[23]*z[14];
    z[23]=n<T>(5,2)*z[6];
    z[32]= - z[23] + n<T>(1,4) + z[30];
    z[32]=z[6]*z[32];
    z[14]=z[32] + static_cast<T>(3)+ z[14];
    z[14]=z[6]*z[14];
    z[32]=static_cast<T>(67)- z[7];
    z[32]=z[7]*z[32];
    z[32]=static_cast<T>(19)+ z[32];
    z[14]=n<T>(1,8)*z[32] + z[14];
    z[14]=z[6]*z[14];
    z[32]=static_cast<T>(9)- z[17];
    z[32]=z[6]*z[32];
    z[32]= - n<T>(65,4) + z[32];
    z[32]=z[32]*z[31];
    z[33]= - n<T>(63,2) - z[5];
    z[34]= - static_cast<T>(19)+ n<T>(3,2)*z[6];
    z[34]=z[6]*z[34];
    z[34]=n<T>(67,2) + z[34];
    z[34]=z[6]*z[34];
    z[33]=n<T>(1,2)*z[33] + z[34];
    z[33]=z[3]*z[33];
    z[34]=static_cast<T>(73)+ n<T>(13,2)*z[5];
    z[32]=z[33] + n<T>(1,2)*z[34] + z[32];
    z[32]=z[32]*z[13];
    z[33]=static_cast<T>(9)- 7*z[5];
    z[33]=z[33]*z[21];
    z[33]=z[33] - static_cast<T>(13)+ n<T>(11,2)*z[5];
    z[33]=z[7]*z[33];
    z[34]= - static_cast<T>(15)- 13*z[5];
    z[33]=n<T>(1,2)*z[34] + z[33];
    z[8]=z[8] + z[12] + z[32] + n<T>(1,4)*z[33] + z[14];
    z[8]=z[2]*z[8];
    z[12]= - z[17] + n<T>(5,8)*z[5] + static_cast<T>(4)- z[26];
    z[12]=z[3]*z[12];
    z[10]= - n<T>(3,2)*z[5] - static_cast<T>(1)+ z[10];
    z[10]=n<T>(1,2)*z[10] + z[12];
    z[10]=z[3]*z[10];
    z[11]=z[18] + z[11];
    z[11]=z[11]*z[27];
    z[10]=z[10] + z[11];
    z[10]=z[1]*z[10];
    z[11]=static_cast<T>(1)- z[4];
    z[11]=7*z[11] - z[5];
    z[11]=n<T>(1,2)*z[11] - z[31];
    z[12]=static_cast<T>(79)- z[20];
    z[12]=n<T>(1,2)*z[12] + z[22];
    z[14]= - static_cast<T>(5)+ n<T>(1,4)*z[6];
    z[14]=z[6]*z[14];
    z[12]=n<T>(1,4)*z[12] + z[14];
    z[12]=z[3]*z[12];
    z[14]= - static_cast<T>(11)+ n<T>(13,4)*z[4];
    z[12]=z[12] + n<T>(3,4)*z[6] + n<T>(1,2)*z[14] - z[5];
    z[12]=z[3]*z[12];
    z[10]=z[10] + n<T>(1,2)*z[11] + z[12];
    z[10]=z[1]*z[10];
    z[11]=static_cast<T>(15)- z[17];
    z[11]=z[11]*z[17];
    z[12]= - n<T>(17,2) + z[6];
    z[12]=z[6]*z[12];
    z[12]=z[12] + static_cast<T>(9)+ z[19];
    z[12]=z[3]*z[12];
    z[11]=z[12] + z[11] - n<T>(63,8) + z[5];
    z[11]=z[3]*z[11];
    z[12]=static_cast<T>(5)- z[28];
    z[14]= - static_cast<T>(1)+ z[22];
    z[14]=z[7]*z[14];
    z[12]=n<T>(1,2)*z[12] + 3*z[14];
    z[14]= - n<T>(23,2) - z[7];
    z[14]=n<T>(1,2)*z[14] + z[31];
    z[14]=z[6]*z[14];
    z[10]=z[10] + z[11] + n<T>(1,4)*z[12] + z[14];
    z[10]=z[1]*z[10];
    z[11]= - n<T>(7,2) + z[5];
    z[12]=static_cast<T>(11)- z[23];
    z[12]=z[6]*z[12];
    z[11]=n<T>(5,2)*z[11] + z[12];
    z[12]= - static_cast<T>(31)+ 7*z[6];
    z[12]=z[6]*z[12];
    z[12]=n<T>(1,8)*z[12] - z[24];
    z[12]=z[3]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[3]*z[11];
    z[12]=11*z[5];
    z[14]=static_cast<T>(5)- z[12];
    z[14]=z[14]*z[21];
    z[12]=z[14] - n<T>(17,2) + z[12];
    z[12]=z[7]*z[12];
    z[12]=z[12] + static_cast<T>(7)- n<T>(19,2)*z[5];
    z[14]= - z[23] + static_cast<T>(4)+ z[21];
    z[14]=z[6]*z[14];
    z[14]= - n<T>(17,8) + z[14];
    z[14]=z[6]*z[14];
    z[8]=z[8] + z[10] + z[11] + n<T>(1,4)*z[12] + z[14];
    z[8]=z[2]*z[8];
    z[10]= - static_cast<T>(1)- n<T>(5,2)*z[5];
    z[10]=z[10]*z[17];
    z[10]=z[10] - n<T>(5,2)*z[25] + n<T>(7,4) + z[22];
    z[10]=z[10]*z[17];
    z[11]= - z[23] - z[21] - static_cast<T>(1)+ z[18];
    z[12]=n<T>(1,2)*z[29] + z[6];
    z[12]=z[3]*z[12];
    z[11]=n<T>(1,2)*z[11] + z[12];
    z[11]=z[11]*z[13];
    z[12]=static_cast<T>(23)- z[30];
    z[12]=z[15]*z[5]*z[12];

    r += z[8] + z[9] + z[10] + z[11] + z[12] - z[16];
 
    return r;
}

template double qqb_2lha_r2311(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2311(const std::array<dd_real,30>&);
#endif
