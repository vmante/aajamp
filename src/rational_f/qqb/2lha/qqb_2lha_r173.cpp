#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r173(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[7];
    z[5]=k[20];
    z[6]=k[12];
    z[7]=k[2];
    z[8]=k[3];
    z[9]=k[11];
    z[10]=k[4];
    z[11]=k[5];
    z[12]=n<T>(17,3) + 7*z[10];
    z[13]= - z[5]*z[10];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[13]=n<T>(1,2)*z[8];
    z[14]=n<T>(73,3) + 3*z[10];
    z[14]=n<T>(1,4)*z[14] - n<T>(7,3)*z[8];
    z[14]=z[14]*z[13];
    z[14]=z[14] - n<T>(31,12) - z[10];
    z[14]=z[8]*z[14];
    z[15]=n<T>(1,4)*z[8];
    z[16]= - static_cast<T>(9)+ n<T>(95,6)*z[8];
    z[16]=z[16]*z[15];
    z[17]=n<T>(3,2)*z[4];
    z[18]= - z[17] + 3*z[5];
    z[16]=z[16] - n<T>(77,24) + z[18];
    z[19]=n<T>(1,2)*z[1];
    z[16]=z[16]*z[19];
    z[20]= - static_cast<T>(1)+ n<T>(1,3)*z[8];
    z[20]=z[8]*z[20];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[8]*z[10]*z[20];
    z[20]= - n<T>(1,3)*z[10] + z[20];
    z[13]=z[13] - 1;
    z[13]=z[13]*z[8];
    z[21]=n<T>(1,2) + z[13];
    z[21]=z[1]*z[21];
    z[20]=n<T>(1,2)*z[20] + z[21];
    z[21]=n<T>(11,2)*z[2];
    z[20]=z[20]*z[21];
    z[22]=n<T>(1,4)*z[4];
    z[23]=z[10]*z[22];
    z[12]=z[20] + z[16] + z[23] + n<T>(1,2)*z[12] + z[14];
    z[12]=z[2]*z[12];
    z[14]= - z[5] - 1;
    z[16]=n<T>(1,4)*z[10];
    z[14]=z[16]*z[14];
    z[14]=static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[14];
    z[16]= - n<T>(5,2)*z[8] + static_cast<T>(11)+ z[16];
    z[16]=z[16]*z[15];
    z[20]=n<T>(1,2)*z[4];
    z[23]= - z[20] + n<T>(1,2);
    z[23]=z[10]*z[23];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[23]*z[22];
    z[24]=npow(z[4],2);
    z[25]=n<T>(3,8)*z[24];
    z[26]=npow(z[5],2);
    z[27]=z[25] + n<T>(23,16)*z[8] - static_cast<T>(1)+ n<T>(3,4)*z[26];
    z[27]=z[1]*z[27];
    z[28]= - static_cast<T>(23)- n<T>(1,2)*z[10];
    z[12]=z[12] + z[27] + z[23] + z[16] + n<T>(1,8)*z[28] + z[14];
    z[12]=z[2]*z[12];
    z[14]=n<T>(1,2)*z[5];
    z[16]=static_cast<T>(1)- z[10];
    z[16]=z[5]*z[16];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=z[16]*z[14];
    z[23]=static_cast<T>(1)+ 9*z[10];
    z[27]=3*z[4];
    z[28]= - z[10]*z[27];
    z[23]=n<T>(1,2)*z[23] + z[28];
    z[23]=z[4]*z[23];
    z[23]=z[23] - static_cast<T>(1)- n<T>(9,2)*z[10];
    z[23]=z[23]*z[22];
    z[28]= - static_cast<T>(5)+ n<T>(9,2)*z[4];
    z[28]=z[4]*z[28];
    z[28]=n<T>(9,4) + z[28];
    z[28]=z[4]*z[28];
    z[28]=z[28] + n<T>(1,4) + z[26];
    z[28]=z[28]*z[19];
    z[29]=n<T>(1,8)*z[8];
    z[16]=z[28] + z[23] - z[29] + z[16] - static_cast<T>(1)+ n<T>(7,12)*z[10];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[2]*z[12];
    z[16]=static_cast<T>(1)- z[4];
    z[16]=z[16]*z[27];
    z[23]=n<T>(7,3) - 3*z[24];
    z[23]=z[1]*z[23];
    z[16]=z[23] + n<T>(7,3) + z[16];
    z[16]=z[20]*z[16];
    z[16]= - n<T>(7,6) + z[26] + z[16];
    z[12]=n<T>(1,4)*z[16] + z[12];
    z[16]=z[7] - 1;
    z[23]=z[16]*z[27];
    z[23]=n<T>(5,2) + z[23];
    z[23]=z[23]*z[24];
    z[28]=n<T>(1,2)*z[6];
    z[30]=static_cast<T>(1)+ z[28];
    z[31]=static_cast<T>(1)- z[6];
    z[31]=z[8]*z[6]*z[31];
    z[32]=npow(z[4],3);
    z[33]= - n<T>(7,4)*z[6] - z[32];
    z[33]=z[1]*z[33];
    z[23]=n<T>(3,4)*z[33] + n<T>(1,4)*z[23] + n<T>(21,16)*z[31] + n<T>(1,2)*z[30] - 
    z[26];
    z[23]=z[23]*z[19];
    z[30]=z[26] - 1;
    z[28]= - static_cast<T>(1)+ z[28];
    z[28]=z[6]*z[28];
    z[28]=z[28] + z[30];
    z[15]=z[28]*z[15];
    z[28]=z[4]*z[7];
    z[16]= - z[16]*z[28];
    z[31]=static_cast<T>(1)- 5*z[7];
    z[16]=n<T>(1,4)*z[31] + z[16];
    z[16]=z[16]*z[25];
    z[15]=z[23] + z[16] + z[15] - n<T>(41,64)*z[6] - z[5];
    z[15]=z[1]*z[15];
    z[16]=z[9] - 1;
    z[16]=z[16]*z[6];
    z[23]=z[7]*z[9];
    z[25]=31*z[23] - 41*z[16] - static_cast<T>(35)+ 97*z[9];
    z[31]=n<T>(35,4)*z[9];
    z[16]=n<T>(41,4)*z[16] + n<T>(91,4) - 33*z[9];
    z[16]=z[6]*z[16];
    z[16]=z[16] + z[11] + z[31];
    z[16]=z[8]*z[16];
    z[16]=n<T>(1,4)*z[25] + z[16];
    z[25]=npow(z[7],2);
    z[33]= - z[23] + static_cast<T>(1)+ z[9];
    z[33]=z[7]*z[33];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[4]*z[33]*z[25];
    z[34]= - 5*z[23] + static_cast<T>(5)+ z[9];
    z[34]=z[7]*z[34];
    z[34]= - static_cast<T>(1)+ z[34];
    z[34]=z[7]*z[34];
    z[33]=n<T>(1,4)*z[34] + z[33];
    z[33]=z[33]*z[27];
    z[34]= - n<T>(5,2)*z[23] + n<T>(5,2) + z[9];
    z[34]=z[7]*z[34];
    z[34]= - static_cast<T>(1)+ z[34];
    z[33]=n<T>(1,2)*z[34] + z[33];
    z[33]=z[4]*z[33];
    z[16]=n<T>(1,2)*z[16] + z[33];
    z[15]=n<T>(1,8)*z[16] + z[15];
    z[16]=static_cast<T>(3)+ z[20];
    z[16]=z[16]*z[20];
    z[33]=n<T>(1,2)*z[24];
    z[34]= - static_cast<T>(1)+ z[27];
    z[34]=z[34]*z[33];
    z[34]=z[34] - n<T>(17,8) - z[26];
    z[34]=z[1]*z[34];
    z[35]= - static_cast<T>(5)+ z[5];
    z[35]=z[5]*z[35];
    z[16]=z[34] + z[16] + z[35] + n<T>(25,8)*z[8];
    z[16]=z[1]*z[16];
    z[34]=npow(z[8],2);
    z[34]=z[20] + n<T>(41,12) - 3*z[34];
    z[16]=n<T>(1,2)*z[34] + z[16];
    z[16]=z[1]*z[16];
    z[34]=z[8] - 3;
    z[34]=z[34]*z[8];
    z[34]=z[34] + n<T>(53,6);
    z[34]=z[34]*z[8];
    z[35]= - n<T>(15,2) + z[34];
    z[16]=n<T>(1,2)*z[35] + z[16];
    z[35]=z[20] + 1;
    z[35]=z[35]*z[20];
    z[14]= - static_cast<T>(1)+ z[14];
    z[14]=z[5]*z[14];
    z[14]=z[35] - n<T>(17,16) + z[14];
    z[14]=z[14]*z[19];
    z[22]=z[22] - z[5];
    z[14]=z[14] + n<T>(17,16)*z[8] - n<T>(23,24) - z[22];
    z[14]=z[1]*z[14];
    z[36]=n<T>(11,3) - z[8];
    z[36]=z[8]*z[36];
    z[36]=n<T>(41,6) + z[36];
    z[14]=n<T>(1,8)*z[36] + z[14];
    z[14]=z[14]*z[19];
    z[36]=n<T>(17,16) + z[8];
    z[20]= - z[20] + n<T>(7,24) + z[5];
    z[20]=z[1]*z[20];
    z[20]=n<T>(1,3)*z[36] + n<T>(1,4)*z[20];
    z[36]=npow(z[1],2);
    z[20]=z[20]*z[36];
    z[37]=z[2]*npow(z[1],3);
    z[20]=z[20] + n<T>(11,24)*z[37];
    z[20]=z[2]*z[20];
    z[14]=z[14] + z[20];
    z[14]=z[2]*z[14];
    z[14]=n<T>(1,8)*z[16] + z[14];
    z[14]=z[2]*z[14];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[3]*z[14];
    z[15]=static_cast<T>(21)- n<T>(17,2)*z[6];
    z[15]=z[6]*z[15];
    z[16]=z[1]*npow(z[6],2);
    z[15]= - n<T>(21,8)*z[16] + n<T>(1,4)*z[15] + 3*z[32];
    z[15]=z[1]*z[15];
    z[16]=static_cast<T>(1)- n<T>(1,2)*z[7];
    z[16]=z[16]*z[27];
    z[16]= - n<T>(11,4) + z[16];
    z[16]=z[16]*z[24];
    z[20]=static_cast<T>(7)- n<T>(11,4)*z[6];
    z[20]=z[6]*z[20];
    z[15]=z[15] + z[16] + n<T>(3,4)*z[20] + z[30];
    z[15]=z[1]*z[15];
    z[16]=z[9]*z[25];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[16]*z[28];
    z[20]=static_cast<T>(1)+ n<T>(1,4)*z[23];
    z[20]=z[7]*z[20];
    z[16]=z[16] - n<T>(1,4) + z[20];
    z[16]=z[16]*z[27];
    z[20]=n<T>(3,2) + z[23];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[16]=z[4]*z[16];
    z[20]=z[11] - z[31];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[16]= - static_cast<T>(3)- z[8];
    z[16]=z[16]*z[29];
    z[20]= - static_cast<T>(1)- z[4];
    z[17]=z[20]*z[17];
    z[20]=z[5] - 1;
    z[20]=z[20]*z[5];
    z[23]=n<T>(17,8) - z[20];
    z[17]=z[17] + 3*z[23] + n<T>(17,8)*z[8];
    z[17]=z[1]*z[17];
    z[16]=n<T>(1,8)*z[17] + z[16] + n<T>(23,12) + z[22];
    z[16]=z[1]*z[16];
    z[17]= - static_cast<T>(1)+ z[8];
    z[17]=z[17]*z[36]*z[21];
    z[21]=n<T>(23,6) - z[8];
    z[21]=z[8]*z[21];
    z[18]=n<T>(17,4)*z[8] + n<T>(7,6) - z[18];
    z[18]=z[1]*z[18];
    z[18]=z[18] - n<T>(17,6) + z[21];
    z[18]=z[1]*z[18];
    z[17]=z[18] + z[17];
    z[17]=z[2]*z[17];
    z[18]= - n<T>(41,6) + z[34];
    z[16]=n<T>(1,4)*z[17] + n<T>(1,16)*z[18] + z[16];
    z[16]=z[2]*z[16];
    z[17]=n<T>(13,2) - 9*z[4];
    z[17]=z[17]*z[33];
    z[17]=z[17] + n<T>(17,4) + z[26];
    z[17]=z[17]*z[19];
    z[17]=z[17] - z[35] - z[8] + n<T>(21,8) - z[20];
    z[17]=z[1]*z[17];
    z[13]=z[17] - n<T>(1,8)*z[4] - n<T>(5,8) + z[13];
    z[13]=n<T>(1,4)*z[13] + z[16];
    z[13]=z[2]*z[13];
    z[13]=z[14] + n<T>(1,8)*z[15] + z[13];
    z[13]=z[3]*z[13];
    z[12]=n<T>(1,2)*z[12] + z[13];

    r += n<T>(1,2)*z[12];
 
    return r;
}

template double qqb_2lha_r173(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r173(const std::array<dd_real,30>&);
#endif
