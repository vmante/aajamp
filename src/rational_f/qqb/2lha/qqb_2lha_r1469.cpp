#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1469(const std::array<T,30>& k) {
  T z[75];
  T r = 0;

    z[1]=k[1];
    z[2]=k[4];
    z[3]=k[12];
    z[4]=k[17];
    z[5]=k[6];
    z[6]=k[7];
    z[7]=k[13];
    z[8]=k[20];
    z[9]=k[8];
    z[10]=k[3];
    z[11]=k[2];
    z[12]=k[10];
    z[13]=k[5];
    z[14]=k[11];
    z[15]=k[9];
    z[16]=z[3] - 1;
    z[17]=n<T>(1,2)*z[3];
    z[18]=z[16]*z[17];
    z[19]=z[17] - 1;
    z[20]=z[19]*z[3];
    z[21]=z[20] + n<T>(1,2);
    z[22]=3*z[5];
    z[23]=z[21]*z[22];
    z[24]=z[16]*z[3];
    z[25]=z[24] - z[23];
    z[25]=z[5]*z[25];
    z[25]=z[18] + z[25];
    z[25]=z[5]*z[25];
    z[26]=z[5]*z[3];
    z[27]=z[26]*z[16];
    z[28]=3*z[27];
    z[29]= - static_cast<T>(3)+ 5*z[3];
    z[29]=z[29]*z[17];
    z[29]=z[29] - z[28];
    z[30]=npow(z[5],2);
    z[29]=z[29]*z[30];
    z[31]=npow(z[3],2);
    z[32]=z[31]*z[5];
    z[33]=z[32] - z[31];
    z[34]=z[33]*z[30];
    z[35]=z[34]*z[1];
    z[29]=z[29] - z[35];
    z[36]=n<T>(1,2)*z[1];
    z[29]=z[29]*z[36];
    z[37]=n<T>(1,4)*z[31];
    z[25]=z[29] - z[37] + z[25];
    z[25]=z[1]*z[25];
    z[29]=z[1]*z[5];
    z[38]=z[33]*z[29];
    z[28]=z[24] - z[28];
    z[28]=z[5]*z[28];
    z[28]= - z[38] + z[31] + z[28];
    z[28]=z[28]*z[36];
    z[39]= - z[23] - z[21];
    z[39]=z[5]*z[39];
    z[28]=z[28] + z[18] + z[39];
    z[28]=z[1]*z[28];
    z[39]=z[3] - 3;
    z[40]=z[39]*z[17];
    z[40]=z[40] + 1;
    z[41]=z[40]*z[5];
    z[42]=z[5] + 1;
    z[43]=n<T>(1,2)*z[2];
    z[44]=z[42]*z[43];
    z[45]=z[39]*z[3];
    z[46]= - static_cast<T>(3)- z[45];
    z[44]=z[44] + n<T>(1,2)*z[46] - z[41];
    z[44]=z[5]*z[44];
    z[28]=z[28] - n<T>(1,2) + z[44];
    z[28]=z[8]*z[28];
    z[44]= - static_cast<T>(1)+ z[18];
    z[44]=n<T>(1,2)*z[44] - z[41];
    z[44]=z[5]*z[44];
    z[44]=z[44] + n<T>(1,4) + z[20];
    z[44]=z[5]*z[44];
    z[46]=z[5] + n<T>(1,2);
    z[47]=z[46]*z[30]*z[43];
    z[25]=n<T>(1,2)*z[28] + z[47] + z[44] + z[25];
    z[25]=z[8]*z[25];
    z[28]= - z[31]*z[43];
    z[28]= - z[24] + z[28];
    z[28]=z[2]*z[28];
    z[28]=z[28] - z[21];
    z[28]=z[7]*z[28];
    z[44]=z[31]*z[2];
    z[28]=7*z[28] - 5*z[24] - n<T>(3,2)*z[44];
    z[28]=z[2]*z[28];
    z[28]= - 7*z[21] + z[28];
    z[28]=z[7]*z[28];
    z[47]=z[18]*z[2];
    z[47]=z[47] + z[21];
    z[47]=z[47]*z[4];
    z[48]=n<T>(1,4)*z[3];
    z[49]= - static_cast<T>(3)- z[3];
    z[49]=z[49]*z[48];
    z[50]=n<T>(1,4)*z[9] + 1;
    z[51]=z[31]*z[10];
    z[52]=z[51] - z[3];
    z[50]=z[9]*z[52]*z[50];
    z[52]=z[37]*z[2];
    z[28]=n<T>(1,8)*z[28] + n<T>(7,4)*z[50] + z[47] - z[52] + z[49] + z[51];
    z[28]=z[15]*z[28];
    z[23]=z[18] - z[23];
    z[23]=z[23]*z[22];
    z[49]=z[48] + 1;
    z[50]=z[49]*z[17];
    z[23]=z[23] - static_cast<T>(1)+ z[50];
    z[50]=n<T>(1,2)*z[5];
    z[23]=z[23]*z[50];
    z[53]= - n<T>(3,4) + z[3];
    z[53]=z[3]*z[53];
    z[53]=z[53] - n<T>(3,2)*z[27];
    z[53]=z[5]*z[53];
    z[53]=z[37] + z[53];
    z[53]=z[5]*z[53];
    z[34]= - z[36]*z[34];
    z[34]=z[53] + z[34];
    z[34]=z[1]*z[34];
    z[53]= - static_cast<T>(1)+ z[31];
    z[23]=n<T>(3,2)*z[34] + n<T>(1,3)*z[53] + z[23];
    z[23]=z[1]*z[23];
    z[34]=static_cast<T>(1)- n<T>(7,4)*z[3];
    z[34]=z[3]*z[34];
    z[34]=z[34] + n<T>(9,2)*z[27];
    z[34]=z[5]*z[34];
    z[34]=n<T>(3,2)*z[38] - n<T>(17,12)*z[31] + z[34];
    z[34]=z[1]*z[34];
    z[53]=n<T>(1,6)*z[3];
    z[54]=n<T>(65,4) - 17*z[3];
    z[54]=z[54]*z[53];
    z[55]=z[21]*z[5];
    z[56]= - n<T>(7,2) + z[3];
    z[56]=z[3]*z[56];
    z[56]=9*z[55] + n<T>(5,2) + z[56];
    z[56]=z[5]*z[56];
    z[34]=z[34] + z[54] + z[56];
    z[34]=z[1]*z[34];
    z[22]=z[40]*z[22];
    z[40]= - static_cast<T>(9)+ n<T>(5,2)*z[3];
    z[40]=z[3]*z[40];
    z[40]=n<T>(19,2) + z[40];
    z[22]=n<T>(1,2)*z[40] + z[22];
    z[22]=z[5]*z[22];
    z[40]=3*z[3];
    z[54]=n<T>(11,2) - z[40];
    z[54]=z[3]*z[54];
    z[54]=n<T>(11,2) + z[54];
    z[22]=z[34] + n<T>(1,4)*z[54] + z[22];
    z[34]=z[33]*z[1];
    z[54]= - n<T>(2,3) + z[3];
    z[54]=z[3]*z[54];
    z[34]= - n<T>(1,3)*z[34] + z[54] - z[27];
    z[34]=z[1]*z[34];
    z[54]=z[3] - 2;
    z[54]=z[54]*z[3];
    z[56]=z[54] + 1;
    z[57]=z[56]*z[5];
    z[58]=z[3] - n<T>(3,2);
    z[59]=z[3]*z[58];
    z[34]=z[34] - z[57] + n<T>(1,2) + z[59];
    z[34]=z[1]*z[34];
    z[59]=n<T>(1,3)*z[3];
    z[60]=z[59] - 1;
    z[61]=z[60]*z[3];
    z[61]=z[61] + n<T>(2,3);
    z[61]=z[61]*z[5];
    z[62]=z[3] - n<T>(5,2);
    z[62]=z[62]*z[3];
    z[63]=n<T>(1,2) + z[62];
    z[34]=z[34] + n<T>(1,3)*z[63] - z[61];
    z[34]=z[6]*z[34];
    z[22]=n<T>(1,4)*z[22] + z[34];
    z[22]=z[6]*z[22];
    z[34]=n<T>(1,8)*z[3];
    z[63]= - static_cast<T>(49)+ 41*z[3];
    z[63]=z[63]*z[34];
    z[63]= - static_cast<T>(5)+ z[63];
    z[39]=n<T>(1,4)*z[39] - z[41];
    z[41]=n<T>(3,2)*z[5];
    z[39]=z[39]*z[41];
    z[39]=z[39] - static_cast<T>(1)- n<T>(5,16)*z[45];
    z[39]=z[5]*z[39];
    z[22]=z[22] + z[23] + n<T>(1,12)*z[63] + z[39];
    z[22]=z[6]*z[22];
    z[23]=static_cast<T>(3)+ n<T>(37,4)*z[3];
    z[23]=z[23]*z[34];
    z[39]=static_cast<T>(643)- 283*z[3];
    z[39]=z[39]*z[59];
    z[39]= - static_cast<T>(115)+ z[39];
    z[39]=n<T>(1,16)*z[39] + 2*z[57];
    z[39]=z[5]*z[39];
    z[23]=z[39] - static_cast<T>(3)+ z[23];
    z[23]=z[5]*z[23];
    z[23]= - n<T>(1,8)*z[24] + z[23];
    z[23]=z[5]*z[23];
    z[39]= - static_cast<T>(1)- n<T>(17,12)*z[3];
    z[39]=z[39]*z[48];
    z[39]=z[39] + z[27];
    z[39]=z[5]*z[39];
    z[63]=n<T>(1,16)*z[3];
    z[64]= - static_cast<T>(1)- z[3];
    z[64]=z[64]*z[63];
    z[39]=z[64] + z[39];
    z[39]=z[5]*z[39];
    z[64]=n<T>(1,16)*z[31];
    z[39]= - z[64] + z[39];
    z[39]=z[39]*z[29];
    z[23]=z[23] + z[39];
    z[23]=z[1]*z[23];
    z[39]= - static_cast<T>(5)+ 13*z[3];
    z[39]=z[39]*z[17];
    z[39]=static_cast<T>(47)+ z[39];
    z[65]=z[45] + 2;
    z[66]=z[65]*z[5];
    z[67]= - static_cast<T>(299)+ n<T>(131,2)*z[3];
    z[67]=z[3]*z[67];
    z[67]=n<T>(371,2) + z[67];
    z[67]=n<T>(1,48)*z[67] - z[66];
    z[67]=z[5]*z[67];
    z[39]=n<T>(1,16)*z[39] + z[67];
    z[39]=z[5]*z[39];
    z[67]= - static_cast<T>(1)+ n<T>(3,2)*z[3];
    z[68]=z[67]*z[63];
    z[39]=z[68] + z[39];
    z[39]=z[5]*z[39];
    z[68]=n<T>(3,2) + z[59];
    z[68]=z[3]*z[68];
    z[68]= - n<T>(11,6) + z[68];
    z[68]=n<T>(1,4)*z[68] - z[57];
    z[68]=z[5]*z[68];
    z[68]= - n<T>(1,16)*z[20] + z[68];
    z[68]=z[5]*z[68];
    z[68]=n<T>(1,32)*z[31] + z[68];
    z[68]=z[68]*z[29];
    z[69]=n<T>(19,6) + z[20];
    z[69]=n<T>(1,8)*z[69] + z[61];
    z[69]=z[5]*z[69];
    z[69]=n<T>(1,32)*z[24] + z[69];
    z[69]=z[10]*z[69]*z[30];
    z[39]=z[69] + z[39] + z[68];
    z[39]=z[10]*z[39];
    z[68]= - static_cast<T>(965)+ 107*z[3];
    z[68]=z[68]*z[53];
    z[68]= - static_cast<T>(95)+ z[68];
    z[69]=static_cast<T>(1619)- 427*z[3];
    z[69]=z[69]*z[34];
    z[69]= - static_cast<T>(137)+ z[69];
    z[66]=n<T>(1,12)*z[69] + z[66];
    z[66]=z[5]*z[66];
    z[66]=n<T>(1,32)*z[68] + z[66];
    z[66]=z[5]*z[66];
    z[68]= - static_cast<T>(1)+ n<T>(11,2)*z[3];
    z[68]=z[3]*z[68];
    z[68]=static_cast<T>(31)+ z[68];
    z[66]=n<T>(1,16)*z[68] + z[66];
    z[66]=z[5]*z[66];
    z[23]=z[39] + z[66] + z[23];
    z[23]=z[10]*z[23];
    z[39]=z[31]*z[1];
    z[66]= - static_cast<T>(1)- n<T>(3,4)*z[5];
    z[66]=z[5]*z[66];
    z[66]=n<T>(3,16)*z[39] + n<T>(17,24)*z[24] + z[66];
    z[68]=z[1]*z[20];
    z[42]=z[68] + z[62] + z[42];
    z[42]=z[6]*z[42];
    z[42]=n<T>(1,2)*z[66] + n<T>(1,3)*z[42];
    z[42]=z[6]*z[42];
    z[41]=z[46]*z[41];
    z[41]=static_cast<T>(1)+ z[41];
    z[41]=z[5]*z[41];
    z[46]=n<T>(5,8)*z[31];
    z[41]=z[46] + z[41];
    z[41]=n<T>(1,2)*z[41] + z[42];
    z[41]=z[6]*z[41];
    z[42]=static_cast<T>(7)- z[5];
    z[42]=z[5]*z[42];
    z[42]= - n<T>(31,16) + z[42];
    z[42]=z[42]*z[30];
    z[62]=npow(z[5],3);
    z[66]= - n<T>(23,8) + z[5];
    z[66]=z[66]*z[62];
    z[68]=z[10]*npow(z[5],4);
    z[66]=z[66] - n<T>(1,3)*z[68];
    z[66]=z[10]*z[66];
    z[42]=z[42] + z[66];
    z[42]=z[10]*z[42];
    z[66]=npow(z[6],3);
    z[68]=z[66]*z[44];
    z[68]=n<T>(1,3)*z[68];
    z[69]=z[24] + n<T>(1,3)*z[39];
    z[69]=z[6]*z[69];
    z[69]=n<T>(3,16)*z[31] + z[69];
    z[70]=npow(z[6],2);
    z[69]=z[69]*z[70];
    z[69]=z[69] + z[68];
    z[69]=z[69]*z[43];
    z[71]=n<T>(1,3)*z[5];
    z[72]= - n<T>(43,8) + z[71];
    z[72]=z[5]*z[72];
    z[72]=n<T>(31,16) + z[72];
    z[72]=z[5]*z[72];
    z[72]= - n<T>(1,3) + z[72];
    z[72]=z[5]*z[72];
    z[41]=z[69] + z[42] + z[72] + z[41];
    z[41]=z[2]*z[41];
    z[19]= - z[19]*z[17];
    z[42]=z[36]*z[31];
    z[69]=z[24] + z[42];
    z[69]=z[1]*z[69];
    z[67]= - z[3]*z[67];
    z[67]=z[67] - z[42];
    z[72]=n<T>(1,2)*z[10];
    z[67]=z[67]*z[72];
    z[19]=z[67] + z[19] + z[69];
    z[19]=z[10]*z[19];
    z[67]=npow(z[1],2);
    z[45]= - z[67]*z[45];
    z[45]= - z[18] + z[45];
    z[69]=z[1]*z[3];
    z[73]=z[58]*z[69];
    z[74]= - z[10]*z[18];
    z[73]=z[73] + z[74];
    z[73]=z[10]*z[73];
    z[45]=n<T>(1,2)*z[45] + z[73];
    z[45]=z[10]*z[45];
    z[73]= - z[3]*z[67];
    z[73]= - z[17] + z[73];
    z[73]=z[1]*z[73];
    z[73]=n<T>(9,2)*z[3] + z[73];
    z[45]=n<T>(1,2)*z[73] + z[45];
    z[45]=z[9]*z[45];
    z[42]= - z[20] - z[42];
    z[42]=z[1]*z[42];
    z[49]= - z[3]*z[49];
    z[42]=z[49] + z[42];
    z[42]=z[42]*z[36];
    z[49]=n<T>(7,2) + z[3];
    z[49]=z[3]*z[49];
    z[19]=n<T>(1,2)*z[45] + z[19] + z[49] + z[42];
    z[42]=n<T>(1,8)*z[9];
    z[19]=z[19]*z[42];
    z[45]=z[60]*z[17];
    z[45]=z[45] + n<T>(1,3);
    z[45]=z[45]*z[6];
    z[49]=n<T>(41,6) + 19*z[20];
    z[49]=n<T>(1,16)*z[49] + z[45];
    z[49]=z[6]*z[49];
    z[18]=z[18] + z[49];
    z[18]=z[6]*z[18];
    z[49]=z[6]*z[24];
    z[49]=n<T>(25,48)*z[31] + z[49];
    z[49]=z[49]*z[70];
    z[49]=z[49] + z[68];
    z[43]=z[49]*z[43];
    z[20]=n<T>(1,3) + z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(11,16)*z[24] + z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(13,32)*z[31] + z[20];
    z[20]=z[6]*z[20];
    z[20]=z[20] + z[43];
    z[20]=z[2]*z[20];
    z[18]=z[20] + n<T>(67,96)*z[31] + z[18];
    z[18]=z[2]*z[18];
    z[20]= - static_cast<T>(5)+ 7*z[3];
    z[20]=z[20]*z[63];
    z[43]=n<T>(3,16)*z[21] + z[45];
    z[43]=z[6]*z[43];
    z[45]=z[9]*z[10];
    z[45]=z[45] + 1;
    z[45]=z[37]*z[45];
    z[45]=z[51] + z[45];
    z[42]=z[45]*z[42];
    z[18]=z[42] + z[18] + n<T>(37,96)*z[51] + z[20] + z[43];
    z[18]=z[12]*z[18];
    z[20]=static_cast<T>(257)- n<T>(989,6)*z[3];
    z[20]=z[20]*z[34];
    z[20]= - static_cast<T>(9)+ z[20];
    z[42]= - n<T>(1021,3) + n<T>(309,2)*z[3];
    z[42]=z[3]*z[42];
    z[42]=n<T>(547,3) + z[42];
    z[42]=n<T>(1,16)*z[42] - z[57];
    z[42]=z[5]*z[42];
    z[20]=n<T>(1,4)*z[20] + z[42];
    z[20]=z[5]*z[20];
    z[42]= - static_cast<T>(31)- n<T>(1,12)*z[31];
    z[20]=n<T>(1,16)*z[42] + z[20];
    z[20]=z[5]*z[20];
    z[42]= - static_cast<T>(3)- n<T>(103,2)*z[3];
    z[42]=z[42]*z[63];
    z[43]= - n<T>(20,3) + n<T>(189,32)*z[3];
    z[43]=z[3]*z[43];
    z[27]=z[43] - z[27];
    z[27]=z[5]*z[27];
    z[27]=z[42] + z[27];
    z[27]=z[5]*z[27];
    z[42]= - static_cast<T>(1)+ n<T>(3,4)*z[3];
    z[43]=z[42]*z[34];
    z[27]=z[43] + z[27];
    z[27]=z[5]*z[27];
    z[43]=z[46] - z[32];
    z[43]=z[43]*z[71];
    z[43]=z[64] + z[43];
    z[43]=z[5]*z[43];
    z[43]=z[64] + z[43];
    z[29]=z[43]*z[29];
    z[27]=z[27] + z[29];
    z[27]=z[1]*z[27];
    z[20]=z[20] + z[27];
    z[20]=z[1]*z[20];
    z[27]=z[39] + z[24];
    z[27]=z[27]*z[2];
    z[27]=z[27] + static_cast<T>(1)+ 2*z[54];
    z[29]=static_cast<T>(4)- z[40];
    z[29]=z[3]*z[29];
    z[29]=z[29] - 3*z[39];
    z[29]=z[1]*z[29];
    z[29]=2*z[29] + 3*z[27];
    z[29]=z[2]*z[29];
    z[43]= - z[31]*z[67];
    z[27]=z[43] + z[27];
    z[27]=z[2]*z[27];
    z[43]=z[69] - z[56];
    z[43]=z[1]*z[43];
    z[27]=z[27] + z[43] + z[65];
    z[43]=3*z[4];
    z[27]=z[43]*z[1]*z[27];
    z[45]=static_cast<T>(1)+ z[40];
    z[45]=z[45]*z[69];
    z[46]=static_cast<T>(20)- 9*z[3];
    z[46]=z[3]*z[46];
    z[45]=z[45] - static_cast<T>(11)+ z[46];
    z[45]=z[1]*z[45];
    z[27]=z[27] + z[29] + 3*z[65] + z[45];
    z[27]=z[4]*z[27];
    z[29]=z[42]*z[26];
    z[29]=z[29] - z[37];
    z[33]=z[5]*z[33]*z[36];
    z[33]=z[33] - z[29];
    z[33]=z[1]*z[33];
    z[36]= - static_cast<T>(29)+ 25*z[3];
    z[36]=z[36]*z[48];
    z[33]=z[33] + static_cast<T>(2)+ z[36];
    z[33]=z[1]*z[33];
    z[36]=static_cast<T>(31)- 23*z[3];
    z[36]=z[3]*z[36];
    z[36]=z[36] - 11*z[39];
    z[36]=z[2]*z[36];
    z[37]=static_cast<T>(27)- n<T>(23,2)*z[3];
    z[37]=z[3]*z[37];
    z[37]= - n<T>(31,2) + z[37];
    z[27]=z[27] + n<T>(1,4)*z[36] + n<T>(1,2)*z[37] + z[33];
    z[27]=z[4]*z[27];
    z[17]=static_cast<T>(3)- z[17];
    z[17]=z[17]*z[26];
    z[33]=n<T>(1,2)*z[31];
    z[17]= - z[33] + z[17];
    z[17]=z[17]*z[50];
    z[17]=z[17] + z[35];
    z[17]=z[1]*z[17];
    z[17]=z[17] - z[29];
    z[17]=z[1]*z[17];
    z[29]= - n<T>(19,4) + z[40];
    z[29]=z[3]*z[29];
    z[17]=z[27] + z[52] + z[17] + n<T>(7,4) + z[29];
    z[17]=z[4]*z[17];
    z[27]=static_cast<T>(19)- 11*z[3];
    z[27]=z[27]*z[34];
    z[27]= - static_cast<T>(1)+ z[27];
    z[29]=z[3] - n<T>(25,16);
    z[29]=z[29]*z[3];
    z[29]=z[29] + n<T>(9,16);
    z[34]= - n<T>(7,8)*z[55] + z[29];
    z[34]=z[5]*z[34];
    z[36]= - static_cast<T>(1)+ n<T>(5,4)*z[3];
    z[36]=z[36]*z[3];
    z[36]=z[36] - n<T>(1,4);
    z[34]= - n<T>(3,8)*z[36] + z[34];
    z[34]=z[5]*z[34];
    z[27]=n<T>(1,4)*z[27] + z[34];
    z[21]=z[10]*z[62]*z[21];
    z[34]= - n<T>(21,8)*z[55] + z[29];
    z[34]=z[34]*z[30];
    z[34]=z[34] + n<T>(7,8)*z[21];
    z[34]=z[34]*z[72];
    z[29]=n<T>(21,16)*z[55] - z[29];
    z[29]=z[5]*z[29];
    z[29]=n<T>(3,16)*z[36] + z[29];
    z[29]=z[5]*z[29];
    z[29]=z[29] + z[34];
    z[29]=z[10]*z[29];
    z[27]=n<T>(1,4)*z[47] + n<T>(1,2)*z[27] + z[29];
    z[27]=z[13]*z[27];
    z[29]=n<T>(9,2) + z[3];
    z[29]=z[29]*z[26];
    z[29]=n<T>(9,2)*z[38] - z[31] + z[29];
    z[29]=z[1]*z[29];
    z[29]=n<T>(11,4)*z[3] + z[29];
    z[29]=z[1]*z[29];
    z[34]=z[44] + z[24] - z[39];
    z[34]=z[2]*z[34];
    z[29]=z[29] + n<T>(7,4)*z[34];
    z[29]=z[7]*z[29];
    z[26]=z[58]*z[26];
    z[26]= - z[33] + z[26];
    z[26]=z[5]*z[26];
    z[26]=z[26] - z[35];
    z[26]=z[1]*z[26];
    z[26]=z[32] + 9*z[26];
    z[26]=z[1]*z[26];
    z[24]=z[29] - z[44] + n<T>(7,4)*z[24] + z[26];
    z[24]=z[7]*z[24];
    z[26]=static_cast<T>(1361)- 365*z[3];
    z[26]=z[26]*z[53];
    z[26]=static_cast<T>(13)+ z[26];
    z[29]= - static_cast<T>(85)+ n<T>(401,16)*z[3];
    z[29]=z[29]*z[59];
    z[29]=n<T>(309,16) + z[29];
    z[29]=n<T>(1,2)*z[29] - z[61];
    z[29]=z[5]*z[29];
    z[26]=n<T>(1,32)*z[26] + z[29];
    z[26]=z[5]*z[26];
    z[29]=n<T>(11,32) - n<T>(2,3)*z[3];
    z[29]=z[3]*z[29];
    z[26]=z[26] - n<T>(91,48) + z[29];
    z[26]=z[5]*z[26];
    z[29]= - static_cast<T>(5)+ z[3];
    z[29]=z[29]*z[48];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[29]*z[30];
    z[21]=z[29] + n<T>(3,2)*z[21];
    z[21]=z[10]*z[21];
    z[29]= - static_cast<T>(1)+ z[48];
    z[29]=z[3]*z[29];
    z[29]=n<T>(3,4) + z[29];
    z[29]=z[29]*z[50];
    z[21]=z[29] + z[21];
    z[21]=z[10]*z[21];
    z[16]= - n<T>(1,8)*z[16] + z[21];
    z[16]=z[14]*z[16];
    z[21]=z[2]*z[66];
    z[21]=z[70] + z[21];
    z[21]=z[12]*z[21];
    z[21]=z[66] + z[21];
    z[29]=z[43] - 2;
    z[29]=z[1]*z[29];
    z[29]=static_cast<T>(3)+ z[29];
    z[29]=z[4]*z[29];
    z[29]= - static_cast<T>(2)+ z[29];
    z[29]=z[29]*npow(z[4],2);
    z[21]=z[29] + n<T>(1,6)*z[21];
    z[21]=z[11]*z[21];
    z[29]=static_cast<T>(1)+ n<T>(29,16)*z[31];

    r += n<T>(1,8)*z[16] + z[17] + z[18] + z[19] + z[20] + z[21] + z[22] + 
      z[23] + n<T>(1,16)*z[24] + z[25] + z[26] + z[27] + n<T>(1,4)*z[28] + n<T>(1,3)
      *z[29] + z[41];
 
    return r;
}

template double qqb_2lha_r1469(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1469(const std::array<dd_real,30>&);
#endif
