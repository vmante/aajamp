#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1271(const std::array<T,30>& k) {
  T z[10];
  T r = 0;

    z[1]=k[3];
    z[2]=k[5];
    z[3]=k[6];
    z[4]=k[12];
    z[5]=k[11];
    z[6]=z[1] - 1;
    z[6]=z[6]*z[2];
    z[7]=n<T>(1,2)*z[1];
    z[8]= - z[5]*z[7];
    z[8]=n<T>(3,2)*z[6] - static_cast<T>(1)+ z[8];
    z[8]=z[4]*z[8];
    z[9]=z[1]*z[5];
    z[6]=z[8] + z[9] - z[6];
    z[8]=n<T>(1,2)*z[5];
    z[9]=npow(z[1],2);
    z[8]=z[9]*z[8];
    z[7]=z[7] - 1;
    z[7]=z[7]*z[1];
    z[7]=z[7] + n<T>(1,2);
    z[7]=z[7]*z[2];
    z[9]= - z[5]*z[9];
    z[9]=static_cast<T>(1)+ z[9];
    z[9]=n<T>(1,2)*z[9] + z[7];
    z[9]=z[4]*z[9];
    z[7]=z[9] + z[8] - z[7];
    z[7]=z[3]*z[7];
    z[6]=n<T>(1,2)*z[6] + z[7];
    z[6]=z[3]*z[6];
    z[7]=z[4] - 1;
    z[7]=z[2]*z[7];
    z[7]=z[5] + z[7];
    z[6]=n<T>(1,4)*z[7] + z[6];

    r += n<T>(1,4)*z[6]*z[4]*z[3];
 
    return r;
}

template double qqb_2lha_r1271(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1271(const std::array<dd_real,30>&);
#endif
