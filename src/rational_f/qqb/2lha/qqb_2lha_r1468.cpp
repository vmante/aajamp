#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1468(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[12];
    z[5]=k[4];
    z[6]=k[8];
    z[7]=k[3];
    z[8]=k[2];
    z[9]=k[10];
    z[10]=k[9];
    z[11]=n<T>(1,2)*z[6];
    z[12]=n<T>(1,2)*z[10];
    z[13]= - static_cast<T>(1)- z[12];
    z[13]=z[13]*z[11];
    z[14]=z[10] - 1;
    z[15]=static_cast<T>(1)+ z[6];
    z[15]=z[7]*z[15];
    z[13]=z[15] + z[13] - z[14];
    z[13]=z[6]*z[13];
    z[15]=n<T>(3,4)*z[10];
    z[16]=n<T>(1,4)*z[6];
    z[17]=z[16] + 1;
    z[18]= - z[6]*z[17];
    z[18]= - n<T>(3,4) + z[18];
    z[18]=z[9]*z[18];
    z[13]=z[18] - z[15] + z[13];
    z[13]=z[7]*z[13];
    z[18]= - z[1] - n<T>(3,2);
    z[18]=z[6]*z[18];
    z[19]=npow(z[6],2);
    z[20]=z[19]*z[7];
    z[18]= - z[20] + z[18];
    z[18]=z[1]*z[18];
    z[21]=z[5]*z[9];
    z[17]= - z[9]*z[17];
    z[13]=z[18] - n<T>(3,4)*z[21] + z[13] + z[17] - n<T>(3,2) - z[6];
    z[17]=n<T>(1,2)*z[1];
    z[18]=z[17] + 1;
    z[22]=z[18] - z[7];
    z[22]=z[22]*z[1];
    z[23]=n<T>(1,2)*z[7];
    z[24]=z[23] - 1;
    z[25]=z[24]*z[7];
    z[25]=z[25] + n<T>(1,2);
    z[22]=z[22] + z[25];
    z[26]=n<T>(1,2)*z[2];
    z[22]=z[22]*z[26];
    z[27]=n<T>(1,8)*z[7];
    z[28]= - static_cast<T>(1)- z[7];
    z[28]=z[28]*z[27];
    z[29]=n<T>(1,8)*z[1];
    z[30]=static_cast<T>(3)+ z[1];
    z[30]=z[30]*z[29];
    z[28]= - z[22] + z[30] + n<T>(1,3) + z[28];
    z[28]=z[2]*z[28];
    z[30]=n<T>(1,3)*z[9];
    z[31]=n<T>(1,3)*z[21];
    z[32]=z[31] + z[9];
    z[33]=n<T>(1,3) + z[32];
    z[33]=z[5]*z[33];
    z[33]=z[33] + static_cast<T>(1)+ z[9];
    z[33]=z[5]*z[33];
    z[33]=z[33] + static_cast<T>(1)+ z[30];
    z[33]=z[5]*z[33];
    z[34]=static_cast<T>(1)+ n<T>(1,3)*z[5];
    z[35]=n<T>(1,3)*z[1];
    z[36]=z[35] + z[34];
    z[36]=z[1]*z[36];
    z[37]=z[5]*z[34];
    z[36]=z[36] + static_cast<T>(1)+ z[37];
    z[36]=z[1]*z[36];
    z[37]= - static_cast<T>(1)- z[35];
    z[37]=z[1]*z[37];
    z[37]= - static_cast<T>(1)+ z[37];
    z[37]=z[1]*z[37];
    z[37]= - n<T>(1,3) + z[37];
    z[37]=z[2]*z[37];
    z[33]=z[37] + z[36] + n<T>(1,3) + z[33];
    z[36]=n<T>(1,2)*z[3];
    z[33]=z[33]*z[36];
    z[32]=z[5]*z[32];
    z[32]=z[9] + z[32];
    z[32]=z[5]*z[32];
    z[32]=z[30] + z[32];
    z[34]= - n<T>(1,2)*z[34] - z[35];
    z[34]=z[1]*z[34];
    z[37]=z[18]*z[1];
    z[37]=z[37] + n<T>(1,2);
    z[37]=z[37]*z[2];
    z[32]=z[33] + z[37] + n<T>(1,2)*z[32] + z[34];
    z[32]=z[3]*z[32];
    z[33]= - static_cast<T>(1)- z[1];
    z[33]=z[2]*z[33];
    z[33]=z[35] + z[33];
    z[32]=n<T>(1,2)*z[33] + z[32];
    z[32]=z[32]*z[36];
    z[13]=z[32] + n<T>(1,8)*z[13] + z[28];
    z[13]=z[4]*z[13];
    z[28]=n<T>(1,2)*z[9];
    z[32]=z[9] + n<T>(1,2)*z[21];
    z[33]= - n<T>(1,2) - z[32];
    z[33]=z[5]*z[33];
    z[33]=z[33] - static_cast<T>(1)- z[28];
    z[33]=z[5]*z[33];
    z[34]=n<T>(1,2)*z[5];
    z[36]= - z[34] - z[18];
    z[36]=z[1]*z[36];
    z[33]=z[37] + z[36] - n<T>(1,2) + z[33];
    z[33]=z[3]*z[33];
    z[36]= - static_cast<T>(3)- z[17];
    z[36]=z[1]*z[36];
    z[36]= - n<T>(5,2) + z[36];
    z[36]=z[36]*z[26];
    z[32]= - z[5]*z[32];
    z[28]=z[33] + z[36] + z[17] - z[28] + z[32];
    z[28]=z[3]*z[28];
    z[32]=z[2]*z[18];
    z[28]=z[32] + z[28];
    z[28]=z[3]*z[28];
    z[32]= - z[19]*z[17];
    z[16]=z[16] - 1;
    z[16]=z[16]*z[6];
    z[20]=z[32] - z[16] + z[20];
    z[20]=z[1]*z[20];
    z[12]= - static_cast<T>(1)+ z[12];
    z[11]=z[12]*z[11];
    z[11]=z[11] + z[14];
    z[11]=z[6]*z[11];
    z[11]=z[15] + z[11];
    z[12]= - z[19]*z[23];
    z[12]=z[16] + z[12];
    z[12]=z[7]*z[12];
    z[11]=z[20] + n<T>(1,2)*z[11] + z[12];
    z[12]= - static_cast<T>(3)+ z[7];
    z[12]=z[12]*z[23];
    z[14]= - z[1]*z[24];
    z[12]= - z[22] + z[14] + static_cast<T>(1)+ z[12];
    z[12]=z[2]*z[12];
    z[14]= - z[1] - n<T>(9,4) + z[7];
    z[12]=n<T>(1,2)*z[14] + z[12];
    z[12]=z[2]*z[12];
    z[11]=z[28] + n<T>(1,2)*z[11] + z[12];
    z[11]=n<T>(1,2)*z[11] + z[13];
    z[11]=z[4]*z[11];
    z[12]=static_cast<T>(1)+ n<T>(1,2)*z[8];
    z[13]=z[9]*z[12];
    z[13]=z[21] + static_cast<T>(1)+ z[13];
    z[13]=z[5]*z[13];
    z[14]=static_cast<T>(1)+ z[8];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[14]= - static_cast<T>(1)+ z[34];
    z[14]=n<T>(1,3)*z[14] - z[17];
    z[14]=z[2]*z[14];
    z[13]=z[14] + n<T>(1,3)*z[13] + z[17];
    z[13]=z[3]*z[13];
    z[12]=z[12]*z[30];
    z[14]=static_cast<T>(3)- z[5];
    z[14]=n<T>(1,2)*z[14] + z[1];
    z[14]=z[14]*z[26];
    z[12]=z[13] + z[14] + z[31] + n<T>(1,4) + z[12];
    z[12]=z[3]*z[12];
    z[13]=z[34] - z[18];
    z[13]=z[2]*z[13];
    z[13]=z[13] - n<T>(1,2) - z[35];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[3]*z[12];
    z[13]=z[7] - 1;
    z[14]= - z[13]*z[34];
    z[15]=npow(z[7],2);
    z[16]= - n<T>(1,2) - z[7];
    z[16]=z[1]*z[16];
    z[14]=z[16] + z[14] - static_cast<T>(1)+ z[15];
    z[15]= - z[5] + 1;
    z[15]=z[25]*z[15];
    z[13]= - z[1]*z[13];
    z[13]=z[13] + z[15];
    z[13]=z[2]*z[13];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[13]*z[26];
    z[13]=z[13] - z[29] - n<T>(1,6)*z[5] + n<T>(1,3) + z[27];
    z[13]=z[2]*z[13];
    z[12]=z[12] + n<T>(1,6) + z[13];

    r += z[11] + n<T>(1,2)*z[12];
 
    return r;
}

template double qqb_2lha_r1468(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1468(const std::array<dd_real,30>&);
#endif
