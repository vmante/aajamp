#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r684(const std::array<T,30>& k) {
  T z[37];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[8];
    z[6]=k[4];
    z[7]=k[10];
    z[8]=k[5];
    z[9]=k[9];
    z[10]=k[3];
    z[11]=k[15];
    z[12]=k[13];
    z[13]=n<T>(1,2)*z[1];
    z[14]=3*z[4];
    z[15]=static_cast<T>(13)+ z[14];
    z[15]=z[15]*z[13];
    z[16]=n<T>(3,2)*z[4];
    z[15]=z[15] - n<T>(71,3) - z[16];
    z[17]=static_cast<T>(47)- 13*z[1];
    z[18]=z[2]*z[7];
    z[17]=n<T>(1,12)*z[17] + z[18];
    z[18]=n<T>(1,2)*z[2];
    z[17]=z[17]*z[18];
    z[19]=static_cast<T>(13)- n<T>(35,8)*z[1];
    z[17]=n<T>(1,3)*z[19] + z[17];
    z[17]=z[2]*z[17];
    z[19]=n<T>(1,4)*z[6];
    z[20]= - n<T>(21,2) - z[4];
    z[20]=n<T>(3,2)*z[20] - n<T>(11,3)*z[2];
    z[20]=z[20]*z[19];
    z[15]=z[20] + n<T>(1,4)*z[15] + z[17];
    z[15]=z[6]*z[15];
    z[17]=n<T>(1,3)*z[2];
    z[20]= - static_cast<T>(331)- 5*z[2];
    z[20]=z[20]*z[17];
    z[21]=static_cast<T>(35)+ z[4];
    z[20]=3*z[21] + z[20];
    z[21]=z[4] + z[12];
    z[22]=n<T>(13,3) + z[21];
    z[22]=n<T>(1,2)*z[22] + 11*z[11];
    z[22]=z[6]*z[22];
    z[16]=z[22] - n<T>(17,6)*z[2] + static_cast<T>(47)+ z[16];
    z[16]=z[6]*z[16];
    z[16]=n<T>(1,2)*z[20] + z[16];
    z[20]=n<T>(1,2)*z[6];
    z[16]=z[16]*z[20];
    z[22]=static_cast<T>(3)+ 5*z[7];
    z[22]=n<T>(1,2)*z[22] + z[5];
    z[22]=z[22]*z[18];
    z[23]=n<T>(3,2)*z[5];
    z[22]=z[22] + n<T>(31,3) - z[23];
    z[22]=z[2]*z[22];
    z[24]=3*z[5];
    z[25]= - n<T>(463,12) + z[24];
    z[22]=n<T>(1,2)*z[25] + z[22];
    z[22]=z[2]*z[22];
    z[25]=n<T>(109,3) + z[4];
    z[25]=n<T>(1,2)*z[25] - z[5];
    z[16]=z[16] + n<T>(1,2)*z[25] + z[22];
    z[22]=n<T>(71,2) - z[2];
    z[22]=z[22]*z[17];
    z[22]= - n<T>(91,8) + z[22];
    z[25]=z[6]*z[11];
    z[26]=n<T>(1,6)*z[25];
    z[27]= - static_cast<T>(11)- z[11];
    z[27]=z[27]*z[26];
    z[28]= - static_cast<T>(1)- n<T>(5,6)*z[11];
    z[27]=5*z[28] + z[27];
    z[27]=z[27]*z[20];
    z[27]=z[27] - n<T>(31,4) + 2*z[2];
    z[27]=z[6]*z[27];
    z[22]=n<T>(1,2)*z[22] + z[27];
    z[22]=z[6]*z[22];
    z[27]=n<T>(131,3) - z[24];
    z[28]=static_cast<T>(1)+ n<T>(1,4)*z[5];
    z[29]= - n<T>(1,4)*z[7] - z[28];
    z[29]=z[2]*z[29];
    z[29]=z[29] - n<T>(13,3) + n<T>(3,4)*z[5];
    z[29]=z[2]*z[29];
    z[27]=n<T>(1,4)*z[27] + z[29];
    z[27]=z[2]*z[27];
    z[29]=n<T>(1,2)*z[5];
    z[30]= - n<T>(35,3) + z[29];
    z[27]=n<T>(1,2)*z[30] + z[27];
    z[22]=n<T>(1,4)*z[27] + z[22];
    z[22]=z[9]*z[22];
    z[16]=n<T>(1,2)*z[16] + z[22];
    z[16]=z[9]*z[16];
    z[22]=n<T>(1,3)*z[6];
    z[27]=z[2] - 1;
    z[30]= - z[22] + z[27];
    z[30]=z[30]*z[20];
    z[31]=z[18] - 1;
    z[31]=z[31]*z[2];
    z[31]=z[31] + n<T>(1,2);
    z[30]=z[30] - z[31];
    z[30]=z[6]*z[30];
    z[17]=z[17] - 1;
    z[17]=z[17]*z[2];
    z[17]=z[17] + 1;
    z[17]=z[17]*z[2];
    z[17]=z[17] - n<T>(1,3);
    z[30]=n<T>(1,2)*z[17] + z[30];
    z[32]=n<T>(1,4)*z[25];
    z[33]=static_cast<T>(1)+ z[32];
    z[22]=z[33]*z[22];
    z[22]= - n<T>(1,2)*z[27] + z[22];
    z[22]=z[6]*z[22];
    z[33]= - static_cast<T>(2)+ z[2];
    z[33]=z[2]*z[33];
    z[33]=static_cast<T>(1)+ z[33];
    z[22]=n<T>(1,3)*z[33] + z[22];
    z[22]=z[6]*z[22];
    z[17]= - n<T>(1,4)*z[17] + z[22];
    z[17]=z[9]*z[17];
    z[17]=n<T>(25,2)*z[30] + 7*z[17];
    z[17]=z[9]*z[17];
    z[22]=z[1] - n<T>(11,4);
    z[22]=z[22]*z[1];
    z[30]=z[22] + n<T>(9,2);
    z[22]=n<T>(3,2) + n<T>(1,3)*z[22];
    z[33]= - z[2]*z[22];
    z[33]=z[33] + z[30];
    z[33]=z[2]*z[33];
    z[30]=z[33] - z[30];
    z[30]=z[2]*z[30];
    z[33]= - static_cast<T>(47)+ 19*z[1];
    z[31]= - z[33]*z[31];
    z[27]=z[6]*z[27];
    z[27]=n<T>(1,3)*z[31] - n<T>(11,2)*z[27];
    z[27]=z[27]*z[20];
    z[17]=z[17] + z[27] + z[30] + z[22];
    z[17]=z[3]*z[17];
    z[22]= - n<T>(5,6) - z[4];
    z[22]=z[22]*z[13];
    z[22]=z[22] - z[29] + n<T>(13,3) + n<T>(1,2)*z[4];
    z[22]=z[22]*z[13];
    z[27]=n<T>(1,4)*z[4];
    z[30]=n<T>(3,8)*z[5];
    z[22]=z[22] + z[30] - n<T>(13,3) - z[27];
    z[31]=n<T>(1,4)*z[1];
    z[33]= - n<T>(17,12)*z[1] + static_cast<T>(1)- z[23];
    z[33]=z[33]*z[31];
    z[34]=n<T>(13,3) + z[5];
    z[34]=z[1]*z[34];
    z[34]=z[34] - z[23] - n<T>(13,3) - n<T>(9,2)*z[7];
    z[35]=n<T>(1,8)*z[2];
    z[34]=z[34]*z[35];
    z[36]=n<T>(9,16)*z[5];
    z[33]=z[34] + z[33] - n<T>(4,3) + z[36];
    z[33]=z[2]*z[33];
    z[34]=n<T>(25,3) - n<T>(9,8)*z[5];
    z[30]=n<T>(7,12)*z[1] - static_cast<T>(2)+ z[30];
    z[30]=z[1]*z[30];
    z[30]=z[33] + n<T>(1,2)*z[34] + z[30];
    z[30]=z[2]*z[30];
    z[15]=z[17] + z[16] + z[15] + n<T>(1,2)*z[22] + z[30];
    z[15]=z[3]*z[15];
    z[16]=n<T>(1,4)*z[2];
    z[17]= - z[14] - n<T>(13,3) + n<T>(1,2)*z[12];
    z[17]= - z[16] + n<T>(1,4)*z[17] - 9*z[11];
    z[22]=n<T>(1,3)*z[11];
    z[30]= - n<T>(5,4) - z[11];
    z[30]=z[30]*z[22];
    z[21]= - n<T>(1,8)*z[21] + z[30];
    z[21]=z[6]*z[21];
    z[17]=n<T>(1,2)*z[17] + z[21];
    z[17]=z[6]*z[17];
    z[14]= - n<T>(179,3) - z[14];
    z[21]= - static_cast<T>(3)+ z[2];
    z[21]=z[2]*z[21];
    z[14]=n<T>(1,2)*z[14] + z[21];
    z[14]=n<T>(1,4)*z[14] + z[17];
    z[14]=z[6]*z[14];
    z[17]= - static_cast<T>(1)+ z[24];
    z[21]=z[2]*z[28];
    z[17]=n<T>(1,2)*z[17] - 3*z[21];
    z[17]=z[2]*z[17];
    z[21]=static_cast<T>(7)- z[23];
    z[17]=n<T>(1,2)*z[21] + z[17];
    z[21]=n<T>(1,2) + z[11];
    z[21]=z[21]*z[26];
    z[23]=n<T>(25,3) + z[11];
    z[23]=z[11]*z[23];
    z[21]=z[21] - n<T>(1,3) + n<T>(1,4)*z[23];
    z[21]=z[6]*z[21];
    z[21]=z[21] + n<T>(11,12)*z[2] + static_cast<T>(2)+ n<T>(5,2)*z[11];
    z[21]=z[6]*z[21];
    z[23]=n<T>(3,2) - z[2];
    z[23]=z[2]*z[23];
    z[23]=n<T>(61,8) + z[23];
    z[21]=n<T>(1,2)*z[23] + z[21];
    z[21]=z[6]*z[21];
    z[17]=n<T>(1,4)*z[17] + z[21];
    z[17]=z[9]*z[17];
    z[21]= - z[2] + n<T>(9,2)*z[5] + static_cast<T>(7)+ n<T>(1,2)*z[7];
    z[21]=z[2]*z[21];
    z[21]=z[21] + n<T>(101,6) - 9*z[5];
    z[21]=z[21]*z[35];
    z[14]=z[17] + z[14] + z[21] + z[36] - n<T>(8,3) - n<T>(1,8)*z[4];
    z[14]=z[9]*z[14];
    z[17]= - n<T>(9,2) - z[5];
    z[17]=z[1]*z[17];
    z[17]=z[17] + n<T>(61,6) + z[24];
    z[21]= - static_cast<T>(27)- z[7];
    z[23]=n<T>(17,3) + z[29];
    z[23]=z[1]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[23]= - n<T>(1,3) - z[29];
    z[23]=z[2]*z[23];
    z[21]=n<T>(1,2)*z[21] + z[23];
    z[21]=z[2]*z[21];
    z[17]=n<T>(1,2)*z[17] + z[21];
    z[17]=z[17]*z[18];
    z[21]=z[29] - n<T>(13,6) + z[4];
    z[21]=z[21]*z[31];
    z[17]=z[17] + z[21] + static_cast<T>(1)- z[29];
    z[21]= - static_cast<T>(3)- z[4];
    z[13]=z[21]*z[13];
    z[13]=n<T>(19,3) + z[13];
    z[21]=static_cast<T>(37)- n<T>(17,2)*z[1];
    z[23]=n<T>(1,3) + n<T>(7,8)*z[7];
    z[23]=z[2]*z[23];
    z[21]=n<T>(1,6)*z[21] + z[23];
    z[21]=z[2]*z[21];
    z[13]=n<T>(1,2)*z[13] + z[21];
    z[21]= - n<T>(1,12)*z[2] + n<T>(25,12)*z[11] + z[27] - n<T>(4,3) + n<T>(1,8)*z[12];
    z[21]=z[6]*z[21];
    z[13]=n<T>(1,2)*z[13] + z[21];
    z[13]=z[6]*z[13];
    z[13]=z[15] + z[14] + n<T>(1,2)*z[17] + z[13];
    z[13]=z[3]*z[13];
    z[14]=n<T>(1,2)*z[10];
    z[15]=static_cast<T>(5)- z[10];
    z[15]=z[11]*z[15];
    z[15]=n<T>(1,6)*z[15] - n<T>(1,3) + z[14];
    z[15]=z[11]*z[15];
    z[17]=z[22] - 1;
    z[21]=z[17]*z[25];
    z[23]=z[2]*z[8];
    z[25]= - static_cast<T>(3)- n<T>(1,4)*z[12];
    z[15]=z[21] + n<T>(1,4)*z[23] + n<T>(1,2)*z[25] + z[15];
    z[15]=z[6]*z[15];
    z[21]= - z[11]*z[14];
    z[21]=z[21] + n<T>(19,2) + z[10];
    z[21]=z[21]*z[22];
    z[25]=static_cast<T>(1)- z[23];
    z[25]=z[25]*z[18];
    z[26]= - n<T>(41,6) + z[10];
    z[15]=z[15] + z[25] + n<T>(1,2)*z[26] + z[21];
    z[15]=z[15]*z[20];
    z[20]= - static_cast<T>(13)- 5*z[10];
    z[21]= - n<T>(1,2) + n<T>(1,3)*z[10];
    z[21]=z[11]*z[21];
    z[20]=n<T>(1,6)*z[20] + z[21];
    z[20]=z[11]*z[20];
    z[21]=n<T>(19,3) - z[10];
    z[18]= - z[18] + n<T>(1,2)*z[21] + z[20];
    z[20]= - static_cast<T>(1)+ n<T>(1,4)*z[10];
    z[20]=z[20]*z[22];
    z[21]=static_cast<T>(1)- z[10];
    z[20]=n<T>(1,4)*z[21] + z[20];
    z[20]=z[11]*z[20];
    z[17]= - z[17]*z[32];
    z[17]=z[17] + n<T>(1,2) + z[20];
    z[17]=z[6]*z[17];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[6]*z[17];
    z[18]=z[11]*z[10];
    z[20]=z[18] - static_cast<T>(13)- z[10];
    z[20]=z[20]*z[22];
    z[21]=static_cast<T>(19)- 9*z[10];
    z[20]=n<T>(1,4)*z[21] + z[20];
    z[17]=z[17] + n<T>(1,4)*z[20] - z[2];
    z[17]=z[6]*z[17];
    z[20]=static_cast<T>(3)- z[10];
    z[20]=z[20]*z[29];
    z[20]=z[20] + n<T>(7,3) - 3*z[10];
    z[21]=z[10]*z[22];
    z[25]= - static_cast<T>(5)- z[5];
    z[25]=z[2]*z[25];
    z[20]=n<T>(3,4)*z[25] + n<T>(1,2)*z[20] + z[21];
    z[17]=n<T>(1,4)*z[20] + z[17];
    z[17]=z[9]*z[17];
    z[20]=3*z[8];
    z[21]=static_cast<T>(19)- z[20];
    z[25]=z[8]*npow(z[2],2);
    z[21]=z[25] + n<T>(1,2)*z[21] + z[24];
    z[21]=z[21]*z[35];
    z[24]= - static_cast<T>(3)+ z[14];
    z[24]=z[5]*z[24];
    z[15]=z[17] + z[15] + z[21] - n<T>(1,12)*z[18] + n<T>(1,8)*z[24] - n<T>(1,3) + 5.
   /16.*z[10];
    z[15]=z[9]*z[15];
    z[17]=n<T>(3,2) - z[22];
    z[17]=z[11]*z[17];
    z[18]=z[8]*z[19];
    z[17]=z[18] - n<T>(3,4)*z[23] - n<T>(17,24) + z[17];
    z[17]=z[6]*z[17];
    z[14]=z[14]*z[7];
    z[18]= - z[20] + n<T>(49,3) + z[12];
    z[18]= - z[14] + n<T>(1,2)*z[18] + z[4];
    z[19]=3*z[23] + n<T>(17,3) - z[7];
    z[16]=z[19]*z[16];
    z[16]=z[17] + z[16] + n<T>(1,4)*z[18] - z[11];
    z[16]=z[6]*z[16];
    z[17]= - n<T>(17,3) - z[5];
    z[17]=n<T>(1,2)*z[17] - z[23];
    z[17]=z[2]*z[17];
    z[18]= - z[7] - n<T>(13,3) + z[20];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[17]=z[2]*z[17];
    z[14]=z[17] + z[29] + n<T>(7,3) - z[14];
    z[14]=n<T>(1,4)*z[14] + z[16];

    r += z[13] + n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qqb_2lha_r684(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r684(const std::array<dd_real,30>&);
#endif
