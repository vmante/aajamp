#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1682(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[13];
    z[6]=k[2];
    z[7]=k[11];
    z[8]=k[24];
    z[9]=k[3];
    z[10]=k[4];
    z[11]=z[4] + 1;
    z[12]=z[11]*z[2];
    z[13]=npow(z[4],2);
    z[14]= - z[13] - z[12];
    z[15]=z[1]*z[2];
    z[14]=z[14]*z[15];
    z[16]=n<T>(1,2)*z[4];
    z[17]=z[16] + 1;
    z[17]=z[17]*z[4];
    z[18]=n<T>(1,2) + z[17];
    z[18]=z[8]*z[2]*z[18];
    z[19]=z[5]*z[13];
    z[18]=z[18] + z[19];
    z[18]=z[9]*z[18];
    z[17]=z[2] - n<T>(1,2) + z[17];
    z[17]=z[2]*z[17];
    z[19]=static_cast<T>(1)- z[3];
    z[19]=z[10]*z[19]*npow(z[2],2);
    z[14]=z[19] + z[18] + 3*z[14] - n<T>(1,2)*z[13] + z[17];
    z[17]= - z[11]*z[16];
    z[18]=z[6]*z[3];
    z[19]= - z[18] + 1;
    z[19]=z[13]*z[19];
    z[11]=z[3]*z[11]*z[4];
    z[19]=z[11] + z[19];
    z[20]=n<T>(1,4)*z[6];
    z[19]=z[20]*z[19];
    z[17]=z[19] + z[17];
    z[17]=z[8]*z[17];
    z[19]=n<T>(1,2)*z[2];
    z[21]=static_cast<T>(1)- z[2];
    z[19]=z[21]*z[19];
    z[21]=z[4]*z[12];
    z[21]=z[21] - z[13];
    z[22]=z[21]*z[15];
    z[12]= - z[4] + z[12];
    z[12]=z[2]*z[12];
    z[12]=z[12] - n<T>(1,2)*z[22];
    z[12]=z[1]*z[12];
    z[12]=z[19] + z[12];
    z[12]=z[3]*z[12];
    z[15]=n<T>(3,2) + z[15];
    z[15]=z[1]*z[21]*z[15];
    z[15]=z[16] + z[15];
    z[15]=z[5]*z[15];
    z[16]=z[18] + 1;
    z[13]=z[13]*z[16];
    z[11]= - z[11] + z[13];
    z[11]=z[11]*z[20];
    z[11]= - z[4] + z[11];
    z[11]=z[7]*z[11];
    z[11]=z[11] + z[15] + z[12] + z[17] + n<T>(1,2)*z[14];

    r += n<T>(1,2)*z[11];
 
    return r;
}

template double qqb_2lha_r1682(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1682(const std::array<dd_real,30>&);
#endif
