#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1618(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[12];
    z[8]=k[6];
    z[9]=k[11];
    z[10]=k[5];
    z[11]=n<T>(1,2)*z[6];
    z[12]=static_cast<T>(5)+ 31*z[6];
    z[12]=z[12]*z[11];
    z[12]=static_cast<T>(5)+ z[12];
    z[13]=z[6] + 1;
    z[14]=z[10]*z[6];
    z[15]=z[13]*z[14];
    z[16]=9*z[6];
    z[17]=n<T>(31,4) + z[16];
    z[17]=z[6]*z[17];
    z[15]= - n<T>(1,4)*z[15] + n<T>(5,4) + z[17];
    z[15]=z[10]*z[15];
    z[12]=n<T>(1,2)*z[12] + z[15];
    z[12]=z[10]*z[12];
    z[15]=3*z[6];
    z[17]=z[15] - n<T>(7,2);
    z[17]=z[17]*z[6];
    z[12]=z[12] + static_cast<T>(1)+ z[17];
    z[18]=z[15] - 1;
    z[19]= - n<T>(11,4) + z[15];
    z[19]=z[6]*z[19];
    z[19]=n<T>(1,4) + z[19];
    z[19]=z[10]*z[19];
    z[19]= - n<T>(5,2)*z[18] + z[19];
    z[19]=z[10]*z[19];
    z[19]=static_cast<T>(5)+ z[19];
    z[20]=n<T>(1,4)*z[4];
    z[20]=z[20]*npow(z[10],2);
    z[21]= - n<T>(3,8) + z[6];
    z[21]=z[10]*z[21];
    z[21]= - n<T>(5,4) + z[21];
    z[21]=z[10]*z[21];
    z[21]=z[21] + z[20];
    z[21]=z[4]*z[21];
    z[19]=n<T>(1,2)*z[19] + z[21];
    z[19]=z[4]*z[19];
    z[21]=n<T>(1,2)*z[10];
    z[22]= - n<T>(13,4) - z[16];
    z[22]=z[6]*z[22];
    z[22]= - n<T>(3,4) + z[22];
    z[22]=z[22]*z[21];
    z[17]=z[22] - n<T>(3,4) - z[17];
    z[17]=z[10]*z[17];
    z[22]=z[6] - n<T>(1,4);
    z[17]=z[19] + 5*z[22] + z[17];
    z[17]=z[4]*z[17];
    z[12]=n<T>(1,2)*z[12] + z[17];
    z[12]=z[4]*z[12];
    z[17]=n<T>(1,4)*z[10];
    z[19]=n<T>(3,2) - z[14];
    z[19]=z[19]*z[17];
    z[23]=n<T>(1,2)*z[2];
    z[24]= - static_cast<T>(1)- z[17];
    z[24]=z[24]*z[23];
    z[19]=z[24] + z[19] + static_cast<T>(1)+ n<T>(3,4)*z[6];
    z[19]=z[2]*z[19];
    z[24]=n<T>(3,2)*z[13];
    z[25]= - n<T>(5,2) + z[2];
    z[25]=z[2]*z[25];
    z[26]=z[6] - 1;
    z[27]=z[2] + z[26];
    z[27]=z[1]*z[27];
    z[25]=z[27] + z[24] + z[25];
    z[27]=n<T>(1,2)*z[1];
    z[25]=z[25]*z[27];
    z[28]=n<T>(3,2)*z[6];
    z[29]=z[28] - 1;
    z[30]=n<T>(3,4)*z[2] - n<T>(9,4) + z[6];
    z[30]=z[2]*z[30];
    z[25]=z[25] - n<T>(3,2)*z[29] + z[30];
    z[25]=z[25]*z[27];
    z[27]=npow(z[6],2);
    z[30]=z[17]*z[27];
    z[31]=z[6] - z[30];
    z[31]=z[10]*z[31];
    z[32]= - static_cast<T>(5)+ z[15];
    z[32]=z[6]*z[32];
    z[32]= - static_cast<T>(3)+ z[32];
    z[31]=n<T>(1,4)*z[32] + z[31];
    z[31]=z[10]*z[31];
    z[32]= - static_cast<T>(3)+ z[11];
    z[32]=z[6]*z[32];
    z[31]=z[31] - static_cast<T>(1)+ z[32];
    z[19]=z[25] + n<T>(1,2)*z[31] + z[19];
    z[19]=z[3]*z[19];
    z[25]=z[6] + n<T>(3,2);
    z[31]=n<T>(1,4)*z[6];
    z[32]=z[25]*z[31];
    z[33]=n<T>(1,2)*z[14];
    z[34]=z[13]*z[33];
    z[35]= - static_cast<T>(5)- z[15];
    z[35]=z[6]*z[35];
    z[34]=z[34] - static_cast<T>(1)+ z[35];
    z[34]=z[10]*z[34];
    z[35]=7*z[6];
    z[36]=static_cast<T>(1)+ z[35];
    z[34]=n<T>(1,2)*z[36] + z[34];
    z[34]=z[34]*z[21];
    z[12]=z[19] + z[12] + z[34] - static_cast<T>(1)+ z[32];
    z[19]= - z[33] + n<T>(1,2) - z[35];
    z[19]=z[10]*z[19];
    z[19]=static_cast<T>(7)+ z[19];
    z[32]=z[2] + z[4];
    z[32]= - n<T>(25,4) - z[15] + 3*z[32];
    z[32]=z[2]*z[32];
    z[34]=z[27]*z[21];
    z[35]=z[11] + 1;
    z[35]=z[35]*z[6];
    z[34]= - z[35] + z[34];
    z[34]=z[10]*z[34];
    z[36]=static_cast<T>(1)- n<T>(9,2)*z[6];
    z[36]=z[6]*z[36];
    z[34]=z[34] + n<T>(1,2) + z[36];
    z[34]=z[10]*z[34];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=n<T>(1,2)*z[16] + z[34];
    z[34]=z[2] - z[6];
    z[36]= - n<T>(11,4) + z[34];
    z[36]=z[2]*z[36];
    z[36]=n<T>(9,4) + z[36];
    z[36]=z[2]*z[36];
    z[16]=n<T>(1,2)*z[16] + z[36];
    z[16]=z[3]*z[16];
    z[36]=z[4] - n<T>(5,4)*z[10] - n<T>(17,4) - z[15];
    z[36]=z[4]*z[36];
    z[37]= - n<T>(3,4) - z[6];
    z[37]=z[8]*z[37]*npow(z[4],2);
    z[16]=z[37] + z[16] + z[32] + n<T>(1,2)*z[19] + z[36];
    z[16]=z[9]*z[16];
    z[19]=static_cast<T>(1)+ z[15];
    z[19]=z[10]*z[19];
    z[19]=z[19] - n<T>(1,8) + z[15];
    z[19]=z[10]*z[19];
    z[32]=n<T>(1,8) - z[6];
    z[32]=z[10]*z[32];
    z[32]=n<T>(5,4) + z[32];
    z[32]=z[10]*z[32];
    z[32]=z[32] - z[20];
    z[32]=z[4]*z[32];
    z[19]=z[32] - n<T>(5,2) + z[19];
    z[19]=z[4]*z[19];
    z[32]=z[6] + n<T>(1,2);
    z[17]=z[32]*z[17];
    z[36]= - n<T>(5,8) - z[6];
    z[17]=3*z[36] + z[17];
    z[17]=z[10]*z[17];
    z[36]= - n<T>(23,2) - 19*z[6];
    z[17]=n<T>(1,4)*z[36] + z[17];
    z[17]=z[10]*z[17];
    z[36]= - n<T>(1,4) - z[6];
    z[17]=z[19] + 3*z[36] + z[17];
    z[19]=n<T>(1,2)*z[4];
    z[17]=z[17]*z[19];
    z[36]=static_cast<T>(3)- z[21];
    z[36]=z[10]*z[36];
    z[36]=n<T>(15,2) + z[36];
    z[36]=z[36]*z[21];
    z[36]=static_cast<T>(3)+ z[36];
    z[37]= - static_cast<T>(1)- n<T>(3,4)*z[10];
    z[37]=z[10]*z[37];
    z[37]=z[37] + z[20];
    z[37]=z[4]*z[37];
    z[36]=n<T>(1,2)*z[36] + z[37];
    z[36]=z[4]*z[36];
    z[37]= - n<T>(3,2) - z[10];
    z[37]=z[10]*z[37];
    z[37]= - static_cast<T>(3)+ z[37];
    z[36]=n<T>(1,2)*z[37] + z[36];
    z[23]=z[36]*z[23];
    z[36]= - n<T>(1,4)*z[14] + static_cast<T>(1)+ n<T>(5,4)*z[6];
    z[36]=z[10]*z[36];
    z[13]=n<T>(3,4)*z[13] + z[36];
    z[13]=z[13]*z[21];
    z[13]=z[23] + z[17] + z[13] + static_cast<T>(1)+ n<T>(5,8)*z[6];
    z[13]=z[2]*z[13];
    z[17]= - n<T>(19,4) + z[15];
    z[17]=z[6]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[10]*z[17];
    z[17]= - 5*z[29] + z[17];
    z[17]=z[10]*z[17];
    z[17]=static_cast<T>(5)+ z[17];
    z[23]= - n<T>(5,8) + z[6];
    z[23]=z[10]*z[23];
    z[23]= - n<T>(5,4) + z[23];
    z[23]=z[10]*z[23];
    z[20]=z[23] + z[20];
    z[20]=z[4]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[17]=z[4]*z[17];
    z[20]=static_cast<T>(7)- n<T>(15,4)*z[6];
    z[20]=z[6]*z[20];
    z[23]= - n<T>(27,8) + z[6];
    z[23]=z[6]*z[23];
    z[23]=n<T>(7,4) + z[23];
    z[23]=z[6]*z[23];
    z[23]= - n<T>(1,8) + z[23];
    z[23]=z[10]*z[23];
    z[20]=z[23] - n<T>(3,2) + z[20];
    z[20]=z[10]*z[20];
    z[23]=z[6] - n<T>(3,4);
    z[17]=z[17] + 5*z[23] + z[20];
    z[17]=z[4]*z[17];
    z[20]= - n<T>(17,2) + z[6];
    z[20]=z[6]*z[20];
    z[20]=static_cast<T>(9)+ z[20];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(3,2) + z[20];
    z[20]=z[20]*z[33];
    z[29]=static_cast<T>(13)- n<T>(5,2)*z[6];
    z[29]=z[6]*z[29];
    z[29]= - n<T>(15,2) + z[29];
    z[29]=z[6]*z[29];
    z[20]=z[20] + n<T>(1,2) + z[29];
    z[20]=z[10]*z[20];
    z[29]= - n<T>(27,2) + 5*z[6];
    z[29]=z[6]*z[29];
    z[20]=z[20] + static_cast<T>(3)+ z[29];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[17]=z[17]*z[19];
    z[19]= - z[23]*z[15];
    z[19]= - n<T>(1,8) + z[19];
    z[20]=n<T>(5,2) - z[6];
    z[20]=z[6]*z[20];
    z[20]= - n<T>(3,4) + z[20];
    z[20]=z[20]*z[30];
    z[23]= - n<T>(3,2) + z[6];
    z[23]=z[6]*z[23];
    z[23]=n<T>(1,4) + z[23];
    z[23]=z[6]*z[23];
    z[20]=z[23] + z[20];
    z[20]=z[10]*z[20];
    z[17]=z[17] + n<T>(1,2)*z[19] + z[20];
    z[17]=z[4]*z[17];
    z[18]= - z[18]*z[27];
    z[19]= - n<T>(1,2) + z[6];
    z[19]=z[10]*z[19]*npow(z[6],3);
    z[18]=z[18] + z[19];
    z[18]=z[10]*z[18];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[6]*z[15];
    z[15]=z[15] + z[18];
    z[15]=n<T>(1,8)*z[15] + z[17];
    z[15]=z[5]*z[15];
    z[17]=5*z[4];
    z[18]= - n<T>(1,2) + z[4];
    z[18]=z[18]*z[17];
    z[18]=z[11] + z[18];
    z[18]=z[5]*z[18];
    z[19]= - z[2] + n<T>(1,2) + z[4];
    z[20]=z[6] - z[4];
    z[20]=z[5]*z[20];
    z[20]= - static_cast<T>(1)+ z[20];
    z[20]=z[1]*z[20];
    z[18]=z[20] + 5*z[19] + z[18];
    z[19]=n<T>(1,8)*z[1];
    z[18]=z[18]*z[19];
    z[20]=z[4] + z[6];
    z[17]= - z[20]*z[17];
    z[17]= - z[24] + z[17];
    z[20]=n<T>(5,2)*z[4] + n<T>(11,8) - z[34];
    z[20]=z[2]*z[20];
    z[17]=n<T>(1,2)*z[17] + z[20];
    z[20]= - z[4] - z[26];
    z[20]=z[4]*z[20];
    z[20]=n<T>(5,4)*z[20] + z[22];
    z[20]=z[4]*z[20];
    z[20]= - n<T>(1,8)*z[6] + z[20];
    z[20]=z[5]*z[20];
    z[17]=z[18] + n<T>(1,2)*z[17] + z[20];
    z[17]=z[1]*z[17];
    z[18]=z[6] + n<T>(7,2);
    z[18]=z[18]*z[31];
    z[18]=z[18] + 1;
    z[18]=z[18]*z[6];
    z[18]=z[18] + n<T>(3,8);
    z[18]=z[18]*z[14];
    z[20]= - z[32]*z[11];
    z[20]=static_cast<T>(1)+ z[20];
    z[20]=z[6]*z[20];
    z[20]=n<T>(3,4) + z[20];
    z[20]=n<T>(1,2)*z[20] + z[18];
    z[20]=z[4]*z[20]*z[21];
    z[21]= - z[32]*z[31];
    z[22]=n<T>(1,2) + z[35];
    z[22]=z[6]*z[22];
    z[22]=z[22] - z[18];
    z[22]=z[10]*z[22];
    z[20]=z[20] + z[21] + z[22];
    z[20]=z[4]*z[20];
    z[21]= - n<T>(5,2) - z[6];
    z[11]=z[21]*z[11];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[6]*z[11];
    z[11]= - n<T>(1,4) + z[11];
    z[11]=n<T>(3,2)*z[11] + z[18];
    z[11]=z[10]*z[11];
    z[18]=z[25]*z[28];
    z[18]=static_cast<T>(1)+ z[18];
    z[11]=n<T>(1,2)*z[18] + z[11];
    z[18]= - z[32]*z[19];
    z[11]=z[18] + n<T>(1,2)*z[11] + z[20];
    z[11]=z[8]*z[11];
    z[18]=static_cast<T>(7)+ 3*z[10];
    z[18]=z[27]*z[18];
    z[19]=n<T>(3,2)*z[27];
    z[20]=z[27]*z[1];
    z[21]= - z[19] - z[20];
    z[21]=z[1]*z[21];
    z[19]=z[19] + z[21];
    z[19]=z[1]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[3]*z[18];
    z[14]=z[28] + z[14];
    z[19]= - n<T>(1,2)*z[27] - z[20];
    z[19]=z[1]*z[5]*z[19];
    z[19]=z[6] + z[19];
    z[19]=z[1]*z[19];
    z[20]=static_cast<T>(1)+ n<T>(3,2)*z[10];
    z[20]=z[4]*z[20];
    z[14]=z[18] + z[19] + 3*z[14] + z[20];
    z[14]=z[7]*z[14];

    r += z[11] + n<T>(1,2)*z[12] + z[13] + n<T>(1,8)*z[14] + z[15] + n<T>(1,4)*
      z[16] + z[17];
 
    return r;
}

template double qqb_2lha_r1618(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1618(const std::array<dd_real,30>&);
#endif
