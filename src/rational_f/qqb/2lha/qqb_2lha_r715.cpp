#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r715(const std::array<T,30>& k) {
  T z[53];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=k[9];
    z[7]=k[3];
    z[8]=k[12];
    z[9]=k[15];
    z[10]=k[11];
    z[11]=k[13];
    z[12]=k[28];
    z[13]=n<T>(1,4)*z[6];
    z[14]=5*z[6];
    z[15]= - static_cast<T>(3)+ z[14];
    z[15]=z[15]*z[13];
    z[16]=n<T>(1,4)*z[4];
    z[17]=npow(z[6],2);
    z[18]=z[16]*z[17];
    z[19]=z[6] - n<T>(1,4);
    z[19]=z[19]*z[6];
    z[20]=z[19] + z[18];
    z[20]=z[4]*z[20];
    z[15]=z[15] + z[20];
    z[15]=z[4]*z[15];
    z[20]=z[6] - 1;
    z[21]=n<T>(1,2)*z[6];
    z[22]=z[20]*z[21];
    z[15]=z[22] + z[15];
    z[15]=z[8]*z[15];
    z[23]=n<T>(3,4)*z[6];
    z[24]= - z[20]*z[23];
    z[25]=z[17]*z[4];
    z[26]=static_cast<T>(1)- z[14];
    z[26]=z[6]*z[26];
    z[26]=z[26] - z[25];
    z[26]=z[26]*z[16];
    z[27]=static_cast<T>(1)- n<T>(7,4)*z[6];
    z[27]=z[6]*z[27];
    z[26]=z[27] + z[26];
    z[26]=z[4]*z[26];
    z[15]=z[15] + z[24] + z[26];
    z[15]=z[7]*z[15];
    z[24]= - static_cast<T>(3)- z[6];
    z[24]=z[24]*z[21];
    z[26]=n<T>(1,2)*z[4];
    z[27]=z[26]*z[17];
    z[28]= - static_cast<T>(1)+ n<T>(3,2)*z[6];
    z[28]=z[28]*z[6];
    z[29]=z[28] + z[27];
    z[29]=z[4]*z[29];
    z[24]=z[29] + static_cast<T>(1)+ z[24];
    z[24]=z[4]*z[24];
    z[29]=3*z[6];
    z[30]=z[29] - 5;
    z[30]=z[30]*z[21];
    z[30]=z[30] + 1;
    z[24]=z[24] - z[30];
    z[24]=z[24]*z[26];
    z[31]=z[25] - z[17];
    z[32]=z[3]*npow(z[4],3);
    z[32]=n<T>(1,4)*z[32];
    z[31]=z[31]*z[32];
    z[32]=z[13] - 1;
    z[32]=z[32]*z[6];
    z[33]= - z[32] - z[27];
    z[33]=z[4]*z[33];
    z[34]=z[23] - 1;
    z[34]=z[34]*z[6];
    z[33]=z[34] + z[33];
    z[35]=npow(z[4],2);
    z[33]=z[33]*z[35];
    z[33]=z[33] + z[31];
    z[33]=z[3]*z[33];
    z[36]=z[20]*z[6];
    z[37]=z[36] + z[25];
    z[38]= - z[4]*z[37];
    z[38]=z[17] + z[38];
    z[38]=z[4]*z[38];
    z[38]=z[36] + z[38];
    z[38]=z[7]*z[38];
    z[24]=n<T>(1,4)*z[38] + z[24] + z[33];
    z[24]=z[9]*z[24];
    z[33]= - n<T>(1,2) - z[6];
    z[33]=z[33]*z[21];
    z[38]=n<T>(1,4) + z[6];
    z[38]=z[6]*z[38];
    z[18]=z[38] - z[18];
    z[18]=z[4]*z[18];
    z[18]=z[33] + z[18];
    z[18]=z[18]*z[35];
    z[18]=z[18] + z[31];
    z[18]=z[3]*z[18];
    z[31]=z[6] - 3;
    z[33]= - z[6]*z[31];
    z[33]=z[33] - 3*z[25];
    z[33]=z[4]*z[33];
    z[33]=z[21] + z[33];
    z[33]=z[33]*z[16];
    z[38]=z[25] - z[6];
    z[38]=z[38]*z[7]*z[4];
    z[18]=n<T>(1,8)*z[38] + z[33] + z[18];
    z[18]=z[11]*z[18];
    z[15]=z[18] + z[15] + z[24];
    z[18]=z[6] + 1;
    z[24]= - z[18]*z[13];
    z[33]=7*z[6];
    z[39]=z[33] - 13;
    z[40]=z[4]*z[6];
    z[41]=z[39]*z[40];
    z[22]=z[2]*z[22];
    z[22]=z[22] - n<T>(1,8)*z[41] + static_cast<T>(1)+ z[24];
    z[22]=z[2]*z[22];
    z[24]=z[6] - n<T>(3,2);
    z[41]=z[24]*z[6];
    z[42]= - n<T>(15,4)*z[40] - n<T>(9,2) - z[41];
    z[42]=z[4]*z[42];
    z[43]=z[6] - n<T>(9,4);
    z[43]=z[43]*z[6];
    z[42]=z[42] - n<T>(13,4) - z[43];
    z[22]=n<T>(1,2)*z[42] + z[22];
    z[22]=z[2]*z[22];
    z[42]=z[6] - n<T>(1,2);
    z[44]=z[42]*z[33];
    z[45]=n<T>(5,2)*z[6];
    z[46]=z[45] + 3;
    z[47]=z[46]*z[40];
    z[44]=z[47] + static_cast<T>(5)+ z[44];
    z[44]=z[4]*z[44];
    z[47]=11*z[6];
    z[48]= - static_cast<T>(19)+ z[47];
    z[48]=z[6]*z[48];
    z[48]=static_cast<T>(17)+ z[48];
    z[44]=n<T>(1,2)*z[48] + z[44];
    z[44]=z[4]*z[44];
    z[48]=z[41] + n<T>(5,2);
    z[44]=z[44] + z[48];
    z[22]=n<T>(1,4)*z[44] + z[22];
    z[22]=z[2]*z[22];
    z[44]=n<T>(3,4) - z[6];
    z[44]=z[44]*z[40];
    z[30]= - n<T>(3,2)*z[30] + z[44];
    z[30]=z[4]*z[30];
    z[44]=n<T>(5,2) - z[6];
    z[44]=z[6]*z[44];
    z[44]= - n<T>(3,2) + z[44];
    z[30]=n<T>(3,2)*z[44] + z[30];
    z[30]=z[4]*z[30];
    z[44]= - static_cast<T>(7)+ z[29];
    z[44]=z[44]*z[13];
    z[41]=n<T>(1,2) + z[41];
    z[41]=z[41]*z[26];
    z[41]=z[41] + static_cast<T>(1)+ z[44];
    z[41]=z[4]*z[41];
    z[31]=z[31]*z[21];
    z[31]=z[31] + 1;
    z[44]=n<T>(1,2)*z[31];
    z[41]=z[44] + z[41];
    z[49]=n<T>(1,2)*z[2];
    z[50]=z[49] - n<T>(3,2);
    z[50]=z[31]*z[50];
    z[51]= - n<T>(5,4) - z[43];
    z[51]=z[4]*z[51];
    z[50]=z[51] + z[50];
    z[50]=z[2]*z[50];
    z[41]=3*z[41] + z[50];
    z[41]=z[2]*z[41];
    z[30]=z[41] - z[44] + z[30];
    z[30]=z[2]*z[30];
    z[41]=z[20]*z[29];
    z[41]=z[41] + z[25];
    z[41]=z[41]*z[26];
    z[44]=z[21] - 1;
    z[50]=z[44]*z[6];
    z[50]=z[50] + n<T>(1,2);
    z[41]=3*z[50] + z[41];
    z[41]=z[4]*z[41];
    z[41]=z[41] + z[31];
    z[41]=z[41]*z[26];
    z[30]=z[41] + z[30];
    z[30]=z[3]*z[30];
    z[41]=static_cast<T>(7)- z[45];
    z[41]=z[41]*z[21];
    z[41]= - static_cast<T>(3)+ z[41];
    z[41]=z[4]*z[41];
    z[51]=z[44]*z[14];
    z[51]=n<T>(11,2) + z[51];
    z[51]=z[2]*z[51];
    z[52]= - n<T>(9,2) - z[36];
    z[41]=n<T>(1,4)*z[51] + n<T>(1,2)*z[52] + z[41];
    z[41]=z[2]*z[41];
    z[42]=z[4]*z[42];
    z[28]= - n<T>(13,2)*z[42] + static_cast<T>(5)- z[28];
    z[28]=z[4]*z[28];
    z[42]=n<T>(7,2)*z[6];
    z[51]=static_cast<T>(11)- z[42];
    z[51]=z[6]*z[51];
    z[51]=n<T>(3,2) + z[51];
    z[28]=n<T>(1,2)*z[51] + z[28];
    z[28]=n<T>(1,2)*z[28] + z[41];
    z[28]=z[2]*z[28];
    z[41]=static_cast<T>(1)+ n<T>(5,4)*z[6];
    z[41]=z[41]*z[40];
    z[51]= - static_cast<T>(9)+ z[47];
    z[51]=z[6]*z[51];
    z[51]= - static_cast<T>(1)+ z[51];
    z[41]=n<T>(1,4)*z[51] + z[41];
    z[41]=z[4]*z[41];
    z[51]= - static_cast<T>(6)+ n<T>(11,4)*z[6];
    z[51]=z[6]*z[51];
    z[41]=z[41] + static_cast<T>(1)+ z[51];
    z[41]=z[4]*z[41];
    z[51]=z[23] - 2;
    z[51]=z[51]*z[6];
    z[51]=z[51] + n<T>(1,2);
    z[28]=z[28] + z[41] + z[51];
    z[28]=z[2]*z[28];
    z[41]=z[27] + z[36];
    z[41]=z[41]*z[4];
    z[52]=static_cast<T>(13)- n<T>(15,2)*z[6];
    z[52]=z[6]*z[52];
    z[52]= - 5*z[41] - n<T>(11,2) + z[52];
    z[52]=z[52]*z[16];
    z[51]=z[52] - z[51];
    z[51]=z[4]*z[51];
    z[28]=3*z[30] + z[51] + z[28];
    z[28]=z[3]*z[28];
    z[30]=static_cast<T>(3)- z[47];
    z[30]=z[30]*z[21];
    z[30]=z[30] - z[25];
    z[30]=z[30]*z[16];
    z[47]=n<T>(15,8) - z[6];
    z[47]=z[6]*z[47];
    z[30]=z[30] - n<T>(1,2) + z[47];
    z[30]=z[4]*z[30];
    z[30]= - n<T>(1,4)*z[48] + z[30];
    z[30]=z[4]*z[30];
    z[22]=z[28] + z[30] + z[22];
    z[22]=z[3]*z[22];
    z[28]= - static_cast<T>(1)- z[14];
    z[13]=z[28]*z[13];
    z[28]=n<T>(15,2) - z[6];
    z[28]=z[4]*z[28]*z[21];
    z[13]=z[28] + static_cast<T>(3)+ z[13];
    z[13]=z[4]*z[13];
    z[18]=z[18]*z[21];
    z[28]=n<T>(1,4)*z[40];
    z[30]=z[39]*z[28];
    z[39]= - z[2]*z[36];
    z[18]=z[39] + z[30] - static_cast<T>(1)+ z[18];
    z[18]=z[2]*z[18];
    z[30]=static_cast<T>(3)- z[45];
    z[30]=z[6]*z[30];
    z[30]= - n<T>(1,2) + z[30];
    z[13]=z[18] + n<T>(1,2)*z[30] + z[13];
    z[13]=z[2]*z[13];
    z[18]= - static_cast<T>(7)- z[6];
    z[18]=z[18]*z[28];
    z[18]=z[18] - static_cast<T>(3)+ z[34];
    z[18]=z[4]*z[18];
    z[18]=z[18] + n<T>(1,2) + z[43];
    z[18]=z[4]*z[18];
    z[30]=z[29] - n<T>(11,2);
    z[30]=z[30]*z[6];
    z[30]=z[30] + n<T>(5,2);
    z[39]=n<T>(1,4)*z[30];
    z[13]=z[13] - z[39] + z[18];
    z[13]=z[2]*z[13];
    z[18]= - z[49] - n<T>(1,2);
    z[18]=z[50]*z[18];
    z[31]=z[4]*z[31];
    z[18]=z[31] + z[18];
    z[18]=z[2]*z[18];
    z[31]=static_cast<T>(3)- z[21];
    z[31]=z[6]*z[31];
    z[31]= - static_cast<T>(3)+ z[31];
    z[31]=z[31]*z[26];
    z[31]=z[31] + n<T>(3,4) + z[32];
    z[31]=z[4]*z[31];
    z[32]=n<T>(1,4)*z[50];
    z[18]=z[18] - z[32] + z[31];
    z[18]=z[2]*z[18];
    z[31]= - z[4]*z[44];
    z[24]=n<T>(1,2)*z[24] + z[31];
    z[24]=z[4]*z[24];
    z[24]= - n<T>(1,4)*z[20] + z[24];
    z[24]=z[4]*z[24];
    z[18]=z[18] - z[32] + z[24];
    z[18]=z[2]*z[18];
    z[17]=static_cast<T>(1)- z[17];
    z[24]=static_cast<T>(1)- z[4];
    z[24]=z[4]*z[24];
    z[24]= - n<T>(1,2) + z[24];
    z[24]=z[4]*z[24];
    z[17]=n<T>(1,2)*z[17] + z[24];
    z[17]=z[4]*z[17];
    z[17]=z[17] - z[50];
    z[17]=n<T>(1,4)*z[17] + z[18];
    z[17]=z[5]*z[17];
    z[18]=z[28] + static_cast<T>(1)+ z[23];
    z[18]=z[4]*z[18];
    z[23]= - static_cast<T>(1)+ z[29];
    z[18]=n<T>(1,4)*z[23] + z[18];
    z[18]=z[4]*z[18];
    z[23]= - static_cast<T>(5)- z[6];
    z[23]=z[6]*z[23];
    z[23]=static_cast<T>(5)+ z[23];
    z[18]=n<T>(1,8)*z[23] + z[18];
    z[18]=z[4]*z[18];
    z[13]=z[17] + z[13] - z[39] + z[18];
    z[17]=n<T>(1,2)*z[5];
    z[13]=z[13]*z[17];
    z[14]= - n<T>(1,2) + z[14];
    z[14]=z[14]*z[21];
    z[18]=z[6]*z[46];
    z[18]=z[40] - static_cast<T>(1)+ z[18];
    z[18]=z[18]*z[26];
    z[14]=z[18] - static_cast<T>(1)+ z[14];
    z[14]=z[4]*z[14];
    z[18]= - static_cast<T>(3)+ z[42];
    z[18]=z[6]*z[18];
    z[18]=z[18] + z[27];
    z[18]=z[4]*z[18];
    z[21]=z[41] + z[50];
    z[21]=z[5]*z[21];
    z[18]=z[21] + z[18] + z[30];
    z[17]=z[18]*z[17];
    z[14]=z[17] + n<T>(5,4)*z[36] + z[14];
    z[14]=z[8]*z[14];
    z[17]= - z[8] + z[11];
    z[17]=z[12]*z[17]*z[37]*z[35];
    z[17]=z[36] - z[17];
    z[18]= - static_cast<T>(1)+ n<T>(11,8)*z[6];
    z[18]=z[18]*z[29];
    z[18]=n<T>(1,2) + z[18];
    z[19]=z[19] + n<T>(1,8)*z[25];
    z[19]=z[4]*z[19];
    z[18]=n<T>(1,2)*z[18] + z[19];
    z[18]=z[4]*z[18];
    z[19]= - n<T>(33,2) + z[33];
    z[19]=z[6]*z[19];
    z[19]=static_cast<T>(7)+ z[19];
    z[18]=n<T>(1,8)*z[19] + z[18];
    z[18]=z[4]*z[18];
    z[19]= - z[49] + static_cast<T>(1)+ z[26];
    z[19]=z[2]*z[19];
    z[19]=z[19] - n<T>(1,2) - z[4];
    z[19]=z[2]*z[19];
    z[21]= - static_cast<T>(1)- z[4];
    z[23]= - z[2] + static_cast<T>(3)+ z[4];
    z[23]=z[2]*z[23];
    z[21]=3*z[21] + z[23];
    z[21]=z[2]*z[21];
    z[21]=z[21] + static_cast<T>(1)+ 3*z[4];
    z[21]=z[2]*z[21];
    z[21]= - z[4] + z[21];
    z[21]=z[3]*z[21];
    z[19]=n<T>(3,4)*z[21] + z[26] + z[19];
    z[19]=z[1]*z[19]*npow(z[3],2);
    z[21]=static_cast<T>(1)- z[33];
    z[21]=z[6]*z[21];
    z[21]=z[21] - z[25];
    z[21]=z[4]*z[21];
    z[23]=static_cast<T>(7)- 15*z[6];
    z[23]=z[6]*z[23];
    z[21]=z[21] - static_cast<T>(1)+ z[23];
    z[16]=z[21]*z[16];
    z[21]= - n<T>(1,4) - z[34];
    z[16]=3*z[21] + z[16];
    z[16]=z[16]*z[49];
    z[20]=z[40] + z[20];
    z[20]=z[4]*z[20];
    z[20]=z[20] + z[38];
    z[20]=z[10]*z[20];

    r += z[13] + n<T>(1,4)*z[14] + n<T>(1,2)*z[15] + z[16] - n<T>(5,16)*z[17] + 
      z[18] + z[19] + n<T>(1,16)*z[20] + z[22];
 
    return r;
}

template double qqb_2lha_r715(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r715(const std::array<dd_real,30>&);
#endif
