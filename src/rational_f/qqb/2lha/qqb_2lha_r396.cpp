#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r396(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[4];
    z[5]=k[10];
    z[6]=static_cast<T>(1)- z[4];
    z[7]=npow(z[4],2);
    z[8]=npow(z[5],2);
    z[6]=z[7]*z[8]*z[6];
    z[9]=z[2] - z[1];
    z[10]=n<T>(1,2)*z[5];
    z[11]=static_cast<T>(2)+ z[10];
    z[11]=z[5]*z[11];
    z[12]= - static_cast<T>(1)- z[8];
    z[12]=z[4]*z[12];
    z[11]=n<T>(1,2)*z[12] + z[11] + n<T>(5,2) - z[9];
    z[11]=z[2]*z[11];
    z[12]=z[8]*z[4];
    z[13]=static_cast<T>(2)+ z[5];
    z[13]=z[5]*z[13];
    z[13]= - z[12] + static_cast<T>(1)+ z[13];
    z[13]=z[4]*z[13];
    z[11]=z[13] + z[11];
    z[11]=z[2]*z[11];
    z[13]=static_cast<T>(1)+ z[5];
    z[7]=z[5]*z[13]*z[7];
    z[13]=static_cast<T>(3)+ z[5];
    z[13]=z[5]*z[13];
    z[13]= - z[4] + z[13] + static_cast<T>(3)- z[9];
    z[13]=z[2]*z[13];
    z[10]=static_cast<T>(1)+ z[10];
    z[10]=z[5]*z[10];
    z[10]=n<T>(1,2) + z[10];
    z[10]=z[4]*z[10];
    z[10]=3*z[10] + n<T>(1,2)*z[13];
    z[10]=z[2]*z[10];
    z[7]=n<T>(3,2)*z[7] + z[10];
    z[7]=z[2]*z[7];
    z[10]=z[8]*npow(z[4],3);
    z[7]=n<T>(1,2)*z[10] + z[7];
    z[7]=z[3]*z[7];
    z[6]=z[7] + n<T>(1,2)*z[6] + z[11];
    z[6]=z[3]*z[6];
    z[7]=static_cast<T>(5)+ 3*z[1];
    z[8]= - n<T>(1,2) - z[8];
    z[8]=z[4]*z[8];
    z[7]= - n<T>(3,2)*z[2] + z[8] + n<T>(1,2)*z[7] + z[5];
    z[7]=z[2]*z[7];
    z[8]=n<T>(1,2) - z[12];
    z[8]=z[4]*z[8];
    z[7]=z[8] + z[7];
    z[6]=n<T>(1,2)*z[7] + z[6];
    z[6]=z[3]*z[6];
    z[7]=static_cast<T>(1)- z[9];
    z[6]=n<T>(1,4)*z[7] + z[6];

    r += z[6]*z[3];
 
    return r;
}

template double qqb_2lha_r396(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r396(const std::array<dd_real,30>&);
#endif
