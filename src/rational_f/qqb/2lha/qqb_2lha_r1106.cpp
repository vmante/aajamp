#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1106(const std::array<T,30>& k) {
  T z[16];
  T r = 0;

    z[1]=k[2];
    z[2]=k[4];
    z[3]=k[7];
    z[4]=k[11];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=n<T>(1,2)*z[6];
    z[8]=n<T>(3,4) - z[6];
    z[8]=z[8]*z[7];
    z[9]= - n<T>(5,3) + z[6];
    z[10]=n<T>(1,3)*z[3];
    z[11]= - z[1]*z[10];
    z[9]=n<T>(1,4)*z[9] + z[11];
    z[9]=z[3]*z[9];
    z[8]=z[8] + z[9];
    z[8]=z[3]*z[8];
    z[9]=npow(z[6],2);
    z[8]=n<T>(1,4)*z[9] + z[8];
    z[8]=z[4]*z[8];
    z[11]= - n<T>(3,4)*z[6] + z[10];
    z[11]=z[3]*z[11];
    z[11]=n<T>(3,4)*z[9] + z[11];
    z[11]=z[3]*z[11];
    z[12]=npow(z[6],3);
    z[11]= - z[12] + z[11];
    z[8]=n<T>(1,2)*z[11] + z[8];
    z[8]=z[4]*z[8];
    z[11]=z[6] - 1;
    z[13]=n<T>(1,2)*z[11];
    z[14]= - z[9]*z[13];
    z[15]=n<T>(1,4)*z[6] - z[10];
    z[11]=z[3]*z[11]*z[15];
    z[11]=z[14] + z[11];
    z[11]=z[2]*z[3]*z[11]*npow(z[4],2);
    z[8]=z[8] + n<T>(1,2)*z[11];
    z[8]=z[2]*z[8];
    z[11]=z[1] - 1;
    z[14]= - z[11]*z[10];
    z[14]= - n<T>(3,4) + z[14];
    z[14]=z[3]*z[14];
    z[14]=z[7] + z[14];
    z[14]=z[3]*z[14];
    z[11]=z[11]*z[3];
    z[11]=n<T>(7,4) + z[11];
    z[10]=z[10]*z[1]*z[11];
    z[10]= - z[13] + z[10];
    z[10]=z[3]*z[10];
    z[7]=z[7] + z[10];
    z[7]=z[4]*z[7];
    z[7]=z[7] + z[14] - n<T>(1,2)*z[9];
    z[7]=z[4]*z[7];
    z[9]= - z[5]*z[12];
    z[10]= - z[3]*z[6];
    z[9]=z[9] + z[10];
    z[7]=n<T>(1,2)*z[9] + z[7];
    z[7]=n<T>(1,2)*z[7] + z[8];

    r += n<T>(1,8)*z[7]*z[2];
 
    return r;
}

template double qqb_2lha_r1106(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1106(const std::array<dd_real,30>&);
#endif
