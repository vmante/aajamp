#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1333(const std::array<T,30>& k) {
  T z[25];
  T r = 0;

    z[1]=k[1];
    z[2]=k[8];
    z[3]=k[12];
    z[4]=k[3];
    z[5]=k[4];
    z[6]=k[9];
    z[7]=z[1] + 1;
    z[7]=z[7]*z[1];
    z[8]=z[6] - 1;
    z[9]=z[7] + z[8];
    z[7]=z[7] - 1;
    z[7]=z[7]*z[1];
    z[10]=z[7] - z[8];
    z[11]=z[2]*z[10];
    z[11]= - n<T>(5,2)*z[9] + z[11];
    z[12]=n<T>(1,2)*z[2];
    z[11]=z[11]*z[12];
    z[13]=z[6] + 1;
    z[14]=n<T>(1,4)*z[5];
    z[15]= - z[13]*z[14];
    z[16]=z[1] + 3;
    z[17]=n<T>(1,2)*z[5];
    z[18]= - z[17] + z[16];
    z[19]=n<T>(1,2)*z[1];
    z[18]=z[18]*z[19];
    z[11]=z[11] + z[18] + z[15] - z[8];
    z[15]=n<T>(1,4)*z[1];
    z[18]= - static_cast<T>(7)+ z[5];
    z[18]=n<T>(1,2)*z[18] - z[1];
    z[18]=z[18]*z[15];
    z[18]=z[18] - static_cast<T>(1)+ z[14];
    z[18]=z[1]*z[18];
    z[20]=z[19] + 1;
    z[21]=z[20]*npow(z[1],2)*z[12];
    z[14]=z[21] + z[14] + z[18];
    z[14]=z[3]*z[14];
    z[11]=n<T>(1,2)*z[11] + z[14];
    z[11]=z[3]*z[11];
    z[14]=z[2]*z[9];
    z[18]= - n<T>(7,4) - z[1];
    z[18]=z[1]*z[18];
    z[14]=n<T>(1,4)*z[14] + n<T>(3,4)*z[8] + z[18];
    z[14]=z[2]*z[14];
    z[18]=z[5]*z[6];
    z[13]=3*z[13] + z[18];
    z[15]=z[15] + 1;
    z[18]=z[1]*z[15];
    z[13]=z[14] + n<T>(1,4)*z[13] + z[18];
    z[13]=z[3]*z[13];
    z[14]=z[1] + n<T>(1,2);
    z[18]=static_cast<T>(1)+ n<T>(3,2)*z[1];
    z[18]=z[18]*z[1];
    z[18]=z[18] - n<T>(1,2);
    z[21]= - z[2]*z[18];
    z[21]=n<T>(5,2)*z[14] + z[21];
    z[21]=z[2]*z[21];
    z[13]=z[13] + z[21] - z[15];
    z[13]=z[3]*z[13];
    z[15]=3*z[1];
    z[21]=z[15] + 1;
    z[22]=z[2]*z[21];
    z[23]= - z[2]*z[14];
    z[23]=z[23] + n<T>(3,2) + z[1];
    z[23]=z[3]*z[23];
    z[22]=z[23] - n<T>(5,2) + z[22];
    z[23]=n<T>(1,2)*z[3];
    z[22]=z[23]*z[22];
    z[24]= - n<T>(1,4) - z[1];
    z[24]=z[2]*z[24];
    z[22]=z[22] + n<T>(1,2) + z[24];
    z[22]=z[2]*z[22];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[3]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[24]=n<T>(1,2)*z[4];
    z[23]=z[24]*z[23]*npow(z[2],2);
    z[22]=z[23] + z[22];
    z[22]=z[4]*z[22];
    z[15]=z[14]*z[15];
    z[15]= - n<T>(1,2) + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] - z[21];
    z[15]=z[2]*z[15];
    z[15]=n<T>(1,2) + z[15];
    z[13]=z[22] + n<T>(1,2)*z[15] + z[13];
    z[13]=z[13]*z[24];
    z[15]= - n<T>(3,4) - z[1];
    z[15]=z[1]*z[15];
    z[15]=n<T>(1,2) + z[15];
    z[15]=z[1]*z[15];
    z[15]= - n<T>(1,4) + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] + z[18];
    z[15]=z[2]*z[15];
    z[14]= - n<T>(1,2)*z[14] + z[15];
    z[11]=z[13] + n<T>(1,2)*z[14] + z[11];
    z[11]=z[4]*z[11];
    z[7]=static_cast<T>(1)+ z[7];
    z[7]=z[1]*z[7];
    z[7]=z[7] + z[8];
    z[7]=z[7]*z[12];
    z[7]=z[7] - z[10];
    z[7]=z[2]*z[7];
    z[8]=z[17] - z[20];
    z[8]=z[1]*z[8];
    z[10]= - z[5] + z[16];
    z[10]=z[10]*z[19];
    z[10]=z[10] + static_cast<T>(1)- n<T>(3,2)*z[5];
    z[10]=z[1]*z[10];
    z[10]= - z[5] + z[10];
    z[10]=z[3]*z[10];
    z[8]=z[10] + z[5] + z[8];
    z[8]=z[3]*z[1]*z[8];
    z[7]=z[8] + n<T>(1,2)*z[9] + z[7];

    r += n<T>(1,4)*z[7] + z[11];
 
    return r;
}

template double qqb_2lha_r1333(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1333(const std::array<dd_real,30>&);
#endif
