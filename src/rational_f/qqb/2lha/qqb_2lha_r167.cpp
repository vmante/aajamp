#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r167(const std::array<T,30>& k) {
  T z[20];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[3];
    z[5]=k[2];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[5];
    z[9]=z[4] - z[1];
    z[10]= - n<T>(41,2) - 19*z[9];
    z[11]=npow(z[1],2);
    z[10]=z[3]*z[11]*z[10];
    z[12]=123*z[1];
    z[13]=z[12]*z[4];
    z[14]=static_cast<T>(123)- 181*z[1];
    z[14]=z[1]*z[14];
    z[10]=3*z[10] + z[14] - z[13];
    z[14]=n<T>(1,4)*z[3];
    z[10]=z[10]*z[14];
    z[15]=z[1] - z[7];
    z[16]= - z[4]*z[7];
    z[16]= - 3*z[15] + z[16];
    z[17]=n<T>(1,2)*z[4];
    z[16]=z[16]*z[17];
    z[18]= - n<T>(1,2)*z[7] + z[1];
    z[16]=3*z[18] + z[16];
    z[16]=z[4]*z[16];
    z[18]=static_cast<T>(1)- z[4];
    z[11]=z[11]*z[18];
    z[18]= - z[3]*npow(z[1],3);
    z[11]=3*z[11] + z[18];
    z[18]=n<T>(1,2)*z[3];
    z[11]=z[11]*z[18];
    z[19]=z[7] - 3*z[1];
    z[11]=z[11] + n<T>(1,2)*z[19] + z[16];
    z[11]=z[2]*z[11];
    z[16]=67*z[7];
    z[19]= - n<T>(351,2) - z[16] + 57*z[9];
    z[19]=z[4]*z[19];
    z[15]=n<T>(1,4)*z[19] + static_cast<T>(45)- n<T>(67,2)*z[15];
    z[15]=z[4]*z[15];
    z[16]=191*z[1] - n<T>(123,2) - z[16];
    z[10]=n<T>(57,2)*z[11] + z[10] + n<T>(1,4)*z[16] + z[15];
    z[10]=z[2]*z[10];
    z[11]=n<T>(1,2) + z[12];
    z[11]=z[1]*z[11];
    z[11]=z[11] - z[13];
    z[11]=z[3]*z[11];
    z[11]=z[11] + z[17] - n<T>(1,2) - 261*z[1];
    z[11]=z[3]*z[11];
    z[12]=n<T>(37,2)*z[1];
    z[13]= - z[12] + static_cast<T>(69)+ n<T>(37,2)*z[7];
    z[15]= - static_cast<T>(17)- n<T>(37,16)*z[7] + n<T>(67,8)*z[9];
    z[15]=z[4]*z[15];
    z[10]=n<T>(1,2)*z[10] + n<T>(1,16)*z[11] + n<T>(1,8)*z[13] + z[15];
    z[10]=z[2]*z[10];
    z[9]=static_cast<T>(11)+ z[9];
    z[9]=z[9]*z[18];
    z[9]=static_cast<T>(9)+ z[9];
    z[9]=z[9]*z[18];
    z[9]=z[9] + n<T>(37,2)*z[4] - static_cast<T>(17)- z[12];
    z[9]=n<T>(1,8)*z[9] + z[10];
    z[9]=z[2]*z[9];
    z[10]=3*z[6];
    z[11]=static_cast<T>(1)- z[5];
    z[11]=z[11]*z[10];
    z[12]=n<T>(35,8)*z[8];
    z[13]=z[12] - z[10];
    z[13]=z[4]*z[13];
    z[11]=z[13] - n<T>(11,8) + z[11];
    z[11]=z[3]*z[11];
    z[10]=z[11] + z[12] + z[10];
    z[10]=z[10]*z[14];

    r += z[9] + z[10];
 
    return r;
}

template double qqb_2lha_r167(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r167(const std::array<dd_real,30>&);
#endif
