#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r844(const std::array<T,30>& k) {
  T z[36];
  T r = 0;

    z[1]=k[2];
    z[2]=k[5];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[11];
    z[6]=k[24];
    z[7]=k[9];
    z[8]=k[3];
    z[9]=k[13];
    z[10]=k[27];
    z[11]=k[15];
    z[12]=k[6];
    z[13]=z[1] - 3;
    z[13]=z[13]*z[1];
    z[13]=z[13] + 3;
    z[13]=z[13]*z[1];
    z[13]=z[13] - 1;
    z[13]=z[13]*z[3];
    z[14]=z[8]*z[9];
    z[15]= - static_cast<T>(1)+ 3*z[14];
    z[15]=z[13] - n<T>(5,2)*z[15] - z[7];
    z[14]= - static_cast<T>(5)+ n<T>(3,2)*z[14];
    z[14]=z[8]*z[14];
    z[16]=3*z[10];
    z[17]= - npow(z[8],2)*z[16];
    z[14]=z[14] + z[17];
    z[14]=z[10]*z[14];
    z[17]=n<T>(1,2)*z[1];
    z[18]=z[1] + static_cast<T>(1)- z[7];
    z[18]=z[18]*z[17];
    z[14]=z[18] + z[14] - n<T>(1,2)*z[15];
    z[15]=n<T>(3,2)*z[10];
    z[18]= - z[15] - 1;
    z[19]=z[5] + z[7];
    z[18]=z[10]*z[19]*z[18];
    z[20]=z[11] + 1;
    z[21]=z[7] + z[20];
    z[22]=z[7]*z[17];
    z[18]=z[22] + n<T>(1,2)*z[21] + z[18];
    z[21]=z[5] + z[11];
    z[22]=n<T>(1,2)*z[12];
    z[23]= - n<T>(1,2)*z[9] + static_cast<T>(3)- z[22];
    z[23]=n<T>(1,2)*z[23] - z[21];
    z[24]=n<T>(1,4)*z[6];
    z[25]=z[24] + n<T>(1,4)*z[5];
    z[26]=z[1] - 1;
    z[27]=z[3]*z[26];
    z[27]=n<T>(3,2)*z[27] - static_cast<T>(1)- z[25];
    z[27]=z[3]*z[27];
    z[23]=z[27] - z[1] + n<T>(1,2)*z[23] - z[10];
    z[23]=z[3]*z[23];
    z[27]=3*z[3];
    z[28]= - z[9] - z[20];
    z[28]=n<T>(1,2)*z[28] - z[27];
    z[28]=z[3]*z[28];
    z[20]=n<T>(1,2)*z[20];
    z[28]=z[20] + z[28];
    z[28]=z[3]*z[28];
    z[29]=n<T>(1,2)*z[7];
    z[28]= - z[29] + z[28];
    z[30]=n<T>(1,2)*z[4];
    z[28]=z[28]*z[30];
    z[18]=z[28] + n<T>(1,2)*z[18] + z[23];
    z[18]=z[4]*z[18];
    z[23]=z[5] - 1;
    z[22]= - z[22] - z[23];
    z[28]=z[26]*z[1];
    z[31]=static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[31]=z[1]*z[31];
    z[31]= - n<T>(5,4) + z[31];
    z[31]=z[3]*z[31];
    z[22]=z[31] + n<T>(1,4)*z[22] + z[28];
    z[22]=z[3]*z[22];
    z[31]=n<T>(1,2)*z[5];
    z[32]=z[31] - 1;
    z[29]= - z[29] - z[8] - z[32];
    z[29]=z[29]*z[16];
    z[29]=z[29] + z[31] - n<T>(7,2) - z[7];
    z[29]=z[10]*z[29];
    z[33]=3*z[7] - z[6] + 5*z[9] - z[11];
    z[29]= - z[1] + n<T>(1,2)*z[33] + z[29];
    z[18]=z[18] + n<T>(1,2)*z[29] + z[22];
    z[18]=z[4]*z[18];
    z[14]=n<T>(1,2)*z[14] + z[18];
    z[14]=z[2]*z[14];
    z[18]=n<T>(1,2)*z[6];
    z[22]=3*z[28] + z[18] + z[32];
    z[28]=z[17] - 1;
    z[28]=z[28]*z[1];
    z[29]=z[28] + n<T>(1,2);
    z[32]=z[29]*z[27];
    z[22]=n<T>(1,2)*z[22] + z[32];
    z[22]=z[3]*z[22];
    z[33]=n<T>(1,8)*z[5];
    z[22]=z[22] + n<T>(5,4)*z[1] + z[33] - z[24] - static_cast<T>(1)+ n<T>(1,16)*z[12];
    z[22]=z[3]*z[22];
    z[24]=n<T>(1,2)*z[10];
    z[24]=z[24]*z[19];
    z[34]= - z[16] - 1;
    z[34]=z[34]*z[24];
    z[27]= - z[26]*z[27];
    z[35]= - n<T>(7,2)*z[1] - z[5] - n<T>(1,2) - z[6];
    z[27]=n<T>(1,2)*z[35] + z[27];
    z[27]=z[3]*z[27];
    z[21]= - static_cast<T>(3)- z[21];
    z[21]=z[27] + n<T>(1,2)*z[21] - z[10];
    z[21]=z[3]*z[21];
    z[21]=z[34] + z[21];
    z[21]=z[21]*z[30];
    z[27]=static_cast<T>(1)- z[19];
    z[16]=z[27]*z[16];
    z[16]=z[16] - z[7] + n<T>(7,2)*z[5];
    z[16]=z[10]*z[16];
    z[16]= - z[20] + z[16];
    z[16]=z[21] + n<T>(1,4)*z[16] + z[22];
    z[16]=z[4]*z[16];
    z[20]= - static_cast<T>(11)+ z[12];
    z[20]=n<T>(1,2)*z[20] + z[5];
    z[20]= - n<T>(5,8)*z[13] + n<T>(1,8)*z[20] - z[28];
    z[20]=z[3]*z[20];
    z[21]=z[23]*z[15];
    z[17]=z[17] - z[5] + z[21];
    z[14]=n<T>(1,2)*z[14] + z[16] + n<T>(1,4)*z[17] + z[20];
    z[14]=z[2]*z[14];
    z[16]= - z[32] + z[31] + z[18] - z[26];
    z[16]=z[3]*z[16];
    z[16]=z[31] + z[16];
    z[16]=z[3]*z[16];
    z[17]=z[5] - z[24];
    z[17]=z[17]*z[15];
    z[18]=z[4]*z[19]*npow(z[10],2);
    z[16]= - n<T>(3,4)*z[18] + z[17] + z[16];
    z[16]=z[16]*z[30];
    z[17]=z[25] - 1;
    z[17]= - z[17]*z[29];
    z[13]=n<T>(3,4)*z[13] + z[17];
    z[13]=z[3]*z[13];
    z[13]=z[33] + z[13];
    z[13]=z[3]*z[13];
    z[15]= - static_cast<T>(1)+ z[15];
    z[15]=z[5]*z[15];

    r += z[13] + z[14] + n<T>(1,4)*z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r844(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r844(const std::array<dd_real,30>&);
#endif
