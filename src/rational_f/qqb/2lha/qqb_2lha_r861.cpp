#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r861(const std::array<T,30>& k) {
  T z[23];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[5];
    z[7]=k[8];
    z[8]=k[27];
    z[9]=k[13];
    z[10]=npow(z[8],2);
    z[11]=z[10]*z[5];
    z[12]= - static_cast<T>(1)- z[8];
    z[12]=z[8]*z[12];
    z[12]=z[12] + z[11];
    z[13]=n<T>(1,2)*z[5];
    z[12]=z[12]*z[13];
    z[12]=z[12] - n<T>(5,2) + z[8];
    z[14]=n<T>(1,4)*z[5];
    z[12]=z[12]*z[14];
    z[15]=3*z[4];
    z[16]= - static_cast<T>(1)+ z[5];
    z[16]=n<T>(7,2)*z[16] + z[15];
    z[17]=n<T>(1,2)*z[4];
    z[16]=z[16]*z[17];
    z[18]=z[14] - n<T>(1,4);
    z[18]=z[8]*z[18];
    z[18]=static_cast<T>(1)+ z[18];
    z[18]=z[5]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[5]*z[18];
    z[16]=z[18] + z[16];
    z[18]=n<T>(1,2)*z[9];
    z[16]=z[16]*z[18];
    z[12]=z[16] - n<T>(5,4)*z[4] + static_cast<T>(1)+ z[12];
    z[12]=z[9]*z[12];
    z[16]=z[4] - 3;
    z[19]=npow(z[4],2);
    z[16]=z[16]*z[19];
    z[20]=z[9]*z[16];
    z[21]=static_cast<T>(7)- 5*z[4];
    z[21]=z[4]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[9]*z[20];
    z[21]=z[3] - z[18];
    z[21]=z[21]*npow(z[4],3);
    z[16]= - z[16] + z[21];
    z[16]=z[9]*z[16];
    z[16]= - z[19] + z[16];
    z[21]=n<T>(1,2)*z[3];
    z[16]=z[16]*z[21];
    z[16]=z[16] - z[4] + n<T>(1,4)*z[20];
    z[16]=z[3]*z[16];
    z[20]=z[8]*z[7];
    z[22]= - z[7] - z[20];
    z[22]=z[8]*z[22];
    z[11]=z[7]*z[11];
    z[11]=z[22] + z[11];
    z[11]=z[11]*z[13];
    z[11]=z[11] - z[7] + z[20];
    z[11]=z[5]*z[11];
    z[11]= - static_cast<T>(3)+ z[11];
    z[11]=z[16] + n<T>(1,4)*z[11] + z[12];
    z[11]=z[6]*z[11];
    z[10]=z[10]*z[13];
    z[12]=z[8] - z[10];
    z[12]=z[5]*z[12];
    z[12]=n<T>(9,2) + z[12];
    z[16]=z[5]*z[8];
    z[16]= - static_cast<T>(3)- z[16];
    z[14]=z[16]*z[14];
    z[14]=z[14] - z[4];
    z[14]=z[9]*z[14];
    z[12]=n<T>(1,2)*z[12] + z[14];
    z[12]=z[9]*z[12];
    z[10]= - z[7]*z[10];
    z[10]=z[20] + z[10];
    z[10]=z[10]*z[13];
    z[10]=z[10] + z[12];
    z[12]= - z[19]*z[18];
    z[12]=z[15] + z[12];
    z[12]=z[9]*z[12];
    z[12]= - n<T>(3,2) + z[12];
    z[13]= - n<T>(3,4)*z[3] + n<T>(3,8)*z[9];
    z[13]=z[19]*z[13];
    z[13]= - z[4] + z[13];
    z[13]=z[3]*z[13];
    z[12]=n<T>(1,4)*z[12] + z[13];
    z[12]=z[3]*z[12];
    z[10]=n<T>(1,2)*z[11] + n<T>(1,4)*z[10] + z[12];
    z[10]=z[6]*z[10];
    z[11]=n<T>(1,2)*z[1];
    z[12]= - z[2]*z[11];
    z[12]=z[1] + z[12];
    z[12]=z[2]*z[12];
    z[13]=static_cast<T>(1)- z[2];
    z[13]=z[4]*z[13];
    z[12]=n<T>(3,2)*z[13] - z[11] + z[12];
    z[12]=z[3]*z[12];
    z[13]= - n<T>(3,2) - z[2];
    z[13]=z[13]*z[17];
    z[11]=z[2] - static_cast<T>(3)- z[11];
    z[11]=z[2]*z[11];
    z[11]=z[12] + z[13] + n<T>(1,2)*z[11] + static_cast<T>(1)+ n<T>(1,4)*z[1];
    z[11]=z[3]*z[11];
    z[12]=z[4] - z[2];
    z[11]=z[11] - static_cast<T>(1)- n<T>(1,4)*z[12];
    z[11]=z[11]*z[21];

    r += z[10] + z[11];
 
    return r;
}

template double qqb_2lha_r861(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r861(const std::array<dd_real,30>&);
#endif
