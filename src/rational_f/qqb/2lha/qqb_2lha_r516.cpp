#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r516(const std::array<T,30>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[4];
    z[6]=k[6];
    z[7]=k[7];
    z[8]=k[2];
    z[9]=k[11];
    z[10]=k[10];
    z[11]=k[12];
    z[12]=static_cast<T>(1)+ n<T>(1,4)*z[10];
    z[13]=3*z[10];
    z[12]=z[12]*z[13];
    z[14]=n<T>(1,2)*z[5];
    z[15]=z[13] + n<T>(7,2);
    z[15]=z[15]*z[10];
    z[16]= - n<T>(1,4) + z[15];
    z[16]=z[16]*z[14];
    z[16]=z[16] + n<T>(5,2) + z[12];
    z[16]=z[5]*z[16];
    z[17]=n<T>(1,2)*z[4];
    z[18]=npow(z[5],2);
    z[19]=z[10] + 3;
    z[20]=z[19]*z[10];
    z[21]= - n<T>(3,2) - z[20];
    z[21]=z[21]*z[18]*z[17];
    z[22]=n<T>(1,2)*z[10];
    z[23]=z[19]*z[22];
    z[23]=z[23] + 1;
    z[24]=n<T>(1,8)*z[23];
    z[16]=z[21] - z[24] + z[16];
    z[16]=z[4]*z[16];
    z[21]=z[5]*z[10];
    z[19]=z[19]*z[21];
    z[25]=z[13] + 1;
    z[25]=z[25]*z[10];
    z[26]=n<T>(5,3) - z[25];
    z[26]=n<T>(1,2)*z[26] + z[19];
    z[27]=n<T>(1,4)*z[5];
    z[26]=z[26]*z[27];
    z[28]=n<T>(4,3) - n<T>(3,16)*z[10];
    z[28]=z[10]*z[28];
    z[16]=z[16] + z[26] + n<T>(161,48) + z[28];
    z[16]=z[4]*z[16];
    z[26]=z[10] + 1;
    z[28]=z[5]*z[26]*z[13];
    z[29]=static_cast<T>(1)- z[22];
    z[29]=z[10]*z[29];
    z[28]=z[28] - n<T>(59,6) + z[29];
    z[29]=npow(z[10],2);
    z[30]=z[14] - n<T>(3,2) + z[29];
    z[30]=z[4]*z[30];
    z[31]= - n<T>(1,2) + z[10];
    z[30]=z[30] + n<T>(5,2)*z[31];
    z[30]=z[5]*z[30];
    z[31]=n<T>(11,2) + z[10];
    z[31]=z[10]*z[31];
    z[30]=n<T>(53,8) + z[31] + z[30];
    z[30]=z[4]*z[30];
    z[28]=n<T>(1,2)*z[28] + z[30];
    z[28]=z[4]*z[28];
    z[30]=z[26]*z[22];
    z[31]=z[22] + 1;
    z[31]=z[31]*z[10];
    z[32]=z[31] + n<T>(1,2);
    z[33]=3*z[32] + z[14];
    z[33]=z[4]*z[33];
    z[34]=static_cast<T>(1)- z[10];
    z[34]=z[10]*z[34];
    z[34]= - n<T>(11,2) + z[34];
    z[33]=n<T>(1,2)*z[34] + z[33];
    z[33]=z[4]*z[33];
    z[33]=z[30] + z[33];
    z[33]=z[4]*z[33];
    z[34]=z[2]*npow(z[4],3);
    z[33]=z[33] - n<T>(1,2)*z[34];
    z[33]=z[2]*z[33];
    z[28]=z[28] + z[33];
    z[33]=n<T>(1,2)*z[2];
    z[28]=z[28]*z[33];
    z[35]=static_cast<T>(9)+ z[10];
    z[35]=z[10]*z[35];
    z[35]=static_cast<T>(15)+ z[35];
    z[35]=z[4]*z[35];
    z[35]= - static_cast<T>(15)+ z[35];
    z[35]=z[35]*z[17];
    z[36]=npow(z[4],2);
    z[20]=static_cast<T>(3)+ z[20];
    z[20]=z[4]*z[20];
    z[20]= - n<T>(7,2) + z[20];
    z[20]=z[20]*z[36];
    z[20]=z[20] - z[34];
    z[20]=z[2]*z[20];
    z[20]=z[35] + z[20];
    z[20]=z[2]*z[20];
    z[35]=n<T>(23,8) + z[10];
    z[35]=z[4]*z[35];
    z[20]=n<T>(1,4)*z[20] - n<T>(5,4) + z[35];
    z[20]=z[2]*z[20];
    z[34]=n<T>(7,2)*z[36] + z[34];
    z[34]=z[2]*z[34];
    z[34]=n<T>(15,2)*z[4] + z[34];
    z[34]=z[2]*z[34];
    z[34]=static_cast<T>(5)+ z[34];
    z[34]=z[1]*z[34];
    z[20]=n<T>(1,4)*z[34] + n<T>(13,8) + z[20];
    z[20]=z[3]*z[20];
    z[34]=z[14]*z[29];
    z[35]=z[34] - n<T>(11,6) - z[13];
    z[16]=z[20] + z[28] + n<T>(1,4)*z[35] + z[16];
    z[16]=z[2]*z[16];
    z[20]=z[10] + n<T>(1,2);
    z[28]=z[20]*z[10];
    z[28]=z[28] + z[34];
    z[28]=z[28]*z[5];
    z[28]=z[28] + z[30];
    z[28]=z[28]*z[2];
    z[35]=n<T>(1,3)*z[10];
    z[37]=static_cast<T>(35)+ 11*z[10];
    z[37]=z[37]*z[35];
    z[37]=static_cast<T>(5)+ z[37];
    z[38]=z[29]*z[5];
    z[39]=static_cast<T>(11)+ n<T>(49,4)*z[10];
    z[39]=z[10]*z[39];
    z[39]=z[39] + n<T>(29,4)*z[38];
    z[40]=n<T>(1,3)*z[5];
    z[39]=z[39]*z[40];
    z[37]=n<T>(1,4)*z[37] + z[39];
    z[37]=z[5]*z[37];
    z[39]=static_cast<T>(9)+ n<T>(17,3)*z[10];
    z[39]=z[10]*z[39];
    z[39]=z[39] + n<T>(13,3)*z[38];
    z[39]=z[39]*z[27];
    z[41]=n<T>(3,2) + z[35];
    z[41]=z[10]*z[41];
    z[39]=z[39] + n<T>(7,6) + z[41];
    z[39]=z[39]*z[18];
    z[41]= - z[10]*z[26];
    z[41]=z[41] - z[34];
    z[41]=z[5]*z[41];
    z[41]=z[41] - z[32];
    z[42]=npow(z[5],3);
    z[41]=z[7]*z[41]*z[42];
    z[39]=z[39] + n<T>(1,6)*z[41];
    z[39]=z[7]*z[39];
    z[41]=n<T>(3,4)*z[10];
    z[26]=z[41]*z[26];
    z[37]=z[39] + n<T>(5,2)*z[28] - z[26] + z[37];
    z[37]=z[11]*z[37];
    z[39]= - n<T>(5,2) - z[10];
    z[39]=z[10]*z[39];
    z[39]=z[39] - n<T>(9,8)*z[38];
    z[39]=z[5]*z[39];
    z[43]= - n<T>(61,8) - z[10];
    z[43]=z[10]*z[43];
    z[43]= - n<T>(73,8) + z[43];
    z[39]=n<T>(1,3)*z[43] + z[39];
    z[39]=z[39]*z[18];
    z[43]=n<T>(17,2) + z[13];
    z[43]=z[10]*z[43];
    z[43]=z[43] + n<T>(13,2)*z[38];
    z[43]=z[43]*z[42];
    z[44]=z[29]*npow(z[5],4);
    z[45]= - z[4]*z[44];
    z[43]=n<T>(1,2)*z[43] + z[45];
    z[43]=z[43]*z[17];
    z[39]=z[39] + z[43];
    z[39]=z[4]*z[39];
    z[43]= - static_cast<T>(5)+ z[22];
    z[35]=z[43]*z[35];
    z[35]=z[35] - z[34];
    z[35]=z[35]*z[42];
    z[43]=n<T>(1,4)*z[4];
    z[44]=z[44]*z[43];
    z[35]=z[35] + z[44];
    z[17]=z[35]*z[17];
    z[35]=z[10] + n<T>(5,24)*z[38];
    z[35]=z[5]*z[35];
    z[35]=n<T>(19,24) + z[35];
    z[18]=z[35]*z[18];
    z[17]=z[18] + z[17];
    z[17]=z[7]*z[17];
    z[18]= - n<T>(13,2)*z[10] - 11*z[38];
    z[18]=z[5]*z[18];
    z[18]= - n<T>(43,2) + z[18];
    z[18]=z[5]*z[18];
    z[17]=z[17] + n<T>(1,12)*z[18] + z[39];
    z[17]=z[7]*z[17];
    z[17]=z[17] + z[37];
    z[18]=static_cast<T>(5)+ z[10];
    z[18]=z[18]*z[22];
    z[18]=static_cast<T>(1)+ z[18];
    z[35]=z[10] + n<T>(3,2);
    z[37]=z[35]*z[21];
    z[39]=z[2]*z[30];
    z[18]=z[39] + n<T>(1,2)*z[18] + z[37];
    z[18]=z[18]*z[33];
    z[33]=static_cast<T>(1)+ n<T>(1,8)*z[10];
    z[33]=z[10]*z[33];
    z[19]=n<T>(1,4)*z[19] + n<T>(3,4) + z[33];
    z[19]=z[5]*z[19];
    z[33]=z[41] + 1;
    z[37]=z[10]*z[33];
    z[37]=n<T>(3,4) + z[37];
    z[18]=z[18] + n<T>(1,2)*z[37] + z[19];
    z[18]=z[2]*z[18];
    z[19]=z[22]*z[5];
    z[22]=z[19] + z[33];
    z[22]=z[5]*z[22];
    z[33]=z[10] + n<T>(3,4);
    z[22]=z[22] + z[33];
    z[22]=z[5]*z[22];
    z[22]= - z[26] + z[22];
    z[30]=z[5]*z[30];
    z[30]=z[30] + z[32];
    z[30]=z[11]*z[30];
    z[32]= - z[5] - z[2];
    z[32]=z[8]*z[32];
    z[18]=n<T>(1,4)*z[32] + n<T>(3,4)*z[30] + n<T>(1,2)*z[22] + z[18];
    z[18]=z[9]*z[18];
    z[22]=n<T>(3,4) + 2*z[10];
    z[21]=z[22]*z[21];
    z[22]=n<T>(9,2) + z[10];
    z[22]=z[10]*z[22];
    z[22]=n<T>(17,8) + z[22];
    z[21]=n<T>(1,2)*z[22] + z[21];
    z[21]=z[5]*z[21];
    z[22]=z[10] + n<T>(9,4);
    z[22]=z[22]*z[10];
    z[22]=z[22] + 1;
    z[21]= - n<T>(1,4)*z[22] + z[21];
    z[21]=z[5]*z[21];
    z[26]= - z[4]*z[42]*z[26];
    z[21]=z[26] - z[24] + z[21];
    z[21]=z[4]*z[21];
    z[24]=static_cast<T>(1)- z[13];
    z[19]=z[24]*z[19];
    z[12]=z[19] + n<T>(19,12) - z[12];
    z[12]=z[5]*z[12];
    z[19]= - n<T>(7,4) - z[10];
    z[19]=z[10]*z[19];
    z[19]= - n<T>(47,4) + z[19];
    z[12]=n<T>(1,6)*z[19] + z[12];
    z[12]=z[5]*z[12];
    z[19]=z[31] + n<T>(1,4);
    z[12]=n<T>(1,2)*z[19] + z[12];
    z[12]=n<T>(1,2)*z[12] + z[21];
    z[12]=z[4]*z[12];
    z[21]=z[35]*z[13];
    z[21]=static_cast<T>(1)+ z[21];
    z[24]=z[29]*z[27];
    z[26]=z[10]*z[33];
    z[24]=z[26] + z[24];
    z[24]=z[5]*z[24];
    z[21]=n<T>(1,2)*z[21] + z[24];
    z[21]=z[5]*z[21];
    z[21]=z[21] + z[22];
    z[21]=z[5]*z[21];
    z[21]=n<T>(1,2)*z[23] + z[21];
    z[21]=z[21]*z[43];
    z[22]= - z[25] - z[34];
    z[22]=z[22]*z[14];
    z[23]= - n<T>(11,4) - z[13];
    z[23]=z[10]*z[23];
    z[22]=z[22] - n<T>(1,4) + z[23];
    z[22]=z[22]*z[27];
    z[23]= - static_cast<T>(1)- n<T>(5,8)*z[10];
    z[23]=z[10]*z[23];
    z[22]=z[22] - n<T>(1,4) + z[23];
    z[22]=z[5]*z[22];
    z[23]= - static_cast<T>(7)- z[13];
    z[23]=z[10]*z[23];
    z[23]= - static_cast<T>(3)+ z[23];
    z[21]=z[21] + n<T>(1,16)*z[23] + z[22];
    z[21]=z[4]*z[21];
    z[13]=z[20]*z[13];
    z[13]=z[13] + z[38];
    z[13]=z[5]*z[13];
    z[13]=z[13] + n<T>(1,2) + z[15];
    z[13]=z[13]*z[14];
    z[13]=z[13] + z[19];
    z[13]=n<T>(1,8)*z[28] + n<T>(1,4)*z[13] + z[21];
    z[13]=z[6]*z[13];
    z[14]=n<T>(1,8)*z[4];
    z[15]= - n<T>(1,2) + z[5];
    z[15]=z[4]*z[15];
    z[15]=n<T>(41,3) + z[15];
    z[15]=z[15]*z[14];
    z[19]= - z[4]*z[27];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=z[2]*z[19]*z[36];
    z[15]=z[15] + z[19];
    z[15]=z[2]*z[15];
    z[19]=z[5] + 1;
    z[14]= - z[19]*z[14];
    z[14]= - z[40] + z[14];
    z[14]=z[4]*z[14];
    z[14]=n<T>(17,12) + z[14];
    z[19]=z[4]*z[19];
    z[19]= - static_cast<T>(1)+ z[19];
    z[19]=z[6]*z[4]*z[19];
    z[20]=z[7]*z[5];
    z[14]= - n<T>(13,24)*z[20] + n<T>(1,16)*z[19] + n<T>(1,2)*z[14] + z[15];
    z[14]=z[1]*z[14];
    z[15]=static_cast<T>(19)- n<T>(73,2)*z[10];
    z[15]=n<T>(1,3)*z[15] - n<T>(7,2)*z[38];
    z[15]=z[5]*z[15];

    r += n<T>(1,3) + z[12] + z[13] + z[14] + n<T>(1,8)*z[15] + z[16] + n<T>(1,2)*
      z[17] + z[18];
 
    return r;
}

template double qqb_2lha_r516(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r516(const std::array<dd_real,30>&);
#endif
