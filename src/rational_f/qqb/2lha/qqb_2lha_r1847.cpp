#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1847(const std::array<T,30>& k) {
  T z[46];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[6];
    z[5]=k[17];
    z[6]=k[8];
    z[7]=k[4];
    z[8]=k[2];
    z[9]=k[10];
    z[10]=k[12];
    z[11]=n<T>(1,2)*z[5];
    z[12]=z[11] - 1;
    z[13]=3*z[5];
    z[14]=z[12]*z[13];
    z[15]=n<T>(3,2)*z[4];
    z[16]=n<T>(1,2)*z[4];
    z[17]= - static_cast<T>(1)- z[16];
    z[17]=z[17]*z[15];
    z[17]=z[17] - n<T>(1,2) - z[14];
    z[18]=3*z[7];
    z[17]=z[17]*z[18];
    z[19]=z[5] - 1;
    z[20]=z[19]*z[13];
    z[21]=z[1] + 3;
    z[22]= - z[21]*z[20];
    z[23]=n<T>(3,2)*z[2];
    z[24]=n<T>(1,2)*z[2];
    z[25]= - n<T>(1,2) - z[24] - z[1];
    z[25]=z[25]*z[23];
    z[26]=3*z[4];
    z[27]= - z[21]*z[26];
    z[28]=3*z[1];
    z[29]=z[28] + 5;
    z[27]=z[27] - z[29];
    z[27]=z[27]*z[16];
    z[17]=z[17] + z[27] + z[25] - static_cast<T>(2)+ z[22];
    z[17]=z[7]*z[17];
    z[22]=z[5] - 3;
    z[22]=z[22]*z[5];
    z[25]= - z[23] - n<T>(5,4) - z[22];
    z[27]= - static_cast<T>(7)- z[26];
    z[27]=z[27]*z[16];
    z[25]=3*z[25] + z[27];
    z[27]=z[11] - 2;
    z[27]=z[27]*z[5];
    z[30]=n<T>(1,4)*z[4];
    z[31]= - static_cast<T>(1)- z[30];
    z[31]=z[4]*z[31];
    z[31]= - z[27] + z[31];
    z[31]=z[31]*z[18];
    z[32]=2*z[5];
    z[33]=static_cast<T>(7)- z[32];
    z[33]=z[5]*z[33];
    z[33]= - n<T>(7,2) + z[33];
    z[34]= - n<T>(19,2) - z[26];
    z[34]=z[4]*z[34];
    z[31]=z[31] + 3*z[33] + z[34];
    z[31]=z[7]*z[31];
    z[25]=3*z[25] + z[31];
    z[25]=z[7]*z[25];
    z[31]= - n<T>(3,2) - 5*z[2];
    z[31]=z[31]*z[24];
    z[33]=z[32] - 5;
    z[33]=z[33]*z[5];
    z[34]= - n<T>(3,2) - z[4];
    z[34]=z[4]*z[34];
    z[31]=z[34] + z[31] - n<T>(5,2) - z[33];
    z[25]=3*z[31] + z[25];
    z[25]=z[7]*z[25];
    z[31]= - static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[31]=z[31]*z[23];
    z[31]=static_cast<T>(2)+ z[31];
    z[31]=z[2]*z[31];
    z[31]= - static_cast<T>(1)+ z[31];
    z[31]=z[2]*z[31];
    z[34]=z[15] + 1;
    z[35]= - z[34]*z[16];
    z[14]=z[25] + z[35] + z[31] - n<T>(7,4) - z[14];
    z[14]=z[9]*z[14];
    z[25]= - static_cast<T>(5)- z[26];
    z[25]=z[25]*z[26];
    z[31]= - n<T>(5,2) - z[4];
    z[31]=z[4]*z[31];
    z[31]=z[31] - n<T>(1,2) - z[33];
    z[31]=z[31]*z[18];
    z[33]=z[5] - 2;
    z[33]=z[33]*z[5];
    z[25]=z[31] + z[25] - n<T>(15,4)*z[2] - n<T>(53,4) - 18*z[33];
    z[25]=z[7]*z[25];
    z[31]=static_cast<T>(3)- z[32];
    z[31]=z[31]*z[13];
    z[31]= - n<T>(7,2) + z[31];
    z[35]= - n<T>(5,2) - z[26];
    z[35]=z[35]*z[26];
    z[36]=3*z[2];
    z[37]= - n<T>(1,4) - z[36];
    z[37]=z[2]*z[37];
    z[25]=z[25] + z[35] + 3*z[31] + z[37];
    z[25]=z[7]*z[25];
    z[31]=z[19]*z[32];
    z[35]=npow(z[4],2);
    z[37]=z[35] + static_cast<T>(1)+ z[31];
    z[38]= - static_cast<T>(1)+ 5*z[6];
    z[38]=z[38]*z[36];
    z[38]=static_cast<T>(37)+ z[38];
    z[39]=npow(z[2],2);
    z[38]=z[38]*z[39];
    z[14]=z[14] + z[25] + n<T>(1,4)*z[38] - 3*z[37];
    z[14]=z[9]*z[14];
    z[25]=z[4] + 1;
    z[37]= - z[32] + z[25];
    z[37]=z[21]*z[37];
    z[38]=z[24] - n<T>(3,2);
    z[40]= - z[1] - z[38];
    z[40]=z[40]*z[24];
    z[21]=z[40] + z[21];
    z[21]=z[2]*z[21];
    z[40]=z[16] - z[5];
    z[41]=z[40] + n<T>(1,2);
    z[42]=z[41]*z[18];
    z[21]=z[42] + z[21] + z[37];
    z[21]=z[7]*z[21];
    z[32]=z[32] - 1;
    z[37]=2*z[4];
    z[42]= - z[37] + 2*z[32];
    z[43]=static_cast<T>(2)+ n<T>(9,4)*z[2];
    z[43]=z[2]*z[43];
    z[43]=z[43] - z[42];
    z[44]=n<T>(7,4)*z[2] - z[42];
    z[37]= - z[37] + 4*z[5];
    z[45]=n<T>(3,2) - z[37];
    z[45]=z[7]*z[45];
    z[44]=3*z[44] + z[45];
    z[44]=z[7]*z[44];
    z[43]=3*z[43] + z[44];
    z[43]=z[7]*z[43];
    z[40]=z[7]*z[40];
    z[37]=z[40] + n<T>(9,4) - z[37];
    z[37]=z[7]*z[37];
    z[32]=z[37] + z[26] - 3*z[32] + 4*z[2];
    z[32]=z[7]*z[32];
    z[37]=static_cast<T>(2)+ n<T>(7,2)*z[2];
    z[37]=z[2]*z[37];
    z[32]=z[32] + z[37] - z[42];
    z[32]=z[7]*z[32];
    z[37]=z[6]*z[24];
    z[37]=static_cast<T>(3)+ z[37];
    z[37]=z[2]*z[37];
    z[37]=static_cast<T>(1)+ z[37];
    z[37]=z[2]*z[37];
    z[37]=static_cast<T>(1)+ z[37];
    z[37]=z[37]*z[24];
    z[32]=z[32] + z[37] + z[41];
    z[32]=z[9]*z[32];
    z[37]=z[2]*z[6];
    z[40]=static_cast<T>(5)+ z[37];
    z[40]=z[2]*z[40];
    z[40]=static_cast<T>(2)+ n<T>(3,4)*z[40];
    z[40]=z[2]*z[40];
    z[40]=static_cast<T>(2)+ z[40];
    z[40]=z[2]*z[40];
    z[32]=z[32] + z[43] + z[40] - z[42];
    z[32]=z[9]*z[32];
    z[40]=n<T>(1,2)*z[1];
    z[41]=z[40] + 1;
    z[41]=z[41]*z[1];
    z[41]=z[41] + n<T>(3,2);
    z[42]=z[41]*z[25];
    z[38]=z[40] - z[38];
    z[38]=z[6]*z[38];
    z[38]= - static_cast<T>(1)+ z[38];
    z[38]=z[2]*z[38];
    z[38]=z[38] + static_cast<T>(3)- z[40];
    z[38]=z[38]*z[24];
    z[38]=z[38] + z[41];
    z[38]=z[2]*z[38];
    z[38]=z[38] + z[41];
    z[38]=z[2]*z[38];
    z[43]= - static_cast<T>(2)- z[1];
    z[43]=z[1]*z[43];
    z[43]= - static_cast<T>(3)+ z[43];
    z[43]=z[5]*z[43];
    z[21]=z[32] + z[21] + z[38] + z[43] + z[42];
    z[21]=z[3]*z[21];
    z[29]=z[29]*z[40];
    z[32]= - z[41]*z[15];
    z[29]=z[32] + static_cast<T>(2)+ z[29];
    z[29]=z[4]*z[29];
    z[32]=z[1] + 1;
    z[32]=z[32]*z[1];
    z[38]= - z[5]*z[41];
    z[38]= - z[32] + z[38];
    z[38]=z[38]*z[13];
    z[41]=z[1]*z[6];
    z[37]=z[41] - z[37];
    z[42]= - static_cast<T>(1)+ n<T>(7,4)*z[6] + n<T>(3,4)*z[37];
    z[42]=z[2]*z[42];
    z[42]=z[42] + static_cast<T>(2)- z[40];
    z[42]=z[42]*z[36];
    z[28]=n<T>(7,2) + z[28];
    z[28]=z[1]*z[28];
    z[28]=z[42] + n<T>(5,2) + z[28];
    z[28]=z[2]*z[28];
    z[42]=static_cast<T>(2)+ n<T>(9,4)*z[1];
    z[42]=z[1]*z[42];
    z[14]=3*z[21] + z[14] + z[17] + z[29] + z[28] + z[38] + n<T>(1,4) + 
    z[42];
    z[14]=z[3]*z[14];
    z[17]=n<T>(5,2)*z[4];
    z[21]=static_cast<T>(1)+ z[17];
    z[17]=z[21]*z[17];
    z[21]=z[25]*z[16];
    z[19]=z[19]*z[5];
    z[21]=z[19] + z[21];
    z[21]=z[21]*z[18];
    z[25]= - static_cast<T>(3)+ n<T>(5,2)*z[5];
    z[25]=z[5]*z[25];
    z[25]=n<T>(5,4) + z[25];
    z[17]=z[21] + 3*z[25] + z[17];
    z[17]=z[7]*z[17];
    z[21]=n<T>(3,4)*z[4];
    z[25]= - static_cast<T>(3)+ 13*z[4];
    z[25]=z[25]*z[21];
    z[28]=static_cast<T>(5)+ 9*z[19];
    z[17]=z[17] + z[25] + n<T>(1,2)*z[28] + z[36];
    z[17]=z[7]*z[17];
    z[25]= - static_cast<T>(5)+ z[23];
    z[25]=z[25]*z[24];
    z[28]= - static_cast<T>(2)+ n<T>(9,4)*z[4];
    z[28]=z[28]*z[26];
    z[27]= - n<T>(1,4) - z[27];
    z[17]=z[17] + z[28] + 3*z[27] + z[25];
    z[17]=z[7]*z[17];
    z[25]= - static_cast<T>(11)+ 7*z[4];
    z[27]=z[25]*z[16];
    z[22]= - static_cast<T>(1)- z[22];
    z[28]= - n<T>(5,2) - z[36];
    z[28]=z[2]*z[28];
    z[22]=z[27] + 3*z[22] + z[28];
    z[17]=n<T>(1,2)*z[22] + z[17];
    z[17]=z[9]*z[17];
    z[22]= - static_cast<T>(1)+ 19*z[4];
    z[22]=z[22]*z[16];
    z[20]=z[22] + n<T>(5,2) + z[20];
    z[22]=z[4]*z[34];
    z[22]=z[31] + z[22];
    z[22]=z[7]*z[22];
    z[20]=n<T>(1,2)*z[20] + z[22];
    z[20]=z[20]*z[18];
    z[22]= - static_cast<T>(4)+ 5*z[4];
    z[22]=z[22]*z[26];
    z[20]=z[20] + z[22] - n<T>(17,4) - 9*z[33];
    z[20]=z[7]*z[20];
    z[22]=static_cast<T>(11)- 5*z[5];
    z[22]=z[22]*z[11];
    z[22]= - static_cast<T>(2)+ z[22];
    z[26]= - n<T>(1,2) + z[6];
    z[26]=z[26]*z[23];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[2]*z[26];
    z[25]=z[25]*z[21];
    z[17]=z[17] + z[20] + z[25] + 3*z[22] + z[26];
    z[17]=z[9]*z[17];
    z[20]=n<T>(3,2) - z[5];
    z[20]=z[20]*z[13];
    z[22]=n<T>(1,2) + z[4];
    z[22]=z[4]*z[22];
    z[19]=z[19] + z[22];
    z[18]=z[19]*z[18];
    z[19]=static_cast<T>(5)+ n<T>(3,2)*z[1];
    z[19]=z[4]*z[19];
    z[19]= - n<T>(13,4) + z[19];
    z[19]=z[4]*z[19];
    z[18]=z[18] + z[20] + z[19];
    z[18]=z[7]*z[18];
    z[19]=static_cast<T>(5)+ z[1];
    z[20]= - n<T>(3,2) - z[1];
    z[20]=z[1]*z[20];
    z[20]= - static_cast<T>(2)+ z[20];
    z[20]=z[5]*z[20];
    z[19]=n<T>(1,2)*z[19] + z[20];
    z[19]=z[19]*z[13];
    z[20]= - z[23] + n<T>(5,2);
    z[20]=z[6]*z[20];
    z[20]=n<T>(3,2)*z[41] - static_cast<T>(1)+ z[20];
    z[20]=z[2]*z[20];
    z[20]=z[20] + static_cast<T>(1)- z[40];
    z[20]=z[20]*z[23];
    z[22]= - static_cast<T>(11)+ z[1];
    z[25]=static_cast<T>(2)+ n<T>(1,4)*z[1];
    z[25]=z[4]*z[25];
    z[22]=n<T>(1,4)*z[22] + z[25];
    z[22]=z[4]*z[22];
    z[25]=npow(z[1],2);
    z[25]= - n<T>(11,2) + 3*z[25];
    z[14]=z[14] + z[17] + z[18] + z[22] + z[20] + n<T>(1,2)*z[25] + z[19];
    z[14]=z[3]*z[14];
    z[17]=npow(z[5],2);
    z[18]=npow(z[10],2);
    z[19]=n<T>(1,3)*z[18];
    z[17]=3*z[17] + z[19] + n<T>(3,2)*z[35];
    z[20]= - z[7]*z[17];
    z[22]=n<T>(1,3)*z[10];
    z[25]=n<T>(5,4) - z[10];
    z[25]=z[25]*z[22];
    z[26]=n<T>(1,2) + z[8];
    z[26]=z[5]*z[26];
    z[26]= - n<T>(3,2) + z[26];
    z[26]=z[26]*z[13];
    z[27]= - static_cast<T>(21)- z[2];
    z[27]=z[27]*z[16];
    z[27]=static_cast<T>(3)+ z[27];
    z[27]=z[27]*z[16];
    z[19]=z[19]*z[2];
    z[20]=z[20] + z[27] - z[19] + z[25] + z[26];
    z[20]=z[7]*z[20];
    z[25]=n<T>(1,2)*z[8];
    z[26]=static_cast<T>(1)+ z[25];
    z[26]=z[5]*z[26];
    z[25]=z[26] - static_cast<T>(2)- z[25];
    z[25]=z[25]*z[13];
    z[26]=n<T>(1,3)*z[2];
    z[27]=n<T>(1,4) - z[10];
    z[27]=z[10]*z[27]*z[26];
    z[28]=static_cast<T>(9)+ z[24];
    z[29]= - static_cast<T>(6)- z[24];
    z[29]=z[4]*z[29];
    z[28]=n<T>(1,2)*z[28] + z[29];
    z[28]=z[4]*z[28];
    z[29]=n<T>(1,2)*z[10];
    z[31]=static_cast<T>(13)+ z[29];
    z[20]=z[20] + z[28] + z[27] + n<T>(1,6)*z[31] + z[25];
    z[20]=z[7]*z[20];
    z[25]=n<T>(1,2)*z[7];
    z[17]=z[25]*z[17];
    z[25]=z[10] - 1;
    z[22]=z[25]*z[22];
    z[19]=z[22] + z[19];
    z[22]=n<T>(3,2)*z[5];
    z[25]=z[5]*z[8];
    z[25]= - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[22];
    z[27]= - static_cast<T>(13)- z[2];
    z[27]=z[4]*z[27];
    z[27]=static_cast<T>(3)+ z[27];
    z[27]=z[27]*z[30];
    z[25]= - z[17] + z[27] + z[25] - z[19];
    z[25]=z[7]*z[25];
    z[27]=static_cast<T>(3)+ z[8];
    z[27]=z[5]*z[27];
    z[27]=z[27] - static_cast<T>(5)- z[8];
    z[22]=z[27]*z[22];
    z[27]= - z[18]*z[24];
    z[28]=static_cast<T>(1)- 2*z[10];
    z[28]=z[10]*z[28];
    z[27]=z[28] + z[27];
    z[26]=z[27]*z[26];
    z[27]= - static_cast<T>(7)- z[2];
    z[21]=z[27]*z[21];
    z[27]=n<T>(1,4)*z[2];
    z[28]=z[27] + 3;
    z[21]=z[21] + z[28];
    z[21]=z[4]*z[21];
    z[29]=static_cast<T>(1)- z[29];
    z[29]=z[10]*z[29];
    z[29]=n<T>(25,4) + z[29];
    z[21]=z[25] + z[21] + z[26] + n<T>(1,3)*z[29] + z[22];
    z[21]=z[7]*z[21];
    z[19]=n<T>(3,4) - z[19];
    z[19]=z[2]*z[19];
    z[22]= - static_cast<T>(5)- z[2];
    z[15]=z[22]*z[15];
    z[15]=z[15] + n<T>(15,2) + z[2];
    z[15]=z[15]*z[16];
    z[22]=static_cast<T>(1)+ z[33];
    z[15]=z[21] + z[15] + 3*z[22] + z[19];
    z[15]=z[7]*z[15];
    z[18]=z[18]*z[39];
    z[19]=static_cast<T>(3)+ z[24];
    z[21]= - static_cast<T>(1)- z[27];
    z[21]=z[4]*z[21];
    z[19]=n<T>(1,2)*z[19] + z[21];
    z[19]=z[4]*z[19];
    z[15]=z[15] - n<T>(1,6)*z[18] + z[19];
    z[15]=z[9]*z[15];
    z[18]= - static_cast<T>(9)- z[2];
    z[18]=z[18]*z[30];
    z[18]=z[18] + z[28];
    z[18]=z[4]*z[18];
    z[12]= - z[5]*z[12];
    z[12]= - n<T>(1,2) + z[12];
    z[19]= - z[10]*z[27];
    z[12]=z[15] + z[20] + z[18] + 3*z[12] + z[19];
    z[12]=z[9]*z[12];
    z[15]= - static_cast<T>(1)- z[32];
    z[15]=z[5]*z[15];
    z[15]=static_cast<T>(1)+ z[15];
    z[15]=z[15]*z[13];
    z[18]=z[6] + z[37];
    z[18]=z[18]*z[23];
    z[19]=static_cast<T>(1)- z[4];
    z[19]=z[19]*z[16];
    z[15]=z[19] + z[18] - n<T>(5,6) + z[15];
    z[18]=static_cast<T>(1)+ z[8];
    z[11]=z[18]*z[11];
    z[11]= - static_cast<T>(1)+ z[11];
    z[11]=z[11]*z[13];
    z[13]=n<T>(3,2) - z[4];
    z[13]=z[13]*z[16];
    z[11]= - z[17] + z[13] + n<T>(1,12)*z[10] + z[11];
    z[11]=z[7]*z[11];

    r += z[11] + z[12] + z[14] + n<T>(1,2)*z[15];
 
    return r;
}

template double qqb_2lha_r1847(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1847(const std::array<dd_real,30>&);
#endif
