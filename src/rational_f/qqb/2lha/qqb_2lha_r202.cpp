#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r202(const std::array<T,30>& k) {
  T z[54];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[8];
    z[5]=k[17];
    z[6]=k[3];
    z[7]=k[13];
    z[8]=k[20];
    z[9]=k[4];
    z[10]=k[12];
    z[11]=k[6];
    z[12]=k[11];
    z[13]=3*z[8];
    z[14]= - static_cast<T>(11)+ n<T>(9,2)*z[8];
    z[14]=z[8]*z[14];
    z[14]= - n<T>(11,2) + z[14];
    z[14]=z[14]*z[13];
    z[15]=z[8] + n<T>(1,2);
    z[15]=z[15]*z[13];
    z[15]=z[15] + 1;
    z[16]=n<T>(3,2)*z[8];
    z[17]=z[15]*z[16];
    z[18]=z[7] - n<T>(3,2);
    z[18]=z[18]*z[7];
    z[17]=z[17] - z[18];
    z[19]=3*z[6];
    z[20]=z[17]*z[19];
    z[21]= - n<T>(75,4) + 7*z[7];
    z[21]=z[7]*z[21];
    z[14]=z[20] + z[21] + z[14];
    z[14]=z[6]*z[14];
    z[20]=static_cast<T>(49)+ n<T>(19,3)*z[7];
    z[20]=z[7]*z[20];
    z[21]=static_cast<T>(61)- n<T>(105,2)*z[8];
    z[21]=z[8]*z[21];
    z[20]=z[21] + static_cast<T>(3)+ z[20];
    z[14]=n<T>(1,2)*z[20] + z[14];
    z[20]=n<T>(1,4)*z[6];
    z[14]=z[14]*z[20];
    z[21]=n<T>(1,2)*z[11];
    z[22]= - n<T>(3,2) - z[6];
    z[22]=z[22]*z[21];
    z[23]=n<T>(1,4)*z[3];
    z[24]=static_cast<T>(13)- 9*z[5];
    z[24]=n<T>(1,2)*z[24] - n<T>(67,3)*z[7];
    z[22]=z[22] - z[23] + n<T>(1,2)*z[24] - z[6];
    z[22]=z[22]*z[21];
    z[24]= - static_cast<T>(1)+ n<T>(1,2)*z[2];
    z[25]=z[2] - 1;
    z[26]=z[25]*z[5];
    z[27]=z[26] - z[24];
    z[27]=z[2]*z[27];
    z[27]= - n<T>(1,2) + z[27];
    z[28]=3*z[5];
    z[27]=z[27]*z[28];
    z[29]=z[2] - n<T>(1,2);
    z[29]=z[29]*z[2];
    z[30]=n<T>(11,4) - z[29];
    z[27]=n<T>(1,2)*z[30] + z[27];
    z[27]=z[5]*z[27];
    z[30]=n<T>(5,6) - z[2];
    z[30]=z[2]*z[30];
    z[30]=n<T>(1,2) + z[30];
    z[30]=z[30]*z[23];
    z[31]=n<T>(37,8)*z[8];
    z[32]= - n<T>(3,4)*z[2] - n<T>(13,4) - z[10];
    z[14]=z[22] + z[30] + z[14] + z[31] + n<T>(161,24)*z[7] + n<T>(1,4)*z[32] + 
    z[27];
    z[14]=z[4]*z[14];
    z[22]=n<T>(5,4)*z[7];
    z[27]= - static_cast<T>(1)+ n<T>(1,2)*z[7];
    z[27]=z[27]*z[22];
    z[30]=n<T>(1,8)*z[8];
    z[32]=27*z[8];
    z[33]= - static_cast<T>(19)+ z[32];
    z[33]=z[33]*z[30];
    z[33]= - static_cast<T>(1)+ z[33];
    z[33]=z[8]*z[33];
    z[34]=z[15]*z[13];
    z[35]= - static_cast<T>(3)+ n<T>(5,2)*z[7];
    z[35]=z[35]*z[7];
    z[36]= - z[35] + z[34];
    z[37]=n<T>(1,8)*z[6];
    z[36]=z[36]*z[37];
    z[27]=z[36] + z[27] + z[33];
    z[19]=z[27]*z[19];
    z[27]= - static_cast<T>(4)+ n<T>(17,8)*z[8];
    z[27]=z[27]*z[13];
    z[33]= - static_cast<T>(1)+ 37*z[8];
    z[33]=n<T>(5,2)*z[3] + n<T>(1,2)*z[33] + z[6];
    z[36]=static_cast<T>(1)- z[6];
    z[36]=z[11]*z[36];
    z[33]=n<T>(1,2)*z[33] + z[36];
    z[33]=z[11]*z[33];
    z[36]=n<T>(1,2)*z[6];
    z[38]= - static_cast<T>(3)- n<T>(1,4)*z[5];
    z[27]=z[33] - n<T>(13,8)*z[3] + z[36] + z[27] + n<T>(1,2)*z[38] - 4*z[7];
    z[27]=z[11]*z[27];
    z[33]=3*z[2];
    z[38]=z[26]*z[33];
    z[39]=n<T>(3,2)*z[2];
    z[40]= - static_cast<T>(2)+ z[39];
    z[40]=z[2]*z[40];
    z[38]=z[38] + n<T>(1,2) + z[40];
    z[38]=z[5]*z[38];
    z[40]=z[2]*z[25];
    z[38]=z[38] + n<T>(1,8) + z[40];
    z[38]=z[5]*z[38];
    z[40]=n<T>(1,8)*z[7];
    z[41]=static_cast<T>(33)- n<T>(19,6)*z[7];
    z[41]=z[41]*z[40];
    z[42]= - n<T>(11,4) + z[8];
    z[42]=z[42]*z[32];
    z[42]=static_cast<T>(23)+ z[42];
    z[43]=n<T>(1,4)*z[8];
    z[42]=z[42]*z[43];
    z[44]=n<T>(1,2)*z[3];
    z[45]=n<T>(5,3) - z[2];
    z[45]=z[2]*z[45];
    z[45]=n<T>(7,24) + z[45];
    z[45]=z[45]*z[44];
    z[46]= - n<T>(4,3) + z[2];
    z[46]=z[2]*z[46];
    z[45]=z[45] - n<T>(7,24) + z[46];
    z[45]=z[3]*z[45];
    z[46]=n<T>(1,2)*z[10];
    z[47]=z[46] - 1;
    z[47]=z[47]*z[10];
    z[48]=n<T>(3,4) + z[47];
    z[14]=z[14] + z[27] + z[45] + z[19] + z[42] + z[41] + n<T>(1,2)*z[48] + 
    z[38];
    z[14]=z[4]*z[14];
    z[19]=z[5] - n<T>(1,2);
    z[27]=n<T>(35,4)*z[7];
    z[31]= - z[21] - n<T>(5,8)*z[3] - z[31] - 3*z[19] - z[27];
    z[31]=z[11]*z[31];
    z[38]=n<T>(1,4)*z[7];
    z[41]=static_cast<T>(49)+ n<T>(131,12)*z[7];
    z[41]=z[41]*z[38];
    z[42]=n<T>(31,8) - z[28];
    z[42]=z[5]*z[42];
    z[45]=static_cast<T>(133)- 51*z[8];
    z[45]=z[8]*z[45];
    z[31]=z[31] + n<T>(9,16)*z[3] + n<T>(1,16)*z[45] + z[42] + z[41];
    z[31]=z[11]*z[31];
    z[41]= - static_cast<T>(3)+ n<T>(7,4)*z[7];
    z[41]=z[7]*z[41];
    z[34]=z[41] - z[34];
    z[41]=n<T>(3,4)*z[6];
    z[34]=z[34]*z[41];
    z[42]=static_cast<T>(3)- z[7];
    z[42]=z[7]*z[42];
    z[45]=n<T>(61,2) - z[32];
    z[45]=z[8]*z[45];
    z[45]=n<T>(29,2) + z[45];
    z[45]=z[8]*z[45];
    z[34]=z[34] + 2*z[42] + n<T>(3,8)*z[45];
    z[34]=z[6]*z[34];
    z[42]=npow(z[2],2);
    z[42]=static_cast<T>(3)- z[42];
    z[45]=static_cast<T>(3)- z[2];
    z[45]=z[2]*z[45];
    z[45]= - static_cast<T>(2)+ z[45];
    z[45]=z[5]*z[45];
    z[42]=n<T>(1,2)*z[42] + z[45];
    z[42]=z[42]*z[28];
    z[29]=z[42] - n<T>(3,8) - z[29];
    z[29]=z[5]*z[29];
    z[42]= - static_cast<T>(43)- n<T>(131,6)*z[7];
    z[42]=z[42]*z[40];
    z[45]=9*z[8];
    z[48]=static_cast<T>(13)- z[13];
    z[48]=z[48]*z[45];
    z[48]= - n<T>(119,2) + z[48];
    z[48]=z[48]*z[30];
    z[49]= - n<T>(11,12) + z[2];
    z[49]=z[2]*z[49];
    z[49]= - n<T>(1,12) + z[49];
    z[49]=z[49]*z[23];
    z[50]=n<T>(11,16) - z[2];
    z[50]=z[2]*z[50];
    z[49]=z[49] + n<T>(1,8) + z[50];
    z[49]=z[3]*z[49];
    z[50]= - n<T>(3,2) - z[10];
    z[29]=z[31] + z[49] + z[34] + z[48] + z[42] + n<T>(1,4)*z[50] + z[29];
    z[29]=z[4]*z[29];
    z[31]= - static_cast<T>(263)+ 153*z[8];
    z[31]=z[31]*z[30];
    z[34]=n<T>(1,8)*z[3];
    z[42]= - static_cast<T>(23)- 9*z[3];
    z[42]=z[42]*z[34];
    z[48]= - static_cast<T>(1)- n<T>(1,2)*z[5];
    z[27]=z[42] + z[31] + 3*z[48] - z[27];
    z[31]=n<T>(9,8)*z[3];
    z[42]=z[31] + static_cast<T>(2)+ n<T>(99,8)*z[8];
    z[42]=z[11]*z[42];
    z[27]=n<T>(1,2)*z[27] + z[42];
    z[27]=z[11]*z[27];
    z[42]= - n<T>(71,2) + z[32];
    z[42]=z[42]*z[16];
    z[42]=static_cast<T>(11)+ z[42];
    z[42]=z[42]*z[43];
    z[48]=static_cast<T>(1)+ n<T>(11,8)*z[3];
    z[48]=z[48]*z[44];
    z[49]=n<T>(5,4) + 2*z[5];
    z[49]=z[5]*z[49];
    z[50]=static_cast<T>(2)+ n<T>(31,48)*z[7];
    z[50]=z[7]*z[50];
    z[27]=z[27] + z[48] + z[42] + z[50] - n<T>(1,2) + z[49];
    z[27]=z[11]*z[27];
    z[42]=n<T>(1,4)*z[10];
    z[48]= - static_cast<T>(1)+ z[10];
    z[48]=z[48]*z[42];
    z[49]=6*z[26] - static_cast<T>(5)+ z[33];
    z[49]=z[5]*z[49];
    z[50]=2*z[2];
    z[49]=z[49] - n<T>(5,2) + z[50];
    z[49]=z[5]*z[49];
    z[51]=n<T>(29,8) - z[45];
    z[51]=z[8]*z[51];
    z[51]=n<T>(5,4) + z[51];
    z[51]=z[51]*z[16];
    z[17]= - z[17]*z[41];
    z[41]=n<T>(13,8) - z[2];
    z[52]=z[3]*z[25];
    z[41]=5*z[41] + z[52];
    z[41]=z[3]*z[41];
    z[41]=n<T>(1,6)*z[41] - n<T>(43,16) + z[50];
    z[41]=z[3]*z[41];
    z[50]=n<T>(9,4) - z[7];
    z[50]=z[7]*z[50];
    z[17]=z[29] + z[27] + z[41] + z[17] + z[51] + z[50] + z[48] + z[49];
    z[17]=z[4]*z[17];
    z[27]=n<T>(3,8)*z[3];
    z[29]=static_cast<T>(1)+ z[44];
    z[29]=z[29]*z[27];
    z[41]=n<T>(3,2) - z[5];
    z[41]=z[5]*z[41];
    z[48]=static_cast<T>(6)+ z[22];
    z[48]=z[7]*z[48];
    z[49]=n<T>(1,2)*z[8];
    z[50]=static_cast<T>(1)- z[49];
    z[50]=z[8]*z[50];
    z[51]= - 3*z[3] - 33*z[8] - 13*z[5] - 31*z[7];
    z[51]=z[11]*z[51];
    z[29]=n<T>(1,8)*z[51] + z[29] + n<T>(51,8)*z[50] + n<T>(3,2)*z[41] + z[48];
    z[29]=z[11]*z[29];
    z[19]= - z[19]*z[28];
    z[19]= - n<T>(5,8) + z[19];
    z[19]=z[5]*z[19];
    z[41]= - static_cast<T>(9)- 13*z[7];
    z[41]=z[41]*z[40];
    z[48]=n<T>(25,2) - z[45];
    z[48]=z[48]*z[43];
    z[48]= - static_cast<T>(1)+ z[48];
    z[16]=z[48]*z[16];
    z[48]= - static_cast<T>(1)- n<T>(3,4)*z[3];
    z[48]=z[48]*z[23];
    z[16]=z[29] + z[48] + z[16] + z[19] + z[41];
    z[16]=z[11]*z[16];
    z[19]=z[2] - 2;
    z[29]= - z[5]*z[19];
    z[29]= - n<T>(1,2)*z[25] + z[29];
    z[28]=z[29]*z[28];
    z[28]=z[28] - z[25];
    z[28]=z[5]*z[28];
    z[29]=n<T>(3,4)*z[8];
    z[41]= - n<T>(17,4) + z[45];
    z[41]=z[8]*z[41];
    z[41]= - n<T>(7,4) + z[41];
    z[41]=z[41]*z[29];
    z[15]=z[15]*z[45];
    z[48]=static_cast<T>(9)- 5*z[7];
    z[48]=z[7]*z[48];
    z[15]=z[48] + z[15];
    z[15]=z[15]*z[37];
    z[37]=n<T>(1,3)*z[3];
    z[24]=z[2]*z[24];
    z[24]=n<T>(1,2) + z[24];
    z[24]=z[24]*z[37];
    z[24]=n<T>(7,8)*z[25] + z[24];
    z[24]=z[24]*z[44];
    z[24]=z[24] + n<T>(9,8) - z[2];
    z[24]=z[3]*z[24];
    z[25]= - static_cast<T>(1)+ z[38];
    z[25]=z[7]*z[25];
    z[15]=z[16] + z[24] + z[15] + z[41] + z[28] + n<T>(3,2)*z[25];
    z[15]=z[15]*npow(z[4],2);
    z[16]=z[10]*z[9];
    z[24]=z[16] - 1;
    z[25]=z[7]*z[24]*z[46];
    z[28]=npow(z[10],2);
    z[25]= - z[28] + z[25];
    z[25]=z[25]*z[38];
    z[41]=npow(z[9],2);
    z[48]=z[41]*z[46];
    z[50]= - z[9] + z[48];
    z[50]=z[10]*z[50];
    z[50]=n<T>(1,2) + z[50];
    z[50]=z[50]*z[37];
    z[24]=z[10]*z[24];
    z[24]= - n<T>(1,8)*z[24] + z[50];
    z[24]=z[3]*z[24];
    z[24]=n<T>(1,4)*z[28] + z[24];
    z[24]=z[3]*z[24];
    z[24]=z[25] + z[24];
    z[15]=n<T>(1,2)*z[24] + z[15];
    z[15]=z[1]*z[15];
    z[24]=n<T>(1,3)*z[9];
    z[25]=z[24] - n<T>(1,8);
    z[28]=n<T>(1,6)*z[16] + z[25];
    z[28]=z[10]*z[28];
    z[28]= - n<T>(1,3) + z[28];
    z[41]=z[41]*z[42];
    z[41]= - z[24] + z[41];
    z[41]=z[10]*z[41];
    z[41]=n<T>(1,12) + z[41];
    z[41]=z[3]*z[41];
    z[28]=n<T>(1,2)*z[28] + z[41];
    z[28]=z[3]*z[28];
    z[41]=z[10] - 3;
    z[41]=z[41]*z[10];
    z[28]=n<T>(1,16)*z[41] + z[28];
    z[28]=z[3]*z[28];
    z[50]=static_cast<T>(3)- 5*z[10];
    z[50]=z[50]*z[46];
    z[51]=z[46]*z[9];
    z[52]=z[51] - static_cast<T>(1)- n<T>(3,2)*z[9];
    z[52]=z[10]*z[52];
    z[52]=n<T>(3,2) + z[52];
    z[52]=z[7]*z[52];
    z[50]=z[50] + z[52];
    z[50]=z[50]*z[40];
    z[52]=npow(z[7],2);
    z[53]= - npow(z[3],2);
    z[53]=z[52] + z[53];
    z[53]=z[11]*z[10]*z[53];
    z[15]=z[15] + z[17] + n<T>(1,16)*z[53] + z[50] + z[28];
    z[15]=z[1]*z[15];
    z[17]=static_cast<T>(55)- n<T>(153,4)*z[8];
    z[17]=z[17]*z[49];
    z[17]= - static_cast<T>(15)+ z[17];
    z[28]=static_cast<T>(7)+ n<T>(9,4)*z[3];
    z[28]=z[28]*z[23];
    z[50]=static_cast<T>(25)- n<T>(99,4)*z[8];
    z[31]= - z[31] + n<T>(1,2)*z[50] + z[6];
    z[31]=z[11]*z[31];
    z[17]=z[31] + z[28] + n<T>(1,2)*z[17] - z[6];
    z[17]=z[11]*z[17];
    z[28]=n<T>(25,3) + z[10];
    z[28]=z[28]*z[40];
    z[31]=n<T>(67,2) - z[32];
    z[31]=z[31]*z[13];
    z[31]= - static_cast<T>(7)+ z[31];
    z[31]=z[31]*z[30];
    z[32]= - n<T>(11,2) - z[10];
    z[32]=z[3]*z[32];
    z[32]=z[10] + z[32];
    z[32]=z[32]*z[34];
    z[17]=z[17] + z[32] + z[31] + z[28] - static_cast<T>(5)- z[5];
    z[17]=z[11]*z[17];
    z[25]=n<T>(17,24)*z[16] + z[25];
    z[25]=z[10]*z[25];
    z[24]= - z[24] + z[48];
    z[24]=z[10]*z[24];
    z[24]=n<T>(1,6) + z[24];
    z[24]=z[3]*z[24];
    z[24]=z[24] + n<T>(5,4)*z[2] - n<T>(11,4) + z[25];
    z[24]=z[24]*z[44];
    z[25]=z[7] - 1;
    z[28]=n<T>(1,3)*z[7];
    z[25]=z[25]*z[28];
    z[31]=n<T>(13,3) + n<T>(3,4)*z[47];
    z[24]=z[24] + z[25] + n<T>(1,2)*z[31] - z[2];
    z[24]=z[3]*z[24];
    z[19]= - 2*z[26] - z[19];
    z[19]=z[5]*z[19];
    z[31]=n<T>(11,2) - 3*z[47];
    z[32]= - static_cast<T>(3)- z[51];
    z[32]=z[7]*z[32];
    z[31]=n<T>(1,2)*z[31] + z[32];
    z[31]=z[31]*z[38];
    z[32]=n<T>(5,4) + z[45];
    z[32]=z[32]*z[13];
    z[32]= - n<T>(17,4) + z[32];
    z[32]=z[32]*z[43];
    z[38]=z[13] - n<T>(1,2);
    z[38]=z[38]*npow(z[8],2);
    z[43]=n<T>(1,2)*z[52] - 3*z[38];
    z[43]=z[6]*z[43];
    z[14]=z[15] + z[14] + z[17] + z[24] + n<T>(3,8)*z[43] + z[32] + z[19] + 
    z[31];
    z[14]=z[1]*z[14];
    z[15]=z[52]*z[36];
    z[15]= - z[18] + z[15];
    z[15]=z[15]*z[36];
    z[17]= - n<T>(3,4) - z[7];
    z[17]=z[7]*z[17];
    z[15]=z[15] - n<T>(1,2) + z[17];
    z[15]=z[6]*z[15];
    z[17]= - static_cast<T>(7)+ z[22];
    z[17]=z[7]*z[17];
    z[15]=z[15] + z[17] + static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[15]=z[15]*z[36];
    z[17]= - n<T>(3,2) + z[6];
    z[17]=z[6]*z[17];
    z[17]=static_cast<T>(1)+ z[17];
    z[17]=z[17]*z[21];
    z[18]=n<T>(1,2)*z[26];
    z[19]= - n<T>(41,8) - z[12];
    z[21]= - n<T>(1,6)*z[2] - n<T>(1,8) + n<T>(1,3)*z[12];
    z[21]=z[2]*z[21];
    z[15]=z[17] + z[15] + n<T>(9,8)*z[7] - z[18] + z[21] + n<T>(1,3)*z[19] - 
    z[46];
    z[15]=z[4]*z[15];
    z[17]=z[6]*z[52];
    z[17]= - z[35] + n<T>(3,2)*z[17];
    z[17]=z[17]*z[36];
    z[19]= - static_cast<T>(1)+ z[28];
    z[19]=z[7]*z[19];
    z[17]=z[17] - n<T>(3,4) + z[19];
    z[17]=z[17]*z[36];
    z[19]= - n<T>(1,4) - z[2];
    z[19]=z[19]*z[44];
    z[21]=n<T>(3,4) + z[41];
    z[22]= - n<T>(41,2) + z[7];
    z[22]=z[7]*z[22];
    z[15]=z[15] + z[19] + z[17] + n<T>(1,12)*z[22] - z[18] + n<T>(1,2)*z[21] + 1.
   /3.*z[2];
    z[17]=static_cast<T>(1)- z[20];
    z[17]=z[6]*z[17];
    z[18]= - n<T>(5,2) + z[6];
    z[18]=z[6]*z[18];
    z[18]=n<T>(3,2) + z[18];
    z[18]=z[11]*z[18];
    z[17]=n<T>(1,4)*z[18] + z[34] - n<T>(2,3) + z[17];
    z[17]=z[11]*z[17];
    z[15]=z[17] + n<T>(1,2)*z[15];
    z[15]=z[4]*z[15];
    z[17]=static_cast<T>(1)- z[9];
    z[18]= - n<T>(7,4) + z[10];
    z[18]=z[10]*z[18];
    z[17]=z[39] + n<T>(3,2)*z[17] + z[18];
    z[18]=3*z[9];
    z[19]=n<T>(1,2) + n<T>(5,3)*z[16];
    z[19]=z[10]*z[19];
    z[19]= - z[33] + z[19] - static_cast<T>(1)+ z[18];
    z[16]=npow(z[16],2)*z[37];
    z[16]=n<T>(1,2)*z[19] + z[16];
    z[16]=z[16]*z[44];
    z[16]=z[16] + n<T>(1,2)*z[17] + z[25];
    z[16]=z[3]*z[16];
    z[17]=z[45]*z[9];
    z[19]= - static_cast<T>(35)- z[18];
    z[19]=n<T>(1,2)*z[19] + z[17];
    z[19]=z[19]*z[13];
    z[19]=n<T>(17,2) + z[19];
    z[19]=z[19]*z[49];
    z[20]= - n<T>(5,2)*z[52] + 9*z[38];
    z[20]=z[20]*z[36];
    z[21]=static_cast<T>(3)- n<T>(7,4)*z[9];
    z[21]=z[7]*z[21];
    z[21]= - n<T>(13,4) + z[21];
    z[21]=z[7]*z[21];
    z[19]=z[20] + z[21] + z[19];
    z[19]=z[19]*z[36];
    z[20]= - n<T>(29,3) + 7*z[9];
    z[20]=z[20]*z[40];
    z[20]=z[20] + n<T>(3,8)*z[10] + n<T>(19,8) - z[9];
    z[20]=z[7]*z[20];
    z[21]=static_cast<T>(21)- 17*z[9];
    z[21]=n<T>(1,2)*z[21] - z[45];
    z[21]=z[21]*z[29];
    z[21]=z[21] + n<T>(65,8) + z[9];
    z[21]=z[8]*z[21];
    z[16]=z[16] + z[19] + z[21] - n<T>(1,6) + z[20];
    z[19]=n<T>(51,2)*z[8] - n<T>(59,2);
    z[19]=z[9]*z[19];
    z[19]= - static_cast<T>(37)+ z[19];
    z[19]=z[19]*z[30];
    z[20]=z[6] - n<T>(23,2) - 9*z[9];
    z[20]=z[20]*z[36];
    z[21]= - n<T>(3,2)*z[3] - n<T>(11,2);
    z[21]=z[9]*z[21];
    z[21]= - static_cast<T>(5)+ z[21];
    z[21]=z[21]*z[34];
    z[22]=z[27] + n<T>(33,8)*z[8] - 9;
    z[22]=z[9]*z[22];
    z[24]= - z[36] + static_cast<T>(1)+ n<T>(9,2)*z[9];
    z[24]=z[6]*z[24];
    z[22]=z[24] - n<T>(1,2) + z[22];
    z[22]=z[11]*z[22];
    z[19]=z[22] + z[21] + z[20] + z[19] + n<T>(21,2) + 10*z[9];
    z[19]=z[11]*z[19];
    z[20]= - static_cast<T>(17)- 21*z[9];
    z[17]=n<T>(1,2)*z[20] + z[17];
    z[13]=z[17]*z[13];
    z[13]=z[13] + n<T>(59,2) - z[18];
    z[13]=z[13]*z[30];
    z[17]= - z[42] + n<T>(1,4) + z[9];
    z[17]=z[3]*z[17];
    z[17]=z[17] + z[46] + n<T>(15,4) - z[9];
    z[17]=z[17]*z[23];
    z[13]=z[19] + z[17] + n<T>(19,4)*z[6] + z[13] - n<T>(131,12) - z[9];
    z[13]=z[11]*z[13];

    r += z[13] + z[14] + z[15] + n<T>(1,2)*z[16];
 
    return r;
}

template double qqb_2lha_r202(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r202(const std::array<dd_real,30>&);
#endif
