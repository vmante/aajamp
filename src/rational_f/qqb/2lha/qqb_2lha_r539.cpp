#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r539(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[2];
    z[2]=k[7];
    z[3]=k[9];
    z[4]=k[4];
    z[5]=k[11];
    z[6]=n<T>(3,2) + z[4];
    z[6]=z[4]*z[6];
    z[7]=z[4] + n<T>(1,2);
    z[8]= - z[1]*z[7];
    z[6]=z[8] + n<T>(1,2) + z[6];
    z[6]=z[3]*z[6];
    z[8]=n<T>(1,2)*z[1];
    z[6]=z[6] + z[8] - z[7];
    z[6]=z[3]*z[6];
    z[7]=z[4] + 1;
    z[9]= - z[8] + z[7];
    z[9]=z[1]*z[9];
    z[10]=static_cast<T>(1)+ n<T>(1,2)*z[4];
    z[11]= - z[4]*z[10];
    z[9]=z[9] - n<T>(1,2) + z[11];
    z[9]=z[3]*z[9];
    z[7]=z[7] + z[9];
    z[9]=z[8] - z[10];
    z[9]=z[1]*z[9];
    z[7]=z[9] + n<T>(1,2)*z[7];
    z[7]=z[3]*z[7];
    z[8]=z[8] - 1;
    z[8]= - z[1]*z[8];
    z[8]= - n<T>(1,2) + z[8];
    z[7]=n<T>(1,2)*z[8] + z[7];
    z[7]=z[2]*z[7];
    z[6]=n<T>(1,4)*z[6] + z[7];
    z[6]=z[2]*z[6];
    z[7]=n<T>(1,8)*z[4];
    z[7]=z[7]*npow(z[3],2);
    z[6]=z[7] + z[6];
    z[6]=z[2]*z[6];
    z[7]= - z[5]*z[7];
    z[6]=z[7] + z[6];

    r += n<T>(9,20)*z[6];
 
    return r;
}

template double qqb_2lha_r539(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r539(const std::array<dd_real,30>&);
#endif
