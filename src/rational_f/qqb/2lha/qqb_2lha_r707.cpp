#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r707(const std::array<T,30>& k) {
  T z[31];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[3];
    z[6]=k[13];
    z[7]=k[6];
    z[8]=k[5];
    z[9]=k[9];
    z[10]=npow(z[4],2);
    z[11]=z[4] + 1;
    z[12]=z[4]*z[11];
    z[12]= - static_cast<T>(5)+ z[12];
    z[12]=z[12]*z[10];
    z[13]=npow(z[4],3);
    z[14]=5*z[3];
    z[15]= - z[13]*z[14];
    z[16]=z[4] - 1;
    z[17]=z[16]*z[13];
    z[18]= - z[3]*npow(z[4],4);
    z[18]=z[17] + z[18];
    z[18]=z[3]*z[18];
    z[13]=z[13] + z[18];
    z[13]=z[9]*z[13];
    z[12]=5*z[13] + z[12] + z[15];
    z[12]=z[9]*z[12];
    z[13]= - z[8]*z[17];
    z[12]=z[13] - z[10] + z[12];
    z[13]=n<T>(1,2)*z[9];
    z[15]=z[11]*z[10]*z[13];
    z[17]= - static_cast<T>(1)+ n<T>(3,2)*z[4];
    z[17]=z[4]*z[17];
    z[15]=z[17] + z[15];
    z[17]=3*z[4];
    z[18]=n<T>(1,2) - z[4];
    z[18]=z[8]*z[18]*z[17];
    z[19]=5*z[4];
    z[20]= - static_cast<T>(1)+ z[19];
    z[18]=n<T>(1,2)*z[20] + z[18];
    z[20]=n<T>(1,4)*z[5];
    z[21]=z[20]*z[8];
    z[21]=z[21] - n<T>(1,2);
    z[22]=n<T>(1,4) - z[4];
    z[22]=z[8]*z[22];
    z[22]=z[22] - z[21];
    z[22]=z[5]*z[22];
    z[18]=n<T>(1,2)*z[18] + z[22];
    z[18]=z[5]*z[18];
    z[10]=z[10]*z[8];
    z[22]=n<T>(3,4) - z[4];
    z[22]=z[22]*z[10];
    z[15]=z[18] + n<T>(1,2)*z[15] + z[22];
    z[15]=z[5]*z[15];
    z[12]=n<T>(1,4)*z[12] + z[15];
    z[12]=z[6]*z[12];
    z[15]=n<T>(1,2)*z[4];
    z[18]= - z[11]*z[15];
    z[22]=z[17] + 5;
    z[23]=z[22]*z[15];
    z[23]=static_cast<T>(1)+ z[23];
    z[23]=z[3]*z[23];
    z[18]=z[18] + z[23];
    z[23]=n<T>(5,2)*z[3];
    z[18]=z[18]*z[23];
    z[24]=z[3] - 1;
    z[25]=static_cast<T>(1)+ n<T>(1,4)*z[4];
    z[25]=z[25]*z[4];
    z[25]=z[25] + n<T>(3,2);
    z[25]=z[25]*z[4];
    z[25]=z[25] + 1;
    z[25]=z[25]*z[4];
    z[25]=z[25] + n<T>(1,4);
    z[24]=z[9]*z[14]*z[25]*z[24];
    z[25]= - n<T>(3,2)*z[3] + 1;
    z[25]=z[14]*z[25];
    z[25]= - z[15] + z[25];
    z[26]=z[4] + 3;
    z[26]=z[26]*z[4];
    z[26]=z[26] + 3;
    z[26]=z[26]*z[4];
    z[26]=z[26] + 1;
    z[25]=z[26]*z[25];
    z[24]=n<T>(1,2)*z[25] + z[24];
    z[24]=z[9]*z[24];
    z[25]=z[15] + 1;
    z[26]=z[25]*z[4];
    z[26]=z[26] + n<T>(1,2);
    z[10]=z[26]*z[10];
    z[10]=n<T>(1,2)*z[10] + z[18] + z[24];
    z[10]=z[7]*z[10];
    z[10]=z[10] + z[12];
    z[12]= - static_cast<T>(9)- z[4];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,2)*z[12] + z[25];
    z[12]=z[3]*z[12];
    z[18]=z[4] + n<T>(1,2);
    z[24]= - z[3]*z[18];
    z[24]= - n<T>(1,2) + z[24];
    z[24]=z[3]*z[24];
    z[25]=npow(z[3],2);
    z[27]=z[2]*z[25];
    z[24]=z[24] + z[27];
    z[24]=z[2]*z[24];
    z[12]=z[24] + n<T>(1,2) + z[12];
    z[24]=n<T>(1,4)*z[2];
    z[12]=z[12]*z[24];
    z[27]= - z[4] - z[5];
    z[27]=z[6]*z[27];
    z[27]=z[27] - 1;
    z[28]=n<T>(1,8)*z[5];
    z[27]=z[28]*z[27];
    z[28]=n<T>(1,8)*z[11];
    z[29]=n<T>(13,8) + z[4];
    z[29]=z[3]*z[29];
    z[29]= - z[28] + z[29];
    z[29]=z[3]*z[29];
    z[25]=z[7]*z[11]*z[25];
    z[12]= - n<T>(5,8)*z[25] + z[12] - z[28] + z[29] + z[27];
    z[12]=z[1]*z[12];
    z[25]= - static_cast<T>(17)- 7*z[4];
    z[25]=z[3]*z[25];
    z[25]=z[25] + n<T>(11,2) + z[19];
    z[25]=z[3]*z[25];
    z[25]=static_cast<T>(1)+ z[25];
    z[27]= - n<T>(7,8) - z[4];
    z[23]=z[27]*z[23];
    z[23]=z[23] + n<T>(21,16) + z[4];
    z[23]=z[23]*z[9];
    z[27]=9*z[4];
    z[28]=n<T>(23,2) + z[27];
    z[28]=z[3]*z[28];
    z[28]= - n<T>(31,2) + 3*z[28];
    z[23]=n<T>(1,8)*z[28] + z[23];
    z[23]=z[9]*z[3]*z[23];
    z[28]= - static_cast<T>(1)+ z[14];
    z[28]=z[28]*z[13];
    z[29]=n<T>(9,2)*z[3];
    z[28]=z[28] + static_cast<T>(1)- z[29];
    z[28]=z[13]*z[28];
    z[28]=z[28] - n<T>(1,2) + z[3];
    z[30]=n<T>(1,2)*z[2];
    z[28]=z[30]*z[3]*z[28];
    z[23]=z[28] + n<T>(1,8)*z[25] + z[23];
    z[23]=z[2]*z[23];
    z[25]= - n<T>(17,2) - z[17];
    z[25]=z[4]*z[25];
    z[25]= - n<T>(11,2) + z[25];
    z[25]=z[25]*z[29];
    z[28]=n<T>(53,4) - z[17];
    z[28]=z[4]*z[28];
    z[25]=z[25] + static_cast<T>(15)+ z[28];
    z[25]=z[3]*z[25];
    z[22]=z[4]*z[22];
    z[22]=n<T>(9,4) + z[22];
    z[22]=z[22]*z[14];
    z[27]= - n<T>(77,4) - z[27];
    z[27]=z[4]*z[27];
    z[22]=z[22] - n<T>(41,4) + z[27];
    z[22]=z[3]*z[22];
    z[22]=n<T>(3,2)*z[11] + z[22];
    z[22]=z[9]*z[22];
    z[16]=z[22] + n<T>(3,2)*z[16] + z[25];
    z[16]=z[9]*z[16];
    z[22]= - n<T>(11,2) - z[17];
    z[22]=z[4]*z[22];
    z[25]=n<T>(53,2) + z[17];
    z[25]=z[4]*z[25];
    z[25]=static_cast<T>(27)+ z[25];
    z[25]=z[3]*z[25];
    z[22]=z[25] - static_cast<T>(5)+ z[22];
    z[22]=z[3]*z[22];
    z[25]= - static_cast<T>(3)- z[19];
    z[22]=n<T>(1,2)*z[25] + z[22];
    z[16]=n<T>(1,2)*z[22] + z[16];
    z[16]=n<T>(1,4)*z[16] + z[23];
    z[16]=z[2]*z[16];
    z[22]= - n<T>(37,8) + z[4];
    z[22]=z[4]*z[22];
    z[22]= - n<T>(83,8) + z[22];
    z[22]=z[4]*z[22];
    z[22]= - n<T>(43,8) + z[22];
    z[23]=n<T>(11,2) + z[4];
    z[23]=z[4]*z[23];
    z[23]=static_cast<T>(1)+ n<T>(1,8)*z[23];
    z[23]=z[4]*z[23];
    z[23]=n<T>(7,16) + z[23];
    z[23]=z[3]*z[23];
    z[22]=n<T>(1,2)*z[22] + 9*z[23];
    z[22]=z[3]*z[22];
    z[23]= - n<T>(9,4) - z[4];
    z[23]=z[23]*z[15];
    z[23]= - static_cast<T>(1)+ z[23];
    z[23]=z[4]*z[23];
    z[23]= - n<T>(5,16) + z[23];
    z[14]=z[23]*z[14];
    z[23]=n<T>(81,16) + 2*z[4];
    z[23]=z[4]*z[23];
    z[23]=n<T>(81,16) + z[23];
    z[23]=z[4]*z[23];
    z[14]=z[14] + n<T>(27,16) + z[23];
    z[14]=z[3]*z[14];
    z[14]= - n<T>(3,4)*z[26] + z[14];
    z[14]=z[9]*z[14];
    z[23]= - n<T>(3,2) + z[4];
    z[23]=z[4]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[4]*z[23];
    z[23]=static_cast<T>(3)+ z[23];
    z[14]=z[14] + n<T>(1,8)*z[23] + z[22];
    z[14]=z[9]*z[14];
    z[22]= - n<T>(3,4) - z[4];
    z[22]=z[9]*z[22];
    z[22]=z[22] + n<T>(7,4) + z[17];
    z[22]=z[22]*z[13];
    z[23]= - static_cast<T>(1)+ z[13];
    z[23]=z[9]*z[23];
    z[23]=n<T>(1,2) + z[23];
    z[23]=z[23]*z[30];
    z[18]=z[23] + z[22] - z[18];
    z[18]=z[2]*z[18];
    z[22]=n<T>(3,2) + z[4];
    z[22]=z[4]*z[22];
    z[22]=n<T>(1,2) + z[22];
    z[13]=z[22]*z[13];
    z[22]= - n<T>(7,2) - z[17];
    z[22]=z[4]*z[22];
    z[13]=z[13] - n<T>(3,4) + z[22];
    z[13]=z[9]*z[13];
    z[22]=n<T>(11,4) + z[17];
    z[22]=z[4]*z[22];
    z[13]=z[13] + n<T>(1,2) + z[22];
    z[13]=n<T>(1,2)*z[13] + z[18];
    z[13]=z[2]*z[13];
    z[18]=n<T>(7,4) + z[4];
    z[18]=z[4]*z[18];
    z[18]=n<T>(3,4) + z[18];
    z[18]=z[9]*z[18]*z[15];
    z[22]= - n<T>(9,8) - z[4];
    z[22]=z[4]*z[22];
    z[22]= - n<T>(3,8) + z[22];
    z[22]=z[4]*z[22];
    z[13]=z[13] + z[22] + z[18];
    z[18]=n<T>(1,2)*z[8];
    z[13]=z[13]*z[18];
    z[18]= - n<T>(1,2) - z[17];
    z[15]=z[18]*z[15];
    z[18]=z[24] - z[4];
    z[22]=n<T>(1,4) - z[18];
    z[22]=z[2]*z[22];
    z[15]=z[15] + z[22];
    z[15]=z[8]*z[15];
    z[18]=z[8]*z[18];
    z[18]=z[18] - z[21];
    z[18]=z[5]*z[18];
    z[19]=static_cast<T>(1)+ z[19];
    z[19]=n<T>(1,2)*z[19] - z[2];
    z[15]=z[18] + n<T>(1,2)*z[19] + z[15];
    z[15]=z[15]*z[20];
    z[11]=z[11]*z[17];
    z[11]=static_cast<T>(1)+ z[11];
    z[17]=static_cast<T>(3)- z[4];
    z[17]=z[4]*z[17];
    z[17]=n<T>(3,2) + z[17];
    z[18]= - static_cast<T>(4)- n<T>(21,16)*z[4];
    z[18]=z[4]*z[18];
    z[18]= - n<T>(19,8) + z[18];
    z[18]=z[3]*z[18];
    z[17]=n<T>(1,8)*z[17] + z[18];
    z[17]=z[3]*z[17];

    r += n<T>(1,4)*z[10] + n<T>(1,16)*z[11] + n<T>(1,2)*z[12] + z[13] + z[14] + 
      z[15] + z[16] + z[17];
 
    return r;
}

template double qqb_2lha_r707(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r707(const std::array<dd_real,30>&);
#endif
