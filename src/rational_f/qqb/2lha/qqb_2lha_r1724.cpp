#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1724(const std::array<T,30>& k) {
  T z[38];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[13];
    z[5]=k[17];
    z[6]=k[2];
    z[7]=k[3];
    z[8]=k[4];
    z[9]=k[11];
    z[10]=k[10];
    z[11]=n<T>(11,2)*z[4];
    z[12]=n<T>(3,4)*z[2];
    z[13]=3*z[5];
    z[14]=n<T>(1,2) - z[13];
    z[14]=z[12] + 3*z[14] + z[11];
    z[14]=z[2]*z[14];
    z[15]=n<T>(1,2)*z[4];
    z[11]= - static_cast<T>(19)- z[11];
    z[11]=z[11]*z[15];
    z[16]=n<T>(3,2)*z[5];
    z[17]=static_cast<T>(5)- z[16];
    z[17]=z[5]*z[17];
    z[17]= - n<T>(3,4) + z[17];
    z[11]=z[14] + 3*z[17] + z[11];
    z[11]=z[2]*z[11];
    z[14]=static_cast<T>(2)- n<T>(1,2)*z[5];
    z[14]=z[5]*z[14];
    z[17]=n<T>(1,4)*z[4];
    z[18]= - static_cast<T>(1)- z[17];
    z[18]=z[4]*z[18];
    z[19]=z[15] - z[5];
    z[20]=z[2]*z[19];
    z[14]=z[20] + z[14] + z[18];
    z[14]=z[2]*z[14];
    z[18]=static_cast<T>(1)+ z[4];
    z[18]=z[18]*z[15];
    z[20]=z[5] - 1;
    z[21]=z[20]*z[5];
    z[14]=z[14] + z[21] + z[18];
    z[14]=z[2]*z[14];
    z[18]=npow(z[5],2);
    z[22]=npow(z[4],2);
    z[23]=z[18] + n<T>(1,2)*z[22];
    z[14]= - n<T>(1,2)*z[23] + z[14];
    z[24]=3*z[1];
    z[14]=z[14]*z[24];
    z[25]=n<T>(1,2)*z[6];
    z[26]=static_cast<T>(2)- z[25];
    z[26]=z[5]*z[26];
    z[26]= - static_cast<T>(2)+ z[26];
    z[26]=z[26]*z[13];
    z[27]=11*z[4];
    z[28]=static_cast<T>(27)+ z[27];
    z[17]=z[28]*z[17];
    z[28]=z[22]*z[7];
    z[11]=z[14] + z[11] + n<T>(11,4)*z[28] + z[26] + z[17];
    z[11]=z[1]*z[11];
    z[14]= - z[27] - n<T>(15,4)*z[28];
    z[14]=z[7]*z[14];
    z[17]=z[2]*z[7];
    z[26]=static_cast<T>(1)+ z[17];
    z[12]=z[26]*z[12];
    z[26]= - static_cast<T>(1)+ 9*z[5];
    z[12]=z[12] + n<T>(3,4)*z[7] + n<T>(15,2)*z[4] - z[26];
    z[12]=z[2]*z[12];
    z[27]= - n<T>(3,2) + z[6];
    z[27]=z[5]*z[27];
    z[27]=z[27] + static_cast<T>(4)- z[6];
    z[27]=z[5]*z[27];
    z[27]= - n<T>(5,2)*z[4] - n<T>(11,4) + z[27];
    z[11]=z[11] + z[12] + 3*z[27] + z[14];
    z[11]=z[1]*z[11];
    z[12]= - z[5]*z[25];
    z[12]=z[12] - static_cast<T>(1)+ z[6];
    z[12]=z[5]*z[12];
    z[12]=z[12] + static_cast<T>(1)- z[25];
    z[14]=static_cast<T>(31)- 9*z[4];
    z[14]=z[4]*z[14];
    z[14]=z[14] + 9*z[28];
    z[14]=z[7]*z[14];
    z[14]=n<T>(1,4)*z[14] + n<T>(21,4) - z[4];
    z[14]=z[7]*z[14];
    z[27]=npow(z[2],2);
    z[27]=n<T>(3,4)*z[27];
    z[29]=npow(z[7],2);
    z[30]=z[29]*z[2];
    z[31]=z[7] + z[30];
    z[31]=z[31]*z[27];
    z[11]=z[11] + z[31] + 3*z[12] + z[14];
    z[11]=z[1]*z[11];
    z[12]=n<T>(1,2)*z[7];
    z[14]= - z[22]*z[12];
    z[31]= - static_cast<T>(2)+ z[4];
    z[31]=z[4]*z[31];
    z[14]=z[31] + z[14];
    z[14]=z[7]*z[14];
    z[31]=npow(z[9],2);
    z[32]=n<T>(1,3)*z[31];
    z[33]= - npow(z[6],2)*z[32];
    z[33]= - static_cast<T>(3)+ z[33];
    z[34]=static_cast<T>(2)- z[15];
    z[34]=z[4]*z[34];
    z[14]=z[14] + n<T>(1,2)*z[33] + z[34];
    z[14]=z[7]*z[14];
    z[33]=z[9] + 1;
    z[34]=z[33]*z[9];
    z[35]=z[31]*z[6];
    z[36]=z[34] - z[35];
    z[36]=z[6]*z[36];
    z[36]= - z[9] + z[36];
    z[37]=n<T>(1,3)*z[6];
    z[36]=z[36]*z[37];
    z[14]=z[36] + z[14];
    z[14]=z[7]*z[14];
    z[36]=z[2]*npow(z[7],3);
    z[29]=z[29] + z[36];
    z[36]=z[7]*z[10];
    z[36]=z[36] + 1;
    z[27]=z[27]*z[36]*z[29];
    z[25]= - z[31]*z[25];
    z[25]=z[34] + z[25];
    z[25]=z[6]*z[25];
    z[29]=n<T>(1,2)*z[9];
    z[34]= - static_cast<T>(2)- z[29];
    z[34]=z[9]*z[34];
    z[25]=z[25] - n<T>(1,2) + z[34];
    z[25]=z[6]*z[25];
    z[25]=z[25] + z[33];
    z[25]=z[6]*z[25];
    z[25]= - n<T>(1,2) + z[25];
    z[11]=z[11] + z[27] + n<T>(1,3)*z[25] + z[14];
    z[11]=z[3]*z[11];
    z[14]=n<T>(13,2)*z[4];
    z[25]=z[14] - z[26];
    z[26]=n<T>(1,2)*z[2];
    z[27]=z[7] - 1;
    z[33]=z[2]*z[27];
    z[33]=9*z[33] + static_cast<T>(19)+ 3*z[7];
    z[33]=z[33]*z[26];
    z[25]=3*z[25] + z[33];
    z[25]=z[25]*z[26];
    z[33]=n<T>(3,2)*z[2];
    z[13]=z[33] + n<T>(3,2)*z[4] - n<T>(1,2) - z[13];
    z[13]=z[2]*z[13];
    z[15]= - static_cast<T>(1)- z[15];
    z[15]=z[4]*z[15];
    z[13]=z[13] - 2*z[21] + z[15];
    z[13]=z[2]*z[13];
    z[13]=z[13] + z[23];
    z[13]=z[13]*z[24];
    z[15]= - static_cast<T>(5)+ z[6];
    z[15]=z[5]*z[15];
    z[15]=static_cast<T>(5)+ z[15];
    z[15]=z[5]*z[15];
    z[14]=z[15] - z[14];
    z[13]=z[13] + z[25] + n<T>(3,2)*z[14] - 4*z[28];
    z[13]=z[1]*z[13];
    z[14]=n<T>(41,2) - 7*z[4];
    z[14]=z[4]*z[14];
    z[14]=z[14] + 7*z[28];
    z[14]=z[7]*z[14];
    z[15]=n<T>(3,2)*z[7];
    z[21]= - static_cast<T>(1)+ z[15];
    z[17]=z[21]*z[17];
    z[17]=z[17] - static_cast<T>(1)+ n<T>(5,2)*z[7];
    z[21]=3*z[2];
    z[17]=z[17]*z[21];
    z[17]=n<T>(5,2) + z[17];
    z[17]=z[2]*z[17];
    z[20]= - z[6]*z[20];
    z[20]= - static_cast<T>(3)+ z[20];
    z[20]=z[5]*z[20];
    z[20]=n<T>(7,2) + z[20];
    z[14]=z[17] + 3*z[20] + z[14];
    z[13]=n<T>(1,2)*z[14] + z[13];
    z[13]=z[1]*z[13];
    z[14]=z[37]*z[31];
    z[17]= - n<T>(7,2) + 2*z[4];
    z[17]=z[4]*z[17];
    z[17]= - z[28] + z[14] + z[17];
    z[17]=z[7]*z[17];
    z[20]=z[9] + n<T>(7,4);
    z[20]=z[20]*z[9];
    z[23]= - z[20] + 2*z[35];
    z[23]=z[23]*z[37];
    z[25]=n<T>(7,2) - z[4];
    z[25]=z[4]*z[25];
    z[17]=z[17] + z[25] - n<T>(3,2) + z[23];
    z[17]=z[7]*z[17];
    z[20]= - z[20] + z[35];
    z[20]=z[20]*z[37];
    z[23]=static_cast<T>(1)+ n<T>(7,3)*z[9];
    z[20]=n<T>(1,4)*z[23] + z[20];
    z[20]=z[6]*z[20];
    z[23]=z[36]*z[7];
    z[25]= - static_cast<T>(1)+ 3*z[23];
    z[25]=z[25]*z[30];
    z[30]= - static_cast<T>(1)+ 5*z[23];
    z[30]=z[7]*z[30];
    z[25]=z[30] + z[25];
    z[25]=z[25]*z[26];
    z[23]=z[23] + z[25];
    z[23]=z[23]*z[33];
    z[11]=z[11] + z[13] + z[23] + z[17] - n<T>(1,4) + z[20];
    z[11]=z[3]*z[11];
    z[13]=z[12] - 1;
    z[13]=z[7]*z[13];
    z[13]=n<T>(1,2) + z[13];
    z[17]=z[8] + n<T>(3,2);
    z[13]=z[21]*z[17]*z[13];
    z[17]=3*z[8];
    z[15]= - z[15] + n<T>(17,2) + z[17];
    z[15]=z[7]*z[15];
    z[15]=z[15] - static_cast<T>(7)- z[17];
    z[13]=n<T>(1,2)*z[15] + z[13];
    z[13]=z[2]*z[13];
    z[13]= - n<T>(3,4)*z[27] + z[13];
    z[13]=z[2]*z[13];
    z[15]=z[27]*z[21];
    z[15]=z[15] + n<T>(7,2) + z[7];
    z[15]=z[15]*z[26];
    z[15]=z[15] + n<T>(1,2) + z[19];
    z[15]=z[2]*z[15];
    z[15]=z[15] - z[18] - n<T>(1,4)*z[22];
    z[15]=z[15]*z[24];
    z[18]=static_cast<T>(3)- n<T>(5,4)*z[4];
    z[18]=z[4]*z[18];
    z[13]=z[15] + z[13] + n<T>(5,4)*z[28] - z[16] + z[18];
    z[13]=z[1]*z[13];
    z[15]= - z[32] - z[22];
    z[15]=z[15]*z[12];
    z[16]=z[8]*z[10];
    z[18]=z[16] + static_cast<T>(1)+ z[10];
    z[18]=3*z[18] + z[9];
    z[19]= - n<T>(3,2) + z[4];
    z[19]=z[4]*z[19];
    z[15]=z[15] + z[19] + n<T>(1,4)*z[18] - z[14];
    z[15]=z[7]*z[15];
    z[18]=z[17]*z[10];
    z[19]=7*z[10] + z[18];
    z[19]=z[7]*z[19];
    z[19]=z[19] + static_cast<T>(7)+ z[8];
    z[12]=z[19]*z[12];
    z[12]=z[12] - n<T>(5,2) - z[8];
    z[12]=z[7]*z[12];
    z[19]=z[8] + 1;
    z[16]=3*z[10] + z[16];
    z[16]=z[7]*z[16];
    z[16]=z[16] + static_cast<T>(3)+ z[8];
    z[16]=z[7]*z[16];
    z[16]= - 3*z[19] + z[16];
    z[16]=z[7]*z[16];
    z[16]=z[16] + static_cast<T>(1)+ z[17];
    z[16]=z[7]*z[16];
    z[16]= - z[8] + z[16];
    z[16]=z[16]*z[26];
    z[12]=z[16] + n<T>(1,2)*z[19] + z[12];
    z[12]=z[2]*z[12];
    z[16]=z[18] + static_cast<T>(1)+ 5*z[10];
    z[16]=z[7]*z[16];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=z[7]*z[16];
    z[16]= - static_cast<T>(1)+ z[16];
    z[12]=n<T>(1,2)*z[16] + z[12];
    z[12]=z[12]*z[33];
    z[14]=z[29] - z[14];
    z[14]=z[6]*z[14];
    z[16]=static_cast<T>(3)- z[4];
    z[16]=z[4]*z[16];
    z[14]=z[16] - static_cast<T>(1)+ z[14];

    r += z[11] + z[12] + z[13] + n<T>(1,2)*z[14] + z[15];
 
    return r;
}

template double qqb_2lha_r1724(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1724(const std::array<dd_real,30>&);
#endif
