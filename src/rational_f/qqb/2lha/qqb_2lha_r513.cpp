#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r513(const std::array<T,30>& k) {
  T z[11];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=z[2] + 3;
    z[5]=z[5]*z[2];
    z[5]=z[5] + 3;
    z[5]=z[5]*z[2];
    z[5]=z[5] + 1;
    z[6]=n<T>(1,2)*z[2];
    z[7]= - z[5]*z[6];
    z[8]=n<T>(1,4)*z[2];
    z[9]=z[8] + 1;
    z[9]=z[9]*z[2];
    z[9]=z[9] + n<T>(3,2);
    z[9]=z[9]*z[2];
    z[9]=z[9] + 1;
    z[9]=z[9]*z[2];
    z[9]=z[9] + n<T>(1,4);
    z[9]=z[9]*z[3];
    z[10]= - n<T>(1,4)*z[5] - z[9];
    z[10]=z[1]*z[10];
    z[9]= - z[2]*z[9];
    z[7]=z[10] + z[7] + z[9];
    z[7]=z[3]*z[7];
    z[9]=z[6] + 1;
    z[9]=z[9]*z[2];
    z[9]=z[9] + n<T>(1,2);
    z[10]= - z[9]*z[6];
    z[7]=z[10] + z[7];
    z[7]=z[4]*z[7];
    z[10]= - n<T>(5,2) - z[2];
    z[6]=z[10]*z[6];
    z[6]= - static_cast<T>(1)+ z[6];
    z[6]=z[2]*z[6];
    z[5]= - z[3]*z[5]*z[8];
    z[5]=z[5] - n<T>(1,4) + z[6];
    z[5]=z[3]*z[5];
    z[5]= - n<T>(1,2)*z[9] + z[5] + z[7];

    r += n<T>(1,2)*z[5];
 
    return r;
}

template double qqb_2lha_r513(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r513(const std::array<dd_real,30>&);
#endif
