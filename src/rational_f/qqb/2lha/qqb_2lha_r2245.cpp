#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2245(const std::array<T,30>& k) {
  T z[17];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[12];
    z[4]=k[8];
    z[5]=k[3];
    z[6]=k[21];
    z[7]=k[10];
    z[8]=k[4];
    z[9]=z[4] - z[2];
    z[9]=z[9]*z[5];
    z[10]=z[9] + static_cast<T>(1)- z[4];
    z[11]=n<T>(3,8)*z[6];
    z[10]=z[11]*z[10];
    z[12]=n<T>(1,3)*z[7];
    z[13]= - n<T>(3,8) + z[12];
    z[13]=z[4]*z[13];
    z[10]=z[13] + z[12] + n<T>(3,4)*z[2] + z[10];
    z[10]=z[5]*z[10];
    z[13]=n<T>(3,8)*z[2];
    z[14]= - static_cast<T>(1)- z[1];
    z[14]=z[14]*z[13];
    z[15]=z[8] + 1;
    z[16]= - z[15]*z[11];
    z[15]=z[7]*z[15];
    z[15]= - n<T>(1,8) + z[15];
    z[10]=z[10] + z[16] + n<T>(1,3)*z[15] + z[14];
    z[10]=z[3]*z[10];
    z[9]=z[6]*z[9];
    z[9]=z[9] + z[2];
    z[14]=static_cast<T>(1)+ n<T>(1,2)*z[6];
    z[14]=z[4]*z[14];
    z[9]=z[14] - n<T>(1,2)*z[9];
    z[9]=z[5]*z[9];
    z[14]= - n<T>(5,3) - n<T>(3,2)*z[1];
    z[14]=z[4]*z[14];
    z[9]=z[10] + n<T>(3,4)*z[9] + z[11] + n<T>(1,4)*z[14] - z[12] + z[13];

    r += z[9]*z[3];
 
    return r;
}

template double qqb_2lha_r2245(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2245(const std::array<dd_real,30>&);
#endif
