#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r7(const std::array<T,30>& k) {
  T z[24];
  T r = 0;

    z[1]=k[1];
    z[2]=k[6];
    z[3]=k[8];
    z[4]=k[20];
    z[5]=k[3];
    z[6]=k[4];
    z[7]=2*z[3];
    z[8]= - static_cast<T>(1)+ z[3];
    z[8]=z[8]*z[7];
    z[9]=z[4]*z[3];
    z[10]=static_cast<T>(5)+ z[3];
    z[10]=z[3]*z[10];
    z[10]=z[10] - 2*z[9];
    z[10]=z[4]*z[10];
    z[11]=4*z[2];
    z[12]=z[3] - z[9];
    z[12]=z[12]*z[11];
    z[8]=z[12] + z[8] + z[10];
    z[8]=z[2]*z[8];
    z[10]=static_cast<T>(2)+ n<T>(1,2)*z[3];
    z[10]=z[10]*z[9];
    z[12]= - n<T>(2,3) - z[3];
    z[12]=z[3]*z[12];
    z[10]=z[12] + n<T>(1,3)*z[10];
    z[10]=z[4]*z[10];
    z[12]=npow(z[3],2);
    z[8]=n<T>(1,3)*z[8] - n<T>(2,3)*z[12] + z[10];
    z[8]=z[2]*z[8];
    z[10]=n<T>(1,2)*z[5];
    z[13]=z[10]*z[12];
    z[7]=z[7] + 1;
    z[14]=n<T>(1,3)*z[3];
    z[15]= - z[7]*z[14];
    z[15]=z[15] - z[13];
    z[15]=z[4]*z[15];
    z[15]=n<T>(7,6)*z[12] + z[15];
    z[15]=z[4]*z[15];
    z[16]=z[12]*z[4];
    z[17]=z[12] - z[16];
    z[17]=z[4]*z[17];
    z[18]= - 2*z[12] + n<T>(1,2)*z[16];
    z[18]=z[4]*z[18];
    z[16]=z[2]*z[16];
    z[16]=z[18] + z[16];
    z[16]=z[2]*z[16];
    z[16]=z[17] + z[16];
    z[16]=z[2]*z[16];
    z[17]=n<T>(1,2)*z[12];
    z[18]=npow(z[4],2);
    z[17]=z[18]*z[17];
    z[16]=z[17] + z[16];
    z[16]=z[1]*z[16];
    z[8]=n<T>(1,3)*z[16] + z[15] + z[8];
    z[8]=z[1]*z[8];
    z[15]=z[3] + 2;
    z[16]=z[15]*z[14];
    z[16]=z[16] + z[13];
    z[16]=z[5]*z[16];
    z[17]=static_cast<T>(1)+ n<T>(5,2)*z[3];
    z[16]=n<T>(1,3)*z[17] + z[16];
    z[16]=z[4]*z[16];
    z[17]=z[12]*z[5];
    z[19]= - n<T>(3,2) - n<T>(2,3)*z[3];
    z[19]=z[3]*z[19];
    z[16]=z[16] + z[19] - n<T>(4,3)*z[17];
    z[16]=z[4]*z[16];
    z[19]=z[3] + 1;
    z[20]=2*z[5];
    z[21]=z[19]*z[20];
    z[21]=z[21] + static_cast<T>(4)+ z[3];
    z[19]=z[4] - z[19];
    z[19]=z[4]*z[19];
    z[22]=z[4] - static_cast<T>(2)+ z[5];
    z[22]=z[2]*z[22];
    z[19]=2*z[22] + n<T>(1,3)*z[21] + z[19];
    z[19]=z[2]*z[19];
    z[7]=z[3]*z[7];
    z[12]=static_cast<T>(1)+ z[12];
    z[12]=z[5]*z[12];
    z[7]=z[7] + z[12];
    z[9]= - n<T>(1,2)*z[9] + n<T>(1,3) + n<T>(3,2)*z[3];
    z[9]=z[4]*z[9];
    z[7]=z[19] + n<T>(1,3)*z[7] + z[9];
    z[7]=z[2]*z[7];
    z[7]=z[8] + z[7] + n<T>(1,6) + z[16];
    z[7]=z[1]*z[7];
    z[8]=z[6] - 1;
    z[9]=z[8]*z[20];
    z[9]=z[9] + static_cast<T>(7)- z[6];
    z[9]=z[5]*z[9];
    z[12]= - static_cast<T>(4)- z[6];
    z[9]=2*z[12] + z[9];
    z[12]=n<T>(1,3)*z[6];
    z[16]=z[12] - 1;
    z[19]=n<T>(2,3)*z[6];
    z[21]= - z[4]*z[19];
    z[21]=z[21] - z[16];
    z[21]=z[4]*z[21];
    z[22]=z[12]*z[4];
    z[23]=z[5]*z[12];
    z[23]= - z[6] + z[23];
    z[23]=z[5]*z[23];
    z[23]= - z[22] + z[6] + z[23];
    z[11]=z[23]*z[11];
    z[9]=z[11] + n<T>(1,3)*z[9] + z[21];
    z[9]=z[2]*z[9];
    z[11]=z[14] + 1;
    z[14]= - n<T>(2,3)*z[5] + z[12] + z[11];
    z[14]=z[5]*z[14];
    z[19]=n<T>(1,2) - z[19];
    z[18]=z[19]*z[18];
    z[19]=n<T>(1,2) - z[3];
    z[9]=z[9] + z[18] + n<T>(1,3)*z[19] + z[14];
    z[9]=z[2]*z[9];
    z[14]=n<T>(5,3)*z[3] + z[17];
    z[14]=z[14]*z[10];
    z[13]= - z[3] - z[13];
    z[17]=n<T>(1,3)*z[5];
    z[13]=z[13]*z[17];
    z[11]= - n<T>(1,2)*z[11] + z[13];
    z[11]=z[5]*z[11];
    z[11]= - n<T>(1,3)*z[8] + z[11];
    z[11]=z[4]*z[11];
    z[11]=z[11] + n<T>(1,3)*z[15] + z[14];
    z[11]=z[4]*z[11];
    z[13]=n<T>(1,2) - z[5];
    z[7]=z[7] + z[9] + n<T>(1,3)*z[13] + z[11];
    z[7]=z[1]*z[7];
    z[9]=n<T>(1,3)*z[4];
    z[11]=z[9] - n<T>(4,3);
    z[13]=npow(z[6],2);
    z[11]=z[13]*z[11];
    z[14]= - static_cast<T>(4)+ z[5];
    z[14]=z[17]*z[14];
    z[14]=static_cast<T>(2)+ z[14];
    z[14]=z[5]*z[13]*z[14];
    z[11]=z[14] + z[11];
    z[11]=z[2]*z[11];
    z[14]=n<T>(1,2)*z[6];
    z[15]= - z[20] + static_cast<T>(7)+ z[14];
    z[15]=z[17]*z[15];
    z[15]=z[15] - static_cast<T>(3)- z[12];
    z[15]=z[5]*z[6]*z[15];
    z[18]=n<T>(1,2)*z[4];
    z[13]=z[13]*z[18];
    z[18]=z[6]*z[8];
    z[13]=z[18] + z[13];
    z[9]=z[13]*z[9];
    z[9]=z[11] + z[9] + n<T>(5,3)*z[6] + z[15];
    z[9]=z[2]*z[9];
    z[11]=z[17] - z[12] - 1;
    z[11]=z[5]*z[11];
    z[11]=z[11] + static_cast<T>(1)+ z[14];
    z[11]=z[5]*z[11];
    z[12]=z[6] - n<T>(1,2);
    z[13]=z[12]*z[22];
    z[13]= - z[14] + z[13];
    z[13]=z[4]*z[13];
    z[15]= - static_cast<T>(1)+ z[14];
    z[9]=z[9] + z[13] + n<T>(1,3)*z[15] + z[11];
    z[9]=z[2]*z[9];
    z[11]=z[16]*z[14];
    z[10]=z[10] + z[12];
    z[10]=z[10]*z[17];
    z[10]=z[11] + z[10];
    z[10]=z[4]*z[10];
    z[8]= - z[5] - z[8];
    z[8]=n<T>(1,3)*z[8] + z[10];
    z[8]=z[4]*z[8];
    z[10]= - static_cast<T>(1)+ z[5];
    z[10]=z[5]*z[10];

    r += z[7] + z[8] + z[9] + n<T>(1,6)*z[10];
 
    return r;
}

template double qqb_2lha_r7(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r7(const std::array<dd_real,30>&);
#endif
