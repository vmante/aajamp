#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1140(const std::array<T,30>& k) {
  T z[21];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[3];
    z[5]=k[13];
    z[6]=k[11];
    z[7]=k[4];
    z[8]=k[8];
    z[9]=k[28];
    z[10]=n<T>(1,4)*z[9];
    z[11]= - n<T>(13,16) + z[8];
    z[11]=n<T>(13,48)*z[2] - n<T>(13,16)*z[7] + n<T>(1,3)*z[11] - z[10];
    z[11]=z[2]*z[11];
    z[12]=n<T>(1,8) - z[8];
    z[13]=n<T>(1,2)*z[7];
    z[14]=z[13] + n<T>(1,3);
    z[15]=z[7]*z[14];
    z[11]=z[11] + n<T>(13,8)*z[15] + n<T>(1,3)*z[12] + z[10];
    z[11]=z[2]*z[11];
    z[12]=npow(z[5],2);
    z[15]= - n<T>(3,2) + z[5];
    z[15]=z[15]*z[12];
    z[15]= - n<T>(13,12) + z[15];
    z[15]=z[7]*z[15];
    z[16]= - n<T>(11,2) + 5*z[5];
    z[16]=z[5]*z[16];
    z[16]= - n<T>(13,6) + z[16];
    z[15]=n<T>(1,2)*z[16] + z[15];
    z[15]=z[15]*z[13];
    z[15]=z[15] - static_cast<T>(1)+ n<T>(5,6)*z[5];
    z[15]=z[15]*z[13];
    z[11]=z[15] + z[11];
    z[11]=z[6]*z[11];
    z[15]=static_cast<T>(1)- z[8];
    z[15]= - n<T>(11,24)*z[2] + n<T>(11,12)*z[7] + n<T>(1,3)*z[15] + z[10];
    z[15]=z[2]*z[15];
    z[16]=n<T>(11,8) + z[8];
    z[17]= - n<T>(11,3) - n<T>(7,2)*z[12];
    z[17]=z[7]*z[17];
    z[17]= - n<T>(1,3) + n<T>(1,8)*z[17];
    z[17]=z[7]*z[17];
    z[10]=z[11] + z[15] + z[17] + n<T>(1,3)*z[16] - z[10];
    z[10]=z[6]*z[10];
    z[11]= - n<T>(3,16) - n<T>(1,3)*z[5];
    z[11]=z[7]*z[11];
    z[10]=z[10] + n<T>(3,16)*z[2] - n<T>(1,16) + z[11];
    z[11]=static_cast<T>(7)+ 11*z[5];
    z[11]=z[11]*z[13];
    z[11]=static_cast<T>(11)+ z[11];
    z[11]=z[11]*z[13];
    z[15]=25*z[7];
    z[16]=z[13] + 1;
    z[17]= - z[16]*z[15];
    z[15]= - n<T>(25,2)*z[2] + n<T>(43,2) + z[15];
    z[15]=z[2]*z[15];
    z[15]=z[15] - static_cast<T>(11)+ z[17];
    z[17]=n<T>(1,2)*z[2];
    z[15]=z[15]*z[17];
    z[11]=z[15] + static_cast<T>(1)+ z[11];
    z[15]= - z[5]*z[13];
    z[15]= - n<T>(5,3) + z[15];
    z[18]=z[5] - 1;
    z[15]=npow(z[7],2)*z[18]*z[15];
    z[18]=n<T>(1,3) + n<T>(1,4)*z[7];
    z[19]=z[7]*z[18];
    z[14]=n<T>(1,6)*z[2] - z[14];
    z[14]=z[2]*z[14];
    z[14]=n<T>(11,2)*z[14] + n<T>(5,4) + 11*z[19];
    z[14]=z[2]*z[14];
    z[19]=z[16]*z[7];
    z[20]= - static_cast<T>(1)- z[19];
    z[20]=z[7]*z[20];
    z[20]= - static_cast<T>(1)+ n<T>(11,2)*z[20];
    z[14]=n<T>(1,3)*z[20] + z[14];
    z[14]=z[2]*z[14];
    z[14]=n<T>(1,2)*z[15] + z[14];
    z[14]=z[6]*z[14];
    z[11]=n<T>(1,3)*z[11] + z[14];
    z[11]=z[6]*z[11];
    z[14]=n<T>(7,3)*z[7];
    z[15]= - static_cast<T>(7)- z[1];
    z[15]=n<T>(7,3)*z[2] + n<T>(1,2)*z[15] - z[14];
    z[15]=z[2]*z[15];
    z[14]=z[14] + n<T>(7,3) + z[1];
    z[14]=n<T>(1,2)*z[14] + z[15];
    z[11]=n<T>(1,2)*z[14] + z[11];
    z[14]= - n<T>(5,3) - z[13];
    z[14]=z[7]*z[14];
    z[14]= - n<T>(7,6) + z[14];
    z[15]= - n<T>(1,8)*z[2] + z[18];
    z[15]=z[2]*z[15];
    z[14]=n<T>(1,4)*z[14] + z[15];
    z[14]=z[2]*z[14];
    z[15]=z[19] + n<T>(1,2);
    z[14]=n<T>(1,6)*z[15] + z[14];
    z[14]=z[2]*z[14];
    z[18]=n<T>(1,3)*z[2] - static_cast<T>(1)- z[7];
    z[18]=z[18]*z[17];
    z[15]=z[18] + z[15];
    z[15]=z[2]*z[15];
    z[18]= - static_cast<T>(1)- n<T>(1,3)*z[7];
    z[18]=z[7]*z[18];
    z[18]= - static_cast<T>(1)+ z[18];
    z[18]=z[7]*z[18];
    z[18]= - n<T>(1,3) + z[18];
    z[15]=n<T>(1,2)*z[18] + z[15];
    z[15]=z[6]*z[15]*npow(z[2],2);
    z[14]=z[14] + n<T>(1,4)*z[15];
    z[14]=z[6]*z[14];
    z[15]= - static_cast<T>(5)- z[1];
    z[15]=z[2] + n<T>(1,2)*z[15] - z[7];
    z[15]=z[15]*z[17];
    z[15]=z[15] + n<T>(1,2)*z[1] + z[16];
    z[15]=z[2]*z[15];
    z[16]=z[1] + 1;
    z[15]= - n<T>(1,4)*z[16] + z[15];
    z[14]=n<T>(1,6)*z[15] + z[14];
    z[14]=z[3]*z[14];
    z[11]=n<T>(1,8)*z[11] + z[14];
    z[11]=z[3]*z[11];
    z[10]=n<T>(1,2)*z[10] + z[11];
    z[10]=z[3]*z[10];
    z[11]= - 5*z[4] + static_cast<T>(5)+ 7*z[1];
    z[11]=z[4]*z[11];
    z[14]=z[4] - 1;
    z[15]=z[1] - z[14];
    z[15]=z[5]*z[15]*npow(z[4],2);
    z[11]=n<T>(1,4)*z[11] + z[15];
    z[11]=z[5]*z[11];
    z[15]=static_cast<T>(3)- z[4];
    z[15]=z[5]*z[4]*z[15];
    z[15]= - n<T>(1,4)*z[14] + z[15];
    z[15]=z[5]*z[15];
    z[15]= - z[9] + z[15];
    z[15]=z[15]*z[7];
    z[16]=3*z[16] - z[4];
    z[11]=z[15] + n<T>(1,4)*z[16] + z[11];
    z[11]=z[5]*z[11];
    z[15]=n<T>(1,2)*z[5];
    z[14]=z[14]*z[15];
    z[14]=z[14] + n<T>(1,2) + 3*z[9];
    z[16]=z[5] + n<T>(1,4) + z[9];
    z[16]=z[5]*z[16];
    z[17]=z[7]*npow(z[5],3);
    z[16]=z[16] + n<T>(3,2)*z[17];
    z[16]=z[7]*z[16];
    z[14]=n<T>(1,2)*z[14] + z[16];
    z[16]= - n<T>(3,2) - z[9];
    z[15]=z[16]*z[15];
    z[12]=z[7]*z[12];
    z[12]= - n<T>(3,4)*z[12] + z[9] + z[15];
    z[12]=z[12]*z[13];
    z[13]=n<T>(1,3)*z[8] - n<T>(3,4)*z[9];
    z[13]=z[2]*z[13];
    z[12]=z[13] - n<T>(1,3) + z[12];
    z[12]=z[6]*z[12];
    z[12]=n<T>(1,2)*z[14] + z[12];
    z[12]=z[6]*z[12];
    z[11]=n<T>(1,4)*z[11] + z[12];

    r += z[10] + n<T>(1,2)*z[11];
 
    return r;
}

template double qqb_2lha_r1140(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1140(const std::array<dd_real,30>&);
#endif
