#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2098(const std::array<T,30>& k) {
  T z[63];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[6];
    z[5]=k[13];
    z[6]=k[4];
    z[7]=k[3];
    z[8]=k[27];
    z[9]=k[14];
    z[10]=k[24];
    z[11]=k[5];
    z[12]=k[17];
    z[13]=k[9];
    z[14]=n<T>(1,4)*z[4];
    z[15]=static_cast<T>(1)+ 3*z[4];
    z[15]=z[15]*z[14];
    z[16]=z[4] - 3;
    z[17]=n<T>(1,2)*z[4];
    z[18]=z[16]*z[17];
    z[18]= - z[8] + z[18];
    z[19]=n<T>(1,2)*z[1];
    z[18]=z[18]*z[19];
    z[20]=3*z[13];
    z[21]=n<T>(1,2)*z[8];
    z[22]=n<T>(1,2)*z[5];
    z[23]=3*z[9];
    z[15]=z[18] + z[15] - z[22] - z[23] + z[20] + z[21];
    z[18]=z[4] - n<T>(3,2);
    z[24]=n<T>(1,8)*z[5];
    z[25]=n<T>(1,4)*z[5];
    z[26]=z[25] - static_cast<T>(1)+ n<T>(3,4)*z[8];
    z[26]=z[11]*z[26];
    z[26]=z[26] - z[24] + n<T>(5,4)*z[8] - z[10] + z[18];
    z[26]=z[11]*z[26];
    z[15]=n<T>(1,2)*z[15] + z[26];
    z[26]= - n<T>(3,2) - z[4];
    z[26]=z[1]*z[26]*z[17];
    z[27]=n<T>(1,2)*z[10];
    z[28]=z[17] - 1;
    z[29]=z[27] - z[28];
    z[29]=z[11]*z[29];
    z[16]=z[16]*z[4];
    z[30]=3*z[10];
    z[31]= - static_cast<T>(1)+ z[30];
    z[26]=3*z[29] + z[26] - n<T>(3,2)*z[16] + n<T>(1,2)*z[31] - z[23];
    z[29]=n<T>(3,2)*z[1];
    z[31]=z[29] - 11;
    z[32]=9*z[9];
    z[33]=z[32] + z[31];
    z[34]=npow(z[11],2);
    z[35]=n<T>(1,2)*z[34];
    z[36]=z[9] - n<T>(3,2);
    z[37]= - 3*z[36] + z[35];
    z[37]=z[2]*z[37];
    z[38]=n<T>(1,4) + z[11];
    z[38]=z[11]*z[38];
    z[33]=z[37] + n<T>(1,2)*z[33] + z[38];
    z[33]=z[2]*z[33];
    z[26]=n<T>(1,2)*z[26] + z[33];
    z[33]= - z[17] - z[36];
    z[37]=n<T>(1,4)*z[1];
    z[38]=5*z[4];
    z[39]= - static_cast<T>(13)+ z[38];
    z[39]=z[4]*z[39];
    z[39]=static_cast<T>(1)+ z[39];
    z[39]=z[39]*z[37];
    z[33]= - n<T>(3,2)*z[11] + 3*z[33] + z[39];
    z[39]=z[23] - 5;
    z[40]=z[11] - z[29] - z[39];
    z[41]=n<T>(1,2)*z[2];
    z[40]=z[40]*z[41];
    z[42]=n<T>(1,4)*z[11];
    z[40]=z[40] + z[42] + n<T>(13,8)*z[1] - static_cast<T>(4)+ z[23];
    z[40]=z[2]*z[40];
    z[43]=n<T>(5,2)*z[4];
    z[44]=z[43] - 7;
    z[45]=z[1]*z[44];
    z[45]=static_cast<T>(3)+ z[45];
    z[46]=n<T>(3,2) - z[1];
    z[46]=z[2]*z[46];
    z[46]=z[46] - static_cast<T>(3)+ n<T>(13,4)*z[1];
    z[46]=z[2]*z[46];
    z[45]=n<T>(1,2)*z[45] + z[46];
    z[45]=z[3]*z[45];
    z[33]=z[45] + n<T>(1,2)*z[33] + z[40];
    z[33]=z[3]*z[33];
    z[26]=n<T>(1,2)*z[26] + z[33];
    z[26]=z[3]*z[26];
    z[33]=z[11] - 1;
    z[40]=n<T>(1,2)*z[11];
    z[45]=z[33]*z[40];
    z[46]=n<T>(1,2)*z[13];
    z[47]=z[45] + z[46] - z[23];
    z[47]=z[47]*z[41];
    z[48]=static_cast<T>(1)- n<T>(3,8)*z[11];
    z[48]=z[11]*z[48];
    z[47]=z[47] + z[48] + n<T>(9,4)*z[9] - n<T>(3,8) - z[13];
    z[47]=z[2]*z[47];
    z[15]=z[26] + n<T>(1,2)*z[15] + z[47];
    z[15]=z[3]*z[15];
    z[26]=z[8] - z[10];
    z[47]=z[4] + 5*z[5] - static_cast<T>(9)+ z[26];
    z[47]=z[47]*z[42];
    z[28]=z[28]*z[4];
    z[48]=z[10] + 1;
    z[49]=z[8] - z[48];
    z[47]=z[47] + n<T>(3,4)*z[49] + z[28];
    z[47]=z[11]*z[47];
    z[49]=n<T>(3,4)*z[5];
    z[50]=z[9] - 1;
    z[51]= - z[49] + n<T>(3,2)*z[8] - z[50];
    z[52]= - z[45] + z[36];
    z[52]=z[2]*z[52];
    z[53]= - n<T>(1,8) + z[4];
    z[53]=z[4]*z[53];
    z[54]=z[8]*z[37];
    z[47]=z[52] + z[47] + z[54] + n<T>(1,2)*z[51] + z[53];
    z[51]=static_cast<T>(3)- n<T>(5,4)*z[4];
    z[51]=z[51]*z[17];
    z[52]=z[4] + n<T>(1,2);
    z[53]=z[40] + z[52];
    z[53]=z[53]*z[40];
    z[45]=z[45] - n<T>(5,4) + z[9];
    z[45]=z[2]*z[45];
    z[54]=z[38] - 11;
    z[55]= - n<T>(3,2) + z[11];
    z[55]=z[2]*z[55];
    z[55]=z[55] - n<T>(1,4)*z[54] - z[11];
    z[55]=z[3]*z[55];
    z[45]=z[55] + z[45] + z[53] + z[51] + n<T>(3,8) - z[9];
    z[45]=z[3]*z[45];
    z[45]=n<T>(1,2)*z[47] + z[45];
    z[45]=z[3]*z[45];
    z[47]=3*z[5];
    z[51]= - n<T>(11,2) + z[47];
    z[51]=z[5]*z[51];
    z[43]=z[51] - z[43];
    z[43]=z[11]*z[43];
    z[51]=npow(z[5],2);
    z[53]=n<T>(9,2)*z[51];
    z[55]=npow(z[4],2);
    z[43]=z[43] - n<T>(5,2)*z[55] - static_cast<T>(5)+ z[53];
    z[43]=z[43]*z[40];
    z[56]=z[4] - 1;
    z[57]=z[56]*z[14];
    z[58]= - static_cast<T>(1)+ z[47];
    z[58]=z[58]*z[25];
    z[59]=z[23] - n<T>(1,2);
    z[20]=z[43] - z[57] + z[58] + z[20] - z[59];
    z[43]=z[40] - 1;
    z[40]= - z[43]*z[40];
    z[40]=z[40] - z[46] + z[9];
    z[40]=z[2]*z[40];
    z[20]=z[45] + n<T>(1,4)*z[20] + z[40];
    z[20]=z[3]*z[20];
    z[40]=static_cast<T>(1)+ z[47];
    z[25]=z[40]*z[25];
    z[40]=static_cast<T>(1)- 7*z[4];
    z[40]=z[40]*z[14];
    z[45]= - n<T>(9,4) + z[5];
    z[45]=z[5]*z[45];
    z[45]=z[45] - z[57];
    z[45]=z[11]*z[45];
    z[25]=z[45] + z[40] - static_cast<T>(1)+ z[25];
    z[25]=z[25]*z[42];
    z[40]=z[4]*z[54];
    z[40]= - z[5] + z[40];
    z[45]= - z[14] - n<T>(3,2) + z[5];
    z[45]=z[11]*z[45];
    z[40]=n<T>(1,8)*z[40] + z[45];
    z[40]=z[11]*z[40];
    z[40]= - n<T>(1,2)*z[9] + z[40];
    z[45]=z[42] - 1;
    z[46]=n<T>(5,8)*z[4] + z[45];
    z[46]=z[3]*z[11]*z[46];
    z[40]=n<T>(1,2)*z[40] + z[46];
    z[40]=z[3]*z[40];
    z[25]=z[25] + z[40];
    z[25]=z[3]*z[25];
    z[40]=n<T>(1,2)*z[55];
    z[46]=z[40] - static_cast<T>(1)- n<T>(3,4)*z[51];
    z[52]=z[52]*z[14];
    z[54]=n<T>(1,8) - z[5];
    z[54]=z[5]*z[54];
    z[52]=z[54] + z[52];
    z[52]=z[11]*z[52];
    z[46]=n<T>(1,2)*z[46] + z[52];
    z[46]=z[11]*z[46];
    z[52]=z[9] - z[13];
    z[46]= - n<T>(1,2)*z[52] + z[46];
    z[54]= - static_cast<T>(1)+ z[22];
    z[54]=z[5]*z[54];
    z[58]=z[5] - z[4];
    z[58]=z[3]*z[58];
    z[54]=z[58] + z[54] - z[28];
    z[54]=z[3]*z[54];
    z[58]=z[51] - z[55];
    z[54]= - z[58] + z[54];
    z[34]=z[3]*z[34]*z[54];
    z[35]=z[58]*z[35];
    z[34]=z[35] + z[34];
    z[34]=z[6]*z[34];
    z[25]=n<T>(1,8)*z[34] + n<T>(1,2)*z[46] + z[25];
    z[25]=z[3]*z[25];
    z[34]=z[51] - n<T>(1,4)*z[55];
    z[34]=z[11]*z[34];
    z[34]= - z[12] + z[34];
    z[34]=z[11]*z[34];
    z[35]=z[12]*z[13];
    z[34]=z[35] + z[34];
    z[25]=n<T>(1,4)*z[34] + z[25];
    z[25]=z[6]*z[25];
    z[34]=z[40] - 5*z[12] - z[53];
    z[40]=n<T>(1,2)*z[7];
    z[46]=z[40] - 1;
    z[47]=z[46]*z[47];
    z[47]=n<T>(1,4) + z[47];
    z[47]=z[5]*z[47];
    z[47]=z[47] + n<T>(3,4)*z[4];
    z[47]=z[11]*z[47];
    z[34]=n<T>(1,2)*z[34] + z[47];
    z[34]=z[11]*z[34];
    z[34]=n<T>(5,2)*z[35] + z[34];
    z[47]=z[11]*z[12];
    z[43]=n<T>(1,2)*z[12] - z[43];
    z[43]=z[43]*z[47];
    z[51]= - z[13] - n<T>(1,2)*z[35];
    z[51]=z[12]*z[51];
    z[43]=z[51] + z[43];
    z[43]=z[2]*z[43];
    z[34]=n<T>(1,2)*z[34] + z[43];
    z[20]=z[25] + n<T>(1,2)*z[34] + z[20];
    z[20]=z[6]*z[20];
    z[25]=z[7] - 1;
    z[34]=z[5]*z[25];
    z[34]=static_cast<T>(1)- 9*z[34];
    z[24]=z[34]*z[24];
    z[34]=static_cast<T>(1)- 3*z[7];
    z[43]=z[46]*z[7];
    z[43]=z[43] + n<T>(1,2);
    z[51]=z[5]*z[43];
    z[34]=n<T>(1,8)*z[34] + z[51];
    z[34]=z[5]*z[34];
    z[51]= - z[8] - z[48];
    z[34]=n<T>(1,2)*z[51] + z[34];
    z[34]=z[11]*z[34];
    z[51]=z[8] + z[10];
    z[24]=z[34] - z[14] + z[24] - n<T>(1,4)*z[51] - z[12];
    z[24]=z[11]*z[24];
    z[34]= - n<T>(1,2) + z[5];
    z[34]=z[34]*z[49];
    z[53]=z[4]*z[5];
    z[54]= - static_cast<T>(3)+ z[5];
    z[54]=z[5]*z[54];
    z[54]=z[54] + z[53];
    z[55]=n<T>(1,8)*z[1];
    z[54]=z[4]*z[54]*z[55];
    z[24]=z[24] + z[54] + n<T>(1,8)*z[53] + z[34] + n<T>(1,4)*z[8] + z[35];
    z[34]=static_cast<T>(1)+ n<T>(3,8)*z[12];
    z[34]=z[12]*z[34];
    z[34]=z[34] - n<T>(3,8)*z[47];
    z[34]=z[11]*z[34];
    z[54]=z[12] + 1;
    z[54]=z[54]*z[12];
    z[54]=z[54] - z[47];
    z[54]=z[54]*z[11];
    z[58]=z[35] + z[13];
    z[58]=z[58]*z[12];
    z[54]=z[54] - z[58];
    z[58]=n<T>(1,4)*z[2];
    z[60]= - z[54]*z[58];
    z[61]= - z[13] - n<T>(3,8)*z[35];
    z[61]=z[12]*z[61];
    z[34]=z[60] + z[61] + z[34];
    z[34]=z[2]*z[34];
    z[15]=z[20] + z[15] + n<T>(1,2)*z[24] + z[34];
    z[15]=z[6]*z[15];
    z[20]=z[22] - 3;
    z[20]=z[20]*z[5];
    z[24]=z[17]*z[5];
    z[34]=n<T>(3,2)*z[5] - z[53];
    z[34]=z[1]*z[34];
    z[34]=z[34] + z[20] + z[24];
    z[34]=z[4]*z[34];
    z[20]= - z[20] + z[34];
    z[20]=z[1]*z[20];
    z[34]=static_cast<T>(1)+ n<T>(3,2)*z[7];
    z[60]=z[5]*z[7];
    z[61]=n<T>(3,2)*z[60] - z[34];
    z[61]=z[5]*z[61];
    z[20]=z[20] + z[24] + z[61] + z[35] - n<T>(3,2) + z[51];
    z[24]=z[22]*z[7];
    z[51]= - z[24] + 1;
    z[51]=z[49]*z[25]*z[51];
    z[61]=n<T>(1,4)*z[7];
    z[62]=static_cast<T>(9)- 5*z[7];
    z[62]=z[62]*z[61];
    z[24]=z[43]*z[24];
    z[24]=z[24] - static_cast<T>(1)+ z[62];
    z[22]=z[24]*z[22];
    z[24]=z[26]*z[61];
    z[24]=z[24] - n<T>(1,2) - z[8];
    z[24]=z[7]*z[24];
    z[26]= - static_cast<T>(1)+ 3*z[8];
    z[22]=z[22] + n<T>(1,4)*z[26] + z[24];
    z[22]=z[11]*z[22];
    z[24]=z[8]*z[46];
    z[26]=n<T>(1,4)*z[12];
    z[22]=z[22] + z[51] - z[26] + n<T>(1,4)*z[10] - z[24];
    z[22]=z[11]*z[22];
    z[26]=z[26] - z[45];
    z[26]=z[26]*z[47];
    z[45]= - z[54]*z[41];
    z[35]= - z[13] - n<T>(1,4)*z[35];
    z[35]=z[12]*z[35];
    z[26]=z[45] + z[35] + z[26];
    z[26]=z[2]*z[26];
    z[20]=z[26] + n<T>(1,4)*z[20] + z[22];
    z[22]= - static_cast<T>(9)- z[27];
    z[22]=n<T>(1,2)*z[22] + z[23];
    z[22]=z[2]*z[22];
    z[26]=3*z[1];
    z[35]=z[11] - z[26] + n<T>(41,2) - 15*z[9];
    z[22]=n<T>(1,2)*z[35] + z[22];
    z[22]=z[22]*z[41];
    z[35]=n<T>(3,4)*z[1];
    z[30]= - static_cast<T>(23)+ z[30];
    z[22]=z[22] - z[42] + z[35] + n<T>(1,8)*z[30] + z[23];
    z[22]=z[2]*z[22];
    z[30]= - static_cast<T>(17)+ z[29];
    z[30]=z[30]*z[37];
    z[41]=z[29] + z[36];
    z[41]=z[2]*z[41];
    z[30]=z[41] + z[30] + n<T>(19,4) - z[23];
    z[30]=z[2]*z[30];
    z[41]=static_cast<T>(9)- z[1];
    z[41]=z[41]*z[19];
    z[30]=z[30] + z[41] + z[39];
    z[30]=z[2]*z[30];
    z[38]=static_cast<T>(17)- z[38];
    z[38]=z[38]*z[55];
    z[38]= - static_cast<T>(1)+ z[38];
    z[38]=z[1]*z[38];
    z[39]=static_cast<T>(1)- n<T>(7,8)*z[1];
    z[39]=z[39]*z[26];
    z[41]=static_cast<T>(1)- z[37];
    z[41]=z[2]*z[41];
    z[41]=z[41] - static_cast<T>(3)+ n<T>(11,8)*z[1];
    z[41]=z[2]*z[1]*z[41];
    z[39]=z[39] + z[41];
    z[39]=z[2]*z[39];
    z[38]=z[38] + z[39];
    z[38]=z[3]*z[38];
    z[39]= - z[4]*z[44];
    z[39]= - n<T>(7,2) + z[39];
    z[39]=z[39]*z[37];
    z[39]=z[39] - n<T>(9,2) + z[4];
    z[39]=z[39]*z[19];
    z[30]=z[38] + z[30] + z[39] + n<T>(7,4) - z[9];
    z[30]=z[3]*z[30];
    z[38]=z[4] - n<T>(5,2);
    z[39]= - z[4]*z[38];
    z[39]= - n<T>(3,2) + z[39];
    z[39]=z[39]*z[55];
    z[41]=z[14] - 1;
    z[42]=z[41]*z[4];
    z[44]=z[42] + n<T>(3,4);
    z[39]=z[39] + z[44];
    z[39]=z[1]*z[39];
    z[45]= - z[23] - z[10] - n<T>(3,2) + n<T>(3,2)*z[4];
    z[22]=z[30] + z[22] + n<T>(1,4)*z[45] + z[39];
    z[22]=z[3]*z[22];
    z[17]=z[17] - 3;
    z[18]= - z[1]*z[18];
    z[18]=z[18] + z[17];
    z[18]=z[4]*z[18];
    z[18]=static_cast<T>(3)+ z[18];
    z[18]=z[18]*z[19];
    z[30]= - n<T>(3,2) + z[10];
    z[18]=z[18] - z[14] + n<T>(1,2)*z[30] - z[52];
    z[30]=static_cast<T>(3)- z[10];
    z[33]=z[11]*z[33];
    z[30]=z[33] - z[32] + n<T>(1,2)*z[30] + z[13];
    z[32]=z[2]*z[9];
    z[30]=n<T>(1,4)*z[30] + z[32];
    z[30]=z[2]*z[30];
    z[33]=z[11] + z[23] - n<T>(1,4) - z[13];
    z[30]=n<T>(1,2)*z[33] + z[30];
    z[30]=z[2]*z[30];
    z[18]=z[22] + n<T>(1,4)*z[18] + z[30];
    z[18]=z[3]*z[18];
    z[15]=z[15] + n<T>(1,2)*z[20] + z[18];
    z[15]=z[6]*z[15];
    z[18]= - z[5] + z[53];
    z[14]=z[1]*z[18]*z[14];
    z[18]= - z[5]*z[42];
    z[14]=z[14] - z[49] + z[18];
    z[14]=z[1]*z[14];
    z[18]=z[5]*z[34];
    z[18]= - z[53] + n<T>(3,2) + z[18];
    z[14]=n<T>(1,2)*z[18] + z[14];
    z[14]=z[1]*z[14];
    z[18]=z[7]*z[24];
    z[18]=z[21] + z[18];
    z[18]=z[7]*z[18];
    z[20]= - z[43]*z[60];
    z[18]=z[18] + z[20];
    z[18]=z[11]*z[18];
    z[20]= - z[7]*z[8];
    z[20]=z[20] + z[60];
    z[20]=z[25]*z[20];
    z[18]=z[18] + z[20];
    z[18]=z[11]*z[18];
    z[20]= - static_cast<T>(1)- z[40];
    z[20]=z[20]*z[60];
    z[21]= - n<T>(1,2) + z[8];
    z[21]=z[7]*z[21];
    z[20]=z[20] - n<T>(1,2) + z[21];
    z[14]=z[58] + z[18] + n<T>(1,2)*z[20] + z[14];
    z[18]= - z[31]*z[37];
    z[20]= - z[26] - z[50];
    z[20]=z[20]*z[58];
    z[18]=z[20] + z[18] + z[50];
    z[18]=z[2]*z[18];
    z[20]=z[1] - 3;
    z[21]=z[1]*z[20];
    z[21]= - 3*z[50] + n<T>(5,2)*z[21];
    z[18]=n<T>(1,2)*z[21] + z[18];
    z[18]=z[2]*z[18];
    z[21]= - static_cast<T>(3)- z[55];
    z[21]=z[1]*z[21];
    z[21]=n<T>(9,2) + z[21];
    z[21]=z[21]*z[19];
    z[18]=z[18] + z[21] + z[50];
    z[18]=z[2]*z[18];
    z[21]=npow(z[1],2);
    z[22]=n<T>(1,2)*z[21];
    z[20]= - z[20]*z[22];
    z[24]=z[58] - static_cast<T>(1)+ z[55];
    z[24]=z[2]*z[21]*z[24];
    z[20]=z[20] + z[24];
    z[20]=z[2]*z[20];
    z[24]= - static_cast<T>(1)+ z[35];
    z[21]=z[24]*z[21];
    z[20]=z[21] + z[20];
    z[20]=z[2]*z[20];
    z[21]=z[1]*z[41];
    z[21]=n<T>(1,2) + z[21];
    z[21]=z[21]*z[22];
    z[20]=z[21] + z[20];
    z[20]=z[3]*z[20];
    z[16]=static_cast<T>(3)+ z[16];
    z[16]=z[16]*z[37];
    z[16]=z[16] - z[17];
    z[16]=z[16]*z[19];
    z[16]= - static_cast<T>(1)+ z[16];
    z[16]=z[1]*z[16];
    z[16]= - n<T>(1,2)*z[50] + z[16];
    z[16]=z[20] + n<T>(1,2)*z[16] + z[18];
    z[16]=z[3]*z[16];
    z[17]=static_cast<T>(9)+ z[10];
    z[17]=n<T>(1,2)*z[17] - z[23];
    z[17]=3*z[17] - n<T>(7,2)*z[1];
    z[18]= - static_cast<T>(7)- z[10];
    z[18]=z[29] + n<T>(3,2)*z[18] + 7*z[9];
    z[20]=static_cast<T>(3)+ z[27];
    z[20]=n<T>(1,2)*z[20] - z[9];
    z[20]=z[2]*z[20];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[18]=z[2]*z[18];
    z[17]=n<T>(1,2)*z[17] + z[18];
    z[17]=z[2]*z[17];
    z[18]= - static_cast<T>(15)- z[10];
    z[18]=n<T>(1,2)*z[18] + 5*z[9];
    z[20]=static_cast<T>(1)+ n<T>(3,8)*z[1];
    z[20]=z[1]*z[20];
    z[17]=z[17] + n<T>(1,2)*z[18] + z[20];
    z[17]=z[2]*z[17];
    z[18]=static_cast<T>(5)- z[4];
    z[18]=z[4]*z[18];
    z[18]= - static_cast<T>(7)+ z[18];
    z[20]=n<T>(1,2) + z[28];
    z[20]=z[1]*z[20];
    z[18]=n<T>(1,2)*z[18] + z[20];
    z[18]=z[1]*z[18];
    z[18]=z[18] - z[56];
    z[18]=z[18]*z[19];
    z[18]=z[18] - z[36];
    z[17]=n<T>(1,2)*z[18] + z[17];
    z[16]=n<T>(1,2)*z[17] + z[16];
    z[16]=z[3]*z[16];
    z[17]= - z[32] + z[59];
    z[17]=z[2]*z[17];
    z[17]=z[17] + n<T>(1,2)*z[48] - z[23];
    z[17]=z[2]*z[17];
    z[17]=z[17] - z[35] + z[9] + n<T>(1,2) - z[10];
    z[17]=z[2]*z[17];
    z[18]=z[1]*z[57];
    z[18]=z[18] - z[44];
    z[18]=z[1]*z[18];
    z[18]= - n<T>(1,2)*z[38] + z[18];
    z[18]=z[1]*z[18];
    z[19]= - static_cast<T>(1)+ z[10];
    z[17]=z[17] + n<T>(1,2)*z[19] + z[18];
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[3]*z[16];

    r += n<T>(1,4)*z[14] + z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r2098(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2098(const std::array<dd_real,30>&);
#endif
