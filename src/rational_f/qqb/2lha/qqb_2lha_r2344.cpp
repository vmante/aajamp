#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2344(const std::array<T,30>& k) {
  T z[14];
  T r = 0;

    z[1]=k[3];
    z[2]=k[4];
    z[3]=k[10];
    z[4]=k[12];
    z[5]=n<T>(1,2)*z[2];
    z[6]=static_cast<T>(1)+ 5*z[2];
    z[6]=z[6]*z[5];
    z[7]=2*z[3];
    z[8]=z[2] + 1;
    z[7]=z[7]*z[8];
    z[9]=npow(z[2],2);
    z[10]=z[9]*z[7];
    z[6]=z[6] + z[10];
    z[6]=z[3]*z[6];
    z[10]=z[2] + 2;
    z[10]=z[10]*z[2];
    z[10]=z[10] + 1;
    z[11]=z[10]*z[3];
    z[12]= - 2*z[8] - z[11];
    z[13]=z[9]*z[3];
    z[12]=z[12]*z[13];
    z[9]= - z[9] + z[12];
    z[9]=z[4]*z[9];
    z[6]=z[9] + z[5] + z[6];
    z[6]=z[4]*z[6];
    z[7]=z[2]*z[7];
    z[9]= - static_cast<T>(3)+ z[2];
    z[7]=n<T>(1,2)*z[9] + z[7];
    z[7]=z[3]*z[7];
    z[8]= - z[8] - z[11];
    z[9]=z[4]*z[3];
    z[8]=z[9]*z[2]*z[8];
    z[7]=z[7] + 2*z[8];
    z[7]=z[4]*z[7];
    z[8]= - z[1]*z[10]*npow(z[9],2);
    z[7]=z[7] + z[8];
    z[7]=z[1]*z[7];
    z[5]= - z[5] - z[13];
    z[5]=z[3]*z[5];

    r +=  - n<T>(1,2) + z[5] + z[6] + z[7];
 
    return r;
}

template double qqb_2lha_r2344(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2344(const std::array<dd_real,30>&);
#endif
