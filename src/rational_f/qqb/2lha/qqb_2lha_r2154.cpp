#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2154(const std::array<T,30>& k) {
  T z[33];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[20];
    z[5]=k[6];
    z[6]=k[7];
    z[7]=k[17];
    z[8]=k[2];
    z[9]=k[4];
    z[10]=k[11];
    z[11]=k[15];
    z[12]=11*z[7];
    z[13]=z[12] + 1;
    z[14]=n<T>(1,2)*z[7];
    z[15]=z[13]*z[14];
    z[16]=static_cast<T>(1)+ 11*z[11];
    z[16]=z[16]*z[11];
    z[17]=n<T>(1,2)*z[6];
    z[15]=z[15] + z[5] + z[17] + z[16];
    z[18]=z[5] - z[6];
    z[19]=static_cast<T>(29)- 33*z[7];
    z[19]=z[7]*z[19];
    z[19]=z[19] + 25*z[11] - z[18];
    z[20]=19*z[11];
    z[21]=z[20] + z[6];
    z[22]=n<T>(1,2)*z[5];
    z[23]= - n<T>(19,2)*z[7] - z[22] - z[21];
    z[23]=z[10]*z[23];
    z[24]=19*z[7];
    z[25]=z[24] + z[5];
    z[26]=z[3]*z[25];
    z[19]= - n<T>(3,2)*z[26] + n<T>(1,2)*z[19] + z[23];
    z[23]=n<T>(1,2)*z[3];
    z[19]=z[23]*z[19];
    z[26]=n<T>(11,2)*z[7];
    z[27]=static_cast<T>(13)- z[26];
    z[27]=z[27]*z[14];
    z[28]=n<T>(11,2)*z[11];
    z[29]=static_cast<T>(7)- z[28];
    z[29]=z[11]*z[29];
    z[27]=z[29] + z[27];
    z[27]=z[10]*z[27];
    z[15]=z[19] + n<T>(1,2)*z[15] + z[27];
    z[15]=z[3]*z[10]*z[15];
    z[19]=z[13]*z[7];
    z[27]=z[5] + z[6];
    z[29]=z[27] + z[19] + z[16];
    z[30]=npow(z[10],2);
    z[31]=n<T>(1,2)*z[30];
    z[32]=z[29]*z[31];
    z[20]= - z[24] - z[20] - z[27];
    z[20]=z[20]*z[23];
    z[24]=static_cast<T>(9)- z[28];
    z[24]=z[11]*z[24];
    z[26]=static_cast<T>(9)- z[26];
    z[26]=z[7]*z[26];
    z[20]=z[20] + z[24] + z[26];
    z[20]=z[3]*z[30]*z[20];
    z[20]=z[32] + z[20];
    z[20]=z[9]*z[20]*z[23];
    z[16]=z[19] + z[22] + z[6] + n<T>(1,2)*z[16];
    z[16]=z[16]*z[31];
    z[15]=z[20] + z[16] + z[15];
    z[15]=z[9]*z[15];
    z[16]=static_cast<T>(1)- z[8];
    z[16]=z[16]*z[17];
    z[13]= - z[8]*z[13];
    z[13]=static_cast<T>(19)+ z[13];
    z[13]=z[13]*z[14];
    z[19]=static_cast<T>(5)- z[28];
    z[19]=z[11]*z[19];
    z[13]=z[13] + z[16] + z[19];
    z[13]=z[10]*z[13];
    z[13]=n<T>(1,2)*z[29] + z[13];
    z[13]=z[10]*z[13];
    z[16]=z[22]*z[6];
    z[19]=z[4] + 1;
    z[19]=z[19]*z[4];
    z[20]=z[2]*z[19];
    z[13]=z[13] + z[16] + z[19] - z[20];
    z[19]=z[10]*z[21];
    z[18]= - z[19] + 17*z[11] - z[18];
    z[19]=n<T>(19,2) - z[12];
    z[19]=z[7]*z[19];
    z[18]=z[19] + n<T>(1,2)*z[18];
    z[18]=z[10]*z[18];
    z[19]=z[2] - 1;
    z[17]=z[18] + z[22] + z[11] + z[17] - z[19];
    z[18]=n<T>(1,2) - z[7];
    z[18]=z[18]*z[12];
    z[21]= - z[10]*z[25];
    z[18]=z[21] - z[22] + z[18];
    z[21]=n<T>(1,4)*z[5];
    z[23]=z[21] + n<T>(19,4)*z[7];
    z[24]=static_cast<T>(1)- n<T>(1,2)*z[2];
    z[24]=5*z[24] - z[23];
    z[24]=z[3]*z[24];
    z[18]=n<T>(1,4)*z[18] + z[24];
    z[18]=z[3]*z[18];
    z[17]=n<T>(1,4)*z[17] + z[18];
    z[17]=z[3]*z[17];
    z[13]=n<T>(1,2)*z[15] + n<T>(1,4)*z[13] + z[17];
    z[13]=z[9]*z[13];
    z[15]=npow(z[4],2);
    z[17]=z[15] + z[20];
    z[17]=z[1]*z[17];
    z[18]=z[1] + 1;
    z[16]=z[18]*z[16];
    z[20]=z[4] - z[20];
    z[20]=z[2]*z[20];
    z[15]=z[16] + z[17] + z[15] + z[20];
    z[16]= - z[18]*z[21];
    z[17]=npow(z[2],2);
    z[18]=static_cast<T>(1)+ z[2];
    z[18]=z[1]*z[18];
    z[17]= - z[17] + z[18];
    z[18]= - z[1]*z[23];
    z[17]=5*z[17] + z[18];
    z[17]=z[3]*z[17];
    z[18]=z[1] - z[2];
    z[12]=z[12]*z[1];
    z[12]=z[12] + 11;
    z[20]= - 7*z[1] - z[12];
    z[20]=z[7]*z[20];
    z[16]=z[17] + n<T>(1,4)*z[20] + z[16] + static_cast<T>(3)+ 5*z[18];
    z[16]=z[3]*z[16];
    z[17]= - static_cast<T>(1)+ z[1];
    z[17]=z[17]*z[22];
    z[12]=z[1] - z[12];
    z[12]=z[12]*z[14];
    z[14]=z[1] - z[19];
    z[14]=z[2]*z[14];
    z[12]=z[12] + z[17] + static_cast<T>(1)+ z[14];
    z[12]=n<T>(1,2)*z[12] + z[16];
    z[12]=z[3]*z[12];
    z[12]=n<T>(1,2)*z[15] + z[12];

    r += n<T>(1,2)*z[12] + z[13];
 
    return r;
}

template double qqb_2lha_r2154(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2154(const std::array<dd_real,30>&);
#endif
