#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1008(const std::array<T,30>& k) {
  T z[41];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[12];
    z[6]=k[11];
    z[7]=k[3];
    z[8]=k[5];
    z[9]=k[8];
    z[10]=k[9];
    z[11]=k[27];
    z[12]=k[13];
    z[13]=k[15];
    z[14]=n<T>(3,8)*z[8];
    z[15]=static_cast<T>(3)+ 7*z[2];
    z[15]=z[15]*z[14];
    z[15]=z[15] - n<T>(13,8)*z[2] + n<T>(73,8) + z[6];
    z[15]=z[8]*z[15];
    z[16]=5*z[6];
    z[17]= - n<T>(63,2)*z[5] - z[16];
    z[18]=25*z[5];
    z[19]= - static_cast<T>(11)- z[18];
    z[19]= - n<T>(1,4)*z[6] + n<T>(1,8)*z[19] + z[13];
    z[19]=z[8]*z[19];
    z[17]=n<T>(1,4)*z[17] + z[19];
    z[17]=z[8]*z[17];
    z[19]=z[1]*z[5];
    z[20]= - 51*z[5] - 13*z[19];
    z[17]=n<T>(1,8)*z[20] + z[17];
    z[17]=z[4]*z[17];
    z[20]=n<T>(1,2)*z[2];
    z[21]=z[20] - 1;
    z[21]=z[10]*z[21];
    z[22]=n<T>(51,2) + z[16];
    z[15]=z[17] + z[15] + n<T>(13,8)*z[1] + n<T>(1,4)*z[22] + z[21];
    z[15]=z[4]*z[15];
    z[17]=n<T>(13,2)*z[1];
    z[21]=17*z[10];
    z[22]=z[17] + z[21] + static_cast<T>(27)+ z[16];
    z[23]=n<T>(15,8) - z[10];
    z[23]=z[2]*z[23];
    z[22]=n<T>(1,4)*z[22] + z[23];
    z[22]=z[2]*z[22];
    z[23]=5*z[10];
    z[24]= - z[23] - n<T>(23,2) - z[6];
    z[17]=3*z[24] - z[17];
    z[24]=n<T>(1,2)*z[8];
    z[25]=static_cast<T>(1)+ n<T>(23,4)*z[2];
    z[25]=z[2]*z[25];
    z[25]= - n<T>(27,4) + z[25];
    z[25]=z[25]*z[24];
    z[15]=z[15] + z[25] + n<T>(1,4)*z[17] + z[22];
    z[15]=z[4]*z[15];
    z[17]=9*z[10];
    z[22]=n<T>(1,4)*z[1];
    z[25]=n<T>(69,2) + z[16];
    z[26]=z[2]*z[6];
    z[27]= - n<T>(3,2)*z[26] + n<T>(13,4)*z[1] - n<T>(9,4) + z[10];
    z[27]=z[2]*z[27];
    z[25]=z[27] - z[22] + n<T>(1,2)*z[25] - z[17];
    z[25]=z[2]*z[25];
    z[25]=z[25] - n<T>(37,4)*z[1] + z[21] - n<T>(111,4) - z[6];
    z[25]=z[2]*z[25];
    z[27]=n<T>(17,4) - 3*z[10];
    z[25]=z[25] + 3*z[27] + n<T>(25,4)*z[1];
    z[15]=n<T>(1,2)*z[25] + z[15];
    z[15]=z[4]*z[15];
    z[25]=n<T>(3,2)*z[1];
    z[27]=static_cast<T>(23)+ z[25];
    z[27]=z[1]*z[27];
    z[28]=3*z[6];
    z[29]=z[2]*z[28];
    z[29]=z[29] - n<T>(11,2)*z[1] - static_cast<T>(5)- 7*z[6];
    z[29]=z[2]*z[29];
    z[27]=z[29] + z[27] + z[23] + static_cast<T>(11)+ z[16];
    z[29]=n<T>(1,4)*z[2];
    z[27]=z[27]*z[29];
    z[30]=z[6] + 3;
    z[21]= - z[21] - z[30];
    z[31]=n<T>(1,8)*z[1];
    z[32]= - static_cast<T>(1)- z[31];
    z[32]=z[1]*z[32];
    z[21]=z[27] + n<T>(1,4)*z[21] + 9*z[32];
    z[21]=z[2]*z[21];
    z[27]=static_cast<T>(25)+ n<T>(9,2)*z[1];
    z[27]=z[1]*z[27];
    z[27]=z[27] - static_cast<T>(7)+ 19*z[10];
    z[21]=n<T>(1,4)*z[27] + z[21];
    z[21]=z[2]*z[21];
    z[27]=3*z[5];
    z[32]= - z[27] - z[19];
    z[33]= - static_cast<T>(3)- z[8];
    z[27]=z[27] + z[6];
    z[27]=z[8]*z[27]*z[33];
    z[27]=3*z[32] + z[27];
    z[32]=n<T>(1,2)*z[4];
    z[27]=z[27]*z[32];
    z[33]=z[1] + z[30];
    z[34]=z[8] + z[6];
    z[34]=static_cast<T>(3)+ n<T>(5,2)*z[34];
    z[34]=z[8]*z[34];
    z[27]=z[27] + n<T>(3,2)*z[33] + z[34];
    z[27]=z[27]*z[32];
    z[33]=z[25] + n<T>(3,2)*z[30] - z[10];
    z[33]=z[33]*z[20];
    z[34]=z[1] - z[10];
    z[35]=z[2] - 1;
    z[36]=z[8]*z[35];
    z[27]=z[27] + 5*z[36] + z[33] - n<T>(9,4) - z[6] - n<T>(3,4)*z[34];
    z[27]=z[4]*z[27];
    z[33]=n<T>(1,2)*z[1];
    z[34]=static_cast<T>(9)+ z[6];
    z[34]=z[33] + n<T>(1,2)*z[34] + z[10];
    z[34]=z[2]*z[34];
    z[36]=n<T>(1,2)*z[6];
    z[34]=z[34] - z[1] - n<T>(5,2)*z[10] - static_cast<T>(9)- z[36];
    z[34]=z[2]*z[34];
    z[37]=static_cast<T>(3)+ z[10];
    z[37]=3*z[37] + z[1];
    z[34]=n<T>(1,2)*z[37] + z[34];
    z[27]=n<T>(3,2)*z[34] + z[27];
    z[27]=z[4]*z[27];
    z[34]=n<T>(5,2)*z[1];
    z[30]=z[34] + n<T>(1,2)*z[30] - z[10];
    z[37]= - npow(z[2],2)*z[36];
    z[30]=3*z[30] + z[37];
    z[30]=z[2]*z[30];
    z[30]=z[30] - n<T>(45,2)*z[1] + n<T>(21,2)*z[10] - n<T>(27,2) - z[6];
    z[30]=z[30]*z[20];
    z[37]=n<T>(15,4)*z[1] + n<T>(9,4) - 2*z[10];
    z[30]=3*z[37] + z[30];
    z[30]=z[2]*z[30];
    z[37]=z[10] - 1;
    z[38]=3*z[37] - 5*z[1];
    z[27]=z[27] + n<T>(3,4)*z[38] + z[30];
    z[27]=z[4]*z[27];
    z[30]=z[1] + 1;
    z[38]=z[30]*z[25];
    z[39]=z[28] + 1;
    z[26]=z[26] - z[39];
    z[26]=z[26]*z[20];
    z[26]=z[26] + z[38] + n<T>(1,2)*z[39] + z[10];
    z[26]=z[26]*z[20];
    z[30]=z[30]*z[1];
    z[38]=3*z[30];
    z[39]= - n<T>(9,2)*z[10] + static_cast<T>(3)- z[36];
    z[26]=z[26] + n<T>(1,2)*z[39] - z[38];
    z[26]=z[2]*z[26];
    z[39]=9*z[30] - static_cast<T>(7)+ n<T>(15,2)*z[10];
    z[26]=n<T>(1,2)*z[39] + z[26];
    z[26]=z[2]*z[26];
    z[26]=z[26] - n<T>(11,4)*z[37] - z[38];
    z[26]=z[2]*z[26];
    z[30]=z[30] + z[37];
    z[26]=z[27] + n<T>(3,4)*z[30] + z[26];
    z[26]=z[3]*z[26];
    z[27]= - static_cast<T>(13)- 3*z[1];
    z[27]=z[27]*z[31];
    z[15]=z[26] + z[15] + z[21] + z[27] + static_cast<T>(1)- n<T>(7,4)*z[10];
    z[15]=z[3]*z[15];
    z[21]=n<T>(1,4)*z[12];
    z[26]= - n<T>(1,8) - z[5];
    z[26]= - n<T>(15,8)*z[2] - z[21] + 5*z[26] + n<T>(13,4)*z[13];
    z[26]=z[8]*z[26];
    z[27]=3*z[13];
    z[30]= - n<T>(5,2) - 41*z[5];
    z[26]=z[26] - z[36] + n<T>(1,4)*z[30] + z[27];
    z[26]=z[8]*z[26];
    z[30]=npow(z[8],2);
    z[38]=z[30]*z[4];
    z[39]= - static_cast<T>(2)+ n<T>(1,2)*z[13];
    z[39]=z[13]*z[39];
    z[39]=n<T>(9,16) + z[39];
    z[39]=z[39]*z[38];
    z[18]= - z[18] + z[10];
    z[18]=z[39] + z[26] + n<T>(1,4)*z[18] - z[19];
    z[18]=z[4]*z[18];
    z[19]=static_cast<T>(19)+ n<T>(23,2)*z[2];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(3,2) + z[19];
    z[19]=z[19]*z[24];
    z[26]=n<T>(19,2) + z[2];
    z[26]=z[2]*z[26];
    z[19]=z[19] + n<T>(43,2) + z[26];
    z[26]=n<T>(1,4)*z[8];
    z[19]=z[19]*z[26];
    z[39]=n<T>(7,2)*z[10] + n<T>(15,2) + z[6];
    z[40]=n<T>(15,16)*z[2] - n<T>(3,8) - z[10];
    z[40]=z[2]*z[40];
    z[18]=z[18] + z[19] + z[40] + n<T>(1,2)*z[39] + z[1];
    z[18]=z[4]*z[18];
    z[19]=n<T>(7,2) + z[10];
    z[28]= - n<T>(11,2) - z[28];
    z[28]=z[2]*z[28];
    z[19]=z[28] + 3*z[19] + n<T>(7,2)*z[1];
    z[19]=z[19]*z[20];
    z[19]=z[19] - z[25] - n<T>(13,2)*z[10] + n<T>(25,4) + z[6];
    z[19]=z[2]*z[19];
    z[25]=n<T>(11,2)*z[2];
    z[28]= - static_cast<T>(7)+ z[25];
    z[28]=z[2]*z[28];
    z[28]=n<T>(3,2) + z[28];
    z[28]=z[28]*z[24];
    z[39]= - z[33] - n<T>(35,2) + 11*z[10];
    z[19]=z[28] + n<T>(1,2)*z[39] + z[19];
    z[18]=n<T>(1,2)*z[19] + z[18];
    z[18]=z[4]*z[18];
    z[19]=3*z[2];
    z[28]=n<T>(3,4) + z[6];
    z[28]=z[28]*z[19];
    z[16]=z[28] - z[34] - n<T>(23,2) - z[16];
    z[16]=z[16]*z[29];
    z[28]=static_cast<T>(15)- z[33];
    z[28]=z[28]*z[31];
    z[31]=n<T>(29,4) + z[6];
    z[16]=z[16] + z[28] + n<T>(1,2)*z[31] + z[10];
    z[16]=z[2]*z[16];
    z[28]= - static_cast<T>(15)+ z[1];
    z[28]=z[28]*z[33];
    z[17]=z[28] - n<T>(7,2) - z[17];
    z[16]=n<T>(1,4)*z[17] + z[16];
    z[16]=z[2]*z[16];
    z[17]=static_cast<T>(5)- z[33];
    z[17]=z[17]*z[33];
    z[17]=z[17] - n<T>(7,4) + z[23];
    z[15]=z[15] + z[18] + n<T>(1,4)*z[17] + z[16];
    z[15]=z[3]*z[15];
    z[16]=n<T>(1,2) + z[6];
    z[16]=z[2]*z[16];
    z[16]=z[16] + z[33] - n<T>(5,2) - z[6];
    z[16]=z[2]*z[16];
    z[16]=z[16] - z[22] + static_cast<T>(1)+ z[10];
    z[16]=z[2]*z[16];
    z[17]= - z[2]*z[35];
    z[17]= - static_cast<T>(3)+ z[17];
    z[17]=z[2]*z[17];
    z[17]=static_cast<T>(3)+ z[17];
    z[17]=z[17]*z[26];
    z[16]=z[17] + z[16] - z[22] - z[37];
    z[17]=n<T>(7,4)*z[12];
    z[18]= - n<T>(17,2) - 45*z[5];
    z[18]=n<T>(9,8)*z[2] - z[17] + n<T>(1,4)*z[18] + 11*z[13];
    z[22]= - static_cast<T>(7)+ z[2];
    z[22]=z[22]*z[29];
    z[23]=n<T>(7,8)*z[12];
    z[22]=z[22] - z[23] + n<T>(15,4)*z[13] + static_cast<T>(1)- n<T>(29,8)*z[5];
    z[22]=z[8]*z[22];
    z[18]=n<T>(1,2)*z[18] + z[22];
    z[18]=z[8]*z[18];
    z[22]= - static_cast<T>(11)+ z[27];
    z[22]=z[13]*z[22];
    z[26]=static_cast<T>(1)+ z[21];
    z[26]=z[12]*z[26];
    z[22]= - z[20] + z[26] + n<T>(25,8) + z[22];
    z[22]=z[22]*z[24];
    z[24]=z[13] - 1;
    z[26]=z[24]*z[13];
    z[22]=2*z[26] + z[22];
    z[22]=z[8]*z[22];
    z[26]= - z[38]*z[26];
    z[22]=z[22] + z[26];
    z[22]=z[4]*z[22];
    z[26]=n<T>(5,8)*z[2];
    z[18]=z[22] + z[18] + z[26] - n<T>(1,4)*z[10] - 2*z[5] + z[24];
    z[18]=z[4]*z[18];
    z[22]= - z[33] + n<T>(9,2) + z[10];
    z[24]= - static_cast<T>(1)- z[36];
    z[24]=z[2]*z[24];
    z[22]=n<T>(1,2)*z[22] + z[24];
    z[22]=z[2]*z[22];
    z[22]=z[22] + n<T>(9,8) - z[10];
    z[24]= - static_cast<T>(1)+ n<T>(7,2)*z[2];
    z[24]=z[2]*z[24];
    z[24]= - n<T>(1,2) + z[24];
    z[14]=z[24]*z[14];
    z[24]=static_cast<T>(2)+ n<T>(1,8)*z[2];
    z[24]=z[2]*z[24];
    z[14]=z[14] - n<T>(1,2) + z[24];
    z[14]=z[8]*z[14];
    z[14]=z[18] + n<T>(1,2)*z[22] + z[14];
    z[14]=z[4]*z[14];
    z[14]=z[15] + n<T>(1,4)*z[16] + z[14];
    z[14]=z[3]*z[14];
    z[15]=z[12]*z[11];
    z[16]=npow(z[11],2);
    z[15]=z[15] + z[16];
    z[15]=z[15]*z[12];
    z[18]=z[16]*z[9];
    z[15]=z[15] + z[18];
    z[22]=z[15]*z[7];
    z[24]=z[11] + n<T>(1,2);
    z[27]=n<T>(1,4)*z[11];
    z[24]=z[24]*z[27];
    z[28]=z[9]*z[24];
    z[27]= - static_cast<T>(1)+ z[27];
    z[27]=z[12]*z[27];
    z[24]=z[24] + z[27];
    z[24]=z[12]*z[24];
    z[24]= - n<T>(1,4)*z[22] + z[28] + z[24];
    z[24]=z[7]*z[24];
    z[27]=z[11] - 1;
    z[27]=z[27]*z[9];
    z[28]=static_cast<T>(3)- z[11];
    z[28]=n<T>(3,8)*z[28] + z[12];
    z[28]=z[12]*z[28];
    z[24]=z[24] - n<T>(3,8)*z[27] + z[28];
    z[24]=z[7]*z[24];
    z[28]= - n<T>(17,4)*z[12] + n<T>(13,4) + z[13];
    z[24]=n<T>(1,2)*z[28] + z[24];
    z[24]=z[7]*z[24];
    z[28]=z[7]*z[9];
    z[31]= - static_cast<T>(3)- z[9];
    z[31]=3*z[31] - 5*z[28];
    z[33]=n<T>(1,2)*z[7];
    z[34]=z[20] - z[33] - n<T>(1,2);
    z[34]=z[9]*z[34];
    z[34]= - static_cast<T>(3)+ z[34];
    z[34]=z[2]*z[34];
    z[36]=z[7] + static_cast<T>(1)+ n<T>(1,2)*z[9];
    z[34]=n<T>(3,2)*z[36] + z[34];
    z[34]=z[2]*z[34];
    z[31]=n<T>(1,4)*z[31] + z[34];
    z[31]=z[31]*z[20];
    z[34]=n<T>(5,2) - z[11];
    z[34]=z[12]*z[34];
    z[16]= - z[16] + z[34];
    z[16]=z[12]*z[16];
    z[15]=z[15]*z[33];
    z[15]=z[15] - z[18] + z[16];
    z[15]=z[7]*z[15];
    z[16]=n<T>(1,2)*z[11];
    z[18]=z[16] + 1;
    z[18]=z[18]*z[11];
    z[34]= - static_cast<T>(5)+ z[16];
    z[34]=z[12]*z[34];
    z[34]=z[34] - static_cast<T>(1)+ z[18];
    z[34]=z[12]*z[34];
    z[18]= - n<T>(1,2) + z[18];
    z[18]=z[9]*z[18];
    z[15]=z[15] + z[18] + z[34];
    z[15]=z[15]*z[33];
    z[18]= - n<T>(7,2) - z[27];
    z[16]=n<T>(5,4)*z[12] + static_cast<T>(3)- z[16];
    z[16]=z[12]*z[16];
    z[15]=z[15] + n<T>(1,2)*z[18] + z[16];
    z[15]=z[7]*z[15];
    z[15]=z[15] - n<T>(5,2)*z[12] + n<T>(3,2) + z[13];
    z[15]=z[15]*z[33];
    z[16]= - n<T>(3,2) - z[28];
    z[16]=z[16]*z[29];
    z[16]=z[16] + n<T>(1,8) + z[7];
    z[16]=z[2]*z[16];
    z[18]= - static_cast<T>(1)- z[28];
    z[16]=n<T>(3,8)*z[18] + z[16];
    z[16]=z[2]*z[16];
    z[18]=static_cast<T>(1)+ n<T>(1,4)*z[28];
    z[18]=z[7]*z[18];
    z[16]=z[16] - n<T>(3,4) + z[18];
    z[16]=z[2]*z[16];
    z[15]=z[15] + z[16];
    z[15]=z[8]*z[15];
    z[15]=z[15] + z[31] - n<T>(1,2) + z[24];
    z[15]=z[8]*z[15];
    z[16]=z[9]*z[11];
    z[18]=3*z[12];
    z[24]= - z[11] + z[18];
    z[24]=z[12]*z[24];
    z[16]=z[22] - z[16] + z[24];
    z[16]=z[16]*z[33];
    z[16]=z[16] - z[9] - z[18];
    z[16]=z[7]*z[16];
    z[18]=static_cast<T>(1)+ z[9];
    z[22]=z[9]*z[35];
    z[22]= - n<T>(9,2) + z[22];
    z[22]=z[2]*z[22];
    z[18]=n<T>(3,2)*z[18] + z[22];
    z[18]=z[2]*z[18];
    z[22]= - static_cast<T>(1)- 3*z[9];
    z[16]=z[18] + n<T>(1,2)*z[22] + z[16];
    z[15]=n<T>(1,4)*z[16] + z[15];
    z[16]=npow(z[12],2);
    z[18]=z[16]*z[7];
    z[22]=n<T>(5,4) - z[13];
    z[22]=z[13]*z[22];
    z[24]= - static_cast<T>(5)- 7*z[12];
    z[24]=z[12]*z[24];
    z[22]=n<T>(7,16)*z[18] + z[22] + n<T>(1,8)*z[24];
    z[22]=z[7]*z[22];
    z[24]= - static_cast<T>(5)+ n<T>(3,2)*z[13];
    z[24]=z[13]*z[24];
    z[23]=static_cast<T>(3)+ z[23];
    z[23]=z[12]*z[23];
    z[27]= - n<T>(1,4) + z[7];
    z[27]=n<T>(1,4)*z[27] - z[2];
    z[27]=z[2]*z[27];
    z[22]=z[27] + z[22] + n<T>(1,2)*z[23] + n<T>(21,16) + z[24];
    z[22]=z[8]*z[22];
    z[23]=n<T>(5,2) + z[12];
    z[23]=z[23]*z[21];
    z[24]=npow(z[13],2);
    z[27]= - z[24] - n<T>(1,2)*z[16];
    z[27]=z[27]*z[33];
    z[28]= - static_cast<T>(3)+ n<T>(5,2)*z[13];
    z[28]=z[13]*z[28];
    z[22]=z[22] - n<T>(7,8)*z[2] + z[27] + z[23] + n<T>(1,8) + z[28];
    z[22]=z[8]*z[22];
    z[23]= - static_cast<T>(1)- z[12];
    z[23]=z[23]*z[21];
    z[16]=z[24] + n<T>(1,4)*z[16];
    z[16]=z[16]*z[33];
    z[27]=n<T>(9,4) - 2*z[13];
    z[27]=z[13]*z[27];
    z[16]=z[20] + z[16] + z[23] - n<T>(1,16) + z[27];
    z[16]=z[8]*z[16];
    z[20]=z[24] - n<T>(1,8);
    z[16]=z[16] - z[20];
    z[16]=z[8]*z[16];
    z[20]=z[20]*z[30]*z[32];
    z[16]=z[16] + z[20];
    z[16]=z[4]*z[16];
    z[16]=z[16] + z[22] - n<T>(1,16) + z[24];
    z[16]=z[4]*z[16];
    z[20]=static_cast<T>(9)+ 5*z[12];
    z[20]=z[20]*z[21];
    z[22]= - n<T>(3,2) + z[13];
    z[22]=z[13]*z[22];
    z[20]= - n<T>(5,4)*z[18] + z[22] + z[20];
    z[20]=z[20]*z[33];
    z[22]=static_cast<T>(13)+ n<T>(9,2)*z[12];
    z[21]=z[22]*z[21];
    z[22]= - static_cast<T>(1)- n<T>(9,4)*z[12];
    z[22]=z[12]*z[22];
    z[22]=z[22] + n<T>(9,8)*z[18];
    z[22]=z[7]*z[22];
    z[23]= - static_cast<T>(3)+ z[13];
    z[23]=z[13]*z[23];
    z[21]=z[22] + z[21] - n<T>(1,8) + z[23];
    z[21]=z[21]*z[33];
    z[22]=n<T>(1,2) - 5*z[7];
    z[19]=n<T>(1,2)*z[22] + z[19];
    z[19]=z[2]*z[19];
    z[19]=n<T>(3,2) + z[19];
    z[19]=z[19]*z[29];
    z[19]=z[19] + z[21] - n<T>(9,8)*z[12] + n<T>(7,4)*z[13] + n<T>(5,8) - z[5];
    z[19]=z[8]*z[19];
    z[21]= - z[7] + z[25];
    z[21]=z[21]*z[29];
    z[17]=z[19] + z[21] + z[20] - z[17] - z[5] + n<T>(11,4)*z[13];
    z[17]=z[8]*z[17];
    z[16]=z[16] + z[17] + z[26] + n<T>(1,8)*z[18] - n<T>(3,8)*z[12] + n<T>(1,16) + 
    z[13];
    z[16]=z[4]*z[16];

    r += z[14] + n<T>(1,2)*z[15] + z[16];
 
    return r;
}

template double qqb_2lha_r1008(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1008(const std::array<dd_real,30>&);
#endif
