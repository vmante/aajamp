#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r2024(const std::array<T,30>& k) {
  T z[13];
  T r = 0;

    z[1]=k[1];
    z[2]=k[2];
    z[3]=k[7];
    z[4]=k[4];
    z[5]=k[5];
    z[6]=npow(z[5],2);
    z[7]=z[6]*z[4];
    z[8]=22*z[5];
    z[9]=11*z[7] + static_cast<T>(41)+ z[8];
    z[9]=z[4]*z[9];
    z[10]=41*z[1];
    z[9]=z[9] - static_cast<T>(33)- z[10];
    z[11]=22*z[4];
    z[12]= - 22*z[2] + z[11] + static_cast<T>(66)+ z[10];
    z[12]=z[2]*z[12];
    z[9]=2*z[9] + z[12];
    z[9]=z[2]*z[9];
    z[11]= - z[6]*z[11];
    z[8]=static_cast<T>(85)+ z[8];
    z[8]=z[5]*z[8];
    z[8]=z[8] + z[11];
    z[8]=z[4]*z[8];
    z[11]= - static_cast<T>(26)- 11*z[5];
    z[8]=4*z[11] + z[8];
    z[8]=z[4]*z[8];
    z[11]= - z[1] + z[4];
    z[12]=z[2]*z[1];
    z[11]=3*z[11] + z[12];
    z[11]=z[2]*z[11];
    z[12]=z[4]*z[5];
    z[12]= - static_cast<T>(2)+ z[12];
    z[12]=z[4]*z[12];
    z[12]=z[1] + z[12];
    z[11]=3*z[12] + z[11];
    z[11]=z[2]*z[11];
    z[12]= - 3*z[5] + z[7];
    z[12]=z[4]*z[12];
    z[12]=static_cast<T>(3)+ z[12];
    z[12]=z[4]*z[12];
    z[11]=z[11] - z[1] + z[12];
    z[11]=z[3]*z[11];
    z[8]=22*z[11] + z[9] + z[8] + static_cast<T>(22)+ z[10];
    z[8]=z[3]*z[8];
    z[9]=static_cast<T>(4)+ z[1];
    z[6]=static_cast<T>(41)+ 22*z[6];
    z[6]=z[4]*z[6];
    z[6]= - 82*z[2] + 41*z[9] + 2*z[6];
    z[6]=z[2]*z[6];
    z[9]= - static_cast<T>(2)- z[1];
    z[7]= - 44*z[7] + static_cast<T>(41)+ 126*z[5];
    z[7]=z[4]*z[7];
    z[6]=2*z[8] + z[6] + 41*z[9] + z[7];
    z[6]=z[3]*z[6];
    z[7]= - z[2] + static_cast<T>(1)+ z[4];
    z[6]=41*z[7] + z[6];

    r += z[6]*z[3];
 
    return r;
}

template double qqb_2lha_r2024(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r2024(const std::array<dd_real,30>&);
#endif
