#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r1861(const std::array<T,30>& k) {
  T z[32];
  T r = 0;

    z[1]=k[1];
    z[2]=k[3];
    z[3]=k[13];
    z[4]=k[17];
    z[5]=k[4];
    z[6]=k[2];
    z[7]=k[10];
    z[8]=k[15];
    z[9]=z[4] - 1;
    z[10]=n<T>(1,2)*z[9];
    z[11]=11*z[2];
    z[12]= - static_cast<T>(5)+ z[11];
    z[12]=z[2]*z[12];
    z[12]= - z[10] + z[12];
    z[13]=3*z[7];
    z[12]=z[12]*z[13];
    z[14]=3*z[9];
    z[15]= - n<T>(37,2) + 23*z[2];
    z[15]=z[2]*z[15];
    z[12]=z[12] - z[14] + z[15];
    z[12]=z[7]*z[12];
    z[15]=n<T>(61,4) - 5*z[2];
    z[15]=z[2]*z[15];
    z[15]= - n<T>(19,4) + z[15];
    z[15]=z[2]*z[15];
    z[12]=z[12] - n<T>(3,4)*z[9] + z[15];
    z[15]=z[11] + n<T>(11,2) - z[4];
    z[16]= - static_cast<T>(5)- z[4];
    z[16]=n<T>(1,2)*z[16] + 16*z[2];
    z[16]=z[7]*z[16];
    z[15]=n<T>(1,2)*z[15] + z[16];
    z[15]=z[7]*z[15];
    z[16]=n<T>(1,2)*z[8];
    z[17]=n<T>(1,2)*z[4];
    z[18]= - z[17] + static_cast<T>(21)- z[16];
    z[19]=z[5]*npow(z[7],2);
    z[20]=n<T>(1,4)*z[19];
    z[18]=z[18]*z[20];
    z[15]=z[15] + z[18];
    z[15]=z[5]*z[15];
    z[12]=n<T>(1,2)*z[12] + z[15];
    z[12]=z[5]*z[12];
    z[15]=13*z[2];
    z[18]= - n<T>(107,2) + z[15];
    z[18]=z[2]*z[18];
    z[18]=static_cast<T>(3)+ z[18];
    z[18]=z[2]*z[18];
    z[14]= - z[14] + z[18];
    z[18]=3*z[2];
    z[21]= - n<T>(5,2) + 2*z[2];
    z[21]=z[21]*z[18];
    z[21]=n<T>(1,2) + z[21];
    z[21]=z[2]*z[21];
    z[10]= - z[10] + z[21];
    z[10]=z[7]*z[10];
    z[10]=n<T>(1,2)*z[14] + z[10];
    z[10]=z[7]*z[10];
    z[14]=z[1] + 3;
    z[21]= - z[14]*z[9];
    z[22]= - n<T>(129,2) - 25*z[1];
    z[23]=z[2] - z[1];
    z[24]=n<T>(131,4) - 10*z[23];
    z[24]=z[2]*z[24];
    z[22]=n<T>(1,2)*z[22] + z[24];
    z[22]=z[2]*z[22];
    z[22]=n<T>(1,4)*z[14] + z[22];
    z[22]=z[2]*z[22];
    z[10]=z[12] + z[10] + n<T>(1,4)*z[21] + z[22];
    z[10]=z[5]*z[10];
    z[12]= - static_cast<T>(3)- z[7];
    z[21]=2*z[7];
    z[12]=z[21]*z[12];
    z[22]=static_cast<T>(4)- z[23];
    z[22]=z[2]*z[22];
    z[12]=z[12] - 2*z[14] + z[22];
    z[12]=z[12]*npow(z[2],3);
    z[22]= - static_cast<T>(2)- z[7];
    z[13]=z[13]*z[22];
    z[22]=n<T>(1,2)*z[2];
    z[23]=z[22] - 2;
    z[24]= - z[2]*z[23];
    z[13]=z[13] - static_cast<T>(3)+ z[24];
    z[24]=npow(z[2],2);
    z[13]=z[24]*z[13];
    z[25]= - static_cast<T>(1)- z[7];
    z[21]=z[21]*z[2]*z[25];
    z[25]=n<T>(1,2)*z[19];
    z[21]=z[21] - z[25];
    z[21]=z[5]*z[21];
    z[13]=z[21] + z[13];
    z[13]=z[5]*z[13];
    z[12]=z[13] + z[12];
    z[12]=z[5]*z[12];
    z[13]=n<T>(1,2)*z[1];
    z[21]= - static_cast<T>(2)- z[13];
    z[21]=z[1]*z[21];
    z[23]=z[1] - z[23];
    z[23]=z[2]*z[23];
    z[26]=n<T>(1,2)*z[7];
    z[27]= - static_cast<T>(2)- z[26];
    z[27]=z[7]*z[27];
    z[21]=z[27] + z[23] - static_cast<T>(3)+ z[21];
    z[21]=z[21]*npow(z[2],4);
    z[12]=z[12] + z[21];
    z[12]=z[3]*z[12];
    z[21]=5*z[1];
    z[23]= - static_cast<T>(5)- n<T>(3,2)*z[1];
    z[23]=z[23]*z[21];
    z[27]= - n<T>(5,2)*z[2] + n<T>(67,8) + z[21];
    z[27]=z[27]*z[18];
    z[23]=z[27] - n<T>(239,8) + z[23];
    z[23]=z[2]*z[23];
    z[13]=z[13] + 1;
    z[27]=z[13]*z[1];
    z[27]=z[27] + n<T>(3,2);
    z[28]=n<T>(1,4)*z[27];
    z[23]=z[28] + z[23];
    z[23]=z[2]*z[23];
    z[23]=z[28] + z[23];
    z[23]=z[2]*z[23];
    z[28]= - static_cast<T>(5)+ z[22];
    z[28]=z[2]*z[28];
    z[28]=n<T>(1,4) + z[28];
    z[28]=z[2]*z[28];
    z[28]=n<T>(1,4) + z[28];
    z[28]=z[2]*z[28];
    z[28]= - n<T>(1,4)*z[9] + z[28];
    z[28]=z[7]*z[28];
    z[29]= - n<T>(59,2) + z[2];
    z[29]=z[2]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[2]*z[29];
    z[29]=static_cast<T>(1)+ z[29];
    z[29]=z[2]*z[29];
    z[28]=z[28] + z[29] - z[9];
    z[28]=z[28]*z[26];
    z[27]= - z[27]*z[9];
    z[10]=5*z[12] + z[10] + z[28] + n<T>(1,4)*z[27] + z[23];
    z[10]=z[3]*z[10];
    z[12]=static_cast<T>(7)+ z[16];
    z[12]=3*z[12] + z[17];
    z[12]=n<T>(1,2)*z[12] - z[15];
    z[12]=z[12]*z[26];
    z[12]=z[12] - z[17] - n<T>(13,4) + z[8];
    z[12]=z[7]*z[12];
    z[15]=n<T>(1,8)*z[8];
    z[23]=n<T>(1,8)*z[4] - static_cast<T>(3)+ z[15];
    z[19]=z[23]*z[19];
    z[12]=z[12] + z[19];
    z[12]=z[5]*z[12];
    z[19]=static_cast<T>(7)- 5*z[4];
    z[23]= - n<T>(1,2) - z[2];
    z[23]=z[2]*z[23];
    z[19]=n<T>(1,2)*z[19] + z[23];
    z[23]=z[17] - 1;
    z[27]=n<T>(43,4) - 4*z[2];
    z[27]=z[2]*z[27];
    z[27]= - n<T>(3,4)*z[23] + z[27];
    z[27]=z[7]*z[27];
    z[28]=static_cast<T>(5)- n<T>(7,4)*z[4];
    z[28]=3*z[28] + n<T>(7,2)*z[2];
    z[27]=n<T>(1,2)*z[28] + z[27];
    z[27]=z[7]*z[27];
    z[12]=z[12] + n<T>(1,4)*z[19] + z[27];
    z[12]=z[5]*z[12];
    z[19]=n<T>(23,2) - z[2];
    z[19]=z[2]*z[19];
    z[19]=n<T>(3,2) + z[19];
    z[19]=z[2]*z[19];
    z[19]= - n<T>(5,4)*z[9] + z[19];
    z[19]=z[19]*z[26];
    z[27]=static_cast<T>(3)+ n<T>(23,4)*z[2];
    z[27]=z[2]*z[27];
    z[19]=z[19] - n<T>(15,4)*z[9] + z[27];
    z[19]=z[7]*z[19];
    z[27]=static_cast<T>(2)+ n<T>(3,4)*z[1];
    z[27]= - z[27]*z[9];
    z[11]= - z[11] + static_cast<T>(25)+ 11*z[1];
    z[11]=z[11]*z[22];
    z[28]=3*z[1];
    z[11]=z[11] - n<T>(23,8) - z[28];
    z[11]=z[2]*z[11];
    z[11]=z[12] + z[19] + z[11] + z[27];
    z[11]=z[5]*z[11];
    z[12]=static_cast<T>(9)+ 7*z[1];
    z[12]=z[12]*z[1];
    z[12]=z[12] + 11;
    z[19]= - z[12]*z[9];
    z[27]= - static_cast<T>(41)- n<T>(31,2)*z[1];
    z[27]=z[1]*z[27];
    z[29]= - n<T>(31,2)*z[2] + n<T>(161,4) + 31*z[1];
    z[29]=z[2]*z[29];
    z[27]=z[29] - n<T>(65,2) + z[27];
    z[27]=z[2]*z[27];
    z[12]=n<T>(1,4)*z[12] + z[27];
    z[12]=z[2]*z[12];
    z[12]=n<T>(1,4)*z[19] + z[12];
    z[19]= - static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[27]=z[19]*z[18];
    z[27]=n<T>(13,8) + z[27];
    z[27]=z[2]*z[27];
    z[29]=z[2] + 1;
    z[29]=z[29]*z[2];
    z[30]=static_cast<T>(1)+ z[29];
    z[30]=z[2]*z[30];
    z[30]=z[30] - z[9];
    z[31]=n<T>(1,4)*z[7];
    z[30]=z[30]*z[31];
    z[27]=z[30] - n<T>(13,8)*z[9] + z[27];
    z[27]=z[7]*z[27];
    z[10]=z[10] + z[11] + n<T>(1,2)*z[12] + z[27];
    z[10]=z[3]*z[10];
    z[11]=z[22] + n<T>(5,8)*z[4] - static_cast<T>(3)- z[15];
    z[11]=z[7]*z[11];
    z[12]=n<T>(3,2)*z[4] + static_cast<T>(1)- z[16];
    z[11]=n<T>(1,2)*z[12] + z[11];
    z[11]=z[7]*z[11];
    z[12]=z[16] + 1;
    z[15]=z[17] + z[12];
    z[15]=z[15]*z[20];
    z[11]=z[11] + z[15];
    z[11]=z[5]*z[11];
    z[15]=3*z[8];
    z[20]= - static_cast<T>(5)- z[15];
    z[20]=n<T>(1,2)*z[20] + 3*z[4];
    z[27]= - static_cast<T>(7)+ z[22];
    z[27]=z[2]*z[27];
    z[20]=n<T>(1,2)*z[20] + z[27];
    z[20]=z[20]*z[26];
    z[20]=z[20] + n<T>(3,4)*z[2] + z[4] - static_cast<T>(5)- n<T>(13,8)*z[8];
    z[20]=z[7]*z[20];
    z[27]=z[8] - n<T>(1,2);
    z[30]= - 7*z[27] + n<T>(5,2)*z[4];
    z[11]=z[11] + n<T>(1,4)*z[30] + z[20];
    z[11]=z[5]*z[11];
    z[13]= - z[22] + z[13];
    z[13]=z[2]*z[13];
    z[20]=n<T>(1,2)*z[23] - z[24];
    z[20]=z[20]*z[26];
    z[19]=z[2]*z[19];
    z[19]=z[20] + z[19] + n<T>(3,8) - z[4];
    z[19]=z[7]*z[19];
    z[20]= - n<T>(1,4) + z[1];
    z[20]=z[4]*z[20];
    z[20]=z[20] - n<T>(15,4) - z[1];
    z[11]=z[11] + z[19] + n<T>(1,2)*z[20] + z[13];
    z[11]=z[5]*z[11];
    z[13]=z[29] - z[9];
    z[13]=z[13]*z[31];
    z[19]=n<T>(5,2) + z[2];
    z[19]=z[2]*z[19];
    z[13]=z[13] - n<T>(5,2)*z[9] + z[19];
    z[13]=z[13]*z[26];
    z[19]=static_cast<T>(1)+ n<T>(11,8)*z[1];
    z[19]=z[19]*z[1];
    z[19]=z[19] + n<T>(9,8);
    z[19]= - z[19]*z[9];
    z[20]= - z[2] + n<T>(7,4) + 2*z[1];
    z[18]=z[20]*z[18];
    z[20]= - n<T>(25,4) - z[28];
    z[20]=z[1]*z[20];
    z[18]=z[18] - n<T>(7,4) + z[20];
    z[18]=z[2]*z[18];
    z[10]=z[10] + z[11] + z[13] + z[18] + z[19];
    z[10]=z[3]*z[10];
    z[11]=n<T>(1,2)*z[6];
    z[13]=static_cast<T>(1)+ z[11];
    z[13]=z[4]*z[13];
    z[18]= - z[2]*z[27];
    z[11]=z[18] + z[13] - z[11] - z[12];
    z[11]=z[7]*z[11];
    z[13]=z[22]*z[8];
    z[18]=n<T>(3,2) + z[6];
    z[18]=z[4]*z[18];
    z[11]=z[11] - z[13] + n<T>(3,2) + z[18];
    z[11]=z[7]*z[11];
    z[18]= - static_cast<T>(1)+ z[6];
    z[18]=z[18]*z[17];
    z[13]= - z[13] + z[18] + static_cast<T>(1)- n<T>(3,2)*z[8];
    z[13]=z[7]*z[13];
    z[18]=z[4] + z[8];
    z[13]=z[13] - z[18];
    z[13]=z[7]*z[13];
    z[18]= - z[18]*z[25];
    z[13]=z[13] + z[18];
    z[13]=z[5]*z[13];
    z[11]=z[13] + z[11] - z[15] - z[17];
    z[13]=n<T>(1,2)*z[5];
    z[11]=z[11]*z[13];
    z[12]= - z[2]*z[12];
    z[12]=z[12] + z[16] + z[9];
    z[12]=z[7]*z[12];
    z[13]=z[6] + 5;
    z[13]=z[13]*z[9];
    z[15]=static_cast<T>(1)- z[8];
    z[15]=z[2]*z[15];
    z[13]=z[15] + 5*z[8] + z[13];
    z[12]=n<T>(1,2)*z[13] + z[12];
    z[12]=z[12]*z[26];
    z[13]=static_cast<T>(1)+ n<T>(1,4)*z[6];
    z[13]=z[4]*z[13];
    z[11]=z[11] + z[12] + z[13] + n<T>(1,4) + z[8];
    z[11]=z[5]*z[11];
    z[12]= - static_cast<T>(1)- z[21];
    z[12]=z[1]*z[12];
    z[12]= - static_cast<T>(1)+ z[12];
    z[12]=z[12]*z[17];
    z[13]= - z[1]*z[14];
    z[12]=z[12] + n<T>(1,2) + z[13];
    z[9]=z[2] - z[9];
    z[9]=z[9]*z[31];
    z[13]= - z[22] + n<T>(1,2) + z[1];
    z[13]=z[2]*z[13];
    z[9]=z[11] + z[9] + n<T>(1,2)*z[12] + z[13];

    r += n<T>(1,2)*z[9] + z[10];
 
    return r;
}

template double qqb_2lha_r1861(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r1861(const std::array<dd_real,30>&);
#endif
