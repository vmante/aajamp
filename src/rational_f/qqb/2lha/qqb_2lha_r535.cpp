#include "qqb_2lha_rf_decl.hpp"

template<class T>
T qqb_2lha_r535(const std::array<T,30>& k) {
  T z[12];
  T r = 0;

    z[1]=k[4];
    z[2]=k[5];
    z[3]=k[9];
    z[4]=k[11];
    z[5]=k[12];
    z[6]=k[7];
    z[7]=static_cast<T>(1)+ n<T>(1,4)*z[2];
    z[7]=z[7]*z[2];
    z[7]=z[7] + n<T>(3,4);
    z[8]=z[3] - 1;
    z[9]=z[8]*z[3];
    z[10]= - z[9]*z[7];
    z[7]=z[1]*z[7]*npow(z[3],2);
    z[10]= - z[7] + z[10];
    z[11]=3*z[5];
    z[10]=z[11]*z[10];
    z[11]=static_cast<T>(3)+ z[2];
    z[9]=z[2]*z[11]*z[9];
    z[7]=z[4]*z[7];
    z[7]=z[7] + z[9] + z[10];
    z[8]= - z[6]*z[8];
    z[7]=n<T>(1,2)*z[7] + z[8];

    r += n<T>(1,2)*z[7];
 
    return r;
}

template double qqb_2lha_r535(const std::array<double,30>&);

#ifdef HAVE_QD
template dd_real qqb_2lha_r535(const std::array<dd_real,30>&);
#endif
