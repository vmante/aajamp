#ifndef RATFUNS_EVALUATOR_H
#define RATFUNS_EVALUATOR_H

#include <array>
#include <complex>

/* ----------------------------- qqb ----------------------------- */

template <class T>
void evaluate_0l_rf_qqgaa(const std::array<T,31>&, std::array<T,1>&);

template <class T>
void evaluate_1l_rf_qqgaa(const std::array<T,31>&, std::array<T,55>&);

template <class T>
void evaluate_1lha_rf_qqgaa(const std::array<T,31>&, std::array<T,292>&);

template <class T>
void evaluate_2lLC_rf_qqgaa(const std::array<T,31>&, std::array<T,343>&);

#ifdef FULLCOL_ENABLED
template <class T>
void evaluate_2lNLC_rf_qqgaa(const std::array<T,31>&, std::array<T,1365>&);
#endif

#ifdef LOOPSQ_ENABLED
template <class T>
void evaluate_2lha_rf_qqgaa(const std::array<T,30>&, std::array<T,2438>&);
#endif

/* ----------------------------- qg ----------------------------- */
template <class T>
void evaluate_0l_rf_qgqaa(const std::array<T,31>&, std::array<T,1>&);

template <class T>
void evaluate_1l_rf_qgqaa(const std::array<T,31>&, std::array<T,55>&);

template <class T>
void evaluate_1lha_rf_qgqaa(const std::array<T,31>&, std::array<T,275>&);

template <class T>
void evaluate_2lLC_rf_qgqaa(const std::array<T,31>&, std::array<T,223>&);

#ifdef FULLCOL_ENABLED
template <class T>
void evaluate_2lNLC_rf_qgqaa(const std::array<T,31>&, std::array<T,1486>&);
#endif

#ifdef LOOPSQ_ENABLED
template <class T>
void evaluate_2lha_rf_qgqaa(const std::array<T,30>&, std::array<T,2436>&);
#endif

#endif /* RATFUNS_EVALUATOR_H */
