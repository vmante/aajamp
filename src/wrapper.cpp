#include "wrapper.hpp"

AAJObject aajamp_init() {
  aajAmp<double> *amp = new aajAmp<double>;
  return static_cast<AAJObject> (amp);
}

void aajamp_destroy(AAJObject amp) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  delete(myamp);
  return;
}

void aajamp_register_process(AAJObject amp, int idproc) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->register_process(idproc);
  return;
}

void aajamp_register_loop_order(AAJObject amp, int ord) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->register_loop_order(ord);
  return;
}

void aajamp_register_event(AAJObject amp, const double sij[5], const double musq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  std::array<double,5> kin = {sij[0], sij[1], sij[2], sij[3], sij[4]};
  myamp->register_event(kin, musq);
  return;
}

/* Tree-level: qqb */
void aajamp_qqgaa_W00(AAJObject amp, double &born) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W00(born);
  return;
}

/* Tree-level: qg */
void aajamp_qgqaa_W00(AAJObject amp, double &born) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W00(born);
  return;
}

/* One Loop: qqb */
void aajamp_qqgaa_W01_CA(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W01_CA(nlo);
  return;
}

void aajamp_qqgaa_W01_CF(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W01_CF(nlo);
  return;
}

void aajamp_qqgaa_W01_nfaa(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W01_nfaa(nlo);
  return;
}

void aajamp_qqgaa_W01_nf(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W01_nf(nlo);
  return;
}

/* One Loop: qg */
void aajamp_qgqaa_W01_CA(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W01_CA(nlo);
  return;
}

void aajamp_qgqaa_W01_CF(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W01_CF(nlo);
  return;
}

void aajamp_qgqaa_W01_nfaa(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W01_nfaa(nlo);
  return;
}

void aajamp_qgqaa_W01_nf(AAJObject amp, double &nlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W01_nf(nlo);
  return;
}

/* One Loop^2: qqb */
void aajamp_qqgaa_W11_CACA(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_CACA(nlosq);
  return;
}

void aajamp_qqgaa_W11_CFCF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_CFCF(nlosq);
  return;
}

void aajamp_qqgaa_W11_nfaanfaa(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_nfaanfaa(nlosq);
  return;
}

void aajamp_qqgaa_W11_nfnf(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_nfnf(nlosq);
  return;
}

void aajamp_qqgaa_W11_CACF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_CACF(nlosq);
  return;
}

void aajamp_qqgaa_W11_nfaaCA(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_nfaaCA(nlosq);
  return;
}

void aajamp_qqgaa_W11_nfCA(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_nfCA(nlosq);
  return;
}

void aajamp_qqgaa_W11_nfaaCF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_nfaaCF(nlosq);
  return;
}

void aajamp_qqgaa_W11_nfCF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_nfCF(nlosq);
  return;
}

void aajamp_qqgaa_W11_nfnfaa(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W11_nfnfaa(nlosq);
  return;
}

/* One Loop^2: qg */
void aajamp_qgqaa_W11_CACA(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_CACA(nlosq);
  return;
}

void aajamp_qgqaa_W11_CFCF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_CFCF(nlosq);
  return;
}

void aajamp_qgqaa_W11_nfaanfaa(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_nfaanfaa(nlosq);
  return;
}

void aajamp_qgqaa_W11_nfnf(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_nfnf(nlosq);
  return;
}

void aajamp_qgqaa_W11_CACF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_CACF(nlosq);
  return;
}

void aajamp_qgqaa_W11_nfaaCA(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_nfaaCA(nlosq);
  return;
}

void aajamp_qgqaa_W11_nfCA(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_nfCA(nlosq);
  return;
}

void aajamp_qgqaa_W11_nfaaCF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_nfaaCF(nlosq);
  return;
}

void aajamp_qgqaa_W11_nfCF(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_nfCF(nlosq);
  return;
}

void aajamp_qgqaa_W11_nfnfaa(AAJObject amp, double &nlosq) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W11_nfnfaa(nlosq);
  return;
}

/* Two Loop: qqb */
void aajamp_qqgaa_W02_Nc2(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_Nc2(nnlo);
  return;
}

void aajamp_qqgaa_W02_nfCA(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_nfCA(nnlo);
  return;
}

void aajamp_qqgaa_W02_nfCF(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_nfCF(nnlo);
  return;
}

void aajamp_qqgaa_W02_nfnfaa(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_nfnfaa(nnlo);
  return;
}

void aajamp_qqgaa_W02_nfnf(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_nfnf(nnlo);
  return;
}

void aajamp_qqgaa_W02_nfa(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_nfa(nnlo);
  return;
}

#ifdef FULLCOL_ENABLED
void aajamp_qqgaa_W02_Nc0(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_Nc0(nnlo);
  return;
}

void aajamp_qqgaa_W02_Ncm2(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_Ncm2(nnlo);
  return;
}

void aajamp_qqgaa_W02_nfaaNc(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_nfaaNc(nnlo);
  return;
}

void aajamp_qqgaa_W02_nfaaNcm1(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W02_nfaaNcm1(nnlo);
  return;
}
#endif

/* Two Loop: qg */
void aajamp_qgqaa_W02_Nc2(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_Nc2(nnlo);
  return;
}

void aajamp_qgqaa_W02_nfCA(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_nfCA(nnlo);
  return;
}

void aajamp_qgqaa_W02_nfCF(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_nfCF(nnlo);
  return;
}

void aajamp_qgqaa_W02_nfnfaa(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_nfnfaa(nnlo);
  return;
}

void aajamp_qgqaa_W02_nfnf(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_nfnf(nnlo);
  return;
}

#ifdef FULLCOL_ENABLED
void aajamp_qgqaa_W02_Nc0(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_Nc0(nnlo);
  return;
}

void aajamp_qgqaa_W02_Ncm2(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_Ncm2(nnlo);
  return;
}

void aajamp_qgqaa_W02_nfaaNc(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_nfaaNc(nnlo);
  return;
}

void aajamp_qgqaa_W02_nfaaNcm1(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_nfaaNcm1(nnlo);
  return;
}

void aajamp_qgqaa_W02_nfa(AAJObject amp, double &nnlo) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W02_nfa(nnlo);
  return;
}
#endif

#ifdef LOOPSQ_ENABLED
void aajamp_qqgaa_W12_nfaa2Ncp1(AAJObject amp, double &w12int) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W12_nfaa2Ncp1(w12int);
  return;
}

void aajamp_qqgaa_W12_nfaa2Ncm1(AAJObject amp, double &w12int) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W12_nfaa2Ncm1(w12int);
  return;
}

void aajamp_qqgaa_W12_nfaa2nf(AAJObject amp, double &w12int) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qqgaa_W12_nfaa2nf(w12int);
  return;
}

void aajamp_qgqaa_W12_nfaa2Ncp1(AAJObject amp, double &w12int) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W12_nfaa2Ncp1(w12int);
  return;
}

void aajamp_qgqaa_W12_nfaa2Ncm1(AAJObject amp, double &w12int) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W12_nfaa2Ncm1(w12int);
  return;
}

void aajamp_qgqaa_W12_nfaa2nf(AAJObject amp, double &w12int) {
  aajAmp<double> *myamp = static_cast<aajAmp<double>*>(amp);
  myamp->qgqaa_W12_nfaa2nf(w12int);
  return;
}
#endif
