#include "helamplitudes.hpp"

template<class T>
std::complex<T> qqb_HA1L_LmmpE_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+=  4*hr[48];
  r+=  hr[133]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+=  (hr[82]*qqb_l1_tf_CA_3(fw1))/3;
  r+=  (2*hr[250]*qqb_l1_tf_CA_4(fw1))/3;
  r+=  2*hr[129]*qqb_l1_tf_CA_5(tcs,fw1);
  r+=  hr[96]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+=  hr[176]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= -hr[248]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+=  hr[143]*qqb_l1_tf_CA_29(tcs,fw1,fw2);
  r+=  hr[136]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+=  qqb_l1_tf_CA_34(tcs,fw1,fw2);
  r+=  (hr[259]*qqb_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+=  (hr[143]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= -(hr[248]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+=  (hr[273]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+=  (hr[259]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+=  (hr[259]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+=  (hr[276]*qqb_l1_tf_CF_27(fw1))/3;
  r+=  (hr[15]*qqb_l1_tf_CF_28(fw1))/3;
  r+=  (hr[177]*qqb_l1_tf_CF_30(tcs,fw1,fw2))/2;
  r+= -(hr[248]*qqb_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r+= -(10*qqb_l1_tf_CF_36(fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmpO_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[119];
  r+= hr[235]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= hr[156]*qqb_l1_tf_CA_3(fw1);
  r+= -2*hr[211]*qqb_l1_tf_CA_4(fw1);
  r+= -2*hr[212]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= hr[160]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+= -hr[220]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[290]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= -hr[220]*qqb_l1_tf_CA_29(tcs,fw1,fw2);
  r+= hr[195]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+= (hr[233]*qqb_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= -(hr[220]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[290]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[290]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[233]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[233]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= hr[157]*qqb_l1_tf_CF_27(fw1);
  r+= -hr[81]*qqb_l1_tf_CF_28(fw1);
  r+= (hr[290]*qqb_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmpmE_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[54];
  r+= hr[8]*qqb_l1_tf_CA_3(fw1)/3;
  r+= hr[88]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+= hr[185]*qqb_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[226]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[137]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= hr[271]*qqb_l1_tf_CA_24(tcs,fw1,fw2);
  r+= hr[166]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= qqb_l1_tf_CA_34(tcs,fw1,fw2);
  r+= hr[271]*qqb_l1_tf_CF_13(tcs,fw1,fw2)/2;
  r+= -qqb_l1_tf_CF_14(tcs,fw1,fw2);
  r+=  hr[260]*qqb_l1_tf_CF_15(tcs,fw1,fw2)/2;
  r+=  hr[226]*qqb_l1_tf_CF_16(tcs,fw1,fw2)/2;
  r+= -hr[226]*qqb_l1_tf_CF_19(tcs,fw1,fw2)/2;
  r+=  hr[260]*qqb_l1_tf_CF_23(tcs,fw1,fw2)/4;
  r+=  hr[260]*qqb_l1_tf_CF_24(tcs,fw1)/4;
  r+= -(10*qqb_l1_tf_CF_27(fw1))/3;
  r+=  hr[8]*qqb_l1_tf_CF_28(fw1)/3;
  r+=  hr[19]*qqb_l1_tf_CF_29(fw1);
  r+= -hr[184]*qqb_l1_tf_CF_31(tcs,fw1,fw2)/2;
  r+=  hr[144]*qqb_l1_tf_CF_33(tcs,fw1)/2;
  r+= -hr[226]*qqb_l1_tf_CF_34(tcs,fw1,fw2)/2;
  r+= -(10*qqb_l1_tf_CF_36(fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmpmO_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -4*hr[110];
  r+=  hr[63]*qqb_l1_tf_CA_3(fw1);
  r+= -hr[150]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+=  hr[283]*qqb_l1_tf_CA_14(tcs,fw1,fw2);
  r+=  hr[286]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= -hr[201]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= -hr[231]*qqb_l1_tf_CA_24(tcs,fw1,fw2);
  r+=  hr[231]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= -hr[231]*qqb_l1_tf_CF_13(tcs,fw1,fw2)/2;
  r+=  hr[282]*qqb_l1_tf_CF_15(tcs,fw1,fw2)/2;
  r+=  hr[286]*qqb_l1_tf_CF_16(tcs,fw1,fw2)/2;
  r+= -hr[286]*qqb_l1_tf_CF_19(tcs,fw1,fw2)/2;
  r+=  hr[282]*qqb_l1_tf_CF_23(tcs,fw1,fw2)/4;
  r+=  hr[282]*qqb_l1_tf_CF_24(tcs,fw1)/4;
  r+=  hr[63]*qqb_l1_tf_CF_28(fw1);
  r+= -hr[77]*qqb_l1_tf_CF_29(fw1);
  r+=  hr[221]*qqb_l1_tf_CF_33(tcs,fw1)/2;
  r+= -hr[286]*qqb_l1_tf_CF_34(tcs,fw1,fw2)/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmmE_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[70]*qqb_l1_tf_CA_4(fw1);
  r+= 2*hr[135]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= - hr[246]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= hr[267]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+= - hr[246]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+= hr[84]*qqb_l1_tf_CA_11(fw1);
  r+= - 2*qqb_l1_tf_CA_12(tcs,fw1,fw2);
  r+= hr[148]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[269]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= hr[186]*qqb_l1_tf_CA_18(tcs,fw1,fw2);
  r+= hr[187]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[171]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= - hr[267]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[246]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[108]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= - qqb_l1_tf_CA_35(tcs,fw1);
  r+= - (hr[246]*qqb_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= (hr[280]*qqb_l1_tf_CF_27(fw1))/3;
  r+= (hr[278]*qqb_l1_tf_CF_36(fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmmO_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -2*hr[154]*qqb_l1_tf_CA_4(fw1);
  r+= 2*hr[197]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= hr[288]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= - hr[288]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+= hr[288]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+= - hr[159]*qqb_l1_tf_CA_11(fw1);
  r+= hr[210]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[227]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= hr[285]*qqb_l1_tf_CA_18(tcs,fw1,fw2);
  r+= - hr[284]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[228]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= hr[288]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= - hr[288]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[179]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[288]*qqb_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= hr[209]*qqb_l1_tf_CF_27(fw1);
  r+= hr[209]*qqb_l1_tf_CF_36(fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmppE_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= hr[249]*qqb_l1_tf_CA_3(fw1)/3;
  r+= 2*hr[141]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= hr[178]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= hr[267]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+= - hr[246]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+= hr[116]*qqb_l1_tf_CA_12(tcs,fw1,fw2);
  r+= hr[115]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[267]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= - hr[158]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= - 2*qqb_l1_tf_CA_18(tcs,fw1,fw2);
  r+= hr[175]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[255]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[247]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[130]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= - qqb_l1_tf_CA_35(tcs,fw1);
  r+= (hr[267]*qqb_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= - (hr[267]*qqb_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[246]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= - (hr[267]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= (hr[246]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= (hr[66]*qqb_l1_tf_CF_27(fw1))/3;
  r+= hr[76]*qqb_l1_tf_CF_28(fw1);
  r+= - (10*qqb_l1_tf_CF_36(fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmppO_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= hr[218]*qqb_l1_tf_CA_3(fw1);
  r+= 2*hr[219]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= - hr[194]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= hr[288]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+= - hr[288]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+= hr[193]*qqb_l1_tf_CA_12(tcs,fw1,fw2);
  r+= - hr[200]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[288]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= - hr[225]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[234]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[287]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[289]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[192]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[288]*qqb_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= -(hr[288]*qqb_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[288]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= -(hr[288]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= (hr[288]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= hr[152]*qqb_l1_tf_CF_27(fw1);
  r+= hr[165]*qqb_l1_tf_CF_28(fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmpE_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= hr[87];
  r+= hr[180]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= (hr[161]*qqb_l1_tf_CA_4(fw1))/12;
  r+= hr[272]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+= hr[83]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[134]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= hr[258]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[173]*qqb_l1_tf_CA_30(tcs,fw1,fw2);
  r+= qqb_l1_tf_CA_34(tcs,fw1,fw2);
  r+= (hr[272]*qqb_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= (hr[264]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= -qqb_l1_tf_CF_15(tcs,fw1,fw2);
  r+= (hr[191]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= (hr[180]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[263]*qqb_l1_tf_CF_19(tcs,fw1,fw2))/2;
  r+= -(hr[180]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= -(hr[180]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (10*qqb_l1_tf_CF_26(fw1))/3;
  r+= (hr[281]*qqb_l1_tf_CF_27(fw1))/24;
  r+= (hr[24]*qqb_l1_tf_CF_28(fw1))/8;
  r+= (hr[272]*qqb_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= (hr[163]*qqb_l1_tf_CF_33(tcs,fw1))/2;
  r+= (hr[261]*qqb_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r+= (hr[67]*qqb_l1_tf_CF_36(fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmpO_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[169];
  r+= hr[242]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= hr[222]*qqb_l1_tf_CA_4(fw1);
  r+= hr[286]*qqb_l1_tf_CA_10(tcs,fw1,fw2);
  r+= hr[170]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= -hr[190]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= -hr[244]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[240]*qqb_l1_tf_CA_30(tcs,fw1,fw2);
  r+= (hr[286]*qqb_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[286]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= (hr[243]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= (hr[242]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= -(hr[202]*qqb_l1_tf_CF_19(tcs,fw1,fw2))/2;
  r+= (hr[245]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[245]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[222]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[97]*qqb_l1_tf_CF_28(fw1))/2;
  r+= (hr[286]*qqb_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= (hr[224]*qqb_l1_tf_CF_33(tcs,fw1))/2;
  r+= hr[139]*qqb_l1_tf_CF_36(fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LppmE_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[59];
  r+= hr[168]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= (hr[277]*qqb_l1_tf_CA_3(fw1))/3;
  r+= 2*hr[145]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= hr[273]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+= (hr[75]*qqb_l1_tf_CA_11(fw1))/3;
  r+= hr[43]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[273]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[164]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[256]*qqb_l1_tf_CA_31(tcs,fw1,fw2);
  r+= hr[111]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+= qqb_l1_tf_CA_34(tcs,fw1,fw2);
  r+= (hr[167]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= (hr[266]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[253]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[248]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= -(hr[168]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= -(hr[168]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[279]*qqb_l1_tf_CF_27(fw1))/3;
  r+= (hr[277]*qqb_l1_tf_CF_28(fw1))/3;
  r+= (hr[14]*qqb_l1_tf_CF_29(fw1))/3;
  r+= (hr[265]*qqb_l1_tf_CF_30(tcs,fw1,fw2))/2;
  r+= (hr[257]*qqb_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r+= (hr[75]*qqb_l1_tf_CF_36(fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LppmO_CA(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[121];
  r+= -hr[215]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= hr[204]*qqb_l1_tf_CA_3(fw1);
  r+= 2*hr[203]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= -hr[290]*qqb_l1_tf_CA_7(tcs,fw1,fw2);
  r+= -hr[153]*qqb_l1_tf_CA_11(fw1);
  r+= -hr[114]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= -hr[290]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[230]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[236]*qqb_l1_tf_CA_31(tcs,fw1,fw2);
  r+= hr[181]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+= -(hr[238]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= (hr[291]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[229]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[290]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[215]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[215]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= hr[205]*qqb_l1_tf_CF_27(fw1);
  r+= hr[204]*qqb_l1_tf_CF_28(fw1);
  r+= -hr[78]*qqb_l1_tf_CF_29(fw1);
  r+= (hr[290]*qqb_l1_tf_CF_30(tcs,fw1,fw2))/2;
  r+= -hr[153]*qqb_l1_tf_CF_36(fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpppE_CA(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+=hr[112]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpppO_CA(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+=hr[189]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmmE_CA(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+=hr[89]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmmO_CA(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+=hr[174]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmpE_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[36];
  r+= hr[133]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= (hr[56]*qqb_l1_tf_CA_3(fw1))/2;
  r+= hr[252]*qqb_l1_tf_CA_4(fw1);
  r+= 2*hr[129]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= hr[248]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[176]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= -hr[248]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[172]*qqb_l1_tf_CA_29(tcs,fw1,fw2);
  r+= hr[136]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+= (hr[259]*qqb_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= (hr[172]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= -(hr[248]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[273]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[177]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[259]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[251]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[37]*qqb_l1_tf_CF_28(fw1))/2;
  r+= (hr[177]*qqb_l1_tf_CF_30(tcs,fw1,fw2))/2;
  r+= (hr[138]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[273]*qqb_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmpO_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[99];
  r+= hr[235]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= -(hr[128]*qqb_l1_tf_CA_3(fw1))/2;
  r+= hr[213]*qqb_l1_tf_CA_4(fw1);
  r+= -2*hr[212]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= -hr[290]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= -hr[220]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[290]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[241]*qqb_l1_tf_CA_29(tcs,fw1,fw2);
  r+= hr[195]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+= (hr[233]*qqb_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= (hr[241]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[290]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[290]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[233]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[233]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[127]*qqb_l1_tf_CF_27(fw1))/2;
  r+= -(hr[107]*qqb_l1_tf_CF_28(fw1))/2;
  r+= (hr[216]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[290]*qqb_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmpmE_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[32];
  r+= (hr[4]*qqb_l1_tf_CA_3(fw1))/2;
  r+= hr[185]*qqb_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[137]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= hr[105]*qqb_l1_tf_CA_24(tcs,fw1,fw2);
  r+= hr[166]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= (hr[105]*qqb_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -qqb_l1_tf_CF_14(tcs,fw1,fw2);
  r+= (hr[260]*qqb_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= (hr[226]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= -(hr[226]*qqb_l1_tf_CF_19(tcs,fw1,fw2))/2;
  r+= (hr[184]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[260]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= 3*qqb_l1_tf_CF_27(fw1);
  r+= (hr[4]*qqb_l1_tf_CF_28(fw1))/2;
  r+= (hr[17]*qqb_l1_tf_CF_29(fw1))/2;
  r+= -(hr[184]*qqb_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= (hr[117]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[144]*qqb_l1_tf_CF_33(tcs,fw1))/2;
  r+= (hr[272]*qqb_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmpmO_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[90];
  r+= (hr[49]*qqb_l1_tf_CA_3(fw1))/2;
  r+= hr[283]*qqb_l1_tf_CA_14(tcs,fw1,fw2);
  r+= -hr[201]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= -hr[188]*qqb_l1_tf_CA_24(tcs,fw1,fw2);
  r+= hr[231]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= -(hr[188]*qqb_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= (hr[282]*qqb_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= (hr[286]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= -(hr[286]*qqb_l1_tf_CF_19(tcs,fw1,fw2))/2;
  r+= (hr[282]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[282]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[49]*qqb_l1_tf_CF_28(fw1))/2;
  r+= -(hr[73]*qqb_l1_tf_CF_29(fw1))/2;
  r+= -(hr[196]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[221]*qqb_l1_tf_CF_33(tcs,fw1))/2;
  r+= -(hr[286]*qqb_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmmE_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[46];
  r+= hr[29]*qqb_l1_tf_CA_4(fw1);
  r+= 2*hr[135]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= -hr[246]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= (hr[58]*qqb_l1_tf_CA_11(fw1))/2;
  r+= -2*qqb_l1_tf_CA_12(tcs,fw1,fw2);
  r+= hr[148]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[269]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= hr[186]*qqb_l1_tf_CA_18(tcs,fw1,fw2);
  r+= hr[187]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[171]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= hr[108]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= -(hr[246]*qqb_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= (hr[267]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= -(hr[246]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= qqb_l1_tf_CF_24(tcs,fw1)/2;
  r+= (hr[147]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[275]*qqb_l1_tf_CF_36(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmmO_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -2*hr[113];
  r+= hr[104]*qqb_l1_tf_CA_4(fw1);
  r+= 2*hr[197]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= hr[288]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= -(hr[124]*qqb_l1_tf_CA_11(fw1))/2;
  r+= hr[210]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[227]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= hr[285]*qqb_l1_tf_CA_18(tcs,fw1,fw2);
  r+= -hr[284]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[228]*qqb_l1_tf_CA_25(tcs,fw1,fw2);
  r+= hr[179]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[288]*qqb_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= -(hr[288]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= (hr[288]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= (hr[208]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[208]*qqb_l1_tf_CF_36(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmppE_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[74];
  r+= (hr[140]*qqb_l1_tf_CA_3(fw1))/2;
  r+= 2*hr[141]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= hr[178]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= hr[116]*qqb_l1_tf_CA_12(tcs,fw1,fw2);
  r+= hr[115]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[267]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= -hr[158]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= -2*qqb_l1_tf_CA_18(tcs,fw1,fw2);
  r+= hr[175]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[175]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[158]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[130]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[267]*qqb_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= -(hr[267]*qqb_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[246]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= qqb_l1_tf_CF_24(tcs,fw1)/2;
  r+= (hr[51]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[44]*qqb_l1_tf_CF_28(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmppO_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -2*hr[149];
  r+= (hr[217]*qqb_l1_tf_CA_3(fw1))/2;
  r+= 2*hr[219]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= -hr[194]*qqb_l1_tf_CA_6(tcs,fw1,fw2);
  r+= hr[193]*qqb_l1_tf_CA_12(tcs,fw1,fw2);
  r+= -hr[200]*qqb_l1_tf_CA_15(tcs,fw1);
  r+= hr[288]*qqb_l1_tf_CA_16(tcs,fw1,fw2);
  r+= -hr[225]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[234]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[234]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[225]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[192]*qqb_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[288]*qqb_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= -(hr[288]*qqb_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[288]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= -(hr[122]*qqb_l1_tf_CF_27(fw1))/2;
  r+= -(hr[123]*qqb_l1_tf_CF_28(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmpE_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[47];
  r+= hr[180]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= hr[162]*qqb_l1_tf_CA_4(fw1);
  r+= hr[258]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[134]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= hr[258]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[106]*qqb_l1_tf_CA_30(tcs,fw1,fw2);
  r+= (hr[272]*qqb_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= (hr[264]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= -qqb_l1_tf_CF_15(tcs,fw1,fw2);
  r+= (hr[191]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= (hr[180]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[262]*qqb_l1_tf_CF_19(tcs,fw1,fw2))/2;
  r+= (hr[270]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= -(hr[180]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= 3*qqb_l1_tf_CF_26(fw1);
  r+= (hr[162]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[20]*qqb_l1_tf_CF_28(fw1))/2;
  r+= -(hr[226]*qqb_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= (hr[142]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[163]*qqb_l1_tf_CF_33(tcs,fw1))/2;
  r+= (hr[261]*qqb_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r+= (hr[30]*qqb_l1_tf_CF_36(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmpO_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[118];
  r+= hr[242]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= hr[223]*qqb_l1_tf_CA_4(fw1);
  r+= -hr[244]*qqb_l1_tf_CA_19(tcs,fw1,fw2);
  r+= -hr[190]*qqb_l1_tf_CA_21(tcs,fw1,fw2);
  r+= -hr[244]*qqb_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[182]*qqb_l1_tf_CA_30(tcs,fw1,fw2);
  r+= (hr[286]*qqb_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[286]*qqb_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= (hr[243]*qqb_l1_tf_CF_16(tcs,fw1,fw2))/2;
  r+= (hr[242]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[239]*qqb_l1_tf_CF_19(tcs,fw1,fw2))/2;
  r+= (hr[245]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[245]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[223]*qqb_l1_tf_CF_27(fw1))/2;
  r+= -(hr[92]*qqb_l1_tf_CF_28(fw1))/2;
  r+= (hr[286]*qqb_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= (hr[214]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[224]*qqb_l1_tf_CF_33(tcs,fw1))/2;
  r+= (hr[101]*qqb_l1_tf_CF_36(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LppmE_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[38];
  r+= hr[168]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= (hr[274]*qqb_l1_tf_CA_3(fw1))/2;
  r+= 2*hr[145]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= (hr[55]*qqb_l1_tf_CA_11(fw1))/2;
  r+= -hr[164]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[273]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[164]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[183]*qqb_l1_tf_CA_31(tcs,fw1,fw2);
  r+= hr[111]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+= (hr[167]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= (hr[266]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[254]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[248]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[268]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= -(hr[168]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[146]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[274]*qqb_l1_tf_CF_28(fw1))/2;
  r+= (hr[16]*qqb_l1_tf_CF_29(fw1))/2;
  r+= (hr[248]*qqb_l1_tf_CF_30(tcs,fw1,fw2))/2;
  r+= (hr[131]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[257]*qqb_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r+= (hr[132]*qqb_l1_tf_CF_36(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LppmO_CF(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[100];
  r+= -hr[215]*qqb_l1_tf_CA_2(tcs,fw1,fw2);
  r+= (hr[206]*qqb_l1_tf_CA_3(fw1))/2;
  r+= 2*hr[203]*qqb_l1_tf_CA_5(tcs,fw1);
  r+= -(hr[126]*qqb_l1_tf_CA_11(fw1))/2;
  r+= -hr[230]*qqb_l1_tf_CA_17(tcs,fw1,fw2);
  r+= -hr[290]*qqb_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[230]*qqb_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[237]*qqb_l1_tf_CA_31(tcs,fw1,fw2);
  r+= hr[181]*qqb_l1_tf_CA_32(tcs,fw1,fw2);
  r+= -(hr[238]*qqb_l1_tf_CF_17(tcs,fw1,fw2))/2;
  r+= (hr[291]*qqb_l1_tf_CF_18(tcs,fw1,fw2))/2;
  r+= (hr[232]*qqb_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[290]*qqb_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[215]*qqb_l1_tf_CF_23(tcs,fw1,fw2))/4;
  r+= (hr[215]*qqb_l1_tf_CF_24(tcs,fw1))/4;
  r+= (hr[207]*qqb_l1_tf_CF_27(fw1))/2;
  r+= (hr[206]*qqb_l1_tf_CF_28(fw1))/2;
  r+= (hr[72]*qqb_l1_tf_CF_29(fw1))/2;
  r+= (hr[290]*qqb_l1_tf_CF_30(tcs,fw1,fw2))/2;
  r+= (hr[198]*qqb_l1_tf_CF_32(tcs,fw1))/2;
  r+= (hr[199]*qqb_l1_tf_CF_36(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpppE_CF(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[71]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpppO_CF(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[155]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmmE_CF(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[64]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmmO_CF(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[151]/4;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmpE_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[62];
  r+= (hr[31]*qqb_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= (hr[39]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= (hr[25]*qqb_l1_tf_nfaa_6(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmpO_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[125];
  r+= (hr[95]*qqb_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= (hr[103]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= (hr[80]*qqb_l1_tf_nfaa_6(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmpmE_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[7];
  r+= (hr[26]*qqb_l1_tf_nfaa_3(tcs,fw1,fw2))/2;
  r+= (hr[1]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= (hr[18]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmpmO_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[50];
  r+= -(hr[91]*qqb_l1_tf_nfaa_3(tcs,fw1,fw2))/2;
  r+= -(hr[21]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= -(hr[65]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmmE_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[10];
  r+= (hr[6]*qqb_l1_tf_nfaa_4(tcs,fw1,fw2))/2;
  r+= (hr[2]*qqb_l1_tf_nfaa_6(fw1))/2;
  r+= (hr[13]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmmO_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[57];
  r+= -(hr[52]*qqb_l1_tf_nfaa_4(tcs,fw1,fw2))/2;
  r+= (hr[22]*qqb_l1_tf_nfaa_6(fw1))/2;
  r+= -(hr[68]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmppE_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[45];
  r+= (hr[33]*qqb_l1_tf_nfaa_4(tcs,fw1,fw2))/2;
  r+= (hr[12]*qqb_l1_tf_nfaa_6(fw1))/2;
  r+= (hr[40]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmppO_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -8*hr[98];
  r+= (hr[94]*qqb_l1_tf_nfaa_4(tcs,fw1,fw2))/2;
  r+= -(hr[61]*qqb_l1_tf_nfaa_6(fw1))/2;
  r+= (hr[102]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmpE_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[34];
  r+= (hr[35]*qqb_l1_tf_nfaa_3(tcs,fw1,fw2))/2;
  r+= (hr[9]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= (hr[27]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpmpO_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[86];
  r+= (hr[109]*qqb_l1_tf_nfaa_3(tcs,fw1,fw2))/2;
  r+= (hr[60]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= (hr[79]*qqb_l1_tf_nfaa_7(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LppmE_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[28];
  r+= (hr[5]*qqb_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= (hr[11]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= (hr[3]*qqb_l1_tf_nfaa_6(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LppmO_nfaa(
  const std::array<T, 292>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -8*hr[85];
  r+= -(hr[53]*qqb_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= -(hr[69]*qqb_l1_tf_nfaa_5(fw1))/2;
  r+= -(hr[42]*qqb_l1_tf_nfaa_6(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpppE_nfaa(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+=hr[41]/2;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LpppO_nfaa(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[120]/2;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmmE_nfaa(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+= hr[23]/2;
  return r;
}

template<class T>
std::complex<T> qqb_HA1L_LmmmO_nfaa(
  const std::array<T, 292>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[93]/2;
  return r;
}

/*---------------------------------------------------------------------------*/
/*                                q g -> q a a                               */
/*---------------------------------------------------------------------------*/

template<class T>
std::complex<T> qg_HA1L_LmmpE_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[68];
  r+= hr[108]*qg_l1_tf_CA_2(fw1);
  r+= hr[127]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= (hr[4]*qg_l1_tf_CA_4(tcs,fw1))/3;
  r+= hr[173]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -qg_l1_tf_CA_10(fw1);
  r+= hr[98]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= -hr[238]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= qg_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[172]*qg_l1_tf_CA_32(tcs,fw1,fw2);
  r+= hr[81]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= qg_l1_tf_CF_9(tcs,fw1,fw2);
  r+= (hr[238]*qg_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[238]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[238]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[250]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[241]*qg_l1_tf_CF_26(tcs,fw1))/3;
  r+= -(hr[238]*qg_l1_tf_CF_32(tcs,fw1,fw2))/2;
  r+= -(10*qg_l1_tf_CF_33(fw1))/3;
  r+= (hr[141]*qg_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r+= (hr[44]*qg_l1_tf_CF_36(tcs,fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmpO_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[126];
  r+= hr[192]*qg_l1_tf_CA_2(fw1);
  r+= hr[271]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= hr[59]*qg_l1_tf_CA_4(tcs,fw1);
  r+= -hr[210]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[178]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= -hr[272]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= -hr[210]*qg_l1_tf_CA_32(tcs,fw1,fw2);
  r+= -hr[169]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[272]*qg_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[272]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[272]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= -(hr[217]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= hr[192]*qg_l1_tf_CF_26(tcs,fw1);
  r+= -(hr[272]*qg_l1_tf_CF_32(tcs,fw1,fw2))/2;
  r+= hr[119]*qg_l1_tf_CF_36(tcs,fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmpmE_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[65];
  r+= hr[237]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= hr[131]*qg_l1_tf_CA_6(fw1);
  r+= -qg_l1_tf_CA_10(fw1);
  r+= hr[157]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= (hr[266]*qg_l1_tf_CA_12(fw1))/3;
  r+= hr[93]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[251]*qg_l1_tf_CA_16(tcs,fw1,fw2);
  r+= -(hr[45]*qg_l1_tf_CA_18(tcs,fw1))/3;
  r+= hr[237]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= qg_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[54]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= hr[177]*qg_l1_tf_CA_34(tcs,fw1,fw2);
  r+= -(hr[237]*qg_l1_tf_CF_6(tcs,fw1,fw2))/2;
  r+= qg_l1_tf_CF_8(tcs,fw1,fw2);
  r+= (hr[263]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= -(hr[237]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[236]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[16]*qg_l1_tf_CF_30(tcs,fw1))/3;
  r+= -(hr[237]*qg_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= -(10*qg_l1_tf_CF_33(fw1))/3;
  r+= (hr[259]*qg_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r+= (hr[45]*qg_l1_tf_CF_36(tcs,fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmpmO_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[136];
  r+= -hr[269]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= -hr[201]*qg_l1_tf_CA_6(fw1);
  r+= -hr[219]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= hr[202]*qg_l1_tf_CA_12(fw1);
  r+= hr[179]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[270]*qg_l1_tf_CA_16(tcs,fw1,fw2);
  r+= hr[118]*qg_l1_tf_CA_18(tcs,fw1);
  r+= -hr[269]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= -hr[138]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= -hr[219]*qg_l1_tf_CA_34(tcs,fw1,fw2);
  r+= (hr[269]*qg_l1_tf_CF_6(tcs,fw1,fw2))/2;
  r+= -(hr[269]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= (hr[269]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[231]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= hr[89]*qg_l1_tf_CF_30(tcs,fw1);
  r+= (hr[269]*qg_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= -hr[118]*qg_l1_tf_CF_36(tcs,fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmmE_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= hr[107]*qg_l1_tf_CA_2(fw1);
  r+= hr[146]*qg_l1_tf_CA_6(fw1);
  r+= hr[109]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= hr[183]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= -qg_l1_tf_CA_10(fw1);
  r+= hr[145]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= hr[101]*qg_l1_tf_CA_12(fw1);
  r+= hr[162]*qg_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[133]*qg_l1_tf_CA_20(tcs,fw1,fw2);
  r+= hr[262]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= 3*hr[62]*qg_l1_tf_CA_26(fw1);
  r+= hr[262]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[252]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= (hr[262]*qg_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[228]*qg_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[244]*qg_l1_tf_CF_33(fw1))/3;
  r+= -(10*qg_l1_tf_CF_36(tcs,fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmmO_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -(hr[197]*qg_l1_tf_CA_2(fw1));
  r+= -hr[216]*qg_l1_tf_CA_6(fw1);
  r+= -hr[182]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= -hr[239]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[275]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[229]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= hr[191]*qg_l1_tf_CA_12(fw1);
  r+= hr[230]*qg_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[207]*qg_l1_tf_CA_20(tcs,fw1,fw2);
  r+= -hr[275]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= -hr[275]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= -3*hr[164]*qg_l1_tf_CA_26(fw1);
  r+= -hr[275]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= -hr[275]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[275]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[275]*qg_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[275]*qg_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= hr[215]*qg_l1_tf_CF_33(fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmppE_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= hr[140]*qg_l1_tf_CA_2(fw1);
  r+= hr[71]*qg_l1_tf_CA_4(tcs,fw1);
  r+= hr[110]*qg_l1_tf_CA_6(fw1);
  r+= hr[122]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= hr[176]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= -qg_l1_tf_CA_10(fw1);
  r+= hr[142]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[80]*qg_l1_tf_CA_18(tcs,fw1);
  r+= hr[262]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= hr[175]*qg_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[112]*qg_l1_tf_CA_29(tcs,fw1,fw2);
  r+= hr[262]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[252]*qg_l1_tf_CF_8(tcs,fw1,fw2))/2;
  r+= (hr[228]*qg_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= (hr[262]*qg_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= -(10*qg_l1_tf_CF_33(fw1))/3;
  r+= (hr[243]*qg_l1_tf_CF_36(tcs,fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmppO_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -(hr[205]*qg_l1_tf_CA_2(fw1));
  r+= hr[206]*qg_l1_tf_CA_4(tcs,fw1);
  r+= hr[189]*qg_l1_tf_CA_6(fw1);
  r+= hr[180]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= hr[275]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[220]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[211]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= -hr[166]*qg_l1_tf_CA_18(tcs,fw1);
  r+= hr[275]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= hr[275]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= -hr[187]*qg_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[185]*qg_l1_tf_CA_29(tcs,fw1,fw2);
  r+= hr[275]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= hr[275]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= -(hr[275]*qg_l1_tf_CF_8(tcs,fw1,fw2))/2;
  r+= -(hr[275]*qg_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= (hr[275]*qg_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= hr[150]*qg_l1_tf_CF_36(tcs,fw1);
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmpE_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[121];
  r+= hr[24]*qg_l1_tf_CA_4(tcs,fw1);
  r+= hr[184]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= hr[144]*qg_l1_tf_CA_6(fw1);
  r+= hr[155]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= -qg_l1_tf_CA_10(fw1);
  r+= hr[263]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= hr[117]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[167]*qg_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[69]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= hr[75]*qg_l1_tf_CA_26(fw1);
  r+= qg_l1_tf_CA_27(tcs,fw1,fw2);
  r+= hr[263]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= -(hr[237]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= (hr[253]*qg_l1_tf_CF_12(tcs,fw1,fw2))/2;
  r+= (hr[257]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[233]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[168]*qg_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= (hr[253]*qg_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r+= -(10*qg_l1_tf_CF_36(tcs,fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmpO_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -4*hr[186];
  r+= hr[115]*qg_l1_tf_CA_4(tcs,fw1);
  r+= hr[240]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= hr[208]*qg_l1_tf_CA_6(fw1);
  r+= hr[213]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[269]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= -hr[181]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[213]*qg_l1_tf_CA_22(tcs,fw1,fw2);
  r+= hr[149]*qg_l1_tf_CA_25(tcs,fw1,fw2);
  r+= -3*hr[163]*qg_l1_tf_CA_26(fw1);
  r+= hr[269]*qg_l1_tf_CA_31(tcs,fw1,fw2);
  r+= -(hr[269]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= -(hr[269]*qg_l1_tf_CF_12(tcs,fw1,fw2))/2;
  r+= -(hr[222]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[268]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= -(hr[269]*qg_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LppmE_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[96];
  r+= hr[111]*qg_l1_tf_CA_2(fw1);
  r+= hr[254]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= -qg_l1_tf_CA_10(fw1);
  r+= (hr[61]*qg_l1_tf_CA_12(fw1))/3;
  r+= hr[148]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= hr[120]*qg_l1_tf_CA_15(tcs,fw1,fw2);
  r+= hr[174]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[258]*qg_l1_tf_CA_23(tcs,fw1,fw2);
  r+= hr[92]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= qg_l1_tf_CA_27(tcs,fw1,fw2);
  r+= -hr[254]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= (hr[254]*qg_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= -(hr[238]*qg_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= qg_l1_tf_CF_13(tcs,fw1,fw2);
  r+= (hr[264]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[255]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[242]*qg_l1_tf_CF_28(fw1))/3;
  r+= hr[41]*qg_l1_tf_CF_30(tcs,fw1);
  r+= (hr[161]*qg_l1_tf_CF_32(tcs,fw1,fw2))/2;
  r+= (hr[60]*qg_l1_tf_CF_33(fw1))/3;
  r+= (hr[254]*qg_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r+= -(10*qg_l1_tf_CF_36(tcs,fw1))/3;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LppmO_CA(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 4*hr[159];
  r+= -hr[194]*qg_l1_tf_CA_2(fw1);
  r+= -hr[272]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= hr[124]*qg_l1_tf_CA_12(fw1);
  r+= hr[193]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= hr[196]*qg_l1_tf_CA_15(tcs,fw1,fw2);
  r+= -hr[218]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[218]*qg_l1_tf_CA_23(tcs,fw1,fw2);
  r+= hr[152]*qg_l1_tf_CA_24(tcs,fw1,fw2);
  r+= hr[272]*qg_l1_tf_CA_33(tcs,fw1,fw2);
  r+= -(hr[272]*qg_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= (hr[272]*qg_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= (hr[227]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[274]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= -hr[194]*qg_l1_tf_CF_28(fw1);
  r+= -hr[123]*qg_l1_tf_CF_30(tcs,fw1);
  r+= -hr[125]*qg_l1_tf_CF_33(fw1);
  r+= -(hr[272]*qg_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpppE_CA(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= hr[104]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpppO_CA(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= hr[188]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmmE_CA(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= hr[76]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmmO_CA(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[165]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmpE_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[28];
  r+= hr[108]*qg_l1_tf_CA_2(fw1);
  r+= hr[127]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= (hr[2]*qg_l1_tf_CA_4(tcs,fw1))/2;
  r+= hr[173]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= hr[98]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= hr[130]*qg_l1_tf_CA_32(tcs,fw1,fw2);
  r+= qg_l1_tf_CF_7(fw1)/2;
  r+= qg_l1_tf_CF_9(tcs,fw1,fw2);
  r+= (hr[238]*qg_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[238]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[254]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[249]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[245]*qg_l1_tf_CF_26(tcs,fw1))/2;
  r+= (hr[158]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= -(hr[254]*qg_l1_tf_CF_32(tcs,fw1,fw2))/2;
  r+= (hr[141]*qg_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r+= (hr[34]*qg_l1_tf_CF_36(tcs,fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmpO_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -2*hr[88];
  r+= hr[192]*qg_l1_tf_CA_2(fw1);
  r+= hr[271]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= (hr[50]*qg_l1_tf_CA_4(tcs,fw1))/2;
  r+= -hr[210]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[178]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= hr[221]*qg_l1_tf_CA_32(tcs,fw1,fw2);
  r+= (hr[272]*qg_l1_tf_CF_11(tcs,fw1,fw2))/2;
  r+= (hr[272]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[272]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[224]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= hr[192]*qg_l1_tf_CF_26(tcs,fw1);
  r+= -(hr[212]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= -(hr[272]*qg_l1_tf_CF_32(tcs,fw1,fw2))/2;
  r+= (hr[103]*qg_l1_tf_CF_36(tcs,fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmpmE_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[35];
  r+= hr[237]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= hr[131]*qg_l1_tf_CA_6(fw1);
  r+= hr[157]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= (hr[15]*qg_l1_tf_CA_12(fw1))/2;
  r+= hr[93]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[251]*qg_l1_tf_CA_16(tcs,fw1,fw2);
  r+= (hr[31]*qg_l1_tf_CA_18(tcs,fw1))/2;
  r+= hr[137]*qg_l1_tf_CA_34(tcs,fw1,fw2);
  r+= -(hr[237]*qg_l1_tf_CF_6(tcs,fw1,fw2))/2;
  r+= qg_l1_tf_CF_7(fw1)/2;
  r+= qg_l1_tf_CF_8(tcs,fw1,fw2);
  r+= (hr[263]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= (hr[253]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[235]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[135]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= (hr[265]*qg_l1_tf_CF_30(tcs,fw1))/2;
  r+= (hr[253]*qg_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= (hr[259]*qg_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r+= -(hr[31]*qg_l1_tf_CF_36(tcs,fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmpmO_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[94];
  r+= -hr[269]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= -hr[201]*qg_l1_tf_CA_6(fw1);
  r+= -hr[219]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= -(hr[79]*qg_l1_tf_CA_12(fw1))/2;
  r+= hr[179]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[270]*qg_l1_tf_CA_16(tcs,fw1,fw2);
  r+= (hr[91]*qg_l1_tf_CA_18(tcs,fw1))/2;
  r+= hr[226]*qg_l1_tf_CA_34(tcs,fw1,fw2);
  r+= (hr[269]*qg_l1_tf_CF_6(tcs,fw1,fw2))/2;
  r+= -(hr[269]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= (hr[269]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[232]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[195]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= (hr[203]*qg_l1_tf_CF_30(tcs,fw1))/2;
  r+= (hr[269]*qg_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= -(hr[91]*qg_l1_tf_CF_36(tcs,fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmmE_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[72];
  r+= hr[107]*qg_l1_tf_CA_2(fw1);
  r+= hr[146]*qg_l1_tf_CA_6(fw1);
  r+= hr[109]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= hr[183]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[145]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= (hr[63]*qg_l1_tf_CA_12(fw1))/2;
  r+= hr[162]*qg_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[133]*qg_l1_tf_CA_20(tcs,fw1,fw2);
  r+= -(3*hr[17]*qg_l1_tf_CA_26(fw1))/2;
  r+= qg_l1_tf_CF_7(fw1)/2;
  r+= (hr[252]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= (hr[262]*qg_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[228]*qg_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[248]*qg_l1_tf_CF_33(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmmO_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[160];
  r+= -hr[197]*qg_l1_tf_CA_2(fw1);
  r+= -hr[216]*qg_l1_tf_CA_6(fw1);
  r+= -hr[182]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= -hr[239]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[275]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[229]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= (hr[143]*qg_l1_tf_CA_12(fw1))/2;
  r+= hr[230]*qg_l1_tf_CA_19(tcs,fw1,fw2);
  r+= hr[207]*qg_l1_tf_CA_20(tcs,fw1,fw2);
  r+= (3*hr[116]*qg_l1_tf_CA_26(fw1))/2;
  r+= (hr[275]*qg_l1_tf_CF_13(tcs,fw1,fw2))/2;
  r+= -(hr[275]*qg_l1_tf_CF_20(tcs,fw1,fw2))/2;
  r+= (hr[275]*qg_l1_tf_CF_21(tcs,fw1,fw2))/2;
  r+= (hr[214]*qg_l1_tf_CF_33(fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmppE_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[67];
  r+= hr[140]*qg_l1_tf_CA_2(fw1);
  r+= (hr[23]*qg_l1_tf_CA_4(tcs,fw1))/2;
  r+= hr[110]*qg_l1_tf_CA_6(fw1);
  r+= hr[122]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= -hr[228]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= hr[176]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[142]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= (hr[55]*qg_l1_tf_CA_18(tcs,fw1))/2;
  r+= hr[175]*qg_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[112]*qg_l1_tf_CA_29(tcs,fw1,fw2);
  r+= qg_l1_tf_CF_7(fw1)/2;
  r+= (hr[252]*qg_l1_tf_CF_8(tcs,fw1,fw2))/2;
  r+= (hr[228]*qg_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= (hr[262]*qg_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= (hr[247]*qg_l1_tf_CF_36(tcs,fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmppO_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -2*hr[139];
  r+= -hr[205]*qg_l1_tf_CA_2(fw1);
  r+= (hr[82]*qg_l1_tf_CA_4(tcs,fw1))/2;
  r+= hr[189]*qg_l1_tf_CA_6(fw1);
  r+= hr[180]*qg_l1_tf_CA_7(tcs,fw1,fw2);
  r+= hr[275]*qg_l1_tf_CA_8(tcs,fw1,fw2);
  r+= -hr[220]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[211]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= -(hr[128]*qg_l1_tf_CA_18(tcs,fw1))/2;
  r+= -hr[187]*qg_l1_tf_CA_28(tcs,fw1,fw2);
  r+= hr[185]*qg_l1_tf_CA_29(tcs,fw1,fw2);
  r+= -(hr[275]*qg_l1_tf_CF_8(tcs,fw1,fw2))/2;
  r+= -(hr[275]*qg_l1_tf_CF_14(tcs,fw1,fw2))/2;
  r+= (hr[275]*qg_l1_tf_CF_15(tcs,fw1,fw2))/2;
  r+= (hr[204]*qg_l1_tf_CF_36(tcs,fw1))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmpE_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[78];
  r+= (hr[25]*qg_l1_tf_CA_4(tcs,fw1))/2;
  r+= hr[184]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= hr[144]*qg_l1_tf_CA_6(fw1);
  r+= hr[155]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[263]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= hr[117]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[170]*qg_l1_tf_CA_22(tcs,fw1,fw2);
  r+= (3*hr[56]*qg_l1_tf_CA_26(fw1))/2;
  r+= qg_l1_tf_CF_7(fw1)/2;
  r+= -(hr[237]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= (hr[253]*qg_l1_tf_CF_12(tcs,fw1,fw2))/2;
  r+= (hr[171]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[234]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[156]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= (hr[168]*qg_l1_tf_CF_31(tcs,fw1,fw2))/2;
  r+= -(hr[237]*qg_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmpO_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -2*hr[151];
  r+= -(hr[102]*qg_l1_tf_CA_4(tcs,fw1))/2;
  r+= hr[240]*qg_l1_tf_CA_5(tcs,fw1,fw2);
  r+= hr[208]*qg_l1_tf_CA_6(fw1);
  r+= hr[213]*qg_l1_tf_CA_9(tcs,fw1,fw2);
  r+= hr[269]*qg_l1_tf_CA_11(tcs,fw1,fw2);
  r+= -hr[181]*qg_l1_tf_CA_14(tcs,fw1,fw2);
  r+= hr[223]*qg_l1_tf_CA_22(tcs,fw1,fw2);
  r+= -(3*hr[134]*qg_l1_tf_CA_26(fw1))/2;
  r+= -(hr[269]*qg_l1_tf_CF_10(tcs,fw1,fw2))/2;
  r+= -(hr[269]*qg_l1_tf_CF_12(tcs,fw1,fw2))/2;
  r+= -(hr[190]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[267]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[209]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= -(hr[269]*qg_l1_tf_CF_34(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LppmE_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[58];
  r+= hr[111]*qg_l1_tf_CA_2(fw1);
  r+= hr[254]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= (hr[153]*qg_l1_tf_CA_12(fw1))/2;
  r+= hr[148]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= hr[120]*qg_l1_tf_CA_15(tcs,fw1,fw2);
  r+= hr[174]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[260]*qg_l1_tf_CA_23(tcs,fw1,fw2);
  r+= (hr[254]*qg_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= qg_l1_tf_CF_7(fw1)/2;
  r+= -(hr[238]*qg_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= qg_l1_tf_CF_13(tcs,fw1,fw2);
  r+= (hr[261]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[256]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= (hr[246]*qg_l1_tf_CF_28(fw1))/2;
  r+= (hr[154]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= (hr[40]*qg_l1_tf_CF_30(tcs,fw1))/2;
  r+= (hr[161]*qg_l1_tf_CF_32(tcs,fw1,fw2))/2;
  r+= (hr[32]*qg_l1_tf_CF_33(fw1))/2;
  r+= (hr[238]*qg_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LppmO_CF(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 2*hr[129];
  r+= -hr[194]*qg_l1_tf_CA_2(fw1);
  r+= -hr[272]*qg_l1_tf_CA_3(tcs,fw1,fw2);
  r+= (hr[200]*qg_l1_tf_CA_12(fw1))/2;
  r+= hr[193]*qg_l1_tf_CA_13(tcs,fw1,fw2);
  r+= hr[196]*qg_l1_tf_CA_15(tcs,fw1,fw2);
  r+= -hr[218]*qg_l1_tf_CA_17(tcs,fw1,fw2);
  r+= hr[225]*qg_l1_tf_CA_23(tcs,fw1,fw2);
  r+= -(hr[272]*qg_l1_tf_CF_5(tcs,fw1,fw2))/2;
  r+= (hr[272]*qg_l1_tf_CF_9(tcs,fw1,fw2))/2;
  r+= (hr[198]*qg_l1_tf_CF_19(tcs,fw1,fw2))/4;
  r+= (hr[273]*qg_l1_tf_CF_22(tcs,fw1,fw2))/2;
  r+= -hr[194]*qg_l1_tf_CF_28(fw1);
  r+= -(hr[199]*qg_l1_tf_CF_29(tcs,fw1))/2;
  r+= (hr[114]*qg_l1_tf_CF_30(tcs,fw1))/2;
  r+= -(hr[90]*qg_l1_tf_CF_33(fw1))/2;
  r+= -(hr[272]*qg_l1_tf_CF_35(tcs,fw1,fw2))/2;
  r/=8;
  r*=-1;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpppE_CF(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[57]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpppO_CF(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[147]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmmE_CF(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[53]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmmO_CF(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[132]/4;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmpE_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[37];
  r+= (hr[12]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= (hr[21]*qg_l1_tf_nfaa_5(tcs,fw1,fw2))/2;
  r+= (hr[20]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmpO_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[85];
  r+= (hr[66]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= -(hr[86]*qg_l1_tf_nfaa_5(tcs,fw1,fw2))/2;
  r+= (hr[77]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmpmE_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[19];
  r+= (hr[6]*qg_l1_tf_nfaa_4(fw1))/2;
  r+= (hr[14]*qg_l1_tf_nfaa_6(tcs,fw1,fw2))/2;
  r+= (hr[7]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmpmO_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[51];
  r+= -(hr[47]*qg_l1_tf_nfaa_4(fw1))/2;
  r+= -(hr[70]*qg_l1_tf_nfaa_6(tcs,fw1,fw2))/2;
  r+= (hr[52]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmmE_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[27];
  r+= (hr[1]*qg_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= (hr[11]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= (hr[5]*qg_l1_tf_nfaa_4(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmmO_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[113];
  r+= (hr[39]*qg_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= -(hr[74]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= -(hr[64]*qg_l1_tf_nfaa_4(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmppE_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[36];
  r+= (hr[8]*qg_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= (hr[10]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= (hr[13]*qg_l1_tf_nfaa_4(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmppO_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -8*hr[87];
  r+= (hr[49]*qg_l1_tf_nfaa_2(tcs,fw1,fw2))/2;
  r+= (hr[48]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= -(hr[73]*qg_l1_tf_nfaa_4(fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmpE_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[30];
  r+= (hr[3]*qg_l1_tf_nfaa_4(fw1))/2;
  r+= (hr[18]*qg_l1_tf_nfaa_6(tcs,fw1,fw2))/2;
  r+= (hr[26]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpmpO_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= -8*hr[95];
  r+= (hr[46]*qg_l1_tf_nfaa_4(fw1))/2;
  r+= (hr[97]*qg_l1_tf_nfaa_6(tcs,fw1,fw2))/2;
  r+= -(hr[84]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LppmE_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[29];
  r+= (hr[9]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= (hr[33]*qg_l1_tf_nfaa_5(tcs,fw1,fw2))/2;
  r+= (hr[43]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LppmO_nfaa(
  const std::array<T, 275>& hr,
  const std::array<T, 85>& tcs,
  const std::array<std::complex<T>,19>& fw1,
  const std::array<std::complex<T>,24>& fw2
) {
  std::complex<T> r(0,0);

  r+= 8*hr[100];
  r+= (hr[42]*qg_l1_tf_nfaa_3(fw1))/2;
  r+= -(hr[83]*qg_l1_tf_nfaa_5(tcs,fw1,fw2))/2;
  r+= (hr[105]*qg_l1_tf_nfaa_7(tcs,fw1))/2;
  r/=16;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpppE_nfaa(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= hr[22]/2;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LpppO_nfaa(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[99]/2;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmmE_nfaa(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= hr[38]/2;
  return r;
}

template<class T>
std::complex<T> qg_HA1L_LmmmO_nfaa(
  const std::array<T, 275>& hr
) {
  std::complex<T> r(0,0);

  r+= -hr[106]/2;
  return r;
}

/* --------------------------------- 2-loop -------------------------------- */
#ifdef LOOPSQ_ENABLED
/* ------------------------------ qqb ----------------------------- */
template<class T>
std::complex<T> qqb_HA2L_LmmmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[52]*tf[2];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[51]*tf[18];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmpE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[5]*tf[2];
  r+= rf[6]*tf[3];
  r+= rf[7]*tf[4];
  r+= rf[8]*tf[5];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmpO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[1]*tf[1];
  r+= rf[3]*tf[3];
  r+= rf[2]*tf[4];
  r+= rf[4]*tf[5];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmpmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[13]*tf[2];
  r+= rf[15]*tf[6];
  r+= rf[14]*tf[7];
  r+= rf[16]*tf[8];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmpmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[9]*tf[2];
  r+= rf[11]*tf[6];
  r+= rf[10]*tf[7];
  r+= rf[12]*tf[8];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmppE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[29]*tf[2];
  r+= rf[31]*tf[4];
  r+= rf[32]*tf[11];
  r+= rf[30]*tf[13];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmppO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[25]*tf[1];
  r+= rf[27]*tf[4];
  r+= rf[28]*tf[12];
  r+= rf[26]*tf[13];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[21]*tf[2];
  r+= rf[23]*tf[9];
  r+= rf[22]*tf[10];
  r+= rf[24]*tf[12];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[17]*tf[2];
  r+= rf[19]*tf[9];
  r+= rf[18]*tf[10];
  r+= rf[20]*tf[11];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmpE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[37]*tf[1];
  r+= rf[39]*tf[3];
  r+= rf[40]*tf[8];
  r+= rf[38]*tf[13];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmpO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[35]*tf[3];
  r+= rf[34]*tf[13];
  r+= rf[33]*tf[14];
  r+= rf[36]*tf[15];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LppmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[45]*tf[2];
  r+= rf[46]*tf[9];
  r+= rf[47]*tf[16];
  r+= rf[48]*tf[17];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LppmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[41]*tf[2];
  r+= rf[42]*tf[9];
  r+= rf[43]*tf[16];
  r+= rf[44]*tf[17];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpppE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[50]*tf[2];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpppO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[49]*tf[14];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1609];
  r+= rf[1624]*tf[38];
  r+= rf[1625]*tf[41];
  r+= rf[1613]*tf[45];
  r+= rf[1606]*tf[46];
  r+= rf[1607]*tf[48];
  r+= rf[1621]*tf[49];
  r+= rf[1608]*tf[56];
  r+= rf[1605]*tf[61];
  r+= eps5*rf[1651]*tf[82];
  r+= eps5*rf[1652]*tf[83];
  r+= eps5*rf[1654]*tf[84];
  r+= eps5*rf[1657]*tf[85];
  r+= rf[1610]*tf[89];
  r+= eps5*rf[1656]*tf[287];
  r+= rf[1658]*tf[1191];
  r+= rf[1644]*tf[1192];
  r+= rf[1646]*tf[1193];
  r+= rf[1659]*tf[1194];
  r+= rf[1660]*tf[1195];
  r+= rf[1645]*tf[1196];
  r+= rf[1639]*tf[1198];
  r+= rf[1638]*tf[1199];
  r+= rf[1647]*tf[1200];
  r+= rf[1648]*tf[1201];
  r+= rf[1641]*tf[1202];
  r+= rf[1643]*tf[1203];
  r+= rf[1642]*tf[1204];
  r+= rf[1640]*tf[1205];
  r+= eps5*rf[1650]*tf[1206];
  r+= rf[1614]*tf[1207];
  r+= rf[1612]*tf[1208];
  r+= rf[1611]*tf[1209];
  r+= rf[1617]*tf[1210];
  r+= rf[1615]*tf[1211];
  r+= rf[1584]*tf[1212];
  r+= rf[1616]*tf[1213];
  r+= rf[1618]*tf[1221];
  r+= rf[1629]*tf[1222];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1595];
  r+= rf[1601]*tf[38];
  r+= rf[1600]*tf[41];
  r+= rf[1591]*tf[45];
  r+= rf[1588]*tf[46];
  r+= rf[1586]*tf[48];
  r+= rf[1599]*tf[49];
  r+= rf[1587]*tf[56];
  r+= rf[1589]*tf[61];
  r+= eps5*rf[1633]*tf[82];
  r+= eps5*rf[1634]*tf[83];
  r+= eps5*rf[1635]*tf[84];
  r+= eps5*rf[1636]*tf[85];
  r+= rf[1594]*tf[89];
  r+= eps5*rf[1637]*tf[287];
  r+= rf[1653]*tf[1191];
  r+= rf[1627]*tf[1192];
  r+= rf[1626]*tf[1193];
  r+= rf[1655]*tf[1195];
  r+= rf[1628]*tf[1196];
  r+= rf[1649]*tf[1197];
  r+= rf[1604]*tf[1198];
  r+= rf[1603]*tf[1199];
  r+= rf[1630]*tf[1200];
  r+= rf[1631]*tf[1201];
  r+= rf[1619]*tf[1202];
  r+= rf[1623]*tf[1203];
  r+= rf[1622]*tf[1204];
  r+= rf[1620]*tf[1205];
  r+= eps5*rf[1632]*tf[1206];
  r+= rf[1590]*tf[1214];
  r+= rf[1593]*tf[1215];
  r+= rf[1592]*tf[1216];
  r+= rf[1597]*tf[1217];
  r+= rf[1598]*tf[1218];
  r+= rf[1583]*tf[1219];
  r+= rf[1596]*tf[1220];
  r+= rf[1585]*tf[1221];
  r+= rf[1602]*tf[1222];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmpE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= eps5*rf[116]*tf[19];
  r+= eps5*rf[172]*tf[20];
  r+= eps5*rf[84]*tf[21];
  r+= eps5*rf[96]*tf[21];
  r+= eps5*rf[89]*tf[22];
  r+= eps5*rf[55]*tf[24];
  r+= eps5*rf[167]*tf[25];
  r+= eps5*rf[54]*tf[26];
  r+= eps5*rf[66]*tf[27];
  r+= eps5*rf[57]*tf[30];
  r+= eps5*rf[85]*tf[31];
  r+= eps5*rf[97]*tf[31];
  r+= eps5*rf[86]*tf[32];
  r+= eps5*rf[170]*tf[33];
  r+= eps5*rf[63]*tf[35];
  r+= eps5*rf[114]*tf[37];
  r+= rf[217]*tf[39];
  r+= rf[216]*tf[40];
  r+= rf[228]*tf[42];
  r+= rf[226]*tf[43];
  r+= rf[215]*tf[44];
  r+= rf[208]*tf[45];
  r+= rf[207]*tf[46];
  r+= rf[229]*tf[47];
  r+= rf[210]*tf[48];
  r+= rf[225]*tf[50];
  r+= rf[286]*tf[51];
  r+= rf[263]*tf[52];
  r+= rf[255]*tf[55];
  r+= rf[211]*tf[56];
  r+= rf[287]*tf[57];
  r+= rf[252]*tf[58];
  r+= rf[244]*tf[60];
  r+= rf[206]*tf[61];
  r+= rf[285]*tf[62];
  r+= rf[262]*tf[63];
  r+= rf[239]*tf[64];
  r+= rf[245]*tf[65];
  r+= rf[237]*tf[66];
  r+= rf[264]*tf[67];
  r+= rf[238]*tf[68];
  r+= rf[223]*tf[69];
  r+= rf[251]*tf[71];
  r+= rf[192]*tf[72];
  r+= rf[92]*tf[73];
  r+= rf[250]*tf[75];
  r+= rf[118]*tf[76];
  r+= rf[209]*tf[78];
  r+= rf[288]*tf[79];
  r+= rf[224]*tf[80];
  r+= rf[165]*tf[81];
  r+= eps5*rf[200]*tf[82];
  r+= eps5*rf[141]*tf[83];
  r+= eps5*rf[148]*tf[84];
  r+= eps5*rf[139]*tf[85];
  r+= eps5*rf[83]*tf[86];
  r+= rf[270]*tf[88];
  r+= rf[214]*tf[90];
  r+= rf[218]*tf[91];
  r+= rf[212]*tf[92];
  r+= rf[256]*tf[93];
  r+= rf[233]*tf[94];
  r+= rf[213]*tf[95];
  r+= rf[253]*tf[96];
  r+= rf[254]*tf[97];
  r+= rf[232]*tf[98];
  r+= rf[231]*tf[100];
  r+= rf[189]*tf[101];
  r+= rf[230]*tf[102];
  r+= rf[235]*tf[105];
  r+= rf[234]*tf[107];
  r+= eps5*rf[142]*tf[108];
  r+= eps5*rf[143]*tf[109];
  r+= eps5*rf[137]*tf[110];
  r+= eps5*rf[133]*tf[111];
  r+= eps5*rf[171]*tf[114];
  r+= eps5*rf[62]*tf[115];
  r+= eps5*rf[135]*tf[116];
  r+= eps5*rf[60]*tf[117];
  r+= eps5*rf[82]*tf[118];
  r+= eps5*rf[134]*tf[119];
  r+= eps5*rf[136]*tf[120];
  r+= eps5*rf[56]*tf[121];
  r+= eps5*rf[61]*tf[121];
  r+= eps5*rf[65]*tf[122];
  r+= eps5*rf[145]*tf[124];
  r+= eps5*rf[113]*tf[125];
  r+= eps5*rf[138]*tf[127];
  r+= eps5*rf[58]*tf[128];
  r+= eps5*rf[115]*tf[129];
  r+= eps5*rf[91]*tf[131];
  r+= eps5*rf[176]*tf[132];
  r+= rf[266]*tf[134];
  r+= rf[274]*tf[135];
  r+= rf[101]*tf[136];
  r+= rf[272]*tf[137];
  r+= rf[161]*tf[138];
  r+= rf[162]*tf[139];
  r+= rf[158]*tf[141];
  r+= rf[221]*tf[143];
  r+= rf[194]*tf[144];
  r+= rf[196]*tf[145];
  r+= rf[191]*tf[148];
  r+= rf[273]*tf[154];
  r+= rf[236]*tf[155];
  r+= rf[222]*tf[157];
  r+= rf[164]*tf[160];
  r+= rf[160]*tf[161];
  r+= rf[203]*tf[162];
  r+= rf[187]*tf[163];
  r+= rf[267]*tf[165];
  r+= rf[265]*tf[166];
  r+= rf[278]*tf[167];
  r+= rf[78]*tf[168];
  r+= rf[193]*tf[171];
  r+= rf[121]*tf[175];
  r+= rf[219]*tf[177];
  r+= rf[220]*tf[178];
  r+= rf[190]*tf[179];
  r+= rf[279]*tf[181];
  r+= rf[77]*tf[182];
  r+= rf[94]*tf[184];
  r+= rf[276]*tf[185];
  r+= rf[268]*tf[187];
  r+= rf[205]*tf[189];
  r+= rf[204]*tf[190];
  r+= rf[188]*tf[191];
  r+= rf[277]*tf[193];
  r+= rf[76]*tf[194];
  r+= rf[119]*tf[197];
  r+= rf[120]*tf[199];
  r+= rf[280]*tf[200];
  r+= rf[246]*tf[201];
  r+= rf[93]*tf[202];
  r+= rf[247]*tf[203];
  r+= rf[242]*tf[204];
  r+= rf[227]*tf[205];
  r+= rf[248]*tf[208];
  r+= rf[269]*tf[210];
  r+= rf[249]*tf[212];
  r+= rf[195]*tf[213];
  r+= rf[240]*tf[216];
  r+= rf[243]*tf[218];
  r+= rf[241]*tf[221];
  r+= eps5*rf[64]*tf[224];
  r+= eps5*rf[53]*tf[226];
  r+= rf[159]*tf[228];
  r+= rf[275]*tf[229];
  r+= rf[163]*tf[232];
  r+= rf[166]*tf[235];
  r+= rf[261]*tf[237];
  r+= rf[281]*tf[238];
  r+= rf[271]*tf[239];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmpO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= eps5*rf[258]*tf[23];
  r+= eps5*rf[289]*tf[28];
  r+= eps5*rf[260]*tf[29];
  r+= eps5*rf[259]*tf[34];
  r+= eps5*rf[257]*tf[36];
  r+= rf[102]*tf[38];
  r+= rf[122]*tf[41];
  r+= rf[153]*tf[42];
  r+= rf[155]*tf[43];
  r+= rf[103]*tf[44];
  r+= rf[110]*tf[45];
  r+= rf[99]*tf[46];
  r+= rf[152]*tf[47];
  r+= rf[107]*tf[48];
  r+= rf[156]*tf[49];
  r+= rf[201]*tf[51];
  r+= rf[184]*tf[52];
  r+= rf[198]*tf[53];
  r+= rf[130]*tf[54];
  r+= rf[124]*tf[55];
  r+= rf[108]*tf[56];
  r+= rf[199]*tf[59];
  r+= rf[98]*tf[61];
  r+= rf[202]*tf[62];
  r+= rf[185]*tf[63];
  r+= rf[147]*tf[64];
  r+= rf[140]*tf[65];
  r+= rf[144]*tf[66];
  r+= rf[183]*tf[67];
  r+= rf[146]*tf[68];
  r+= rf[111]*tf[69];
  r+= rf[132]*tf[70];
  r+= rf[87]*tf[72];
  r+= rf[75]*tf[73];
  r+= rf[131]*tf[74];
  r+= rf[68]*tf[76];
  r+= rf[109]*tf[77];
  r+= rf[197]*tf[79];
  r+= rf[112]*tf[80];
  r+= rf[67]*tf[81];
  r+= eps5*rf[100]*tf[82];
  r+= eps5*rf[72]*tf[83];
  r+= eps5*rf[71]*tf[84];
  r+= eps5*rf[70]*tf[85];
  r+= eps5*rf[69]*tf[87];
  r+= rf[177]*tf[88];
  r+= rf[104]*tf[89];
  r+= rf[117]*tf[91];
  r+= rf[105]*tf[92];
  r+= rf[123]*tf[93];
  r+= rf[151]*tf[94];
  r+= rf[106]*tf[95];
  r+= rf[129]*tf[96];
  r+= rf[128]*tf[97];
  r+= rf[150]*tf[98];
  r+= rf[125]*tf[99];
  r+= rf[80]*tf[101];
  r+= rf[126]*tf[103];
  r+= rf[149]*tf[104];
  r+= rf[127]*tf[106];
  r+= eps5*rf[291]*tf[112];
  r+= eps5*rf[282]*tf[113];
  r+= eps5*rf[283]*tf[123];
  r+= eps5*rf[292]*tf[126];
  r+= eps5*rf[290]*tf[130];
  r+= eps5*rf[284]*tf[133];
  r+= rf[181]*tf[134];
  r+= rf[173]*tf[135];
  r+= rf[175]*tf[137];
  r+= rf[65]*tf[140];
  r+= rf[58]*tf[142];
  r+= rf[84]*tf[146];
  r+= rf[115]*tf[147];
  r+= rf[90]*tf[149];
  r+= rf[170]*tf[150];
  r+= rf[86]*tf[151];
  r+= rf[174]*tf[152];
  r+= rf[97]*tf[153];
  r+= rf[79]*tf[156];
  r+= rf[145]*tf[158];
  r+= rf[143]*tf[159];
  r+= rf[74]*tf[163];
  r+= rf[61]*tf[164];
  r+= rf[180]*tf[165];
  r+= rf[182]*tf[166];
  r+= rf[169]*tf[167];
  r+= rf[113]*tf[169];
  r+= rf[114]*tf[170];
  r+= rf[66]*tf[172];
  r+= rf[56]*tf[173];
  r+= rf[57]*tf[174];
  r+= rf[59]*tf[175];
  r+= rf[54]*tf[176];
  r+= rf[81]*tf[179];
  r+= rf[88]*tf[180];
  r+= rf[91]*tf[183];
  r+= rf[60]*tf[186];
  r+= rf[179]*tf[187];
  r+= rf[136]*tf[188];
  r+= rf[95]*tf[190];
  r+= rf[73]*tf[191];
  r+= rf[85]*tf[192];
  r+= rf[55]*tf[195];
  r+= rf[89]*tf[196];
  r+= rf[82]*tf[198];
  r+= rf[168]*tf[200];
  r+= rf[154]*tf[206];
  r+= rf[96]*tf[207];
  r+= rf[135]*tf[209];
  r+= rf[178]*tf[210];
  r+= rf[134]*tf[211];
  r+= rf[63]*tf[214];
  r+= rf[137]*tf[215];
  r+= rf[116]*tf[217];
  r+= rf[138]*tf[219];
  r+= rf[133]*tf[220];
  r+= rf[142]*tf[222];
  r+= eps5*rf[186]*tf[223];
  r+= eps5*rf[157]*tf[225];
  r+= rf[62]*tf[227];
  r+= rf[172]*tf[230];
  r+= rf[64]*tf[231];
  r+= rf[171]*tf[233];
  r+= rf[176]*tf[234];
  r+= rf[167]*tf[236];
  r+= rf[53]*tf[240];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmpmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[466]*tf[39];
  r+= rf[477]*tf[40];
  r+= rf[468]*tf[42];
  r+= rf[488]*tf[43];
  r+= rf[442]*tf[44];
  r+= rf[484]*tf[45];
  r+= rf[486]*tf[46];
  r+= rf[495]*tf[47];
  r+= rf[436]*tf[48];
  r+= rf[492]*tf[50];
  r+= rf[520]*tf[51];
  r+= rf[508]*tf[52];
  r+= rf[417]*tf[56];
  r+= rf[485]*tf[61];
  r+= rf[522]*tf[62];
  r+= rf[512]*tf[63];
  r+= rf[498]*tf[67];
  r+= eps5*rf[414]*tf[82];
  r+= eps5*rf[413]*tf[83];
  r+= eps5*rf[412]*tf[85];
  r+= rf[497]*tf[88];
  r+= rf[443]*tf[90];
  r+= rf[457]*tf[91];
  r+= rf[456]*tf[93];
  r+= rf[479]*tf[101];
  r+= rf[362]*tf[104];
  r+= eps5*rf[369]*tf[122];
  r+= eps5*rf[438]*tf[241];
  r+= eps5*rf[386]*tf[242];
  r+= eps5*rf[392]*tf[243];
  r+= eps5*rf[439]*tf[244];
  r+= eps5*rf[370]*tf[245];
  r+= eps5*rf[379]*tf[246];
  r+= eps5*rf[294]*tf[247];
  r+= eps5*rf[293]*tf[248];
  r+= eps5*rf[304]*tf[249];
  r+= eps5*rf[308]*tf[250];
  r+= eps5*rf[393]*tf[253];
  r+= eps5*rf[382]*tf[254];
  r+= eps5*rf[357]*tf[255];
  r+= eps5*rf[399]*tf[256];
  r+= eps5*rf[298]*tf[257];
  r+= eps5*rf[437]*tf[258];
  r+= eps5*rf[440]*tf[259];
  r+= eps5*rf[380]*tf[260];
  r+= eps5*rf[363]*tf[261];
  r+= eps5*rf[383]*tf[262];
  r+= eps5*rf[371]*tf[264];
  r+= eps5*rf[307]*tf[266];
  r+= rf[455]*tf[267];
  r+= rf[519]*tf[268];
  r+= rf[491]*tf[269];
  r+= rf[490]*tf[270];
  r+= rf[496]*tf[271];
  r+= rf[518]*tf[272];
  r+= rf[460]*tf[273];
  r+= rf[516]*tf[274];
  r+= rf[501]*tf[275];
  r+= rf[444]*tf[276];
  r+= rf[359]*tf[277];
  r+= rf[343]*tf[278];
  r+= rf[445]*tf[279];
  r+= rf[461]*tf[280];
  r+= rf[446]*tf[281];
  r+= rf[447]*tf[282];
  r+= rf[489]*tf[283];
  r+= rf[510]*tf[284];
  r+= eps5*rf[410]*tf[285];
  r+= eps5*rf[419]*tf[287];
  r+= rf[478]*tf[288];
  r+= rf[448]*tf[289];
  r+= rf[487]*tf[290];
  r+= rf[434]*tf[291];
  r+= rf[341]*tf[292];
  r+= rf[449]*tf[293];
  r+= rf[453]*tf[294];
  r+= rf[450]*tf[295];
  r+= rf[435]*tf[296];
  r+= eps5*rf[296]*tf[297];
  r+= eps5*rf[351]*tf[298];
  r+= eps5*rf[378]*tf[299];
  r+= eps5*rf[300]*tf[301];
  r+= eps5*rf[367]*tf[302];
  r+= eps5*rf[305]*tf[303];
  r+= eps5*rf[377]*tf[304];
  r+= eps5*rf[384]*tf[305];
  r+= eps5*rf[323]*tf[306];
  r+= eps5*rf[297]*tf[307];
  r+= eps5*rf[394]*tf[308];
  r+= eps5*rf[322]*tf[309];
  r+= eps5*rf[381]*tf[311];
  r+= eps5*rf[309]*tf[312];
  r+= eps5*rf[352]*tf[313];
  r+= eps5*rf[328]*tf[315];
  r+= eps5*rf[396]*tf[317];
  r+= rf[504]*tf[318];
  r+= rf[521]*tf[319];
  r+= rf[529]*tf[322];
  r+= rf[482]*tf[323];
  r+= rf[462]*tf[324];
  r+= rf[465]*tf[325];
  r+= rf[481]*tf[326];
  r+= rf[330]*tf[328];
  r+= rf[356]*tf[329];
  r+= rf[505]*tf[331];
  r+= rf[463]*tf[332];
  r+= rf[326]*tf[335];
  r+= rf[431]*tf[340];
  r+= rf[532]*tf[348];
  r+= rf[506]*tf[349];
  r+= rf[511]*tf[350];
  r+= rf[525]*tf[351];
  r+= rf[523]*tf[352];
  r+= rf[527]*tf[354];
  r+= rf[517]*tf[366];
  r+= rf[524]*tf[367];
  r+= rf[349]*tf[368];
  r+= rf[515]*tf[370];
  r+= rf[483]*tf[372];
  r+= rf[528]*tf[373];
  r+= rf[513]*tf[375];
  r+= rf[531]*tf[376];
  r+= rf[320]*tf[377];
  r+= rf[451]*tf[378];
  r+= rf[325]*tf[380];
  r+= rf[500]*tf[390];
  r+= rf[499]*tf[392];
  r+= rf[472]*tf[393];
  r+= rf[530]*tf[396];
  r+= rf[503]*tf[398];
  r+= rf[502]*tf[399];
  r+= rf[430]*tf[400];
  r+= rf[526]*tf[401];
  r+= rf[329]*tf[402];
  r+= rf[429]*tf[406];
  r+= rf[514]*tf[407];
  r+= rf[459]*tf[408];
  r+= rf[454]*tf[409];
  r+= rf[342]*tf[410];
  r+= rf[507]*tf[412];
  r+= rf[509]*tf[413];
  r+= rf[355]*tf[414];
  r+= rf[458]*tf[420];
  r+= rf[480]*tf[421];
  r+= rf[452]*tf[424];
  r+= rf[464]*tf[425];
  r+= rf[358]*tf[426];
  r+= eps5*rf[306]*tf[429];
  r+= eps5*rf[295]*tf[431];
  r+= rf[350]*tf[432];
  r+= rf[470]*tf[434];
  r+= rf[353]*tf[436];
  r+= rf[471]*tf[437];
  r+= rf[467]*tf[440];
  r+= rf[469]*tf[442];
  r+= rf[354]*tf[445];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmpmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= eps5*rf[426]*tf[36];
  r+= rf[385]*tf[39];
  r+= rf[374]*tf[40];
  r+= rf[387]*tf[42];
  r+= rf[373]*tf[43];
  r+= rf[331]*tf[44];
  r+= rf[340]*tf[45];
  r+= rf[335]*tf[46];
  r+= rf[316]*tf[48];
  r+= rf[334]*tf[50];
  r+= rf[407]*tf[51];
  r+= rf[421]*tf[52];
  r+= rf[318]*tf[56];
  r+= rf[336]*tf[61];
  r+= rf[402]*tf[62];
  r+= rf[416]*tf[63];
  r+= rf[422]*tf[67];
  r+= eps5*rf[319]*tf[82];
  r+= eps5*rf[313]*tf[83];
  r+= eps5*rf[317]*tf[85];
  r+= rf[415]*tf[88];
  r+= rf[332]*tf[90];
  r+= rf[365]*tf[93];
  r+= eps5*rf[474]*tf[112];
  r+= eps5*rf[494]*tf[123];
  r+= eps5*rf[427]*tf[251];
  r+= eps5*rf[425]*tf[252];
  r+= eps5*rf[476]*tf[263];
  r+= eps5*rf[424]*tf[265];
  r+= rf[364]*tf[267];
  r+= rf[406]*tf[268];
  r+= rf[403]*tf[270];
  r+= rf[408]*tf[272];
  r+= rf[376]*tf[274];
  r+= rf[333]*tf[276];
  r+= eps5*rf[312]*tf[286];
  r+= eps5*rf[314]*tf[287];
  r+= rf[346]*tf[289];
  r+= rf[344]*tf[290];
  r+= rf[299]*tf[292];
  r+= rf[347]*tf[293];
  r+= eps5*rf[493]*tf[300];
  r+= eps5*rf[475]*tf[310];
  r+= eps5*rf[423]*tf[314];
  r+= eps5*rf[473]*tf[316];
  r+= rf[391]*tf[318];
  r+= rf[401]*tf[319];
  r+= rf[369]*tf[320];
  r+= rf[328]*tf[321];
  r+= rf[370]*tf[327];
  r+= rf[381]*tf[330];
  r+= rf[382]*tf[333];
  r+= rf[309]*tf[334];
  r+= rf[294]*tf[336];
  r+= rf[307]*tf[337];
  r+= rf[378]*tf[338];
  r+= rf[438]*tf[339];
  r+= rf[324]*tf[340];
  r+= rf[372]*tf[341];
  r+= rf[337]*tf[342];
  r+= rf[339]*tf[343];
  r+= rf[323]*tf[344];
  r+= rf[322]*tf[345];
  r+= rf[377]*tf[346];
  r+= rf[440]*tf[347];
  r+= rf[441]*tf[348];
  r+= rf[388]*tf[349];
  r+= rf[395]*tf[350];
  r+= rf[433]*tf[351];
  r+= rf[321]*tf[353];
  r+= rf[371]*tf[355];
  r+= rf[409]*tf[356];
  r+= rf[375]*tf[357];
  r+= rf[338]*tf[358];
  r+= rf[366]*tf[359];
  r+= rf[345]*tf[360];
  r+= rf[303]*tf[361];
  r+= rf[361]*tf[362];
  r+= rf[360]*tf[363];
  r+= rf[311]*tf[364];
  r+= rf[302]*tf[365];
  r+= rf[390]*tf[366];
  r+= rf[428]*tf[367];
  r+= rf[304]*tf[369];
  r+= rf[380]*tf[371];
  r+= rf[437]*tf[374];
  r+= rf[327]*tf[375];
  r+= rf[293]*tf[379];
  r+= rf[399]*tf[381];
  r+= rf[420]*tf[382];
  r+= rf[404]*tf[383];
  r+= rf[411]*tf[384];
  r+= rf[418]*tf[385];
  r+= rf[310]*tf[386];
  r+= rf[405]*tf[387];
  r+= rf[368]*tf[388];
  r+= rf[348]*tf[389];
  r+= rf[297]*tf[391];
  r+= rf[367]*tf[394];
  r+= rf[439]*tf[395];
  r+= rf[301]*tf[397];
  r+= rf[398]*tf[398];
  r+= rf[397]*tf[399];
  r+= rf[432]*tf[401];
  r+= rf[384]*tf[403];
  r+= rf[393]*tf[404];
  r+= rf[363]*tf[405];
  r+= rf[383]*tf[411];
  r+= rf[389]*tf[413];
  r+= rf[379]*tf[415];
  r+= rf[357]*tf[416];
  r+= rf[352]*tf[417];
  r+= rf[351]*tf[418];
  r+= rf[308]*tf[419];
  r+= rf[300]*tf[422];
  r+= rf[298]*tf[423];
  r+= rf[296]*tf[427];
  r+= eps5*rf[400]*tf[428];
  r+= eps5*rf[315]*tf[430];
  r+= rf[305]*tf[433];
  r+= rf[392]*tf[435];
  r+= rf[394]*tf[438];
  r+= rf[396]*tf[439];
  r+= rf[386]*tf[441];
  r+= rf[295]*tf[443];
  r+= rf[306]*tf[444];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmppE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[900];
  r+= eps5*rf[866]*tf[28];
  r+= rf[958]*tf[39];
  r+= rf[981]*tf[40];
  r+= rf[991]*tf[42];
  r+= rf[980]*tf[50];
  r+= rf[959]*tf[51];
  r+= rf[1002]*tf[52];
  r+= rf[914]*tf[53];
  r+= rf[913]*tf[54];
  r+= rf[922]*tf[55];
  r+= rf[984]*tf[59];
  r+= rf[989]*tf[62];
  r+= rf[992]*tf[63];
  r+= rf[1004]*tf[67];
  r+= eps5*rf[881]*tf[82];
  r+= eps5*rf[891]*tf[83];
  r+= eps5*rf[887]*tf[84];
  r+= rf[988]*tf[88];
  r+= eps5*rf[787]*tf[249];
  r+= rf[951]*tf[267];
  r+= rf[911]*tf[270];
  r+= eps5*rf[880]*tf[287];
  r+= rf[967]*tf[318];
  r+= rf[1020]*tf[348];
  r+= rf[1017]*tf[449];
  r+= rf[905]*tf[474];
  r+= rf[962]*tf[475];
  r+= rf[904]*tf[476];
  r+= rf[961]*tf[478];
  r+= rf[960]*tf[482];
  r+= rf[963]*tf[483];
  r+= rf[969]*tf[491];
  r+= rf[955]*tf[496];
  r+= rf[901]*tf[499];
  r+= rf[1006]*tf[533];
  r+= rf[908]*tf[598];
  r+= eps5*rf[944]*tf[643];
  r+= eps5*rf[779]*tf[644];
  r+= eps5*rf[839]*tf[644];
  r+= eps5*rf[942]*tf[645];
  r+= eps5*rf[943]*tf[645];
  r+= eps5*rf[945]*tf[645];
  r+= eps5*rf[786]*tf[647];
  r+= eps5*rf[838]*tf[649];
  r+= eps5*rf[870]*tf[650];
  r+= eps5*rf[818]*tf[651];
  r+= eps5*rf[806]*tf[652];
  r+= eps5*rf[825]*tf[654];
  r+= eps5*rf[829]*tf[655];
  r+= rf[910]*tf[657];
  r+= rf[986]*tf[658];
  r+= rf[985]*tf[659];
  r+= rf[987]*tf[660];
  r+= rf[954]*tf[662];
  r+= rf[982]*tf[664];
  r+= rf[921]*tf[665];
  r+= rf[956]*tf[666];
  r+= rf[1003]*tf[667];
  r+= rf[998]*tf[668];
  r+= rf[997]*tf[669];
  r+= rf[968]*tf[670];
  r+= rf[983]*tf[671];
  r+= rf[990]*tf[672];
  r+= eps5*rf[877]*tf[673];
  r+= rf[931]*tf[674];
  r+= rf[902]*tf[675];
  r+= rf[953]*tf[677];
  r+= rf[903]*tf[678];
  r+= rf[929]*tf[679];
  r+= rf[950]*tf[680];
  r+= rf[952]*tf[681];
  r+= rf[883]*tf[683];
  r+= rf[933]*tf[684];
  r+= rf[957]*tf[685];
  r+= rf[932]*tf[686];
  r+= rf[923]*tf[688];
  r+= eps5*rf[871]*tf[689];
  r+= eps5*rf[867]*tf[690];
  r+= eps5*rf[819]*tf[691];
  r+= eps5*rf[843]*tf[692];
  r+= eps5*rf[842]*tf[693];
  r+= eps5*rf[780]*tf[695];
  r+= eps5*rf[864]*tf[696];
  r+= eps5*rf[872]*tf[697];
  r+= eps5*rf[803]*tf[698];
  r+= eps5*rf[804]*tf[699];
  r+= eps5*rf[862]*tf[700];
  r+= eps5*rf[840]*tf[701];
  r+= eps5*rf[861]*tf[702];
  r+= eps5*rf[817]*tf[703];
  r+= eps5*rf[824]*tf[704];
  r+= eps5*rf[814]*tf[705];
  r+= eps5*rf[785]*tf[706];
  r+= eps5*rf[827]*tf[707];
  r+= eps5*rf[815]*tf[708];
  r+= eps5*rf[816]*tf[710];
  r+= rf[994]*tf[711];
  r+= rf[830]*tf[713];
  r+= rf[1016]*tf[714];
  r+= rf[1007]*tf[719];
  r+= rf[1009]*tf[724];
  r+= rf[1001]*tf[726];
  r+= rf[833]*tf[728];
  r+= rf[999]*tf[731];
  r+= rf[907]*tf[732];
  r+= rf[834]*tf[734];
  r+= rf[996]*tf[738];
  r+= rf[836]*tf[739];
  r+= rf[899]*tf[740];
  r+= rf[970]*tf[741];
  r+= rf[977]*tf[742];
  r+= rf[1008]*tf[745];
  r+= rf[1013]*tf[746];
  r+= rf[993]*tf[750];
  r+= rf[835]*tf[752];
  r+= rf[979]*tf[753];
  r+= rf[1005]*tf[754];
  r+= rf[1011]*tf[757];
  r+= rf[1012]*tf[758];
  r+= rf[995]*tf[759];
  r+= rf[1019]*tf[760];
  r+= rf[909]*tf[761];
  r+= rf[919]*tf[763];
  r+= rf[906]*tf[765];
  r+= rf[973]*tf[768];
  r+= rf[1000]*tf[769];
  r+= rf[1010]*tf[770];
  r+= rf[927]*tf[773];
  r+= rf[918]*tf[774];
  r+= rf[1018]*tf[775];
  r+= rf[912]*tf[776];
  r+= rf[964]*tf[778];
  r+= rf[978]*tf[779];
  r+= rf[1014]*tf[781];
  r+= rf[1015]*tf[782];
  r+= eps5*rf[781]*tf[787];
  r+= eps5*rf[784]*tf[789];
  r+= rf[965]*tf[791];
  r+= rf[926]*tf[792];
  r+= rf[915]*tf[793];
  r+= rf[924]*tf[794];
  r+= rf[928]*tf[795];
  r+= rf[966]*tf[796];
  r+= rf[972]*tf[797];
  r+= rf[917]*tf[798];
  r+= rf[925]*tf[799];
  r+= rf[916]*tf[801];
  r+= rf[971]*tf[802];
  r+= rf[920]*tf[803];
  r+= rf[930]*tf[805];
  r+= rf[837]*tf[807];
  r+= rf[831]*tf[808];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmppO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[797];
  r+= eps5*rf[894]*tf[29];
  r+= rf[854]*tf[39];
  r+= rf[853]*tf[40];
  r+= rf[892]*tf[42];
  r+= rf[813]*tf[50];
  r+= rf[855]*tf[51];
  r+= rf[882]*tf[52];
  r+= rf[810]*tf[53];
  r+= rf[811]*tf[54];
  r+= rf[822]*tf[55];
  r+= rf[876]*tf[59];
  r+= rf[884]*tf[62];
  r+= rf[893]*tf[63];
  r+= rf[873]*tf[67];
  r+= eps5*rf[792]*tf[82];
  r+= eps5*rf[794]*tf[83];
  r+= eps5*rf[793]*tf[84];
  r+= rf[885]*tf[88];
  r+= eps5*rf[975]*tf[113];
  r+= eps5*rf[1024]*tf[126];
  r+= rf[845]*tf[267];
  r+= rf[807]*tf[270];
  r+= eps5*rf[790]*tf[287];
  r+= eps5*rf[1023]*tf[300];
  r+= eps5*rf[974]*tf[310];
  r+= rf[865]*tf[318];
  r+= rf[949]*tf[348];
  r+= rf[946]*tf[449];
  r+= eps5*rf[1022]*tf[458];
  r+= rf[802]*tf[474];
  r+= rf[858]*tf[475];
  r+= rf[801]*tf[476];
  r+= rf[857]*tf[478];
  r+= rf[856]*tf[482];
  r+= rf[859]*tf[483];
  r+= rf[869]*tf[491];
  r+= rf[848]*tf[496];
  r+= rf[798]*tf[499];
  r+= rf[934]*tf[533];
  r+= rf[936]*tf[551];
  r+= rf[805]*tf[598];
  r+= eps5*rf[896]*tf[646];
  r+= eps5*rf[897]*tf[648];
  r+= eps5*rf[895]*tf[653];
  r+= eps5*rf[976]*tf[656];
  r+= rf[808]*tf[657];
  r+= rf[888]*tf[658];
  r+= rf[886]*tf[659];
  r+= rf[889]*tf[660];
  r+= rf[850]*tf[661];
  r+= rf[847]*tf[662];
  r+= rf[879]*tf[663];
  r+= rf[874]*tf[664];
  r+= rf[820]*tf[665];
  r+= rf[849]*tf[667];
  r+= rf[878]*tf[669];
  r+= rf[868]*tf[670];
  r+= rf[875]*tf[671];
  r+= rf[890]*tf[672];
  r+= eps5*rf[791]*tf[673];
  r+= rf[812]*tf[674];
  r+= rf[800]*tf[675];
  r+= rf[846]*tf[676];
  r+= rf[799]*tf[678];
  r+= rf[828]*tf[679];
  r+= rf[844]*tf[680];
  r+= rf[852]*tf[682];
  r+= rf[789]*tf[683];
  r+= rf[788]*tf[684];
  r+= rf[851]*tf[685];
  r+= rf[823]*tf[686];
  r+= rf[821]*tf[687];
  r+= eps5*rf[1021]*tf[694];
  r+= eps5*rf[898]*tf[709];
  r+= rf[863]*tf[711];
  r+= rf[867]*tf[712];
  r+= rf[862]*tf[715];
  r+= rf[814]*tf[716];
  r+= rf[945]*tf[717];
  r+= rf[838]*tf[718];
  r+= rf[827]*tf[720];
  r+= rf[785]*tf[721];
  r+= rf[870]*tf[722];
  r+= rf[824]*tf[723];
  r+= rf[825]*tf[725];
  r+= rf[872]*tf[727];
  r+= rf[782]*tf[728];
  r+= rf[866]*tf[729];
  r+= rf[935]*tf[730];
  r+= rf[804]*tf[733];
  r+= rf[864]*tf[735];
  r+= rf[861]*tf[736];
  r+= rf[939]*tf[737];
  r+= rf[786]*tf[743];
  r+= rf[839]*tf[744];
  r+= rf[937]*tf[745];
  r+= rf[840]*tf[747];
  r+= rf[783]*tf[748];
  r+= rf[938]*tf[749];
  r+= rf[832]*tf[750];
  r+= rf[779]*tf[751];
  r+= rf[860]*tf[753];
  r+= rf[841]*tf[754];
  r+= rf[842]*tf[755];
  r+= rf[803]*tf[756];
  r+= rf[940]*tf[757];
  r+= rf[941]*tf[758];
  r+= rf[948]*tf[760];
  r+= rf[818]*tf[762];
  r+= rf[817]*tf[764];
  r+= rf[787]*tf[766];
  r+= rf[806]*tf[767];
  r+= rf[942]*tf[771];
  r+= rf[843]*tf[772];
  r+= rf[826]*tf[773];
  r+= rf[947]*tf[775];
  r+= rf[809]*tf[777];
  r+= rf[944]*tf[780];
  r+= rf[943]*tf[783];
  r+= rf[815]*tf[784];
  r+= rf[871]*tf[785];
  r+= eps5*rf[796]*tf[786];
  r+= eps5*rf[795]*tf[788];
  r+= rf[784]*tf[790];
  r+= rf[819]*tf[800];
  r+= rf[781]*tf[804];
  r+= rf[829]*tf[805];
  r+= rf[780]*tf[806];
  r+= rf[816]*tf[809];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[670];
  r+= eps5*rf[537]*tf[27];
  r+= eps5*rf[621]*tf[28];
  r+= rf[718]*tf[39];
  r+= rf[736]*tf[40];
  r+= rf[764]*tf[42];
  r+= rf[731]*tf[50];
  r+= rf[719]*tf[51];
  r+= rf[754]*tf[52];
  r+= rf[689]*tf[55];
  r+= rf[688]*tf[57];
  r+= rf[687]*tf[58];
  r+= rf[739]*tf[60];
  r+= rf[761]*tf[62];
  r+= rf[746]*tf[63];
  r+= rf[755]*tf[67];
  r+= eps5*rf[659]*tf[83];
  r+= eps5*rf[654]*tf[84];
  r+= eps5*rf[646]*tf[85];
  r+= rf[745]*tf[88];
  r+= rf[772]*tf[165];
  r+= rf[709]*tf[267];
  r+= rf[685]*tf[268];
  r+= rf[749]*tf[275];
  r+= rf[741]*tf[284];
  r+= eps5*rf[649]*tf[287];
  r+= rf[759]*tf[446];
  r+= rf[750]*tf[447];
  r+= rf[771]*tf[448];
  r+= eps5*rf[628]*tf[450];
  r+= eps5*rf[585]*tf[451];
  r+= eps5*rf[575]*tf[452];
  r+= eps5*rf[624]*tf[453];
  r+= eps5*rf[584]*tf[456];
  r+= eps5*rf[548]*tf[459];
  r+= eps5*rf[549]*tf[460];
  r+= eps5*rf[540]*tf[461];
  r+= eps5*rf[536]*tf[462];
  r+= eps5*rf[589]*tf[462];
  r+= eps5*rf[535]*tf[463];
  r+= eps5*rf[593]*tf[463];
  r+= eps5*rf[634]*tf[464];
  r+= eps5*rf[620]*tf[465];
  r+= eps5*rf[586]*tf[466];
  r+= eps5*rf[595]*tf[467];
  r+= eps5*rf[570]*tf[468];
  r+= eps5*rf[556]*tf[469];
  r+= eps5*rf[633]*tf[471];
  r+= rf[674]*tf[474];
  r+= rf[706]*tf[475];
  r+= rf[673]*tf[476];
  r+= rf[684]*tf[477];
  r+= rf[717]*tf[478];
  r+= rf[716]*tf[482];
  r+= rf[707]*tf[483];
  r+= rf[743]*tf[484];
  r+= rf[713]*tf[485];
  r+= rf[721]*tf[486];
  r+= rf[770]*tf[487];
  r+= rf[738]*tf[488];
  r+= rf[744]*tf[489];
  r+= rf[740]*tf[490];
  r+= rf[760]*tf[491];
  r+= rf[737]*tf[492];
  r+= rf[742]*tf[493];
  r+= rf[675]*tf[494];
  r+= rf[714]*tf[495];
  r+= rf[712]*tf[496];
  r+= eps5*rf[645]*tf[497];
  r+= rf[671]*tf[499];
  r+= rf[690]*tf[501];
  r+= rf[703]*tf[502];
  r+= rf[711]*tf[503];
  r+= rf[672]*tf[504];
  r+= rf[704]*tf[505];
  r+= rf[710]*tf[506];
  r+= rf[708]*tf[507];
  r+= rf[705]*tf[508];
  r+= rf[701]*tf[510];
  r+= rf[715]*tf[511];
  r+= rf[681]*tf[512];
  r+= rf[682]*tf[513];
  r+= rf[551]*tf[514];
  r+= eps5*rf[627]*tf[516];
  r+= eps5*rf[626]*tf[517];
  r+= eps5*rf[554]*tf[518];
  r+= eps5*rf[538]*tf[519];
  r+= eps5*rf[592]*tf[520];
  r+= eps5*rf[571]*tf[521];
  r+= eps5*rf[615]*tf[522];
  r+= eps5*rf[555]*tf[524];
  r+= eps5*rf[622]*tf[525];
  r+= eps5*rf[619]*tf[526];
  r+= eps5*rf[581]*tf[527];
  r+= eps5*rf[625]*tf[528];
  r+= eps5*rf[583]*tf[529];
  r+= eps5*rf[582]*tf[532];
  r+= rf[766]*tf[534];
  r+= rf[762]*tf[536];
  r+= rf[691]*tf[538];
  r+= rf[640]*tf[540];
  r+= rf[693]*tf[541];
  r+= rf[679]*tf[544];
  r+= rf[758]*tf[547];
  r+= rf[667]*tf[549];
  r+= rf[726]*tf[552];
  r+= rf[700]*tf[553];
  r+= rf[558]*tf[554];
  r+= rf[735]*tf[558];
  r+= rf[733]*tf[560];
  r+= rf[734]*tf[562];
  r+= rf[669]*tf[564];
  r+= rf[702]*tf[567];
  r+= rf[768]*tf[568];
  r+= rf[767]*tf[569];
  r+= rf[751]*tf[570];
  r+= rf[752]*tf[571];
  r+= rf[732]*tf[572];
  r+= rf[559]*tf[574];
  r+= rf[756]*tf[576];
  r+= rf[757]*tf[578];
  r+= rf[552]*tf[579];
  r+= rf[765]*tf[581];
  r+= rf[748]*tf[583];
  r+= rf[769]*tf[585];
  r+= rf[763]*tf[588];
  r+= rf[753]*tf[589];
  r+= rf[668]*tf[590];
  r+= rf[697]*tf[594];
  r+= rf[683]*tf[595];
  r+= rf[641]*tf[596];
  r+= rf[694]*tf[598];
  r+= rf[723]*tf[599];
  r+= rf[696]*tf[602];
  r+= rf[686]*tf[604];
  r+= rf[747]*tf[607];
  r+= rf[695]*tf[611];
  r+= eps5*rf[533]*tf[612];
  r+= eps5*rf[534]*tf[613];
  r+= eps5*rf[539]*tf[616];
  r+= rf[678]*tf[618];
  r+= rf[720]*tf[619];
  r+= rf[722]*tf[620];
  r+= rf[677]*tf[621];
  r+= rf[725]*tf[624];
  r+= rf[676]*tf[626];
  r+= rf[680]*tf[628];
  r+= rf[569]*tf[629];
  r+= rf[699]*tf[633];
  r+= rf[724]*tf[634];
  r+= rf[553]*tf[636];
  r+= rf[692]*tf[637];
  r+= rf[698]*tf[638];
  r+= rf[550]*tf[639];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[565];
  r+= rf[663]*tf[42];
  r+= rf[613]*tf[51];
  r+= rf[651]*tf[52];
  r+= rf[577]*tf[55];
  r+= rf[579]*tf[57];
  r+= rf[578]*tf[58];
  r+= rf[648]*tf[60];
  r+= rf[664]*tf[62];
  r+= rf[662]*tf[63];
  r+= rf[652]*tf[67];
  r+= eps5*rf[547]*tf[83];
  r+= eps5*rf[546]*tf[84];
  r+= eps5*rf[542]*tf[85];
  r+= rf[661]*tf[88];
  r+= eps5*rf[773]*tf[265];
  r+= rf[607]*tf[267];
  r+= rf[574]*tf[268];
  r+= rf[642]*tf[275];
  r+= rf[660]*tf[284];
  r+= eps5*rf[544]*tf[287];
  r+= rf[643]*tf[348];
  r+= rf[618]*tf[446];
  r+= rf[629]*tf[447];
  r+= rf[644]*tf[449];
  r+= eps5*rf[665]*tf[454];
  r+= eps5*rf[774]*tf[455];
  r+= eps5*rf[666]*tf[457];
  r+= eps5*rf[776]*tf[458];
  r+= eps5*rf[778]*tf[470];
  r+= eps5*rf[730]*tf[472];
  r+= eps5*rf[729]*tf[473];
  r+= rf[561]*tf[474];
  r+= rf[608]*tf[475];
  r+= rf[560]*tf[476];
  r+= rf[573]*tf[477];
  r+= rf[598]*tf[478];
  r+= rf[611]*tf[479];
  r+= rf[612]*tf[480];
  r+= rf[610]*tf[481];
  r+= rf[599]*tf[482];
  r+= rf[609]*tf[483];
  r+= rf[657]*tf[484];
  r+= rf[603]*tf[485];
  r+= rf[631]*tf[486];
  r+= rf[655]*tf[487];
  r+= rf[647]*tf[488];
  r+= rf[656]*tf[489];
  r+= rf[653]*tf[490];
  r+= rf[632]*tf[491];
  r+= rf[650]*tf[492];
  r+= rf[658]*tf[493];
  r+= rf[566]*tf[494];
  r+= rf[600]*tf[495];
  r+= rf[602]*tf[496];
  r+= eps5*rf[541]*tf[498];
  r+= rf[564]*tf[500];
  r+= rf[576]*tf[501];
  r+= rf[563]*tf[502];
  r+= rf[605]*tf[503];
  r+= rf[562]*tf[504];
  r+= rf[572]*tf[505];
  r+= rf[604]*tf[506];
  r+= rf[606]*tf[507];
  r+= rf[545]*tf[509];
  r+= rf[543]*tf[510];
  r+= rf[601]*tf[511];
  r+= rf[568]*tf[512];
  r+= rf[567]*tf[513];
  r+= eps5*rf[777]*tf[515];
  r+= eps5*rf[775]*tf[523];
  r+= eps5*rf[728]*tf[530];
  r+= eps5*rf[727]*tf[531];
  r+= rf[635]*tf[533];
  r+= rf[634]*tf[535];
  r+= rf[626]*tf[537];
  r+= rf[624]*tf[539];
  r+= rf[622]*tf[542];
  r+= rf[589]*tf[543];
  r+= rf[555]*tf[545];
  r+= rf[595]*tf[546];
  r+= rf[593]*tf[548];
  r+= rf[588]*tf[550];
  r+= rf[637]*tf[551];
  r+= rf[581]*tf[555];
  r+= rf[537]*tf[556];
  r+= rf[636]*tf[557];
  r+= rf[591]*tf[559];
  r+= rf[535]*tf[561];
  r+= rf[557]*tf[563];
  r+= rf[536]*tf[565];
  r+= rf[592]*tf[566];
  r+= rf[590]*tf[572];
  r+= rf[597]*tf[573];
  r+= rf[596]*tf[575];
  r+= rf[617]*tf[577];
  r+= rf[615]*tf[580];
  r+= rf[594]*tf[582];
  r+= rf[614]*tf[584];
  r+= rf[623]*tf[586];
  r+= rf[616]*tf[587];
  r+= rf[538]*tf[591];
  r+= rf[585]*tf[592];
  r+= rf[621]*tf[593];
  r+= rf[549]*tf[597];
  r+= rf[587]*tf[598];
  r+= rf[630]*tf[599];
  r+= rf[628]*tf[600];
  r+= rf[554]*tf[601];
  r+= rf[580]*tf[603];
  r+= rf[556]*tf[605];
  r+= rf[586]*tf[606];
  r+= rf[575]*tf[608];
  r+= rf[548]*tf[609];
  r+= rf[584]*tf[610];
  r+= eps5*rf[638]*tf[614];
  r+= eps5*rf[639]*tf[615];
  r+= rf[627]*tf[617];
  r+= rf[570]*tf[622];
  r+= rf[620]*tf[623];
  r+= rf[571]*tf[625];
  r+= rf[633]*tf[627];
  r+= rf[540]*tf[630];
  r+= rf[619]*tf[631];
  r+= rf[625]*tf[632];
  r+= rf[582]*tf[635];
  r+= rf[583]*tf[638];
  r+= rf[534]*tf[640];
  r+= rf[533]*tf[641];
  r+= rf[539]*tf[642];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmpE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= eps5*rf[1025]*tf[27];
  r+= rf[1214]*tf[42];
  r+= rf[1222]*tf[43];
  r+= rf[1238]*tf[51];
  r+= rf[1256]*tf[52];
  r+= rf[1232]*tf[62];
  r+= rf[1260]*tf[63];
  r+= rf[1243]*tf[67];
  r+= eps5*rf[1148]*tf[82];
  r+= eps5*rf[1147]*tf[84];
  r+= eps5*rf[1145]*tf[85];
  r+= rf[1242]*tf[88];
  r+= eps5*rf[1118]*tf[130];
  r+= rf[1186]*tf[267];
  r+= rf[1237]*tf[268];
  r+= rf[1235]*tf[269];
  r+= rf[1234]*tf[270];
  r+= rf[1236]*tf[272];
  r+= rf[1192]*tf[274];
  r+= rf[1176]*tf[276];
  r+= rf[1215]*tf[280];
  r+= rf[1233]*tf[283];
  r+= eps5*rf[1155]*tf[287];
  r+= rf[1258]*tf[319];
  r+= rf[1245]*tf[348];
  r+= rf[1253]*tf[349];
  r+= rf[1259]*tf[350];
  r+= rf[1257]*tf[413];
  r+= rf[1262]*tf[446];
  r+= eps5*rf[1041]*tf[451];
  r+= rf[1196]*tf[479];
  r+= rf[1220]*tf[480];
  r+= rf[1239]*tf[481];
  r+= eps5*rf[1105]*tf[810];
  r+= eps5*rf[1095]*tf[811];
  r+= eps5*rf[1126]*tf[812];
  r+= eps5*rf[1072]*tf[813];
  r+= eps5*rf[1093]*tf[814];
  r+= eps5*rf[1106]*tf[815];
  r+= eps5*rf[1113]*tf[816];
  r+= eps5*rf[1114]*tf[817];
  r+= eps5*rf[1104]*tf[818];
  r+= eps5*rf[1123]*tf[819];
  r+= eps5*rf[1088]*tf[820];
  r+= eps5*rf[1119]*tf[821];
  r+= eps5*rf[1132]*tf[822];
  r+= eps5*rf[1111]*tf[823];
  r+= eps5*rf[1071]*tf[824];
  r+= eps5*rf[1101]*tf[825];
  r+= eps5*rf[1102]*tf[826];
  r+= rf[1174]*tf[827];
  r+= rf[1219]*tf[828];
  r+= rf[1217]*tf[829];
  r+= rf[1241]*tf[830];
  r+= rf[1143]*tf[831];
  r+= rf[1149]*tf[832];
  r+= rf[1218]*tf[833];
  r+= rf[1249]*tf[834];
  r+= rf[1244]*tf[835];
  r+= rf[1240]*tf[836];
  r+= rf[1206]*tf[837];
  r+= rf[1076]*tf[838];
  r+= rf[1207]*tf[839];
  r+= rf[1177]*tf[840];
  r+= rf[1178]*tf[842];
  r+= rf[1189]*tf[843];
  r+= eps5*rf[1144]*tf[844];
  r+= rf[1175]*tf[846];
  r+= rf[1188]*tf[847];
  r+= rf[1216]*tf[848];
  r+= rf[1187]*tf[849];
  r+= rf[1181]*tf[850];
  r+= rf[1221]*tf[851];
  r+= rf[1134]*tf[853];
  r+= rf[1085]*tf[854];
  r+= rf[1180]*tf[855];
  r+= rf[1223]*tf[857];
  r+= rf[1179]*tf[858];
  r+= rf[1184]*tf[860];
  r+= rf[1172]*tf[861];
  r+= rf[1086]*tf[862];
  r+= eps5*rf[1112]*tf[863];
  r+= eps5*rf[1078]*tf[864];
  r+= eps5*rf[1094]*tf[865];
  r+= eps5*rf[1116]*tf[866];
  r+= eps5*rf[1097]*tf[867];
  r+= eps5*rf[1040]*tf[868];
  r+= eps5*rf[1100]*tf[869];
  r+= eps5*rf[1055]*tf[870];
  r+= eps5*rf[1034]*tf[871];
  r+= eps5*rf[1129]*tf[872];
  r+= eps5*rf[1056]*tf[873];
  r+= eps5*rf[1033]*tf[874];
  r+= eps5*rf[1032]*tf[875];
  r+= eps5*rf[1110]*tf[876];
  r+= eps5*rf[1043]*tf[877];
  r+= eps5*rf[1079]*tf[878];
  r+= eps5*rf[1125]*tf[880];
  r+= rf[1212]*tf[882];
  r+= rf[1183]*tf[886];
  r+= rf[1194]*tf[887];
  r+= rf[1226]*tf[888];
  r+= rf[1252]*tf[890];
  r+= rf[1210]*tf[894];
  r+= rf[1211]*tf[895];
  r+= rf[1230]*tf[896];
  r+= rf[1193]*tf[897];
  r+= rf[1227]*tf[898];
  r+= rf[1190]*tf[901];
  r+= rf[1029]*tf[903];
  r+= rf[1263]*tf[905];
  r+= rf[1170]*tf[907];
  r+= rf[1208]*tf[911];
  r+= rf[1251]*tf[912];
  r+= rf[1224]*tf[913];
  r+= rf[1264]*tf[915];
  r+= rf[1171]*tf[916];
  r+= rf[1225]*tf[918];
  r+= rf[1213]*tf[920];
  r+= rf[1074]*tf[924];
  r+= rf[1231]*tf[925];
  r+= rf[1058]*tf[927];
  r+= rf[1261]*tf[929];
  r+= rf[1199]*tf[931];
  r+= rf[1254]*tf[933];
  r+= rf[1169]*tf[934];
  r+= rf[1248]*tf[935];
  r+= rf[1250]*tf[936];
  r+= rf[1028]*tf[940];
  r+= rf[1168]*tf[942];
  r+= rf[1246]*tf[943];
  r+= rf[1195]*tf[944];
  r+= rf[1247]*tf[945];
  r+= rf[1191]*tf[947];
  r+= rf[1185]*tf[948];
  r+= rf[1209]*tf[951];
  r+= rf[1167]*tf[954];
  r+= rf[1255]*tf[958];
  r+= rf[1057]*tf[959];
  r+= rf[1059]*tf[960];
  r+= rf[1182]*tf[961];
  r+= rf[1075]*tf[964];
  r+= rf[1228]*tf[965];
  r+= rf[1229]*tf[967];
  r+= eps5*rf[1042]*tf[969];
  r+= eps5*rf[1027]*tf[972];
  r+= rf[1073]*tf[974];
  r+= rf[1201]*tf[975];
  r+= rf[1198]*tf[978];
  r+= rf[1200]*tf[980];
  r+= rf[1035]*tf[981];
  r+= rf[1197]*tf[983];
  r+= rf[1077]*tf[986];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmpO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= eps5*rf[1158]*tf[36];
  r+= rf[1108]*tf[38];
  r+= rf[1099]*tf[41];
  r+= rf[1131]*tf[42];
  r+= rf[1091]*tf[43];
  r+= rf[1068]*tf[49];
  r+= rf[1142]*tf[51];
  r+= rf[1152]*tf[52];
  r+= rf[1136]*tf[62];
  r+= rf[1153]*tf[63];
  r+= rf[1151]*tf[67];
  r+= eps5*rf[1051]*tf[82];
  r+= eps5*rf[1047]*tf[84];
  r+= eps5*rf[1049]*tf[85];
  r+= rf[1154]*tf[88];
  r+= eps5*rf[1202]*tf[112];
  r+= eps5*rf[1204]*tf[123];
  r+= eps5*rf[1159]*tf[251];
  r+= eps5*rf[1160]*tf[252];
  r+= eps5*rf[1163]*tf[263];
  r+= eps5*rf[1161]*tf[265];
  r+= rf[1087]*tf[267];
  r+= rf[1141]*tf[268];
  r+= rf[1138]*tf[269];
  r+= rf[1139]*tf[270];
  r+= rf[1140]*tf[272];
  r+= rf[1107]*tf[274];
  r+= rf[1062]*tf[276];
  r+= rf[1117]*tf[280];
  r+= rf[1137]*tf[283];
  r+= eps5*rf[1045]*tf[287];
  r+= eps5*rf[1205]*tf[300];
  r+= eps5*rf[1203]*tf[310];
  r+= rf[1135]*tf[319];
  r+= rf[1150]*tf[348];
  r+= rf[1130]*tf[349];
  r+= rf[1124]*tf[350];
  r+= rf[1122]*tf[398];
  r+= rf[1128]*tf[413];
  r+= rf[1165]*tf[446];
  r+= rf[1061]*tf[827];
  r+= rf[1067]*tf[828];
  r+= rf[1063]*tf[829];
  r+= rf[1157]*tf[830];
  r+= rf[1048]*tf[831];
  r+= rf[1050]*tf[832];
  r+= rf[1066]*tf[833];
  r+= rf[1146]*tf[834];
  r+= rf[1092]*tf[835];
  r+= rf[1156]*tf[836];
  r+= rf[1044]*tf[838];
  r+= rf[1109]*tf[839];
  r+= rf[1065]*tf[840];
  r+= rf[1115]*tf[841];
  r+= rf[1064]*tf[842];
  r+= rf[1098]*tf[843];
  r+= eps5*rf[1046]*tf[844];
  r+= rf[1060]*tf[845];
  r+= rf[1089]*tf[847];
  r+= rf[1081]*tf[848];
  r+= rf[1090]*tf[849];
  r+= rf[1084]*tf[850];
  r+= rf[1082]*tf[851];
  r+= rf[1037]*tf[852];
  r+= rf[1039]*tf[854];
  r+= rf[1083]*tf[855];
  r+= rf[1069]*tf[856];
  r+= rf[1080]*tf[859];
  r+= rf[1070]*tf[860];
  r+= rf[1036]*tf[861];
  r+= rf[1038]*tf[862];
  r+= eps5*rf[1133]*tf[879];
  r+= eps5*rf[1162]*tf[881];
  r+= rf[1094]*tf[883];
  r+= rf[1118]*tf[884];
  r+= rf[1112]*tf[885];
  r+= rf[1096]*tf[889];
  r+= rf[1104]*tf[891];
  r+= rf[1105]*tf[892];
  r+= rf[1114]*tf[893];
  r+= rf[1103]*tf[899];
  r+= rf[1116]*tf[900];
  r+= rf[1032]*tf[902];
  r+= rf[1025]*tf[904];
  r+= rf[1166]*tf[906];
  r+= rf[1031]*tf[908];
  r+= rf[1111]*tf[909];
  r+= rf[1026]*tf[910];
  r+= rf[1127]*tf[912];
  r+= rf[1120]*tf[913];
  r+= rf[1041]*tf[914];
  r+= rf[1100]*tf[917];
  r+= rf[1173]*tf[919];
  r+= rf[1072]*tf[921];
  r+= rf[1030]*tf[922];
  r+= rf[1126]*tf[923];
  r+= rf[1095]*tf[926];
  r+= rf[1123]*tf[928];
  r+= rf[1164]*tf[930];
  r+= rf[1106]*tf[932];
  r+= rf[1121]*tf[936];
  r+= rf[1056]*tf[937];
  r+= rf[1034]*tf[938];
  r+= rf[1043]*tf[939];
  r+= rf[1055]*tf[941];
  r+= rf[1110]*tf[946];
  r+= rf[1088]*tf[949];
  r+= rf[1113]*tf[950];
  r+= rf[1071]*tf[952];
  r+= rf[1097]*tf[953];
  r+= rf[1054]*tf[955];
  r+= rf[1101]*tf[956];
  r+= rf[1102]*tf[957];
  r+= rf[1079]*tf[962];
  r+= rf[1033]*tf[963];
  r+= rf[1093]*tf[966];
  r+= rf[1078]*tf[968];
  r+= eps5*rf[1053]*tf[970];
  r+= eps5*rf[1052]*tf[971];
  r+= rf[1040]*tf[973];
  r+= rf[1119]*tf[976];
  r+= rf[1125]*tf[977];
  r+= rf[1027]*tf[979];
  r+= rf[1129]*tf[982];
  r+= rf[1132]*tf[984];
  r+= rf[1042]*tf[985];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LppmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1489]*tf[42];
  r+= rf[1426]*tf[43];
  r+= rf[1482]*tf[51];
  r+= rf[1400]*tf[52];
  r+= rf[1454]*tf[53];
  r+= rf[1395]*tf[55];
  r+= rf[1453]*tf[59];
  r+= rf[1452]*tf[62];
  r+= rf[1486]*tf[63];
  r+= rf[1483]*tf[67];
  r+= rf[1464]*tf[69];
  r+= rf[1463]*tf[79];
  r+= eps5*rf[1369]*tf[82];
  r+= eps5*rf[1447]*tf[83];
  r+= eps5*rf[1321]*tf[84];
  r+= eps5*rf[1432]*tf[85];
  r+= rf[1475]*tf[88];
  r+= rf[1500]*tf[137];
  r+= rf[1499]*tf[165];
  r+= rf[1485]*tf[166];
  r+= rf[1498]*tf[167];
  r+= rf[1487]*tf[210];
  r+= eps5*rf[1299]*tf[243];
  r+= eps5*rf[1281]*tf[287];
  r+= rf[1503]*tf[447];
  r+= rf[1379]*tf[479];
  r+= rf[1378]*tf[480];
  r+= rf[1408]*tf[481];
  r+= rf[1497]*tf[670];
  r+= rf[1377]*tf[827];
  r+= rf[1460]*tf[828];
  r+= rf[1374]*tf[829];
  r+= rf[1376]*tf[831];
  r+= rf[1405]*tf[832];
  r+= rf[1373]*tf[833];
  r+= rf[1425]*tf[846];
  r+= eps5*rf[1310]*tf[987];
  r+= eps5*rf[1323]*tf[988];
  r+= eps5*rf[1362]*tf[990];
  r+= eps5*rf[1339]*tf[991];
  r+= eps5*rf[1283]*tf[992];
  r+= eps5*rf[1361]*tf[993];
  r+= eps5*rf[1279]*tf[994];
  r+= eps5*rf[1441]*tf[995];
  r+= eps5*rf[1282]*tf[996];
  r+= eps5*rf[1292]*tf[997];
  r+= eps5*rf[1294]*tf[998];
  r+= eps5*rf[1280]*tf[1000];
  r+= eps5*rf[1295]*tf[1000];
  r+= eps5*rf[1468]*tf[1000];
  r+= eps5*rf[1466]*tf[1001];
  r+= eps5*rf[1442]*tf[1002];
  r+= eps5*rf[1267]*tf[1003];
  r+= rf[1389]*tf[1004];
  r+= rf[1382]*tf[1005];
  r+= rf[1387]*tf[1006];
  r+= rf[1336]*tf[1007];
  r+= rf[1386]*tf[1009];
  r+= rf[1496]*tf[1010];
  r+= rf[1311]*tf[1011];
  r+= rf[1393]*tf[1012];
  r+= rf[1300]*tf[1013];
  r+= rf[1375]*tf[1014];
  r+= rf[1345]*tf[1015];
  r+= rf[1391]*tf[1016];
  r+= rf[1481]*tf[1017];
  r+= rf[1457]*tf[1018];
  r+= rf[1465]*tf[1019];
  r+= rf[1406]*tf[1020];
  r+= rf[1462]*tf[1021];
  r+= rf[1461]*tf[1022];
  r+= rf[1394]*tf[1023];
  r+= rf[1383]*tf[1024];
  r+= rf[1474]*tf[1026];
  r+= rf[1344]*tf[1027];
  r+= rf[1459]*tf[1028];
  r+= rf[1384]*tf[1029];
  r+= rf[1458]*tf[1030];
  r+= rf[1385]*tf[1032];
  r+= eps5*rf[1269]*tf[1033];
  r+= eps5*rf[1320]*tf[1034];
  r+= eps5*rf[1278]*tf[1035];
  r+= eps5*rf[1270]*tf[1036];
  r+= eps5*rf[1326]*tf[1037];
  r+= eps5*rf[1318]*tf[1038];
  r+= eps5*rf[1439]*tf[1039];
  r+= eps5*rf[1434]*tf[1040];
  r+= eps5*rf[1289]*tf[1041];
  r+= eps5*rf[1288]*tf[1042];
  r+= eps5*rf[1274]*tf[1043];
  r+= eps5*rf[1287]*tf[1043];
  r+= eps5*rf[1290]*tf[1043];
  r+= eps5*rf[1438]*tf[1044];
  r+= eps5*rf[1309]*tf[1045];
  r+= eps5*rf[1325]*tf[1046];
  r+= eps5*rf[1451]*tf[1047];
  r+= eps5*rf[1341]*tf[1048];
  r+= eps5*rf[1342]*tf[1049];
  r+= rf[1484]*tf[1050];
  r+= rf[1372]*tf[1051];
  r+= rf[1416]*tf[1052];
  r+= rf[1392]*tf[1054];
  r+= rf[1346]*tf[1056];
  r+= rf[1348]*tf[1057];
  r+= rf[1380]*tf[1061];
  r+= rf[1491]*tf[1064];
  r+= rf[1502]*tf[1068];
  r+= rf[1413]*tf[1073];
  r+= rf[1335]*tf[1076];
  r+= rf[1422]*tf[1078];
  r+= rf[1353]*tf[1079];
  r+= rf[1414]*tf[1081];
  r+= rf[1423]*tf[1082];
  r+= rf[1427]*tf[1083];
  r+= rf[1471]*tf[1084];
  r+= rf[1349]*tf[1085];
  r+= rf[1490]*tf[1087];
  r+= rf[1420]*tf[1088];
  r+= rf[1333]*tf[1089];
  r+= rf[1350]*tf[1090];
  r+= rf[1479]*tf[1091];
  r+= rf[1411]*tf[1098];
  r+= rf[1478]*tf[1099];
  r+= rf[1492]*tf[1100];
  r+= rf[1419]*tf[1101];
  r+= rf[1504]*tf[1104];
  r+= rf[1410]*tf[1105];
  r+= rf[1493]*tf[1106];
  r+= rf[1501]*tf[1108];
  r+= rf[1415]*tf[1109];
  r+= rf[1494]*tf[1111];
  r+= rf[1470]*tf[1114];
  r+= rf[1480]*tf[1115];
  r+= rf[1495]*tf[1116];
  r+= rf[1473]*tf[1117];
  r+= rf[1390]*tf[1119];
  r+= rf[1381]*tf[1120];
  r+= rf[1371]*tf[1121];
  r+= rf[1347]*tf[1124];
  r+= rf[1488]*tf[1126];
  r+= rf[1388]*tf[1127];
  r+= rf[1417]*tf[1130];
  r+= rf[1421]*tf[1135];
  r+= rf[1337]*tf[1139];
  r+= eps5*rf[1284]*tf[1140];
  r+= eps5*rf[1286]*tf[1142];
  r+= rf[1338]*tf[1145];
  r+= rf[1472]*tf[1146];
  r+= rf[1401]*tf[1148];
  r+= rf[1428]*tf[1149];
  r+= rf[1407]*tf[1153];
  r+= rf[1424]*tf[1156];
  r+= rf[1418]*tf[1157];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LppmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= eps5*rf[1456]*tf[28];
  r+= rf[1440]*tf[42];
  r+= rf[1312]*tf[43];
  r+= rf[1431]*tf[51];
  r+= rf[1340]*tf[52];
  r+= rf[1368]*tf[53];
  r+= rf[1330]*tf[55];
  r+= rf[1367]*tf[59];
  r+= rf[1366]*tf[62];
  r+= rf[1436]*tf[63];
  r+= rf[1430]*tf[67];
  r+= rf[1355]*tf[69];
  r+= rf[1370]*tf[79];
  r+= eps5*rf[1298]*tf[82];
  r+= eps5*rf[1297]*tf[83];
  r+= eps5*rf[1266]*tf[84];
  r+= eps5*rf[1293]*tf[85];
  r+= rf[1365]*tf[88];
  r+= rf[1273]*tf[97];
  r+= eps5*rf[1403]*tf[112];
  r+= eps5*rf[1476]*tf[113];
  r+= eps5*rf[1477]*tf[123];
  r+= eps5*rf[1404]*tf[126];
  r+= eps5*rf[1402]*tf[133];
  r+= rf[1469]*tf[137];
  r+= rf[1435]*tf[166];
  r+= rf[1450]*tf[167];
  r+= eps5*rf[1398]*tf[243];
  r+= eps5*rf[1271]*tf[287];
  r+= rf[1467]*tf[447];
  r+= eps5*rf[1397]*tf[457];
  r+= rf[1308]*tf[479];
  r+= rf[1307]*tf[480];
  r+= rf[1332]*tf[481];
  r+= rf[1449]*tf[670];
  r+= rf[1306]*tf[827];
  r+= rf[1358]*tf[828];
  r+= rf[1301]*tf[829];
  r+= rf[1303]*tf[831];
  r+= rf[1304]*tf[832];
  r+= rf[1296]*tf[833];
  r+= rf[1305]*tf[846];
  r+= eps5*rf[1396]*tf[989];
  r+= eps5*rf[1399]*tf[999];
  r+= rf[1322]*tf[1004];
  r+= rf[1314]*tf[1005];
  r+= rf[1319]*tf[1006];
  r+= rf[1268]*tf[1008];
  r+= rf[1317]*tf[1009];
  r+= rf[1448]*tf[1010];
  r+= rf[1265]*tf[1011];
  r+= rf[1327]*tf[1012];
  r+= rf[1272]*tf[1013];
  r+= rf[1302]*tf[1014];
  r+= rf[1277]*tf[1015];
  r+= rf[1324]*tf[1016];
  r+= rf[1433]*tf[1017];
  r+= rf[1352]*tf[1018];
  r+= rf[1354]*tf[1019];
  r+= rf[1331]*tf[1020];
  r+= rf[1356]*tf[1021];
  r+= rf[1357]*tf[1022];
  r+= rf[1328]*tf[1023];
  r+= rf[1315]*tf[1024];
  r+= rf[1364]*tf[1025];
  r+= rf[1359]*tf[1028];
  r+= rf[1316]*tf[1029];
  r+= rf[1351]*tf[1030];
  r+= rf[1329]*tf[1031];
  r+= eps5*rf[1455]*tf[1047];
  r+= rf[1429]*tf[1050];
  r+= rf[1309]*tf[1052];
  r+= rf[1441]*tf[1053];
  r+= rf[1278]*tf[1055];
  r+= rf[1323]*tf[1058];
  r+= rf[1287]*tf[1059];
  r+= rf[1294]*tf[1060];
  r+= rf[1279]*tf[1062];
  r+= rf[1444]*tf[1063];
  r+= rf[1438]*tf[1065];
  r+= rf[1290]*tf[1066];
  r+= rf[1299]*tf[1067];
  r+= rf[1280]*tf[1069];
  r+= rf[1274]*tf[1070];
  r+= rf[1292]*tf[1071];
  r+= rf[1282]*tf[1072];
  r+= rf[1361]*tf[1074];
  r+= rf[1434]*tf[1075];
  r+= rf[1283]*tf[1077];
  r+= rf[1451]*tf[1080];
  r+= rf[1313]*tf[1086];
  r+= rf[1325]*tf[1092];
  r+= rf[1276]*tf[1093];
  r+= rf[1275]*tf[1094];
  r+= rf[1409]*tf[1095];
  r+= rf[1285]*tf[1096];
  r+= rf[1363]*tf[1097];
  r+= rf[1412]*tf[1099];
  r+= rf[1291]*tf[1102];
  r+= rf[1360]*tf[1103];
  r+= rf[1318]*tf[1107];
  r+= rf[1437]*tf[1110];
  r+= rf[1446]*tf[1112];
  r+= rf[1445]*tf[1113];
  r+= rf[1443]*tf[1118];
  r+= rf[1288]*tf[1122];
  r+= rf[1289]*tf[1123];
  r+= rf[1267]*tf[1125];
  r+= rf[1468]*tf[1128];
  r+= rf[1442]*tf[1129];
  r+= rf[1310]*tf[1131];
  r+= rf[1439]*tf[1132];
  r+= rf[1295]*tf[1133];
  r+= rf[1466]*tf[1134];
  r+= rf[1326]*tf[1136];
  r+= rf[1320]*tf[1137];
  r+= rf[1269]*tf[1138];
  r+= eps5*rf[1334]*tf[1141];
  r+= eps5*rf[1343]*tf[1143];
  r+= rf[1270]*tf[1144];
  r+= rf[1362]*tf[1147];
  r+= rf[1284]*tf[1150];
  r+= rf[1342]*tf[1151];
  r+= rf[1341]*tf[1152];
  r+= rf[1339]*tf[1154];
  r+= rf[1286]*tf[1155];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpppE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1540];
  r+= rf[1551]*tf[38];
  r+= rf[1550]*tf[41];
  r+= rf[1545]*tf[49];
  r+= eps5*rf[1560]*tf[82];
  r+= eps5*rf[1553]*tf[83];
  r+= eps5*rf[1576]*tf[84];
  r+= eps5*rf[1548]*tf[85];
  r+= eps5*rf[1570]*tf[287];
  r+= rf[1534]*tf[1158];
  r+= rf[1531]*tf[1159];
  r+= rf[1535]*tf[1160];
  r+= rf[1530]*tf[1161];
  r+= rf[1532]*tf[1162];
  r+= rf[1536]*tf[1163];
  r+= rf[1577]*tf[1164];
  r+= rf[1573]*tf[1165];
  r+= rf[1565]*tf[1166];
  r+= rf[1578]*tf[1167];
  r+= rf[1563]*tf[1168];
  r+= rf[1581]*tf[1169];
  r+= rf[1582]*tf[1170];
  r+= rf[1564]*tf[1171];
  r+= rf[1580]*tf[1172];
  r+= rf[1567]*tf[1173];
  r+= rf[1579]*tf[1175];
  r+= rf[1562]*tf[1176];
  r+= rf[1566]*tf[1177];
  r+= eps5*rf[1547]*tf[1178];
  r+= rf[1539]*tf[1179];
  r+= rf[1533]*tf[1180];
  r+= rf[1537]*tf[1181];
  r+= rf[1542]*tf[1182];
  r+= rf[1538]*tf[1183];
  r+= rf[1541]*tf[1184];
  r+= rf[1544]*tf[1185];
  r+= rf[1506]*tf[1186];
  r+= rf[1543]*tf[1188];
  r+= rf[1556]*tf[1190];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpppO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1517];
  r+= rf[1527]*tf[38];
  r+= rf[1526]*tf[41];
  r+= rf[1522]*tf[49];
  r+= eps5*rf[1525]*tf[82];
  r+= eps5*rf[1528]*tf[83];
  r+= eps5*rf[1561]*tf[84];
  r+= eps5*rf[1524]*tf[85];
  r+= eps5*rf[1558]*tf[287];
  r+= rf[1511]*tf[1158];
  r+= rf[1508]*tf[1159];
  r+= rf[1512]*tf[1160];
  r+= rf[1507]*tf[1161];
  r+= rf[1509]*tf[1162];
  r+= rf[1513]*tf[1163];
  r+= rf[1568]*tf[1164];
  r+= rf[1559]*tf[1165];
  r+= rf[1554]*tf[1166];
  r+= rf[1569]*tf[1167];
  r+= rf[1549]*tf[1168];
  r+= rf[1574]*tf[1170];
  r+= rf[1552]*tf[1171];
  r+= rf[1572]*tf[1172];
  r+= rf[1557]*tf[1173];
  r+= rf[1575]*tf[1174];
  r+= rf[1571]*tf[1175];
  r+= rf[1546]*tf[1176];
  r+= rf[1555]*tf[1177];
  r+= eps5*rf[1523]*tf[1178];
  r+= rf[1516]*tf[1179];
  r+= rf[1510]*tf[1180];
  r+= rf[1514]*tf[1181];
  r+= rf[1520]*tf[1182];
  r+= rf[1515]*tf[1183];
  r+= rf[1519]*tf[1184];
  r+= rf[1505]*tf[1186];
  r+= rf[1518]*tf[1187];
  r+= rf[1521]*tf[1189];
  r+= rf[1529]*tf[1190];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2426];
  r+= rf[2430]*tf[38];
  r+= rf[2431]*tf[41];
  r+= rf[2429]*tf[49];
  r+= eps5*rf[2419]*tf[1239];
  r+= rf[2407]*tf[1349];
  r+= rf[2423]*tf[1350];
  r+= rf[2412]*tf[1351];
  r+= rf[2422]*tf[1352];
  r+= rf[2424]*tf[1353];
  r+= rf[2425]*tf[1410];
  r+= rf[2433]*tf[1515];
  r+= rf[2427]*tf[1516];
  r+= rf[2432]*tf[1517];
  r+= rf[2435]*tf[1518];
  r+= rf[1584]*tf[1522];
  r+= rf[2399]*tf[1524];
  r+= rf[2436]*tf[1525];
  r+= rf[2434]*tf[1526];
  r+= rf[2428]*tf[1527];
  r+= rf[2400]*tf[1528];
  r+= rf[2398]*tf[1529];
  r+= rf[2437]*tf[1530];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2409];
  r+= rf[2414]*tf[38];
  r+= rf[2413]*tf[41];
  r+= rf[2411]*tf[49];
  r+= eps5*rf[2405]*tf[1239];
  r+= rf[2397]*tf[1349];
  r+= rf[2403]*tf[1350];
  r+= rf[2401]*tf[1351];
  r+= rf[2404]*tf[1352];
  r+= rf[2402]*tf[1353];
  r+= rf[2406]*tf[1410];
  r+= rf[2396]*tf[1411];
  r+= rf[2416]*tf[1515];
  r+= rf[2408]*tf[1516];
  r+= rf[2415]*tf[1517];
  r+= rf[2418]*tf[1518];
  r+= rf[1583]*tf[1522];
  r+= rf[2420]*tf[1523];
  r+= rf[2394]*tf[1524];
  r+= rf[2417]*tf[1526];
  r+= rf[2410]*tf[1527];
  r+= rf[2395]*tf[1529];
  r+= rf[2421]*tf[1530];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmpE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1745];
  r+= rf[1746]*tf[39];
  r+= rf[1744]*tf[40];
  r+= rf[1703]*tf[45];
  r+= rf[1733]*tf[46];
  r+= rf[1740]*tf[48];
  r+= rf[1750]*tf[50];
  r+= rf[1741]*tf[56];
  r+= rf[1734]*tf[61];
  r+= rf[1763]*tf[88];
  r+= rf[1679]*tf[90];
  r+= rf[1675]*tf[93];
  r+= eps5*rf[1728]*tf[112];
  r+= eps5*rf[1727]*tf[123];
  r+= rf[1752]*tf[137];
  r+= rf[1748]*tf[165];
  r+= rf[1681]*tf[189];
  r+= rf[1755]*tf[210];
  r+= rf[1778]*tf[711];
  r+= rf[1769]*tf[1224];
  r+= rf[1749]*tf[1225];
  r+= rf[1753]*tf[1226];
  r+= rf[1743]*tf[1227];
  r+= rf[1761]*tf[1228];
  r+= rf[1754]*tf[1229];
  r+= rf[1760]*tf[1230];
  r+= rf[1737]*tf[1231];
  r+= rf[1768]*tf[1232];
  r+= rf[1742]*tf[1233];
  r+= rf[1762]*tf[1234];
  r+= rf[1767]*tf[1235];
  r+= rf[1771]*tf[1236];
  r+= rf[1747]*tf[1237];
  r+= eps5*rf[1731]*tf[1239];
  r+= rf[1721]*tf[1241];
  r+= rf[1739]*tf[1244];
  r+= rf[1736]*tf[1246];
  r+= rf[1735]*tf[1247];
  r+= rf[1738]*tf[1248];
  r+= rf[1702]*tf[1249];
  r+= rf[1722]*tf[1250];
  r+= rf[1751]*tf[1251];
  r+= rf[1776]*tf[1252];
  r+= rf[1723]*tf[1254];
  r+= rf[1713]*tf[1255];
  r+= rf[1756]*tf[1257];
  r+= rf[1694]*tf[1258];
  r+= rf[1714]*tf[1259];
  r+= rf[1758]*tf[1261];
  r+= rf[1772]*tf[1262];
  r+= rf[1693]*tf[1264];
  r+= rf[1724]*tf[1265];
  r+= rf[1680]*tf[1267];
  r+= rf[1770]*tf[1271];
  r+= rf[1777]*tf[1273];
  r+= rf[1775]*tf[1274];
  r+= rf[1757]*tf[1275];
  r+= rf[1676]*tf[1276];
  r+= rf[1715]*tf[1278];
  r+= rf[1759]*tf[1279];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmmpO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1695];
  r+= rf[1700]*tf[39];
  r+= rf[1697]*tf[40];
  r+= rf[1662]*tf[45];
  r+= rf[1687]*tf[46];
  r+= rf[1690]*tf[48];
  r+= rf[1712]*tf[50];
  r+= rf[1689]*tf[56];
  r+= rf[1688]*tf[61];
  r+= rf[1716]*tf[88];
  r+= rf[1673]*tf[90];
  r+= rf[1664]*tf[93];
  r+= rf[1710]*tf[137];
  r+= rf[1698]*tf[165];
  r+= rf[1707]*tf[210];
  r+= rf[1766]*tf[711];
  r+= eps5*rf[1773]*tf[1223];
  r+= rf[1730]*tf[1224];
  r+= rf[1701]*tf[1225];
  r+= rf[1709]*tf[1226];
  r+= rf[1696]*tf[1227];
  r+= rf[1718]*tf[1228];
  r+= rf[1708]*tf[1229];
  r+= rf[1719]*tf[1230];
  r+= rf[1686]*tf[1231];
  r+= rf[1729]*tf[1232];
  r+= rf[1692]*tf[1233];
  r+= rf[1717]*tf[1234];
  r+= rf[1732]*tf[1235];
  r+= rf[1726]*tf[1236];
  r+= rf[1699]*tf[1238];
  r+= eps5*rf[1691]*tf[1239];
  r+= rf[1683]*tf[1240];
  r+= rf[1685]*tf[1242];
  r+= rf[1682]*tf[1243];
  r+= rf[1670]*tf[1245];
  r+= rf[1684]*tf[1246];
  r+= rf[1663]*tf[1249];
  r+= rf[1672]*tf[1250];
  r+= rf[1711]*tf[1251];
  r+= eps5*rf[1774]*tf[1253];
  r+= rf[1674]*tf[1255];
  r+= rf[1677]*tf[1256];
  r+= rf[1706]*tf[1257];
  r+= rf[1668]*tf[1258];
  r+= rf[1765]*tf[1260];
  r+= rf[1704]*tf[1261];
  r+= rf[1725]*tf[1263];
  r+= rf[1678]*tf[1265];
  r+= rf[1764]*tf[1266];
  r+= rf[1671]*tf[1267];
  r+= rf[1665]*tf[1268];
  r+= rf[1667]*tf[1269];
  r+= -(rf[1661]*tf[1270])/6;
  r+= rf[1727]*tf[1272];
  r+= rf[1705]*tf[1275];
  r+= rf[1666]*tf[1277];
  r+= rf[1669]*tf[1278];
  r+= rf[1720]*tf[1279];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmpmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1833];
  r+= rf[1851]*tf[39];
  r+= rf[1862]*tf[40];
  r+= rf[1834]*tf[45];
  r+= rf[1841]*tf[46];
  r+= rf[1829]*tf[48];
  r+= rf[1869]*tf[50];
  r+= rf[1795]*tf[56];
  r+= rf[1842]*tf[61];
  r+= rf[1874]*tf[88];
  r+= rf[1792]*tf[90];
  r+= rf[1793]*tf[93];
  r+= eps5*rf[1889]*tf[112];
  r+= eps5*rf[1856]*tf[123];
  r+= rf[1866]*tf[319];
  r+= rf[1872]*tf[348];
  r+= rf[1863]*tf[367];
  r+= rf[1845]*tf[413];
  r+= rf[1882]*tf[1224];
  r+= rf[1883]*tf[1236];
  r+= eps5*rf[1858]*tf[1239];
  r+= rf[1846]*tf[1251];
  r+= rf[1847]*tf[1265];
  r+= rf[1873]*tf[1280];
  r+= rf[1868]*tf[1281];
  r+= rf[1876]*tf[1282];
  r+= rf[1861]*tf[1283];
  r+= rf[1875]*tf[1284];
  r+= rf[1850]*tf[1285];
  r+= rf[1867]*tf[1288];
  r+= rf[1843]*tf[1290];
  r+= rf[1837]*tf[1291];
  r+= rf[1880]*tf[1292];
  r+= rf[1881]*tf[1293];
  r+= rf[1879]*tf[1294];
  r+= rf[1838]*tf[1295];
  r+= rf[1836]*tf[1296];
  r+= rf[1830]*tf[1298];
  r+= rf[1794]*tf[1300];
  r+= rf[1840]*tf[1301];
  r+= rf[1835]*tf[1306];
  r+= rf[1839]*tf[1307];
  r+= rf[1892]*tf[1310];
  r+= eps5*rf[1888]*tf[1311];
  r+= eps5*rf[1885]*tf[1312];
  r+= eps5*rf[1848]*tf[1313];
  r+= eps5*rf[1786]*tf[1314];
  r+= eps5*rf[1787]*tf[1314];
  r+= eps5*rf[1884]*tf[1316];
  r+= rf[1878]*tf[1318];
  r+= rf[1894]*tf[1319];
  r+= rf[1891]*tf[1321];
  r+= rf[1890]*tf[1323];
  r+= rf[1877]*tf[1324];
  r+= rf[1852]*tf[1326];
  r+= rf[1844]*tf[1327];
  r+= rf[1895]*tf[1328];
  r+= rf[1831]*tf[1331];
  r+= rf[1865]*tf[1332];
  r+= rf[1864]*tf[1335];
  r+= rf[1896]*tf[1336];
  r+= rf[1893]*tf[1339];
  r+= rf[1832]*tf[1341];
  r+= rf[1809]*tf[1344];
  r+= rf[1860]*tf[1345];
  r+= rf[1808]*tf[1346];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmpmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1796];
  r+= rf[1817]*tf[39];
  r+= rf[1815]*tf[40];
  r+= rf[1798]*tf[45];
  r+= rf[1805]*tf[46];
  r+= rf[1785]*tf[48];
  r+= rf[1802]*tf[50];
  r+= rf[1783]*tf[56];
  r+= rf[1806]*tf[61];
  r+= rf[1826]*tf[88];
  r+= rf[1780]*tf[90];
  r+= rf[1779]*tf[93];
  r+= rf[1822]*tf[319];
  r+= rf[1825]*tf[348];
  r+= rf[1812]*tf[413];
  r+= eps5*rf[1871]*tf[1223];
  r+= rf[1857]*tf[1224];
  r+= rf[1859]*tf[1236];
  r+= eps5*rf[1819]*tf[1239];
  r+= rf[1814]*tf[1265];
  r+= rf[1824]*tf[1281];
  r+= rf[1828]*tf[1282];
  r+= rf[1816]*tf[1285];
  r+= rf[1821]*tf[1286];
  r+= rf[1813]*tf[1287];
  r+= rf[1823]*tf[1288];
  r+= rf[1827]*tf[1289];
  r+= rf[1807]*tf[1290];
  r+= rf[1801]*tf[1291];
  r+= rf[1854]*tf[1292];
  r+= rf[1855]*tf[1293];
  r+= rf[1853]*tf[1294];
  r+= rf[1799]*tf[1297];
  r+= rf[1784]*tf[1299];
  r+= rf[1803]*tf[1302];
  r+= rf[1804]*tf[1303];
  r+= rf[1800]*tf[1304];
  r+= rf[1782]*tf[1305];
  r+= rf[1797]*tf[1308];
  r+= rf[1811]*tf[1309];
  r+= eps5*rf[1870]*tf[1315];
  r+= rf[1849]*tf[1317];
  r+= rf[1887]*tf[1320];
  r+= rf[1886]*tf[1322];
  r+= rf[1848]*tf[1324];
  r+= rf[1889]*tf[1325];
  r+= rf[1818]*tf[1326];
  r+= rf[1810]*tf[1327];
  r+= rf[1781]*tf[1329];
  r+= rf[1884]*tf[1330];
  r+= rf[1790]*tf[1333];
  r+= rf[1789]*tf[1334];
  r+= rf[1791]*tf[1337];
  r+= rf[1885]*tf[1338];
  r+= rf[1888]*tf[1340];
  r+= rf[1786]*tf[1342];
  r+= rf[1787]*tf[1343];
  r+= rf[1788]*tf[1344];
  r+= rf[1820]*tf[1345];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmppE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2052];
  r+= rf[2068]*tf[39];
  r+= rf[2082]*tf[40];
  r+= rf[2081]*tf[50];
  r+= rf[2085]*tf[348];
  r+= rf[2091]*tf[449];
  r+= rf[2099]*tf[491];
  r+= rf[2109]*tf[598];
  r+= rf[2110]*tf[711];
  r+= rf[2115]*tf[775];
  r+= rf[2028]*tf[1158];
  r+= rf[2064]*tf[1159];
  r+= rf[2044]*tf[1160];
  r+= rf[2060]*tf[1161];
  r+= rf[2065]*tf[1162];
  r+= eps5*rf[2078]*tf[1239];
  r+= eps5*rf[2027]*tf[1315];
  r+= rf[2088]*tf[1354];
  r+= rf[2086]*tf[1355];
  r+= rf[2089]*tf[1366];
  r+= rf[2057]*tf[1367];
  r+= rf[2087]*tf[1368];
  r+= rf[2067]*tf[1374];
  r+= rf[2055]*tf[1375];
  r+= eps5*rf[2073]*tf[1377];
  r+= rf[2054]*tf[1378];
  r+= rf[2053]*tf[1385];
  r+= rf[2071]*tf[1386];
  r+= rf[2056]*tf[1387];
  r+= rf[1941]*tf[1397];
  r+= rf[2016]*tf[1398];
  r+= rf[2058]*tf[1399];
  r+= eps5*rf[2022]*tf[1400];
  r+= rf[2096]*tf[1401];
  r+= rf[2025]*tf[1402];
  r+= rf[2063]*tf[1403];
  r+= rf[2094]*tf[1404];
  r+= rf[2095]*tf[1405];
  r+= rf[2066]*tf[1408];
  r+= rf[2072]*tf[1409];
  r+= rf[2070]*tf[1410];
  r+= rf[2061]*tf[1411];
  r+= rf[2098]*tf[1412];
  r+= rf[2062]*tf[1413];
  r+= rf[2051]*tf[1414];
  r+= rf[2059]*tf[1415];
  r+= rf[2097]*tf[1416];
  r+= rf[2029]*tf[1417];
  r+= rf[2116]*tf[1418];
  r+= rf[2111]*tf[1419];
  r+= rf[2114]*tf[1425];
  r+= rf[2093]*tf[1426];
  r+= rf[2092]*tf[1427];
  r+= rf[2117]*tf[1428];
  r+= rf[2113]*tf[1429];
  r+= rf[2108]*tf[1431];
  r+= rf[2112]*tf[1432];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LmppO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2015];
  r+= rf[2039]*tf[39];
  r+= rf[2037]*tf[40];
  r+= rf[2019]*tf[50];
  r+= rf[2046]*tf[348];
  r+= rf[2045]*tf[449];
  r+= rf[2079]*tf[491];
  r+= rf[2100]*tf[711];
  r+= rf[2106]*tf[775];
  r+= rf[2011]*tf[1158];
  r+= rf[2034]*tf[1159];
  r+= rf[2013]*tf[1160];
  r+= rf[2030]*tf[1161];
  r+= rf[2035]*tf[1162];
  r+= eps5*rf[2083]*tf[1223];
  r+= eps5*rf[2043]*tf[1239];
  r+= eps5*rf[2084]*tf[1253];
  r+= eps5*rf[2080]*tf[1348];
  r+= rf[2050]*tf[1354];
  r+= rf[2048]*tf[1355];
  r+= rf[2049]*tf[1366];
  r+= rf[2023]*tf[1367];
  r+= rf[2047]*tf[1368];
  r+= rf[2038]*tf[1374];
  r+= rf[2020]*tf[1375];
  r+= rf[2018]*tf[1378];
  r+= rf[2017]*tf[1385];
  r+= rf[2041]*tf[1386];
  r+= rf[2021]*tf[1387];
  r+= rf[2022]*tf[1397];
  r+= rf[2008]*tf[1398];
  r+= rf[2024]*tf[1399];
  r+= rf[2076]*tf[1401];
  r+= rf[2009]*tf[1402];
  r+= rf[2031]*tf[1403];
  r+= rf[2069]*tf[1404];
  r+= rf[2074]*tf[1405];
  r+= rf[2042]*tf[1406];
  r+= rf[2014]*tf[1407];
  r+= rf[2036]*tf[1408];
  r+= rf[2040]*tf[1410];
  r+= rf[2032]*tf[1411];
  r+= rf[2077]*tf[1412];
  r+= rf[2033]*tf[1413];
  r+= rf[2010]*tf[1415];
  r+= rf[2075]*tf[1416];
  r+= rf[2012]*tf[1417];
  r+= rf[2107]*tf[1418];
  r+= rf[2101]*tf[1419];
  r+= rf[2105]*tf[1420];
  r+= rf[2104]*tf[1421];
  r+= rf[2026]*tf[1422];
  r+= rf[2090]*tf[1423];
  r+= rf[2102]*tf[1424];
  r+= rf[2027]*tf[1430];
  r+= rf[2073]*tf[1433];
  r+= rf[2103]*tf[1434];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1942];
  r+= rf[1959]*tf[39];
  r+= rf[1974]*tf[40];
  r+= rf[1973]*tf[50];
  r+= rf[1977]*tf[348];
  r+= rf[2004]*tf[446];
  r+= rf[2001]*tf[447];
  r+= rf[1985]*tf[449];
  r+= rf[1986]*tf[486];
  r+= rf[2000]*tf[598];
  r+= rf[2006]*tf[599];
  r+= rf[1954]*tf[850];
  r+= eps5*rf[1931]*tf[867];
  r+= eps5*rf[1968]*tf[1239];
  r+= eps5*rf[1906]*tf[1347];
  r+= rf[1925]*tf[1349];
  r+= rf[1955]*tf[1350];
  r+= rf[1916]*tf[1351];
  r+= rf[1951]*tf[1352];
  r+= rf[1956]*tf[1353];
  r+= rf[1980]*tf[1354];
  r+= rf[1978]*tf[1355];
  r+= rf[1908]*tf[1356];
  r+= rf[1917]*tf[1358];
  r+= rf[1953]*tf[1359];
  r+= rf[1952]*tf[1360];
  r+= rf[1988]*tf[1361];
  r+= rf[1962]*tf[1362];
  r+= rf[1949]*tf[1363];
  r+= rf[1991]*tf[1364];
  r+= rf[1983]*tf[1365];
  r+= rf[1981]*tf[1366];
  r+= rf[1947]*tf[1367];
  r+= rf[1979]*tf[1368];
  r+= rf[1989]*tf[1369];
  r+= rf[1961]*tf[1370];
  r+= rf[1950]*tf[1371];
  r+= rf[1957]*tf[1372];
  r+= rf[1958]*tf[1374];
  r+= rf[1945]*tf[1375];
  r+= rf[1987]*tf[1376];
  r+= eps5*rf[1964]*tf[1377];
  r+= rf[1944]*tf[1378];
  r+= rf[1905]*tf[1381];
  r+= rf[2005]*tf[1384];
  r+= rf[1943]*tf[1385];
  r+= rf[1963]*tf[1386];
  r+= rf[1946]*tf[1387];
  r+= rf[1984]*tf[1388];
  r+= rf[1990]*tf[1389];
  r+= rf[2003]*tf[1390];
  r+= rf[2007]*tf[1391];
  r+= rf[2002]*tf[1394];
  r+= rf[1999]*tf[1395];
  r+= rf[1941]*tf[1397];
  r+= rf[1907]*tf[1398];
  r+= rf[1948]*tf[1399];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1909];
  r+= eps5*rf[1976]*tf[112];
  r+= eps5*rf[1975]*tf[123];
  r+= rf[1935]*tf[348];
  r+= rf[1995]*tf[446];
  r+= rf[1998]*tf[447];
  r+= rf[1936]*tf[449];
  r+= rf[1926]*tf[479];
  r+= rf[1928]*tf[480];
  r+= rf[1922]*tf[481];
  r+= rf[1971]*tf[486];
  r+= rf[1921]*tf[510];
  r+= rf[1996]*tf[598];
  r+= rf[1992]*tf[599];
  r+= rf[1920]*tf[850];
  r+= eps5*rf[1934]*tf[1239];
  r+= eps5*rf[1972]*tf[1348];
  r+= rf[1903]*tf[1349];
  r+= rf[1918]*tf[1350];
  r+= rf[1901]*tf[1351];
  r+= rf[1924]*tf[1352];
  r+= rf[1919]*tf[1353];
  r+= rf[1940]*tf[1354];
  r+= rf[1938]*tf[1355];
  r+= rf[1897]*tf[1357];
  r+= rf[1900]*tf[1358];
  r+= rf[1923]*tf[1359];
  r+= rf[1969]*tf[1361];
  r+= rf[1933]*tf[1362];
  r+= rf[1965]*tf[1364];
  r+= rf[1960]*tf[1365];
  r+= rf[1939]*tf[1366];
  r+= rf[1915]*tf[1367];
  r+= rf[1937]*tf[1368];
  r+= rf[1967]*tf[1369];
  r+= rf[1930]*tf[1370];
  r+= rf[1904]*tf[1371];
  r+= rf[1929]*tf[1372];
  r+= rf[1902]*tf[1373];
  r+= rf[1927]*tf[1374];
  r+= rf[1913]*tf[1375];
  r+= rf[1970]*tf[1376];
  r+= rf[1911]*tf[1378];
  r+= rf[1982]*tf[1379];
  r+= rf[1964]*tf[1380];
  r+= rf[1966]*tf[1382];
  r+= rf[1997]*tf[1383];
  r+= rf[1910]*tf[1385];
  r+= rf[1932]*tf[1386];
  r+= rf[1912]*tf[1387];
  r+= rf[1931]*tf[1388];
  r+= rf[1898]*tf[1392];
  r+= rf[1993]*tf[1393];
  r+= rf[1994]*tf[1396];
  r+= rf[1906]*tf[1397];
  r+= rf[1899]*tf[1398];
  r+= rf[1914]*tf[1399];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmpE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2169];
  r+= rf[2190]*tf[39];
  r+= rf[2202]*tf[40];
  r+= rf[2207]*tf[50];
  r+= rf[2211]*tf[88];
  r+= eps5*rf[2195]*tf[112];
  r+= eps5*rf[2223]*tf[302];
  r+= rf[2204]*tf[319];
  r+= rf[2210]*tf[348];
  r+= rf[2182]*tf[413];
  r+= rf[2231]*tf[446];
  r+= rf[2173]*tf[848];
  r+= rf[2177]*tf[850];
  r+= rf[2203]*tf[919];
  r+= rf[2120]*tf[975];
  r+= rf[2219]*tf[1224];
  r+= eps5*rf[2194]*tf[1239];
  r+= rf[2184]*tf[1251];
  r+= eps5*rf[2221]*tf[1253];
  r+= rf[2206]*tf[1281];
  r+= rf[2213]*tf[1282];
  r+= rf[2189]*tf[1285];
  r+= rf[2198]*tf[1286];
  r+= rf[2183]*tf[1287];
  r+= rf[2205]*tf[1288];
  r+= rf[2212]*tf[1289];
  r+= rf[2191]*tf[1326];
  r+= rf[2181]*tf[1327];
  r+= rf[2148]*tf[1344];
  r+= rf[2197]*tf[1345];
  r+= rf[2171]*tf[1349];
  r+= rf[2178]*tf[1350];
  r+= rf[2172]*tf[1351];
  r+= rf[2133]*tf[1352];
  r+= rf[2179]*tf[1353];
  r+= rf[2132]*tf[1356];
  r+= rf[2180]*tf[1436];
  r+= rf[2216]*tf[1437];
  r+= rf[2220]*tf[1438];
  r+= rf[2218]*tf[1439];
  r+= rf[2174]*tf[1440];
  r+= rf[2217]*tf[1441];
  r+= rf[2121]*tf[1443];
  r+= rf[2161]*tf[1444];
  r+= rf[2175]*tf[1445];
  r+= rf[2176]*tf[1447];
  r+= rf[2142]*tf[1449];
  r+= rf[2170]*tf[1450];
  r+= eps5*rf[2224]*tf[1451];
  r+= -(eps5*rf[1661]*tf[1452])/6;
  r+= eps5*rf[2119]*tf[1453];
  r+= eps5*rf[2185]*tf[1454];
  r+= rf[2215]*tf[1456];
  r+= rf[2230]*tf[1457];
  r+= rf[2229]*tf[1458];
  r+= rf[2214]*tf[1460];
  r+= rf[2233]*tf[1461];
  r+= rf[2201]*tf[1464];
  r+= rf[2227]*tf[1465];
  r+= rf[2199]*tf[1466];
  r+= rf[2200]*tf[1468];
  r+= rf[1681]*tf[1470];
  r+= rf[2228]*tf[1471];
  r+= rf[2232]*tf[1472];
  r+= rf[2147]*tf[1474];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpmpO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2134];
  r+= rf[2154]*tf[39];
  r+= rf[2156]*tf[40];
  r+= rf[2141]*tf[50];
  r+= rf[2168]*tf[88];
  r+= rf[2162]*tf[319];
  r+= rf[2165]*tf[348];
  r+= rf[2152]*tf[413];
  r+= rf[2225]*tf[446];
  r+= rf[2136]*tf[828];
  r+= rf[2145]*tf[829];
  r+= rf[2123]*tf[831];
  r+= rf[2125]*tf[832];
  r+= rf[2137]*tf[833];
  r+= rf[2143]*tf[848];
  r+= rf[2140]*tf[850];
  r+= rf[2131]*tf[919];
  r+= eps5*rf[2208]*tf[1223];
  r+= eps5*rf[2157]*tf[1239];
  r+= rf[2150]*tf[1251];
  r+= eps5*rf[2209]*tf[1253];
  r+= rf[2164]*tf[1281];
  r+= rf[2167]*tf[1282];
  r+= rf[2155]*tf[1285];
  r+= rf[2160]*tf[1286];
  r+= rf[2151]*tf[1287];
  r+= rf[2163]*tf[1288];
  r+= rf[2166]*tf[1289];
  r+= rf[2153]*tf[1326];
  r+= rf[2149]*tf[1327];
  r+= rf[2127]*tf[1344];
  r+= rf[2159]*tf[1345];
  r+= rf[2193]*tf[1435];
  r+= rf[2146]*tf[1436];
  r+= rf[2187]*tf[1437];
  r+= rf[2192]*tf[1438];
  r+= rf[2196]*tf[1439];
  r+= rf[2144]*tf[1440];
  r+= rf[2188]*tf[1441];
  r+= rf[2122]*tf[1442];
  r+= rf[2118]*tf[1443];
  r+= rf[2139]*tf[1445];
  r+= rf[2135]*tf[1446];
  r+= rf[2138]*tf[1447];
  r+= rf[2126]*tf[1448];
  r+= rf[2124]*tf[1449];
  r+= rf[2223]*tf[1455];
  r+= rf[2185]*tf[1456];
  r+= rf[2226]*tf[1459];
  r+= rf[2186]*tf[1460];
  r+= rf[2224]*tf[1462];
  r+= rf[2221]*tf[1463];
  r+= rf[2130]*tf[1464];
  r+= rf[2158]*tf[1466];
  r+= -(rf[1661]*tf[1467])/6;
  r+= rf[2129]*tf[1468];
  r+= rf[2222]*tf[1469];
  r+= rf[2119]*tf[1473];
  r+= rf[2128]*tf[1475];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LppmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2301];
  r+= rf[2340]*tf[88];
  r+= eps5*rf[2283]*tf[112];
  r+= eps5*rf[2271]*tf[123];
  r+= rf[2322]*tf[137];
  r+= rf[2323]*tf[165];
  r+= rf[2306]*tf[210];
  r+= rf[2349]*tf[447];
  r+= rf[2302]*tf[479];
  r+= rf[2300]*tf[480];
  r+= rf[2319]*tf[481];
  r+= rf[2333]*tf[1028];
  r+= rf[2330]*tf[1224];
  r+= rf[2326]*tf[1225];
  r+= rf[2314]*tf[1226];
  r+= rf[2324]*tf[1227];
  r+= rf[2311]*tf[1228];
  r+= rf[2305]*tf[1229];
  r+= rf[2327]*tf[1230];
  r+= rf[2325]*tf[1234];
  r+= eps5*rf[2288]*tf[1239];
  r+= rf[2316]*tf[1250];
  r+= rf[2304]*tf[1251];
  r+= rf[2307]*tf[1257];
  r+= rf[2309]*tf[1261];
  r+= rf[2308]*tf[1275];
  r+= rf[2310]*tf[1279];
  r+= rf[2332]*tf[1293];
  r+= rf[2339]*tf[1349];
  r+= rf[2343]*tf[1350];
  r+= rf[2256]*tf[1351];
  r+= rf[2299]*tf[1352];
  r+= rf[2297]*tf[1353];
  r+= rf[2315]*tf[1357];
  r+= rf[2338]*tf[1439];
  r+= rf[2341]*tf[1476];
  r+= rf[2321]*tf[1477];
  r+= rf[2318]*tf[1478];
  r+= rf[2303]*tf[1479];
  r+= rf[2337]*tf[1480];
  r+= rf[2320]*tf[1481];
  r+= rf[2342]*tf[1482];
  r+= rf[2274]*tf[1484];
  r+= rf[2317]*tf[1485];
  r+= rf[2298]*tf[1486];
  r+= rf[2345]*tf[1489];
  r+= rf[2265]*tf[1491];
  r+= rf[2275]*tf[1492];
  r+= rf[2328]*tf[1493];
  r+= rf[2331]*tf[1496];
  r+= rf[2266]*tf[1497];
  r+= rf[1831]*tf[1499];
  r+= rf[2347]*tf[1500];
  r+= rf[2250]*tf[1502];
  r+= rf[2348]*tf[1503];
  r+= rf[2346]*tf[1504];
  r+= rf[2336]*tf[1506];
  r+= rf[2329]*tf[1508];
  r+= rf[2267]*tf[1509];
  r+= rf[2344]*tf[1510];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LppmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2252];
  r+= rf[2296]*tf[88];
  r+= eps5*rf[2335]*tf[112];
  r+= eps5*rf[2334]*tf[123];
  r+= rf[2290]*tf[137];
  r+= rf[2291]*tf[165];
  r+= rf[2260]*tf[210];
  r+= rf[2312]*tf[447];
  r+= rf[2253]*tf[479];
  r+= rf[2251]*tf[480];
  r+= rf[2264]*tf[481];
  r+= rf[2255]*tf[1028];
  r+= rf[2234]*tf[1093];
  r+= rf[2287]*tf[1224];
  r+= rf[2294]*tf[1225];
  r+= rf[2258]*tf[1226];
  r+= rf[2292]*tf[1227];
  r+= rf[2269]*tf[1228];
  r+= rf[2259]*tf[1229];
  r+= rf[2295]*tf[1230];
  r+= rf[2293]*tf[1234];
  r+= eps5*rf[2248]*tf[1239];
  r+= rf[2257]*tf[1251];
  r+= rf[2261]*tf[1257];
  r+= rf[2263]*tf[1261];
  r+= rf[2262]*tf[1275];
  r+= rf[2268]*tf[1279];
  r+= rf[2273]*tf[1293];
  r+= rf[2279]*tf[1349];
  r+= rf[2276]*tf[1350];
  r+= rf[2235]*tf[1351];
  r+= rf[2246]*tf[1352];
  r+= rf[2247]*tf[1353];
  r+= rf[2245]*tf[1357];
  r+= rf[2280]*tf[1439];
  r+= rf[2278]*tf[1476];
  r+= rf[2289]*tf[1477];
  r+= rf[2249]*tf[1478];
  r+= rf[2254]*tf[1479];
  r+= rf[2281]*tf[1480];
  r+= rf[2242]*tf[1481];
  r+= rf[2277]*tf[1482];
  r+= rf[2237]*tf[1483];
  r+= rf[2244]*tf[1485];
  r+= rf[2241]*tf[1486];
  r+= rf[2243]*tf[1487];
  r+= rf[2238]*tf[1488];
  r+= rf[2272]*tf[1490];
  r+= rf[2283]*tf[1494];
  r+= rf[2270]*tf[1495];
  r+= rf[1781]*tf[1498];
  r+= rf[2284]*tf[1500];
  r+= rf[2236]*tf[1501];
  r+= rf[2282]*tf[1503];
  r+= rf[2285]*tf[1504];
  r+= rf[2239]*tf[1505];
  r+= rf[2313]*tf[1507];
  r+= rf[2286]*tf[1511];
  r+= rf[2240]*tf[1512];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpppE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2383];
  r+= rf[2387]*tf[38];
  r+= rf[2386]*tf[41];
  r+= rf[2385]*tf[49];
  r+= rf[2368]*tf[1158];
  r+= rf[2378]*tf[1159];
  r+= rf[2363]*tf[1160];
  r+= rf[2380]*tf[1161];
  r+= rf[2379]*tf[1162];
  r+= rf[2355]*tf[1179];
  r+= eps5*rf[2375]*tf[1239];
  r+= rf[2391]*tf[1405];
  r+= rf[2384]*tf[1410];
  r+= rf[2354]*tf[1415];
  r+= rf[2389]*tf[1515];
  r+= rf[2382]*tf[1516];
  r+= rf[2388]*tf[1517];
  r+= rf[2393]*tf[1518];
  r+= rf[2390]*tf[1519];
  r+= rf[2356]*tf[1520];
  r+= rf[2381]*tf[1521];
  r+= rf[1506]*tf[1522];
  r+= rf[2392]*tf[1523];
  return r;
}

template<class T>
std::complex<T> qqb_HA2L_LpppO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2365];
  r+= rf[2370]*tf[38];
  r+= rf[2369]*tf[41];
  r+= rf[2367]*tf[49];
  r+= rf[2357]*tf[1158];
  r+= rf[2358]*tf[1159];
  r+= rf[2353]*tf[1160];
  r+= rf[2360]*tf[1161];
  r+= rf[2359]*tf[1162];
  r+= rf[2351]*tf[1179];
  r+= eps5*rf[2361]*tf[1239];
  r+= rf[2377]*tf[1405];
  r+= rf[2366]*tf[1410];
  r+= rf[2352]*tf[1513];
  r+= rf[2350]*tf[1514];
  r+= rf[2372]*tf[1515];
  r+= rf[2364]*tf[1516];
  r+= rf[2371]*tf[1517];
  r+= rf[2374]*tf[1518];
  r+= rf[2373]*tf[1519];
  r+= rf[2362]*tf[1521];
  r+= rf[1505]*tf[1522];
  r+= rf[2376]*tf[1523];
  return r;
}

/* ------------------------------ qg ----------------------------- */
template<class T>
std::complex<T> qg_HA2L_LmmmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[52]*tf[18];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[51]*tf[1];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmpE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[5]*tf[1];
  r+= rf[6]*tf[2];
  r+= rf[7]*tf[3];
  r+= rf[8]*tf[5];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmpO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[1]*tf[1];
  r+= rf[2]*tf[2];
  r+= rf[3]*tf[3];
  r+= rf[4]*tf[4];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmpmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[14]*tf[3];
  r+= rf[13]*tf[6];
  r+= rf[15]*tf[7];
  r+= rf[16]*tf[8];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmpmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[10]*tf[3];
  r+= rf[9]*tf[6];
  r+= rf[11]*tf[7];
  r+= rf[12]*tf[9];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmppE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[29]*tf[2];
  r+= rf[31]*tf[6];
  r+= rf[30]*tf[10];
  r+= rf[32]*tf[12];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmppO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[27]*tf[6];
  r+= rf[28]*tf[13];
  r+= rf[25]*tf[14];
  r+= rf[26]*tf[15];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[19]*tf[2];
  r+= rf[23]*tf[6];
  r+= rf[24]*tf[10];
  r+= rf[22]*tf[13];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[18]*tf[1];
  r+= rf[21]*tf[7];
  r+= rf[17]*tf[11];
  r+= rf[20]*tf[12];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmpE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[40]*tf[1];
  r+= rf[37]*tf[3];
  r+= rf[38]*tf[9];
  r+= rf[39]*tf[10];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmpO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[35]*tf[3];
  r+= rf[33]*tf[6];
  r+= rf[34]*tf[7];
  r+= rf[36]*tf[16];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LppmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[45]*tf[2];
  r+= rf[46]*tf[3];
  r+= rf[47]*tf[4];
  r+= rf[48]*tf[6];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LppmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[44]*tf[4];
  r+= rf[41]*tf[6];
  r+= rf[42]*tf[15];
  r+= rf[43]*tf[17];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpppE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[50]*tf[18];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpppO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
) {
  std::complex<T> r(0,0);

  r+= rf[49]*tf[18];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1619];
  r+= eps5*rf[1653]*tf[52];
  r+= eps5*rf[1612]*tf[53];
  r+= eps5*rf[1593]*tf[54];
  r+= eps5*rf[1626]*tf[76];
  r+= eps5*rf[1630]*tf[77];
  r+= eps5*rf[1645]*tf[255];
  r+= rf[1634]*tf[1096];
  r+= rf[1650]*tf[1097];
  r+= rf[1647]*tf[1098];
  r+= rf[1639]*tf[1099];
  r+= rf[1657]*tf[1108];
  r+= rf[1656]*tf[1109];
  r+= rf[1618]*tf[1110];
  r+= rf[1659]*tf[1111];
  r+= rf[1644]*tf[1112];
  r+= rf[1658]*tf[1113];
  r+= rf[1584]*tf[1114];
  r+= rf[1643]*tf[1117];
  r+= rf[1640]*tf[1118];
  r+= rf[1633]*tf[1119];
  r+= rf[1635]*tf[1120];
  r+= rf[1617]*tf[1123];
  r+= rf[1605]*tf[1124];
  r+= rf[1620]*tf[1125];
  r+= rf[1627]*tf[1126];
  r+= rf[1607]*tf[1127];
  r+= rf[1623]*tf[1128];
  r+= rf[1616]*tf[1129];
  r+= rf[1608]*tf[1130];
  r+= rf[1628]*tf[1131];
  r+= rf[1606]*tf[1132];
  r+= rf[1636]*tf[1134];
  r+= rf[1637]*tf[1136];
  r+= rf[1638]*tf[1137];
  r+= rf[1641]*tf[1138];
  r+= rf[1642]*tf[1139];
  r+= rf[1649]*tf[1140];
  r+= rf[1648]*tf[1141];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1594];
  r+= eps5*rf[1614]*tf[52];
  r+= eps5*rf[1596]*tf[53];
  r+= eps5*rf[1583]*tf[54];
  r+= eps5*rf[1600]*tf[75];
  r+= eps5*rf[1603]*tf[77];
  r+= eps5*rf[1604]*tf[255];
  r+= rf[1609]*tf[1096];
  r+= rf[1621]*tf[1097];
  r+= rf[1655]*tf[1098];
  r+= rf[1654]*tf[1108];
  r+= rf[1646]*tf[1109];
  r+= rf[1652]*tf[1111];
  r+= rf[1582]*tf[1114];
  r+= rf[1589]*tf[1115];
  r+= rf[1597]*tf[1116];
  r+= rf[1625]*tf[1118];
  r+= rf[1631]*tf[1121];
  r+= rf[1624]*tf[1122];
  r+= rf[1591]*tf[1123];
  r+= rf[1586]*tf[1124];
  r+= rf[1595]*tf[1125];
  r+= rf[1601]*tf[1126];
  r+= rf[1588]*tf[1127];
  r+= rf[1599]*tf[1128];
  r+= rf[1590]*tf[1129];
  r+= rf[1587]*tf[1130];
  r+= rf[1602]*tf[1131];
  r+= rf[1585]*tf[1132];
  r+= rf[1632]*tf[1133];
  r+= rf[1598]*tf[1134];
  r+= rf[1592]*tf[1135];
  r+= rf[1610]*tf[1136];
  r+= rf[1615]*tf[1137];
  r+= rf[1629]*tf[1139];
  r+= rf[1622]*tf[1142];
  r+= rf[1651]*tf[1143];
  r+= rf[1613]*tf[1144];
  r+= rf[1611]*tf[1145];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmpE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[241]*tf[19];
  r+= rf[274]*tf[20];
  r+= rf[285]*tf[21];
  r+= rf[81]*tf[22];
  r+= rf[211]*tf[23];
  r+= rf[282]*tf[24];
  r+= rf[283]*tf[25];
  r+= rf[273]*tf[26];
  r+= rf[292]*tf[27];
  r+= rf[206]*tf[28];
  r+= rf[284]*tf[29];
  r+= rf[252]*tf[30];
  r+= rf[226]*tf[31];
  r+= rf[276]*tf[32];
  r+= rf[221]*tf[33];
  r+= rf[291]*tf[34];
  r+= rf[269]*tf[35];
  r+= rf[242]*tf[36];
  r+= rf[229]*tf[37];
  r+= rf[191]*tf[38];
  r+= rf[218]*tf[39];
  r+= rf[224]*tf[40];
  r+= rf[290]*tf[41];
  r+= rf[217]*tf[42];
  r+= rf[238]*tf[43];
  r+= rf[254]*tf[44];
  r+= rf[275]*tf[45];
  r+= rf[189]*tf[46];
  r+= rf[277]*tf[47];
  r+= rf[230]*tf[48];
  r+= rf[253]*tf[49];
  r+= rf[247]*tf[50];
  r+= eps5*rf[287]*tf[52];
  r+= eps5*rf[199]*tf[53];
  r+= eps5*rf[140]*tf[54];
  r+= rf[265]*tf[55];
  r+= rf[251]*tf[56];
  r+= rf[225]*tf[57];
  r+= rf[222]*tf[58];
  r+= rf[219]*tf[59];
  r+= rf[210]*tf[60];
  r+= rf[220]*tf[61];
  r+= rf[223]*tf[62];
  r+= rf[244]*tf[63];
  r+= rf[235]*tf[64];
  r+= rf[272]*tf[65];
  r+= rf[98]*tf[66];
  r+= rf[209]*tf[67];
  r+= rf[250]*tf[68];
  r+= rf[246]*tf[69];
  r+= rf[158]*tf[70];
  r+= rf[190]*tf[72];
  r+= rf[248]*tf[73];
  r+= rf[114]*tf[74];
  r+= eps5*rf[80]*tf[75];
  r+= eps5*rf[133]*tf[77];
  r+= rf[267]*tf[78];
  r+= eps5*rf[60]*tf[79];
  r+= eps5*rf[108]*tf[80];
  r+= eps5*rf[79]*tf[81];
  r+= eps5*rf[91]*tf[81];
  r+= eps5*rf[178]*tf[83];
  r+= eps5*rf[88]*tf[85];
  r+= eps5*rf[75]*tf[86];
  r+= eps5*rf[82]*tf[88];
  r+= eps5*rf[83]*tf[89];
  r+= eps5*rf[167]*tf[90];
  r+= eps5*rf[62]*tf[91];
  r+= eps5*rf[71]*tf[94];
  r+= eps5*rf[126]*tf[95];
  r+= eps5*rf[87]*tf[96];
  r+= eps5*rf[143]*tf[97];
  r+= eps5*rf[78]*tf[98];
  r+= eps5*rf[92]*tf[98];
  r+= eps5*rf[76]*tf[99];
  r+= eps5*rf[169]*tf[100];
  r+= eps5*rf[130]*tf[101];
  r+= eps5*rf[63]*tf[102];
  r+= eps5*rf[136]*tf[103];
  r+= eps5*rf[134]*tf[104];
  r+= eps5*rf[170]*tf[106];
  r+= eps5*rf[125]*tf[107];
  r+= eps5*rf[107]*tf[108];
  r+= eps5*rf[106]*tf[109];
  r+= eps5*rf[61]*tf[110];
  r+= eps5*rf[74]*tf[111];
  r+= eps5*rf[131]*tf[112];
  r+= eps5*rf[144]*tf[113];
  r+= eps5*rf[59]*tf[114];
  r+= eps5*rf[104]*tf[115];
  r+= eps5*rf[129]*tf[116];
  r+= eps5*rf[65]*tf[120];
  r+= rf[249]*tf[121];
  r+= rf[263]*tf[122];
  r+= rf[155]*tf[123];
  r+= rf[116]*tf[125];
  r+= rf[239]*tf[126];
  r+= rf[232]*tf[127];
  r+= rf[227]*tf[128];
  r+= rf[237]*tf[129];
  r+= rf[89]*tf[130];
  r+= rf[111]*tf[131];
  r+= rf[233]*tf[132];
  r+= rf[188]*tf[134];
  r+= rf[112]*tf[135];
  r+= rf[97]*tf[137];
  r+= rf[228]*tf[138];
  r+= rf[205]*tf[140];
  r+= rf[156]*tf[146];
  r+= rf[186]*tf[152];
  r+= rf[214]*tf[153];
  r+= rf[260]*tf[156];
  r+= rf[257]*tf[159];
  r+= rf[72]*tf[160];
  r+= rf[259]*tf[161];
  r+= rf[243]*tf[162];
  r+= rf[266]*tf[164];
  r+= rf[90]*tf[166];
  r+= rf[256]*tf[167];
  r+= rf[270]*tf[168];
  r+= rf[95]*tf[169];
  r+= rf[258]*tf[171];
  r+= rf[157]*tf[172];
  r+= rf[115]*tf[173];
  r+= rf[215]*tf[174];
  r+= rf[255]*tf[177];
  r+= rf[204]*tf[178];
  r+= rf[185]*tf[181];
  r+= rf[236]*tf[184];
  r+= rf[264]*tf[185];
  r+= rf[216]*tf[186];
  r+= rf[184]*tf[189];
  r+= rf[261]*tf[190];
  r+= rf[187]*tf[200];
  r+= rf[96]*tf[205];
  r+= rf[213]*tf[207];
  r+= rf[245]*tf[208];
  r+= rf[234]*tf[209];
  r+= rf[212]*tf[210];
  r+= rf[240]*tf[211];
  r+= rf[231]*tf[212];
  r+= rf[113]*tf[213];
  r+= eps5*rf[64]*tf[214];
  r+= eps5*rf[175]*tf[217];
  r+= eps5*rf[66]*tf[218];
  r+= rf[153]*tf[221];
  r+= rf[271]*tf[223];
  r+= rf[154]*tf[225];
  r+= rf[151]*tf[226];
  r+= rf[268]*tf[227];
  r+= rf[262]*tf[229];
  r+= rf[152]*tf[232];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmpO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[138]*tf[19];
  r+= rf[99]*tf[20];
  r+= rf[195]*tf[21];
  r+= rf[70]*tf[22];
  r+= rf[102]*tf[23];
  r+= rf[198]*tf[24];
  r+= rf[197]*tf[25];
  r+= rf[103]*tf[26];
  r+= rf[286]*tf[27];
  r+= rf[93]*tf[28];
  r+= rf[196]*tf[29];
  r+= rf[150]*tf[30];
  r+= rf[124]*tf[31];
  r+= rf[202]*tf[32];
  r+= rf[119]*tf[33];
  r+= rf[288]*tf[34];
  r+= rf[176]*tf[35];
  r+= rf[139]*tf[36];
  r+= rf[127]*tf[37];
  r+= rf[86]*tf[38];
  r+= rf[110]*tf[39];
  r+= rf[122]*tf[40];
  r+= rf[289]*tf[41];
  r+= rf[109]*tf[42];
  r+= rf[135]*tf[43];
  r+= rf[160]*tf[44];
  r+= rf[201]*tf[45];
  r+= rf[77]*tf[46];
  r+= rf[200]*tf[47];
  r+= rf[128]*tf[48];
  r+= rf[159]*tf[49];
  r+= rf[142]*tf[51];
  r+= eps5*rf[203]*tf[52];
  r+= eps5*rf[94]*tf[53];
  r+= eps5*rf[56]*tf[54];
  r+= rf[171]*tf[55];
  r+= rf[149]*tf[56];
  r+= rf[123]*tf[57];
  r+= rf[120]*tf[58];
  r+= rf[117]*tf[59];
  r+= rf[101]*tf[60];
  r+= rf[118]*tf[61];
  r+= rf[121]*tf[62];
  r+= rf[141]*tf[63];
  r+= rf[132]*tf[64];
  r+= rf[179]*tf[65];
  r+= rf[57]*tf[66];
  r+= rf[100]*tf[67];
  r+= rf[148]*tf[68];
  r+= rf[145]*tf[69];
  r+= rf[69]*tf[70];
  r+= rf[73]*tf[71];
  r+= rf[146]*tf[73];
  r+= rf[58]*tf[74];
  r+= eps5*rf[68]*tf[76];
  r+= eps5*rf[55]*tf[77];
  r+= rf[173]*tf[78];
  r+= eps5*rf[279]*tf[82];
  r+= eps5*rf[180]*tf[84];
  r+= eps5*rf[208]*tf[87];
  r+= eps5*rf[281]*tf[92];
  r+= eps5*rf[183]*tf[93];
  r+= eps5*rf[192]*tf[105];
  r+= eps5*rf[280]*tf[106];
  r+= eps5*rf[193]*tf[117];
  r+= eps5*rf[278]*tf[118];
  r+= eps5*rf[207]*tf[119];
  r+= rf[147]*tf[121];
  r+= rf[65]*tf[124];
  r+= rf[78]*tf[133];
  r+= rf[104]*tf[136];
  r+= rf[76]*tf[139];
  r+= rf[74]*tf[141];
  r+= rf[75]*tf[142];
  r+= rf[71]*tf[143];
  r+= rf[169]*tf[144];
  r+= rf[165]*tf[145];
  r+= rf[163]*tf[147];
  r+= rf[170]*tf[148];
  r+= rf[83]*tf[149];
  r+= rf[87]*tf[150];
  r+= rf[92]*tf[151];
  r+= rf[174]*tf[154];
  r+= rf[105]*tf[155];
  r+= rf[79]*tf[157];
  r+= rf[168]*tf[158];
  r+= rf[134]*tf[163];
  r+= rf[143]*tf[165];
  r+= rf[162]*tf[167];
  r+= rf[177]*tf[168];
  r+= rf[107]*tf[170];
  r+= rf[67]*tf[172];
  r+= rf[53]*tf[175];
  r+= rf[82]*tf[176];
  r+= rf[161]*tf[177];
  r+= rf[172]*tf[179];
  r+= rf[164]*tf[180];
  r+= rf[85]*tf[182];
  r+= rf[126]*tf[183];
  r+= rf[84]*tf[187];
  r+= rf[137]*tf[188];
  r+= rf[166]*tf[190];
  r+= rf[106]*tf[191];
  r+= rf[59]*tf[192];
  r+= rf[54]*tf[193];
  r+= rf[62]*tf[194];
  r+= rf[131]*tf[195];
  r+= rf[88]*tf[196];
  r+= rf[60]*tf[197];
  r+= rf[129]*tf[198];
  r+= rf[91]*tf[199];
  r+= rf[144]*tf[201];
  r+= rf[136]*tf[202];
  r+= rf[130]*tf[203];
  r+= rf[125]*tf[204];
  r+= rf[108]*tf[206];
  r+= eps5*rf[182]*tf[215];
  r+= eps5*rf[194]*tf[216];
  r+= eps5*rf[181]*tf[219];
  r+= rf[63]*tf[220];
  r+= rf[178]*tf[222];
  r+= rf[64]*tf[224];
  r+= rf[175]*tf[228];
  r+= rf[167]*tf[230];
  r+= rf[66]*tf[231];
  r+= rf[61]*tf[233];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmpmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[440]*tf[20];
  r+= rf[437]*tf[21];
  r+= rf[359]*tf[22];
  r+= rf[436]*tf[23];
  r+= rf[438]*tf[24];
  r+= rf[441]*tf[25];
  r+= rf[487]*tf[26];
  r+= rf[442]*tf[28];
  r+= rf[357]*tf[29];
  r+= rf[443]*tf[30];
  r+= rf[494]*tf[32];
  r+= rf[511]*tf[41];
  r+= rf[489]*tf[49];
  r+= eps5*rf[388]*tf[53];
  r+= eps5*rf[389]*tf[54];
  r+= rf[526]*tf[55];
  r+= rf[465]*tf[56];
  r+= rf[444]*tf[59];
  r+= eps5*rf[334]*tf[76];
  r+= eps5*rf[364]*tf[77];
  r+= eps5*rf[382]*tf[91];
  r+= eps5*rf[416]*tf[118];
  r+= rf[527]*tf[234];
  r+= eps5*rf[391]*tf[235];
  r+= rf[486]*tf[236];
  r+= rf[503]*tf[237];
  r+= rf[475]*tf[238];
  r+= rf[504]*tf[239];
  r+= rf[502]*tf[240];
  r+= rf[512]*tf[241];
  r+= rf[483]*tf[242];
  r+= rf[506]*tf[243];
  r+= rf[479]*tf[244];
  r+= rf[464]*tf[245];
  r+= rf[476]*tf[247];
  r+= rf[509]*tf[248];
  r+= rf[433]*tf[249];
  r+= rf[474]*tf[250];
  r+= rf[454]*tf[251];
  r+= rf[505]*tf[252];
  r+= rf[508]*tf[253];
  r+= rf[432]*tf[254];
  r+= eps5*rf[352]*tf[255];
  r+= rf[447]*tf[256];
  r+= rf[445]*tf[257];
  r+= rf[458]*tf[258];
  r+= rf[453]*tf[259];
  r+= rf[493]*tf[260];
  r+= rf[532]*tf[261];
  r+= rf[484]*tf[262];
  r+= rf[446]*tf[263];
  r+= rf[462]*tf[264];
  r+= rf[363]*tf[265];
  r+= rf[439]*tf[266];
  r+= rf[463]*tf[267];
  r+= rf[461]*tf[268];
  r+= rf[374]*tf[269];
  r+= rf[375]*tf[271];
  r+= rf[485]*tf[272];
  r+= rf[517]*tf[274];
  r+= eps5*rf[415]*tf[275];
  r+= eps5*rf[371]*tf[276];
  r+= eps5*rf[387]*tf[276];
  r+= eps5*rf[311]*tf[277];
  r+= eps5*rf[390]*tf[278];
  r+= eps5*rf[423]*tf[279];
  r+= eps5*rf[316]*tf[280];
  r+= eps5*rf[320]*tf[281];
  r+= eps5*rf[335]*tf[281];
  r+= eps5*rf[421]*tf[282];
  r+= eps5*rf[315]*tf[283];
  r+= eps5*rf[310]*tf[284];
  r+= eps5*rf[356]*tf[286];
  r+= eps5*rf[386]*tf[287];
  r+= eps5*rf[370]*tf[288];
  r+= eps5*rf[393]*tf[289];
  r+= eps5*rf[333]*tf[291];
  r+= eps5*rf[378]*tf[292];
  r+= eps5*rf[303]*tf[293];
  r+= eps5*rf[346]*tf[293];
  r+= eps5*rf[305]*tf[294];
  r+= eps5*rf[338]*tf[295];
  r+= eps5*rf[336]*tf[296];
  r+= eps5*rf[403]*tf[297];
  r+= eps5*rf[337]*tf[298];
  r+= eps5*rf[380]*tf[299];
  r+= eps5*rf[367]*tf[300];
  r+= eps5*rf[399]*tf[301];
  r+= eps5*rf[381]*tf[302];
  r+= eps5*rf[341]*tf[303];
  r+= eps5*rf[306]*tf[304];
  r+= eps5*rf[392]*tf[305];
  r+= rf[510]*tf[306];
  r+= rf[501]*tf[319];
  r+= rf[477]*tf[320];
  r+= rf[434]*tf[324];
  r+= rf[480]*tf[326];
  r+= rf[500]*tf[327];
  r+= rf[449]*tf[329];
  r+= rf[455]*tf[331];
  r+= rf[362]*tf[332];
  r+= rf[499]*tf[333];
  r+= rf[450]*tf[334];
  r+= rf[451]*tf[335];
  r+= rf[430]*tf[336];
  r+= rf[518]*tf[339];
  r+= rf[482]*tf[340];
  r+= rf[435]*tf[341];
  r+= rf[452]*tf[342];
  r+= rf[515]*tf[349];
  r+= rf[530]*tf[351];
  r+= rf[361]*tf[352];
  r+= rf[496]*tf[353];
  r+= rf[488]*tf[354];
  r+= rf[520]*tf[355];
  r+= rf[497]*tf[356];
  r+= rf[524]*tf[357];
  r+= rf[459]*tf[360];
  r+= rf[498]*tf[362];
  r+= rf[495]*tf[363];
  r+= rf[492]*tf[364];
  r+= rf[513]*tf[366];
  r+= rf[431]*tf[368];
  r+= rf[296]*tf[369];
  r+= rf[478]*tf[370];
  r+= rf[481]*tf[373];
  r+= rf[366]*tf[374];
  r+= rf[516]*tf[375];
  r+= rf[429]*tf[376];
  r+= rf[460]*tf[378];
  r+= rf[491]*tf[379];
  r+= rf[525]*tf[380];
  r+= rf[369]*tf[381];
  r+= rf[528]*tf[382];
  r+= rf[519]*tf[383];
  r+= rf[521]*tf[385];
  r+= rf[507]*tf[387];
  r+= rf[313]*tf[388];
  r+= rf[456]*tf[389];
  r+= rf[490]*tf[390];
  r+= rf[523]*tf[392];
  r+= rf[360]*tf[397];
  r+= eps5*rf[342]*tf[401];
  r+= eps5*rf[419]*tf[403];
  r+= eps5*rf[299]*tf[404];
  r+= rf[514]*tf[405];
  r+= rf[531]*tf[407];
  r+= rf[358]*tf[411];
  r+= rf[448]*tf[412];
  r+= rf[529]*tf[414];
  r+= rf[522]*tf[415];
  r+= rf[457]*tf[416];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmpmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[325]*tf[20];
  r+= rf[322]*tf[21];
  r+= rf[300]*tf[22];
  r+= rf[321]*tf[23];
  r+= rf[323]*tf[24];
  r+= rf[326]*tf[25];
  r+= rf[377]*tf[26];
  r+= rf[327]*tf[28];
  r+= rf[298]*tf[29];
  r+= rf[328]*tf[30];
  r+= rf[384]*tf[32];
  r+= rf[404]*tf[41];
  r+= rf[379]*tf[49];
  r+= eps5*rf[309]*tf[53];
  r+= eps5*rf[308]*tf[54];
  r+= rf[417]*tf[55];
  r+= rf[351]*tf[56];
  r+= rf[329]*tf[59];
  r+= eps5*rf[295]*tf[76];
  r+= eps5*rf[302]*tf[77];
  r+= eps5*rf[469]*tf[82];
  r+= eps5*rf[427]*tf[84];
  r+= eps5*rf[426]*tf[92];
  r+= eps5*rf[428]*tf[93];
  r+= eps5*rf[471]*tf[105];
  r+= eps5*rf[466]*tf[106];
  r+= eps5*rf[470]*tf[117];
  r+= eps5*rf[468]*tf[118];
  r+= rf[418]*tf[234];
  r+= rf[376]*tf[236];
  r+= rf[395]*tf[237];
  r+= rf[354]*tf[238];
  r+= rf[396]*tf[239];
  r+= rf[394]*tf[240];
  r+= rf[405]*tf[241];
  r+= rf[372]*tf[242];
  r+= rf[398]*tf[243];
  r+= rf[368]*tf[244];
  r+= rf[348]*tf[246];
  r+= rf[355]*tf[247];
  r+= rf[401]*tf[248];
  r+= rf[318]*tf[249];
  r+= rf[353]*tf[250];
  r+= rf[340]*tf[251];
  r+= rf[397]*tf[252];
  r+= rf[400]*tf[253];
  r+= rf[317]*tf[254];
  r+= eps5*rf[297]*tf[255];
  r+= rf[332]*tf[256];
  r+= rf[330]*tf[257];
  r+= rf[344]*tf[258];
  r+= rf[339]*tf[259];
  r+= rf[383]*tf[260];
  r+= rf[422]*tf[261];
  r+= rf[373]*tf[262];
  r+= rf[331]*tf[263];
  r+= rf[349]*tf[264];
  r+= rf[301]*tf[265];
  r+= rf[324]*tf[266];
  r+= rf[350]*tf[267];
  r+= rf[347]*tf[268];
  r+= rf[304]*tf[270];
  r+= rf[307]*tf[271];
  r+= rf[365]*tf[273];
  r+= rf[410]*tf[274];
  r+= eps5*rf[472]*tf[285];
  r+= eps5*rf[473]*tf[290];
  r+= rf[402]*tf[306];
  r+= eps5*rf[425]*tf[307];
  r+= eps5*rf[424]*tf[308];
  r+= rf[416]*tf[309];
  r+= rf[392]*tf[310];
  r+= rf[310]*tf[311];
  r+= rf[341]*tf[312];
  r+= rf[336]*tf[313];
  r+= rf[386]*tf[314];
  r+= rf[385]*tf[315];
  r+= rf[343]*tf[316];
  r+= rf[314]*tf[317];
  r+= rf[311]*tf[318];
  r+= rf[316]*tf[321];
  r+= rf[312]*tf[322];
  r+= rf[320]*tf[323];
  r+= rf[315]*tf[325];
  r+= rf[380]*tf[328];
  r+= rf[399]*tf[330];
  r+= rf[387]*tf[337];
  r+= rf[415]*tf[338];
  r+= rf[411]*tf[339];
  r+= rf[335]*tf[343];
  r+= rf[390]*tf[344];
  r+= rf[409]*tf[345];
  r+= rf[367]*tf[346];
  r+= rf[381]*tf[347];
  r+= rf[319]*tf[348];
  r+= rf[407]*tf[349];
  r+= rf[306]*tf[350];
  r+= rf[420]*tf[351];
  r+= rf[345]*tf[358];
  r+= rf[293]*tf[359];
  r+= rf[370]*tf[361];
  r+= rf[408]*tf[365];
  r+= rf[406]*tf[366];
  r+= rf[413]*tf[367];
  r+= rf[294]*tf[371];
  r+= rf[356]*tf[372];
  r+= rf[371]*tf[377];
  r+= rf[414]*tf[384];
  r+= rf[412]*tf[385];
  r+= rf[305]*tf[386];
  r+= rf[303]*tf[391];
  r+= rf[346]*tf[393];
  r+= rf[378]*tf[394];
  r+= rf[393]*tf[395];
  r+= rf[333]*tf[396];
  r+= rf[382]*tf[398];
  r+= rf[338]*tf[399];
  r+= rf[391]*tf[400];
  r+= eps5*rf[467]*tf[402];
  r+= rf[421]*tf[406];
  r+= rf[342]*tf[408];
  r+= rf[419]*tf[409];
  r+= rf[403]*tf[410];
  r+= rf[423]*tf[413];
  r+= rf[337]*tf[417];
  r+= rf[299]*tf[418];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmppE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[895];
  r+= rf[898]*tf[20];
  r+= rf[931]*tf[21];
  r+= rf[919]*tf[28];
  r+= rf[971]*tf[32];
  r+= rf[892]*tf[33];
  r+= rf[927]*tf[40];
  r+= rf[973]*tf[41];
  r+= rf[972]*tf[49];
  r+= eps5*rf[859]*tf[52];
  r+= eps5*rf[787]*tf[53];
  r+= rf[997]*tf[55];
  r+= eps5*rf[875]*tf[76];
  r+= eps5*rf[880]*tf[77];
  r+= eps5*rf[820]*tf[88];
  r+= eps5*rf[858]*tf[106];
  r+= eps5*rf[864]*tf[118];
  r+= rf[1023]*tf[121];
  r+= rf[921]*tf[236];
  r+= rf[905]*tf[254];
  r+= eps5*rf[822]*tf[255];
  r+= rf[982]*tf[260];
  r+= eps5*rf[959]*tf[280];
  r+= rf[939]*tf[351];
  r+= rf[899]*tf[421];
  r+= rf[918]*tf[422];
  r+= rf[933]*tf[423];
  r+= rf[934]*tf[424];
  r+= rf[897]*tf[425];
  r+= rf[922]*tf[427];
  r+= rf[928]*tf[428];
  r+= rf[930]*tf[430];
  r+= rf[923]*tf[431];
  r+= rf[906]*tf[432];
  r+= rf[894]*tf[433];
  r+= rf[941]*tf[434];
  r+= rf[891]*tf[435];
  r+= rf[925]*tf[436];
  r+= rf[924]*tf[437];
  r+= rf[942]*tf[438];
  r+= rf[932]*tf[439];
  r+= rf[992]*tf[440];
  r+= rf[1001]*tf[441];
  r+= rf[904]*tf[442];
  r+= rf[991]*tf[444];
  r+= rf[935]*tf[445];
  r+= rf[994]*tf[448];
  r+= rf[995]*tf[451];
  r+= rf[893]*tf[452];
  r+= rf[1004]*tf[453];
  r+= rf[985]*tf[456];
  r+= rf[915]*tf[457];
  r+= rf[983]*tf[458];
  r+= rf[929]*tf[459];
  r+= rf[1003]*tf[590];
  r+= eps5*rf[799]*tf[591];
  r+= rf[926]*tf[592];
  r+= rf[984]*tf[593];
  r+= rf[896]*tf[595];
  r+= rf[986]*tf[596];
  r+= rf[1002]*tf[600];
  r+= rf[946]*tf[603];
  r+= rf[947]*tf[605];
  r+= rf[1000]*tf[607];
  r+= rf[920]*tf[610];
  r+= rf[996]*tf[614];
  r+= eps5*rf[823]*tf[615];
  r+= eps5*rf[960]*tf[616];
  r+= eps5*rf[963]*tf[616];
  r+= eps5*rf[962]*tf[617];
  r+= eps5*rf[806]*tf[618];
  r+= eps5*rf[866]*tf[619];
  r+= eps5*rf[808]*tf[620];
  r+= eps5*rf[961]*tf[621];
  r+= eps5*rf[860]*tf[622];
  r+= eps5*rf[861]*tf[623];
  r+= eps5*rf[851]*tf[624];
  r+= eps5*rf[865]*tf[625];
  r+= eps5*rf[807]*tf[626];
  r+= eps5*rf[798]*tf[627];
  r+= eps5*rf[805]*tf[628];
  r+= eps5*rf[781]*tf[629];
  r+= eps5*rf[797]*tf[630];
  r+= eps5*rf[816]*tf[631];
  r+= eps5*rf[800]*tf[632];
  r+= eps5*rf[850]*tf[633];
  r+= eps5*rf[827]*tf[634];
  r+= eps5*rf[852]*tf[635];
  r+= eps5*rf[817]*tf[636];
  r+= eps5*rf[821]*tf[637];
  r+= eps5*rf[871]*tf[638];
  r+= eps5*rf[870]*tf[639];
  r+= eps5*rf[804]*tf[640];
  r+= eps5*rf[825]*tf[641];
  r+= eps5*rf[778]*tf[644];
  r+= eps5*rf[801]*tf[645];
  r+= eps5*rf[779]*tf[646];
  r+= rf[1008]*tf[648];
  r+= rf[1021]*tf[649];
  r+= eps5*rf[782]*tf[650];
  r+= rf[940]*tf[652];
  r+= rf[945]*tf[653];
  r+= rf[944]*tf[657];
  r+= rf[913]*tf[658];
  r+= rf[1018]*tf[660];
  r+= rf[914]*tf[661];
  r+= rf[943]*tf[664];
  r+= rf[990]*tf[666];
  r+= rf[949]*tf[668];
  r+= rf[948]*tf[671];
  r+= rf[874]*tf[673];
  r+= rf[993]*tf[674];
  r+= rf[900]*tf[675];
  r+= rf[1015]*tf[676];
  r+= rf[1013]*tf[677];
  r+= rf[1014]*tf[678];
  r+= rf[910]*tf[680];
  r+= rf[911]*tf[684];
  r+= rf[1012]*tf[686];
  r+= rf[936]*tf[687];
  r+= rf[999]*tf[688];
  r+= rf[1016]*tf[689];
  r+= rf[907]*tf[693];
  r+= rf[951]*tf[694];
  r+= rf[1005]*tf[696];
  r+= rf[1020]*tf[698];
  r+= rf[987]*tf[699];
  r+= rf[1019]*tf[701];
  r+= rf[988]*tf[702];
  r+= rf[1009]*tf[706];
  r+= rf[998]*tf[707];
  r+= rf[916]*tf[709];
  r+= rf[912]*tf[711];
  r+= rf[917]*tf[712];
  r+= rf[1007]*tf[714];
  r+= rf[952]*tf[717];
  r+= rf[902]*tf[720];
  r+= rf[908]*tf[721];
  r+= rf[1022]*tf[722];
  r+= rf[1011]*tf[723];
  r+= rf[1010]*tf[725];
  r+= rf[909]*tf[728];
  r+= rf[989]*tf[733];
  r+= rf[950]*tf[734];
  r+= rf[1006]*tf[735];
  r+= rf[1017]*tf[737];
  r+= rf[809]*tf[738];
  r+= rf[937]*tf[739];
  r+= rf[938]*tf[743];
  r+= rf[903]*tf[745];
  r+= rf[803]*tf[746];
  r+= rf[802]*tf[749];
  r+= rf[901]*tf[751];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmppO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[792];
  r+= rf[796]*tf[20];
  r+= rf[842]*tf[21];
  r+= rf[828]*tf[28];
  r+= rf[790]*tf[31];
  r+= rf[846]*tf[32];
  r+= rf[789]*tf[33];
  r+= rf[869]*tf[41];
  r+= rf[847]*tf[49];
  r+= eps5*rf[786]*tf[52];
  r+= eps5*rf[780]*tf[53];
  r+= rf[868]*tf[55];
  r+= eps5*rf[783]*tf[76];
  r+= eps5*rf[785]*tf[77];
  r+= eps5*rf[889]*tf[82];
  r+= eps5*rf[974]*tf[84];
  r+= eps5*rf[978]*tf[87];
  r+= eps5*rf[886]*tf[92];
  r+= eps5*rf[975]*tf[93];
  r+= eps5*rf[887]*tf[106];
  r+= eps5*rf[888]*tf[118];
  r+= eps5*rf[981]*tf[119];
  r+= rf[969]*tf[121];
  r+= rf[831]*tf[236];
  r+= eps5*rf[784]*tf[255];
  r+= rf[885]*tf[260];
  r+= rf[812]*tf[264];
  r+= eps5*rf[979]*tf[285];
  r+= eps5*rf[980]*tf[290];
  r+= rf[853]*tf[351];
  r+= rf[795]*tf[421];
  r+= rf[829]*tf[422];
  r+= rf[844]*tf[423];
  r+= rf[845]*tf[424];
  r+= rf[832]*tf[427];
  r+= rf[838]*tf[428];
  r+= rf[841]*tf[430];
  r+= rf[833]*tf[431];
  r+= rf[815]*tf[432];
  r+= rf[855]*tf[434];
  r+= rf[788]*tf[435];
  r+= rf[835]*tf[436];
  r+= rf[834]*tf[437];
  r+= rf[856]*tf[438];
  r+= rf[843]*tf[439];
  r+= rf[857]*tf[440];
  r+= rf[879]*tf[441];
  r+= rf[848]*tf[445];
  r+= rf[877]*tf[451];
  r+= rf[810]*tf[453];
  r+= rf[824]*tf[457];
  r+= rf[884]*tf[458];
  r+= rf[867]*tf[590];
  r+= eps5*rf[970]*tf[591];
  r+= rf[791]*tf[594];
  r+= rf[883]*tf[596];
  r+= rf[839]*tf[597];
  r+= rf[814]*tf[598];
  r+= rf[813]*tf[599];
  r+= rf[794]*tf[601];
  r+= rf[876]*tf[602];
  r+= rf[863]*tf[603];
  r+= rf[793]*tf[604];
  r+= rf[862]*tf[605];
  r+= rf[837]*tf[606];
  r+= rf[878]*tf[607];
  r+= rf[881]*tf[608];
  r+= rf[836]*tf[609];
  r+= rf[830]*tf[611];
  r+= rf[882]*tf[612];
  r+= rf[840]*tf[613];
  r+= rf[873]*tf[614];
  r+= eps5*rf[976]*tf[642];
  r+= eps5*rf[977]*tf[643];
  r+= rf[826]*tf[647];
  r+= rf[954]*tf[648];
  r+= rf[967]*tf[649];
  r+= eps5*rf[890]*tf[651];
  r+= rf[854]*tf[652];
  r+= rf[806]*tf[654];
  r+= rf[864]*tf[655];
  r+= rf[861]*tf[656];
  r+= rf[963]*tf[659];
  r+= rf[860]*tf[662];
  r+= rf[823]*tf[663];
  r+= rf[960]*tf[665];
  r+= rf[870]*tf[667];
  r+= rf[807]*tf[669];
  r+= rf[871]*tf[670];
  r+= rf[865]*tf[672];
  r+= rf[958]*tf[677];
  r+= rf[808]*tf[679];
  r+= rf[821]*tf[681];
  r+= rf[797]*tf[682];
  r+= rf[781]*tf[683];
  r+= rf[966]*tf[685];
  r+= rf[820]*tf[690];
  r+= rf[959]*tf[691];
  r+= rf[817]*tf[692];
  r+= rf[825]*tf[695];
  r+= rf[872]*tf[697];
  r+= rf[827]*tf[700];
  r+= rf[849]*tf[703];
  r+= rf[818]*tf[704];
  r+= rf[805]*tf[705];
  r+= rf[804]*tf[708];
  r+= rf[866]*tf[710];
  r+= rf[819]*tf[713];
  r+= rf[953]*tf[715];
  r+= rf[965]*tf[716];
  r+= rf[961]*tf[718];
  r+= rf[798]*tf[719];
  r+= rf[968]*tf[722];
  r+= rf[957]*tf[723];
  r+= rf[955]*tf[724];
  r+= rf[956]*tf[725];
  r+= rf[964]*tf[726];
  r+= rf[858]*tf[727];
  r+= rf[816]*tf[729];
  r+= rf[850]*tf[730];
  r+= rf[811]*tf[731];
  r+= rf[800]*tf[732];
  r+= rf[962]*tf[736];
  r+= rf[851]*tf[740];
  r+= rf[782]*tf[741];
  r+= rf[852]*tf[742];
  r+= rf[799]*tf[744];
  r+= rf[779]*tf[747];
  r+= rf[801]*tf[748];
  r+= rf[778]*tf[750];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[700];
  r+= rf[750]*tf[19];
  r+= rf[701]*tf[20];
  r+= rf[715]*tf[21];
  r+= rf[734]*tf[27];
  r+= rf[705]*tf[28];
  r+= rf[742]*tf[32];
  r+= rf[679]*tf[33];
  r+= rf[712]*tf[40];
  r+= rf[777]*tf[41];
  r+= rf[769]*tf[49];
  r+= eps5*rf[630]*tf[52];
  r+= eps5*rf[535]*tf[54];
  r+= rf[772]*tf[55];
  r+= eps5*rf[663]*tf[76];
  r+= eps5*rf[670]*tf[77];
  r+= eps5*rf[638]*tf[82];
  r+= eps5*rf[539]*tf[91];
  r+= eps5*rf[626]*tf[92];
  r+= rf[757]*tf[234];
  r+= rf[716]*tf[236];
  r+= rf[681]*tf[254];
  r+= eps5*rf[590]*tf[255];
  r+= rf[773]*tf[260];
  r+= eps5*rf[632]*tf[285];
  r+= eps5*rf[618]*tf[290];
  r+= rf[735]*tf[385];
  r+= rf[743]*tf[419];
  r+= rf[736]*tf[420];
  r+= rf[702]*tf[421];
  r+= rf[704]*tf[422];
  r+= rf[720]*tf[423];
  r+= rf[753]*tf[424];
  r+= rf[718]*tf[425];
  r+= rf[717]*tf[426];
  r+= rf[707]*tf[427];
  r+= rf[713]*tf[428];
  r+= rf[776]*tf[429];
  r+= rf[714]*tf[430];
  r+= rf[708]*tf[431];
  r+= rf[752]*tf[432];
  r+= rf[683]*tf[433];
  r+= rf[724]*tf[434];
  r+= rf[644]*tf[435];
  r+= rf[745]*tf[436];
  r+= rf[709]*tf[437];
  r+= rf[725]*tf[438];
  r+= rf[754]*tf[439];
  r+= rf[726]*tf[440];
  r+= rf[766]*tf[441];
  r+= rf[680]*tf[442];
  r+= rf[687]*tf[443];
  r+= rf[703]*tf[445];
  r+= rf[768]*tf[446];
  r+= rf[771]*tf[447];
  r+= rf[767]*tf[448];
  r+= rf[711]*tf[449];
  r+= rf[765]*tf[450];
  r+= rf[770]*tf[451];
  r+= rf[682]*tf[452];
  r+= rf[744]*tf[453];
  r+= rf[764]*tf[454];
  r+= rf[706]*tf[455];
  r+= rf[775]*tf[456];
  r+= rf[693]*tf[457];
  r+= rf[774]*tf[458];
  r+= rf[710]*tf[459];
  r+= rf[747]*tf[461];
  r+= eps5*rf[538]*tf[462];
  r+= eps5*rf[584]*tf[463];
  r+= eps5*rf[578]*tf[464];
  r+= eps5*rf[544]*tf[465];
  r+= eps5*rf[588]*tf[466];
  r+= eps5*rf[620]*tf[467];
  r+= eps5*rf[623]*tf[468];
  r+= eps5*rf[552]*tf[469];
  r+= eps5*rf[557]*tf[469];
  r+= eps5*rf[554]*tf[470];
  r+= eps5*rf[587]*tf[471];
  r+= eps5*rf[553]*tf[472];
  r+= eps5*rf[621]*tf[473];
  r+= eps5*rf[540]*tf[474];
  r+= eps5*rf[558]*tf[475];
  r+= eps5*rf[556]*tf[476];
  r+= eps5*rf[536]*tf[477];
  r+= eps5*rf[637]*tf[478];
  r+= eps5*rf[550]*tf[479];
  r+= eps5*rf[624]*tf[480];
  r+= eps5*rf[579]*tf[481];
  r+= eps5*rf[555]*tf[482];
  r+= eps5*rf[634]*tf[483];
  r+= eps5*rf[625]*tf[484];
  r+= eps5*rf[541]*tf[485];
  r+= eps5*rf[580]*tf[486];
  r+= eps5*rf[542]*tf[487];
  r+= eps5*rf[537]*tf[489];
  r+= eps5*rf[633]*tf[490];
  r+= eps5*rf[534]*tf[491];
  r+= eps5*rf[20]*tf[494];
  r+= rf[759]*tf[495];
  r+= rf[755]*tf[496];
  r+= rf[751]*tf[497];
  r+= rf[749]*tf[498];
  r+= rf[760]*tf[500];
  r+= rf[756]*tf[501];
  r+= rf[746]*tf[505];
  r+= rf[748]*tf[506];
  r+= eps5*rf[543]*tf[507];
  r+= rf[685]*tf[509];
  r+= rf[655]*tf[512];
  r+= rf[729]*tf[514];
  r+= rf[563]*tf[515];
  r+= rf[648]*tf[519];
  r+= rf[689]*tf[521];
  r+= rf[564]*tf[525];
  r+= rf[651]*tf[527];
  r+= rf[737]*tf[529];
  r+= rf[738]*tf[530];
  r+= rf[688]*tf[531];
  r+= rf[740]*tf[532];
  r+= rf[656]*tf[533];
  r+= rf[650]*tf[535];
  r+= rf[758]*tf[537];
  r+= rf[653]*tf[540];
  r+= rf[691]*tf[541];
  r+= rf[727]*tf[543];
  r+= rf[583]*tf[544];
  r+= rf[565]*tf[546];
  r+= rf[652]*tf[548];
  r+= rf[722]*tf[549];
  r+= rf[692]*tf[550];
  r+= rf[647]*tf[553];
  r+= rf[739]*tf[555];
  r+= rf[741]*tf[556];
  r+= rf[723]*tf[558];
  r+= rf[733]*tf[559];
  r+= rf[730]*tf[560];
  r+= rf[721]*tf[561];
  r+= rf[575]*tf[562];
  r+= rf[686]*tf[567];
  r+= rf[654]*tf[568];
  r+= rf[732]*tf[569];
  r+= rf[719]*tf[570];
  r+= rf[649]*tf[572];
  r+= rf[577]*tf[574];
  r+= rf[576]*tf[577];
  r+= rf[728]*tf[578];
  r+= rf[574]*tf[579];
  r+= rf[684]*tf[581];
  r+= rf[690]*tf[583];
  r+= rf[731]*tf[586];
  r+= rf[562]*tf[587];
  r+= rf[561]*tf[588];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[573];
  r+= rf[631]*tf[19];
  r+= rf[571]*tf[20];
  r+= rf[592]*tf[21];
  r+= rf[629]*tf[27];
  r+= rf[605]*tf[28];
  r+= rf[645]*tf[32];
  r+= rf[568]*tf[33];
  r+= rf[596]*tf[40];
  r+= rf[669]*tf[41];
  r+= rf[661]*tf[49];
  r+= eps5*rf[548]*tf[52];
  r+= eps5*rf[533]*tf[54];
  r+= rf[675]*tf[55];
  r+= eps5*rf[545]*tf[76];
  r+= eps5*rf[609]*tf[82];
  r+= eps5*rf[696]*tf[84];
  r+= eps5*rf[698]*tf[87];
  r+= eps5*rf[610]*tf[92];
  r+= eps5*rf[695]*tf[93];
  r+= eps5*rf[677]*tf[106];
  r+= eps5*rf[678]*tf[118];
  r+= eps5*rf[761]*tf[119];
  r+= rf[658]*tf[234];
  r+= rf[614]*tf[236];
  r+= rf[570]*tf[254];
  r+= eps5*rf[546]*tf[255];
  r+= rf[671]*tf[260];
  r+= eps5*rf[699]*tf[285];
  r+= eps5*rf[762]*tf[290];
  r+= rf[660]*tf[306];
  r+= rf[628]*tf[385];
  r+= rf[619]*tf[419];
  r+= rf[627]*tf[420];
  r+= rf[572]*tf[421];
  r+= rf[604]*tf[422];
  r+= rf[617]*tf[423];
  r+= rf[616]*tf[424];
  r+= rf[613]*tf[425];
  r+= rf[612]*tf[426];
  r+= rf[601]*tf[427];
  r+= rf[597]*tf[428];
  r+= rf[674]*tf[429];
  r+= rf[593]*tf[430];
  r+= rf[602]*tf[431];
  r+= rf[589]*tf[432];
  r+= rf[567]*tf[433];
  r+= rf[640]*tf[434];
  r+= rf[549]*tf[435];
  r+= rf[600]*tf[436];
  r+= rf[599]*tf[437];
  r+= rf[641]*tf[438];
  r+= rf[594]*tf[439];
  r+= rf[639]*tf[440];
  r+= rf[666]*tf[441];
  r+= rf[569]*tf[442];
  r+= rf[582]*tf[444];
  r+= rf[606]*tf[445];
  r+= rf[668]*tf[446];
  r+= rf[676]*tf[447];
  r+= rf[667]*tf[448];
  r+= rf[595]*tf[449];
  r+= rf[665]*tf[450];
  r+= rf[662]*tf[451];
  r+= rf[566]*tf[452];
  r+= rf[560]*tf[453];
  r+= rf[664]*tf[454];
  r+= rf[603]*tf[455];
  r+= rf[673]*tf[456];
  r+= rf[591]*tf[457];
  r+= rf[672]*tf[458];
  r+= rf[598]*tf[459];
  r+= eps5*rf[547]*tf[460];
  r+= eps5*rf[763]*tf[488];
  r+= eps5*rf[694]*tf[492];
  r+= eps5*rf[697]*tf[493];
  r+= rf[607]*tf[495];
  r+= rf[646]*tf[496];
  r+= rf[581]*tf[499];
  r+= rf[615]*tf[500];
  r+= rf[643]*tf[501];
  r+= rf[657]*tf[502];
  r+= rf[585]*tf[503];
  r+= rf[586]*tf[504];
  r+= eps5*rf[608]*tf[508];
  r+= rf[579]*tf[510];
  r+= rf[555]*tf[511];
  r+= rf[536]*tf[513];
  r+= rf[556]*tf[516];
  r+= rf[540]*tf[517];
  r+= rf[621]*tf[518];
  r+= rf[551]*tf[520];
  r+= rf[553]*tf[522];
  r+= rf[544]*tf[523];
  r+= rf[538]*tf[524];
  r+= rf[558]*tf[526];
  r+= rf[624]*tf[528];
  r+= rf[620]*tf[532];
  r+= rf[554]*tf[534];
  r+= rf[559]*tf[536];
  r+= rf[659]*tf[537];
  r+= rf[623]*tf[538];
  r+= rf[557]*tf[539];
  r+= rf[588]*tf[542];
  r+= rf[637]*tf[545];
  r+= rf[539]*tf[547];
  r+= rf[587]*tf[550];
  r+= rf[626]*tf[551];
  r+= rf[638]*tf[552];
  r+= rf[552]*tf[553];
  r+= rf[622]*tf[554];
  r+= rf[618]*tf[556];
  r+= rf[584]*tf[557];
  r+= rf[642]*tf[558];
  r+= rf[634]*tf[559];
  r+= rf[636]*tf[560];
  r+= rf[632]*tf[563];
  r+= rf[611]*tf[564];
  r+= rf[635]*tf[565];
  r+= rf[578]*tf[566];
  r+= rf[550]*tf[571];
  r+= rf[543]*tf[573];
  r+= rf[537]*tf[575];
  r+= rf[633]*tf[576];
  r+= rf[542]*tf[580];
  r+= rf[580]*tf[581];
  r+= rf[20]*tf[582];
  r+= rf[625]*tf[584];
  r+= rf[541]*tf[585];
  r+= rf[534]*tf[589];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmpE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1158]*tf[20];
  r+= rf[1127]*tf[21];
  r+= rf[1111]*tf[22];
  r+= rf[1155]*tf[23];
  r+= rf[1212]*tf[24];
  r+= rf[1214]*tf[25];
  r+= rf[1156]*tf[26];
  r+= rf[1213]*tf[28];
  r+= rf[1211]*tf[29];
  r+= rf[1216]*tf[30];
  r+= rf[1225]*tf[32];
  r+= rf[1152]*tf[40];
  r+= rf[1255]*tf[41];
  r+= rf[1260]*tf[49];
  r+= eps5*rf[1173]*tf[52];
  r+= eps5*rf[1088]*tf[53];
  r+= eps5*rf[1097]*tf[54];
  r+= rf[1236]*tf[55];
  r+= rf[1215]*tf[56];
  r+= rf[1134]*tf[59];
  r+= eps5*rf[1166]*tf[82];
  r+= eps5*rf[1041]*tf[91];
  r+= eps5*rf[1114]*tf[93];
  r+= eps5*rf[1065]*tf[113];
  r+= rf[1201]*tf[190];
  r+= rf[1153]*tf[236];
  r+= rf[1224]*tf[237];
  r+= rf[1262]*tf[238];
  r+= rf[1205]*tf[239];
  r+= rf[1187]*tf[240];
  r+= rf[1186]*tf[243];
  r+= rf[1055]*tf[244];
  r+= rf[1263]*tf[247];
  r+= rf[1256]*tf[248];
  r+= rf[1228]*tf[249];
  r+= rf[1261]*tf[250];
  r+= rf[1252]*tf[251];
  r+= rf[1223]*tf[252];
  r+= rf[1162]*tf[253];
  r+= rf[1135]*tf[254];
  r+= eps5*rf[1218]*tf[255];
  r+= rf[1150]*tf[256];
  r+= rf[1226]*tf[257];
  r+= rf[1204]*tf[258];
  r+= rf[1250]*tf[259];
  r+= rf[1209]*tf[260];
  r+= rf[1161]*tf[262];
  r+= rf[1237]*tf[263];
  r+= rf[1195]*tf[264];
  r+= rf[1249]*tf[265];
  r+= rf[1227]*tf[267];
  r+= rf[1210]*tf[268];
  r+= rf[1197]*tf[269];
  r+= rf[1154]*tf[271];
  r+= rf[1077]*tf[272];
  r+= rf[1242]*tf[419];
  r+= rf[1251]*tf[429];
  r+= eps5*rf[1067]*tf[466];
  r+= eps5*rf[1170]*tf[468];
  r+= rf[1157]*tf[593];
  r+= rf[1140]*tf[757];
  r+= rf[1151]*tf[764];
  r+= eps5*rf[1113]*tf[766];
  r+= eps5*rf[1066]*tf[767];
  r+= eps5*rf[1040]*tf[768];
  r+= eps5*rf[1063]*tf[769];
  r+= eps5*rf[1050]*tf[770];
  r+= eps5*rf[1107]*tf[771];
  r+= eps5*rf[1058]*tf[772];
  r+= eps5*rf[1060]*tf[773];
  r+= eps5*rf[1169]*tf[774];
  r+= eps5*rf[1115]*tf[775];
  r+= eps5*rf[1171]*tf[776];
  r+= eps5*rf[1026]*tf[777];
  r+= eps5*rf[1028]*tf[778];
  r+= eps5*rf[1046]*tf[779];
  r+= eps5*rf[1078]*tf[780];
  r+= eps5*rf[1110]*tf[781];
  r+= eps5*rf[1101]*tf[782];
  r+= eps5*rf[1048]*tf[783];
  r+= eps5*rf[1080]*tf[784];
  r+= eps5*rf[1102]*tf[785];
  r+= eps5*rf[1109]*tf[785];
  r+= eps5*rf[1168]*tf[786];
  r+= eps5*rf[1172]*tf[787];
  r+= eps5*rf[1049]*tf[788];
  r+= eps5*rf[1081]*tf[789];
  r+= eps5*rf[1024]*tf[790];
  r+= eps5*rf[1116]*tf[791];
  r+= eps5*rf[1031]*tf[792];
  r+= eps5*rf[1177]*tf[793];
  r+= eps5*rf[1062]*tf[794];
  r+= eps5*rf[1061]*tf[795];
  r+= rf[1235]*tf[796];
  r+= rf[1136]*tf[797];
  r+= rf[1131]*tf[800];
  r+= rf[1190]*tf[801];
  r+= rf[1145]*tf[804];
  r+= rf[1129]*tf[805];
  r+= rf[1193]*tf[806];
  r+= rf[1133]*tf[807];
  r+= rf[1142]*tf[808];
  r+= rf[1253]*tf[809];
  r+= rf[1075]*tf[810];
  r+= rf[1203]*tf[811];
  r+= rf[1056]*tf[812];
  r+= rf[1053]*tf[813];
  r+= rf[1200]*tf[819];
  r+= rf[1085]*tf[820];
  r+= rf[1244]*tf[822];
  r+= rf[1247]*tf[823];
  r+= rf[1239]*tf[824];
  r+= rf[1243]*tf[827];
  r+= rf[1139]*tf[828];
  r+= rf[1137]*tf[830];
  r+= rf[1246]*tf[831];
  r+= rf[1207]*tf[832];
  r+= rf[1241]*tf[837];
  r+= rf[1126]*tf[843];
  r+= rf[1198]*tf[844];
  r+= rf[1206]*tf[846];
  r+= rf[1208]*tf[847];
  r+= rf[1191]*tf[848];
  r+= rf[1238]*tf[850];
  r+= rf[1254]*tf[854];
  r+= rf[1128]*tf[855];
  r+= rf[1257]*tf[860];
  r+= rf[1240]*tf[861];
  r+= rf[1196]*tf[863];
  r+= rf[1245]*tf[865];
  r+= rf[1189]*tf[869];
  r+= rf[1054]*tf[870];
  r+= rf[1199]*tf[872];
  r+= rf[1202]*tf[873];
  r+= rf[1192]*tf[876];
  r+= rf[1248]*tf[878];
  r+= rf[1194]*tf[879];
  r+= rf[1143]*tf[881];
  r+= rf[1130]*tf[882];
  r+= rf[1141]*tf[884];
  r+= rf[1132]*tf[886];
  r+= rf[1138]*tf[887];
  r+= eps5*rf[1032]*tf[888];
  r+= eps5*rf[1082]*tf[890];
  r+= eps5*rf[1033]*tf[893];
  r+= rf[1144]*tf[894];
  r+= rf[1147]*tf[896];
  r+= rf[1074]*tf[898];
  r+= rf[1073]*tf[900];
  r+= rf[1057]*tf[901];
  r+= rf[1148]*tf[902];
  r+= rf[1076]*tf[904];
  r+= rf[1146]*tf[907];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmpO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1042]*tf[23];
  r+= rf[1092]*tf[24];
  r+= rf[1090]*tf[25];
  r+= rf[1052]*tf[26];
  r+= rf[1091]*tf[29];
  r+= rf[1095]*tf[30];
  r+= rf[1098]*tf[32];
  r+= rf[1183]*tf[41];
  r+= rf[1222]*tf[49];
  r+= eps5*rf[1039]*tf[52];
  r+= eps5*rf[1035]*tf[53];
  r+= eps5*rf[1025]*tf[54];
  r+= rf[1159]*tf[55];
  r+= rf[1072]*tf[59];
  r+= eps5*rf[1234]*tf[82];
  r+= eps5*rf[1233]*tf[84];
  r+= eps5*rf[1230]*tf[92];
  r+= eps5*rf[1232]*tf[93];
  r+= eps5*rf[1087]*tf[105];
  r+= eps5*rf[1188]*tf[106];
  r+= eps5*rf[1259]*tf[117];
  r+= eps5*rf[1229]*tf[118];
  r+= rf[1181]*tf[190];
  r+= rf[1071]*tf[236];
  r+= rf[1121]*tf[239];
  r+= rf[1176]*tf[240];
  r+= rf[1180]*tf[241];
  r+= rf[1064]*tf[242];
  r+= rf[1089]*tf[243];
  r+= rf[1029]*tf[244];
  r+= rf[1059]*tf[245];
  r+= rf[1219]*tf[247];
  r+= rf[1100]*tf[249];
  r+= rf[1220]*tf[250];
  r+= rf[1047]*tf[253];
  r+= rf[1070]*tf[254];
  r+= eps5*rf[1096]*tf[255];
  r+= rf[1108]*tf[257];
  r+= rf[1119]*tf[258];
  r+= rf[1175]*tf[259];
  r+= rf[1122]*tf[260];
  r+= rf[1079]*tf[261];
  r+= rf[1043]*tf[262];
  r+= rf[1160]*tf[263];
  r+= rf[1174]*tf[265];
  r+= rf[1044]*tf[266];
  r+= rf[1120]*tf[267];
  r+= rf[1106]*tf[268];
  r+= rf[1112]*tf[269];
  r+= rf[1068]*tf[271];
  r+= rf[1030]*tf[272];
  r+= eps5*rf[1231]*tf[285];
  r+= eps5*rf[1258]*tf[290];
  r+= rf[1167]*tf[419];
  r+= rf[1045]*tf[752];
  r+= rf[1038]*tf[753];
  r+= rf[1093]*tf[754];
  r+= rf[1221]*tf[755];
  r+= rf[1036]*tf[756];
  r+= rf[1217]*tf[758];
  r+= rf[1184]*tf[759];
  r+= rf[1178]*tf[760];
  r+= rf[1099]*tf[761];
  r+= rf[1094]*tf[762];
  r+= rf[1069]*tf[763];
  r+= rf[1105]*tf[765];
  r+= eps5*rf[1037]*tf[766];
  r+= rf[1149]*tf[796];
  r+= rf[1062]*tf[798];
  r+= rf[1058]*tf[799];
  r+= rf[1109]*tf[802];
  r+= rf[1102]*tf[803];
  r+= rf[1028]*tf[814];
  r+= rf[1103]*tf[815];
  r+= rf[1177]*tf[816];
  r+= rf[1031]*tf[817];
  r+= rf[1116]*tf[818];
  r+= rf[1172]*tf[821];
  r+= rf[1049]*tf[825];
  r+= rf[1114]*tf[826];
  r+= rf[1164]*tf[829];
  r+= rf[1124]*tf[832];
  r+= rf[1168]*tf[833];
  r+= rf[1040]*tf[834];
  r+= rf[1123]*tf[835];
  r+= rf[1125]*tf[836];
  r+= rf[1118]*tf[838];
  r+= rf[1034]*tf[839];
  r+= rf[1065]*tf[840];
  r+= rf[1041]*tf[841];
  r+= rf[1117]*tf[842];
  r+= rf[1179]*tf[845];
  r+= rf[1066]*tf[849];
  r+= rf[1170]*tf[851];
  r+= rf[1046]*tf[852];
  r+= rf[1163]*tf[853];
  r+= rf[1182]*tf[854];
  r+= rf[1166]*tf[856];
  r+= rf[1110]*tf[857];
  r+= rf[1171]*tf[858];
  r+= rf[1169]*tf[859];
  r+= rf[1185]*tf[860];
  r+= rf[1165]*tf[861];
  r+= rf[1115]*tf[862];
  r+= rf[1048]*tf[864];
  r+= rf[1104]*tf[866];
  r+= rf[1026]*tf[867];
  r+= rf[1101]*tf[868];
  r+= rf[1051]*tf[871];
  r+= rf[1067]*tf[874];
  r+= rf[1024]*tf[875];
  r+= rf[1063]*tf[877];
  r+= rf[1107]*tf[880];
  r+= rf[1027]*tf[883];
  r+= rf[1050]*tf[885];
  r+= eps5*rf[1083]*tf[889];
  r+= eps5*rf[1086]*tf[891];
  r+= eps5*rf[1084]*tf[892];
  r+= rf[1078]*tf[895];
  r+= rf[1081]*tf[897];
  r+= rf[1061]*tf[899];
  r+= rf[1080]*tf[903];
  r+= rf[1033]*tf[905];
  r+= rf[1060]*tf[906];
  r+= rf[1082]*tf[908];
  r+= rf[1032]*tf[909];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LppmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1449]*tf[20];
  r+= rf[1470]*tf[21];
  r+= rf[1342]*tf[22];
  r+= rf[1443]*tf[23];
  r+= rf[1468]*tf[24];
  r+= rf[1467]*tf[25];
  r+= rf[1454]*tf[26];
  r+= rf[1502]*tf[27];
  r+= rf[1377]*tf[28];
  r+= rf[1469]*tf[29];
  r+= rf[1451]*tf[30];
  r+= rf[1401]*tf[31];
  r+= rf[1457]*tf[32];
  r+= rf[1446]*tf[33];
  r+= rf[1404]*tf[36];
  r+= rf[1466]*tf[38];
  r+= rf[1418]*tf[39];
  r+= rf[1391]*tf[40];
  r+= rf[1500]*tf[41];
  r+= rf[1481]*tf[42];
  r+= rf[1448]*tf[43];
  r+= rf[1488]*tf[45];
  r+= rf[1366]*tf[46];
  r+= rf[1458]*tf[47];
  r+= rf[1397]*tf[48];
  r+= rf[1480]*tf[49];
  r+= rf[1447]*tf[50];
  r+= eps5*rf[1476]*tf[52];
  r+= eps5*rf[1353]*tf[53];
  r+= eps5*rf[1316]*tf[54];
  r+= rf[1499]*tf[55];
  r+= rf[1471]*tf[56];
  r+= rf[1390]*tf[59];
  r+= eps5*rf[1308]*tf[77];
  r+= eps5*rf[1315]*tf[82];
  r+= eps5*rf[1323]*tf[89];
  r+= eps5*rf[1424]*tf[92];
  r+= rf[1420]*tf[121];
  r+= rf[1409]*tf[167];
  r+= rf[1411]*tf[171];
  r+= rf[1463]*tf[190];
  r+= eps5*rf[1431]*tf[255];
  r+= rf[1453]*tf[260];
  r+= rf[1421]*tf[268];
  r+= rf[1478]*tf[420];
  r+= rf[1498]*tf[447];
  r+= eps5*rf[1301]*tf[462];
  r+= eps5*rf[1440]*tf[467];
  r+= rf[1496]*tf[676];
  r+= rf[1482]*tf[680];
  r+= rf[1503]*tf[919];
  r+= rf[1501]*tf[920];
  r+= rf[1484]*tf[921];
  r+= rf[1483]*tf[925];
  r+= rf[1450]*tf[927];
  r+= rf[1464]*tf[929];
  r+= rf[1419]*tf[930];
  r+= rf[1405]*tf[931];
  r+= rf[1487]*tf[932];
  r+= rf[1452]*tf[934];
  r+= rf[1444]*tf[936];
  r+= rf[1486]*tf[937];
  r+= rf[1445]*tf[939];
  r+= rf[1465]*tf[940];
  r+= rf[1370]*tf[941];
  r+= rf[1406]*tf[942];
  r+= rf[1365]*tf[943];
  r+= eps5*rf[1290]*tf[944];
  r+= eps5*rf[1269]*tf[945];
  r+= eps5*rf[1276]*tf[945];
  r+= eps5*rf[1278]*tf[945];
  r+= eps5*rf[1313]*tf[946];
  r+= eps5*rf[1310]*tf[947];
  r+= eps5*rf[1435]*tf[948];
  r+= eps5*rf[1321]*tf[949];
  r+= eps5*rf[1335]*tf[950];
  r+= eps5*rf[1285]*tf[951];
  r+= eps5*rf[1334]*tf[952];
  r+= eps5*rf[1430]*tf[953];
  r+= eps5*rf[1427]*tf[954];
  r+= eps5*rf[1297]*tf[955];
  r+= eps5*rf[1322]*tf[956];
  r+= eps5*rf[1311]*tf[957];
  r+= eps5*rf[1320]*tf[958];
  r+= eps5*rf[1267]*tf[959];
  r+= eps5*rf[1332]*tf[960];
  r+= eps5*rf[1268]*tf[961];
  r+= eps5*rf[1314]*tf[962];
  r+= eps5*rf[1277]*tf[963];
  r+= eps5*rf[1429]*tf[964];
  r+= eps5*rf[1289]*tf[965];
  r+= eps5*rf[1307]*tf[966];
  r+= eps5*rf[1428]*tf[967];
  r+= eps5*rf[1298]*tf[968];
  r+= eps5*rf[1280]*tf[969];
  r+= eps5*rf[1286]*tf[970];
  r+= eps5*rf[1302]*tf[971];
  r+= rf[1356]*tf[973];
  r+= rf[1485]*tf[974];
  r+= rf[1388]*tf[978];
  r+= rf[1359]*tf[979];
  r+= rf[1413]*tf[980];
  r+= rf[1386]*tf[982];
  r+= rf[1331]*tf[983];
  r+= rf[1363]*tf[987];
  r+= rf[1367]*tf[989];
  r+= rf[1399]*tf[991];
  r+= rf[1407]*tf[993];
  r+= rf[1402]*tf[997];
  r+= rf[1393]*tf[999];
  r+= rf[1364]*tf[1002];
  r+= rf[1489]*tf[1003];
  r+= rf[1494]*tf[1004];
  r+= rf[1389]*tf[1005];
  r+= rf[1495]*tf[1006];
  r+= rf[1493]*tf[1009];
  r+= rf[1492]*tf[1011];
  r+= rf[1479]*tf[1012];
  r+= rf[1387]*tf[1013];
  r+= rf[1414]*tf[1014];
  r+= rf[1417]*tf[1015];
  r+= rf[1344]*tf[1016];
  r+= rf[1392]*tf[1017];
  r+= rf[1371]*tf[1019];
  r+= rf[1410]*tf[1020];
  r+= rf[1497]*tf[1022];
  r+= rf[1395]*tf[1026];
  r+= rf[1369]*tf[1027];
  r+= rf[1329]*tf[1028];
  r+= rf[1491]*tf[1029];
  r+= rf[1368]*tf[1030];
  r+= rf[1400]*tf[1031];
  r+= rf[1330]*tf[1033];
  r+= rf[1376]*tf[1034];
  r+= rf[1396]*tf[1035];
  r+= rf[1490]*tf[1036];
  r+= rf[1372]*tf[1040];
  r+= rf[1357]*tf[1043];
  r+= rf[1375]*tf[1047];
  r+= rf[1398]*tf[1048];
  r+= rf[1358]*tf[1051];
  r+= eps5*rf[1336]*tf[1054];
  r+= eps5*rf[1281]*tf[1055];
  r+= eps5*rf[1282]*tf[1056];
  r+= rf[1408]*tf[1058];
  r+= rf[1415]*tf[1059];
  r+= rf[1403]*tf[1062];
  r+= rf[1412]*tf[1063];
  r+= rf[1394]*tf[1064];
  r+= rf[1416]*tf[1066];
  r+= rf[1374]*tf[1068];
  r+= rf[1373]*tf[1072];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LppmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1295]*tf[20];
  r+= rf[1352]*tf[21];
  r+= rf[1272]*tf[28];
  r+= rf[1354]*tf[32];
  r+= rf[1326]*tf[33];
  r+= rf[1348]*tf[38];
  r+= rf[1296]*tf[39];
  r+= rf[1325]*tf[40];
  r+= rf[1472]*tf[41];
  r+= rf[1317]*tf[43];
  r+= rf[1437]*tf[44];
  r+= rf[1434]*tf[45];
  r+= rf[1284]*tf[46];
  r+= rf[1355]*tf[47];
  r+= rf[1319]*tf[48];
  r+= rf[1441]*tf[49];
  r+= rf[1306]*tf[50];
  r+= eps5*rf[1362]*tf[52];
  r+= eps5*rf[1273]*tf[53];
  r+= eps5*rf[1264]*tf[54];
  r+= rf[1455]*tf[55];
  r+= rf[1328]*tf[59];
  r+= eps5*rf[1462]*tf[82];
  r+= eps5*rf[1385]*tf[84];
  r+= eps5*rf[1384]*tf[87];
  r+= eps5*rf[1460]*tf[92];
  r+= eps5*rf[1381]*tf[93];
  r+= eps5*rf[1345]*tf[105];
  r+= eps5*rf[1459]*tf[106];
  r+= eps5*rf[1380]*tf[117];
  r+= eps5*rf[1461]*tf[118];
  r+= eps5*rf[1383]*tf[119];
  r+= rf[1304]*tf[121];
  r+= rf[1340]*tf[167];
  r+= rf[1339]*tf[171];
  r+= rf[1341]*tf[177];
  r+= rf[1378]*tf[190];
  r+= eps5*rf[1271]*tf[255];
  r+= rf[1327]*tf[260];
  r+= rf[1288]*tf[268];
  r+= rf[1442]*tf[420];
  r+= rf[1456]*tf[447];
  r+= rf[1287]*tf[454];
  r+= eps5*rf[1265]*tf[460];
  r+= rf[1422]*tf[676];
  r+= rf[1438]*tf[680];
  r+= rf[1305]*tf[765];
  r+= rf[1292]*tf[910];
  r+= rf[1350]*tf[911];
  r+= rf[1349]*tf[912];
  r+= rf[1300]*tf[913];
  r+= rf[1361]*tf[914];
  r+= rf[1351]*tf[915];
  r+= rf[1474]*tf[916];
  r+= rf[1266]*tf[917];
  r+= rf[1475]*tf[918];
  r+= rf[1477]*tf[919];
  r+= rf[1473]*tf[920];
  r+= rf[1436]*tf[922];
  r+= rf[1309]*tf[923];
  r+= rf[1439]*tf[924];
  r+= rf[1360]*tf[926];
  r+= rf[1324]*tf[928];
  r+= rf[1347]*tf[929];
  r+= rf[1293]*tf[930];
  r+= rf[1312]*tf[931];
  r+= rf[1433]*tf[933];
  r+= rf[1333]*tf[935];
  r+= rf[1432]*tf[937];
  r+= rf[1294]*tf[938];
  r+= rf[1346]*tf[940];
  r+= rf[1303]*tf[942];
  r+= rf[1291]*tf[943];
  r+= eps5*rf[1379]*tf[972];
  r+= rf[1314]*tf[975];
  r+= rf[1297]*tf[976];
  r+= rf[1299]*tf[977];
  r+= rf[1322]*tf[981];
  r+= rf[1277]*tf[984];
  r+= rf[1430]*tf[985];
  r+= rf[1435]*tf[986];
  r+= rf[1285]*tf[988];
  r+= rf[1274]*tf[989];
  r+= rf[1320]*tf[990];
  r+= rf[1321]*tf[992];
  r+= rf[1268]*tf[994];
  r+= rf[1323]*tf[995];
  r+= rf[1302]*tf[996];
  r+= rf[1427]*tf[998];
  r+= rf[1311]*tf[1000];
  r+= rf[1278]*tf[1001];
  r+= rf[1423]*tf[1006];
  r+= rf[1269]*tf[1007];
  r+= rf[1270]*tf[1008];
  r+= rf[1426]*tf[1009];
  r+= rf[1425]*tf[1010];
  r+= rf[1440]*tf[1012];
  r+= rf[1338]*tf[1018];
  r+= rf[1337]*tf[1021];
  r+= rf[1424]*tf[1023];
  r+= rf[1276]*tf[1024];
  r+= rf[1301]*tf[1025];
  r+= rf[1267]*tf[1028];
  r+= rf[1283]*tf[1030];
  r+= rf[1290]*tf[1032];
  r+= rf[1279]*tf[1034];
  r+= rf[1275]*tf[1037];
  r+= rf[1429]*tf[1038];
  r+= rf[1298]*tf[1039];
  r+= rf[1289]*tf[1041];
  r+= rf[1315]*tf[1042];
  r+= rf[1428]*tf[1044];
  r+= rf[1280]*tf[1045];
  r+= rf[1318]*tf[1046];
  r+= rf[1286]*tf[1049];
  r+= rf[1313]*tf[1050];
  r+= eps5*rf[1343]*tf[1052];
  r+= eps5*rf[1382]*tf[1053];
  r+= rf[1332]*tf[1057];
  r+= rf[1335]*tf[1060];
  r+= rf[1310]*tf[1061];
  r+= rf[1334]*tf[1065];
  r+= rf[1336]*tf[1067];
  r+= rf[1307]*tf[1069];
  r+= rf[1281]*tf[1070];
  r+= rf[1282]*tf[1071];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpppE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1538];
  r+= eps5*rf[1574]*tf[52];
  r+= eps5*rf[1555]*tf[53];
  r+= eps5*rf[1517]*tf[54];
  r+= eps5*rf[1544]*tf[75];
  r+= eps5*rf[1548]*tf[77];
  r+= eps5*rf[1566]*tf[255];
  r+= rf[1537]*tf[1073];
  r+= rf[1557]*tf[1074];
  r+= rf[1534]*tf[1075];
  r+= rf[1581]*tf[1076];
  r+= rf[1580]*tf[1077];
  r+= rf[1533]*tf[1078];
  r+= rf[1513]*tf[1081];
  r+= rf[1578]*tf[1082];
  r+= rf[1571]*tf[1083];
  r+= rf[1562]*tf[1084];
  r+= rf[1564]*tf[1085];
  r+= rf[1536]*tf[1086];
  r+= rf[1527]*tf[1087];
  r+= rf[1539]*tf[1088];
  r+= rf[1546]*tf[1089];
  r+= rf[1529]*tf[1090];
  r+= rf[1541]*tf[1091];
  r+= rf[1535]*tf[1092];
  r+= rf[1530]*tf[1093];
  r+= rf[1547]*tf[1094];
  r+= rf[1528]*tf[1095];
  r+= rf[1577]*tf[1096];
  r+= rf[1572]*tf[1097];
  r+= rf[1576]*tf[1098];
  r+= rf[1559]*tf[1099];
  r+= rf[1579]*tf[1100];
  r+= rf[1558]*tf[1101];
  r+= rf[1505]*tf[1102];
  r+= rf[1560]*tf[1103];
  r+= rf[1565]*tf[1104];
  r+= rf[1561]*tf[1106];
  r+= rf[1563]*tf[1107];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpppO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1516];
  r+= eps5*rf[1556]*tf[52];
  r+= eps5*rf[1512]*tf[53];
  r+= eps5*rf[1506]*tf[54];
  r+= eps5*rf[1523]*tf[76];
  r+= eps5*rf[1552]*tf[255];
  r+= eps5*rf[1526]*tf[460];
  r+= rf[1518]*tf[1073];
  r+= rf[1531]*tf[1074];
  r+= rf[1514]*tf[1075];
  r+= rf[1573]*tf[1076];
  r+= rf[1575]*tf[1077];
  r+= rf[1515]*tf[1079];
  r+= rf[1549]*tf[1080];
  r+= rf[1507]*tf[1081];
  r+= rf[1554]*tf[1083];
  r+= rf[1543]*tf[1084];
  r+= rf[1551]*tf[1085];
  r+= rf[1519]*tf[1086];
  r+= rf[1510]*tf[1087];
  r+= rf[1521]*tf[1088];
  r+= rf[1525]*tf[1089];
  r+= rf[1508]*tf[1090];
  r+= rf[1522]*tf[1091];
  r+= rf[1520]*tf[1092];
  r+= rf[1509]*tf[1093];
  r+= rf[1524]*tf[1094];
  r+= rf[1511]*tf[1095];
  r+= rf[1569]*tf[1096];
  r+= rf[1553]*tf[1097];
  r+= rf[1570]*tf[1098];
  r+= rf[1540]*tf[1099];
  r+= rf[1568]*tf[1100];
  r+= rf[1532]*tf[1101];
  r+= rf[1504]*tf[1102];
  r+= rf[1545]*tf[1103];
  r+= rf[1567]*tf[1105];
  r+= rf[1542]*tf[1106];
  r+= rf[1550]*tf[1107];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2426];
  r+= rf[2435]*tf[1096];
  r+= rf[2425]*tf[1098];
  r+= rf[2432]*tf[1099];
  r+= rf[2396]*tf[1134];
  r+= rf[2410]*tf[1146];
  r+= rf[2418]*tf[1147];
  r+= rf[2429]*tf[1148];
  r+= rf[2416]*tf[1149];
  r+= rf[2428]*tf[1150];
  r+= eps5*rf[2422]*tf[1159];
  r+= rf[2420]*tf[1164];
  r+= rf[2433]*tf[1208];
  r+= rf[2427]*tf[1225];
  r+= rf[2431]*tf[1280];
  r+= rf[2402]*tf[1363];
  r+= rf[2417]*tf[1364];
  r+= rf[2430]*tf[1365];
  r+= rf[2419]*tf[1366];
  r+= rf[1584]*tf[1456];
  r+= rf[2434]*tf[1459];
  r+= rf[2397]*tf[1463];
  r+= rf[2424]*tf[1464];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2404];
  r+= rf[2407]*tf[1084];
  r+= rf[2421]*tf[1097];
  r+= rf[2403]*tf[1098];
  r+= rf[2414]*tf[1099];
  r+= rf[2413]*tf[1137];
  r+= rf[2394]*tf[1146];
  r+= rf[2400]*tf[1147];
  r+= rf[2411]*tf[1148];
  r+= rf[2399]*tf[1149];
  r+= rf[2409]*tf[1150];
  r+= eps5*rf[2408]*tf[1159];
  r+= rf[2406]*tf[1164];
  r+= rf[2415]*tf[1361];
  r+= rf[2395]*tf[1363];
  r+= rf[2398]*tf[1364];
  r+= rf[2412]*tf[1365];
  r+= rf[2401]*tf[1366];
  r+= rf[2405]*tf[1377];
  r+= rf[1582]*tf[1456];
  r+= rf[2423]*tf[1458];
  r+= rf[2393]*tf[1462];
  r+= rf[2392]*tf[1465];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmpE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1747];
  r+= rf[1701]*tf[20];
  r+= rf[1735]*tf[21];
  r+= rf[1742]*tf[28];
  r+= rf[1713]*tf[38];
  r+= rf[1763]*tf[55];
  r+= rf[1754]*tf[59];
  r+= eps5*rf[1729]*tf[105];
  r+= eps5*rf[1728]*tf[117];
  r+= rf[1750]*tf[121];
  r+= rf[1755]*tf[167];
  r+= rf[1758]*tf[190];
  r+= rf[1716]*tf[229];
  r+= rf[1692]*tf[919];
  r+= rf[1740]*tf[920];
  r+= rf[1702]*tf[1146];
  r+= rf[1741]*tf[1147];
  r+= rf[1748]*tf[1148];
  r+= rf[1734]*tf[1149];
  r+= rf[1760]*tf[1150];
  r+= rf[1739]*tf[1151];
  r+= rf[1737]*tf[1152];
  r+= rf[1769]*tf[1153];
  r+= rf[1745]*tf[1154];
  r+= rf[1756]*tf[1155];
  r+= rf[1772]*tf[1156];
  r+= rf[1768]*tf[1157];
  r+= rf[1761]*tf[1158];
  r+= eps5*rf[1732]*tf[1159];
  r+= rf[1759]*tf[1162];
  r+= rf[1743]*tf[1164];
  r+= rf[1751]*tf[1165];
  r+= rf[1736]*tf[1166];
  r+= rf[1703]*tf[1167];
  r+= rf[1738]*tf[1168];
  r+= rf[1749]*tf[1169];
  r+= rf[1762]*tf[1171];
  r+= rf[1672]*tf[1172];
  r+= rf[1746]*tf[1173];
  r+= rf[1744]*tf[1174];
  r+= rf[1775]*tf[1175];
  r+= rf[1753]*tf[1176];
  r+= rf[1757]*tf[1177];
  r+= rf[1752]*tf[1178];
  r+= rf[1719]*tf[1180];
  r+= rf[1717]*tf[1182];
  r+= rf[1704]*tf[1187];
  r+= rf[1726]*tf[1188];
  r+= rf[1776]*tf[1189];
  r+= rf[1715]*tf[1192];
  r+= rf[1671]*tf[1193];
  r+= rf[1679]*tf[1194];
  r+= rf[1718]*tf[1195];
  r+= rf[1725]*tf[1196];
  r+= rf[1777]*tf[1197];
  r+= rf[1771]*tf[1198];
  r+= rf[1770]*tf[1199];
  r+= rf[1765]*tf[1201];
  r+= rf[1764]*tf[1202];
  r+= rf[1714]*tf[1203];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmmpO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1696];
  r+= rf[1663]*tf[20];
  r+= rf[1690]*tf[21];
  r+= rf[1688]*tf[28];
  r+= rf[1675]*tf[38];
  r+= rf[1723]*tf[55];
  r+= rf[1697]*tf[56];
  r+= rf[1707]*tf[59];
  r+= eps5*rf[1774]*tf[105];
  r+= eps5*rf[1773]*tf[117];
  r+= rf[1699]*tf[121];
  r+= rf[1708]*tf[167];
  r+= rf[1661]*tf[172];
  r+= rf[1711]*tf[190];
  r+= rf[1676]*tf[919];
  r+= rf[1684]*tf[920];
  r+= rf[1712]*tf[926];
  r+= rf[1682]*tf[1151];
  r+= rf[1680]*tf[1152];
  r+= rf[1727]*tf[1153];
  r+= rf[1693]*tf[1154];
  r+= rf[1709]*tf[1155];
  r+= rf[1731]*tf[1156];
  r+= rf[1733]*tf[1157];
  r+= rf[1720]*tf[1158];
  r+= eps5*rf[1691]*tf[1159];
  r+= rf[1662]*tf[1160];
  r+= rf[1687]*tf[1161];
  r+= rf[1694]*tf[1162];
  r+= rf[1689]*tf[1163];
  r+= rf[1685]*tf[1164];
  r+= rf[1700]*tf[1165];
  r+= rf[1665]*tf[1167];
  r+= rf[1681]*tf[1168];
  r+= rf[1698]*tf[1169];
  r+= rf[1683]*tf[1170];
  r+= rf[1721]*tf[1171];
  r+= rf[1666]*tf[1172];
  r+= rf[1695]*tf[1173];
  r+= rf[1686]*tf[1174];
  r+= rf[1706]*tf[1176];
  r+= rf[1710]*tf[1177];
  r+= rf[1705]*tf[1178];
  r+= rf[1674]*tf[1179];
  r+= rf[1670]*tf[1181];
  r+= rf[1660]*tf[1183];
  r+= rf[1766]*tf[1184];
  r+= rf[1673]*tf[1185];
  r+= rf[1729]*tf[1186];
  r+= rf[1677]*tf[1188];
  r+= rf[1767]*tf[1189];
  r+= rf[1664]*tf[1190];
  r+= rf[1668]*tf[1191];
  r+= rf[1678]*tf[1196];
  r+= rf[1730]*tf[1198];
  r+= rf[1722]*tf[1200];
  r+= rf[1724]*tf[1201];
  r+= rf[1667]*tf[1204];
  r+= rf[1669]*tf[1205];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmpmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1839];
  r+= rf[1823]*tf[20];
  r+= rf[1850]*tf[21];
  r+= rf[1841]*tf[28];
  r+= rf[1887]*tf[55];
  r+= rf[1852]*tf[59];
  r+= eps5*rf[1866]*tf[105];
  r+= eps5*rf[1879]*tf[117];
  r+= rf[1824]*tf[247];
  r+= rf[1819]*tf[249];
  r+= rf[1847]*tf[250];
  r+= eps5*rf[1778]*tf[294];
  r+= eps5*rf[1883]*tf[294];
  r+= rf[1892]*tf[356];
  r+= rf[1818]*tf[358];
  r+= rf[1878]*tf[796];
  r+= rf[1849]*tf[1146];
  r+= rf[1842]*tf[1147];
  r+= rf[1876]*tf[1148];
  r+= rf[1820]*tf[1149];
  r+= rf[1846]*tf[1150];
  r+= rf[1888]*tf[1156];
  r+= eps5*rf[1865]*tf[1159];
  r+= rf[1859]*tf[1162];
  r+= rf[1863]*tf[1164];
  r+= rf[1870]*tf[1206];
  r+= rf[1843]*tf[1207];
  r+= rf[1848]*tf[1208];
  r+= rf[1889]*tf[1209];
  r+= rf[1885]*tf[1210];
  r+= rf[1875]*tf[1211];
  r+= rf[1872]*tf[1212];
  r+= rf[1845]*tf[1213];
  r+= rf[1821]*tf[1215];
  r+= rf[1844]*tf[1216];
  r+= rf[1871]*tf[1218];
  r+= rf[1884]*tf[1219];
  r+= rf[1840]*tf[1220];
  r+= rf[1822]*tf[1221];
  r+= rf[1862]*tf[1223];
  r+= rf[1877]*tf[1225];
  r+= rf[1873]*tf[1226];
  r+= eps5*rf[1881]*tf[1227];
  r+= eps5*rf[1880]*tf[1228];
  r+= eps5*rf[1804]*tf[1229];
  r+= eps5*rf[1811]*tf[1229];
  r+= eps5*rf[1788]*tf[1230];
  r+= rf[1874]*tf[1231];
  r+= rf[1891]*tf[1232];
  r+= rf[1893]*tf[1234];
  r+= rf[1854]*tf[1235];
  r+= rf[1894]*tf[1237];
  r+= rf[1890]*tf[1238];
  r+= rf[1826]*tf[1239];
  r+= rf[1856]*tf[1241];
  r+= rf[1853]*tf[1243];
  r+= rf[1855]*tf[1244];
  r+= rf[1790]*tf[1246];
  r+= rf[1861]*tf[1248];
  r+= rf[1791]*tf[1250];
  r+= rf[1857]*tf[1251];
  r+= rf[1789]*tf[1255];
  r+= rf[1895]*tf[1256];
  r+= rf[1886]*tf[1257];
  r+= rf[1860]*tf[1259];
  r+= rf[1851]*tf[1260];
  r+= rf[1858]*tf[1262];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmpmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1792];
  r+= rf[1786]*tf[20];
  r+= rf[1803]*tf[21];
  r+= rf[1802]*tf[24];
  r+= rf[1795]*tf[25];
  r+= rf[1831]*tf[26];
  r+= rf[1794]*tf[28];
  r+= rf[1783]*tf[29];
  r+= rf[1798]*tf[30];
  r+= rf[1837]*tf[55];
  r+= rf[1812]*tf[56];
  r+= rf[1805]*tf[59];
  r+= eps5*rf[1869]*tf[105];
  r+= eps5*rf[1868]*tf[117];
  r+= rf[1787]*tf[247];
  r+= rf[1779]*tf[249];
  r+= rf[1800]*tf[250];
  r+= rf[1782]*tf[360];
  r+= rf[1834]*tf[796];
  r+= rf[1864]*tf[1156];
  r+= eps5*rf[1817]*tf[1159];
  r+= rf[1815]*tf[1164];
  r+= rf[1825]*tf[1206];
  r+= rf[1797]*tf[1207];
  r+= rf[1801]*tf[1208];
  r+= rf[1867]*tf[1209];
  r+= rf[1835]*tf[1210];
  r+= rf[1833]*tf[1211];
  r+= rf[1829]*tf[1212];
  r+= rf[1838]*tf[1214];
  r+= rf[1784]*tf[1215];
  r+= rf[1796]*tf[1216];
  r+= rf[1799]*tf[1217];
  r+= rf[1827]*tf[1218];
  r+= rf[1793]*tf[1220];
  r+= rf[1785]*tf[1221];
  r+= rf[1816]*tf[1222];
  r+= rf[1828]*tf[1224];
  r+= rf[1830]*tf[1226];
  r+= rf[1832]*tf[1231];
  r+= rf[1881]*tf[1233];
  r+= rf[1806]*tf[1236];
  r+= rf[1882]*tf[1237];
  r+= rf[1788]*tf[1240];
  r+= rf[1808]*tf[1242];
  r+= rf[1807]*tf[1244];
  r+= rf[1809]*tf[1245];
  r+= rf[1780]*tf[1246];
  r+= rf[1814]*tf[1247];
  r+= rf[1879]*tf[1249];
  r+= rf[1781]*tf[1250];
  r+= rf[1810]*tf[1251];
  r+= rf[1880]*tf[1252];
  r+= rf[1778]*tf[1253];
  r+= rf[1883]*tf[1254];
  r+= rf[1836]*tf[1258];
  r+= rf[1813]*tf[1259];
  r+= rf[1804]*tf[1261];
  r+= rf[1811]*tf[1263];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmppE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2057];
  r+= rf[2039]*tf[20];
  r+= rf[2066]*tf[21];
  r+= rf[2071]*tf[28];
  r+= rf[2077]*tf[56];
  r+= rf[2053]*tf[121];
  r+= rf[2052]*tf[249];
  r+= rf[2067]*tf[441];
  r+= rf[2115]*tf[725];
  r+= rf[2061]*tf[926];
  r+= rf[2064]*tf[1158];
  r+= eps5*rf[2084]*tf[1159];
  r+= rf[2030]*tf[1160];
  r+= rf[2070]*tf[1161];
  r+= rf[2076]*tf[1162];
  r+= rf[2065]*tf[1163];
  r+= rf[2102]*tf[1198];
  r+= rf[2080]*tf[1231];
  r+= eps5*rf[1918]*tf[1265];
  r+= rf[2075]*tf[1278];
  r+= rf[2062]*tf[1292];
  r+= eps5*rf[2086]*tf[1294];
  r+= rf[2055]*tf[1295];
  r+= rf[2059]*tf[1312];
  r+= rf[1954]*tf[1314];
  r+= rf[2112]*tf[1316];
  r+= rf[2100]*tf[1317];
  r+= rf[2101]*tf[1318];
  r+= rf[2079]*tf[1319];
  r+= rf[2023]*tf[1320];
  r+= rf[2081]*tf[1321];
  r+= rf[2072]*tf[1323];
  r+= rf[2089]*tf[1324];
  r+= rf[2063]*tf[1325];
  r+= rf[2096]*tf[1326];
  r+= rf[2097]*tf[1327];
  r+= rf[2078]*tf[1328];
  r+= rf[2031]*tf[1329];
  r+= rf[2058]*tf[1330];
  r+= rf[2069]*tf[1331];
  r+= rf[2099]*tf[1332];
  r+= rf[2068]*tf[1333];
  r+= rf[2073]*tf[1334];
  r+= rf[2060]*tf[1335];
  r+= rf[2054]*tf[1337];
  r+= eps5*rf[2013]*tf[1338];
  r+= rf[2056]*tf[1339];
  r+= rf[2110]*tf[1340];
  r+= rf[2045]*tf[1341];
  r+= rf[2108]*tf[1345];
  r+= rf[2098]*tf[1346];
  r+= rf[2111]*tf[1347];
  r+= rf[2113]*tf[1349];
  r+= rf[2114]*tf[1351];
  r+= rf[2026]*tf[1352];
  r+= rf[2109]*tf[1354];
  r+= rf[2020]*tf[1355];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LmppO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2019];
  r+= rf[2012]*tf[20];
  r+= rf[2033]*tf[21];
  r+= rf[2038]*tf[28];
  r+= eps5*rf[2091]*tf[105];
  r+= eps5*rf[2092]*tf[117];
  r+= rf[2015]*tf[121];
  r+= rf[2014]*tf[249];
  r+= rf[2034]*tf[441];
  r+= rf[2107]*tf[725];
  r+= rf[2051]*tf[1099];
  r+= rf[2010]*tf[1146];
  r+= rf[2037]*tf[1147];
  r+= rf[2044]*tf[1148];
  r+= rf[2032]*tf[1149];
  r+= rf[2025]*tf[1150];
  r+= rf[2029]*tf[1158];
  r+= eps5*rf[2050]*tf[1159];
  r+= rf[2043]*tf[1162];
  r+= rf[2088]*tf[1198];
  r+= rf[2049]*tf[1231];
  r+= eps5*rf[2090]*tf[1266];
  r+= rf[2083]*tf[1269];
  r+= rf[2042]*tf[1278];
  r+= rf[2027]*tf[1292];
  r+= rf[2017]*tf[1295];
  r+= rf[2022]*tf[1312];
  r+= rf[2007]*tf[1313];
  r+= rf[1918]*tf[1314];
  r+= rf[2103]*tf[1316];
  r+= rf[2087]*tf[1318];
  r+= rf[2047]*tf[1319];
  r+= rf[2008]*tf[1320];
  r+= rf[2048]*tf[1322];
  r+= rf[2028]*tf[1325];
  r+= rf[2074]*tf[1326];
  r+= rf[2082]*tf[1327];
  r+= rf[2046]*tf[1328];
  r+= rf[2011]*tf[1329];
  r+= rf[2021]*tf[1330];
  r+= rf[2036]*tf[1331];
  r+= rf[2085]*tf[1332];
  r+= rf[2035]*tf[1333];
  r+= rf[2040]*tf[1334];
  r+= rf[2024]*tf[1335];
  r+= rf[2041]*tf[1336];
  r+= rf[2016]*tf[1337];
  r+= rf[2018]*tf[1339];
  r+= rf[2095]*tf[1340];
  r+= rf[2013]*tf[1342];
  r+= rf[2104]*tf[1343];
  r+= rf[2086]*tf[1344];
  r+= rf[2093]*tf[1345];
  r+= rf[2106]*tf[1348];
  r+= rf[2009]*tf[1350];
  r+= rf[2105]*tf[1353];
  r+= rf[2094]*tf[1354];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1957];
  r+= rf[1933]*tf[20];
  r+= rf[1958]*tf[21];
  r+= rf[1962]*tf[28];
  r+= rf[1985]*tf[56];
  r+= eps5*rf[1906]*tf[113];
  r+= rf[1912]*tf[121];
  r+= rf[2005]*tf[419];
  r+= rf[2003]*tf[420];
  r+= rf[2006]*tf[560];
  r+= rf[1973]*tf[649];
  r+= rf[1984]*tf[926];
  r+= rf[1988]*tf[1137];
  r+= eps5*rf[1968]*tf[1159];
  r+= rf[1944]*tf[1160];
  r+= rf[1963]*tf[1161];
  r+= rf[1976]*tf[1162];
  r+= rf[1959]*tf[1163];
  r+= rf[1990]*tf[1210];
  r+= rf[1981]*tf[1231];
  r+= eps5*rf[1918]*tf[1265];
  r+= rf[1960]*tf[1267];
  r+= rf[1942]*tf[1268];
  r+= rf[1998]*tf[1269];
  r+= rf[1997]*tf[1270];
  r+= rf[1983]*tf[1272];
  r+= rf[1961]*tf[1273];
  r+= rf[1965]*tf[1274];
  r+= rf[1980]*tf[1276];
  r+= rf[1977]*tf[1277];
  r+= rf[1979]*tf[1278];
  r+= rf[2001]*tf[1281];
  r+= rf[1943]*tf[1282];
  r+= rf[1991]*tf[1283];
  r+= rf[1964]*tf[1284];
  r+= rf[1999]*tf[1285];
  r+= rf[1975]*tf[1286];
  r+= rf[1989]*tf[1287];
  r+= rf[1982]*tf[1288];
  r+= rf[1978]*tf[1289];
  r+= rf[1914]*tf[1290];
  r+= rf[1911]*tf[1292];
  r+= eps5*rf[1971]*tf[1294];
  r+= rf[1915]*tf[1295];
  r+= rf[1974]*tf[1296];
  r+= rf[1945]*tf[1298];
  r+= rf[1948]*tf[1299];
  r+= rf[1917]*tf[1300];
  r+= rf[1946]*tf[1303];
  r+= rf[2002]*tf[1304];
  r+= rf[2004]*tf[1306];
  r+= rf[1947]*tf[1307];
  r+= rf[1949]*tf[1309];
  r+= rf[2000]*tf[1310];
  r+= rf[1953]*tf[1312];
  r+= rf[1913]*tf[1313];
  r+= rf[1954]*tf[1314];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[1910];
  r+= rf[1902]*tf[20];
  r+= rf[1925]*tf[21];
  r+= rf[1921]*tf[28];
  r+= rf[1935]*tf[56];
  r+= rf[1899]*tf[121];
  r+= rf[1993]*tf[419];
  r+= rf[1995]*tf[420];
  r+= rf[1992]*tf[560];
  r+= rf[1932]*tf[649];
  r+= rf[1919]*tf[926];
  r+= eps5*rf[1929]*tf[1159];
  r+= rf[1905]*tf[1160];
  r+= rf[1920]*tf[1161];
  r+= rf[1938]*tf[1162];
  r+= rf[1926]*tf[1163];
  r+= rf[1951]*tf[1210];
  r+= rf[1940]*tf[1231];
  r+= eps5*rf[1986]*tf[1264];
  r+= eps5*rf[1956]*tf[1266];
  r+= rf[1922]*tf[1267];
  r+= rf[1904]*tf[1268];
  r+= rf[1970]*tf[1269];
  r+= rf[1972]*tf[1270];
  r+= rf[1924]*tf[1271];
  r+= rf[1923]*tf[1273];
  r+= rf[1927]*tf[1275];
  r+= rf[1941]*tf[1276];
  r+= rf[1934]*tf[1278];
  r+= rf[1952]*tf[1279];
  r+= rf[1950]*tf[1280];
  r+= rf[1966]*tf[1281];
  r+= rf[1903]*tf[1282];
  r+= rf[1955]*tf[1283];
  r+= rf[1928]*tf[1284];
  r+= rf[1969]*tf[1285];
  r+= rf[1930]*tf[1286];
  r+= rf[1939]*tf[1288];
  r+= rf[1937]*tf[1289];
  r+= rf[1897]*tf[1290];
  r+= rf[1936]*tf[1291];
  r+= rf[1900]*tf[1292];
  r+= eps5*rf[1987]*tf[1293];
  r+= rf[1896]*tf[1295];
  r+= rf[1931]*tf[1296];
  r+= rf[1906]*tf[1297];
  r+= rf[1908]*tf[1301];
  r+= rf[1971]*tf[1302];
  r+= rf[1996]*tf[1304];
  r+= rf[1901]*tf[1305];
  r+= rf[1994]*tf[1306];
  r+= rf[1909]*tf[1307];
  r+= rf[1967]*tf[1308];
  r+= rf[1907]*tf[1309];
  r+= rf[1898]*tf[1311];
  r+= rf[1916]*tf[1312];
  r+= rf[1918]*tf[1315];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmpE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2186];
  r+= rf[2227]*tf[55];
  r+= rf[2178]*tf[59];
  r+= eps5*rf[2163]*tf[105];
  r+= eps5*rf[2196]*tf[117];
  r+= rf[2231]*tf[419];
  r+= rf[2189]*tf[1146];
  r+= rf[2190]*tf[1147];
  r+= rf[2184]*tf[1148];
  r+= rf[2193]*tf[1149];
  r+= rf[2216]*tf[1150];
  r+= rf[2221]*tf[1153];
  r+= eps5*rf[2162]*tf[1159];
  r+= rf[2172]*tf[1164];
  r+= rf[2203]*tf[1210];
  r+= rf[2179]*tf[1211];
  r+= rf[2202]*tf[1212];
  r+= rf[2213]*tf[1218];
  r+= rf[2185]*tf[1219];
  r+= rf[2226]*tf[1220];
  r+= rf[2210]*tf[1231];
  r+= rf[2176]*tf[1244];
  r+= rf[2209]*tf[1251];
  r+= rf[2181]*tf[1257];
  r+= rf[2180]*tf[1259];
  r+= rf[2182]*tf[1296];
  r+= rf[2225]*tf[1339];
  r+= rf[2191]*tf[1356];
  r+= rf[2219]*tf[1357];
  r+= rf[2222]*tf[1358];
  r+= rf[2220]*tf[1359];
  r+= rf[2170]*tf[1360];
  r+= rf[2198]*tf[1361];
  r+= rf[2206]*tf[1362];
  r+= rf[2134]*tf[1363];
  r+= rf[2171]*tf[1364];
  r+= rf[2215]*tf[1365];
  r+= rf[2169]*tf[1366];
  r+= rf[2217]*tf[1367];
  r+= rf[2218]*tf[1371];
  r+= rf[2192]*tf[1372];
  r+= rf[2214]*tf[1373];
  r+= rf[2173]*tf[1375];
  r+= rf[2175]*tf[1376];
  r+= rf[2224]*tf[1378];
  r+= rf[2228]*tf[1379];
  r+= rf[2205]*tf[1380];
  r+= rf[2230]*tf[1381];
  r+= rf[1679]*tf[1383];
  r+= rf[2212]*tf[1385];
  r+= rf[2177]*tf[1387];
  r+= rf[2174]*tf[1389];
  r+= rf[2145]*tf[1391];
  r+= rf[2211]*tf[1394];
  r+= rf[2223]*tf[1395];
  r+= rf[2137]*tf[1397];
  r+= rf[2204]*tf[1398];
  r+= rf[2229]*tf[1401];
  r+= rf[2119]*tf[1403];
  r+= rf[2146]*tf[1405];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpmpO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2136];
  r+= rf[2187]*tf[55];
  r+= rf[2142]*tf[59];
  r+= eps5*rf[2207]*tf[105];
  r+= eps5*rf[2208]*tf[117];
  r+= rf[2155]*tf[267];
  r+= rf[2201]*tf[419];
  r+= rf[2183]*tf[649];
  r+= rf[2122]*tf[1146];
  r+= rf[2128]*tf[1147];
  r+= rf[2138]*tf[1148];
  r+= rf[2124]*tf[1149];
  r+= rf[2154]*tf[1150];
  r+= rf[2164]*tf[1153];
  r+= eps5*rf[2130]*tf[1159];
  r+= rf[2129]*tf[1164];
  r+= rf[2151]*tf[1210];
  r+= rf[2141]*tf[1211];
  r+= rf[2150]*tf[1212];
  r+= rf[2147]*tf[1214];
  r+= rf[2166]*tf[1231];
  r+= rf[2144]*tf[1244];
  r+= rf[2165]*tf[1251];
  r+= rf[2148]*tf[1257];
  r+= rf[2140]*tf[1259];
  r+= rf[2143]*tf[1296];
  r+= rf[2131]*tf[1356];
  r+= rf[2159]*tf[1357];
  r+= rf[2160]*tf[1358];
  r+= rf[2158]*tf[1359];
  r+= rf[2125]*tf[1360];
  r+= rf[2157]*tf[1361];
  r+= rf[2161]*tf[1362];
  r+= rf[2116]*tf[1363];
  r+= rf[2123]*tf[1364];
  r+= rf[2153]*tf[1365];
  r+= rf[2127]*tf[1366];
  r+= rf[2149]*tf[1368];
  r+= rf[2126]*tf[1369];
  r+= rf[2188]*tf[1370];
  r+= rf[2156]*tf[1371];
  r+= rf[2152]*tf[1373];
  r+= rf[2132]*tf[1374];
  r+= rf[2135]*tf[1377];
  r+= rf[1660]*tf[1382];
  r+= rf[2120]*tf[1384];
  r+= rf[2168]*tf[1385];
  r+= rf[2199]*tf[1386];
  r+= rf[2118]*tf[1388];
  r+= rf[2163]*tf[1390];
  r+= rf[2139]*tf[1392];
  r+= rf[2195]*tf[1393];
  r+= rf[2167]*tf[1394];
  r+= rf[2133]*tf[1396];
  r+= rf[2194]*tf[1399];
  r+= rf[2197]*tf[1400];
  r+= rf[2200]*tf[1402];
  r+= rf[2117]*tf[1404];
  r+= rf[2121]*tf[1406];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LppmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2319];
  r+= rf[2347]*tf[55];
  r+= rf[2300]*tf[59];
  r+= eps5*rf[2318]*tf[105];
  r+= eps5*rf[2275]*tf[117];
  r+= rf[2298]*tf[121];
  r+= rf[2301]*tf[167];
  r+= rf[2331]*tf[190];
  r+= rf[2345]*tf[420];
  r+= rf[2332]*tf[940];
  r+= rf[2310]*tf[1146];
  r+= rf[2307]*tf[1147];
  r+= rf[2342]*tf[1148];
  r+= rf[2311]*tf[1149];
  r+= rf[2321]*tf[1150];
  r+= rf[2328]*tf[1154];
  r+= rf[2302]*tf[1155];
  r+= rf[2327]*tf[1158];
  r+= eps5*rf[2284]*tf[1159];
  r+= rf[2294]*tf[1164];
  r+= rf[2346]*tf[1165];
  r+= rf[2297]*tf[1169];
  r+= rf[2303]*tf[1177];
  r+= rf[2299]*tf[1178];
  r+= rf[2336]*tf[1209];
  r+= rf[2325]*tf[1362];
  r+= rf[2257]*tf[1363];
  r+= rf[2288]*tf[1364];
  r+= rf[2341]*tf[1365];
  r+= rf[2290]*tf[1366];
  r+= rf[2309]*tf[1407];
  r+= rf[2337]*tf[1408];
  r+= rf[2334]*tf[1409];
  r+= rf[2324]*tf[1410];
  r+= rf[2335]*tf[1411];
  r+= rf[2289]*tf[1412];
  r+= rf[2308]*tf[1417];
  r+= rf[2320]*tf[1419];
  r+= rf[2333]*tf[1420];
  r+= rf[2312]*tf[1422];
  r+= rf[2296]*tf[1423];
  r+= rf[2293]*tf[1425];
  r+= rf[2304]*tf[1426];
  r+= rf[2343]*tf[1427];
  r+= rf[2344]*tf[1428];
  r+= rf[2338]*tf[1429];
  r+= rf[2340]*tf[1432];
  r+= rf[2287]*tf[1435];
  r+= rf[2286]*tf[1436];
  r+= rf[2268]*tf[1438];
  r+= rf[2281]*tf[1439];
  r+= rf[2280]*tf[1441];
  r+= rf[2339]*tf[1442];
  r+= rf[1789]*tf[1443];
  r+= rf[2291]*tf[1445];
  r+= rf[2326]*tf[1447];
  r+= rf[2279]*tf[1448];
  r+= rf[2306]*tf[1450];
  r+= rf[2305]*tf[1451];
  r+= rf[2292]*tf[1453];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LppmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2255];
  r+= rf[2323]*tf[55];
  r+= rf[2262]*tf[59];
  r+= eps5*rf[2329]*tf[105];
  r+= eps5*rf[2330]*tf[117];
  r+= rf[2258]*tf[121];
  r+= rf[2261]*tf[167];
  r+= rf[2295]*tf[190];
  r+= rf[2316]*tf[420];
  r+= rf[2240]*tf[911];
  r+= rf[2246]*tf[912];
  r+= rf[2256]*tf[913];
  r+= rf[2282]*tf[914];
  r+= rf[2234]*tf[915];
  r+= rf[2233]*tf[1009];
  r+= rf[2277]*tf[1154];
  r+= rf[2260]*tf[1155];
  r+= rf[2278]*tf[1158];
  r+= eps5*rf[2236]*tf[1159];
  r+= rf[2248]*tf[1164];
  r+= rf[2253]*tf[1169];
  r+= rf[2267]*tf[1171];
  r+= rf[2263]*tf[1176];
  r+= rf[2259]*tf[1177];
  r+= rf[2264]*tf[1178];
  r+= rf[2274]*tf[1209];
  r+= rf[2276]*tf[1362];
  r+= rf[2252]*tf[1407];
  r+= rf[2271]*tf[1408];
  r+= rf[2272]*tf[1409];
  r+= rf[2285]*tf[1410];
  r+= rf[2273]*tf[1411];
  r+= rf[2243]*tf[1412];
  r+= rf[2232]*tf[1413];
  r+= rf[2245]*tf[1414];
  r+= rf[2283]*tf[1415];
  r+= rf[2235]*tf[1416];
  r+= rf[2244]*tf[1417];
  r+= rf[2269]*tf[1418];
  r+= rf[2270]*tf[1420];
  r+= rf[2322]*tf[1421];
  r+= rf[2247]*tf[1422];
  r+= rf[2254]*tf[1423];
  r+= rf[2250]*tf[1424];
  r+= rf[2241]*tf[1430];
  r+= rf[2315]*tf[1431];
  r+= rf[2314]*tf[1433];
  r+= rf[1778]*tf[1434];
  r+= rf[2251]*tf[1435];
  r+= rf[2249]*tf[1437];
  r+= rf[2237]*tf[1439];
  r+= rf[2317]*tf[1440];
  r+= rf[2238]*tf[1441];
  r+= rf[2275]*tf[1444];
  r+= rf[2313]*tf[1446];
  r+= rf[2239]*tf[1449];
  r+= rf[2265]*tf[1450];
  r+= rf[2266]*tf[1451];
  r+= rf[2242]*tf[1452];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpppE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2382];
  r+= rf[2383]*tf[1085];
  r+= rf[2390]*tf[1097];
  r+= rf[2381]*tf[1098];
  r+= rf[2388]*tf[1099];
  r+= rf[2387]*tf[1137];
  r+= rf[2366]*tf[1146];
  r+= rf[2377]*tf[1147];
  r+= rf[2385]*tf[1148];
  r+= rf[2375]*tf[1149];
  r+= rf[2384]*tf[1150];
  r+= eps5*rf[2373]*tf[1159];
  r+= rf[2379]*tf[1164];
  r+= rf[2361]*tf[1363];
  r+= rf[2376]*tf[1364];
  r+= rf[2386]*tf[1365];
  r+= rf[2378]*tf[1366];
  r+= rf[2391]*tf[1410];
  r+= rf[2351]*tf[1454];
  r+= rf[2380]*tf[1455];
  r+= rf[1505]*tf[1456];
  r+= rf[2352]*tf[1457];
  r+= rf[2389]*tf[1459];
  return r;
}

template<class T>
std::complex<T> qg_HA2L_LpppO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
) {
  std::complex<T> r(0,0);

  r+= rf[2362];
  r+= rf[2372]*tf[1097];
  r+= rf[2363]*tf[1098];
  r+= rf[2353]*tf[1146];
  r+= rf[2354]*tf[1147];
  r+= rf[2368]*tf[1148];
  r+= rf[2356]*tf[1149];
  r+= rf[2365]*tf[1150];
  r+= eps5*rf[2359]*tf[1159];
  r+= rf[2358]*tf[1164];
  r+= rf[2349]*tf[1271];
  r+= rf[2369]*tf[1280];
  r+= rf[2370]*tf[1324];
  r+= rf[2371]*tf[1361];
  r+= rf[2350]*tf[1363];
  r+= rf[2357]*tf[1364];
  r+= rf[2367]*tf[1365];
  r+= rf[2355]*tf[1366];
  r+= rf[2364]*tf[1376];
  r+= rf[1504]*tf[1456];
  r+= rf[2374]*tf[1458];
  r+= rf[2348]*tf[1460];
  r+= rf[2360]*tf[1461];
  return r;
}
#endif

/*---------------------------------------------------------------------------*/

template std::complex<double> qqb_HA1L_LmmpE_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmpE_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmmpO_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmpO_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmpmE_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmpmE_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmpmO_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmpmO_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmmE_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmmE_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmmO_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmmO_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmppE_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmppE_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmppO_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmppO_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmpE_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmpE_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmpO_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmpO_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LppmE_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LppmE_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LppmO_CA(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LppmO_CA(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpppE_CA(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpppE_CA(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LpppO_CA(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpppO_CA(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmmE_CA(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmmE_CA(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmmO_CA(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmmO_CA(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmpE_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmpE_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmmpO_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmpO_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmpmE_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmpmE_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmpmO_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmpmO_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmmE_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmmE_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmmO_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmmO_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmppE_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmppE_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmppO_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmppO_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmpE_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmpE_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmpO_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmpO_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LppmE_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LppmE_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LppmO_CF(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LppmO_CF(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpppE_CF(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpppE_CF(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LpppO_CF(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpppO_CF(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmmE_CF(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmmE_CF(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmmO_CF(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmmO_CF(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmpE_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmpE_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmmpO_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmpO_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmpmE_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmpmE_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmpmO_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmpmO_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmmE_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmmE_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmmO_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmmO_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmppE_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmppE_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LmppO_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmppO_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmpE_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmpE_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpmpO_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpmpO_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LppmE_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LppmE_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LppmO_nfaa(
  const std::array<double,292>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LppmO_nfaa(
  const std::array<dd_real,292>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qqb_HA1L_LpppE_nfaa(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpppE_nfaa(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LpppO_nfaa(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LpppO_nfaa(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmmE_nfaa(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmmE_nfaa(
  const std::array<dd_real,292>& hr
);
#endif

template std::complex<double> qqb_HA1L_LmmmO_nfaa(
  const std::array<double,292>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA1L_LmmmO_nfaa(
  const std::array<dd_real,292>& hr
);
#endif

/*---------------------------------------------------------------------------*/

template std::complex<double> qg_HA1L_LmmpE_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmpE_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmmpO_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmpO_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmpmE_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmpmE_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmpmO_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmpmO_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmmE_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmmE_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmmO_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmmO_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmppE_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmppE_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmppO_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmppO_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmpE_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmpE_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmpO_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmpO_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LppmE_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LppmE_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LppmO_CA(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LppmO_CA(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpppE_CA(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpppE_CA(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LpppO_CA(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpppO_CA(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmmE_CA(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmmE_CA(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmmO_CA(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmmO_CA(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmpE_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmpE_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmmpO_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmpO_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmpmE_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmpmE_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmpmO_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmpmO_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmmE_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmmE_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmmO_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmmO_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmppE_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmppE_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmppO_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmppO_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmpE_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmpE_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmpO_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmpO_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LppmE_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LppmE_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LppmO_CF(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LppmO_CF(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpppE_CF(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpppE_CF(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LpppO_CF(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpppO_CF(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmmE_CF(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmmE_CF(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmmO_CF(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmmO_CF(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmpE_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmpE_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmmpO_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmpO_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmpmE_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmpmE_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmpmO_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmpmO_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmmE_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmmE_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmmO_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmmO_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmppE_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmppE_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LmppO_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmppO_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmpE_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmpE_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpmpO_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpmpO_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LppmE_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LppmE_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LppmO_nfaa(
  const std::array<double,275>& hr,
  const std::array<double,85>& tcs,
  const std::array<std::complex<double>,19>& fw1,
  const std::array<std::complex<double>,24>& fw2
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LppmO_nfaa(
  const std::array<dd_real,275>& hr,
  const std::array<dd_real,85>& tcs,
  const std::array<std::complex<dd_real>,19>& fw1,
  const std::array<std::complex<dd_real>,24>& fw2
);
#endif

template std::complex<double> qg_HA1L_LpppE_nfaa(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpppE_nfaa(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LpppO_nfaa(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LpppO_nfaa(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmmE_nfaa(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmmE_nfaa(
  const std::array<dd_real,275>& hr
);
#endif

template std::complex<double> qg_HA1L_LmmmO_nfaa(
  const std::array<double,275>& hr
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA1L_LmmmO_nfaa(
  const std::array<dd_real,275>& hr
);
#endif

/* --------------------------------- 2-loop -------------------------------- */
#ifdef LOOPSQ_ENABLED
/* ---------------------------------- qqb ---------------------------------- */
template std::complex<double> qqb_HA2L_LmmmE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmmE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmmmO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmmO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmmpE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmpE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmmpO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmpO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmpmE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmpmE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmpmO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmpmO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmppE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmppE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmppO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmppO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LpmmE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmmE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LpmmO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmmO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LpmpE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmpE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LpmpO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmpO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LppmE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LppmE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LppmO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LppmO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LpppE_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpppE_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LpppO_nfnfaa(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpppO_nfnfaa(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf
);
#endif

template std::complex<double> qqb_HA2L_LmmmE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmmE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmmmO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmmO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmmpE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmpE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmmpO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmpO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmpmE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmpmE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmpmO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmpmO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmppE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmppE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmppO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmppO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmmE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmmE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmmO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmmO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmpE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmpE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmpO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmpO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LppmE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LppmE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LppmO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LppmO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpppE_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpppE_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpppO_nfaaNcp1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpppO_nfaaNcp1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmmmE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmmE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmmmO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmmO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmmpE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmpE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmmpO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmmpO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmpmE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmpmE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmpmO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmpmO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmppE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmppE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LmppO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LmppO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmmE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmmE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmmO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmmO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmpE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmpE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpmpO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpmpO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LppmE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LppmE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LppmO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LppmO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpppE_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpppE_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qqb_HA2L_LpppO_nfaaNcm1(
  const std::array<double, 2438>& rf,
  const std::array<std::complex<double>, 1531>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qqb_HA2L_LpppO_nfaaNcm1(
  const std::array<dd_real, 2438>& rf,
  const std::array<std::complex<dd_real>, 1531>& tf,
  const std::complex<dd_real> eps5
);
#endif

/* ---------------------------------- qg ----------------------------------- */
template std::complex<double> qg_HA2L_LmmmE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmmE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmmmO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmmO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmmpE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmpE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmmpO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmpO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmpmE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmpmE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmpmO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmpmO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmppE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmppE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmppO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmppO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LpmmE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmmE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LpmmO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmmO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LpmpE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmpE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LpmpO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmpO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LppmE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LppmE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LppmO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LppmO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LpppE_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpppE_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LpppO_nfnfaa(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpppO_nfnfaa(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf
);
#endif

template std::complex<double> qg_HA2L_LmmmE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmmE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmmmO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmmO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmmpE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmpE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmmpO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmpO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmpmE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmpmE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmpmO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmpmO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmppE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmppE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmppO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmppO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmmE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmmE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmmO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmmO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmpE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmpE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmpO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmpO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LppmE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LppmE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LppmO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LppmO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpppE_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpppE_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpppO_nfaaNcp1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpppO_nfaaNcp1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmmmE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmmE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmmmO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmmO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmmpE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmpE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmmpO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmmpO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmpmE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmpmE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmpmO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmpmO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmppE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmppE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LmppO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LmppO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmmE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmmE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmmO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmmO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmpE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmpE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpmpO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpmpO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LppmE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LppmE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LppmO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LppmO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpppE_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpppE_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif

template std::complex<double> qg_HA2L_LpppO_nfaaNcm1(
  const std::array<double, 2436>& rf,
  const std::array<std::complex<double>, 1466>& tf,
  const std::complex<double> eps5
);

#ifdef HAVE_QD
template std::complex<dd_real> qg_HA2L_LpppO_nfaaNcm1(
  const std::array<dd_real, 2436>& rf,
  const std::array<std::complex<dd_real>, 1466>& tf,
  const std::complex<dd_real> eps5
);
#endif
#endif /* LOOPSQ_ENABLED */
