#include <cmath>
#include <complex.h>
#include "aajamp.hpp"
#include "aux/util.hpp"
#include "PentagonFunctions/Constants.h"
#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif

template <class T>
void aajAmp<T>::register_process(int proc_id){
    process_id = proc_id;

    T_Interf.register_interf_process(proc_id);
#ifdef HAVE_QD
    dd_Interf.register_interf_process(proc_id);
#endif
}

template <class T>
void aajAmp<T>::register_loop_order(int ord){
    loop_ord = ord;

    T_Interf.register_interf_loop_order(ord);
#ifdef HAVE_QD
    dd_Interf.register_interf_loop_order(ord);
#endif
}

template <class T>
void aajAmp<T>::set_stability_threshold(double x){
    den_thres = x;
}

template <class T>
void aajAmp<T>::set_stability_rescue(int n){
    stability_rescue = n;
}

template <class T>
void aajAmp<T>::register_event(const std::array<T,5>& sij_in, const T musq_in){

  stable_point = true;

  std::complex<T> CI(0,1);

  musq = musq_in;

  sij[0] = sij_in[0]; // s_{12}
  sij[1] = sij_in[1]; // s_{23}
  sij[2] = sij_in[2]; // s_{34}
  sij[3] = sij_in[3]; // s_{45}
  sij[4] = sij_in[4]; // s_{51}

  // Check on eps5, i.e. sqrt(gram_determinant)
  im_eps5  = (sij[0]*sij[1] + sij[1]*sij[2] - sij[2]*sij[3] +
              sij[3]*sij[4] - sij[4]*sij[0]);
  im_eps5 *= im_eps5;
  im_eps5 += -4*sij[0]*sij[1]*sij[2]*(sij[1] - sij[3] - sij[4]);

  if (im_eps5 < 0) {
    im_eps5 = sqrt(-im_eps5);
    eps5 = CI*im_eps5;
  } else {
    aajamp::OUT2STDERR("Trying to register an unphysical event: GD > 0\n");
  }

  // Computes invariants and denominators
  this->compute_invariants();

  // Check for potential instabilities
  if(stability_rescue > 0){
    this->check_stable_point();
  };

#ifdef HAVE_QD
  if(stable_point){
    T_Interf.register_interf_event(sij,musq);
  } else{
    dd_musq = static_cast<dd_real>(musq);
    dd_sij[0] = static_cast<dd_real>(sij[0]);
    dd_sij[1] = static_cast<dd_real>(sij[1]);
    dd_sij[2] = static_cast<dd_real>(sij[2]);
    dd_sij[3] = static_cast<dd_real>(sij[3]);
    dd_sij[4] = static_cast<dd_real>(sij[4]);
    dd_Interf.register_interf_event(dd_sij,dd_musq);
  }
#else
    T_Interf.register_interf_event(sij,musq);
#endif
}


template <class T>
void aajAmp<T>::compute_invariants(){
  k[0]=static_cast<T>(1);
  k[1]=sij[0];
  k[2]=sij[1];
  k[3]=sij[2];
  k[4]=sij[3];
  k[5]=sij[4];
  k[6]=1/sij[0];
  k[7]=1/sij[1];
  k[8]=1/sij[2];
  k[9]=1/sij[3];
  k[10]=1/sij[4];
  k[11]=1/(sij[0]+sij[1]-sij[3]);
  k[12]=1/(sij[1]-sij[3]-sij[4]);
  k[13]=1/(sij[1]+sij[2]-sij[4]);
  k[14]=1/(sij[0]-sij[2]+sij[4]);
  k[15]=1/(sij[0]-sij[2]-sij[3]);
  k[16]=1/(sij[0]+sij[1]);
  k[17]=1/(sij[0]-sij[2]);
  k[18]=1/(sij[1]+sij[2]);
  k[19]=1/(sij[0]-sij[3]);
  k[20]=1/(sij[1]-sij[3]);
  k[21]=1/(sij[0]+sij[1]-sij[2]-sij[3]);
  k[22]=1/(sij[2]+sij[3]);
  k[23]=1/(sij[1]-sij[4]);
  k[24]=1/(sij[2]-sij[4]);
  k[25]=1/(sij[0]+sij[1]-sij[3]-sij[4]);
  k[26]=1/(sij[1]+sij[2]-sij[3]-sij[4]);
  k[27]=1/(sij[0]+sij[4]);
  k[28]=1/(sij[0]-sij[1]-sij[2]+sij[4]);
  k[29]=1/(sij[0]-sij[2]-sij[3]+sij[4]);
  k[30]=1/(sij[3]+sij[4]);
}

template <class T>
void aajAmp<T>::check_stable_point(){

  // First check on denominators
  for(unsigned int i = 6; i < 31; i++){
    stable_point = stable_point && (fabs(1./(k[1]*k[i])) > den_thres);
  }
}

// --------------------------------------------------------
// Interference terms
// --------------------------------------------------------

// --------------------------------------------------------
// q qb -> g a a: Tree x Tree
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qqgaa_W00(T& res){
  res = T_Interf.qqgaa_interf_W00();
}

template <>
void aajAmp<double>::qqgaa_W00(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W00();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W00();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W00();
#endif
}

// --------------------------------------------------------
// q qb -> g a a: Tree x 1-loop
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qqgaa_W01_CA(T& res){
    res = T_Interf.qqgaa_interf_W01_CA();
}

template <class T>
void aajAmp<T>::qqgaa_W01_CF(T& res){
    res = T_Interf.qqgaa_interf_W01_CF();
}

template <class T>
void aajAmp<T>::qqgaa_W01_nfaa(T& res){
    res = T_Interf.qqgaa_interf_W01_nfaa();
}

template <class T>
void aajAmp<T>::qqgaa_W01_nf(T& res){
  res = T_Interf.qqgaa_interf_W01_nf();
}

template <>
void aajAmp<double>::qqgaa_W01_CA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W01_CA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W01_CA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W01_CA();
#endif
}

template <>
void aajAmp<double>::qqgaa_W01_CF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W01_CF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W01_CF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W01_CF();
#endif
}

template <>
void aajAmp<double>::qqgaa_W01_nfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W01_nfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W01_nfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W01_nfaa();
#endif
}

template <>
void aajAmp<double>::qqgaa_W01_nf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W01_nf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W01_nf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W01_nf();
#endif
}

// --------------------------------------------------------
// q qb -> g a a: 1-loop x 1-loop
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qqgaa_W11_CACA(T& res){
  res = T_Interf.qqgaa_interf_W11_CACA();
}

template <class T>
void aajAmp<T>::qqgaa_W11_CFCF(T& res){
  res = T_Interf.qqgaa_interf_W11_CFCF();
}

template <class T>
void aajAmp<T>::qqgaa_W11_nfaanfaa(T& res){
  res = T_Interf.qqgaa_interf_W11_nfaanfaa();
}

template <class T>
void aajAmp<T>::qqgaa_W11_nfnf(T& res){
  res = T_Interf.qqgaa_interf_W11_nfnf();
}

template <class T>
void aajAmp<T>::qqgaa_W11_CACF(T& res){
  res = T_Interf.qqgaa_interf_W11_CACF();
}

template <class T>
void aajAmp<T>::qqgaa_W11_nfaaCA(T& res){
  res = T_Interf.qqgaa_interf_W11_nfaaCA();
}

template <class T>
void aajAmp<T>::qqgaa_W11_nfCA(T& res){
  res = T_Interf.qqgaa_interf_W11_nfCA();
}

template <class T>
void aajAmp<T>::qqgaa_W11_nfaaCF(T& res){
  res = T_Interf.qqgaa_interf_W11_nfaaCF();
}

template <class T>
void aajAmp<T>::qqgaa_W11_nfCF(T& res){
  res = T_Interf.qqgaa_interf_W11_nfCF();
}

template <class T>
void aajAmp<T>::qqgaa_W11_nfnfaa(T& res){
  res = T_Interf.qqgaa_interf_W11_nfnfaa();
}

template <>
void aajAmp<double>::qqgaa_W11_CACA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_CACA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_CACA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_CACA();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_CFCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_CFCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_CFCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_CFCF();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_nfaanfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_nfaanfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_nfaanfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_nfaanfaa();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_nfnf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_nfnf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_nfnf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_nfnf();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_CACF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_CACF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_CACF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_CACF();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_nfaaCA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_nfaaCA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_nfaaCA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_nfaaCA();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_nfCA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_nfCA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_nfCA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_nfCA();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_nfaaCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_nfaaCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_nfaaCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_nfaaCF();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_nfCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_nfCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_nfCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_nfCF();
#endif
}

template <>
void aajAmp<double>::qqgaa_W11_nfnfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W11_nfnfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W11_nfnfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W11_nfnfaa();
#endif
}

// --------------------------------------------------------
// q g -> q a a: 1-loop x 1-loop
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qgqaa_W11_CACA(T& res){
  res = T_Interf.qgqaa_interf_W11_CACA();
}

template <class T>
void aajAmp<T>::qgqaa_W11_CFCF(T& res){
  res = T_Interf.qgqaa_interf_W11_CFCF();
}

template <class T>
void aajAmp<T>::qgqaa_W11_nfaanfaa(T& res){
  res = T_Interf.qgqaa_interf_W11_nfaanfaa();
}

template <class T>
void aajAmp<T>::qgqaa_W11_nfnf(T& res){
  res = T_Interf.qgqaa_interf_W11_nfnf();
}

template <class T>
void aajAmp<T>::qgqaa_W11_CACF(T& res){
  res = T_Interf.qgqaa_interf_W11_CACF();
}

template <class T>
void aajAmp<T>::qgqaa_W11_nfaaCA(T& res){
  res = T_Interf.qgqaa_interf_W11_nfaaCA();
}

template <class T>
void aajAmp<T>::qgqaa_W11_nfCA(T& res){
  res = T_Interf.qgqaa_interf_W11_nfCA();
}

template <class T>
void aajAmp<T>::qgqaa_W11_nfaaCF(T& res){
  res = T_Interf.qgqaa_interf_W11_nfaaCF();
}

template <class T>
void aajAmp<T>::qgqaa_W11_nfCF(T& res){
  res = T_Interf.qgqaa_interf_W11_nfCF();
}

template <class T>
void aajAmp<T>::qgqaa_W11_nfnfaa(T& res){
  res = T_Interf.qgqaa_interf_W11_nfnfaa();
}

template <>
void aajAmp<double>::qgqaa_W11_CACA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_CACA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_CACA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_CACA();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_CFCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_CFCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_CFCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_CFCF();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_nfaanfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_nfaanfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_nfaanfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_nfaanfaa();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_nfnf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_nfnf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_nfnf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_nfnf();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_CACF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_CACF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_CACF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_CACF();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_nfaaCA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_nfaaCA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_nfaaCA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_nfaaCA();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_nfCA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_nfCA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_nfCA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_nfCA();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_nfaaCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_nfaaCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_nfaaCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_nfaaCF();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_nfCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_nfCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_nfCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_nfCF();
#endif
}

template <>
void aajAmp<double>::qgqaa_W11_nfnfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W11_nfnfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W11_nfnfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W11_nfnfaa();
#endif
}


// --------------------------------------------------------
// q qb -> g a a: Tree x 2-loop
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qqgaa_W02_Nc2(T& res){
    res = T_Interf.qqgaa_interf_W02_Nc2();
}

template <class T>
void aajAmp<T>::qqgaa_W02_nfCA(T& res){
    res = T_Interf.qqgaa_interf_W02_nfCA();
}

template <class T>
void aajAmp<T>::qqgaa_W02_nfCF(T& res){
    res = T_Interf.qqgaa_interf_W02_nfCF();
}

template <class T>
void aajAmp<T>::qqgaa_W02_nfnfaa(T& res){
    res = T_Interf.qqgaa_interf_W02_nfnfaa();
}

template <class T>
void aajAmp<T>::qqgaa_W02_nfa(T& res){
    res = T_Interf.qqgaa_interf_W02_nfa();
}

template <class T>
void aajAmp<T>::qqgaa_W02_nfnf(T& res){
  res = T_Interf.qqgaa_interf_W02_nfnf();
}

#ifdef FULLCOL_ENABLED
template <class T>
void aajAmp<T>::qqgaa_W02_Nc0(T& res){
    res = T_Interf.qqgaa_interf_W02_Nc0();
}

template <class T>
void aajAmp<T>::qqgaa_W02_Ncm2(T& res){
    res = T_Interf.qqgaa_interf_W02_Ncm2();
}

template <class T>
void aajAmp<T>::qqgaa_W02_nfaaNc(T& res){
    res = T_Interf.qqgaa_interf_W02_nfaaNc();
}

template <class T>
void aajAmp<T>::qqgaa_W02_nfaaNcm1(T& res){
    res = T_Interf.qqgaa_interf_W02_nfaaNcm1();
}
#endif

template <>
void aajAmp<double>::qqgaa_W02_Nc2(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_Nc2();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_Nc2();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_Nc2();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_nfCA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_nfCA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_nfCA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_nfCA();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_nfCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_nfCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_nfCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_nfCF();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_nfnfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_nfnfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_nfnfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_nfnfaa();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_nfa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_nfa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_nfa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_nfa();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_nfnf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_nfnf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_nfnf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_nfnf();
#endif
}

#ifdef FULLCOL_ENABLED
template <>
void aajAmp<double>::qqgaa_W02_Nc0(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_Nc0();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_Nc0();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_Nc0();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_Ncm2(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_Ncm2();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_Ncm2();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_Ncm2();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_nfaaNc(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_nfaaNc();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_nfaaNc();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_nfaaNc();
#endif
}

template <>
void aajAmp<double>::qqgaa_W02_nfaaNcm1(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W02_nfaaNcm1();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W02_nfaaNcm1();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W02_nfaaNcm1();
#endif
}
#endif /* FULLCOL_ENABLED */

// --------------------------------------------------------
// q g -> q a a: Tree x Tree
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qgqaa_W00(T& res){
  res = T_Interf.qgqaa_interf_W00();
}

template <>
void aajAmp<double>::qgqaa_W00(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W00();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W00();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W00();
#endif
}

// --------------------------------------------------------
// q g -> q a a: Tree x 1-loop
// --------------------------------------------------------

template <class T>
void aajAmp<T>::qgqaa_W01_CA(T& res){
    res = T_Interf.qgqaa_interf_W01_CA();
}

template <class T>
void aajAmp<T>::qgqaa_W01_CF(T& res){
    res = T_Interf.qgqaa_interf_W01_CF();
}

template <class T>
void aajAmp<T>::qgqaa_W01_nfaa(T& res){
    res = T_Interf.qgqaa_interf_W01_nfaa();
}

template <class T>
void aajAmp<T>::qgqaa_W01_nf(T& res){
  res = T_Interf.qgqaa_interf_W01_nf();
}

template <>
void aajAmp<double>::qgqaa_W01_CA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W01_CA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W01_CA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W01_CA();
#endif
}

template <>
void aajAmp<double>::qgqaa_W01_CF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W01_CF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W01_CF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W01_CF();
#endif
}

template <>
void aajAmp<double>::qgqaa_W01_nfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W01_nfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W01_nfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W01_nfaa();
#endif
}

template <>
void aajAmp<double>::qgqaa_W01_nf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W01_nf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W01_nf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W01_nf();
#endif
}

// --------------------------------------------------------
// Tree x 2-loop
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qgqaa_W02_Nc2(T& res){
    res = T_Interf.qgqaa_interf_W02_Nc2();
}

template <class T>
void aajAmp<T>::qgqaa_W02_nfCA(T& res){
    res = T_Interf.qgqaa_interf_W02_nfCA();
}

template <class T>
void aajAmp<T>::qgqaa_W02_nfCF(T& res){
    res = T_Interf.qgqaa_interf_W02_nfCF();
}

template <class T>
void aajAmp<T>::qgqaa_W02_nfnfaa(T& res){
    res = T_Interf.qgqaa_interf_W02_nfnfaa();
}

template <class T>
void aajAmp<T>::qgqaa_W02_nfnf(T& res){
  res = T_Interf.qgqaa_interf_W02_nfnf();
}

#ifdef FULLCOL_ENABLED
template <class T>
void aajAmp<T>::qgqaa_W02_Nc0(T& res){
    res = T_Interf.qgqaa_interf_W02_Nc0();
}

template <class T>
void aajAmp<T>::qgqaa_W02_Ncm2(T& res){
    res = T_Interf.qgqaa_interf_W02_Ncm2();
}

template <class T>
void aajAmp<T>::qgqaa_W02_nfaaNc(T& res){
    res = T_Interf.qgqaa_interf_W02_nfaaNc();
}

template <class T>
void aajAmp<T>::qgqaa_W02_nfaaNcm1(T& res){
    res = T_Interf.qgqaa_interf_W02_nfaaNcm1();
}

template <class T>
void aajAmp<T>::qgqaa_W02_nfa(T& res){
    res = T_Interf.qgqaa_interf_W02_nfa();
}
#endif

template <>
void aajAmp<double>::qgqaa_W02_Nc2(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_Nc2();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_Nc2();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_Nc2();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_nfCA(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_nfCA();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_nfCA();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_nfCA();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_nfCF(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_nfCF();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_nfCF();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_nfCF();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_nfnfaa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_nfnfaa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_nfnfaa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_nfnfaa();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_nfnf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_nfnf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_nfnf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_nfnf();
#endif
}

#ifdef FULLCOL_ENABLED
template <>
void aajAmp<double>::qgqaa_W02_Nc0(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_Nc0();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_Nc0();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_Nc0();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_Ncm2(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_Ncm2();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_Ncm2();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_Ncm2();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_nfaaNc(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_nfaaNc();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_nfaaNc();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_nfaaNc();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_nfaaNcm1(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_nfaaNcm1();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_nfaaNcm1();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_nfaaNcm1();
#endif
}

template <>
void aajAmp<double>::qgqaa_W02_nfa(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W02_nfa();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W02_nfa();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W02_nfa();
#endif
}
#endif /* FULLCOL_ENABLED */

#ifdef LOOPSQ_ENABLED
// --------------------------------------------------------
// q qb -> g a a: 1-loop x 2-loop
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qqgaa_W12_nfaa2Ncp1(T& res){
  res = T_Interf.qqgaa_interf_W12_nfaa2Ncp1();
}

template <class T>
void aajAmp<T>::qqgaa_W12_nfaa2Ncm1(T& res){
  res = T_Interf.qqgaa_interf_W12_nfaa2Ncm1();
}

template <class T>
void aajAmp<T>::qqgaa_W12_nfaa2nf(T& res){
  res = T_Interf.qqgaa_interf_W12_nfaa2nf();
}

template <>
void aajAmp<double>::qqgaa_W12_nfaa2Ncp1(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W12_nfaa2Ncp1();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W12_nfaa2Ncp1();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W12_nfaa2Ncp1();
#endif
}

template <>
void aajAmp<double>::qqgaa_W12_nfaa2Ncm1(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W12_nfaa2Ncm1();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W12_nfaa2Ncm1();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W12_nfaa2Ncm1();
#endif
}

template <>
void aajAmp<double>::qqgaa_W12_nfaa2nf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qqgaa_interf_W12_nfaa2nf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qqgaa_interf_W12_nfaa2nf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qqgaa_interf_W12_nfaa2nf();
#endif
}

// --------------------------------------------------------
// q g -> q a a: 1-loop x 2-loop
// --------------------------------------------------------
template <class T>
void aajAmp<T>::qgqaa_W12_nfaa2Ncp1(T& res){
  res = T_Interf.qgqaa_interf_W12_nfaa2Ncp1();
}

template <class T>
void aajAmp<T>::qgqaa_W12_nfaa2Ncm1(T& res){
  res = T_Interf.qgqaa_interf_W12_nfaa2Ncm1();
}

template <class T>
void aajAmp<T>::qgqaa_W12_nfaa2nf(T& res){
  res = T_Interf.qgqaa_interf_W12_nfaa2nf();
}

template <>
void aajAmp<double>::qgqaa_W12_nfaa2Ncp1(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W12_nfaa2Ncp1();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W12_nfaa2Ncp1();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W12_nfaa2Ncp1();
#endif
}

template <>
void aajAmp<double>::qgqaa_W12_nfaa2Ncm1(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W12_nfaa2Ncm1();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W12_nfaa2Ncm1();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W12_nfaa2Ncm1();
#endif
}

template <>
void aajAmp<double>::qgqaa_W12_nfaa2nf(double& res){
#ifdef HAVE_QD
  if(stable_point){
    res = T_Interf.qgqaa_interf_W12_nfaa2nf();
  } else{
    dd_real dd_res;
    dd_res = dd_Interf.qgqaa_interf_W12_nfaa2nf();
    res = to_double(dd_res);
  }
#else
    res = T_Interf.qgqaa_interf_W12_nfaa2nf();
#endif
}
#endif /* LOOPSQ_ENABLED */

template class aajAmp<double>;
#ifdef HAVE_QD
template class aajAmp<dd_real>;
#endif
