# aajamp #
*Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel, Lorenzo Tancredi*

`aajamp` is a **C++** program for the computation of one- and two-loop QCD corrections to di-photon production in association with an extra jet based on [2102.01820][1].

If you use `aajamp` in your research work, please cite [2102.01820][1] along with its external dependency [2009.07803][3].

## External dependencies
In order to install and run `aajamp` the following package must be available:

+ [PentagonFunctions++][2] is a C++ library by D. Chicherin and V. Sotnikov for fast numerical evaluation of pentagon functions based on [2009.07803][3]. You need to install this before proceeding with `aajamp`. 

`aajamp` operates by default in double precision. Although optional, we strongly recommend the user  to allow for quadruple precision evaluations as well, and this can achieved by installing:

+ [QD][4] is a library by D. Bailey, Y. Hida and X. Li based on [QD-Arithmetic][5]. After downloading the code, please consider configuring QD with `--enable-shared` in order to create the dynamic libraries. 

If quadruple precision is required within `aajamp`, you need first to install `PentagonFunctions++` with `QD` enabled. After installation, make sure that the `QD` package config file qd.pc, which is inside the `QD` main folder, is visible to your `PKG_CONFIG_PATH` environment variable.


## Installation

The minimal installation of  `aajamp`, e.g. in the folder **$HOME/mytools**, goes as follows:

    mkdir build && cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/mytools -DPENTAGONFUNCTIONS_INSTALL_PATH=PF_PATH
    make
    make install

where `PF_PATH` is the installation path of  `PentagonFunctions++`. If quadruple precision is needed you can add the following in configuration phase

    -DUSE_QD=ON -DQD_INSTALL_PATH=QD_PATH
   
where `QD_PATH` is the installation path of  the `QD` library.  If you want to uninstall `aajamp` just run the following inside the build folder

    make uninstall

If a local installation is desired instead, simply follow

    mkdir build && cd build
    cmake .. -DPENTAGONFUNCTIONS_INSTALL_PATH=PF_PATH
    make
    

## Usage

`aajamp` provides the tree-one-loop and tree-two-loop interference terms contributing to the squared matrix element for di-photon production plus jet in quark-antiquark and quark-gluon scattering. Its natural use thus is within a Monte Carlo program for the evaluation of cross sections. We thus recommend to install `aajamp` in `Release` mode and link it to your program.

The usage of `aajamp` is best described through the demos in the `examples` folder. We provide three files: one for a one-loop calculation, one for a two-loop computation and a further one illustrating the impact and usage of the precision control system described in Section 7 of [2102.01820][1].

A brief description follows.
You first need to declare an object of class aajAmp

    aajAmp<double> amp;

You then need to register the process(es) you are interested in and the corresponding loop order

    amp.register_process(id);
    amp.register_loop_order(n);

where `id=1,2` for the $`q\bar{q} \to g \gamma \gamma`$ and $`q g \to q \gamma \gamma`$ channels respectively and `n=0,1,2` for tree-level squared, tree-one-loop and tree-two-loop interference. After defining an event through the set of indipendent invariants $`\left\lbrace s_{12}, s_{23}, s_{34}, s_{45}, s_{51}\right\rbrace`$, you need to register the event as follows.

    double musq = pow(91.1876,2.);
    std::array<double,5> kinematics;
    kinematics={s12, s23, s34, s45, s51};
    amp.register_event(kinematics,musq);

For the assignment of the kinematics see Section 2 of [2102.01820][1]. At this point you are ready to extract values for the interference terms. Suppose you have registered with `id=2` and `n=2` you can obtain the tree-level squared, the one-loop $`C_A`$ coefficient and the two-loop leading colour one with

    double born, oneloop_CA, twoloop_LC;
    amp.qgqaa_W00(born);
    amp.qgqaa_W01_CA(oneloop_CA);
    amp.qgqaa_W02_LC(twoloop_LC);

The complete list of available options for the $`q\bar{q} \to g \gamma \gamma`$ channel is

    amp.qqgaa_W00(double&);
    amp.qqgaa_W01_CA(double&);
    amp.qqgaa_W01_CF(double&);
    amp.qqgaa_W01_nfaa(double&);
    amp.qqgaa_W01_nf(double&);
    amp.qqgaa_W02_LC(double&);
    amp.qqgaa_W02_nfCA(double&);
    amp.qqgaa_W02_nfCF(double&);
    amp.qqgaa_W02_nfnfaa(double&);
    amp.qqgaa_W02_nfnf(double&);

and by replacing `qqgaa_` with `qgqaa_` one can obtain the equivalent for the $`qg`$ channel. For details about the definitions of the various colour factors see Section 3 of [2102.01820][1]. Please note that the functions listed above return the real part of the interference terms defined in [2102.01820][1].


[1]: https://arxiv.org/abs/2102.01820 "2102.01820"
[2]: https://gitlab.com/pentagon-functions/PentagonFunctions-cpp "PentagonFunctions"
[3]: https://arxiv.org/abs/2009.07803 "2009.07803"
[4]: https://www.davidhbailey.com/dhbsoftware
[5]: https://www.davidhbailey.com/dhbpapers/quad-double.pdf
