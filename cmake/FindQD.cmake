# Tries to find an installation of qd:
#   https://www.davidhbailey.com/dhbsoftware/
#
# Usage of this module as follows:
#
#     find_package(QD)
#
# Variables used by this module, they can change the default behaviour and need
# to be set before calling find_package:
#
# QD_INSTALL_PATH  Set this variable to the installation path of
#                  QD if the module has problems
#                  finding the proper installation path.
#
# Variables defined by this module:
#
#  QD_FOUND         System has found QD libs/headers
#  QD_LIBRARIES     The QD library 
#  QD_INCLUDE_DIR   The location of QD headers

find_library(QDLIBFILE
  NAMES qd
  HINTS ${QD_INSTALL_PATH}/lib ${QD_INSTALL_PATH}/lib/x86_64-linux-gnu)

find_path(QD_INCLUDE_DIR
  NAMES qd/dd_real.h
  HINTS ${QD_INSTALL_PATH}/include)

set(QD_LIBRARIES ${QDLIBFILE})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  QD
  DEFAULT_MSG
  QD_LIBRARIES
  QD_INCLUDE_DIR)

mark_as_advanced(
  QD_INSTALL_PATH
  QDLIBFILE
  QD_LIBRARIES
  QD_INCLUDE_DIR)
