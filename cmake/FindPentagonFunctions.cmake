# Tries to find an installation of PentagonFunctions-cpp:
#   https://gitlab.com/pentagon-functions/PentagonFunctions-cpp
#
# Usage of this module as follows:
#
#     find_package(PentagonFunctions)
#
# Variables used by this module, they can change the default behaviour and need
# to be set before calling find_package:
#
# PENTAGONFUNCTIONS_INSTALL_PATH  Set this variable to the installation path of
#                                 PentagonFunctions-cpp if the module has problems
#                                 finding the proper installation path.
#
# Variables defined by this module:
#
#  PENTAGONFUNCTIONS_FOUND         System has found PentagonFunctions libs/headers
#  PENTAGONFUNCTIONS_LIBRARIES     The PentagonFunctions library 
#  PENTAGONFUNCTIONS_INCLUDE_DIR   The location of PentagonFunctions headers

find_library(PENTAGONFUNCTIONSLIBFILE
  NAMES PentagonFunctions
  HINTS ${PENTAGONFUNCTIONS_INSTALL_PATH}/lib ${PENTAGONFUNCTIONS_INSTALL_PATH}/lib/x86_64-linux-gnu)

find_library(LI2LIBFILE
  NAMES Li2++
  HINTS  ${PENTAGONFUNCTIONS_INSTALL_PATH}/lib ${PENTAGONFUNCTIONS_INSTALL_PATH}/lib/x86_64-linux-gnu)

find_path(PENTAGONFUNCTIONS_INCLUDE_DIR
  NAMES PentagonFunctions/FunctionID.h
  HINTS ${PENTAGONFUNCTIONS_INSTALL_PATH}/include)

set(PENTAGONFUNCTIONS_LIBRARIES ${PENTAGONFUNCTIONSLIBFILE} ${LI2LIBFILE})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  PentagonFunctions
  DEFAULT_MSG
  PENTAGONFUNCTIONS_LIBRARIES
  PENTAGONFUNCTIONS_INCLUDE_DIR)

mark_as_advanced(
  PENTAGONFUNCTIONS_INSTALL_PATH
  PENTAGONFUNCTIONSLIBFILE
  PENTAGONFUNCTIONS_LIBRARIES
  PENTAGONFUNCTIONS_INCLUDE_DIR)
