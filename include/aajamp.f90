MODULE aajamp
  use, intrinsic :: iso_c_binding, only : c_int, c_double, c_ptr, c_null_ptr, c_associated
  implicit none
  private

  real(8), parameter :: pi = 3.1415926535897932384626433832795d0
  real(8), parameter :: fourpi = 4*pi
  real(8), parameter :: fourpi3 = fourpi**3

  real(8), parameter :: nup = 2.d0, ndn = 3.d0
  real(8), parameter :: nf = nup + ndn
  real(8), parameter :: Qup = 2.d0/3.d0, Qdn=-1.d0/3.d0
  real(8), parameter :: Qup2 = 4.d0/9.d0, Qdn2=1.d0/9.d0
  real(8), parameter :: Qup4 = Qup2**2, Qdn4=Qdn2**2
  real(8), parameter :: Nc = 3.d0, Nc2 = 9.d0, xg = 8.d0 
  real(8), parameter :: CA = 3.d0, CF = 4.d0/3.d0
  real(8), parameter :: nfaaU = (Qup2*nup + Qdn2*ndn)/Qup2
  real(8), parameter :: nfaaD = (Qup2*nup + Qdn2*ndn)/Qdn2
  real(8), parameter :: nfaU  = (Qup*nup + Qdn*ndn)/Qup2
  real(8), parameter :: nfaD  = (Qup*nup + Qdn*ndn)/Qdn2
  real(8), parameter :: avgqqb = 1.d0/4/Nc2, avgqg = 1.d0/4/Nc/xg
  
  interface

     function aajamp_init() result(optr) bind(C, name="aajamp_init")
       import :: c_ptr
       implicit none
       type(c_ptr) :: optr
     end function aajamp_init

     subroutine aajamp_destroy(optr) bind(C, name="aajamp_destroy")
       import :: c_ptr
       implicit none
       type(c_ptr), intent(in), value :: optr
     end subroutine aajamp_destroy

     subroutine aajamp_register_process(optr, ival) bind(C, name="aajamp_register_process")
       import :: c_int, c_ptr
       implicit none
       type(c_ptr), intent(in), value :: optr
       integer(c_int), intent(in), value :: ival
     end subroutine aajamp_register_process

     subroutine aajamp_register_loop_order(optr, ival) bind(C, name="aajamp_register_loop_order")
       import :: c_int, c_ptr
       implicit none
       type(c_ptr), intent(in), value :: optr
       integer(c_int), intent(in), value :: ival
     end subroutine aajamp_register_loop_order

     subroutine aajamp_register_event(optr, sij, musq) bind(C, name="aajamp_register_event")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(in) :: sij(5)
       real(c_double), intent(in) :: musq
     end subroutine aajamp_register_event

     !-- Tree-level: qqb
     !------------------------------------------------------------------------!
     subroutine aajamp_qqgaa_W00(optr, qqgaa_W00) bind(C, name="aajamp_qqgaa_W00")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W00
     end subroutine aajamp_qqgaa_W00

     !-- Tree-level: qg
     !------------------------------------------------------------------------!
     subroutine aajamp_qgqaa_W00(optr, qgqaa_W00) bind(C, name="aajamp_qgqaa_W00")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W00
     end subroutine aajamp_qgqaa_W00

     !-- One-loop: qqb
     !------------------------------------------------------------------------!
     subroutine aajamp_qqgaa_W01_CA(optr, qqgaa_W01) bind(C, name="aajamp_qqgaa_W01_CA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W01
     end subroutine aajamp_qqgaa_W01_CA

     subroutine aajamp_qqgaa_W01_CF(optr, qqgaa_W01) bind(C, name="aajamp_qqgaa_W01_CF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W01
     end subroutine aajamp_qqgaa_W01_CF

     subroutine aajamp_qqgaa_W01_nfaa(optr, qqgaa_W01) bind(C, name="aajamp_qqgaa_W01_nfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W01
     end subroutine aajamp_qqgaa_W01_nfaa

     subroutine aajamp_qqgaa_W01_nf(optr, qqgaa_W01) bind(C, name="aajamp_qqgaa_W01_nf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W01
     end subroutine aajamp_qqgaa_W01_nf

     !-- One-loop: qg
     !------------------------------------------------------------------------!
     subroutine aajamp_qgqaa_W01_CA(optr, qgqaa_W01) bind(C, name="aajamp_qgqaa_W01_CA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W01
     end subroutine aajamp_qgqaa_W01_CA

     subroutine aajamp_qgqaa_W01_CF(optr, qgqaa_W01) bind(C, name="aajamp_qgqaa_W01_CF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W01
     end subroutine aajamp_qgqaa_W01_CF

     subroutine aajamp_qgqaa_W01_nfaa(optr, qgqaa_W01) bind(C, name="aajamp_qgqaa_W01_nfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W01
     end subroutine aajamp_qgqaa_W01_nfaa

     subroutine aajamp_qgqaa_W01_nf(optr, qgqaa_W01) bind(C, name="aajamp_qgqaa_W01_nf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W01
     end subroutine aajamp_qgqaa_W01_nf

     !-- One-loop^2: qqb
     !------------------------------------------------------------------------!
     subroutine aajamp_qqgaa_W11_CACA(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_CACA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_CACA

     subroutine aajamp_qqgaa_W11_CFCF(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_CFCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_CFCF

     subroutine aajamp_qqgaa_W11_nfaanfaa(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_nfaanfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_nfaanfaa

     subroutine aajamp_qqgaa_W11_nfnf(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_nfnf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_nfnf

     subroutine aajamp_qqgaa_W11_CACF(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_CACF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_CACF

     subroutine aajamp_qqgaa_W11_nfaaCA(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_nfaaCA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_nfaaCA

     subroutine aajamp_qqgaa_W11_nfCA(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_nfCA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_nfCA

     subroutine aajamp_qqgaa_W11_nfaaCF(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_nfaaCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_nfaaCF

     subroutine aajamp_qqgaa_W11_nfCF(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_nfCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_nfCF

     subroutine aajamp_qqgaa_W11_nfnfaa(optr, qqgaa_W11) bind(C, name="aajamp_qqgaa_W11_nfnfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W11
     end subroutine aajamp_qqgaa_W11_nfnfaa

     !-- One-loop^2: qg
     !------------------------------------------------------------------------!
     subroutine aajamp_qgqaa_W11_CACA(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_CACA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_CACA

     subroutine aajamp_qgqaa_W11_CFCF(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_CFCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_CFCF

     subroutine aajamp_qgqaa_W11_nfaanfaa(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_nfaanfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_nfaanfaa

     subroutine aajamp_qgqaa_W11_nfnf(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_nfnf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_nfnf

     subroutine aajamp_qgqaa_W11_CACF(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_CACF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_CACF

     subroutine aajamp_qgqaa_W11_nfaaCA(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_nfaaCA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_nfaaCA

     subroutine aajamp_qgqaa_W11_nfCA(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_nfCA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_nfCA

     subroutine aajamp_qgqaa_W11_nfaaCF(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_nfaaCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_nfaaCF

     subroutine aajamp_qgqaa_W11_nfCF(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_nfCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_nfCF

     subroutine aajamp_qgqaa_W11_nfnfaa(optr, qgqaa_W11) bind(C, name="aajamp_qgqaa_W11_nfnfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W11
     end subroutine aajamp_qgqaa_W11_nfnfaa

     !-- Two-loop: qqb
     !------------------------------------------------------------------------!
     subroutine aajamp_qqgaa_W02_Nc2(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_Nc2")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_Nc2

     subroutine aajamp_qqgaa_W02_Nc0(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_Nc0")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_Nc0

     subroutine aajamp_qqgaa_W02_Ncm2(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_Ncm2")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_Ncm2

     subroutine aajamp_qqgaa_W02_nfCA(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_nfCA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_nfCA

     subroutine aajamp_qqgaa_W02_nfCF(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_nfCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_nfCF

     subroutine aajamp_qqgaa_W02_nfnfaa(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_nfnfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_nfnfaa

     subroutine aajamp_qqgaa_W02_nfnf(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_nfnf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_nfnf

     subroutine aajamp_qqgaa_W02_nfaaNc(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_nfaaNc")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_nfaaNc

     subroutine aajamp_qqgaa_W02_nfaaNcm1(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_nfaaNcm1")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_nfaaNcm1

     subroutine aajamp_qqgaa_W02_nfa(optr, qqgaa_W02) bind(C, name="aajamp_qqgaa_W02_nfa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qqgaa_W02_nfa

     !-- Two-loop: qg
     !------------------------------------------------------------------------!
     subroutine aajamp_qgqaa_W02_Nc2(optr, qgqaa_W02) bind(C, name="aajamp_qgqaa_W02_Nc2")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W02
     end subroutine aajamp_qgqaa_W02_Nc2

     subroutine aajamp_qgqaa_W02_Nc0(optr, qgqaa_W02) bind(C, name="aajamp_qgqaa_W02_Nc0")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W02
     end subroutine aajamp_qgqaa_W02_Nc0

     subroutine aajamp_qgqaa_W02_Ncm2(optr, qgqaa_W02) bind(C, name="aajamp_qgqaa_W02_Ncm2")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W02
     end subroutine aajamp_qgqaa_W02_Ncm2

     subroutine aajamp_qgqaa_W02_nfCA(optr, qgqaa_W02) bind(C, name="aajamp_qgqaa_W02_nfCA")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W02
     end subroutine aajamp_qgqaa_W02_nfCA

     subroutine aajamp_qgqaa_W02_nfCF(optr, qgqaa_W02) bind(C, name="aajamp_qgqaa_W02_nfCF")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W02
     end subroutine aajamp_qgqaa_W02_nfCF

     subroutine aajamp_qgqaa_W02_nfnfaa(optr, qgqaa_W02) bind(C, name="aajamp_qgqaa_W02_nfnfaa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W02
     end subroutine aajamp_qgqaa_W02_nfnfaa

     subroutine aajamp_qgqaa_W02_nfnf(optr, qgqaa_W02) bind(C, name="aajamp_qgqaa_W02_nfnf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W02
     end subroutine aajamp_qgqaa_W02_nfnf

     subroutine aajamp_qgqaa_W02_nfaaNc(optr, qqgaa_W02) bind(C, name="aajamp_qgqaa_W02_nfaaNc")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qgqaa_W02_nfaaNc

     subroutine aajamp_qgqaa_W02_nfaaNcm1(optr, qqgaa_W02) bind(C, name="aajamp_qgqaa_W02_nfaaNcm1")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qgqaa_W02_nfaaNcm1

     subroutine aajamp_qgqaa_W02_nfa(optr, qqgaa_W02) bind(C, name="aajamp_qgqaa_W02_nfa")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W02
     end subroutine aajamp_qgqaa_W02_nfa

     !-- one x two-loop: qqb
     !------------------------------------------------------------------------!
     subroutine aajamp_qqgaa_W12_nfaa2Ncp1(optr, qqgaa_W12) bind(C, name="aajamp_qqgaa_W12_nfaa2Ncp1")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W12
     end subroutine aajamp_qqgaa_W12_nfaa2Ncp1

     subroutine aajamp_qqgaa_W12_nfaa2Ncm1(optr, qqgaa_W12) bind(C, name="aajamp_qqgaa_W12_nfaa2Ncm1")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W12
     end subroutine aajamp_qqgaa_W12_nfaa2Ncm1

     subroutine aajamp_qqgaa_W12_nfaa2nf(optr, qqgaa_W12) bind(C, name="aajamp_qqgaa_W12_nfaa2nf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qqgaa_W12
     end subroutine aajamp_qqgaa_W12_nfaa2nf

     !-- one x two-loop: qg
     !------------------------------------------------------------------------!
     subroutine aajamp_qgqaa_W12_nfaa2Ncp1(optr, qgqaa_W12) bind(C, name="aajamp_qgqaa_W12_nfaa2Ncp1")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W12
     end subroutine aajamp_qgqaa_W12_nfaa2Ncp1

     subroutine aajamp_qgqaa_W12_nfaa2Ncm1(optr, qgqaa_W12) bind(C, name="aajamp_qgqaa_W12_nfaa2Ncm1")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W12
     end subroutine aajamp_qgqaa_W12_nfaa2Ncm1

     subroutine aajamp_qgqaa_W12_nfaa2nf(optr, qgqaa_W12) bind(C, name="aajamp_qgqaa_W12_nfaa2nf")
       import :: c_ptr, c_double
       implicit none
       type(c_ptr), intent(in), value :: optr
       real(c_double), intent(out) :: qgqaa_W12
     end subroutine aajamp_qgqaa_W12_nfaa2nf

  end interface

  type(c_ptr), save :: aaj_obj = c_null_ptr

  !---------------------------------------------------------------------------!
  !                             Public interfaces                             !
  !---------------------------------------------------------------------------!
  public :: AAJ_init, AAJ_destroy
  public :: AAJ_register_process
  public :: AAJ_register_loop_order
  public :: AAJ_register_event
  !-- Tree-level: qqb & qg
  public :: AAJ_qqgaa_W00, AAJ_qgqaa_W00
  !-- One-loop: qqb
  public :: AAJ_qqgaa_W01_CA
  public :: AAJ_qqgaa_W01_CF
  public :: AAJ_qqgaa_W01_nfaa
  public :: AAJ_qqgaa_W01_nf
  public :: AAJ_qqgaa_W01_Ncp1
  public :: AAJ_qqgaa_W01_Ncm1
  public :: AAJ_qqgaa_W01
  !-- One-loop: qg
  public :: AAJ_qgqaa_W01_CA
  public :: AAJ_qgqaa_W01_CF
  public :: AAJ_qgqaa_W01_nfaa
  public :: AAJ_qgqaa_W01_nf
  public :: AAJ_qgqaa_W01_Ncp1
  public :: AAJ_qgqaa_W01_Ncm1
  public :: AAJ_qgqaa_W01
  !-- One-loop^2: qqb
  public :: AAJ_qqgaa_W11_CACA
  public :: AAJ_qqgaa_W11_CFCF
  public :: AAJ_qqgaa_W11_nfaanfaa
  public :: AAJ_qqgaa_W11_nfnf
  public :: AAJ_qqgaa_W11_CACF
  public :: AAJ_qqgaa_W11_nfaaCA
  public :: AAJ_qqgaa_W11_nfCA
  public :: AAJ_qqgaa_W11_nfaaCF
  public :: AAJ_qqgaa_W11_nfCF
  public :: AAJ_qqgaa_W11_nfnfaa
  public :: AAJ_qqgaa_W11_Ncp2
  public :: AAJ_qqgaa_W11_Nc0
  public :: AAJ_qqgaa_W11_Ncm2
  public :: AAJ_qqgaa_W11_nfaaNcp1
  public :: AAJ_qqgaa_W11_nfaaNcm1
  public :: AAJ_qqgaa_W11_nfNcp1
  public :: AAJ_qqgaa_W11_nfNcm1
  public :: AAJ_qqgaa_W11
  !-- One-loop^2: qg
  public :: AAJ_qgqaa_W11_CACA
  public :: AAJ_qgqaa_W11_CFCF
  public :: AAJ_qgqaa_W11_nfaanfaa
  public :: AAJ_qgqaa_W11_nfnf
  public :: AAJ_qgqaa_W11_CACF
  public :: AAJ_qgqaa_W11_nfaaCA
  public :: AAJ_qgqaa_W11_nfCA
  public :: AAJ_qgqaa_W11_nfaaCF
  public :: AAJ_qgqaa_W11_nfCF
  public :: AAJ_qgqaa_W11_nfnfaa
  public :: AAJ_qgqaa_W11_Ncp2
  public :: AAJ_qgqaa_W11_Nc0
  public :: AAJ_qgqaa_W11_Ncm2
  public :: AAJ_qgqaa_W11_nfaaNcp1
  public :: AAJ_qgqaa_W11_nfaaNcm1
  public :: AAJ_qgqaa_W11_nfNcp1
  public :: AAJ_qgqaa_W11_nfNcm1
  public :: AAJ_qgqaa_W11
  !-- Two-loop: qqb
  public :: AAJ_qqgaa_W02_nfCA
  public :: AAJ_qqgaa_W02_nfCF
  public :: AAJ_qqgaa_W02_Ncp2
  public :: AAJ_qqgaa_W02_Nc0
  public :: AAJ_qqgaa_W02_Ncm2
  public :: AAJ_qqgaa_W02_nfNcp1
  public :: AAJ_qqgaa_W02_nfNcm1
  public :: AAJ_qqgaa_W02_nfnf
  public :: AAJ_qqgaa_W02_nfnfaa
  public :: AAJ_qqgaa_W02_nfaaNcp1
  public :: AAJ_qqgaa_W02_nfaaNcm1
  public :: AAJ_qqgaa_W02_nfa
  public :: AAJ_qqgaa_W02_LC
  public :: AAJ_qqgaa_W02
  !-- Two-loop: qg
  public :: AAJ_qgqaa_W02_nfCA
  public :: AAJ_qgqaa_W02_nfCF
  public :: AAJ_qgqaa_W02_Ncp2
  public :: AAJ_qgqaa_W02_Nc0
  public :: AAJ_qgqaa_W02_Ncm2
  public :: AAJ_qgqaa_W02_nfNcp1
  public :: AAJ_qgqaa_W02_nfNcm1
  public :: AAJ_qgqaa_W02_nfnf
  public :: AAJ_qgqaa_W02_nfnfaa
  public :: AAJ_qgqaa_W02_nfaaNcp1
  public :: AAJ_qgqaa_W02_nfaaNcm1
  public :: AAJ_qgqaa_W02_nfa
  public :: AAJ_qgqaa_W02_LC
  public :: AAJ_qgqaa_W02
  !-- one x two-loop: qqb
  public :: AAJ_qqgaa_W12_nfaa2Ncp1
  public :: AAJ_qqgaa_W12_nfaa2Ncm1
  public :: AAJ_qqgaa_W12_nfaa2nf
  !-- one x two-loop: qg
  public :: AAJ_qgqaa_W12_nfaa2Ncp1
  public :: AAJ_qgqaa_W12_nfaa2Ncm1
  public :: AAJ_qgqaa_W12_nfaa2nf

contains

  subroutine AAJ_init()
    if(c_associated(aaj_obj)) then
       call aajamp_destroy(aaj_obj)
    endif
    aaj_obj = aajamp_init()
    return
  end subroutine AAJ_init

  subroutine AAJ_destroy()
    if(c_associated(aaj_obj)) then
       call aajamp_destroy(aaj_obj)
    endif
    return
  end subroutine AAJ_destroy

  subroutine AAJ_register_process(idproc)
    implicit none
    integer(c_int), intent(in) :: idproc
    if(c_associated(aaj_obj)) then
       call aajamp_register_process(aaj_obj, idproc)
    endif
    return
  end subroutine AAJ_register_process

  subroutine AAJ_register_loop_order(ord)
    implicit none
    integer(c_int), intent(in) :: ord
    if(c_associated(aaj_obj)) then
       call aajamp_register_loop_order(aaj_obj, ord)
    endif
    return
  end subroutine AAJ_register_loop_order

  subroutine AAJ_register_event(sij,musq)
    implicit none
    real(c_double), intent(in) :: sij(5)
    real(c_double), intent(in), value :: musq
    if(c_associated(aaj_obj)) then
       call aajamp_register_event(aaj_obj, sij, musq)
    endif
    return
  end subroutine AAJ_register_event

  !---------------------------------------------------------------------------!
  !                              Tree-level: qqb                              !
  !---------------------------------------------------------------------------!

  subroutine AAJ_qqgaa_W00(alpha,alphas,qqgaa_W00)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qqgaa_W00(2)
    real(c_double) :: born
    real(8) :: LOF

    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W00(aaj_obj, born)
    endif

    LOF = xg * fourpi3 * alpha**2 * alphas

    qqgaa_W00(1) = born * Qdn4
    qqgaa_W00(2) = born * Qup4

    qqgaa_W00 = qqgaa_W00 * LOF * avgqqb

    return
  end subroutine AAJ_qqgaa_W00

  !---------------------------------------------------------------------------!
  !                              Tree-level: qg                               !
  !---------------------------------------------------------------------------!

  subroutine AAJ_qgqaa_W00(alpha,alphas,qgqaa_W00)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qgqaa_W00(2)
    real(c_double) :: born
    real(8) :: LOF

    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W00(aaj_obj, born)
    endif

    LOF = xg * fourpi3 * alpha**2 * alphas

    qgqaa_W00(1) = born * Qdn4
    qgqaa_W00(2) = born * Qup4

    qgqaa_W00 = qgqaa_W00 * LOF * avgqg

    return
  end subroutine AAJ_qgqaa_W00

  !---------------------------------------------------------------------------!
  !                               One-loop: qqb                               !
  !---------------------------------------------------------------------------!

  subroutine AAJ_qqgaa_W01_CA(qqgaa_W01)
    implicit none
    real(c_double), intent(out) :: qqgaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W01_CA(aaj_obj, qqgaa_W01)
    endif
    return
  end subroutine AAJ_qqgaa_W01_CA

  subroutine AAJ_qqgaa_W01_CF(qqgaa_W01)
    implicit none
    real(c_double), intent(out) :: qqgaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W01_CF(aaj_obj, qqgaa_W01)
    endif
    return
  end subroutine AAJ_qqgaa_W01_CF

  subroutine AAJ_qqgaa_W01_nfaa(qqgaa_W01)
    implicit none
    real(c_double), intent(out) :: qqgaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W01_nfaa(aaj_obj, qqgaa_W01)
    endif
    return
  end subroutine AAJ_qqgaa_W01_nfaa

  subroutine AAJ_qqgaa_W01_nf(qqgaa_W01)
    implicit none
    real(c_double), intent(out) :: qqgaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W01_nf(aaj_obj, qqgaa_W01)
    endif
    return
  end subroutine AAJ_qqgaa_W01_nf

  subroutine AAJ_qqgaa_W01_Ncp1(qqgaa_W01)
    implicit none
    real(8), intent(out) :: qqgaa_W01
    real(c_double) :: qqgaa_W01_CA, qqgaa_W01_CF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W01_CA(aaj_obj, qqgaa_W01_CA)
       call aajamp_qqgaa_W01_CF(aaj_obj, qqgaa_W01_CF)
    endif
    qqgaa_W01 = qqgaa_W01_CA + qqgaa_W01_CF/2
    return
  end subroutine AAJ_qqgaa_W01_Ncp1

  subroutine AAJ_qqgaa_W01_Ncm1(qqgaa_W01)
    implicit none
    real(c_double), intent(out) :: qqgaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W01_CF(aaj_obj, qqgaa_W01)
    endif
    qqgaa_W01 = -qqgaa_W01/2
    return
  end subroutine AAJ_qqgaa_W01_Ncm1

  subroutine AAJ_qqgaa_W01(alpha,alphas,qqgaa_W01)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qqgaa_W01(2)
    real(8) :: qq_W01_CA,qq_W01_CF,qq_W01_nfaa,qq_W01_nf
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qqgaa_W01_CA(qq_W01_CA)
    call AAJ_qqgaa_W01_CF(qq_W01_CF)
    call AAJ_qqgaa_W01_nfaa(qq_W01_nfaa)
    call AAJ_qqgaa_W01_nf(qq_W01_nf)

    qqgaa_W01(1) = CA * qq_W01_CA    &
                 + CF * qq_W01_CF    &
                 + nfaaD*qq_W01_nfaa &
                 + nf * qq_W01_nf
    qqgaa_W01(1) = qqgaa_W01(1) * Qdn4

    qqgaa_W01(2) = CA * qq_W01_CA    &
                 + CF * qq_W01_CF    &
                 + nfaaU*qq_W01_nfaa &
                 + nf * qq_W01_nf
    qqgaa_W01(2) = qqgaa_W01(2) * Qup4

    !-- Factor 2 for 2 X Re[...]
    qqgaa_W01 = 2 * qqgaa_W01 * LOF * avgqqb

    return
  end subroutine AAJ_qqgaa_W01

  !---------------------------------------------------------------------------!
  !                               One-loop: qg                                !
  !---------------------------------------------------------------------------!

  subroutine AAJ_qgqaa_W01_CA(qgqaa_W01)
    implicit none
    real(c_double), intent(out) :: qgqaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W01_CA(aaj_obj, qgqaa_W01)
    endif
    return
  end subroutine AAJ_qgqaa_W01_CA

  subroutine AAJ_qgqaa_W01_CF(qgqaa_W01)
    implicit none
    real(c_double), intent(out) :: qgqaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W01_CF(aaj_obj, qgqaa_W01)
    endif
    return
  end subroutine AAJ_qgqaa_W01_CF

  subroutine AAJ_qgqaa_W01_nfaa(qgqaa_W01)
    implicit none
    real(c_double), intent(out) :: qgqaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W01_nfaa(aaj_obj, qgqaa_W01)
    endif
    return
  end subroutine AAJ_qgqaa_W01_nfaa

  subroutine AAJ_qgqaa_W01_nf(qgqaa_W01)
    implicit none
    real(c_double), intent(out) :: qgqaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W01_nf(aaj_obj, qgqaa_W01)
    endif
    return
  end subroutine AAJ_qgqaa_W01_nf

  subroutine AAJ_qgqaa_W01_Ncp1(qgqaa_W01)
    implicit none
    real(8), intent(out) :: qgqaa_W01
    real(c_double) :: qgqaa_W01_CA, qgqaa_W01_CF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W01_CA(aaj_obj, qgqaa_W01_CA)
       call aajamp_qgqaa_W01_CF(aaj_obj, qgqaa_W01_CF)
    endif
    qgqaa_W01 = qgqaa_W01_CA + qgqaa_W01_CF/2
    return
  end subroutine AAJ_qgqaa_W01_Ncp1

  subroutine AAJ_qgqaa_W01_Ncm1(qgqaa_W01)
    implicit none
    real(c_double), intent(out) :: qgqaa_W01
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W01_CF(aaj_obj, qgqaa_W01)
    endif
    qgqaa_W01 = -qgqaa_W01/2
    return
  end subroutine AAJ_qgqaa_W01_Ncm1

  subroutine AAJ_qgqaa_W01(alpha,alphas,qgqaa_W01)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qgqaa_W01(2)
    real(8) :: qq_W01_CA,qq_W01_CF,qq_W01_nfaa,qq_W01_nf
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qgqaa_W01_CA(qq_W01_CA)
    call AAJ_qgqaa_W01_CF(qq_W01_CF)
    call AAJ_qgqaa_W01_nfaa(qq_W01_nfaa)
    call AAJ_qgqaa_W01_nf(qq_W01_nf)

    qgqaa_W01(1) = CA * qq_W01_CA    &
                 + CF * qq_W01_CF    &
                 + nfaaD*qq_W01_nfaa &
                 + nf * qq_W01_nf
    qgqaa_W01(1) = qgqaa_W01(1) * Qdn4

    qgqaa_W01(2) = CA * qq_W01_CA    &
                 + CF * qq_W01_CF    &
                 + nfaaU*qq_W01_nfaa &
                 + nf * qq_W01_nf
    qgqaa_W01(2) = qgqaa_W01(2) * Qup4

    !-- Factor 2 for 2 X Re[...]
    qgqaa_W01 = 2 * qgqaa_W01 * LOF * avgqg

    return
  end subroutine AAJ_qgqaa_W01

  !---------------------------------------------------------------------------!
  !                              One-loop^2: qqb                              !
  !---------------------------------------------------------------------------!
  subroutine AAJ_qqgaa_W11_CACA(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_CACA(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_CACA

  subroutine AAJ_qqgaa_W11_CFCF(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_CFCF(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_CFCF

  subroutine AAJ_qqgaa_W11_nfaanfaa(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfaanfaa(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_nfaanfaa

  subroutine AAJ_qqgaa_W11_nfnf(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfnf(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_nfnf

  subroutine AAJ_qqgaa_W11_CACF(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_CACF(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_CACF

  subroutine AAJ_qqgaa_W11_nfaaCA(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfaaCA(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_nfaaCA

  subroutine AAJ_qqgaa_W11_nfCA(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfCA(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_nfCA

  subroutine AAJ_qqgaa_W11_nfaaCF(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfaaCF(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_nfaaCF

  subroutine AAJ_qqgaa_W11_nfCF(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfCF(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_nfCF

  subroutine AAJ_qqgaa_W11_nfnfaa(qqgaa_W11)
    implicit none
    real(c_double), intent(out) :: qqgaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfnfaa(aaj_obj, qqgaa_W11)
    endif
    return
  end subroutine AAJ_qqgaa_W11_nfnfaa

  subroutine AAJ_qqgaa_W11_Ncp2(qqgaa_W11)
    implicit none
    real(8), intent(out) :: qqgaa_W11
    real(c_double) :: W11_CACA, W11_CACF, W11_CFCF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_CACA(aaj_obj, W11_CACA)
       call aajamp_qqgaa_W11_CACF(aaj_obj, W11_CACF)
       call aajamp_qqgaa_W11_CFCF(aaj_obj, W11_CFCF)
    endif
    qqgaa_W11 = W11_CACA + W11_CACF/2 + W11_CFCF/4
    return
  end subroutine AAJ_qqgaa_W11_Ncp2

  subroutine AAJ_qqgaa_W11_Nc0(qqgaa_W11)
    implicit none
    real(8), intent(out) :: qqgaa_W11
    real(c_double) :: W11_CACF, W11_CFCF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_CACF(aaj_obj, W11_CACF)
       call aajamp_qqgaa_W11_CFCF(aaj_obj, W11_CFCF)
    endif
    qqgaa_W11 = -W11_CACF/2 - W11_CFCF/2
    return
  end subroutine AAJ_qqgaa_W11_Nc0

  subroutine AAJ_qqgaa_W11_Ncm2(qqgaa_W11)
    implicit none
    real(8), intent(out) :: qqgaa_W11
    real(c_double) :: W11_CFCF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_CFCF(aaj_obj, W11_CFCF)
    endif
    qqgaa_W11 = W11_CFCF/4
    return
  end subroutine AAJ_qqgaa_W11_Ncm2

  subroutine AAJ_qqgaa_W11_nfaaNcp1(qqgaa_W11)
    implicit none
    real(8), intent(out) :: qqgaa_W11
    real(c_double) :: W11_nfaaCA, W11_nfaaCF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfaaCA(aaj_obj, W11_nfaaCA)
       call aajamp_qqgaa_W11_nfaaCF(aaj_obj, W11_nfaaCF)
    endif
    qqgaa_W11 = W11_nfaaCA + W11_nfaaCF/2
    return
  end subroutine AAJ_qqgaa_W11_nfaaNcp1

  subroutine AAJ_qqgaa_W11_nfaaNcm1(qqgaa_W11)
    implicit none
    real(8), intent(out) :: qqgaa_W11
    real(c_double) :: W11_nfaaCF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfaaCF(aaj_obj, W11_nfaaCF)
    endif
    qqgaa_W11 = - W11_nfaaCF/2
    return
  end subroutine AAJ_qqgaa_W11_nfaaNcm1

  subroutine AAJ_qqgaa_W11_nfNcp1(qqgaa_W11)
    implicit none
    real(8), intent(out) :: qqgaa_W11
    real(c_double) :: W11_nfCA, W11_nfCF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfCA(aaj_obj, W11_nfCA)
       call aajamp_qqgaa_W11_nfCF(aaj_obj, W11_nfCF)
    endif
    qqgaa_W11 = W11_nfCA + W11_nfCF/2
    return
  end subroutine AAJ_qqgaa_W11_nfNcp1

  subroutine AAJ_qqgaa_W11_nfNcm1(qqgaa_W11)
    implicit none
    real(8), intent(out) :: qqgaa_W11
    real(c_double) :: W11_nfCF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W11_nfCF(aaj_obj, W11_nfCF)
    endif
    qqgaa_W11 = - W11_nfCF/2
    return
  end subroutine AAJ_qqgaa_W11_nfNcm1

  subroutine AAJ_qqgaa_W11(alpha,alphas,qqgaa_W11)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qqgaa_W11(2)
    real(8):: qq_W11_CA2,qq_W11_CF2,qq_W11_nfaa2,qq_W11_nf2
    real(8):: qq_W11_CACF,qq_W11_nfaaCA,qq_W11_nfCA
    real(8):: qq_W11_nfaaCF,qq_W11_nfCF,qq_W11_nfnfaa
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qqgaa_W11_CACA(qq_W11_CA2)
    call AAJ_qqgaa_W11_CFCF(qq_W11_CF2)
    call AAJ_qqgaa_W11_nfaanfaa(qq_W11_nfaa2)
    call AAJ_qqgaa_W11_nfnf(qq_W11_nf2)
    call AAJ_qqgaa_W11_CACF(qq_W11_CACF)
    call AAJ_qqgaa_W11_nfaaCA(qq_W11_nfaaCA)
    call AAJ_qqgaa_W11_nfCA(qq_W11_nfCA)
    call AAJ_qqgaa_W11_nfaaCF(qq_W11_nfaaCF)
    call AAJ_qqgaa_W11_nfCF(qq_W11_nfCF)
    call AAJ_qqgaa_W11_nfnfaa(qq_W11_nfnfaa)

    qqgaa_W11(1) = CA**2 * qq_W11_CA2       &
                 + CF**2 * qq_W11_CF2       &
                 + nfaaD**2 * qq_W11_nfaa2  &
                 + nf**2 * qq_W11_nf2       &
                 + CA*CF * qq_W11_CACF      &
                 + CA*nfaaD * qq_W11_nfaaCA &
                 + CA*nf * qq_W11_nfCA      &
                 + CF*nfaaD * qq_W11_nfaaCF &
                 + CF*nf * qq_W11_nfCF      &
                 + nfaaD*nf * qq_W11_nfnfaa
    qqgaa_W11(1) = qqgaa_W11(1) * Qdn4

    qqgaa_W11(2) = CA**2 * qq_W11_CA2       &
                 + CF**2 * qq_W11_CF2       &
                 + nfaaU**2 * qq_W11_nfaa2  &
                 + nf**2 * qq_W11_nf2       &
                 + CA*CF * qq_W11_CACF      &
                 + CA*nfaaU * qq_W11_nfaaCA &
                 + CA*nf * qq_W11_nfCA      &
                 + CF*nfaaU * qq_W11_nfaaCF &
                 + CF*nf * qq_W11_nfCF      &
                 + nfaaU*nf * qq_W11_nfnfaa
    qqgaa_W11(2) = qqgaa_W11(2) * Qup4

    !-- Add overall factor and average over initial spin/colours
    qqgaa_W11 = qqgaa_W11 * LOF * avgqqb

    return
  end subroutine AAJ_qqgaa_W11

  !---------------------------------------------------------------------------!
  !                              One-loop^2: qg                              !
  !---------------------------------------------------------------------------!
  subroutine AAJ_qgqaa_W11_CACA(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_CACA(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_CACA

  subroutine AAJ_qgqaa_W11_CFCF(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_CFCF(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_CFCF

  subroutine AAJ_qgqaa_W11_nfaanfaa(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfaanfaa(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_nfaanfaa

  subroutine AAJ_qgqaa_W11_nfnf(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfnf(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_nfnf

  subroutine AAJ_qgqaa_W11_CACF(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_CACF(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_CACF

  subroutine AAJ_qgqaa_W11_nfaaCA(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfaaCA(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_nfaaCA

  subroutine AAJ_qgqaa_W11_nfCA(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfCA(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_nfCA

  subroutine AAJ_qgqaa_W11_nfaaCF(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfaaCF(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_nfaaCF

  subroutine AAJ_qgqaa_W11_nfCF(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfCF(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_nfCF

  subroutine AAJ_qgqaa_W11_nfnfaa(qgqaa_W11)
    implicit none
    real(c_double), intent(out) :: qgqaa_W11
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfnfaa(aaj_obj, qgqaa_W11)
    endif
    return
  end subroutine AAJ_qgqaa_W11_nfnfaa

  subroutine AAJ_qgqaa_W11_Ncp2(qgqaa_W11)
    implicit none
    real(8), intent(out) :: qgqaa_W11
    real(c_double) :: W11_CACA, W11_CACF, W11_CFCF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_CACA(aaj_obj, W11_CACA)
       call aajamp_qgqaa_W11_CACF(aaj_obj, W11_CACF)
       call aajamp_qgqaa_W11_CFCF(aaj_obj, W11_CFCF)
    endif
    qgqaa_W11 = W11_CACA + W11_CACF/2 + W11_CFCF/4
    return
  end subroutine AAJ_qgqaa_W11_Ncp2

  subroutine AAJ_qgqaa_W11_Nc0(qgqaa_W11)
    implicit none
    real(8), intent(out) :: qgqaa_W11
    real(c_double) :: W11_CACF, W11_CFCF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_CACF(aaj_obj, W11_CACF)
       call aajamp_qgqaa_W11_CFCF(aaj_obj, W11_CFCF)
    endif
    qgqaa_W11 = -W11_CACF/2 - W11_CFCF/2
    return
  end subroutine AAJ_qgqaa_W11_Nc0

  subroutine AAJ_qgqaa_W11_Ncm2(qgqaa_W11)
    implicit none
    real(8), intent(out) :: qgqaa_W11
    real(c_double) :: W11_CFCF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_CFCF(aaj_obj, W11_CFCF)
    endif
    qgqaa_W11 = W11_CFCF/4
    return
  end subroutine AAJ_qgqaa_W11_Ncm2

  subroutine AAJ_qgqaa_W11_nfaaNcp1(qgqaa_W11)
    implicit none
    real(8), intent(out) :: qgqaa_W11
    real(c_double) :: W11_nfaaCA, W11_nfaaCF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfaaCA(aaj_obj, W11_nfaaCA)
       call aajamp_qgqaa_W11_nfaaCF(aaj_obj, W11_nfaaCF)
    endif
    qgqaa_W11 = W11_nfaaCA + W11_nfaaCF/2
    return
  end subroutine AAJ_qgqaa_W11_nfaaNcp1

  subroutine AAJ_qgqaa_W11_nfaaNcm1(qgqaa_W11)
    implicit none
    real(8), intent(out) :: qgqaa_W11
    real(c_double) :: W11_nfaaCF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfaaCF(aaj_obj, W11_nfaaCF)
    endif
    qgqaa_W11 = - W11_nfaaCF/2
    return
  end subroutine AAJ_qgqaa_W11_nfaaNcm1

  subroutine AAJ_qgqaa_W11_nfNcp1(qgqaa_W11)
    implicit none
    real(8), intent(out) :: qgqaa_W11
    real(c_double) :: W11_nfCA, W11_nfCF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfCA(aaj_obj, W11_nfCA)
       call aajamp_qgqaa_W11_nfCF(aaj_obj, W11_nfCF)
    endif
    qgqaa_W11 = W11_nfCA + W11_nfCF/2
    return
  end subroutine AAJ_qgqaa_W11_nfNcp1

  subroutine AAJ_qgqaa_W11_nfNcm1(qgqaa_W11)
    implicit none
    real(8), intent(out) :: qgqaa_W11
    real(c_double) :: W11_nfCF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W11_nfCF(aaj_obj, W11_nfCF)
    endif
    qgqaa_W11 = - W11_nfCF/2
    return
  end subroutine AAJ_qgqaa_W11_nfNcm1

  subroutine AAJ_qgqaa_W11(alpha,alphas,qgqaa_W11)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qgqaa_W11(2)
    real(8):: qq_W11_CA2,qq_W11_CF2,qq_W11_nfaa2,qq_W11_nf2
    real(8):: qq_W11_CACF,qq_W11_nfaaCA,qq_W11_nfCA
    real(8):: qq_W11_nfaaCF,qq_W11_nfCF,qq_W11_nfnfaa
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qgqaa_W11_CACA(qq_W11_CA2)
    call AAJ_qgqaa_W11_CFCF(qq_W11_CF2)
    call AAJ_qgqaa_W11_nfaanfaa(qq_W11_nfaa2)
    call AAJ_qgqaa_W11_nfnf(qq_W11_nf2)
    call AAJ_qgqaa_W11_CACF(qq_W11_CACF)
    call AAJ_qgqaa_W11_nfaaCA(qq_W11_nfaaCA)
    call AAJ_qgqaa_W11_nfCA(qq_W11_nfCA)
    call AAJ_qgqaa_W11_nfaaCF(qq_W11_nfaaCF)
    call AAJ_qgqaa_W11_nfCF(qq_W11_nfCF)
    call AAJ_qgqaa_W11_nfnfaa(qq_W11_nfnfaa)

    qgqaa_W11(1) = CA**2 * qq_W11_CA2       &
                 + CF**2 * qq_W11_CF2       &
                 + nfaaD**2 * qq_W11_nfaa2  &
                 + nf**2 * qq_W11_nf2       &
                 + CA*CF * qq_W11_CACF      &
                 + CA*nfaaD * qq_W11_nfaaCA &
                 + CA*nf * qq_W11_nfCA      &
                 + CF*nfaaD * qq_W11_nfaaCF &
                 + CF*nf * qq_W11_nfCF      &
                 + nfaaD*nf * qq_W11_nfnfaa
    qgqaa_W11(1) = qgqaa_W11(1) * Qdn4

    qgqaa_W11(2) = CA**2 * qq_W11_CA2       &
                 + CF**2 * qq_W11_CF2       &
                 + nfaaU**2 * qq_W11_nfaa2  &
                 + nf**2 * qq_W11_nf2       &
                 + CA*CF * qq_W11_CACF      &
                 + CA*nfaaU * qq_W11_nfaaCA &
                 + CA*nf * qq_W11_nfCA      &
                 + CF*nfaaU * qq_W11_nfaaCF &
                 + CF*nf * qq_W11_nfCF      &
                 + nfaaU*nf * qq_W11_nfnfaa
    qgqaa_W11(2) = qgqaa_W11(2) * Qup4

    !-- Add overall factor and average over initial spin/colours
    qgqaa_W11 = qgqaa_W11 * LOF * avgqg

    return
  end subroutine AAJ_qgqaa_W11

  !---------------------------------------------------------------------------!
  !                               Two-loop: qqb                               !
  !---------------------------------------------------------------------------!

  subroutine AAJ_qqgaa_W02_nfCA(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfCA(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_nfCA

  subroutine AAJ_qqgaa_W02_nfCF(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfCF(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_nfCF

  subroutine AAJ_qqgaa_W02_nfnfaa(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfnfaa(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_nfnfaa

  subroutine AAJ_qqgaa_W02_nfnf(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfnf(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_nfnf

  subroutine AAJ_qqgaa_W02_Ncp2(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_Nc2(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_Ncp2

  subroutine AAJ_qqgaa_W02_Nc0(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_Nc0(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_Nc0

  subroutine AAJ_qqgaa_W02_Ncm2(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_Ncm2(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_Ncm2

  subroutine AAJ_qqgaa_W02_nfNcp1(qqgaa_W02)
    implicit none
    real(8), intent(out) :: qqgaa_W02
    real(c_double) :: qqgaa_W02_CA, qqgaa_W02_CF
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfCA(aaj_obj, qqgaa_W02_CA)
       call aajamp_qqgaa_W02_nfCF(aaj_obj, qqgaa_W02_CF)
    endif
    qqgaa_W02 = qqgaa_W02_CA + qqgaa_W02_CF/2
    return
  end subroutine AAJ_qqgaa_W02_nfNcp1

  subroutine AAJ_qqgaa_W02_nfNcm1(qqgaa_W02)
    implicit none
    real(c_double), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfCF(aaj_obj, qqgaa_W02)
    endif
    qqgaa_W02 = -qqgaa_W02/2
    return
  end subroutine AAJ_qqgaa_W02_nfNcm1

  subroutine AAJ_qqgaa_W02_nfaaNcp1(qqgaa_W02)
    implicit none
    real(8), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfaaNc(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_nfaaNcp1

  subroutine AAJ_qqgaa_W02_nfaaNcm1(qqgaa_W02)
    implicit none
    real(8), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfaaNcm1(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_nfaaNcm1

  subroutine AAJ_qqgaa_W02_nfa(qqgaa_W02)
    implicit none
    real(8), intent(out) :: qqgaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W02_nfa(aaj_obj, qqgaa_W02)
    endif
    return
  end subroutine AAJ_qqgaa_W02_nfa

  subroutine AAJ_qqgaa_W02_LC(alpha,alphas,qqgaa_W02)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qqgaa_W02(2)
    real(8):: qq_W02_Nc2,qq_W02_nfNc,qq_W02_nfnf,tmp
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qqgaa_W02_Ncp2(qq_W02_Nc2)
    call AAJ_qqgaa_W02_nfNcp1(qq_W02_nfNc)
    call AAJ_qqgaa_W02_nfnf(qq_W02_nfnf)

    tmp = Nc2 * qq_W02_Nc2   &
        + nf*Nc*qq_W02_nfNc  &
        + nf**2*qq_W02_nfnf

    qqgaa_W02(1) = tmp * Qdn4
    qqgaa_W02(2) = tmp * Qup4

    !-- Factor 2 for 2 X Re[...]
    qqgaa_W02 = 2 * qqgaa_W02 * LOF * avgqqb

    return
  end subroutine AAJ_qqgaa_W02_LC

  subroutine AAJ_qqgaa_W02(alpha,alphas,qqgaa_W02)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qqgaa_W02(2)
    real(8):: qq_W02_Nc2,qq_W02_nfCA,qq_W02_nfCF,qq_W02_nfnfaa,qq_W02_nfnf
    real(8):: qq_W02_Nc0,qq_W02_Ncm2,qq_W02_nfaaNcp1,qq_W02_nfaaNcm1,qq_W02_nfa
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qqgaa_W02_Ncp2(qq_W02_Nc2)
    call AAJ_qqgaa_W02_Nc0(qq_W02_Nc0)
    call AAJ_qqgaa_W02_Ncm2(qq_W02_Ncm2)
    call AAJ_qqgaa_W02_nfCA(qq_W02_nfCA)
    call AAJ_qqgaa_W02_nfCF(qq_W02_nfCF)
    call AAJ_qqgaa_W02_nfnfaa(qq_W02_nfnfaa)
    call AAJ_qqgaa_W02_nfnf(qq_W02_nfnf)
    call AAJ_qqgaa_W02_nfaaNcp1(qq_W02_nfaaNcp1)
    call AAJ_qqgaa_W02_nfaaNcm1(qq_W02_nfaaNcm1)
    call AAJ_qqgaa_W02_nfa(qq_W02_nfa)

    qqgaa_W02(1) = Nc2 * qq_W02_Nc2           &
                 + qq_W02_Nc0                 &
                 + qq_W02_Ncm2 / Nc2          &
                 + nf*CA*qq_W02_nfCA          &
                 + nf*CF*qq_W02_nfCF          &
                 + nf*nfaaD*qq_W02_nfnfaa     &
                 + nf**2*qq_W02_nfnf          &
                 + (nfaaD*Nc)*qq_W02_nfaaNcp1 &
                 + (nfaaD/Nc)*qq_W02_nfaaNcm1 &
                 + nfaD * qq_W02_nfa*(Nc-4/Nc)
    qqgaa_W02(1) = qqgaa_W02(1) * Qdn4

    qqgaa_W02(2) = Nc2 * qq_W02_Nc2           &
                 + nf*CA*qq_W02_nfCA          &
                 + nf*CF*qq_W02_nfCF          &
                 + nf*nfaaU*qq_W02_nfnfaa     &
                 + nf**2*qq_W02_nfnf          &
                 + qq_W02_Nc0                 &
                 + qq_W02_Ncm2 / Nc2          &
                 + (nfaaU*Nc)*qq_W02_nfaaNcp1 &
                 + (nfaaU/Nc)*qq_W02_nfaaNcm1 &
                 + nfaU * qq_W02_nfa*(Nc-4/Nc)
    qqgaa_W02(2) = qqgaa_W02(2) * Qup4

    !-- Factor 2 for 2 X Re[...]
    qqgaa_W02 = 2 * qqgaa_W02 * LOF * avgqqb

    return
  end subroutine AAJ_qqgaa_W02

  !---------------------------------------------------------------------------!
  !                               Two-loop: qg                                !
  !---------------------------------------------------------------------------!

  subroutine AAJ_qgqaa_W02_nfCA(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfCA(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_nfCA

  subroutine AAJ_qgqaa_W02_nfCF(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfCF(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_nfCF

  subroutine AAJ_qgqaa_W02_nfnfaa(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfnfaa(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_nfnfaa

  subroutine AAJ_qgqaa_W02_nfnf(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfnf(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_nfnf

  subroutine AAJ_qgqaa_W02_Ncp2(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_Nc2(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_Ncp2

  subroutine AAJ_qgqaa_W02_Nc0(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_Nc0(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_Nc0

  subroutine AAJ_qgqaa_W02_Ncm2(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_Ncm2(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_Ncm2

  subroutine AAJ_qgqaa_W02_nfNcp1(qgqaa_W02)
    implicit none
    real(8), intent(out) :: qgqaa_W02
    real(c_double) :: qgqaa_W02_CA, qgqaa_W02_CF
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfCA(aaj_obj, qgqaa_W02_CA)
       call aajamp_qgqaa_W02_nfCF(aaj_obj, qgqaa_W02_CF)
    endif
    qgqaa_W02 = qgqaa_W02_CA + qgqaa_W02_CF/2
    return
  end subroutine AAJ_qgqaa_W02_nfNcp1

  subroutine AAJ_qgqaa_W02_nfNcm1(qgqaa_W02)
    implicit none
    real(c_double), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfCF(aaj_obj, qgqaa_W02)
    endif
    qgqaa_W02 = -qgqaa_W02/2
    return
  end subroutine AAJ_qgqaa_W02_nfNcm1

  subroutine AAJ_qgqaa_W02_nfaaNcp1(qgqaa_W02)
    implicit none
    real(8), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfaaNc(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_nfaaNcp1

  subroutine AAJ_qgqaa_W02_nfaaNcm1(qgqaa_W02)
    implicit none
    real(8), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfaaNcm1(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_nfaaNcm1

  subroutine AAJ_qgqaa_W02_nfa(qgqaa_W02)
    implicit none
    real(8), intent(out) :: qgqaa_W02
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W02_nfa(aaj_obj, qgqaa_W02)
    endif
    return
  end subroutine AAJ_qgqaa_W02_nfa

  subroutine AAJ_qgqaa_W02_LC(alpha,alphas,qgqaa_W02)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qgqaa_W02(2)
    real(8):: qg_W02_Nc2,qg_W02_nfNc,qg_W02_nfnf,tmp
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qgqaa_W02_Ncp2(qg_W02_Nc2)
    call AAJ_qgqaa_W02_nfNcp1(qg_W02_nfNc)
    call AAJ_qgqaa_W02_nfnf(qg_W02_nfnf)

    tmp = Nc2 * qg_W02_Nc2   &
        + nf*Nc*qg_W02_nfNc  &
        + nf**2*qg_W02_nfnf

    qgqaa_W02(1) = tmp * Qdn4
    qgqaa_W02(2) = tmp * Qup4

    !-- Factor 2 for 2 X Re[...]
    qgqaa_W02 = 2 * qgqaa_W02 * LOF * avgqg

    return
  end subroutine AAJ_qgqaa_W02_LC

  subroutine AAJ_qgqaa_W02(alpha,alphas,qgqaa_W02)
    implicit none
    real(8), intent(in)  :: alpha,alphas
    real(8), intent(out) :: qgqaa_W02(2)
    real(8):: qg_W02_LC,qg_W02_nfCA,qg_W02_nfCF,qg_W02_nfnfaa,qg_W02_nfnf
    real(8):: qg_W02_Nc0,qg_W02_Ncm2,qg_W02_nfaaNcp1,qg_W02_nfaaNcm1,qg_W02_nfa
    real(8) :: LOF

    LOF = xg * fourpi3 * alpha**2 * alphas

    call AAJ_qgqaa_W02_Ncp2(qg_W02_LC)
    call AAJ_qgqaa_W02_Nc0(qg_W02_Nc0)
    call AAJ_qgqaa_W02_Ncm2(qg_W02_Ncm2)
    call AAJ_qgqaa_W02_nfCA(qg_W02_nfCA)
    call AAJ_qgqaa_W02_nfCF(qg_W02_nfCF)
    call AAJ_qgqaa_W02_nfnfaa(qg_W02_nfnfaa)
    call AAJ_qgqaa_W02_nfnf(qg_W02_nfnf)
    call AAJ_qgqaa_W02_nfaaNcp1(qg_W02_nfaaNcp1)
    call AAJ_qgqaa_W02_nfaaNcm1(qg_W02_nfaaNcm1)
    call AAJ_qgqaa_W02_nfa(qg_W02_nfa)

    qgqaa_W02(1) = Nc2 * qg_W02_LC            &
                 + nf*CA*qg_W02_nfCA          &
                 + nf*CF*qg_W02_nfCF          &
                 + nf*nfaaD*qg_W02_nfnfaa     &
                 + nf**2*qg_W02_nfnf          &
                 + qg_W02_Nc0                 &
                 + qg_W02_Ncm2 / Nc2          &
                 + (nfaaD*Nc)*qg_W02_nfaaNcp1 &
                 + (nfaaD/Nc)*qg_W02_nfaaNcm1 &
                 + nfaD * qg_W02_nfa*(Nc-4/Nc)
    qgqaa_W02(1) = qgqaa_W02(1) * Qdn4

    qgqaa_W02(2) = Nc2 * qg_W02_LC            &
                 + nf*CA*qg_W02_nfCA          &
                 + nf*CF*qg_W02_nfCF          &
                 + nf*nfaaU*qg_W02_nfnfaa     &
                 + nf**2*qg_W02_nfnf          &
                 + qg_W02_Nc0                 &
                 + qg_W02_Ncm2 / Nc2          &
                 + (nfaaU*Nc)*qg_W02_nfaaNcp1 &
                 + (nfaaU/Nc)*qg_W02_nfaaNcm1 &
                 + nfaU * qg_W02_nfa*(Nc-4/Nc)
    qgqaa_W02(2) = qgqaa_W02(2) * Qup4

    !-- Factor 2 for 2 X Re[...]
    qgqaa_W02 = 2 * qgqaa_W02 * LOF * avgqg

    return
  end subroutine AAJ_qgqaa_W02

  subroutine AAJ_qqgaa_W12_nfaa2Ncp1(qqgaa_W12)
    implicit none
    real(8), intent(out) :: qqgaa_W12
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W12_nfaa2Ncp1(aaj_obj, qqgaa_W12)
    endif
    return
  end subroutine AAJ_qqgaa_W12_nfaa2Ncp1

  subroutine AAJ_qqgaa_W12_nfaa2Ncm1(qqgaa_W12)
    implicit none
    real(8), intent(out) :: qqgaa_W12
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W12_nfaa2Ncm1(aaj_obj, qqgaa_W12)
    endif
    return
  end subroutine AAJ_qqgaa_W12_nfaa2Ncm1

  subroutine AAJ_qqgaa_W12_nfaa2nf(qqgaa_W12)
    implicit none
    real(8), intent(out) :: qqgaa_W12
    if(c_associated(aaj_obj)) then
       call aajamp_qqgaa_W12_nfaa2nf(aaj_obj, qqgaa_W12)
    endif
    return
  end subroutine AAJ_qqgaa_W12_nfaa2nf

  subroutine AAJ_qgqaa_W12_nfaa2Ncp1(qgqaa_W12)
    implicit none
    real(8), intent(out) :: qgqaa_W12
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W12_nfaa2Ncp1(aaj_obj, qgqaa_W12)
    endif
    return
  end subroutine AAJ_qgqaa_W12_nfaa2Ncp1

  subroutine AAJ_qgqaa_W12_nfaa2Ncm1(qgqaa_W12)
    implicit none
    real(8), intent(out) :: qgqaa_W12
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W12_nfaa2Ncm1(aaj_obj, qgqaa_W12)
    endif
    return
  end subroutine AAJ_qgqaa_W12_nfaa2Ncm1

  subroutine AAJ_qgqaa_W12_nfaa2nf(qgqaa_W12)
    implicit none
    real(8), intent(out) :: qgqaa_W12
    if(c_associated(aaj_obj)) then
       call aajamp_qgqaa_W12_nfaa2nf(aaj_obj, qgqaa_W12)
    endif
    return
  end subroutine AAJ_qgqaa_W12_nfaa2nf

END MODULE aajamp
