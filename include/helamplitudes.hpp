#ifndef HELAMPLITUDES_H
#define HELAMPLITUDES_H

#ifdef HAVE_QD
#include "qd/dd_real.h"
#endif
#include <complex>
#include "aux/datatypes.hpp"
#include "aux/aux_functions.hpp"
#include "transcendental_f/qqb/qqb_l1_tf_CA_decl.hpp"
#include "transcendental_f/qg/qg_l1_tf_CA_decl.hpp"
#include "transcendental_f/qqb/qqb_l1_tf_CF_decl.hpp"
#include "transcendental_f/qg/qg_l1_tf_CF_decl.hpp"
#include "transcendental_f/qqb/qqb_l1_tf_nfaa_decl.hpp"
#include "transcendental_f/qg/qg_l1_tf_nfaa_decl.hpp"

/* ------------------------ 1-loop ------------------------ */
template <class T>
std::complex<T> qqb_HA1L_LmmpE_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmmpO_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmpmE_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmpmO_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmmE_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmmO_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmppE_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmppO_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmpE_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmpO_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LppmE_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LppmO_CA(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpppE_CA(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LpppO_CA(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmmE_CA(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmmO_CA(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmpE_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmmpO_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmpmE_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmpmO_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmmE_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmmO_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmppE_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmppO_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmpE_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmpO_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LppmE_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LppmO_CF(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpppE_CF(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LpppO_CF(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmmE_CF(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmmO_CF(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmpE_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmmpO_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmpmE_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmpmO_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmmE_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmmO_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmppE_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LmppO_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmpE_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpmpO_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LppmE_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LppmO_nfaa(
  const std::array<T,292>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qqb_HA1L_LpppE_nfaa(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LpppO_nfaa(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmmE_nfaa(
  const std::array<T,292>&);

template <class T>
std::complex<T> qqb_HA1L_LmmmO_nfaa(
  const std::array<T,292>&);

/*---------------------------------------------------------------------------*/

template <class T>
std::complex<T> qg_HA1L_LmmpE_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmmpO_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmpmE_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmpmO_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmmE_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmmO_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmppE_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmppO_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmpE_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmpO_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LppmE_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LppmO_CA(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpppE_CA(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LpppO_CA(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmmE_CA(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmmO_CA(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmpE_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmmpO_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmpmE_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmpmO_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmmE_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmmO_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmppE_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmppO_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmpE_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmpO_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LppmE_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LppmO_CF(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpppE_CF(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LpppO_CF(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmmE_CF(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmmO_CF(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmpE_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmmpO_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmpmE_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmpmO_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmmE_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmmO_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmppE_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LmppO_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmpE_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpmpO_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LppmE_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LppmO_nfaa(
  const std::array<T,275>&,
  const std::array<T,85>&,
  const std::array<std::complex<T>,19>&,
  const std::array<std::complex<T>,24>&);

template <class T>
std::complex<T> qg_HA1L_LpppE_nfaa(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LpppO_nfaa(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmmE_nfaa(
  const std::array<T,275>&);

template <class T>
std::complex<T> qg_HA1L_LmmmO_nfaa(
  const std::array<T,275>&);

/* ---------------------------- 2-loop --------------------------- */
#ifdef LOOPSQ_ENABLED
/* ----------------------------- qqb ----------------------------- */
template<class T>
std::complex<T> qqb_HA2L_LmmmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmmmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmmpE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmmpO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmpmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmpmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmppE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmppO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LpmmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LpmmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LpmpE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LpmpO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LppmE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LppmO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LpppE_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LpppO_nfnfaa(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf
);

template<class T>
std::complex<T> qqb_HA2L_LmmmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmmmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmmpE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmmpO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmpmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmpmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmppE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmppO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmpE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmpO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LppmE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LppmO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpppE_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpppO_nfaaNcp1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmmmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmmmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmmpE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmmpO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmpmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmpmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmppE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LmppO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmpE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpmpO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LppmE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LppmO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpppE_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qqb_HA2L_LpppO_nfaaNcm1(
  const std::array<T, 2438>& rf,
  const std::array<std::complex<T>, 1531>& tf,
  const std::complex<T> eps5
);

/* ----------------------------- qg ------------------------------ */
template<class T>
std::complex<T> qg_HA2L_LmmmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmmmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmmpE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmmpO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmpmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmpmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmppE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmppO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LpmmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LpmmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LpmpE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LpmpO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LppmE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LppmO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LpppE_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LpppO_nfnfaa(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf
);

template<class T>
std::complex<T> qg_HA2L_LmmmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmmmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmmpE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmmpO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmpmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmpmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmppE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmppO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmpE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmpO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LppmE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LppmO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpppE_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpppO_nfaaNcp1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmmmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmmmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmmpE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmmpO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmpmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmpmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmppE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LmppO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmpE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpmpO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LppmE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LppmO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpppE_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);

template<class T>
std::complex<T> qg_HA2L_LpppO_nfaaNcm1(
  const std::array<T, 2436>& rf,
  const std::array<std::complex<T>, 1466>& tf,
  const std::complex<T> eps5
);
#endif /* LOOPSQ_ENABLED */

#endif /* ifndef HELAMPLITUDES_H */
