#ifndef INTERFERENCES_H
#define INTERFERENCES_H

#include <complex>
#include <array>
#include <vector>
#include "PentagonFunctions/FunctionID.h"

template <typename T>
class Interference{

  using TC = std::complex<T>;

  private:

    std::array<T,5>  sij; // {s12,s23,s34,s45,s15}
    std::array<T,5>  v;   // {s12,s23,s34,s45,s15}/musq, args of PentaFuns
    std::array<T,31> k;   // invariants + denominators
    std::array<T,30> kk;  // invariants + denominators (normalised to s12)
    T musq;               // \mu^2
    T gram, im_eps5;      // Imag(eps5), gram = Det(2*pi.pj)
    TC eps5, eps5t;       // Sqrt[gram]
    TC tr5;               // Sqrt[Delta], normalised by s12^2

    // Tree-level squared
    std::array<T,1> rat0l_qqgaa;
    std::array<T,1> rat0l_qgqaa;

    // rational functions: tree-1loop interference
    std::array<T,55> rat1l_qqgaa;
    std::array<T,55> rat1l_qgqaa;

    // array of rational functions for 1-loop helicity amplitudes
    std::array<T,292> hrat_qqgaa;
    std::array<T,275> hrat_qgqaa;

    // rational functions: tree-2loop(LC) interference
    std::array<T,343> rat2lLC_qqgaa;
    std::array<T,223> rat2lLC_qgqaa;

#ifdef FULLCOL_ENABLED
    std::array<T,1365> rat2lNLC_qqgaa;
    std::array<T,1486> rat2lNLC_qgqaa;
#endif

#ifdef LOOPSQ_ENABLED
    std::array<T,2438> hrat2l_qqgaa;
    std::array<T,2436> hrat2l_qgqaa;
#endif

    // array of squared spinor phases
    std::array<T,8> SpPh_qqb;
    std::array<T,8> SpPh_qg;

    // transcendental constants
    std::array<T,85> tcs;

    // Pentagon functions
    std::array<TC,19> fw1;
    std::vector<PentagonFunctions::FunctionObjectType<T>> PF_evaluators_w1;

    std::array<TC,24> fw2;
    std::vector<PentagonFunctions::FunctionObjectType<T>> PF_evaluators_w2;

    std::array<TC,82> fw3;
    std::vector<PentagonFunctions::FunctionObjectType<T>> PF_evaluators_w3;

    std::array<TC,330> fw4;
    std::vector<PentagonFunctions::FunctionObjectType<T>> PF_evaluators_w4;

#if defined (FULLCOL_ENABLED) || defined (LOOPSQ_ENABLED)
    std::array<TC,25> fw1np;
    std::array<TC,111> fw3np;
    std::array<TC,472> fw4np;
#endif

    // transcendental functions: tree-2loop interference (LC)
    std::array<TC,409> tf2lLC_qqgaa;

    // transcendental functions: tree-2loop interference (NLC)
#ifdef FULLCOL_ENABLED
    std::array<TC,1361> tf2lNLC_qqgaa;
    std::array<TC,1482> tf2lNLC_qgqaa;
#endif

    // transcendental functions: 2-loop helicity amplitudes
#ifdef LOOPSQ_ENABLED
    std::array<TC,1531> tf2lha_qqgaa;
    std::array<TC,1466> tf2lha_qgqaa;
#endif

    // Helicity amplitudes
    std::array<TC,16> Aqqb_1l_CA;
    std::array<TC,16> Aqqb_1l_CF;
    std::array<TC,16> Aqqb_1l_nfaa;
    std::array<TC,16> Aqqb_1l_nf;

    std::array<TC,16> Aqg_1l_CA;
    std::array<TC,16> Aqg_1l_CF;
    std::array<TC,16> Aqg_1l_nfaa;
    std::array<TC,16> Aqg_1l_nf;

#ifdef LOOPSQ_ENABLED
    std::array<TC,16> Aqqb_2l_nfaaNcp1;
    std::array<TC,16> Aqqb_2l_nfaaNcm1;
    std::array<TC,16> Aqqb_2l_nfnfaa;

    std::array<TC,16> Aqg_2l_nfaaNcp1;
    std::array<TC,16> Aqg_2l_nfaaNcm1;
    std::array<TC,16> Aqg_2l_nfnfaa;
#endif

    bool pentagon_functions_w1_initialised;
    bool pentagon_functions_w2_initialised;
    bool pentagon_functions_w3_initialised;
    bool pentagon_functions_w4_initialised;

    bool qqb_active;
    bool qg_active;

    int process_id;
    int loop_ord;
    void set_k();

    void set_rational_functions();
    void set_spinor_phases();
    void set_1lHAMP_rational_functions();
#ifdef LOOPSQ_ENABLED
    void set_2lHAMP_rational_functions();
#endif

    void set_tcs();

    void initialise_PentagonFunctions_w1();
    void initialise_PentagonFunctions_w2();
    void initialise_PentagonFunctions_w3();
    void initialise_PentagonFunctions_w4();

    void set_fw1();
    void set_fw2();
    void set_fw3();
    void set_fw4();

    void set_transcendental_functions();

    void evaluate_1loop_helamplitudes();

#ifdef LOOPSQ_ENABLED
    void set_2lHAMP_transcendental_functions();
    void evaluate_2loop_helamplitudes();
#endif

  public:

    Interference(){
        loop_ord = -1;
        process_id = -1;
        musq = 1;
        pentagon_functions_w1_initialised = false;
        pentagon_functions_w2_initialised = false;
        pentagon_functions_w3_initialised = false;
        pentagon_functions_w4_initialised = false;
        qqb_active = false;
        qg_active = false;
        this->set_tcs();
    };

    void register_interf_process(int);
    void register_interf_loop_order(int);

    void register_interf_event(const std::array<T,5>&, const T);

    // tree x tree interference
    T qqgaa_interf_W00();
    T qgqaa_interf_W00();

    // 2 Re[tree* x 1-loop] interference contributions
    T qqgaa_interf_W01_CA();
    T qqgaa_interf_W01_CF();
    T qqgaa_interf_W01_nfaa();
    T qqgaa_interf_W01_nf();

    T qgqaa_interf_W01_CA();
    T qgqaa_interf_W01_CF();
    T qgqaa_interf_W01_nfaa();
    T qgqaa_interf_W01_nf();

    // 1-loop x 1-loop interference
    T qqgaa_interf_W11_CACA();
    T qqgaa_interf_W11_CFCF();
    T qqgaa_interf_W11_nfaanfaa();
    T qqgaa_interf_W11_nfnf();
    T qqgaa_interf_W11_CACF();
    T qqgaa_interf_W11_nfaaCA();
    T qqgaa_interf_W11_nfCA();
    T qqgaa_interf_W11_nfaaCF();
    T qqgaa_interf_W11_nfCF();
    T qqgaa_interf_W11_nfnfaa();

    T qgqaa_interf_W11_CACA();
    T qgqaa_interf_W11_CFCF();
    T qgqaa_interf_W11_nfaanfaa();
    T qgqaa_interf_W11_nfnf();
    T qgqaa_interf_W11_CACF();
    T qgqaa_interf_W11_nfaaCA();
    T qgqaa_interf_W11_nfCA();
    T qgqaa_interf_W11_nfaaCF();
    T qgqaa_interf_W11_nfCF();
    T qgqaa_interf_W11_nfnfaa();

    // 2 Re[tree* x 2-loop] interference contributions
    T qqgaa_interf_W02_Nc2();
    T qqgaa_interf_W02_nfCA();
    T qqgaa_interf_W02_nfCF();
    T qqgaa_interf_W02_nfnfaa();
    T qqgaa_interf_W02_nfnf();
    T qqgaa_interf_W02_nfa();
#ifdef FULLCOL_ENABLED
    T qqgaa_interf_W02_Nc0();
    T qqgaa_interf_W02_Ncm2();
    T qqgaa_interf_W02_nfaaNc();
    T qqgaa_interf_W02_nfaaNcm1();
#endif

    T qgqaa_interf_W02_Nc2();
    T qgqaa_interf_W02_nfCA();
    T qgqaa_interf_W02_nfCF();
    T qgqaa_interf_W02_nfnfaa();
    T qgqaa_interf_W02_nfnf();
#ifdef FULLCOL_ENABLED
    T qgqaa_interf_W02_Nc0();
    T qgqaa_interf_W02_Ncm2();
    T qgqaa_interf_W02_nfaaNc();
    T qgqaa_interf_W02_nfaaNcm1();
    T qgqaa_interf_W02_nfa();
#endif

#ifdef LOOPSQ_ENABLED
    T qqgaa_interf_W12_nfaa2Ncp1();
    T qqgaa_interf_W12_nfaa2Ncm1();
    T qqgaa_interf_W12_nfaa2nf();

    T qgqaa_interf_W12_nfaa2Ncp1();
    T qgqaa_interf_W12_nfaa2Ncm1();
    T qgqaa_interf_W12_nfaa2nf();
#endif
};
 
#endif /* ifndef INTERFERENCES_H */
