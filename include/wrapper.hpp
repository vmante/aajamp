#ifndef WRAPPER_H
#define WRAPPER_H

#include <iostream>
#include <array>
#include "aajamp.hpp"

typedef void * AAJObject;

extern "C" {
  AAJObject aajamp_init();
  void aajamp_destroy(AAJObject);
  void aajamp_register_process(AAJObject, int);
  void aajamp_register_loop_order(AAJObject, int);
  void aajamp_register_event(AAJObject, const double *, const double);

  /* Tree-level: qqb */
  void aajamp_qqgaa_W00(AAJObject, double&);

  /* Tree-level: qg */
  void aajamp_qgqaa_W00(AAJObject, double&);

  /* One Loop: qqb */
  void aajamp_qqgaa_W01_CA(AAJObject, double&);
  void aajamp_qqgaa_W01_CF(AAJObject, double&);
  void aajamp_qqgaa_W01_nfaa(AAJObject, double&);
  void aajamp_qqgaa_W01_nf(AAJObject, double&);

  /* One Loop: qg */
  void aajamp_qgqaa_W01_CA(AAJObject, double&);
  void aajamp_qgqaa_W01_CF(AAJObject, double&);
  void aajamp_qgqaa_W01_nfaa(AAJObject, double&);
  void aajamp_qgqaa_W01_nf(AAJObject, double&);

  /* One Loop^2: qqb */
  void aajamp_qqgaa_W11_CACA(AAJObject, double&);
  void aajamp_qqgaa_W11_CFCF(AAJObject, double&);
  void aajamp_qqgaa_W11_nfaanfaa(AAJObject, double&);
  void aajamp_qqgaa_W11_nfnf(AAJObject, double&);
  void aajamp_qqgaa_W11_CACF(AAJObject, double&);
  void aajamp_qqgaa_W11_nfaaCA(AAJObject, double&);
  void aajamp_qqgaa_W11_nfCA(AAJObject, double&);
  void aajamp_qqgaa_W11_nfaaCF(AAJObject, double&);
  void aajamp_qqgaa_W11_nfCF(AAJObject, double&);
  void aajamp_qqgaa_W11_nfnfaa(AAJObject, double&);

  /* One Loop^2: qg */
  void aajamp_qgqaa_W11_CACA(AAJObject, double&);
  void aajamp_qgqaa_W11_CFCF(AAJObject, double&);
  void aajamp_qgqaa_W11_nfaanfaa(AAJObject, double&);
  void aajamp_qgqaa_W11_nfnf(AAJObject, double&);
  void aajamp_qgqaa_W11_CACF(AAJObject, double&);
  void aajamp_qgqaa_W11_nfaaCA(AAJObject, double&);
  void aajamp_qgqaa_W11_nfCA(AAJObject, double&);
  void aajamp_qgqaa_W11_nfaaCF(AAJObject, double&);
  void aajamp_qgqaa_W11_nfCF(AAJObject, double&);
  void aajamp_qgqaa_W11_nfnfaa(AAJObject, double&);

  /* Two Loop: qqb */
  void aajamp_qqgaa_W02_Nc2(AAJObject, double&);
  void aajamp_qqgaa_W02_nfCA(AAJObject, double&);
  void aajamp_qqgaa_W02_nfCF(AAJObject, double&);
  void aajamp_qqgaa_W02_nfnfaa(AAJObject, double&);
  void aajamp_qqgaa_W02_nfnf(AAJObject, double&);
  void aajamp_qqgaa_W02_nfa(AAJObject, double&);
#ifdef FULLCOL_ENABLED
  void aajamp_qqgaa_W02_Nc0(AAJObject, double&);
  void aajamp_qqgaa_W02_Ncm2(AAJObject, double&);
  void aajamp_qqgaa_W02_nfaaNc(AAJObject, double&);
  void aajamp_qqgaa_W02_nfaaNcm1(AAJObject, double&);
#endif

  /* Two Loop: qg */
  void aajamp_qgqaa_W02_Nc2(AAJObject, double&);
  void aajamp_qgqaa_W02_nfCA(AAJObject, double&);
  void aajamp_qgqaa_W02_nfCF(AAJObject, double&);
  void aajamp_qgqaa_W02_nfnfaa(AAJObject, double&);
  void aajamp_qgqaa_W02_nfnf(AAJObject, double&);
#ifdef FULLCOL_ENABLED
  void aajamp_qgqaa_W02_Nc0(AAJObject, double&);
  void aajamp_qgqaa_W02_Ncm2(AAJObject, double&);
  void aajamp_qgqaa_W02_nfaaNc(AAJObject, double&);
  void aajamp_qgqaa_W02_nfaaNcm1(AAJObject, double&);
  void aajamp_qgqaa_W02_nfa(AAJObject, double&);
#endif

#ifdef LOOPSQ_ENABLED
  void aajamp_qqgaa_W12_nfaa2Ncp1(AAJObject, double&);
  void aajamp_qqgaa_W12_nfaa2Ncm1(AAJObject, double&);
  void aajamp_qqgaa_W12_nfaa2nf(AAJObject, double&);

  void aajamp_qgqaa_W12_nfaa2Ncp1(AAJObject, double&);
  void aajamp_qgqaa_W12_nfaa2Ncm1(AAJObject, double&);
  void aajamp_qgqaa_W12_nfaa2nf(AAJObject, double&);
#endif
}

#endif /* ifndef WRAPPER_H */
