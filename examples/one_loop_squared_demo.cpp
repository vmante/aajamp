#include <iostream>
#include <iomanip>
#include <cmath>
#include <array>
#include "aajamp.hpp"

int main() {

    aajAmp<double> amp;

    double pi = 3.1415926535897932384626433832795;
    double musq, loopXloop, nnlo;
    double oneloop_sq_CACA,oneloop_sq_CFCF,oneloop_sq_nfaanfaa,oneloop_sq_nfnf;
    double oneloop_sq_CACF,oneloop_sq_nfaaCA,oneloop_sq_nfCA;
    double oneloop_sq_nfaaCF,oneloop_sq_nfCF;
    double oneloop_sq_nfnfaa;
    double twopi, fourpi, Qu, Qd, alphas, alpha;
    double Nc2, CA, CF, nf, nfaa;

    std::array<double,5> kin;

    kin = { 1000000.0000000000000000000000000,
           -761244.13046595612000000000000000,
            865719.13783475631647928302586063,
            126204.04903121366344093936054110,
           -29885.559994750778000000000000000};

    // q q~ -> g a a
    amp.register_process(2);

    // request 1-loop
    amp.register_loop_order(11); 

    // mu^2 = s12
    musq = 100*100;
    amp.register_event(kin,musq);

    // Global factors
    twopi = 2*pi;
    fourpi = 4*pi;

    // QCD factors and electric charges
    Nc2 = 9;
    nf = 5;
    CA = 3;
    CF = 4./3;
    Qu = 2./3;
    Qd = -1./3;
    nfaa = (3*Qd*Qd + 2*Qu*Qu)/(Qu*Qu);

    // alpha_s and alpha
    alphas = 0.118;
    alpha  = 0.0075557860007568903; // ~ 1./132.35

    // Leading Order Factor (LOF): (Nc^2-1) * (e*Qf1)^4*gs^2
    // results for u u~ -> g a a
    double LOF = (Nc2-1)*pow(fourpi,3.)*pow(Qu,4.)*pow(alpha,2.)*alphas;

    // Colour and spin average
    double avg = (4*Nc2);

    // Re[1loop^* x 1-loop]
    amp.qgqaa_W11_CACA(oneloop_sq_CACA);
    amp.qgqaa_W11_CFCF(oneloop_sq_CFCF);
    amp.qgqaa_W11_nfaanfaa(oneloop_sq_nfaanfaa);
    amp.qgqaa_W11_nfnf(oneloop_sq_nfnf);
    amp.qgqaa_W11_CACF(oneloop_sq_CACF);
    amp.qgqaa_W11_nfaaCA(oneloop_sq_nfaaCA);
    amp.qgqaa_W11_nfCA(oneloop_sq_nfCA);
    amp.qgqaa_W11_nfaaCF(oneloop_sq_nfaaCF);
    amp.qgqaa_W11_nfCF(oneloop_sq_nfCF);
    amp.qgqaa_W11_nfnfaa(oneloop_sq_nfnfaa);

    // Build final results for squared matrix element
    /*-----------------------------------------------------------------------*/

    loopXloop = CA*CA*oneloop_sq_CACA + 
                CF*CF*oneloop_sq_CFCF + 
                nfaa*nfaa*oneloop_sq_nfaanfaa + 
                nf*nf*oneloop_sq_nfnf +
                CA*CF*oneloop_sq_CACF + 
                CA*nfaa*oneloop_sq_nfaaCA + 
                CA*nf*oneloop_sq_nfCA + 
                CF*nfaa*oneloop_sq_nfaaCF + 
                CF*nf*oneloop_sq_nfCF + 
                nf*nfaa*oneloop_sq_nfnfaa;

    // powers of alpha_s
//     loopXloop *= pow(alphas/twopi,2.);

    // LOF: multiply with LO factor
    // avg: divide by spin-colour average
    // 2  : coming from 2 Re[..]
    nnlo  = loopXloop;

    std::cout << std::scientific << std::setprecision(17);

    std::cout << "1-loop^2 CAxCA.......: " << oneloop_sq_CACA << std::endl;
    std::cout << "1-loop^2 CFxCF.......: " << oneloop_sq_CFCF << std::endl;
    std::cout << "1-loop^2 nfaaxnfaa...: " << oneloop_sq_nfaanfaa << std::endl;
    std::cout << "1-loop^2 nfxnf.......: " << oneloop_sq_nfnf << std::endl;
    std::cout << "1-loop^2 CAxCF.......: " << oneloop_sq_CACF << std::endl;
    std::cout << "1-loop^2 CAxnfaa.....: " << oneloop_sq_nfaaCA << std::endl;
    std::cout << "1-loop^2 CAxnf.......: " << oneloop_sq_nfCA << std::endl;
    std::cout << "1-loop^2 CFxnfaa.....: " << oneloop_sq_nfaaCF << std::endl;
    std::cout << "1-loop^2 CFxnf.......: " << oneloop_sq_nfCF << std::endl;
    std::cout << "1-loop^2 nfxnfaa.....: " << oneloop_sq_nfnfaa << std::endl;
    std::cout << "\n";
    std::cout << "NNLO-loop^2..........: " << nnlo << std::endl;

    return 0;
}
