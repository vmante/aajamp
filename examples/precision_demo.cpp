#include <iostream>
#include <iomanip>
#include <array>
#include "aajamp.hpp"

int main() {

    aajAmp<double> amp1;

    // rescaling factor
    double rsc = 1.51;
    double musq, musq_rsc, twoloop_Nc2, twoloop_Nc2_rsc;

    std::array<double,5> kin = {1000000.0000000000,-582091.95606054901,
                                6.3861761077278061,212709.37248973263,
                                -739848.47757308464};
    std::array<double,5> kin_rsc = {};

    // Rescaled kinematics
    for (unsigned int i = 0; i < 5; i++)
    {
        kin_rsc[i] = kin[i] * rsc;
    }

    std::cout << std::scientific << std::setprecision(17);

    // deactivate stability rescue system
    amp1.set_stability_rescue(-1);

    // q q~ -> g a a
    amp1.register_process(1);
    amp1.register_loop_order(2);

    musq = kin[0];
    amp1.register_event(kin,musq);
    amp1.qqgaa_W02_Nc2(twoloop_Nc2);

    std::cout << "First evaluation.  Rescue off  : " << twoloop_Nc2 << std::endl;

    musq_rsc = kin_rsc[0];
    amp1.register_event(kin_rsc,musq_rsc);
    amp1.qqgaa_W02_Nc2(twoloop_Nc2_rsc);

    std::cout << "Second evaluation. Rescue off  : " << twoloop_Nc2_rsc*rsc << std::endl;

    // activate back stability rescue system
    amp1.set_stability_rescue(1);

    musq = kin[0];
    amp1.register_event(kin,musq);
    amp1.qqgaa_W02_Nc2(twoloop_Nc2);

    std::cout << "First evaluation.  Rescue on   : " << twoloop_Nc2 << std::endl;

    musq_rsc = kin_rsc[0];
    amp1.register_event(kin_rsc,musq_rsc);
    amp1.qqgaa_W02_Nc2(twoloop_Nc2_rsc);

    std::cout << "Second evaluation. Rescue on   : " << twoloop_Nc2_rsc*rsc << std::endl;

    // reduce stability threshold for activation of rescue system
    // Condition is not met, the evaluation is done in double precision
    amp1.set_stability_threshold(1e-6);
    musq = kin[0];
    amp1.register_event(kin,musq);
    amp1.qqgaa_W02_Nc2(twoloop_Nc2);

    std::cout << "Evaluation with 1e-6 threshold : " << twoloop_Nc2 << std::endl;

    // increase stability threshold for activation of rescue system
    // Condition is met, the evaluation is done in quad-precision
    amp1.set_stability_threshold(1e-3);
    musq = kin[0];
    amp1.register_event(kin,musq);
    amp1.qqgaa_W02_Nc2(twoloop_Nc2);

    std::cout << "Evaluation with 1e-3 threshold : " << twoloop_Nc2 << std::endl;

    return 0;
}
