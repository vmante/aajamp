#include <iostream>
#include <iomanip>
#include <cmath>
#include <array>
#include "aajamp.hpp"

int main() {

    aajAmp<double> amp;

    double pi = 3.1415926535897932384626433832795;
    double born, nlo, treeX1loop;
    double musq, oneloop_CA, oneloop_CF, oneloop_nfaa, oneloop_nf;
    double twopi, fourpi, Qu, Qd, alphas, alpha;
    double Nc2, CA, CF, nf, nfaa;

    std::array<double,5> kin;

    kin = { 1000000.0000000000000000000000000,
           -761244.13046595612000000000000000,
            865719.13783475631647928302586063,
            126204.04903121366344093936054110,
           -29885.559994750778000000000000000};

    // q q~ -> g a a
    amp.register_process(1);

    // request 1-loop
    amp.register_loop_order(1); 

    // mu^2 = 100^2
    musq = 100*100;
    amp.register_event(kin,musq);

    // Global factors
    twopi = 2*pi;
    fourpi = 4*pi;

    // QCD factors and electric charges
    Nc2 = 9;
    nf = 5;
    CA = 3;
    CF = 4./3;
    Qu = 2./3;
    Qd = -1./3;
    nfaa = (3*Qd*Qd + 2*Qu*Qu)/(Qu*Qu);

    // alpha_s and alpha
    alphas = 0.118;
    alpha  = 0.0075557860007568903; // ~ 1./132.35

    // Leading Order Factor (LOF): (Nc^2-1) * (e*Qf1)^4*gs^2
    // results for u u~ -> g a a
    double LOF = (Nc2-1)*pow(fourpi,3.)*pow(Qu,4.)*pow(alpha,2.)*alphas;

    // Colour and spin average
    double avg = (4*Nc2);

    // tree^* x tree
    amp.qqgaa_W00(born);

    // Re[tree^* x 1-loop]
    amp.qqgaa_W01_CA(oneloop_CA);
    amp.qqgaa_W01_CF(oneloop_CF);
    amp.qqgaa_W01_nfaa(oneloop_nfaa);
    amp.qqgaa_W01_nf(oneloop_nf);

    // Build final results for squared matrix element
    /*-----------------------------------------------------------------------*/

    treeX1loop = CA*oneloop_CA + 
                 CF*oneloop_CF + 
                 nfaa*oneloop_nfaa + 
                 nf*oneloop_nf;

    // powers of alpha_s
    treeX1loop *= alphas/twopi;

    // LOF: multiply with LO factor
    // avg: divide by spin-colour average
    // 2  : coming from 2 Re[..]
    born *= LOF/avg;
    nlo   = (LOF/avg)*2*treeX1loop;

    std::cout << std::scientific << std::setprecision(17);

    std::cout << "1-loop CA............: " << oneloop_CA << std::endl;
    std::cout << "1-loop CF............: " << oneloop_CF << std::endl;
    std::cout << "1-loop nfaa..........: " << oneloop_nfaa << std::endl;
    std::cout << "1-loop nf............: " << oneloop_nf << std::endl;
    std::cout << "\n";
    std::cout << "LO...................: " << born << std::endl;
    std::cout << "NLO..................: " << nlo << std::endl;

    return 0;
}
