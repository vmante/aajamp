#include <iostream>
#include <iomanip>
#include <cmath>
#include <array>
#include "aajamp.hpp"

int main() {

    aajAmp<double> amp;

    double pi = 3.1415926535897932384626433832795;
    double born, nnlo, treeX2loop;
    double musq;
    double twoloop_Nc2, twoloop_nfCA, twoloop_nfCF, twoloop_nfnfaa, twoloop_nfnf;
    double twopi, fourpi, Qu, Qd, alphas, alpha;
    double Nc2, CA, CF, nf, nfaa;

    std::array<double,5> kin;

    kin = { 1000000.0000000000000000000000000,
           -761244.13046595612000000000000000,
            865719.13783475631647928302586063,
            126204.04903121366344093936054110,
           -29885.559994750778000000000000000};

    // q q~ -> g a a
    amp.register_process(1);

    // request 2-loop
    amp.register_loop_order(2); 

    // mu^2 = s12
    musq = kin[0];

    amp.register_event(kin,musq);

    // Global factors
    twopi = 2*pi;
    fourpi = 4*pi;

    // QCD factors and electric charges
    Nc2 = 9;
    nf = 5;
    CA = 3;
    CF = 4./3;
    Qu = 2./3;
    Qd = -1./3;
    nfaa = (3*Qd*Qd + 2*Qu*Qu)/(Qu*Qu);

    // alpha_s and alpha
    alphas = 0.118;
    alpha  = 0.0075557860007568903; // ~ 1./132.35

    // Leading Order Factor (LOF): (Nc^2-1) * (e*Qf1)^4*gs^2
    // results for u u~ -> g a a
    double LOF = (Nc2-1)*pow(fourpi,3.)*pow(Qu,4.)*pow(alpha,2.)*alphas;

    // Colour and spin average
    double avg = (4*Nc2);

    // tree^* x tree
    amp.qqgaa_W00(born);

    // Re[tree^* x 2-loop]
    amp.qqgaa_W02_Nc2(twoloop_Nc2);
    amp.qqgaa_W02_nfCA(twoloop_nfCA);
    amp.qqgaa_W02_nfCF(twoloop_nfCF);
    amp.qqgaa_W02_nfnfaa(twoloop_nfnfaa);
    amp.qqgaa_W02_nfnf(twoloop_nfnf);

    // Build final results for squared matrix element
    /*-----------------------------------------------------------------------*/

    treeX2loop = Nc2*twoloop_Nc2 +
                 nf*CA*twoloop_nfCA +
                 nf*CF*twoloop_nfCF +
                 nf*nfaa*twoloop_nfnfaa +
                 nf*nf*twoloop_nfnf;

    // powers of alpha_s
    treeX2loop *= pow(alphas/twopi,2.);

    // LOF: multiply with LO factor
    // avg: divide by spin-colour average
    // 2  : coming from 2 Re[..]
    born *= LOF/avg;
    nnlo  = (LOF/avg)*2*treeX2loop;

    std::cout << std::scientific << std::setprecision(17);

    std::cout << "2-loop Nc^2......: " << twoloop_Nc2 << std::endl;
    std::cout << "2-loop nf*CA.....: " << twoloop_nfCA << std::endl;
    std::cout << "2-loop nf*CF.....: " << twoloop_nfCF << std::endl;
    std::cout << "2-loop nf*nfaa...: " << twoloop_nfnfaa << std::endl;
    std::cout << "2-loop nf^2......: " << twoloop_nfnf << std::endl;

    std::cout << "\n";
    std::cout << "LO...............: " << born << std::endl;
    std::cout << "NNLO.............: " << nnlo << std::endl;

    return 0;
}
